var gulp = require('gulp');
var sass = require('gulp-sass');
var jade = require('gulp-jade');
var browserSync = require('browser-sync');
var imagemin = require('gulp-imagemin'); // Needed to Optimize Images
//var uglify = require('gulp-uglify');
//var jade = require('gulp-jade');
//var livereload = require('gulp-livereload');
var cache = require('gulp-cache'); // Speeds up image optimze
var runSequence = require('run-sequence'); // Needed for default file
var cssnano = require('gulp-cssnano');
var fs = require('fs');
/**
 * Sass task for live injecting into all browsers
 */
gulp.task('sass', function () {
	return gulp.src('./app/scss/**/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('./assets/css'))
		.pipe(browserSync.reload({
			stream: true
		}));
})

gulp.task('useref', function(){
	return gulp.src('./app/css/*.css')
		.pipe(useref())
		// Minifies only if it's a CSS file
		.pipe(gulpIf('*.css', cssnano()))
		.pipe(gulp.dest('./assets/css'))
});

/**
 * Compile jade files into HTML
 */
gulp.task('jade', function () {
	return gulp.src('./app/jade/**/*.jade')
		.pipe(jade({
			pretty: true,
			locals: JSON.parse( fs.readFileSync('./app/json/datas.json', { encoding: 'utf8' }) ),
		}))
		.pipe(gulp.dest('./'))
})

/**
 * Watches file for changes
 */

gulp.task('watch', ['browserSync', 'sass', 'jade'], function () { // Include broswerSync with sass to get tasks to run together
	gulp.watch('./app/scss/**/*.scss', ['sass']);
	// Reloads the browser whenever HTML or JS files change
	gulp.watch('./app/jade/**/*.jade', ['jade']);
	gulp.watch('app/js/*.js', browserSync.reload);
	// Other watchers
});

// Live Reload
gulp.task('browserSync', function () {
	browserSync({
		startPath: '/',
		server: {
			baseDir: './' // Directory for server
		},
	})
	gulp.watch("*.html").on("change", browserSync.reload); // For changes in html file to activate LiveReload
});

/***
 *  Image Optimizer
 ***/
gulp.task('images', function () {
	return gulp.src('./app/images/*.+(png|jpg|gif|svg)') // Path to images and included image types
		.pipe(imagemin())
		.pipe(gulp.dest('./assets/images')) // Destination for optimized images
});

/***
 *  Speed Up Image Optimizer
 ***/
gulp.task('images', function () {
	return gulp.src('./app/images/**/*.+(png|jpg|jpeg|gif|svg)')
		// Caching images that ran through imagemin
		.pipe(cache(imagemin({
			interlaced: true
		})))
		.pipe(gulp.dest('./assets/images'))
});


/***
 *  Fonts
 ***/
gulp.task('fonts', function () {
	return gulp.src(['./app/fonts/*'])
		.pipe(gulp.dest('./assets/fonts'));
});

/**
 * Styles
 */
gulp.task('styles', function () {
	return gulp.src(['./app/css/*.css'])
		.pipe(gulp.dest('./assets/css'))
});

/**
 * CSS
 */
gulp.task('useref', function(){
	return gulp.src(['./app/css/*.css'])
		.pipe(useref())
		// Minifies only if it's a CSS file
		.pipe(gulpIf('*.css', cssnano()))
		.pipe(gulp.dest('dist'))
});

/**
 * Script
 */
gulp.task('script', function () {
	return gulp.src(['./app/js/*.js'])
		.pipe(gulp.dest('./assets/js'))
});


gulp.task('default', function (callback) {
	runSequence(['sass', 'jade', 'browserSync', 'watch', 'images', 'fonts', 'styles', 'script'],
		callback
	)
})
