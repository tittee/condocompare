<?php
defined('BASEPATH') OR exit();
class Configsystem extends CI_Controller {
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));								/***** LOADING HELPER TO AVOID PHP ERROR ****/
		$this->load->library('primaryclass');
		$this->load->library('dateclass');
		$this->load->model('Staff_model');								/***** LOADING Controller * Staff_model as all ****/
		$this->Staff=$this->Staff_model->info();
		$this->load->model('Configsystem_model');								/***** LOADING Controller * Primaryfunc as all ****/
		$this->load->model('M');								/***** LOADING Controller * Primaryfunc as all ****/
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function index()
	{
		$R=$this->Staff_model->info();
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	/* View Offering */
	public function view_type_offering()
	{
		$data= $this->Configsystem_model->get_type_offering();
		//
		$this->load->view('backoffice/configsystem/datatable-type-offering',array(
			'title'=>'ตารางข้อมูลประเภทการเสนอขาย',
			'header'=>'yes',
			'url_href'=>'add_type_offering',
			'js'=>array(''),
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_type_offering()
	{
		$this->load->view('backoffice/configsystem/formadmin-type-offering',array(
			'title'=>'เพิ่มข้อมูลประเภทการเสนอขาย',
			'header'=>'yes',
			'css'=>array(
				'assets/plugins/fileuploads/css/dropify.min.css',
				'assets/plugins/timepicker/bootstrap-timepicker.min.css',
				'assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
			),
			'js'=>array(
				'assets/plugins/moment/moment.js',
				'assets/plugins/fileuploads/js/dropify.min.js',
				'assets/plugins/timepicker/bootstrap-timepicker.min.js',
				'assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
			),
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_data_type_offering()
	{

		$sfilepath = './uploads/config/.';
		$smaxsize = 4096000; //4MB
		$smaxwidth = 100;
		$smaxheight = 100;


		// PIC THUMB===========================================================
		if( $_FILES['type_offering_images'] )
		{
			$type_offering_images = $this->primaryclass->do_upload( 'type_offering_images' , $sfilepath, $smaxsize, $smaxwidth, $smaxheight );
			if( $type_offering_images['upload_data']['file_name'] == '' )
			{
				$type_offering_images_have_file = $this->input->post('old_type_offering_images');
			}else{
				$type_offering_images_have_file = $type_offering_images['upload_data']['file_name'];
			}
		}
		else
		{
			$type_offering_images_have_file = '';
		}//IF PIC THUMB=========================================================

		$data=array(
			'type_offering_title'=>$this->input->post('type_offering_title'),
			'type_offering_images'=>$type_offering_images_have_file,
			'type_offering_detail'=>$this->input->post('type_offering_detail'),
			'createdate'=>date("Y-m-d H:i:s"),
			'publish'=>$this->input->post('publish')
		);

		//var_dump($data);
		$insert = $this->Configsystem_model->set_type_offering($data);
		$this->session->set_flashdata('message', 'Your data inserted Successfully..');
		redirect('configsystem/view_type_offering');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit_type_offering($id)
	{
		$row = $this->Configsystem_model->get_type_offering_record($id);

		$data['row'] = $row;
		//var_dump($row);
		($this->input->post())?$this->Configsystem_model->edit($data):$this->load->view('backoffice/configsystem/formadmin-type-offering', array(
			'title'=>'แก้ไขรายการประเภทการเสนอขาย',
			'header'=>'yes',
			'url_href'=>'edit',
			'css'=>array(
				'assets/plugins/fileuploads/css/dropify.min.css',
				'assets/plugins/timepicker/bootstrap-timepicker.min.css',
				'assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
			),
			'js'=>array(
				'assets/plugins/moment/moment.js',
				'assets/plugins/fileuploads/js/dropify.min.js',
				'assets/plugins/timepicker/bootstrap-timepicker.min.js',
				'assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
			),
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit_data_type_offering()
	{
		$type_offering_id = $this->input->post('type_offering_id');
		$sfilepath = './uploads/config/.';
		$smaxsize = 4096000; //4MB
		$smaxwidth = 100;
		$smaxheight = 100;


		// PIC THUMB===========================================================
		if( $_FILES['type_offering_images'] )
		{
			$type_offering_images = $this->primaryclass->do_upload( 'type_offering_images' , $sfilepath, $smaxsize, $smaxwidth, $smaxheight );
			if( $type_offering_images['upload_data']['file_name'] == '' )
			{
				$type_offering_images_have_file = $this->input->post('old_type_offering_images');
			}else{
				$type_offering_images_have_file = $type_offering_images['upload_data']['file_name'];
			}
		}
		else
		{
			$type_offering_images_have_file = '';
		}//IF PIC THUMB=========================================================

		$data=array(
			'type_offering_id'=>$this->input->post('type_offering_id'),
			'type_offering_title'=>$this->input->post('type_offering_title'),
			'type_offering_images'=>$type_offering_images_have_file,
			'type_offering_detail'=>$this->input->post('type_offering_detail'),
			'updatedate'=>date("Y-m-d H:i:s"),
			'publish'=>$this->input->post('publish')
		);
		//echo $this->Primaryclass->pre_var_dump($data);

		$update = $this->Configsystem_model->update_type_offering($data);
		$this->session->set_flashdata('message', 'Your data Updated Successfully..');
		redirect('configsystem/view_type_offering');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function delete_type_offering($id)
	{
		$delete = $this->M->delete_record('type_offering_id', $id, 'type_offering');

		$this->session->set_flashdata('message', 'Your data Delete Successfully..');
		redirect('configsystem/view_type_offering');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	/* View Propety */
	public function view_type_property()
	{
		$data= $this->Configsystem_model->get_type_property();
		//
		$this->load->view('backoffice/configsystem/datatable-type-property',array(
			'title'=>'ตารางข้อมูลประเภททรัพย์',
			'header'=>'yes',
			'css'=>array(
				'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				'assets/plugins/datatables/jquery.dataTables.min.css',
				'assets/plugins/datatables/buttons.bootstrap.min.css',
				'assets/plugins/datatables/fixedHeader.bootstrap.min.css',
				'assets/plugins/datatables/responsive.bootstrap.min.css',
				'assets/plugins/datatables/scroller.bootstrap.min.css',
			),
			'js'=>array(
				'assets/plugins/tinymce/tinymce.min.js',
				'assets/js/dropzone.min.js',
				'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
				'assets/pages/jquery.sweet-alert.init.js',
				'assets/plugins/datatables/jquery.dataTables.min.js',
				'assets/plugins/datatables/dataTables.bootstrap.js',
				'assets/plugins/datatables/dataTables.buttons.min.js',
				'assets/plugins/datatables/buttons.bootstrap.min.js',
				'assets/plugins/datatables/jszip.min.js',
				'assets/plugins/datatables/pdfmake.min.js',
				'assets/plugins/datatables/vfs_fonts.js',
				'assets/plugins/datatables/buttons.html5.min.js',
				'assets/plugins/datatables/buttons.print.min.js',
				'assets/plugins/datatables/dataTables.fixedHeader.min.js',
				'assets/plugins/datatables/dataTables.keyTable.min.js',
				'assets/plugins/datatables/dataTables.responsive.min.js',
				'assets/plugins/datatables/responsive.bootstrap.min.js',
			),
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function create_type_property()
	{
		$id = 0;
		($this->input->post('action'))?$this->Configsystem_model->set_type_property($id):$this->load->view('backoffice/configsystem/formadmin-type-property', array(
			'title'=>'เพิ่มรายการประเภททรัพย์',
			'header'=>'yes',
			'css'=>array(
				'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
			),
			'js'=>array(
				'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
				'assets/pages/jquery.sweet-alert.init.js',
			),
		));

	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit_type_property($id = 0)
	{
		$data['row'] = $this->Configsystem_model->get_type_property_by_id($id);
		$id = ($this->input->post('action'))? $this->input->post('type_property_id') : 0;
		//var_dump($row);
		($this->input->post('action'))?$this->Configsystem_model->set_type_property($id):$this->load->view('backoffice/configsystem/formadmin-type-property', array(
			'title'=>'แก้ไขรายการประเภททรัพย์',
			'header'=>'yes',
			'css'=>array(
				'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
			),
			'js'=>array(
				'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
				'assets/pages/jquery.sweet-alert.init.js',
			),
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	/**
	 * ZONE
	 */
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public function view_type_zone()
	{
		$data= $this->Configsystem_model->get_type_zone();
		//
		$this->load->view('backoffice/configsystem/datatable-type-zone',array(
			'title'=>'ตารางข้อมูลโซน',
			'header'=>'yes',
			'url_href'=>'add_type_zone',
			'js'=>array(''),
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_type_zone()
	{
		$this->load->view('backoffice/configsystem/formadmin-type-zone',array(
			'title'=>'เพิ่มข้อมูลโซน',
			'header'=>'yes',
			'js'=>array(''),
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_data_type_zone()
	{
		$data=array(
			'type_zone_title'=>$this->input->post('type_zone_title'),
			'type_zone_title_en'=>$this->input->post('type_zone_title_en'),
			'createdate'=>date("Y-m-d H:i:s"),
			'publish'=>$this->input->post('publish')
		);

		//var_dump($data);
		$insert = $this->Configsystem_model->set_type_zone($data);
		$this->session->set_flashdata('message', 'Your data inserted Successfully..');
		redirect('configsystem/view_type_zone');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit_type_zone($id)
	{
		$row = $this->Configsystem_model->get_type_zone_record($id);

		$data['row'] = $row;
		//var_dump($row);
		($this->input->post())?$this->Configsystem_model->edit($data):$this->load->view('backoffice/configsystem/formadmin-type-zone', array(
			'title'=>'แก้ไขรายการโซน',
			'header'=>'yes',
			'url_href'=>'edit',
			'js'=>array(''),
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit_data_type_zone()
	{
		$type_zone_id = $this->input->post('type_zone_id');

		$data=array(
			'type_zone_id'=>$this->input->post('type_zone_id'),
			'type_zone_title'=>$this->input->post('type_zone_title'),
			'type_zone_title_en'=>$this->input->post('type_zone_title_en'),
			'updatedate'=>date("Y-m-d H:i:s"),
			'publish'=>$this->input->post('publish')
		);
		//echo $this->Primaryclass->pre_var_dump($data);

		$update = $this->Configsystem_model->update_type_zone($data);
		$this->session->set_flashdata('message', 'Your data Updated Successfully..');
		redirect('configsystem/view_type_zone');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * SUITES
	 */
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public function view_type_suites()
	{
		$data= $this->Configsystem_model->get_type_suites();
		//
		$this->load->view('backoffice/configsystem/datatable-type-suites',array(
			'title'=>'ตารางข้อมูลแบบห้องชุด',
			'header'=>'yes',
			'url_href'=>'add_type_suites',
			'js'=>array(''),
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_type_suites()
	{
		$this->load->view('backoffice/configsystem/formadmin-type-suites',array(
			'title'=>'เพิ่มข้อมูลแบบห้องชุด',
			'header'=>'yes',
			'js'=>array(''),
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_data_type_suites()
	{
		$data=array(
			'type_suites_title'=>$this->input->post('type_suites_title'),
			'type_suites_title_en'=>$this->input->post('type_suites_title_en'),
			'createdate'=>date("Y-m-d H:i:s"),
			'publish'=>$this->input->post('publish')
		);

		//var_dump($data);
		$insert = $this->Configsystem_model->set_type_suites($data);
		$this->session->set_flashdata('message', 'Your data inserted Successfully..');
		redirect('configsystem/view_type_suites');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit_type_suites($id)
	{
		$row = $this->Configsystem_model->get_type_suites_record($id);

		$data['row'] = $row;
		//var_dump($row);
		($this->input->post())?$this->Configsystem_model->edit($data):$this->load->view('backoffice/configsystem/formadmin-type-suites', array(
			'title'=>'แก้ไขรายการแบบห้องชุด',
			'header'=>'yes',
			'url_href'=>'edit',
			'js'=>array(''),
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit_data_type_suites()
	{
		$type_suites_id = $this->input->post('type_suites_id');

		$data=array(
			'type_suites_id'=>$this->input->post('type_suites_id'),
			'type_suites_title'=>$this->input->post('type_suites_title'),
			'type_suites_title_en'=>$this->input->post('type_suites_title_en'),
			'updatedate'=>date("Y-m-d H:i:s"),
			'publish'=>$this->input->post('publish')
		);
		//echo $this->Primaryclass->pre_var_dump($data);

		$update = $this->Configsystem_model->update_type_suites($data);
		$this->session->set_flashdata('message', 'Your data Updated Successfully..');
		redirect('configsystem/view_type_suites');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function delete_type_suites($id)
	{
		$delete = $this->M->delete_record('type_suites_id', $id, 'type_suites');

		$this->session->set_flashdata('message', 'Your data Delete Successfully..');
		redirect('configsystem/view_type_suites');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * DECORATION
	 */
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public function view_type_decoration()
	{
		$data= $this->Configsystem_model->get_type_decoration();
		//
		$this->load->view('backoffice/configsystem/datatable-type-decoration',array(
			'title'=>'ตารางข้อมูลการตกแต่ง',
			'header'=>'yes',
			'url_href'=>'add_type_decoration',
			'js'=>array(''),
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_type_decoration()
	{
		$this->load->view('backoffice/configsystem/formadmin-type-decoration',array(
			'title'=>'เพิ่มข้อมูลการตกแต่ง',
			'header'=>'yes',
			'js'=>array(''),
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_data_type_decoration()
	{
		$data=array(
			'type_decoration_title'=>$this->input->post('type_decoration_title'),
			'type_decoration_title_en'=>$this->input->post('type_decoration_title_en'),
			'createdate'=>date("Y-m-d H:i:s"),
			'publish'=>$this->input->post('publish')
		);

		//var_dump($data);
		$insert = $this->Configsystem_model->set_type_decoration($data);
		$this->session->set_flashdata('message', 'Your data inserted Successfully..');
		redirect('configsystem/view_type_decoration');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit_type_decoration($id)
	{
		$row = $this->Configsystem_model->get_type_decoration_record($id);

		$data['row'] = $row;
		//var_dump($row);
		($this->input->post())?$this->Configsystem_model->edit($data):$this->load->view('backoffice/configsystem/formadmin-type-decoration', array(
			'title'=>'แก้ไขรายการการตกแต่ง',
			'header'=>'yes',
			'url_href'=>'edit',
			'js'=>array(''),
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit_data_type_decoration()
	{
		$type_decoration_id = $this->input->post('type_decoration_id');

		$data=array(
			'type_decoration_id'=>$this->input->post('type_decoration_id'),
			'type_decoration_title'=>$this->input->post('type_decoration_title'),
			'type_decoration_title_en'=>$this->input->post('type_decoration_title_en'),
			'updatedate'=>date("Y-m-d H:i:s"),
			'publish'=>$this->input->post('publish')
		);
		//echo $this->Primaryclass->pre_var_dump($data);

		$update = $this->Configsystem_model->update_type_decoration($data);
		$this->session->set_flashdata('message', 'Your data Updated Successfully..');
		redirect('configsystem/view_type_decoration');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function delete_type_decoration($id)
	{
		$delete = $this->M->delete_record('type_decoration_id', $id, 'type_decoration');

		$this->session->set_flashdata('message', 'Your data Delete Successfully..');
		redirect('configsystem/view_type_decoration');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * STATUS
	 */
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public function view_type_status()
	{
		$data= $this->Configsystem_model->get_type_status();
		//
		$this->load->view('backoffice/configsystem/datatable-type-status',array(
			'title'=>'ตารางข้อมูลประเภทสถานะประกาศ',
			'header'=>'yes',
			'url_href'=>'add_type_status',
			'js'=>array(''),
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



	public function add_type_status()
	{
		$this->load->view('backoffice/configsystem/formadmin-type-status',array(
			'title'=>'เพิ่มข้อมูลประเภทสถานะประกาศ',
			'header'=>'yes',
			'js'=>array(''),
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_data_type_status()
	{
		$data=array(
			'type_status_title'=>$this->input->post('type_status_title'),
			'type_status_detail'=>$this->input->post('type_status_detail'),
			'createdate'=>date("Y-m-d H:i:s"),
			'publish'=>$this->input->post('publish')
		);

		//var_dump($data);
		$insert = $this->Configsystem_model->set_type_status($data);
		$this->session->set_flashdata('message', 'Your data inserted Successfully..');
		redirect('configsystem/view_type_status');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit_type_status($id)
	{
		$row = $this->Configsystem_model->get_type_status_record($id);

		$data['row'] = $row;
		//var_dump($row);
		($this->input->post())?$this->Configsystem_model->edit($data):$this->load->view('backoffice/configsystem/formadmin-type-status', array(
			'title'=>'แก้ไขรายการประเภทสถานะประกาศ',
			'header'=>'yes',
			'url_href'=>'edit',
			'js'=>array(''),
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit_data_type_status()
	{
		$type_status_id = $this->input->post('type_status_id');

		$data=array(
			'type_status_id'=>$this->input->post('type_status_id'),
			'type_status_title'=>$this->input->post('type_status_title'),
			'type_status_detail'=>$this->input->post('type_status_detail'),
			'updatedate'=>date("Y-m-d H:i:s"),
			'publish'=>$this->input->post('publish')
		);
		//echo $this->Primaryclass->pre_var_dump($data);

		$update = $this->Configsystem_model->update_type_status($data);
		$this->session->set_flashdata('message', 'Your data Updated Successfully..');
		redirect('configsystem/view_type_status');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function delete_type_status($id)
	{
		$delete = $this->M->delete_record('type_status_id', $id, 'type_status');
		redirect('configsystem/view_type_status');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
}
