<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Captcha extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // loading captcha helper
        $this->load->helper('captcha');
        $this->load->library('Primaryclass');
        $this->load->model('Captcha_model');
    }
    //-----------------------------------------------------------------------------------------------

    public function load_captcha()
    {
        $success_captcha = $this->Captcha_model->create_captcha();
        // $msg = 'Submit the word you see below:';

        // $msg .= '<input type="text" name="captcha" value="" />';
        // $this->primaryclass->pre_var_dump($success_captcha);
        $this->load->view('captcha', $success_captcha);
        // $this->load->view('captcha', $msg);
    }
    //-----------------------------------------------------------------------------------------------


    public function check_captcha($str)
    {
      $success = $this->Captcha_model->get_captcha();
    }
    //-----------------------------------------------------------------------------------------------
}
