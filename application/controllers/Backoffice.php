<?php
defined('BASEPATH') OR exit();
class Backoffice extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('cookie');
		$this->load->model('M'); 	/* LOADING MODEL * Staff_model as staff */
		$this->load->model('Staff_model'); 	/* LOADING MODEL * Staff_model as staff */
		$this->load->model('Backoffice_model'); 	/* LOADING MODEL * Backoffice_model as staff */
		$this->load->model('Condominium_model'); 	/* LOADING MODEL * Condominium_model as staff */
		$this->load->model('Land_model'); 	/* LOADING MODEL * Land_model as staff */

		$this->load->library('Primaryclass');
		//$this->Staff=$this->Staff_model->info();
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function index()
	{
		$R=$this->Staff_model->info();
		redirect( 'backoffice/login' );
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function logout(){
		$this->Staff_model->logout();
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function login(){

		($this->input->post())?$this->Staff_model->login():$this->load->view('backoffice/login',array(
			'title'=>'LOG IN BACKOFFICE~S',
			'header'=>'',
			'css'=>array(
				'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
			),
			'js'=>array(
				'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
				'assets/pages/jquery.sweet-alert.init.js',
			),
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function dashboard(){
		$R=$this->Staff_model->info();

    $hob_member = 2;
    $genaral_member = 1;
    $data['all_hob_member'] = $this->Backoffice_model->get_member_type($hob_member);
    $data['all_hob_member_today'] = $this->Backoffice_model->get_member_type_now($hob_member);

    $data['all_general_member'] = $this->Backoffice_model->get_member_type($genaral_member);
    $data['all_general_member_today'] = $this->Backoffice_model->get_member_type_now($genaral_member);

    $data['staff_most_condo'] = $this->Backoffice_model->get_staff_most_condo();
    $data['all_condo'] = $this->Condominium_model->condo_all_record_count();

    $data['staff_most_land'] = $this->Backoffice_model->get_staff_most_land();
    $data['all_land'] = $this->Land_model->land_all_record_count();

		$this->load->view('backoffice/dashboard',array(
			'title'=>'กราฟรายการสรุป',
			'header'=>'yes',
			'BodyClass'=>'fixed-left',
			'css'=>array(
				'assets/plugins/morris/morris.css'
			),
			'js'=>array(
				'assets/plugins/jquery-knob/jquery.knob.js',
				'assets/plugins/morris/morris.min.js',
				'assets/plugins/raphael/raphael-min.js',
				'assets/pages/jquery.dashboard.js',
			),
      'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function remove_image_update()
	{
		$data=$this->security->xss_clean($this->input->post());
		$data=array(
			'site_option_id'=>$this->input->post('site_option_id'),
			$this->input->post('field_name') => NULL,
		);
		$sfilepath = 'uploads/site_setting/';
		$imagefile = $sfilepath.$this->input->post('imagefile');
		$this->M->update_record_dalete_image($data, $imagefile, 'site_option');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function site_general()
	{
		$R=$this->Staff_model->info();
		if( $this->input->post('action' ))
		{
			//มี action หลังจากการ Submit
			$sfilepath = './uploads/site_setting/.';
			$smaxsize_site_logo = 2048000; //2MB
			$smaxwidth_site_logo = 220;
			$smaxheight_site_logo = 100;

			$smaxsize_site_favicon = 1024000; //2MB
			$smaxwidth_site_favicon = 16;
			$smaxheight_site_favicon = 16;
			// PIC THUMB===========================================================
			if( $_FILES['site_logo'] )
			{
				$site_logo = $this->primaryclass->do_upload( 'site_logo' , $sfilepath, $smaxsize_site_logo, $smaxwidth_site_logo, $smaxheight_site_logo );
				$this->primaryclass->pre_var_dump($site_logo);
				if( $site_logo['upload_data']['file_name'] == '' )
				{
					$site_logo_have_file = $this->input->post('old_site_logo');
				}else{
					$site_logo_have_file = $site_logo['upload_data']['file_name'];
				}
			}
			else
			{
				$site_logo_have_file = '';
			}//IF PIC THUMB=========================================================

			if( $_FILES['site_favicon'] )
			{
				$site_favicon = $this->primaryclass->do_upload( 'site_favicon' , $sfilepath, $smaxsize_site_favicon, $smaxwidth_site_favicon, $smaxheight_site_favicon );
				if( $site_favicon['upload_data']['file_name'] == '' )
				{
					$site_favicon_have_file = $this->input->post('old_site_favicon');
				}else{
					$site_favicon_have_file = $site_favicon['upload_data']['file_name'];
				}
			}
			else
			{
				$site_favicon_have_file = '';
			}//IF PIC THUMB=========================================================

			$data = $this->security->xss_clean($this->input->post());
			$data=array(
				'site_option_id'=>$this->input->post('site_option_id'),
				'site_logo'=>$site_logo_have_file,
				'site_favicon'=>$site_favicon_have_file,
			);

			$update = $this->M->edit( $this->input->post('site_option_id'), $data ,'site_option','site_option_id');

		}
		else
		{
			$id = 1;
			$data['row'] = $this->Backoffice_model->get_genaral_by_id($id);
			$this->load->view('backoffice/general/formadmin-general',array(
				'title'=>'การตั้งค่าระบบทั่วไป',
				'header'=>'yes',
				'BodyClass'=>'fixed-left',
				'css'=>array(
					'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
					'assets/css/dropzone.min.css',
				),
				'js'=>array(
					'assets/plugins/tinymce/tinymce.min.js',
					'assets/js/dropzone.min.js',
					'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
					'assets/pages/jquery.sweet-alert.init.js',
				),
				'data'=>$data,
			));
		}
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function site_email(){
		$R=$this->Staff_model->info();

		if( $this->input->post('action' ) )
		{
			$data = $this->security->xss_clean($this->input->post());

			$email_array = "";
			$i = 0;
			foreach($data['site_email'] as $email_key => $email_value)
			{
				if( !empty($email_value) )
				{
					$email_array .= ','.$email_value;
				}

			}

			$email_array = substr($email_array,1);
//			echo $this->primaryclass->pre_var_dump($email_array);

			$data=array(
				'site_option_id'=>$data['site_option_id'],
				'site_email'=>$email_array,
			);
			$update = $this->M->edit( $this->input->post('site_option_id'), $data ,'site_option','site_option_id');

		}
		else
		{
			$id = 1;
			$data['row'] = $this->Backoffice_model->get_email_by_id($id);
			$this->load->view('backoffice/general/formadmin-email',array(
				'title'=>'การตั้งค่าอีเมล์ระบบ',
				'header'=>'yes',
				'BodyClass'=>'fixed-left',
				'css'=>array(
					'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				),
				'js'=>array(
					'assets/plugins/tinymce/tinymce.min.js',
					'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
					'assets/pages/jquery.sweet-alert.init.js',
				),
				'data'=>$data,
			));
		}
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function site_seo(){
		$R=$this->Staff_model->info();
		if( $this->input->post('action') )
		{

			$data = $this->security->xss_clean($this->input->post());
			$data=array(
				'site_option_id'=>$data['site_option_id'],
				'site_seo_title'=>$data['site_seo_title'],
				'site_seo_meta'=>$data['site_seo_meta'],
				'site_seo_description'=>$data['site_seo_description'],
			);
			$update = $this->M->edit( $this->input->post('site_option_id'), $data ,'site_option','site_option_id');
		}
		else
		{
			$id = 1;
			$data['row'] = $this->Backoffice_model->get_seo_by_id($id);
			$this->load->view('backoffice/general/formadmin-seo',array(
				'title'=>'การตั้งค่า SEO',
				'header'=>'yes',
				'BodyClass'=>'fixed-left',
				'css'=>array(
					'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
					'assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
				),
				'js'=>array(
					'assets/plugins/tinymce/tinymce.min.js',
					'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
					'assets/pages/jquery.sweet-alert.init.js',
					'assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js',
					'assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js',
				),
				'data'=>$data,
			));

		}

	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function site_social(){
		$R=$this->Staff_model->info();

		if( $this->input->post('action' ) )
		{

			$data = $this->security->xss_clean($this->input->post());
			$data=array(
				'site_option_id'=>$data['site_option_id'],
				'site_facebook'=>$data['site_facebook'],
				'site_twitter'=>$data['site_twitter'],
				'site_youtube'=>$data['site_youtube'],
			);
			$update = $this->M->edit( $this->input->post('site_option_id'), $data ,'site_option','site_option_id');
		}
		else
		{
			$id = 1;
			$data['row'] = $this->Backoffice_model->get_social_by_id($id);
			$this->load->view('backoffice/general/formadmin-social',array(
				'title'=>'การตั้งค่าระบบโซเชี่ยล',
				'header'=>'yes',
				'BodyClass'=>'fixed-left',
				'css'=>array(
					'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				),
				'js'=>array(
					'assets/plugins/tinymce/tinymce.min.js',
					'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
					'assets/pages/jquery.sweet-alert.init.js',
				),
				'data'=>$data,
			));
		}
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function site_footer()
	{
		$R=$this->Staff_model->info();
		if( $this->input->post('action' ) )
		{

			$data = $this->security->xss_clean($this->input->post());
			$data=array(
				'site_option_id'=>$data['site_option_id'],
				'site_copyright'=>$data['site_copyright'],
				'site_footer'=>$data['site_footer'],
			);
			$update = $this->M->edit( $this->input->post('site_option_id'), $data ,'site_option','site_option_id');
		}
		else
		{
			$id = 1;
			$data['row'] = $this->Backoffice_model->get_footer_by_id($id);
			$this->load->view('backoffice/general/formadmin-footer',array(
				'title'=>'ตั้งค่า Footer',
				'googlemap'=>'yes',
				'header'=>'yes',
				'css'=>array(
					'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				),
				'js'=>array(
					'assets/plugins/tinymce/tinymce.min.js',
					'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
					'assets/pages/jquery.sweet-alert.init.js',
				),
				'wysiwigeditor'=>'wysiwigeditor',
				'data'=>$data,
			));
		}
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	public function site_video()
	{
		$R=$this->Staff_model->info();
		if( $this->input->post('action' ) )
		{
			$data = $this->security->xss_clean($this->input->post());
			$data=array(
				'site_option_id'=>$data['site_option_id'],
				'site_video_youtube'=>$data['site_video_youtube'],
				'site_video_text'=>$data['site_video_text'],
				'site_video_url'=>$data['site_video_url'],
			);
			$update = $this->M->edit( $this->input->post('site_option_id'), $data ,'site_option','site_option_id');
		}
		else
		{
			$id = 1;
			$data['row'] = $this->Backoffice_model->get_footer_by_id($id);
			$this->load->view('backoffice/general/formadmin-video',array(
				'title'=>'วีดีโอโครงการใหม่',
				'googlemap'=>'yes',
				'header'=>'yes',
				'wysiwigeditor'=>'wysiwigeditor',
				'data'=>$data,
			));
		}
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public function site_loan(){
		$R=$this->Staff_model->info();

		if( $this->input->post('action' ) )
		{
      $data = $this->security->xss_clean($this->input->post());
      $data=array(
        'site_option_id'=>$data['site_option_id'],
        'site_mrl '=>$data['site_mrl'],
        'site_mor'=>$data['site_mor'],
        'site_mrr'=>$data['site_mrr'],
        'site_cpr'=>$data['site_cpr'],
      );
      $update = $this->M->edit( $this->input->post('site_option_id'), $data ,'site_option','site_option_id');

		}
		else
		{
			$id = 1;
			$data['row'] = $this->Backoffice_model->get_loan_by_id($id);
			$this->load->view('backoffice/general/formadmin-loan',array(
				'title'=>'การตั้งค่าเงินกู้',
				'header'=>'yes',
				'BodyClass'=>'fixed-left',
				'css'=>array(
					'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				),
				'js'=>array(
					'assets/plugins/tinymce/tinymce.min.js',
					'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
					'assets/pages/jquery.sweet-alert.init.js',
				),
				'data'=>$data,
			));
		}
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  

}
