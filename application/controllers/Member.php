<?php
defined('BASEPATH') OR exit();
class Member extends CI_Controller {
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));								/***** LOADING HELPER TO AVOID PHP ERROR ****/
		$this->load->helper('cookie');
		$this->load->library('encryption');
		$this->load->library('Primaryclass');
		$this->load->library('dateclass');
		$this->load->library('session');
		$this->load->model('M'); 	/* LOADING MODEL * Member_model as user */
		$this->load->model('Member_model'); 	/* LOADING MODEL * Member_model as user */
		$this->load->model('Condominium_model'); 	/* LOADING MODEL * Member_model as user */
		$this->load->model('Configsystem_model');								/***** LOADING Controller * Configsystem_model ****/
		// Load library and url helper
		$this->load->library('facebook');
		$this->load->model('Staff_model');
		$R=$this->Staff_model->info();/* LOADING MODEL * Staff_model as staff */
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public function index(){
		$R=$this->Staff_model->info();
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

public function json_member()
{
    $list = $this->Member_model->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $member) {
      $no++;
        $id = $member->member_id;

        $member_mobileno = (!empty($member->member_mobileno))? $member->member_mobileno : '';

        $output_checked = '<input type="checkbox" name="member_id[]" class="member_id" value="'.$id.'"> '. $no;
        $output_id = $id;

        $output_fullname = $member->member_fname.' '.$member->member_lname;
        $output_email = $member->member_email;

        $output_mobileno = $member_mobileno;

        $output_type_id = $this->primaryclass->get_member_type($member->member_type_id);


        $output_approved = $this->primaryclass->get_approved($member->approved);



        $path_edit = site_url('member/edit_member/'.$id);
        $path_images = site_url('member/images/'.$id);
        $path_share = site_url('member/share/'.$id);
        $output_manage = '';

        $output_manage .= '<a href="'. $path_edit .'" class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit this Item"><i class="fa fa-pencil"></i></a>';

        if( $member->approved == 1){
        $output_manage .= '<a href="#" class="btn btn-approved btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="'. $id .'"><i class="zmdi zmdi-lock-open"></i></a>';
        } else {
        $output_manage .= '<a href="#" class="btn btn-unapproved btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id='. $id .'><i class="zmdi zmdi-lock-outline"></i></a>';
        }

        $output_manage .= '<a href="#" id="'. $id .'" class="btn btn-delete btn-icon waves-effect waves-light btn-danger m-b-5 m-r-5"><i class="ti-trash"></i></a>';



        $row = array();
        $row[] = $output_checked;
        $row[] = $output_id;
        $row[] = $output_fullname;
        $row[] = $output_email;
        $row[] = $output_mobileno;
        $row[] = $output_type_id;
        $row[] = $output_approved;
        $row[] = $output_manage;

        $data[] = $row;
    }

    $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Member_model->count_all(),
            "recordsFiltered" => $this->Member_model->count_filtered(),
            "data" => $data,
    );
    //output to json format
    echo json_encode($output);

    // $data = $this->Condominium_model->get_json_condo();
    // $this->output
    //     ->set_content_type('application/json')
    //     ->set_output(json_encode($output));

    // echo json_encode($data);
    // $this->primaryclass->pre_var_dump($data);
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function member(){

		$data = $this->Member_model->viewdata();
		$this->load->view('backoffice/member/datatable-member',array(
			'title'=>'ตารางข้อมูลสมาชิก',
			'header'=>'yes',
			'url_href'=>'add_member',
			'css'=>array(
				'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				'assets/plugins/datatables/jquery.dataTables.min.css',
				'assets/plugins/datatables/buttons.bootstrap.min.css',
				'assets/plugins/datatables/fixedHeader.bootstrap.min.css',
				'assets/plugins/datatables/responsive.bootstrap.min.css',
				'assets/plugins/datatables/scroller.bootstrap.min.css',
			),
			'js'=>array(
				'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
				'assets/pages/jquery.sweet-alert.init.js',
				'assets/plugins/datatables/jquery.dataTables.min.js',
				'assets/plugins/datatables/dataTables.bootstrap.js',
				'assets/plugins/datatables/dataTables.buttons.min.js',
				'assets/plugins/datatables/buttons.bootstrap.min.js',
				'assets/plugins/datatables/jszip.min.js',
				'assets/plugins/datatables/pdfmake.min.js',
				'assets/plugins/datatables/vfs_fonts.js',
				'assets/plugins/datatables/buttons.html5.min.js',
				'assets/plugins/datatables/buttons.print.min.js',
				'assets/plugins/datatables/dataTables.fixedHeader.min.js',
				'assets/plugins/datatables/dataTables.keyTable.min.js',
				'assets/plugins/datatables/dataTables.responsive.min.js',
				'assets/plugins/datatables/responsive.bootstrap.min.js',
			),
			'data'=>$data
		));
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_member(){
		$provinces = $this->M->get_provinces();

		$this->load->view('backoffice/member/formadmin',array(
			'title'=>'ตารางข้อมูลสมาชิก',
			'header'=>'yes',
			'formup'=>'add',
			'css'=>array(
				'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
			),
			'js'=>array(
				'assets/plugins/tinymce/tinymce.min.js',
				'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
				'assets/pages/jquery.sweet-alert.init.js',
			),
			'wysiwigeditor'=>'wysiwigeditor',
			'provinces'=>$provinces,
		));
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_data_member(){
		$sfilepath = './uploads/member/.';

		if( isset($_FILES['member_profile']) ){
			$member_profile = $this->primaryclass->do_upload('member_profile', $sfilepath);
			$member_profile_have = $member_profile['upload_data']['file_name'];
		}else{
			$member_profile_have = 'http://placehold.it/150x150/000000/999999';
		}

		//echo $this->primaryclass->pre_var_dump($member_profile);
		$data=array(
			'member_fname'=>$this->input->post('member_fname'),
			'member_lname'=>$this->input->post('member_lname'),
			'member_type_id'=>$this->input->post('member_type_id'),
			'member_email'=>$this->input->post('member_email'),
			'member_mobileno'=>$this->input->post('member_mobileno'),
			'member_profile'=>$member_profile_have,
			'member_password'=>md5($this->input->post('member_password')),
			'member_address'=>$this->input->post('member_address'),
			'provinces_id'=>$this->input->post('provinces_id'),
			'districts_id'=>$this->input->post('districts_id'),
			'sub_districts_id'=>$this->input->post('sub_districts_id'),
			'zipcode'=>$this->input->post('zipcode'),
			'ip'=>$this->input->ip_address(),
			'approved'=>$this->input->post('approved'),
			'createdate'=>date("Y-m-d H:i:s")
		);

		$insert = $this->Member_model->add($data);
		//$this->session->set_flashdata('message', 'Your data inserted Successfully..');
		//redirect('member/member');
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit_member($id)
	{
		$row = $this->Member_model->getdata($id);

		$provinces = $this->M->get_provinces();
//		$districts = $this->M->get_districts();
//		$subdistricts = $this->M->get_subdistricts();

		$data['row'] = $row;
		//var_dump($row);
		($this->input->post())?$this->Member_model->edit($data):$this->load->view('backoffice/member/formadmin', array(
			'title'=>'เพิ่มดูแลระบบ',
			'header'=>'yes',
			'url_href'=>'edit_member',
			'formup'=>'edit',
			'css'=>array(
				'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
			),
			'js'=>array(
				'assets/plugins/tinymce/tinymce.min.js',
				'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
				'assets/pages/jquery.sweet-alert.init.js',
			),
			'wysiwigeditor'=>'wysiwigeditor',
			'data'=>$data,
			'provinces'=>$provinces,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit_data_member()
	{
		$member_id = $this->input->post('member_id');
		/* Upload Image */
		$sfilepath = './uploads/member/.';
		$member_profile = $this->primaryclass->do_upload('member_profile', $sfilepath);

		if( $_FILES['member_profile'] )
		{
			if( $member_profile['upload_data']['file_name'] == '' )
			{
				$member_profile_have_file = $this->input->post('old_member_profile');
			}else{
				$member_profile_have_file = $member_profile['upload_data']['file_name'];
			}
		}
		else
		{
			$member_profile_have_file = '';
		}//IF PIC THUMB=========================================================

		$data=$this->security->xss_clean($this->input->post());
		if( $data['chk_pwd'] === '1' )//เปลี่ยนรหัสผ่าน
		{
			$data=array(
				'member_id'=>$data['member_id'],
				'member_fname'=>$data['member_fname'],
				'member_lname'=>$data['member_lname'],
				'member_type_id'=>$data['member_type_id'],
				'member_email'=>$data['member_email'],
				'member_mobileno'=>$data['member_mobileno'],
				'member_profile'=>$member_profile_have_file,
				'member_password'=>md5($data['member_password']),
				'member_address'=>$data['member_address'],
				'provinces_id'=>$data['provinces_id'],
				'districts_id'=>$data['districts_id'],
				'sub_districts_id'=>$data['sub_districts_id'],
				'zipcode'=>$data['zipcode'],
				'ip'=>$this->input->ip_address(),
				'approved'=>$data['approved'],
				'updatedate'=>date("Y-m-d H:i:s")
			);
		}
		else
		{
			$data=array(
				'member_id'=>$data['member_id'],
				'member_fname'=>$data['member_fname'],
				'member_lname'=>$data['member_lname'],
				'member_type_id'=>$data['member_type_id'],
				'member_email'=>$data['member_email'],
				'member_mobileno'=>$data['member_mobileno'],
				'member_profile'=>$member_profile_have_file,
				'member_address'=>$data['member_address'],
				'provinces_id'=>$data['provinces_id'],
				'districts_id'=>$data['districts_id'],
				'sub_districts_id'=>$data['sub_districts_id'],
				'zipcode'=>$data['zipcode'],
				'ip'=>$this->input->ip_address(),
				'approved'=>$data['approved'],
				'updatedate'=>date("Y-m-d H:i:s")
			);
		}

		//echo $this->primaryclass->pre_var_dump($data);

		$update = $this->Member_model->edit($data);
		//$this->session->set_flashdata('message', 'Your data Updated Successfully..');
		//redirect('member/member');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function approved_member($id)
	{
		$data=array(
			'member_id'=>$this->input->post('member_id'),
			'approved'=>2,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);
		$approved = $this->Member_model->edit($data);
		//echo $this->primaryclass->pre_var_dump($approved);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function unapproved_member($id)
	{
		$data=array(
			'member_id'=>$this->input->post('member_id'),
			'approved'=>1,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);
		$approved = $this->Member_model->edit($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function delete_member($id)
	{
		$delete = $this->M->delete_record('member_id', $id, 'member');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public function approved_multi_member()
  {
    $data = $this->security->xss_clean($this->input->post());
    // $this->primaryclass->pre_var_dump($data);
    if (isset($data['member_id'])) {
      //echo "yes";
      foreach ($data as $key => $value) {
        if( $key === 'member_id')
        {
          foreach ($value as $member_id)
          {
            $data_update=array(
              'member_id'=>$member_id,
              'approved'=>'2',
              'updatedate'=>date("Y-m-d H:i:s"),
        			'updateby'=>$_SESSION['zID'],
            );
            //$this->primaryclass->pre_var_dump($data_update);
            $approved = $this->Member_model->edit($data_update);
          }
        }
      }
    }
  }
  //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public function unapproved_multi_member()
  {
    $data = $this->security->xss_clean($this->input->post());
    // $this->primaryclass->pre_var_dump($data);
    if (isset($data['member_id'])) {
      //echo "yes";
      foreach ($data as $key => $value) {
        if( $key === 'member_id')
        {
          foreach ($value as $member_id)
          {
            $data_update=array(
              'member_id'=>$member_id,
              'approved'=>1,
              'updatedate'=>date("Y-m-d H:i:s"),
              'updateby'=>$_SESSION['zID'],
            );
            // $this->primaryclass->pre_var_dump($data_update);
            $approved = $this->Member_model->edit($data_update);
          }
        }
      }
    }
  }
  //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public function delete_multi_member()
  {
    $data = $this->security->xss_clean($this->input->post());
    // $this->primaryclass->pre_var_dump($data);
    if (isset($data['member_id'])) {
      //echo "yes";
      foreach ($data as $key => $value) {
        if( $key === 'member_id')
        {
          foreach ($value as $member_id)
          {
            $delete = $this->M->delete_record('member_id', $member_id, 'member');
          }
        }
      }
    }

  }
  //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

}
