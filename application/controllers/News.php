<?php
defined('BASEPATH') OR exit();
class News extends CI_Controller {
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));								/***** LOADING HELPER TO AVOID PHP ERROR ****/
		$this->load->library('primaryclass');
		$this->load->library('dateclass');
		$this->load->model('Staff_model');								/***** LOADING Controller * Staff_model as all ****/
		// $this->Staff=$this->Staff_model->info();
		$this->load->model('News_model');								/***** LOADING Controller * Primaryfunc as all ****/
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//FRONTEND
	public function index( $category_id = 0 )
	{
			$this->load->model('Page_model');
			$this->load->helper('text');
			$data_category = $this->News_model->get_news_category_publish();
			$data_banner = $this->News_model->get_news_highlight();
			$data_mostread = $this->News_model->get_news_mostread();

			if( $category_id !== 0 ){
				$url_page = base_url().'news-category/'.$category_id;
			}else{
				$url_page = base_url().'news';
			}
      // ========================================================================
      // 					PAGINATION
      // ========================================================================
      $data['news_total'] = $this->News_model->get_news_record($category_id);
      $per_page = '15';

      $page_pagination = $this->primaryclass->set_pagination($url_page, $data['news_total'], $per_page);
      $data['links'] = $this->pagination->create_links();

      // DATA #1
      $data['news'] = $this->News_model->get_newslist($page_pagination['per_page'], $page_pagination['page'], $category_id);
      // ========================================================================

			$breadcrumbs_arr = array(
				'1' => array(
					'title' => 'ข่าวสารล่าสุด',
					'href' => 'news',
				),
			);
			$breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

			$this->load->view('frontend/news', array(
					'title' => 'ข่าวสารล่าสุด',
					'css' => array('assets/css/slippry.min.css'),
					'js' => array('assets/js/slippry.min.js'),
					'data_category' => $data_category,
					'breadcrumbs' => $breadcrumbs,
					'data_banner' => $data_banner,
					'data_mostread' => $data_mostread,
					'data' => $data,
					'slippry' => 'slippry',
			));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//FRONTEND
	public function newsinside($slug)
	{
		$this->load->model('Page_model');
			$this->load->helper('text');

			$data = $this->News_model->get_news_by_slug(urldecode($slug));
			// $this->primaryclass->pre_var_dump($data);
			$news_id = (!empty($data->news_id)) ? $data->news_id : '0';

			$update_visited = $this->M->update_visited('news_id', $news_id, 'news');
			$data_mostread = $this->News_model->get_news_mostread();

			//HEADER SEO
			$news_items = $this->M->get_data_by_id('news_id, news_title, news_pic_thumb, news_caption', 'news_id', $news_id, 'news');

			$breadcrumbs_arr = array(
				'1' => array(
					'title' => 'ข่าวสารล่าสุด',
					'href' => 'news',
				),
				'2' => array(
					'title' => $data->news_title,
					'href' => '../news-details/'.$data->news_id,
				),
			);
			$breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

			$this->load->view('frontend/news-inside', array(
					'title' => 'รายละเอียดข่าวสาร',
					'data' => $data,
					'breadcrumbs' => $breadcrumbs,
					'news_items' => $news_items,
					'data_mostread' => $data_mostread,
					'isslider' => true,
					'sharepage' => true,
			));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



	public function news()//List news
	{
		$this->Staff=$this->Staff_model->info();
		$data= $this->News_model->get_news();
		$this->load->view('backoffice/news/datatable',array(
			'title'=>'ตารางข้อมูลข่าวสาร',
			'header'=>'yes',
			'url_href'=>'add_news',
			'css'=>array(
				'assets/plugins/RWD-Table-Patterns/dist/css/rwd-table.min.css',
				'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
			),
			'js'=>array(
				'assets/plugins/RWD-Table-Patterns/dist/js/rwd-table.min.js',
				'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
				'assets/pages/jquery.sweet-alert.init.js',
			),
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_news()
	{

		if($this->input->post('action'))
		{
			$sfilepath = './uploads/news/.';
			$smaxsize = 4096000; //4MB
			$smaxwidth_news_pic_thumb = 1200;
			$smaxheight_news_pic_thumb = 1200;

			$smaxwidth_news_pic_large = 1200;
			$smaxheight_news_pic_large = 1200;

			// PIC THUMB===========================================================
			if( $_FILES['news_pic_thumb'] )
			{
				$news_pic_thumb = $this->primaryclass->do_upload( 'news_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_news_pic_thumb, $smaxheight_news_pic_thumb );
				if( $news_pic_thumb['upload_data']['file_name'] == '' )
				{
					$news_pic_thumb_have_file = $this->input->post('old_news_pic_thumb');
				}else{
					$news_pic_thumb_have_file = $news_pic_thumb['upload_data']['file_name'];
				}
			}
			else
			{
				$news_pic_thumb_have_file = '';
			}//IF PIC THUMB=========================================================

			// PIC LARGE=============================================================
			if( $_FILES['news_pic_large'] )
			{
				$news_pic_large = $this->primaryclass->do_upload('news_pic_large', $sfilepath, $smaxsize, $smaxwidth_news_pic_large, $smaxheight_news_pic_large );
				if( $news_pic_large['upload_data']['file_name'] == '' )
				{
					$news_pic_large_have_file = $this->input->post('old_news_pic_large');
				}else{
					$news_pic_large_have_file = $news_pic_large['upload_data']['file_name'];
				}
			}
			else
			{
				$news_pic_large_have_file = '';
			}//IF PIC LARGE==========================================================

      $news_slug = url_title($this->input->post('news_title'), 'dash', TRUE);
			$data=array(
				'fk_news_category_id'=>$this->input->post('fk_news_category_id'),
				'news_title'=>$this->input->post('news_title'),
				'news_caption'=>$this->input->post('news_caption'),
				'news_description'=>$this->input->post('news_description'),
				'news_pic_thumb'=>$news_pic_thumb_have_file,
				'news_pic_large'=>$news_pic_large_have_file,
				'news_slug'=>$news_slug,
				// 'language_slug'=>$this->input->post('language_slug'),
				'meta_title'=>$this->input->post('meta_title'),
				'meta_keyword'=>$this->input->post('meta_keyword'),
				'meta_description'=>$this->input->post('meta_description'),
				'highlight'=>$this->input->post('highlight'),
				'recommended'=>$this->input->post('recommended'),
				'publish'=>$this->input->post('publish'),
				'createdate'=>date("Y-m-d H:i:s"),
				'createby'=>$_SESSION['zID'],
			);

			$insert = $this->News_model->insert_news($data);

		}
		else
		{
			$news_category 	= $this->News_model->get_news_category_option();

			$this->load->view('backoffice/news/formadmin',array(
				'title'=>'เพิ่มข้อมูลข่าวสาร',
				'header'=>'yes',
				'formup'=>'add',
				'css'=>array(
					'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				),
				'js'=>array(
					'assets/plugins/tinymce/tinymce.min.js',
					'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
					'assets/pages/jquery.sweet-alert.init.js',
				),
				'wysiwigeditor'=>'wysiwigeditor',
				'news_category'=>$news_category,
			));
		}

	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	public function edit_news($id=0)
	{
		if($this->input->post('action'))
		{
			/* Upload Image */
			$sfilepath = './uploads/news/.';
			$smaxsize = 4096000; //4MB
			$smaxwidth_news_pic_thumb = 1000;
			$smaxheight_news_pic_thumb = 1000;

			$smaxwidth_news_pic_large = 1200;
			$smaxheight_news_pic_large = 1000;

			// PIC THUMB===========================================================
			if( $_FILES['news_pic_thumb'] )
			{
	//			$news_pic_thumb = $this->primaryclass->do_upload( 'news_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_news_pic_thumb, $smaxheight_news_pic_thumb );
				$news_pic_thumb = $this->primaryclass->do_upload( 'news_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_news_pic_thumb, $smaxheight_news_pic_thumb );
				//$this->primaryclass->pre_var_dump($news_pic_thumb);

				if( $news_pic_thumb['upload_data']['file_name'] == '' )
				{
					$news_pic_thumb_have_file = $this->input->post('old_news_pic_thumb');
				}else{
					$news_pic_thumb_have_file = $news_pic_thumb['upload_data']['file_name'];

				}
			}
			else
			{
				$news_pic_thumb_have_file = '';
			}//IF PIC THUMB=========================================================

			// PIC LARGE=============================================================
			if( $_FILES['news_pic_large'] )
			{
				$news_pic_large = $this->primaryclass->do_upload('news_pic_large', $sfilepath, $smaxsize, $smaxwidth_news_pic_large, $smaxheight_news_pic_large );
				if( $news_pic_large['upload_data']['file_name'] == '' )
				{
					$news_pic_large_have_file = $this->input->post('old_news_pic_large');
				}else{
					$news_pic_large_have_file = $news_pic_large['upload_data']['file_name'];
				}
			}
			else
			{
				$news_pic_large_have_file = '';
			}//IF PIC LARGE==========================================================

			$data=$this->security->xss_clean($this->input->post());
			$news_slug = url_title($data['news_title'], 'dash', TRUE);
			$data=array(
				'news_id'=>$data['news_id'],
				'fk_news_category_id'=>$data['fk_news_category_id'],
				'news_title'=>$data['news_title'],
				'news_caption'=>$data['news_caption'],
				'news_description'=>$data['news_description'],
				'news_pic_thumb'=>$news_pic_thumb_have_file,
				'news_pic_large'=>$news_pic_large_have_file,
				'news_slug'=>$news_slug,
				// 'language_slug'=>$data['language_slug'],
				'meta_title'=>$data['meta_title'],
				'meta_keyword'=>$data['meta_keyword'],
				'meta_description'=>$data['meta_description'],
				'highlight'=>$data['highlight'],
				'recommended'=>$data['recommended'],
				'publish'=>$data['publish'],
				'updatedate'=>date("Y-m-d H:i:s"),
				'createby'=>$_SESSION['zID'],
			);
			$update = $this->News_model->update_news($data);
		}
		else
		{
			$row = $this->News_model->get_info($id);
			$news_category 	= $this->News_model->get_news_category_option();
			$data['row'] = $row;
			$this->load->view('backoffice/news/formadmin', array(
				'title'=>'แก้ไขข้อมูลข่าวสาร',
				'header'=>'yes',
				'url_href'=>'edit',
				'formup'=>'edit',
				'css'=>array(
					'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				),
				'js'=>array(
					'assets/plugins/tinymce/tinymce.min.js',
					'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
					'assets/pages/jquery.sweet-alert.init.js',
				),
				'wysiwigeditor'=>'wysiwigeditor',
				'data'=>$data,
				'news_category'=>$news_category,
			));
		}
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	public function display_news($id)
	{
		$data=array(
			'news_id'=>$this->input->post('news_id'),
			'publish'=>2,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);

		//echo $this->primaryclass->pre_var_dump($_POST['id']);
		$publish = $this->News_model->update_news($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function undisplay_news($id)
	{
		$data=array(
			'news_id'=>$this->input->post('news_id'),
			'publish'=>1,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);
		//echo $_POST['news_id'];
		//echo $this->primaryclass->pre_var_dump($data);
		$publish = $this->News_model->update_news($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public function update_slug()
	{
		$publish = $this->News_model->update_slug();
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function delete_news($id)
	{
		$delete = $this->News_model->delete_record('news_id', $id, 'news');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	/* news Category */
	public function category()
	{
		$this->Staff=$this->Staff_model->info();
		$data= $this->News_model->get_news_category();

		$this->load->view('backoffice/news/datatable-category',array(
			'title'=>'ตารางข้อมูลหมวดหมู่ข่าวสาร',
			'header'=>'yes',
			'url_href'=>'add_news_category',
			'css'=>array(
				'assets/plugins/RWD-Table-Patterns/dist/css/rwd-table.min.css',
				'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
			),
			'js'=>array(
				'assets/plugins/RWD-Table-Patterns/dist/js/rwd-table.min.js',
				'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
				'assets/pages/jquery.sweet-alert.init.js',
			),
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_news_category()
	{
		if($this->input->post('action'))
		{
			$sfilepath = './uploads/news/.';
			$smaxsize = 4096000; //4MB
			$smaxwidth_news_category_pic_thumb = 1200;
			$smaxheight_news_category_pic_thumb = 1200;

			$smaxwidth_news_category_pic_large = 1200;
			$smaxheight_news_category_pic_large = 1200;

			// PIC THUMB===========================================================
			if( $_FILES['news_category_pic_thumb'] )
			{
				$news_category_pic_thumb = $this->primaryclass->do_upload( 'news_category_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_news_category_pic_thumb, $smaxheight_news_category_pic_thumb );
				if( $news_category_pic_thumb['upload_data']['file_name'] == '' )
				{
					$news_category_pic_thumb_have_file = $this->input->post('old_news_category_pic_thumb');
				}else{
					$news_category_pic_thumb_have_file = $news_category_pic_thumb['upload_data']['file_name'];
				}
			}
			else
			{
				$news_category_pic_thumb_have_file = '';
			}//IF PIC THUMB=========================================================

			// PIC LARGE=============================================================
			if( $_FILES['news_category_pic_large'] )
			{
				$news_category_pic_large = $this->primaryclass->do_upload('news_category_pic_large', $sfilepath, $smaxsize, $smaxwidth_news_category_pic_large, $smaxheight_news_category_pic_large );
				if( $news_category_pic_large['upload_data']['file_name'] == '' )
				{
					$news_category_pic_large_have_file = $this->input->post('old_news_category_pic_large');
				}else{
					$news_category_pic_large_have_file = $news_category_pic_large['upload_data']['file_name'];
				}
			}
			else
			{
				$news_category_pic_large_have_file = '';
			}//IF PIC LARGE==========================================================

			$data=$this->security->xss_clean($this->input->post());
			$data=array(
				'news_category_name'=>$data['news_category_name'],
				'news_category_expert'=>$data['news_category_expert'],
				'news_category_description'=>$data['news_category_description'],
				'news_category_pic_thumb'=>$news_category_pic_thumb_have_file,
				'news_category_pic_large'=>$news_category_pic_large_have_file,
				'meta_title'=>$data['meta_title'],
				'meta_keyword'=>$data['meta_keyword'],
				'meta_description'=>$data['meta_description'],
				'publish'=>$data['publish'],
				'createdate'=>date("Y-m-d H:i:s"),
				'createby'=>$_SESSION['zID'],
			);
			$insert = $this->News_model->insert_news_category($data);

		}
		else
		{
			$this->load->view('backoffice/news/formadmin-category',array(
				'title'=>'เพิ่มข้อมูลหมวดหมู่ข่าวสาร',
				'header'=>'yes',
				'formup'=>'add',
				'css'=>array(
					'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				),
				'js'=>array(
					'assets/plugins/tinymce/tinymce.min.js',
					'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
					'assets/pages/jquery.sweet-alert.init.js',
				),
				'wysiwigeditor'=>'wysiwigeditor',
			));
		}

	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	public function edit_news_category($id=0)
	{
		if($this->input->post('action'))
		{
			/* Upload Image */
			$sfilepath = './uploads/news/.';
			$smaxsize = 4096000; //4MB
			$smaxwidth_news_category_pic_thumb = 1000;
			$smaxheight_news_category_pic_thumb = 1000;

			$smaxwidth_news_category_pic_large = 1200;
			$smaxheight_news_category_pic_large = 1000;

			// PIC THUMB===========================================================
			if( $_FILES['news_category_pic_thumb'] )
			{
	//			$news_category_pic_thumb = $this->primaryclass->do_upload( 'news_category_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_news_category_pic_thumb, $smaxheight_news_category_pic_thumb );
				$news_category_pic_thumb = $this->primaryclass->do_upload( 'news_category_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_news_category_pic_thumb, $smaxheight_news_category_pic_thumb );
				//$this->primaryclass->pre_var_dump($news_category_pic_thumb);

				if( $news_category_pic_thumb['upload_data']['file_name'] == '' )
				{
					$news_category_pic_thumb_have_file = $this->input->post('old_news_category_pic_thumb');
				}else{
					$news_category_pic_thumb_have_file = $news_category_pic_thumb['upload_data']['file_name'];

				}
			}
			else
			{
				$news_category_pic_thumb_have_file = '';
			}//IF PIC THUMB=========================================================

			// PIC LARGE=============================================================
			if( $_FILES['news_category_pic_large'] )
			{
				$news_category_pic_large = $this->primaryclass->do_upload('news_category_pic_large', $sfilepath, $smaxsize, $smaxwidth_news_category_pic_large, $smaxheight_news_category_pic_large );
				if( $news_category_pic_large['upload_data']['file_name'] == '' )
				{
					$news_category_pic_large_have_file = $this->input->post('old_news_category_pic_large');
				}else{
					$news_category_pic_large_have_file = $news_category_pic_large['upload_data']['file_name'];
				}
			}
			else
			{
				$news_category_pic_large_have_file = '';
			}//IF PIC LARGE==========================================================


			$data=array(
				'news_category_id'=>$this->input->post('news_category_id'),
				'news_category_name'=>$this->input->post('news_category_name'),
				'news_category_expert'=>$this->input->post('news_category_expert'),
				'news_category_description'=>$this->input->post('news_category_description'),
				'news_category_pic_thumb'=>$news_category_pic_thumb_have_file,
				'news_category_pic_large'=>$news_category_pic_large_have_file,
				'meta_title'=>$this->input->post('meta_title'),
				'meta_keyword'=>$this->input->post('meta_keyword'),
				'meta_description'=>$this->input->post('meta_description'),
				'publish'=>$this->input->post('publish'),
				'updatedate'=>date("Y-m-d H:i:s"),
				'updateby'=>$_SESSION['zID'],
			);
			//echo $this->Primaryclass->pre_var_dump($data);
			$update = $this->News_model->update_news_category($data);
		}
		else
		{
			$row = $this->News_model->get_news_category_record($id);
			$data['row'] = $row;
	//		var_dump($row);

			//($this->input->post())?$this->News_model->edit_data_news_category($data):$this->load->view('backoffice/news/formadmin-category', array(
			$this->load->view('backoffice/news/formadmin-category', array(
				'title'=>'แก้ไขข้อมูลหมวดหมู่ข่าวสาร',
				'header'=>'yes',
				'url_href'=>'edit',
				'formup'=>'edit',
				'css'=>array(
					'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				),
				'js'=>array(
					'assets/plugins/tinymce/tinymce.min.js',
					'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
					'assets/pages/jquery.sweet-alert.init.js',
				),
				'wysiwigeditor'=>'wysiwigeditor',
				'data'=>$data,
			));
		}
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	public function display_news_category($id)
	{
		$data=array(
			'news_category_id'=>$this->input->post('news_category_id'),
			'publish'=>2,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);

		//echo $this->primaryclass->pre_var_dump($_POST['id']);
		$publish = $this->News_model->update_news_category($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function undisplay_news_category($id)
	{
		$data=array(
			'news_category_id'=>$this->input->post('news_category_id'),
			'publish'=>1,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);
		//echo $_POST['news_category_id'];
		//echo $this->primaryclass->pre_var_dump($data);
		$publish = $this->News_model->update_news_category($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function delete_news_category($id)
	{
		$delete = $this->News_model->delete_record('news_category_id', $id, 'news_category');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\

	public function set_news_order()
	{
    $data=$this->security->xss_clean($this->input->post());

    if( count($data['count_order']) > 0 )
    {
      for( $i=0; $i<=count($data['count_order']); $i++)
      {
        $orders = $this->M->set_order('news', $data['id'], 'news_id', 'news_order', $data['news_order']);
      }
    }
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\

}
