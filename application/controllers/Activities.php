<?php
defined('BASEPATH') OR exit();
class activities extends CI_Controller {
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));								/***** LOADING HELPER TO AVOID PHP ERROR ****/
		$this->load->library('primaryclass');
		$this->load->library('dateclass');
		$this->load->model('Staff_model');								/***** LOADING Controller * Staff_model as all ****/

		$this->load->model('Activities_model');								/***** LOADING Controller * Primaryfunc as all ****/
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function index()
	{
		// $R=$this->Staff_model->info();
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function home( $category_id = 0 )
	{
			$this->load->helper('text');
			$this->load->model('Page_model');

			// $data_category = $this->Activities_model->get_activities_publish();
			$data_banner = $this->Activities_model->get_activities_highlight();
			$data_mostread = $this->Activities_model->get_activities_mostread();

			if( $category_id !== 0 ){
				$url_page = base_url().'activities-category/'.$category_id;
			}else{
				$url_page = base_url().'activities';
			}
      // ========================================================================
      // 					PAGINATION
      // ========================================================================
      $data['activities_total'] = $this->Activities_model->get_activities_record($category_id);
      $per_page = '15';

      $page_pagination = $this->primaryclass->set_pagination($url_page, $data['activities_total'], $per_page);
      $data['links'] = $this->pagination->create_links();

      // DATA #1
      $data['activities'] = $this->Activities_model->get_activitieslist($page_pagination['per_page'], $page_pagination['page'], $category_id);
      // ========================================================================

			$breadcrumbs_arr = array(
				'1' => array(
					'title' => 'กิจกรรม',
					'href' => 'activities',
				),
			);


			$breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

			$this->load->view('frontend/activities', array(
					'title' => 'กิจกรรม',
					// 'data_category' => $data_category,
					'breadcrumbs' => $breadcrumbs,
					'data_banner' => $data_banner,
					'data_mostread' => $data_mostread,
					'data' => $data,
					'slippry' => 'slippry',
					'css' => array('assets/css/slippry.min.css'),
					'js' => array('assets/js/slippry.min.js'),
			));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function activitiesinside($id)
	{
		$this->load->model('Page_model');
			$this->load->helper('text');
			$data = $this->Activities_model->get_info($id);
			$data_mostread = $this->Activities_model->get_activities_mostread();
			$update_visited = $this->M->update_visited('activities_id', $id, 'activities');

			$breadcrumbs_arr = array(
				'1' => array(
					'title' => 'กิจกรรม',
					'href' => 'activities',
				),
				'2' => array(
					'title' => $data->activities_title,
					'href' => '../activities-details/'.$id,
				),
			);
			$breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

			$this->load->view('frontend/activities-inside', array(
					'title' => 'รายละเอียดกิจกรรม',
					'data' => $data,
					'breadcrumbs' => $breadcrumbs,
					'data_mostread' => $data_mostread,
					'isslider' => true,
					'sharepage' => true,
			));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	public function activities()//List activities
	{
		$this->Staff=$this->Staff_model->info();
		$data= $this->Activities_model->get_activities();
		$this->load->view('backoffice/activities/datatable',array(
			'title'=>'ตารางข้อมูลกิจกรรม',
			'header'=>'yes',
			'url_href'=>'add_activities',
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_activities()
	{
		$this->Staff=$this->Staff_model->info();
		if($this->input->post('action'))
		{
			$sfilepath = './uploads/activities/.';
			$smaxsize = 4096000; //4MB
			$smaxwidth_activities_pic_thumb = 1200;
			$smaxheight_activities_pic_thumb = 1200;

			$smaxwidth_activities_pic_large = 1200;
			$smaxheight_activities_pic_large = 1200;

			// PIC THUMB===========================================================
			if( $_FILES['activities_pic_thumb'] )
			{
				$activities_pic_thumb = $this->primaryclass->do_upload( 'activities_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_activities_pic_thumb, $smaxheight_activities_pic_thumb );
				if( $activities_pic_thumb['upload_data']['file_name'] == '' )
				{
					$activities_pic_thumb_have_file = $this->input->post('old_activities_pic_thumb');
				}else{
					$activities_pic_thumb_have_file = $activities_pic_thumb['upload_data']['file_name'];
				}
			}
			else
			{
				$activities_pic_thumb_have_file = '';
			}//IF PIC THUMB=========================================================

			// PIC LARGE=============================================================
			if( $_FILES['activities_pic_large'] )
			{
				$activities_pic_large = $this->primaryclass->do_upload('activities_pic_large', $sfilepath, $smaxsize, $smaxwidth_activities_pic_large, $smaxheight_activities_pic_large );
				if( $activities_pic_large['upload_data']['file_name'] == '' )
				{
					$activities_pic_large_have_file = $this->input->post('old_activities_pic_large');
				}else{
					$activities_pic_large_have_file = $activities_pic_large['upload_data']['file_name'];
				}
			}
			else
			{
				$activities_pic_large_have_file = '';
			}//IF PIC LARGE==========================================================

			$activitiesdate = $this->primaryclass->set_explode_date_time( $this->input->post('activities_date') , $this->input->post('activities_time'));
			$createdate = $this->primaryclass->set_explode_date_time( $this->input->post('createdate_date') , $this->input->post('createdate_time'));
			$updatedate = $this->primaryclass->set_explode_date_time( $this->input->post('updatedate_date') , $this->input->post('updatedate_time'));

			$activities_slug = url_title($this->input->post('activities_slug'), 'dash', TRUE);
			$data=array(
				'fk_activities_category_id'=>$this->input->post('fk_activities_category_id'),
				'activities_title'=>$this->input->post('activities_title'),
				'activities_date'=>$activitiesdate,
				'activities_days'=>$this->input->post('activities_days'),
				'activities_caption'=>$this->input->post('activities_caption'),
				'activities_description'=>$this->input->post('activities_description'),
				'activities_pic_thumb'=>$activities_pic_thumb_have_file,
				'activities_pic_large'=>$activities_pic_large_have_file,
				'activities_slug'=>$activities_slug,
				// 'language_slug'=>$this->input->post('language_slug'),
				'meta_title'=>$this->input->post('meta_title'),
				'meta_keyword'=>$this->input->post('meta_keyword'),
				'meta_description'=>$this->input->post('meta_description'),
				'highlight'=>$this->input->post('highlight'),
				'recommended'=>$this->input->post('recommended'),
				'publish'=>$this->input->post('publish'),
				'createdate'=>$createdate,
				'updatedate'=>$updatedate,
				'createby'=>$_SESSION['zID'],
			);

			$insert = $this->Activities_model->insert_activities($data);

		}
		else
		{
			//$activities_category 	= $this->Activities_model->get_activities_category_option();

			$this->load->view('backoffice/activities/formadmin',array(
				'title'=>'เพิ่มข้อมูลกิจกรรม',
				'header'=>'yes',
				'formup'=>'add',
				'wysiwigeditor'=>'wysiwigeditor',
				'css'=>array(
					'assets/plugins/timepicker/bootstrap-timepicker.min.css',
					'assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
				),
				'js'=>array(
					'assets/plugins/moment/moment.js',
					'assets/plugins/timepicker/bootstrap-timepicker.min.js',
					'assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
				),
				//'activities_category'=>$activities_category,
			));
		}

	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	public function edit_activities($id=0)
	{
		$this->Staff=$this->Staff_model->info();
		if($this->input->post('action'))
		{
			/* Upload Image */
			$sfilepath = './uploads/activities/.';
			$smaxsize = 4096000; //4MB
			$smaxwidth_activities_pic_thumb = 1000;
			$smaxheight_activities_pic_thumb = 1000;

			$smaxwidth_activities_pic_large = 1200;
			$smaxheight_activities_pic_large = 1000;

			// PIC THUMB===========================================================
			if( $_FILES['activities_pic_thumb'] )
			{
	//			$activities_pic_thumb = $this->primaryclass->do_upload( 'activities_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_activities_pic_thumb, $smaxheight_activities_pic_thumb );
				$activities_pic_thumb = $this->primaryclass->do_upload( 'activities_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_activities_pic_thumb, $smaxheight_activities_pic_thumb );
				//$this->primaryclass->pre_var_dump($activities_pic_thumb);

				if( $activities_pic_thumb['upload_data']['file_name'] == '' )
				{
					$activities_pic_thumb_have_file = $this->input->post('old_activities_pic_thumb');
				}else{
					$activities_pic_thumb_have_file = $activities_pic_thumb['upload_data']['file_name'];

				}
			}
			else
			{
				$activities_pic_thumb_have_file = '';
			}//IF PIC THUMB=========================================================

			// PIC LARGE=============================================================
			if( $_FILES['activities_pic_large'] )
			{
				$activities_pic_large = $this->primaryclass->do_upload('activities_pic_large', $sfilepath, $smaxsize, $smaxwidth_activities_pic_large, $smaxheight_activities_pic_large );
				if( $activities_pic_large['upload_data']['file_name'] == '' )
				{
					$activities_pic_large_have_file = $this->input->post('old_activities_pic_large');
				}else{
					$activities_pic_large_have_file = $activities_pic_large['upload_data']['file_name'];
				}
			}
			else
			{
				$activities_pic_large_have_file = '';
			}//IF PIC LARGE==========================================================

			$activitiesdate = $this->primaryclass->set_explode_date_time( $this->input->post('activities_date') , $this->input->post('activities_time'));
			$createdate = $this->primaryclass->set_explode_date_time( $this->input->post('createdate_date') , $this->input->post('createdate_time'));
			$updatedate = $this->primaryclass->set_explode_date_time( $this->input->post('updatedate_date') , $this->input->post('updatedate_time'));

			$data=$this->security->xss_clean($this->input->post());
			$activities_slug = url_title($data['activities_slug'], 'dash', TRUE);
			$data=array(
				'activities_id'=>$data['activities_id'],
				'fk_activities_category_id'=>$data['fk_activities_category_id'],
				'activities_title'=>$data['activities_title'],
				'activities_date'=>$activitiesdate,
				'activities_days'=>$data['activities_days'],
				'activities_caption'=>$data['activities_caption'],
				'activities_description'=>$data['activities_description'],
				'activities_pic_thumb'=>$activities_pic_thumb_have_file,
				'activities_pic_large'=>$activities_pic_large_have_file,
				'activities_slug'=>$activities_slug,
				// 'language_slug'=>$data['language_slug'],
				'meta_title'=>$data['meta_title'],
				'meta_keyword'=>$data['meta_keyword'],
				'meta_description'=>$data['meta_description'],
				'highlight'=>$data['highlight'],
				'recommended'=>$data['recommended'],
				'publish'=>$data['publish'],
				'createdate'=>$createdate,
				'updatedate'=>$updatedate,
				'createby'=>$_SESSION['zID'],
			);
			$update = $this->Activities_model->update_activities($data);
		}
		else
		{
			$row = $this->Activities_model->get_info($id);
			// $activities_category 	= $this->Activities_model->get_activities_category_option();
			$data['row'] = $row;
			$this->load->view('backoffice/activities/formadmin', array(
				'title'=>'แก้ไขข้อมูลกิจกรรม',
				'header'=>'yes',
				'url_href'=>'edit',
				'formup'=>'edit',
				'css'=>array(
					'assets/plugins/timepicker/bootstrap-timepicker.min.css',
					'assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
				),
				'js'=>array(
					'assets/plugins/moment/moment.js',
					'assets/plugins/timepicker/bootstrap-timepicker.min.js',
					'assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
				),
				'wysiwigeditor'=>'wysiwigeditor',
				'data'=>$data,
				// 'activities_category'=>$activities_category,
			));
		}
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	public function display_activities($id)
	{
		$this->Staff=$this->Staff_model->info();
		$data=array(
			'activities_id'=>$this->input->post('activities_id'),
			'publish'=>2,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);

		//echo $this->primaryclass->pre_var_dump($_POST['id']);
		$publish = $this->Activities_model->update_activities($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function undisplay_activities($id)
	{
		$this->Staff=$this->Staff_model->info();
		$data=array(
			'activities_id'=>$this->input->post('activities_id'),
			'publish'=>1,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);
		//echo $_POST['activities_id'];
		//echo $this->primaryclass->pre_var_dump($data);
		$publish = $this->Activities_model->update_activities($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function delete_activities($id)
	{
		$delete = $this->Activities_model->delete_record('activities_id', $id, 'activities');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	/* activities Category */
	public function category()
	{
		//var_dump( $this->Staff );
		$data= $this->Activities_model->get_activities_category();

		$this->load->view('backoffice/activities/datatable-category',array(
			'title'=>'ตารางข้อมูลหมวดหมู่กิจกรรม',
			'header'=>'yes',
			'url_href'=>'add_activities_category',
			'css'=>array(
				'assets/plugins/RWD-Table-Patterns/dist/css/rwd-table.min.css',
				'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
			),
			'js'=>array(
				'assets/plugins/RWD-Table-Patterns/dist/js/rwd-table.min.js',
				'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
				'assets/pages/jquery.sweet-alert.init.js',
			),
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_activities_category()
	{
		if($this->input->post('action'))
		{
			$sfilepath = './uploads/activities/.';
			$smaxsize = 4096000; //4MB
			$smaxwidth_activities_category_pic_thumb = 1200;
			$smaxheight_activities_category_pic_thumb = 1200;

			$smaxwidth_activities_category_pic_large = 1200;
			$smaxheight_activities_category_pic_large = 1200;

			// PIC THUMB===========================================================
			if( $_FILES['activities_category_pic_thumb'] )
			{
				$activities_category_pic_thumb = $this->primaryclass->do_upload( 'activities_category_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_activities_category_pic_thumb, $smaxheight_activities_category_pic_thumb );
				if( $activities_category_pic_thumb['upload_data']['file_name'] == '' )
				{
					$activities_category_pic_thumb_have_file = $this->input->post('old_activities_category_pic_thumb');
				}else{
					$activities_category_pic_thumb_have_file = $activities_category_pic_thumb['upload_data']['file_name'];
				}
			}
			else
			{
				$activities_category_pic_thumb_have_file = '';
			}//IF PIC THUMB=========================================================

			// PIC LARGE=============================================================
			if( $_FILES['activities_category_pic_large'] )
			{
				$activities_category_pic_large = $this->primaryclass->do_upload('activities_category_pic_large', $sfilepath, $smaxsize, $smaxwidth_activities_category_pic_large, $smaxheight_activities_category_pic_large );
				if( $activities_category_pic_large['upload_data']['file_name'] == '' )
				{
					$activities_category_pic_large_have_file = $this->input->post('old_activities_category_pic_large');
				}else{
					$activities_category_pic_large_have_file = $activities_category_pic_large['upload_data']['file_name'];
				}
			}
			else
			{
				$activities_category_pic_large_have_file = '';
			}//IF PIC LARGE==========================================================

			$data=$this->security->xss_clean($this->input->post());
			$data=array(
				'activities_category_name'=>$data['activities_category_name'],
				'activities_category_expert'=>$data['activities_category_expert'],
				'activities_category_description'=>$data['activities_category_description'],
				'activities_category_pic_thumb'=>$activities_category_pic_thumb_have_file,
				'activities_category_pic_large'=>$activities_category_pic_large_have_file,
				'meta_title'=>$data['meta_title'],
				'meta_keyword'=>$data['meta_keyword'],
				'meta_description'=>$data['meta_description'],
				'publish'=>$data['publish'],
				'createdate'=>date("Y-m-d H:i:s"),
				'createby'=>$_SESSION['zID'],
			);
			$insert = $this->Activities_model->insert_activities_category($data);

		}
		else
		{
			$this->load->view('backoffice/activities/formadmin-category',array(
				'title'=>'เพิ่มข้อมูลหมวดหมู่กิจกรรม',
				'header'=>'yes',
				'formup'=>'add',
				'css'=>array(
					'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				),
				'js'=>array(
					'assets/plugins/tinymce/tinymce.min.js',
					'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
					'assets/pages/jquery.sweet-alert.init.js',
				),
				'wysiwigeditor'=>'wysiwigeditor',
			));
		}

	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	public function edit_activities_category($id=0)
	{
		if($this->input->post('action'))
		{
			/* Upload Image */
			$sfilepath = './uploads/activities/.';
			$smaxsize = 4096000; //4MB
			$smaxwidth_activities_category_pic_thumb = 1000;
			$smaxheight_activities_category_pic_thumb = 1000;

			$smaxwidth_activities_category_pic_large = 1200;
			$smaxheight_activities_category_pic_large = 1000;

			// PIC THUMB===========================================================
			if( $_FILES['activities_category_pic_thumb'] )
			{
	//			$activities_category_pic_thumb = $this->primaryclass->do_upload( 'activities_category_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_activities_category_pic_thumb, $smaxheight_activities_category_pic_thumb );
				$activities_category_pic_thumb = $this->primaryclass->do_upload( 'activities_category_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_activities_category_pic_thumb, $smaxheight_activities_category_pic_thumb );
				//$this->primaryclass->pre_var_dump($activities_category_pic_thumb);

				if( $activities_category_pic_thumb['upload_data']['file_name'] == '' )
				{
					$activities_category_pic_thumb_have_file = $this->input->post('old_activities_category_pic_thumb');
				}else{
					$activities_category_pic_thumb_have_file = $activities_category_pic_thumb['upload_data']['file_name'];

				}
			}
			else
			{
				$activities_category_pic_thumb_have_file = '';
			}//IF PIC THUMB=========================================================

			// PIC LARGE=============================================================
			if( $_FILES['activities_category_pic_large'] )
			{
				$activities_category_pic_large = $this->primaryclass->do_upload('activities_category_pic_large', $sfilepath, $smaxsize, $smaxwidth_activities_category_pic_large, $smaxheight_activities_category_pic_large );
				if( $activities_category_pic_large['upload_data']['file_name'] == '' )
				{
					$activities_category_pic_large_have_file = $this->input->post('old_activities_category_pic_large');
				}else{
					$activities_category_pic_large_have_file = $activities_category_pic_large['upload_data']['file_name'];
				}
			}
			else
			{
				$activities_category_pic_large_have_file = '';
			}//IF PIC LARGE==========================================================


			$data=array(
				'activities_category_id'=>$this->input->post('activities_category_id'),
				'activities_category_name'=>$this->input->post('activities_category_name'),
				'activities_category_expert'=>$this->input->post('activities_category_expert'),
				'activities_category_description'=>$this->input->post('activities_category_description'),
				'activities_category_pic_thumb'=>$activities_category_pic_thumb_have_file,
				'activities_category_pic_large'=>$activities_category_pic_large_have_file,
				'meta_title'=>$this->input->post('meta_title'),
				'meta_keyword'=>$this->input->post('meta_keyword'),
				'meta_description'=>$this->input->post('meta_description'),
				'publish'=>$this->input->post('publish'),
				'updatedate'=>date("Y-m-d H:i:s"),
				'updateby'=>$_SESSION['zID'],
			);
			//echo $this->Primaryclass->pre_var_dump($data);
			$update = $this->Activities_model->update_activities_category($data);
		}
		else
		{
			$row = $this->Activities_model->get_activities_category_record($id);
			$data['row'] = $row;
	//		var_dump($row);

			//($this->input->post())?$this->Activities_model->edit_data_activities_category($data):$this->load->view('backoffice/activities/formadmin-category', array(
			$this->load->view('backoffice/activities/formadmin-category', array(
				'title'=>'แก้ไขข้อมูลหมวดหมู่กิจกรรม',
				'header'=>'yes',
				'url_href'=>'edit',
				'formup'=>'edit',
				'css'=>array(
					'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				),
				'js'=>array(
					'assets/plugins/tinymce/tinymce.min.js',
					'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
					'assets/pages/jquery.sweet-alert.init.js',
				),
				'wysiwigeditor'=>'wysiwigeditor',
				'data'=>$data,
			));
		}
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	public function display_activities_category($id)
	{
		$data=array(
			'activities_category_id'=>$this->input->post('activities_category_id'),
			'publish'=>2,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);

		//echo $this->primaryclass->pre_var_dump($_POST['id']);
		$publish = $this->Activities_model->update_activities_category($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function undisplay_activities_category($id)
	{
		$data=array(
			'activities_category_id'=>$this->input->post('activities_category_id'),
			'publish'=>1,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);
		//echo $_POST['activities_category_id'];
		//echo $this->primaryclass->pre_var_dump($data);
		$publish = $this->Activities_model->update_activities_category($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function delete_activities_category($id)
	{
		$delete = $this->Activities_model->delete_record('activities_category_id', $id, 'activities_category');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
}
