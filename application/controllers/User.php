<?php
defined('BASEPATH') OR exit();
class User extends CI_Controller {
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));								/***** LOADING HELPER TO AVOID PHP ERROR ****/
		$this->load->library('encryption');
		$this->load->model('User_model','User'); 	/* LOADING MODEL * User_model as user */
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public function index($add=0){

		$this->User->logout();

		$this->load->view('user/login',array(
			'title'=>'เข้าสู่ระบบ',
			'header'=>'',
			'js'=>array('')
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public function signup(){
		//$this->load->view('user/add');
		$this->load->view('user/signup',array(
			'title'=>'รายชื่อลูกค้า',
			'header'=>'yes',
			'js'=>array('')
		));
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public function submit_signup(){
		$data=array(
			'username'=>$this->input->post('email'),
			'password'=>md5($this->input->post('password')),
			'fullname'=>$this->input->post('fullname'),
			'createdate'=>date("Y-m-d H:i:s"),
			'ip'=>$this->input->ip_address()
		);

		$insert = $this->User->add_data($data);
		$this->session->set_flashdata('message', 'Your data inserted Successfully..');
		redirect('user/login');
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public function login(){

		($this->input->post())?$this->User_model->login():$this->load->view('user/login',array(
			'title'=>'Sign In',
			'header'=>'',
			'js'=>array('')
		));
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public function check_login(){
		//Field validation succeeded.  Validate against database
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$data=array(
			'username'=>$this->input->post('username'),
			'password'=>md5($this->input->post('password')),
		);

		//query the database
		$result = $this->User_model->login($data);
		//var_dump($result);
		if($result)
		{
			$this->session->set_userdata('zUSER',$data['username']);
			redirect('backoffice/index');
		}
		else
		{
			echo 'Invalid username or password';
			//$this->form_validation->set_message('check_database', 'Invalid username or password');
			return false;
		}
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public function view()
	{
		$data = $this->User_model->viewdata();

		($this->input->post())?'':$this->load->view('backoffice/datatable',array(
			'title'=>'รายชื่อผู้ดูแลระบบ',
			'header'=>'yes',
			'url_href'=>'add',
			'js'=>array(''),
			'data'=>$data
		));
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function delete($staff_id){
		//echo $staff_id = $this->input->post('staff_id');
		$delete = $this->User_model->delete($staff_id);

		$this->session->set_flashdata('message', 'Your data Delete Successfully..');
		redirect('user/view');
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add(){
		($this->input->post())?$this->User_model->add($data):$this->load->view('backoffice/formadmin',array(
			'title'=>'เพิ่มดูแลระบบ',
			'header'=>'yes',
			'url_href'=>'add',
			'js'=>array('')
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_staff()
	{

		$sfilepath = './uploads/staff/.';

		$avatar = $this->do_upload('avatar', $sfilepath);

		$data=array(
			'username'=>$this->input->post('username'),
			'password'=>md5($this->input->post('password')),
			'email'=>$this->input->post('email'),
			'phone'=>$this->input->post('phone'),
			'avatar'=>$avatar['upload_data']['file_name'],
			'fullname'=>$this->input->post('fullname'),
			'staff_group_id'=>$this->input->post('staff_group_id'),
			'approved'=>$this->input->post('approved'),
			'lastlogin'=>date("Y-m-d H:i:s"),
			'createdate'=>date("Y-m-d H:i:s"),
			'ip'=>$this->input->ip_address()
		);

//		echo '<pre>';
//		var_dump($data);
//		echo '</pre>';

		$insert = $this->User->add($data);
		$this->session->set_flashdata('message', 'Your data inserted Successfully..');
		redirect('user/view');
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit($staff_id)
	{
		$row = $this->User_model->getdata($staff_id);
		$data['row'] = $row;
		//var_dump($row);
		($this->input->post())?$this->User_model->edit($data):$this->load->view('backoffice/formadmin', array(
			'title'=>'เพิ่มดูแลระบบ',
			'header'=>'yes',
			'url_href'=>'edit',
			'js'=>array(''),
			'data'=>$data
		));
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit_staff()
	{
		$staff_id = $this->input->post('staff_id');
		/* Upload Image */
		$sfilepath = './uploads/staff/.';
		$avatar = $this->do_upload('avatar', $sfilepath);

		if( $avatar['upload_data']['file_name'] == '' )
		{
			$avatar_have_file = $this->input->post('old_avatar');
		}else{
			$avatar_have_file = $avatar['upload_data']['file_name'];
		}


		$data=array(
			'staff_id'=>$this->input->post('staff_id'),
			'username'=>$this->input->post('username'),
			'avatar'=>$avatar['upload_data']['file_name'],
			'email'=>$this->input->post('email'),
			'phone'=>$this->input->post('phone'),
			'avatar'=>$avatar_have_file,
			'fullname'=>$this->input->post('fullname'),
			'staff_group_id'=>$this->input->post('staff_group_id'),
			'approved'=>$this->input->post('approved'),
			'updatedate'=>date("Y-m-d H:i:s"),
			'ip'=>$this->input->ip_address()
		);
		$update = $this->User->edit($data);
		$this->session->set_flashdata('message', 'Your data Updated Successfully..');
		redirect('user/view');

	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		public function do_upload($sfilename, $sfilepath)
	{
		$config['upload_path']          = $sfilepath;
		$config['allowed_types']        = 'jpeg|jpg|png';
		$config['max_size']             = 2048000;
		$config['max_width']            = 2400;
		$config['max_height']           = 1400;

		$this->load->library('upload', $config);

		//if ( ! $this->upload->do_upload('userfile'))
//		if ( ! $this->upload->do_upload( $sfilename ))
//		{
//			return $error = array('error' => $this->upload->display_errors());
//
//			//$this->load->view('upload_form', $error);
//		}
		if( $this->upload->do_upload( $sfilename ) )
		{
			return $data = array('upload_data' => $this->upload->data());

			//$this->load->view('upload_success', $data);
		}
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

}
