<?php
defined('BASEPATH') OR exit;
class Main extends CI_Controller {
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	function __construct(){
		parent::__construct();
		//$this->Settings=$this->M->setting();
		//$this->Staff=$this->Staff_model->info();
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//	public function index()
//	{
//		$R=$this->Staff_model->info();
//		redirect( ($R)? $R : 'main/login' );
//	}
//	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
//	public function logout(){
//		$this->Staff_model->logout();
//	}
//	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
//	public function login(){
//		($this->input->post())?$this->Staff_model->login():$this->load->view('backoffice/login',array(
//			'title'=>'LOG IN BACKOFFICE~S',
//			'header'=>'',
//			'css'=>array(
//				'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
//			),
//			'js'=>array(
//				'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
//				'assets/pages/jquery.sweet-alert.init.js',
//			),
//		));
//	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	//public function index(){$R=$this->User_model->info(1);redirect(($R)?$R:'user/login');}
//	public function autocomplete($table,$name){$this->M->autocomplete($table,$name);}
//	public function set($id,$table,$name){$this->M->JSON($this->M->set($id,$table,$name));}
//	public function edit($id=0,$table,$name){$this->M->JSON($this->M->edit($id,$table,$name));}
//	public function delete($table,$name){$this->M->delete($table,$name);}
//	public function filter($table,$name){$this->M->filter($table,$name);}
//	public function save(){exit('Access Denied');}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit($table,$id=0){
		$result = $this->M->set($table,$id);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function delete($table,$pk,$id=0)
	{
		if( $this->input->post() )
		{
			foreach($this->input->post() as $row)
			{
				echo unlink("uploads/".$table."/".$row);
			}
			//var_dump($this->input->post());
		}
		$result = $this->M->delete($table,$pk,$id);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


		public function img_crop_file($sFilePath){


			$imgUrl = $_POST['imgUrl'];
			// original sizes
			$imgInitW = $_POST['imgInitW'];
			$imgInitH = $_POST['imgInitH'];
			// resized sizes
			$imgW = $_POST['imgW'];
			$imgH = $_POST['imgH'];
			// offsets
			$imgY1 = $_POST['imgY1'];
			$imgX1 = $_POST['imgX1'];
			// crop box
			$cropW = $_POST['cropW'];
			$cropH = $_POST['cropH'];
			// rotation angle
			$angle = $_POST['rotation'];

			$jpeg_quality = 100;

//			$output_filename = "temp/croppedImg_".rand();
			$output_filename = "uploads/".$sFilePath."/".$sFilePath."_".rand();

			// uncomment line below to save the cropped image in the same location as the original image.
			//$output_filename = dirname($imgUrl). "/croppedImg_".rand();

			$what = getimagesize($imgUrl);

			switch(strtolower($what['mime']))
			{
					case 'image/png':
							$img_r = imagecreatefrompng($imgUrl);
					$source_image = imagecreatefrompng($imgUrl);
					$type = '.png';
							break;
					case 'image/jpeg':
							$img_r = imagecreatefromjpeg($imgUrl);
					$source_image = imagecreatefromjpeg($imgUrl);
					error_log("jpg");
					$type = '.jpeg';
							break;
					case 'image/gif':
							$img_r = imagecreatefromgif($imgUrl);
					$source_image = imagecreatefromgif($imgUrl);
					$type = '.gif';
							break;
					default: die('image type not supported');
			}


			//Check write Access to Directory

			if(!is_writable(dirname($output_filename))){
				$response = Array(
						"status" => 'error',
						"message" => 'Can`t write cropped File'
					);
			}else{

				// resize the original image to size of editor
				$resizedImage = imagecreatetruecolor($imgW, $imgH);
				imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
				// rotate the rezized image
				$rotated_image = imagerotate($resizedImage, -$angle, 0);
				// find new width & height of rotated image
				$rotated_width = imagesx($rotated_image);
				$rotated_height = imagesy($rotated_image);
				// diff between rotated & original sizes
				$dx = $rotated_width - $imgW;
				$dy = $rotated_height - $imgH;
				// crop rotated image to fit into original rezized rectangle
				$cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
				imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
				imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
				// crop image into selected area
				$final_image = imagecreatetruecolor($cropW, $cropH);
				imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
				imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
				// finally output png image
				//imagepng($final_image, $output_filename.$type, $png_quality);
				imagejpeg($final_image, $output_filename.$type, $jpeg_quality);
				$response = Array(
						"status" => 'success',
//						"url" => $output_filename.$type
						"url" => $output_filename.$type
					);
			}
			print json_encode($response);
		}
		//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		public function img_save_file( $sFilePath )
		{
//			$imagePath = "temp/";
			$imagePath = "uploads/".$sFilePath."/";

			$allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
			$temp = explode(".", $_FILES["img"]["name"]);
			$extension = end($temp);

			//Check write Access to Directory

			if(!is_writable($imagePath)){
				$response = Array(
					"status" => 'error',
					"message" => 'Can`t upload File; no write Access'
				);
				print json_encode($response);
				return;
			}

			if ( in_array($extension, $allowedExts))
				{
				if ($_FILES["img"]["error"] > 0)
				{
					 $response = array(
						"status" => 'error',
						"message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
					);
				}
				else
				{

//					echo move_uploaded_file($_FILES["img"]["tmp_name"], $imagePath . $extension);
//					echo  in_array($extension, $allowedExts);
					$filename = $_FILES["img"]["tmp_name"];
					//$filename = base_url();
					list($width, $height) = getimagesize( $filename );

					move_uploaded_file($filename,  $imagePath . $_FILES["img"]["name"]);

					$response = array(
					"status" => 'success',
					"url" => base_url().$imagePath.$_FILES["img"]["name"],
					"width" => $width,
					"height" => $height
					);

				}
				}
			else
				{
				 $response = array(
					"status" => 'error',
					"message" => 'something went wrong, most likely file is to large for upload. check upload_max_filesize, post_max_size and memory_limit in you php.ini',
				);
				}

				print json_encode($response);

		}
		//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

}
