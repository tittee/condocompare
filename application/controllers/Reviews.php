<?php
defined('BASEPATH') OR exit();
class Reviews extends CI_Controller {
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));								/***** LOADING HELPER TO AVOID PHP ERROR ****/
		$this->load->library('primaryclass');
		$this->load->library('dateclass');
		$this->load->model('Staff_model');								/***** LOADING Controller * Staff_model as all ****/

		$this->load->model('Reviews_model');								/***** LOADING Controller * Primaryfunc as all ****/
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



  public function index( $category_id = 0 )
  {
    $this->load->model('Page_model');
      $this->load->helper('text');
      $data_category = $this->Reviews_model->get_reviews_category_publish();
      $data_mostread = $this->Reviews_model->get_reviews_mostread();


      if( $category_id !== 0 ){
				$url_page = base_url().'reviews-category/'.$category_id;
			}else{
				$url_page = base_url().'reviews';
			}
      // ========================================================================
      // 					PAGINATION
      // ========================================================================
      $data['reviews_total'] = $this->Reviews_model->get_reviews_record($category_id);
      $per_page = '15';

      $page_pagination = $this->primaryclass->set_pagination($url_page, $data['reviews_total'], $per_page);
      $data['links'] = $this->pagination->create_links();

      // DATA #1
      $data['reviews'] = $this->Reviews_model->get_reviewslist($page_pagination['per_page'], $page_pagination['page'], $category_id);
      // ========================================================================

      $breadcrumbs_arr = array(
        '1' => array(
          'title' => 'รีวิวทั้งหมด',
          'href' => 'reviews',
        ),
      );
      $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

      $this->load->view('frontend/reviews', array(
          'title' => 'รีวิวทั้งหมด',
          'css' => array('assets/css/slippry.min.css'),
          'js' => array('assets/js/slippry.min.js'),
          'data_category' => $data_category,
          'data_mostread' => $data_mostread,
          'breadcrumbs' => $breadcrumbs,
          'data' => $data,
          'slippry' => 'slippry',
      ));
  }
  //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public function reviewsinside($id)
  {
    $this->load->model('Page_model');
      $this->load->helper('text');

    //HEADER SEO
      $reviews_items = $this->M->get_data_by_id('reviews_id, reviews_title, reviews_pic_thumb, reviews_caption', 'reviews_id', $id, 'reviews');

      $data = $this->Reviews_model->get_info($id);
      $data_mostread = $this->Reviews_model->get_reviews_mostread();
      $update_visited = $this->M->update_visited('reviews_id', $id, 'reviews');
      $breadcrumbs_arr = array(
        '1' => array(
          'title' => 'รีวิวทั้งหมด',
          'href' => 'reviews',
        ),
        '2' => array(
          'title' => $data->reviews_title,
          'href' => '../reviews-details/'.$data->reviews_id,
        ),
      );
      $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

      $this->load->view('frontend/reviews-inside', array(
        'title' => 'รายละเอียดรีวิว',
        'data' => $data,
        'breadcrumbs' => $breadcrumbs,
        'reviews_items' => $reviews_items,
        'data_mostread' => $data_mostread,
        'isslider' => true,
        'sharepage' => true,
    ));
  }
  //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function reviews()//List reviews
	{
    $this->Staff=$this->Staff_model->info();
		$data= $this->Reviews_model->get_reviews();
		$this->load->view('backoffice/reviews/datatable',array(
			'title'=>'ตารางข้อมูลรีวิว',
			'header'=>'yes',
			'url_href'=>'add_reviews',
			'css'=>array(
				'assets/plugins/RWD-Table-Patterns/dist/css/rwd-table.min.css',
				'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
			),
			'js'=>array(
				'assets/plugins/RWD-Table-Patterns/dist/js/rwd-table.min.js',
				'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
				'assets/pages/jquery.sweet-alert.init.js',
			),
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_reviews()
	{
    $this->Staff=$this->Staff_model->info();
		if($this->input->post('action'))
		{
			$sfilepath = './uploads/reviews/.';
			$smaxsize = 4096000; //4MB
			$smaxwidth_reviews_pic_thumb = 1200;
			$smaxheight_reviews_pic_thumb = 1200;

			$smaxwidth_reviews_pic_large = 1200;
			$smaxheight_reviews_pic_large = 1200;

			// PIC THUMB===========================================================
			if( $_FILES['reviews_pic_thumb'] )
			{
				$reviews_pic_thumb = $this->primaryclass->do_upload( 'reviews_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_reviews_pic_thumb, $smaxheight_reviews_pic_thumb );
				if( $reviews_pic_thumb['upload_data']['file_name'] == '' )
				{
					$reviews_pic_thumb_have_file = $this->input->post('old_reviews_pic_thumb');
				}else{
					$reviews_pic_thumb_have_file = $reviews_pic_thumb['upload_data']['file_name'];
				}
			}
			else
			{
				$reviews_pic_thumb_have_file = '';
			}//IF PIC THUMB=========================================================

			// PIC LARGE=============================================================
			if( $_FILES['reviews_pic_large'] )
			{
				$reviews_pic_large = $this->primaryclass->do_upload('reviews_pic_large', $sfilepath, $smaxsize, $smaxwidth_reviews_pic_large, $smaxheight_reviews_pic_large );
				if( $reviews_pic_large['upload_data']['file_name'] == '' )
				{
					$reviews_pic_large_have_file = $this->input->post('old_reviews_pic_large');
				}else{
					$reviews_pic_large_have_file = $reviews_pic_large['upload_data']['file_name'];
				}
			}
			else
			{
				$reviews_pic_large_have_file = '';
			}//IF PIC LARGE==========================================================

      $reviews_slug = url_title($data['reviews_title'], 'dash', TRUE);
			$data=array(
				'fk_reviews_category_id'=>$this->input->post('fk_reviews_category_id'),
				'reviews_title'=>$this->input->post('reviews_title'),
				'reviews_caption'=>$this->input->post('reviews_caption'),
				'reviews_description'=>$this->input->post('reviews_description'),
				'reviews_pic_thumb'=>$reviews_pic_thumb_have_file,
				'reviews_pic_large'=>$reviews_pic_large_have_file,
				'reviews_slug'=>$reviews_slug,
				// 'language_slug'=>$this->input->post('language_slug'),
				'meta_title'=>$this->input->post('meta_title'),
				'meta_keyword'=>$this->input->post('meta_keyword'),
				'meta_description'=>$this->input->post('meta_description'),
				'highlight'=>$this->input->post('highlight'),
				'recommended'=>$this->input->post('recommended'),
				'publish'=>$this->input->post('publish'),
				'createdate'=>date("Y-m-d H:i:s"),
				'createby'=>$_SESSION['zID'],
			);

			$insert = $this->Reviews_model->insert_reviews($data);

		}
		else
		{
			$reviews_category 	= $this->Reviews_model->get_reviews_category_option();
			$this->load->view('backoffice/reviews/formadmin',array(
				'title'=>'เพิ่มข้อมูลรีวิว',
				'header'=>'yes',
				'formup'=>'add',
				'css'=>array(
					'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				),
				'js'=>array(
					'assets/plugins/tinymce/tinymce.min.js',
					'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
					'assets/pages/jquery.sweet-alert.init.js',
				),
				'wysiwigeditor'=>'wysiwigeditor',
				'reviews_category'=>$reviews_category,
			));
		}

	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit_reviews($id=0)
	{
    $this->Staff=$this->Staff_model->info();
		if($this->input->post('action'))
		{
			/* Upload Image */
			$sfilepath = './uploads/reviews/.';
			$smaxsize = 4096000; //4MB
			$smaxwidth_reviews_pic_thumb = 1000;
			$smaxheight_reviews_pic_thumb = 1000;

			$smaxwidth_reviews_pic_large = 1200;
			$smaxheight_reviews_pic_large = 1000;

			// PIC THUMB===========================================================
			if( $_FILES['reviews_pic_thumb'] )
			{
	//			$reviews_pic_thumb = $this->primaryclass->do_upload( 'reviews_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_reviews_pic_thumb, $smaxheight_reviews_pic_thumb );
				$reviews_pic_thumb = $this->primaryclass->do_upload( 'reviews_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_reviews_pic_thumb, $smaxheight_reviews_pic_thumb );
				//$this->primaryclass->pre_var_dump($reviews_pic_thumb);

				if( $reviews_pic_thumb['upload_data']['file_name'] == '' )
				{
					$reviews_pic_thumb_have_file = $this->input->post('old_reviews_pic_thumb');
				}else{
					$reviews_pic_thumb_have_file = $reviews_pic_thumb['upload_data']['file_name'];

				}
			}
			else
			{
				$reviews_pic_thumb_have_file = '';
			}//IF PIC THUMB=========================================================

			// PIC LARGE=============================================================
			if( $_FILES['reviews_pic_large'] )
			{
				$reviews_pic_large = $this->primaryclass->do_upload('reviews_pic_large', $sfilepath, $smaxsize, $smaxwidth_reviews_pic_large, $smaxheight_reviews_pic_large );
				if( $reviews_pic_large['upload_data']['file_name'] == '' )
				{
					$reviews_pic_large_have_file = $this->input->post('old_reviews_pic_large');
				}else{
					$reviews_pic_large_have_file = $reviews_pic_large['upload_data']['file_name'];
				}
			}
			else
			{
				$reviews_pic_large_have_file = '';
			}//IF PIC LARGE==========================================================

			$data=$this->security->xss_clean($this->input->post());
      $reviews_slug = url_title($data['reviews_slug'], 'dash', TRUE);
			$data=array(
				'reviews_id'=>$data['reviews_id'],
				'fk_reviews_category_id'=>$data['fk_reviews_category_id'],
				'reviews_title'=>$data['reviews_title'],
				'reviews_caption'=>$data['reviews_caption'],
				'reviews_description'=>$data['reviews_description'],
				'reviews_pic_thumb'=>$reviews_pic_thumb_have_file,
				'reviews_pic_large'=>$reviews_pic_large_have_file,
				'reviews_slug'=>$reviews_slug,
				// 'language_slug'=>$data['language_slug'],
				'meta_title'=>$data['meta_title'],
				'meta_keyword'=>$data['meta_keyword'],
				'meta_description'=>$data['meta_description'],
				'highlight'=>$data['highlight'],
				'recommended'=>$data['recommended'],
				'publish'=>$data['publish'],
				'updatedate'=>date("Y-m-d H:i:s"),
				'createby'=>$_SESSION['zID'],
			);
			$update = $this->Reviews_model->update_reviews($data);
		}
		else
		{
			$row = $this->Reviews_model->get_info($id);
			$reviews_category 	= $this->Reviews_model->get_reviews_category_option();
			$data['row'] = $row;
			$this->load->view('backoffice/reviews/formadmin', array(
				'title'=>'แก้ไขข้อมูลข่าวสาร',
				'header'=>'yes',
				'url_href'=>'edit',
				'formup'=>'edit',
				'css'=>array(
					'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				),
				'js'=>array(
					'assets/plugins/tinymce/tinymce.min.js',
					'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
					'assets/pages/jquery.sweet-alert.init.js',
				),
				'wysiwigeditor'=>'wysiwigeditor',
				'data'=>$data,
				'reviews_category'=>$reviews_category,
			));
		}
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function display_reviews($id)
	{
		$data=array(
			'reviews_id'=>$this->input->post('reviews_id'),
			'publish'=>2,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);

		//echo $this->primaryclass->pre_var_dump($_POST['id']);
		$publish = $this->Reviews_model->update_reviews($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function undisplay_reviews($id)
	{
		$data=array(
			'reviews_id'=>$this->input->post('reviews_id'),
			'publish'=>1,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);
		//echo $_POST['reviews_id'];
		//echo $this->primaryclass->pre_var_dump($data);
		$publish = $this->Reviews_model->update_reviews($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function delete_reviews($id)
	{
		$delete = $this->Reviews_model->delete_record('reviews_id', $id, 'reviews');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	/* reviews Category */
	public function category()
	{
		//var_dump( $this->Staff );
		$data= $this->Reviews_model->get_reviews_category();

		$this->load->view('backoffice/reviews/datatable-category',array(
			'title'=>'ตารางข้อมูลหมวดหมู่รีวิว',
			'header'=>'yes',
			'url_href'=>'add_reviews_category',
			'css'=>array(
				'assets/plugins/RWD-Table-Patterns/dist/css/rwd-table.min.css',
				'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
			),
			'js'=>array(
				'assets/plugins/RWD-Table-Patterns/dist/js/rwd-table.min.js',
				'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
				'assets/pages/jquery.sweet-alert.init.js',
			),
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_reviews_category()
	{
		if($this->input->post('action'))
		{
			$sfilepath = './uploads/reviews/.';
			$smaxsize = 4096000; //4MB
			$smaxwidth_reviews_category_pic_thumb = 1200;
			$smaxheight_reviews_category_pic_thumb = 1200;

			$smaxwidth_reviews_category_pic_large = 1200;
			$smaxheight_reviews_category_pic_large = 1200;

			// PIC THUMB===========================================================
			if( $_FILES['reviews_category_pic_thumb'] )
			{
				$reviews_category_pic_thumb = $this->primaryclass->do_upload( 'reviews_category_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_reviews_category_pic_thumb, $smaxheight_reviews_category_pic_thumb );
				if( $reviews_category_pic_thumb['upload_data']['file_name'] == '' )
				{
					$reviews_category_pic_thumb_have_file = $this->input->post('old_reviews_category_pic_thumb');
				}else{
					$reviews_category_pic_thumb_have_file = $reviews_category_pic_thumb['upload_data']['file_name'];
				}
			}
			else
			{
				$reviews_category_pic_thumb_have_file = '';
			}//IF PIC THUMB=========================================================

			// PIC LARGE=============================================================
			if( $_FILES['reviews_category_pic_large'] )
			{
				$reviews_category_pic_large = $this->primaryclass->do_upload('reviews_category_pic_large', $sfilepath, $smaxsize, $smaxwidth_reviews_category_pic_large, $smaxheight_reviews_category_pic_large );
				if( $reviews_category_pic_large['upload_data']['file_name'] == '' )
				{
					$reviews_category_pic_large_have_file = $this->input->post('old_reviews_category_pic_large');
				}else{
					$reviews_category_pic_large_have_file = $reviews_category_pic_large['upload_data']['file_name'];
				}
			}
			else
			{
				$reviews_category_pic_large_have_file = '';
			}//IF PIC LARGE==========================================================

			$data=$this->security->xss_clean($this->input->post());
			$data=array(
				'reviews_category_name'=>$data['reviews_category_name'],
				'reviews_category_expert'=>$data['reviews_category_expert'],
				'reviews_category_description'=>$data['reviews_category_description'],
				'reviews_category_pic_thumb'=>$reviews_category_pic_thumb_have_file,
				'reviews_category_pic_large'=>$reviews_category_pic_large_have_file,
				'meta_title'=>$data['meta_title'],
				'meta_keyword'=>$data['meta_keyword'],
				'meta_description'=>$data['meta_description'],
				'publish'=>$data['publish'],
				'createdate'=>date("Y-m-d H:i:s"),
				'createby'=>$_SESSION['zID'],
			);
			$insert = $this->Reviews_model->insert_reviews_category($data);

		}
		else
		{
			$this->load->view('backoffice/reviews/formadmin-category',array(
				'title'=>'เพิ่มข้อมูลหมวดหมู่รีวิว',
				'header'=>'yes',
				'formup'=>'add',
				'css'=>array(
					'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				),
				'js'=>array(
					'assets/plugins/tinymce/tinymce.min.js',
					'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
					'assets/pages/jquery.sweet-alert.init.js',
				),
				'wysiwigeditor'=>'wysiwigeditor',
			));
		}

	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_data_reviews_category()
	{
		$sfilepath = './uploads/reviews/.';

		if( $_FILES['reviews_category_pic_thumb'] )
		{
			$pic_thumb = $this->do_upload('reviews_category_pic_thumb', $sfilepath);
		}
		else
		{
			$pic_thumb = '';
		}

		if( $_FILES['reviews_category_pic_large'] )
		{
			$pic_large = $this->do_upload('reviews_category_pic_large', $sfilepath);
		}
		else
		{
			$pic_large = '';
		}

		$data=array(
			'reviews_category_name'=>$this->input->post('reviews_category_name'),
			'reviews_category_expert'=>$this->input->post('reviews_category_expert'),
			'reviews_category_description'=>$this->input->post('reviews_category_description'),
			'reviews_category_pic_thumb'=>$pic_thumb['upload_data']['file_name'],
			'reviews_category_pic_large'=>$pic_large['upload_data']['file_name'],
			'meta_title'=>$this->input->post('meta_title'),
			'meta_keyword'=>$this->input->post('meta_keyword'),
			'meta_description'=>$this->input->post('meta_description'),
			'publish'=>$this->input->post('publish'),
			'createdate'=>date("Y-m-d H:i:s"),
			'createby'=>$_SESSION['zID'],
		);

		//var_dump($data);
		$insert = $this->Reviews_model->insert_reviews_category($data);
		//$this->session->set_flashdata('message', 'Your data inserted Successfully..');
		//redirect('reviews/category');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit_reviews_category($id=0)
	{
		if($this->input->post('action'))
		{
			/* Upload Image */
			$sfilepath = './uploads/reviews/.';
			$smaxsize = 4096000; //4MB
			$smaxwidth_reviews_category_pic_thumb = 1000;
			$smaxheight_reviews_category_pic_thumb = 1000;

			$smaxwidth_reviews_category_pic_large = 1200;
			$smaxheight_reviews_category_pic_large = 1000;

			// PIC THUMB===========================================================
			if( $_FILES['reviews_category_pic_thumb'] )
			{
	//			$reviews_category_pic_thumb = $this->primaryclass->do_upload( 'reviews_category_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_reviews_category_pic_thumb, $smaxheight_reviews_category_pic_thumb );
				$reviews_category_pic_thumb = $this->primaryclass->do_upload( 'reviews_category_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_reviews_category_pic_thumb, $smaxheight_reviews_category_pic_thumb );
				//$this->primaryclass->pre_var_dump($reviews_category_pic_thumb);

				if( $reviews_category_pic_thumb['upload_data']['file_name'] == '' )
				{
					$reviews_category_pic_thumb_have_file = $this->input->post('old_reviews_category_pic_thumb');
				}else{
					$reviews_category_pic_thumb_have_file = $reviews_category_pic_thumb['upload_data']['file_name'];

				}
			}
			else
			{
				$reviews_category_pic_thumb_have_file = '';
			}//IF PIC THUMB=========================================================

			// PIC LARGE=============================================================
			if( $_FILES['reviews_category_pic_large'] )
			{
				$reviews_category_pic_large = $this->primaryclass->do_upload('reviews_category_pic_large', $sfilepath, $smaxsize, $smaxwidth_reviews_category_pic_large, $smaxheight_reviews_category_pic_large );
				if( $reviews_category_pic_large['upload_data']['file_name'] == '' )
				{
					$reviews_category_pic_large_have_file = $this->input->post('old_reviews_category_pic_large');
				}else{
					$reviews_category_pic_large_have_file = $reviews_category_pic_large['upload_data']['file_name'];
				}
			}
			else
			{
				$reviews_category_pic_large_have_file = '';
			}//IF PIC LARGE==========================================================


			$data=array(
				'reviews_category_id'=>$this->input->post('reviews_category_id'),
				'reviews_category_name'=>$this->input->post('reviews_category_name'),
				'reviews_category_expert'=>$this->input->post('reviews_category_expert'),
				'reviews_category_description'=>$this->input->post('reviews_category_description'),
				'reviews_category_pic_thumb'=>$reviews_category_pic_thumb_have_file,
				'reviews_category_pic_large'=>$reviews_category_pic_large_have_file,
				'meta_title'=>$this->input->post('meta_title'),
				'meta_keyword'=>$this->input->post('meta_keyword'),
				'meta_description'=>$this->input->post('meta_description'),
				'publish'=>$this->input->post('publish'),
				'updatedate'=>date("Y-m-d H:i:s"),
				'updateby'=>$_SESSION['zID'],
			);
			//echo $this->Primaryclass->pre_var_dump($data);
			$update = $this->Reviews_model->update_reviews_category($data);
		}
		else
		{
			$row = $this->Reviews_model->get_reviews_category_record($id);
			$data['row'] = $row;
	//		var_dump($row);

			//($this->input->post())?$this->Reviews_model->edit_data_reviews_category($data):$this->load->view('backoffice/reviews/formadmin-category', array(
			$this->load->view('backoffice/reviews/formadmin-category', array(
				'title'=>'แก้ไขข้อมูลหมวดหมู่รีวิว',
				'header'=>'yes',
				'url_href'=>'edit',
				'formup'=>'edit',
				'css'=>array(
					'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				),
				'js'=>array(
					'assets/plugins/tinymce/tinymce.min.js',
					'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
					'assets/pages/jquery.sweet-alert.init.js',
				),
				'wysiwigeditor'=>'wysiwigeditor',
				'data'=>$data,
			));
		}
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit_data_reviews_category()
	{
		$category_id = $this->input->post('category_id');
		/* Upload Image */
		$sfilepath = './uploads/reviews/.';
		$pic_thumb = $this->do_upload('reviews_category_pic_thumb', $sfilepath);
		$pic_large = $this->do_upload('reviews_category_pic_large', $sfilepath);


		if( $_FILES['reviews_category_pic_thumb'] )
		{
			if( $pic_thumb['upload_data']['file_name'] == '' )
			{
				$pic_thumb_have_file = $this->input->post('old_reviews_category_pic_thumb');
			}else{
				$pic_thumb_have_file = $pic_thumb['upload_data']['file_name'];
			}
		}
		else
		{
			$pic_thumb_have_file = '';
		}//IF PIC THUMB


		if( $_FILES['reviews_category_pic_large'] )
		{
			if( $pic_large['upload_data']['file_name'] == '' )
			{
				$pic_large_have_file = $this->input->post('old_reviews_category_pic_large');
			}else{
				$pic_large_have_file = $pic_thumb['upload_data']['file_name'];
			}
		}
		else
		{
			$pic_large_have_file = '';
		}//IF PIC LARGE


		$data=array(
			'reviews_category_id'=>$this->input->post('reviews_category_id'),
			'reviews_category_name'=>$this->input->post('reviews_category_name'),
			'reviews_category_expert'=>$this->input->post('reviews_category_expert'),
			'reviews_category_description'=>$this->input->post('reviews_category_description'),
			'reviews_category_pic_thumb'=>$pic_thumb_have_file,
			'reviews_category_pic_large'=>$pic_large_have_file,
			'meta_title'=>$this->input->post('meta_title'),
			'meta_keyword'=>$this->input->post('meta_keyword'),
			'meta_description'=>$this->input->post('meta_description'),
			'publish'=>$this->input->post('publish'),
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);
		//echo $this->Primaryclass->pre_var_dump($data);

		$update = $this->Reviews_model->update_reviews_category($data);
		$this->session->set_flashdata('message', 'Your data Updated Successfully..');
		redirect('reviews/list_reviews_category');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function display_reviews_category($id)
	{
		$data=array(
			'reviews_category_id'=>$this->input->post('reviews_category_id'),
			'publish'=>2,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);

		//echo $this->primaryclass->pre_var_dump($_POST['id']);
		$publish = $this->Reviews_model->update_reviews_category($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function undisplay_reviews_category($id)
	{
		$data=array(
			'reviews_category_id'=>$this->input->post('reviews_category_id'),
			'publish'=>1,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);
		//echo $_POST['reviews_category_id'];
		//echo $this->primaryclass->pre_var_dump($data);
		$publish = $this->Reviews_model->update_reviews_category($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function delete_reviews_category($id)
	{
		$delete = $this->Reviews_model->delete_record('reviews_category_id', $id, 'reviews_category');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
}
