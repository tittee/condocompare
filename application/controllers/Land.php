<?php

defined('BASEPATH') or exit;

class Land extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('encryption');
        $this->load->library('primaryclass');
        $this->load->library('dateclass');
        $this->load->library('upload');
        $this->load->model('Staff_model');
        /* LOADING MODEL * Staff_model as staff */
        $this->Staff = $this->Staff_model->info();
        $this->load->model('Configsystem_model', 'Configsystem');/*         * *** LOADING Controller * Configsystem_model *** */
        $this->load->model('Land_model');
        $this->load->model('Member_model', 'Member');
        $this->load->model('Condominium_model');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function json_land()
    {
        $list = $this->Land_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $land) {
          $no++;
            $id = $land->land_id;
            $land_title = $land->land_title;
            $visited = $land->visited;

            $output_id = '<input type="checkbox" name="land_id[]" class="land_id" value="'.$id.'"> '. $no;

            $pic_thumb = base_url().'uploads/land/'.$land->pic_thumb;
            $pic_thumb_path = '<img src="'.$pic_thumb.'" style="width: 100px;" class="img-thumbnail" alt="">';

            $output_title = '';
            $output_title .= $land_title . '<br />';
    				$output_title .= 'จำนวนคนดู : ' . $visited;

            $land_property_id = ( !empty($land->land_property_id) )? $land->land_property_id : 'ยังไม่ทำการระบุ';

            $createdate = $land->createdate;
            $modifydate = $land->modifydate;
            $createdate_t = ($createdate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
            $modifydate_t = ($modifydate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($modifydate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
            $approved = $this->primaryclass->get_approved($land->approved);

            $output_date = '';
            $output_date .= 'วันที่สร้าง : ' . $createdate_t . '<br />';
            $output_date .= 'วันที่แก้ไข : ' . $modifydate_t . '<br />';
    				$output_date .= 'การแสดงผล : ' . $approved;

            $path_edit = site_url('land/edit/'.$id);
    				$path_images = site_url('land/images/'.$id);
    				$path_share = site_url('land/share/'.$id);
    				$output_manage = '';
    				$output_manage .= '<a href='. $path_edit .' class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5"><i class="fa fa-pencil"></i></a>';
    				$output_manage .= '<a href='. $path_images .' class="btn btn-icon waves-effect waves-light btn-purple m-b-5 m-r-5"><i class="zmdi zmdi-collection-image-o"></i></a>';
            if( $approved == 1){
    				$output_manage .= '<a href="#" class="btn btn-approved btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id='. $id .'><i class="zmdi zmdi-lock-open"></i></a>';
          }else{

            $output_manage .= '<a href="#" class="btn btn-unapproved btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id='. $id .'><i class="zmdi zmdi-lock-outline"></i></a>';
          }
    				$output_manage .= '<a href="#" id='. $id .' class="btn btn-delete btn-icon waves-effect waves-light btn-danger m-b-5 m-r-5"><i class="ti-trash"></i></a>';
    				$output_manage .= '<a href='.$path_share.' class="btn btn-icon waves-effect waves-light btn-pink m-b-5 m-r-5"><i class="zmdi zmdi-share"></i></a>';


            $row = array();
            $row[] = $output_id;
            $row[] = $pic_thumb_path;
            $row[] = $output_title;
            $row[] = $land_property_id;
            $row[] = $output_date;
            $row[] = $output_manage;

            $data[] = $row;
        }

        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->Land_model->count_all(),
                "recordsFiltered" => $this->Land_model->count_filtered(),
                "data" => $data,
        );
        //output to json format
        echo json_encode($output);

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    public function view()
    {
        $data = $this->Land_model->get_land();

        $this->load->view('backoffice/land/datatable-land',
            array('
				title' => 'ตารางข้อมูลที่ดิน',
                'header' => 'yes',
                'url_href' => 'add',
                'css' => array(
                    'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
                    'assets/plugins/datatables/jquery.dataTables.min.css',
                    'assets/plugins/datatables/buttons.bootstrap.min.css',
                    'assets/plugins/datatables/fixedHeader.bootstrap.min.css',
                    'assets/plugins/datatables/responsive.bootstrap.min.css',
                    'assets/plugins/datatables/scroller.bootstrap.min.css',
                ),
                'js' => array(
                    'assets/plugins/tinymce/tinymce.min.js',
                    'assets/js/dropzone.min.js',
                    'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
                    'assets/pages/jquery.sweet-alert.init.js',
                    'assets/plugins/datatables/jquery.dataTables.min.js',
                    'assets/plugins/datatables/dataTables.bootstrap.js',
                    'assets/plugins/datatables/dataTables.buttons.min.js',
                    'assets/plugins/datatables/buttons.bootstrap.min.js',
                    'assets/plugins/datatables/jszip.min.js',
                    'assets/plugins/datatables/pdfmake.min.js',
                    'assets/plugins/datatables/vfs_fonts.js',
                    'assets/plugins/datatables/buttons.html5.min.js',
                    'assets/plugins/datatables/buttons.print.min.js',
                    'assets/plugins/datatables/dataTables.fixedHeader.min.js',
                    'assets/plugins/datatables/dataTables.keyTable.min.js',
                    'assets/plugins/datatables/dataTables.responsive.min.js',
                    'assets/plugins/datatables/responsive.bootstrap.min.js',
                    ),
            'data' => $data, )
        );
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add()
    {
        $id = 0;

        $provinces = $this->M->get_provinces();
        $staff_list = $this->User_model->get_staff_option();
        $type_property = $this->Configsystem->get_type_property_option();
        $type_zone = $this->Configsystem->get_type_zone_option();
        $type_offering = $this->Configsystem->get_type_offering_option();
        $member = $this->Member->get_fullname();
        $transportation_category = $this->Condominium_model->get_transportation_category_option();

        $sfilepath = './uploads/land/.';
        $smaxsize = 4096000; //4MB
        $smaxwidth_pic_thumb = 2000;
        $smaxheight_pic_thumb = 2000;

        $smaxwidth_pic_large = 2000;
        $smaxheight_pic_large = 2000;

        if ($this->input->post('action')) {
            // PIC THUMB===========================================================
            if ($_FILES['pic_thumb']) {
                $pic_thumb = $this->primaryclass->do_upload('pic_thumb', $sfilepath, $smaxsize, $smaxwidth_pic_thumb, $smaxheight_pic_thumb);
                if ($pic_thumb['upload_data']['file_name'] == '') {
                    $pic_thumb_have_file = $this->input->post('old_pic_thumb');
                } else {
                    $pic_thumb_have_file = $pic_thumb['upload_data']['file_name'];
                }
            } else {
                $pic_thumb_have_file = '';
            }//IF PIC THUMB=========================================================

            // PIC LARGE=============================================================
            if ($_FILES['pic_large']) {
                $pic_large = $this->primaryclass->do_upload('pic_large', $sfilepath, $smaxsize, $smaxwidth_pic_large, $smaxheight_pic_large);
                if ($pic_large['upload_data']['file_name'] == '') {
                    $pic_large_have_file = $this->input->post('old_pic_large');
                } else {
                    $pic_large_have_file = $pic_large['upload_data']['file_name'];
                }
            } else {
                $pic_large_have_file = '';
            }//IF PIC LARGE==========================================================

            $data = $this->security->xss_clean($this->input->post());
            $data = array(
                'land_property_id' => $data['land_property_id'],
                'member_id' => $data['member_id'],
                'staff_id' => $data['staff_id'],
                'land_title' => $data['land_title'],
                'land_owner_name' => $data['land_owner_name'],
                'land_holder_name' => $data['land_holder_name'],
                'property_type_id' => $data['property_type_id'],
                'type_zone_id' => $data['type_zone_id'],
                'zone_id' => $data['zone_id'],
                'type_offering_id' => $data['type_offering_id'],
                'land_expert' => trim($data['land_expert']),
                'land_description' => trim($data['land_description']),
                'pic_thumb' => $pic_thumb_have_file,
                'pic_large' => $pic_large_have_file,
                'provinces_id' => $data['provinces_id'],
                'districts_id' => $data['districts_id'],
                'sub_districts_id' => $data['sub_districts_id'],
                'land_zipcode' => $data['land_zipcode'],
                // 'land_size' => $data['land_size'],
                // 'land_size_unit' => $data['land_size_unit'],
                'land_total_price' => $data['land_total_price'],
                'land_price_per_square_meter' => $data['land_price_per_square_meter'],
                'land_price_per_square_mortgage' => $data['land_price_per_square_mortgage'],
                //'land_googlemaps' => trim($data['land_googlemaps']),
                'latitude' => trim($data['latitude']),
                'longitude' => trim($data['longitude']),
                'land_tag' => $data['land_tag'],
                'createdate' => date('Y-m-d H:i:s'),
                'visited' => 0,
                'type_status_id' => 0,
                'newsarrival' => $data['newsarrival'],
                'highlight' => $data['highlight'],
                'approved' => $data['approved'],
                'land_size_square_rai' => $data['land_size_square_rai'],
                'land_size_square_ngan' => $data['land_size_square_ngan'],
                'land_size_square_yard' => $data['land_size_square_yard'],
            );
        }

        ($this->input->post('action')) ? $this->Land_model->set_land($id, $data) : $this->load->view('backoffice/land/formadmin',
            array(
                'title' => 'เพิ่มข้อมูลที่ดิน',
                'header' => 'yes',
                'googlemap' => 'yes',
                'css' => array(
                        'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
                        'assets/css/dropzone.min.css', ),
                'js' => array(
                        'assets/plugins/tinymce/tinymce.min.js',
                        'assets/js/dropzone.min.js',
                        'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
                        'assets/pages/jquery.sweet-alert.init.js', ),
                'wysiwigeditor' => 'wysiwigeditor',
                'provinces' => $provinces,
                'staff_list' => $staff_list,
                'type_property' => $type_property,
                'type_zone' => $type_zone,
                'type_offering' => $type_offering,
                'transportation_category' => $transportation_category,
                'member' => $member, )
                                         );
    }
        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        public function edit($id = 0)
        {
            $row = $this->Land_model->get_info($id);
            $provinces = $this->M->get_provinces();
            //$districts 		= $this->Member->get_districts();
            //$subdistricts = $this->Member->get_subdistricts();
            $staff_list = $this->User_model->get_staff_option();
            $type_property = $this->Configsystem->get_type_property_option();
            $type_zone = $this->Configsystem->get_type_zone_option();
            $type_offering = $this->Configsystem->get_type_offering_option();
            $transportation_category = $this->Condominium_model->get_transportation_category_option();
            $transportation = $this->Condominium_model->get_transportation_option();
            $land_relation_transportation = $this->Land_model->get_transportation_relation_row_by_land($id);

            $member = $this->Member->get_fullname();

            if ($this->input->post('action')) {
                $sfilepath = './uploads/land/.';
                $smaxsize = 4096000; //4MB
                $smaxwidth_pic_thumb = 2000;
                $smaxheight_pic_thumb = 2000;

                $smaxwidth_pic_large = 2000;
                $smaxheight_pic_large = 2000;

                // PIC THUMB===========================================================
                if ($_FILES['pic_thumb']) {
                    $pic_thumb = $this->primaryclass->do_upload('pic_thumb', $sfilepath, $smaxsize, $smaxwidth_pic_thumb, $smaxheight_pic_thumb);
                    if ($pic_thumb['upload_data']['file_name'] == '') {
                        $pic_thumb_have_file = $this->input->post('old_pic_thumb');
                    } else {
                        $pic_thumb_have_file = $pic_thumb['upload_data']['file_name'];
                    }
                } else {
                    $pic_thumb_have_file = '';
                }//IF PIC THUMB=========================================================

                // PIC LARGE=============================================================
                if ($_FILES['pic_large']) {
                    $pic_large = $this->primaryclass->do_upload('pic_large', $sfilepath, $smaxsize, $smaxwidth_pic_large, $smaxheight_pic_large);
                    if ($pic_large['upload_data']['file_name'] == '') {
                        $pic_large_have_file = $this->input->post('old_pic_large');
                    } else {
                        $pic_large_have_file = $pic_large['upload_data']['file_name'];
                    }
                } else {
                    $pic_large_have_file = '';
                }//IF PIC LARGE==========================================================

                $data = $this->security->xss_clean($this->input->post());
                $data = array(
                    'land_id' => $data['land_id'],
                    'land_property_id' => $data['land_property_id'],
                    'member_id' => $data['member_id'],
                    'staff_id' => $data['staff_id'],
                    'land_title' => $data['land_title'],
                    'land_owner_name' => $data['land_owner_name'],
                    'land_holder_name' => $data['land_holder_name'],
                    'property_type_id' => $data['property_type_id'],
                    'type_zone_id' => $data['type_zone_id'],
                    'zone_id' => $data['zone_id'],
                    'type_offering_id' => $data['type_offering_id'],
                    'land_expert' => trim($data['land_expert']),
                    'land_description' => trim($data['land_description']),
                    'pic_thumb' => $pic_thumb_have_file,
                    'pic_large' => $pic_large_have_file,
                    //'land_no' => $data['land_no'],
                    //'land_alley' => $data['land_alley'],
                    //'land_road' => $data['land_road'],
                    //'land_address' => trim($data['land_address']),
                    'provinces_id' => $data['provinces_id'],
                    'districts_id' => $data['districts_id'],
                    'sub_districts_id' => $data['sub_districts_id'],
                    'land_zipcode' => $data['land_zipcode'],
                    // 'land_size' => $data['land_size'],
                    // 'land_size_unit' => $data['land_size_unit'],
                    'land_total_price' => $data['land_total_price'],
                    'land_price_per_square_meter' => $data['land_price_per_square_meter'],
                    'land_price_per_square_mortgage' => $data['land_price_per_square_mortgage'],
                    //'land_googlemaps' => trim($data['land_googlemaps']),
                    'latitude' => trim($data['latitude']),
                    'longitude' => trim($data['longitude']),
                    'land_tag' => $data['land_tag'],
                    'createdate' => date('Y-m-d H:i:s'),
                    'visited' => 0,
                    'type_status_id' => 0,
                    'newsarrival' => $data['newsarrival'],
                    'highlight' => $data['highlight'],
                    'approved' => $data['approved'],
                    'land_size_square_rai' => $data['land_size_square_rai'],
                    'land_size_square_ngan' => $data['land_size_square_ngan'],
                    'land_size_square_yard' => $data['land_size_square_yard'],
                );
            }

            if ($this->input->post('action')) {
                // $this->primaryclass->pre_var_dump($data);
                $id = $data['land_id'];
                $this->Land_model->set_land($id, $data);
            } else {
                # code...
                $this->load->view('backoffice/land/formadmin',
                    array(
                        'title' => 'แก้ไขข้อมูลที่ดิน',
                        'googlemap' => 'yes',
                        'header' => 'yes',
                        'url_href' => 'edit',
                        'css' => array(
                                'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
                                'assets/css/dropzone.min.css',
                        ),
                        'js' => array(
                                'assets/plugins/tinymce/tinymce.min.js',
                                'assets/js/dropzone.min.js',
                                'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
                                'assets/pages/jquery.sweet-alert.init.js',
                        ),
                        'wysiwigeditor' => 'wysiwigeditor',
                        'data' => $row,
                        'provinces' => $provinces,
                        'type_property' => $type_property,
                        'type_zone' => $type_zone,
                        'type_offering' => $type_offering,
                        'transportation_category' => $transportation_category,
                        'transportation' => $transportation,
                        'member' => $member,
                        'staff_list' => $staff_list,
                        'land_relation_transportation' => $land_relation_transportation,
                ));
            }
        }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        public function approved_land($id = 0)
        {
            $data = $this->security->xss_clean($this->input->post());
            $data = array(
                'land_id' => $data['land_id'],
                'approved' => $data['approved'],
                'modifydate' => date('Y-m-d H:i:s'),
            );

            $approved = $this->Land_model->set_land_by_id($data);
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function approved_multi_land()
    {
        $data = $this->security->xss_clean($this->input->post());
      // $this->primaryclass->pre_var_dump($data);
      if (isset($data['land_id'])) {
          //echo "yes";
        foreach ($data as $key => $value) {
            if ($key === 'land_id') {
                foreach ($value as $land_id) {
                    $data_update = array(
                'land_id' => $land_id,
                'approved' => '2',
                'modifydate' => date('Y-m-d H:i:s'),
              );
              //$this->primaryclass->pre_var_dump($data_update);
              $approved = $this->Land_model->set_land_by_id($data_update);
                }
            }
        }
      }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function unapproved_multi_land()
    {
        $data = $this->security->xss_clean($this->input->post());
      // $this->primaryclass->pre_var_dump($data);
      if (isset($data['land_id'])) {
          //echo "yes";
        foreach ($data as $key => $value) {
            if ($key === 'land_id') {
                foreach ($value as $land_id) {
                    $data_update = array(
                'land_id' => $land_id,
                'approved' => '1',
                'modifydate' => date('Y-m-d H:i:s'),
              );
              // $this->primaryclass->pre_var_dump($data_update);
              $approved = $this->Land_model->set_land_by_id($data_update);
                }
            }
        }
      }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

      public function delete_land($id)
      {
          $delete = $this->Land_model->delete_record('land_id', $id, 'land');
      }
      //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function delete_multi_land()
    {
        $data = $this->security->xss_clean($this->input->post());
      // $this->primaryclass->pre_var_dump($data);
      if (isset($data['land_id'])) {
          //echo "yes";
        foreach ($data as $key => $value) {
            if ($key === 'land_id') {
                foreach ($value as $land_id) {
                    $delete = $this->Land_model->delete_record('land_id', $land_id, 'land');
                }
            }
        }
      }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //call to fill the second dropdown with the provinces
    public function transportation_category_option()
    {
        //set selected country id from POST
        $transportation_category_id = $this->input->post('id', true);

        $data['transportation_category'] = $this->Condominium_model->get_transportation_category_option();

        //echo count($data['transportation']);
        $output = null;

        $output .= '<div class="form-group">';
        $output .= '<label class="col-md-2 control-label m-t-10">สถานที่ใกล้เคียง</label>';
        $output .= '<div class="col-sm-12 col-md-4">';
        $output .= '<select id="transportation_category_id" class="form-control" name="transportation_category_id[]" onchange="(this)">';
        foreach ($data['transportation_category'] as $row) {
            if (count($data['transportation_category']) <= 0) {
                //here we build a dropdown item line for each query result
                $output .= '<optgroup>';
                $output .= '<option value>---เลือก----</option>';
                $output .= '</optgroup>';
            } else {
                $output .= "<optgroup label='".$row->transportation_category_title."'>";

                //เอาข้อมูล Transportation by ID มา
                $data['transportation'] = $this->Condominium_model->get_transportation_option($row->transportation_category_id);
                foreach ($data['transportation'] as $row_in) {
                    //here we build a dropdown item line for each query result
                    $output .= "<option value='".$row_in->transportation_id."'>".$row_in->transportation_title.'</option>';
                }
                $output .= '</optgroup>';
            }
        }
        $output .= '</select>';
        $output .= '</div>';
        $output .= '<div class="col-sm-10 col-md-4">';
        $output .= '<input type="text" name="transportation_distance[]" class="inputmaxlength form-control" maxlength="50" placeholder="">';
        $output .= '<span class="font-13 text-muted">ตัวอย่าง : 7 นาที &#40;1.46 กิโลเมตร&#41;</span>';
        $output .= '</div>';
        $output .= '<div class="col-sm-2 col-md-2">';
        $output .= '<div class="field_wrapper">';
        $output .= '<div>';
        $output .= '<a href="javascript:void(0);" class="remove_button"><i class="remove_button fa fa-minus"></i></a>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
        echo $output;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function images($land_id = 0)
    {
        $data = $this->Land_model->get_land_image($land_id);

        $this->load->view('backoffice/land/datatable-image', array(
            'title' => 'รูปภาพที่ดินเพิ่มเติม',
            'header' => 'yes',
            'url_href' => '../add_image/'.$land_id,
            'css' => array(
                'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
                'assets/plugins/datatables/jquery.dataTables.min.css',
                'assets/plugins/datatables/buttons.bootstrap.min.css',
                'assets/plugins/datatables/fixedHeader.bootstrap.min.css',
                'assets/plugins/datatables/responsive.bootstrap.min.css',
                'assets/plugins/datatables/scroller.bootstrap.min.css',
            ),
            'js' => array(
                'assets/plugins/tinymce/tinymce.min.js',
                'assets/js/dropzone.min.js',
                'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
                'assets/pages/jquery.sweet-alert.init.js',
                'assets/plugins/datatables/jquery.dataTables.min.js',
                'assets/plugins/datatables/dataTables.bootstrap.js',
                'assets/plugins/datatables/dataTables.buttons.min.js',
                'assets/plugins/datatables/buttons.bootstrap.min.js',
                'assets/plugins/datatables/jszip.min.js',
                'assets/plugins/datatables/pdfmake.min.js',
                'assets/plugins/datatables/vfs_fonts.js',
                'assets/plugins/datatables/buttons.html5.min.js',
                'assets/plugins/datatables/buttons.print.min.js',
                'assets/plugins/datatables/dataTables.fixedHeader.min.js',
                'assets/plugins/datatables/dataTables.keyTable.min.js',
                'assets/plugins/datatables/dataTables.responsive.min.js',
                'assets/plugins/datatables/responsive.bootstrap.min.js',
            ),
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_image($land_id = 0, $id = 0)
    {
        $R = $this->Staff_model->info();
        if ($this->input->post('action')) {
            //มี action หลังจากการ Submit
            $sfilepath = './uploads/land/.';
            $smaxsize = 4096000; //4MB
            $smaxwidth = 2000;
            $smaxheight = 2000;

            for ($i = 1; $i <= 5; ++$i) {
                // PIC LARGE===========================================================
                if ($_FILES['pic_large_'.$i]) {
                    $pic_large[$i] = $this->primaryclass->do_upload('pic_large_'.$i, $sfilepath, $smaxsize, $smaxwidth, $smaxheight);
                    //$this->primaryclass->pre_var_dump($pic_large[$i]['upload_data']['file_name']);

                    if (!empty($pic_large[$i])) {
                        $data = $this->security->xss_clean($this->input->post());
                        $data = array(
                            'land_id' => $this->input->post('land_id'),
                            'land_images_title' => $this->input->post('land_images_title'),
                            'land_images_description' => $this->input->post('land_images_description'),
                            'pic_large' => $pic_large[$i]['upload_data']['file_name'],
                            'land_images_url' => $this->input->post('land_images_url'),
                            'createdate' => date('Y-m-d H:i:s'),
                            'publish' => $this->input->post('publish'),
                        );
                        $insert = $this->Land_model->set_image_land(0, $data);
                    }
                } else {
                    $pic_large[$i] = '';
                }
                // END PIC LARGE=======================================================
            }
        } else {
            //ไม่มี ACTION

            $this->load->view('backoffice/land/formadmin-image', array(
                'title' => 'เพิ่มรูปภาพข้อมูลที่ดิน',
                'header' => 'yes',
                'BodyClass' => 'fixed-left',
                'wysiwigeditor' => 'wysiwigeditor',
                'css' => array(
                    'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
                    'assets/css/dropzone.min.css',
                ),
                'js' => array(
                    'assets/plugins/tinymce/tinymce.min.js',
                    'assets/js/dropzone.min.js',
                    'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
                    'assets/pages/jquery.sweet-alert.init.js',
                ),
            ));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_image($land_id = 0, $id = 0)
    {
        $land_id = $this->uri->segment(3);
        $id = $this->uri->segment(4);
        if (empty($id)) {
            show_404();
        }

        $R = $this->Staff_model->info();
        if ($this->input->post('action')) {
            //มี action หลังจากการ Submit
            $sfilepath = './uploads/land/.';
            $smaxsize = 4096000; //4MB
            $smaxwidth = 2000;
            $smaxheight = 2000;
            // PIC THUMB===========================================================
            if ($_FILES['pic_large']) {
                $pic_large = $this->primaryclass->do_upload('pic_large', $sfilepath, $smaxsize, $smaxwidth, $smaxheight);

                if ($pic_large['upload_data']['file_name'] == '') {
                    $pic_large_have_file = $this->input->post('old_pic_large');
                } else {
                    $pic_large_have_file = $pic_large['upload_data']['file_name'];
                }
            } else {
                $pic_large_have_file = '';
            }//IF PIC THUMB=========================================================

            $data = $this->security->xss_clean($this->input->post());
            $this->primaryclass->pre_var_dump($data);
            $data = array(
                'land_id' => $data['land_id'],
                'land_images_title' => $data['land_images_title'],
                'land_images_description' => $data['land_images_description'],
                'pic_large' => $pic_large_have_file,
                'updatedate' => date('Y-m-d H:i:s'),
                'publish' => $this->input->post('publish'),
            );
            $this->primaryclass->pre_var_dump($data);
            echo $update = $this->Land_model->set_image_land($id, $data);
        } else {
            $data = array();
            $row = $this->Land_model->get_land_image_by_id($land_id, $id);

            $data['row'] = $row;
            //ไม่มี ACTION
            $this->load->view('backoffice/land/formadmin-image', array(
                'title' => 'แก้ไขรูปภาพข้อมูลที่ดิน',
                'header' => 'yes',
                'BodyClass' => 'fixed-left',
                'wysiwigeditor' => 'wysiwigeditor',
                'css' => array(
                    'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
                    'assets/css/dropzone.min.css',
                ),
                'js' => array(
                    'assets/plugins/tinymce/tinymce.min.js',
                    'assets/js/dropzone.min.js',
                    'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
                    'assets/pages/jquery.sweet-alert.init.js',
                ),
                'data' => $data,
            ));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function remove_land_images_update()
    {
        $data = $this->security->xss_clean($this->input->post());
        $data = array(
            'land_images_id' => $data['land_images_id'],
            $this->input->post('field_name') => null,
        );
            //$this->primaryclass->pre_var_dump($data);

        $sfilepath = 'uploads/land/';
        $imagefile = $sfilepath.$this->input->post('imagefile');
        $this->M->update_record_dalete_image($data, $imagefile, 'land_images');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function display_land_images($id)
    {
        $data = array(
            'land_images_id' => $this->input->post('land_images_id'),
            'publish' => 2,
            'updatedate' => date('Y-m-d H:i:s'),
        );

        //echo $this->primaryclass->pre_var_dump($_POST['id']);
        $publish = $this->Land_model->update_land_images($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function undisplay_land_images($id)
    {
        $data = array(
            'land_images_id' => $this->input->post('land_images_id'),
            'publish' => 1,
            'updatedate' => date('Y-m-d H:i:s'),
        );
        //echo $_POST['land_images_id'];
        //echo $this->primaryclass->pre_var_dump($data);
        $publish = $this->Land_model->update_land_images($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function share($id)
    {
        $data['data'] = $this->Land_model->get_share_land($id);
        $data['type_status'] = $this->Configsystem->get_type_status_option();

        $this->load->view('backoffice/land/datatable-share', array(
            'title' => 'การแชร์ที่ดิน',
            'header' => 'yes',
            'css' => array(
                'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
                'assets/plugins/datatables/jquery.dataTables.min.css',
                'assets/plugins/datatables/buttons.bootstrap.min.css',
                'assets/plugins/datatables/fixedHeader.bootstrap.min.css',
                'assets/plugins/datatables/responsive.bootstrap.min.css',
                'assets/plugins/datatables/scroller.bootstrap.min.css',
                'assets/plugins/custombox/dist/custombox.min.css',
            ),
            'js' => array(
                'assets/plugins/tinymce/tinymce.min.js',
                'assets/js/dropzone.min.js',
                'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
                'assets/pages/jquery.sweet-alert.init.js',
                'assets/plugins/datatables/jquery.dataTables.min.js',
                'assets/plugins/datatables/dataTables.bootstrap.js',
                'assets/plugins/datatables/dataTables.buttons.min.js',
                'assets/plugins/datatables/buttons.bootstrap.min.js',
                'assets/plugins/datatables/jszip.min.js',
                'assets/plugins/datatables/pdfmake.min.js',
                'assets/plugins/datatables/vfs_fonts.js',
                'assets/plugins/datatables/buttons.html5.min.js',
                'assets/plugins/datatables/buttons.print.min.js',
                'assets/plugins/datatables/dataTables.fixedHeader.min.js',
                'assets/plugins/datatables/dataTables.keyTable.min.js',
                'assets/plugins/datatables/dataTables.responsive.min.js',
                'assets/plugins/datatables/responsive.bootstrap.min.js',
                'assets/plugins/custombox/dist/custombox.min.js',
                'assets/plugins/custombox/dist/legacy.min.js',
            ),
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function share_with_contact($id = 0, $member_share_id = 0)
    {
        //$data= $this->Land_model->get_share_condo($id);
        $data['data'] = $this->Land_model->get_land_contact_list($id, $member_share_id);
        $data['type_status'] = $this->Configsystem->get_type_status_option();

        $this->load->view('backoffice/land/datatable-share-form', array(
            'title' => 'การแชร์ที่ดินที่ได้รับการติดต่อ',
            'header' => 'yes',
            'url_href' => 'add',
            'css' => array(
                'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
                'assets/plugins/datatables/jquery.dataTables.min.css',
                'assets/plugins/datatables/buttons.bootstrap.min.css',
                'assets/plugins/datatables/fixedHeader.bootstrap.min.css',
                'assets/plugins/datatables/responsive.bootstrap.min.css',
                'assets/plugins/datatables/scroller.bootstrap.min.css',
                'assets/plugins/custombox/dist/custombox.min.css',
            ),
            'js' => array(
                'assets/plugins/tinymce/tinymce.min.js',
                'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
                'assets/pages/jquery.sweet-alert.init.js',
                'assets/plugins/datatables/jquery.dataTables.min.js',
                'assets/plugins/datatables/dataTables.bootstrap.js',
                'assets/plugins/datatables/dataTables.buttons.min.js',
                'assets/plugins/datatables/buttons.bootstrap.min.js',
                'assets/plugins/datatables/jszip.min.js',
                'assets/plugins/datatables/pdfmake.min.js',
                'assets/plugins/datatables/vfs_fonts.js',
                'assets/plugins/datatables/buttons.html5.min.js',
                'assets/plugins/datatables/buttons.print.min.js',
                'assets/plugins/datatables/dataTables.fixedHeader.min.js',
                'assets/plugins/datatables/dataTables.keyTable.min.js',
                'assets/plugins/datatables/dataTables.responsive.min.js',
                'assets/plugins/datatables/responsive.bootstrap.min.js',
                'assets/plugins/custombox/dist/custombox.min.js',
                'assets/plugins/custombox/dist/legacy.min.js',
            ),
            'data' => $data,
        ));
    //		$this->output->enable_profiler(TRUE);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_modal_status_share_form()
    {
        //Load Library
        $this->load->library('email');

        $data = $this->security->xss_clean($this->input->post());

        //$this->primaryclass->pre_var_dump($data);

        if (!empty($data)) {
            if ($this->input->post('action') === 'form-share') {
                $data = array(
                    'land_share_form_id' => $data['land_share_form_id'],
                    'fk_land_id' => $data['fk_land_id'],
                    'fk_member_id' => $data['fk_member_id'],
                    'fk_type_status_id' => $data['fk_type_status_id'],
                );

                $update = $this->Land_model->update_land_share_form_status($data);
                if ($update) {

                    ///////// EMAIL /////////

                    $fk_land_id = $data['fk_land_id'];
                    $fk_member_id = $data['fk_member_id'];
                    //สถานะ
                    $fk_type_status_id = (!empty($data['fk_type_status_id'])) ? $data['fk_type_status_id'] : '';
                    $fk_type_status_title = (!empty($fk_type_status_id)) ? $this->Configsystem->get_type_status_title($fk_type_status_id) : '';

                    //1. เจ้าของ Stock
                    $data_member_owner = $this->Land_model->get_member_owner_land($fk_land_id);
                    $land_property_id = (!empty($data_member_owner->land_property_id)) ? $data_member_owner->land_property_id : '';
                    $data_member_owner_fname = (!empty($data_member_owner->member_fname)) ? $data_member_owner->member_fname : '';
                    $data_member_owner_lname = (!empty($data_member_owner->member_lname)) ? $data_member_owner->member_lname : '';
                    $data_member_owner_email = (!empty($data_member_owner->member_email)) ? $data_member_owner->member_email : '';
                    $data_member_owner_mobileno = (!empty($data_member_owner->member_mobileno)) ? $data_member_owner->member_mobileno : '';

                    //2. ผู้ที่สนใจ (คนที่เมล์ติดต่อมา)
                    $data_member_share_land = $this->Land_model->get_land_contact_by_id($fk_land_id, $fk_member_id);
                    $land_share_form_name = (!empty($data_member_share_land->land_share_form_name)) ? $data_member_share_land->land_share_form_name : '';
                    $land_share_form_phone = (!empty($data_member_share_land->land_share_form_phone)) ? $data_member_share_land->land_share_form_phone : '';
                    $land_share_form_email = (!empty($data_member_share_land->land_share_form_email)) ? $data_member_share_land->land_share_form_email : '';
                    $land_share_form_message = (!empty($data_member_share_land->land_share_form_message)) ? $data_member_share_land->land_share_form_message : '';

                    //3. ผู้ที่ทำการแชร์ (HOB)
                    //$this->primaryclass->pre_var_dump($data_member_share_land);
                    $data_member_share_land_fname = (!empty($data_member_share_land->member_fname)) ? $data_member_share_land->member_fname : '';
                    $data_member_share_land_lname = (!empty($data_member_share_land->member_lname)) ? $data_member_share_land->member_lname : '';
                    $data_member_share_land_email = (!empty($data_member_share_land->member_email)) ? $data_member_share_land->member_email : '';
                    $data_member_share_land_mobileno = (!empty($data_member_share_land->member_mobileno)) ? $data_member_share_land->member_mobileno : '';

                    //4. Admin
                    $site_id = 1;
                    $site_setting = $this->M->get_main_setting($site_id);

                    $website_name = base_url();
                    $site_logo = (!empty($site_setting['site_logo'])) ? $site_setting['site_logo'] : $website_name.'assets/images/logo.jpg';
                    $site_email = (!empty($site_setting['site_email'])) ? $site_setting['site_email'] : 'hawk@gmail.com, wut@hosting.co.th, tee.emilond@gmail.com';

                    //ข้อความของสถานะอีเมล์
                    if ($fk_type_status_id === 2) {
                        //โทรนัด, มีคนสนใจคอนโด

                        //1. หัวข้อ และข้อความ เจ้าของ Stock
                        $subject_for_land_owner = "แจ้งเตือน ประกาศ รหัสทรัพย์ $land_property_id";
                        $message_for_land_owner = "<h4>สวัสดีคุณ, $data_member_owner_fname $data_member_owner_lname </h4>\n";
                        $message_for_land_owner .= "<p>ขณะนี้ มีผู้สนใจประกาศ รหัสทรัพย์ '$land_property_id' ของท่าน</p>\n";
                        $message_for_land_owner .= "<hr>\n";
                        $message_for_land_owner .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_land_owner .= "<hr>\n";

                        $data_email_for_land_owner = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_land_owner,
                            'email_message' => $message_for_land_owner,
                            'email' => $data_member_owner_email,
                        );
                        $success_email_for_land_owner = $this->email_send_form_status($data_email_for_land_owner);

                        //2. หัวข้อ และข้อความ ผู้ที่สนใจ (คนที่เมล์ติดต่อมา)
                        $subject_for_land_contact = "แจ้งเตือน ประกาศ รหัสทรัพย์ $land_property_id";
                        $message_for_land_contact = "<h4>เรียนคุณ, $land_share_form_name </h4>\n";
                        $message_for_land_contact .= "<p>ขอบคุณครับ คุณ $land_share_form_name เจ้าหน้าที่ของเราจะรีบติดต่อกลับท่านโดยเร็วที่สุด</p>\n";
                        $message_for_land_contact .= "<hr>\n";
                        $message_for_land_contact .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_land_contact .= "<hr>\n";

                        $data_email_for_land_contact = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_land_contact,
                            'email_message' => $message_for_land_contact,
                            'email' => $land_share_form_email,
                        );
                        $success_email_for_land_contact = $this->email_send_form_status($data_email_for_land_contact);

                        //3. หัวข้อ และข้อความ ผู้ที่ทำการแชร์ (HOB)
                        $subject_for_land_share = "แจ้งเตือน ประกาศ รหัสทรัพย์ $land_property_id";
                        $message_for_land_share = "<h4>เรียนคุณ, $data_member_share_land_fname </h4>\n";
                        $message_for_land_share .= "<p><p>ขณะนี้ มีผู้สนใจ ทรัพย์รหัส '$land_property_id' ที่ท่านได้แชร์ออกไป</p>\n";
                        $message_for_land_share .= "<hr>\n";
                        $message_for_land_share .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_land_share .= "<hr>\n";

                        $data_email_for_land_share = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_land_share,
                            'email_message' => $message_for_land_share,
                            'email' => $data_member_share_land_email,
                        );
                        $success_email_for_land_share = $this->email_send_form_status($data_email_for_land_share);

                        //4. แอดมิน
                        $subject_for_land_admin = "แจ้งเตือน ประกาศ รหัสทรัพย์ $land_property_id ผ่านทางเว็บไซต์";
                        $message_for_land_admin = "<h4>เรียนคุณ, ผู้ดูแลระบบ </h4>\n";
                        $message_for_land_admin .= "<p><p>ขณะนี้ มีผู้สนใจ ทรัพย์รหัส '$land_property_id' ทางเว็บไซต์</p>\n";
                        $message_for_land_admin .= "<hr>\n";
                        $message_for_land_admin .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_land_admin .= "<hr>\n";
                        $message_for_land_admin .= "<h6>ผู้แชร์</h6>\n";
                        $message_for_land_admin .= "<p><storng>ชื่อผู้แชร์ : </storng> $data_member_share_land_fname $data_member_share_land_lname</p>\n";
                        $message_for_land_admin .= "<p><storng>อีเมล์ : </storng> $data_member_owner_email</p>\n";
                        $message_for_land_admin .= "<p><storng>เบอร์ติดต่อ : </storng> $data_member_share_land_mobileno</p>\n";
                        $message_for_land_admin .= "<hr>\n";
                        $message_for_land_admin .= "<h6>ผู้สนใจ</h6>\n";
                        $message_for_land_admin .= "<p><storng>ชื่อผู้สนใจ : </storng> $land_share_form_name</p>\n";
                        $message_for_land_admin .= "<p><storng>อีเมล์ : </storng> $land_share_form_email</p>\n";
                        $message_for_land_admin .= "<p><storng>เบอร์ติดต่อ : </storng> $land_share_form_phone</p>\n";
                        $message_for_land_admin .= "<p><storng>ข้อความ : </storng> $land_share_form_message</p>\n";
                        $message_for_land_admin .= "<hr>\n";

                        $data_email_for_land_admin = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_land_admin,
                            'email_message' => $message_for_land_admin,
                            'email' => $site_email,
                        );
                        $success_email_for_land_share = $this->email_send_form_status($data_email_for_land_admin);
                    }
                    // END IF

                    else {
                        //นัดดูสถานที่ | //เสนอราคา / ต่อรองราคา | //ทำสัญญา | //นัดวันทำสัญญา | //ซื้อขายสมบูรณ์

                        //1. หัวข้อ และข้อความ เจ้าของ Stock
                        $subject_for_land_owner = "แจ้งเตือน ประกาศ รหัสทรัพย์ $land_property_id";
                        $message_for_land_owner = "<h4>สวัสดีคุณ, $data_member_owner_fname $data_member_owner_lname </h4>\n";
                        $message_for_land_owner .= "<p>ขณะนี้ มีผู้สนใจประกาศ รหัสทรัพย์ '$land_property_id' ของท่าน</p>\n";
                        $message_for_land_owner .= "<hr>\n";
                        $message_for_land_owner .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_land_owner .= "<hr>\n";

                        $data_email_for_land_owner = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_land_owner,
                            'email_message' => $message_for_land_owner,
                            'email' => $data_member_owner_email,
                        );
                        $success_email_for_land_owner = $this->email_send_form_status($data_email_for_land_owner);

                        //2. หัวข้อ และข้อความ ผู้ที่สนใจ (คนที่เมล์ติดต่อมา)
                        $subject_for_land_contact = "แจ้งเตือน ประกาศ รหัสทรัพย์ $land_property_id";
                        $message_for_land_contact = "<h4>เรียนคุณ, $land_share_form_name </h4>\n";
                        $message_for_land_contact .= "<p>ขอบคุณครับ คุณ $land_share_form_name เจ้าหน้าที่ของเราจะรีบติดต่อกลับท่านโดยเร็วที่สุด</p>\n";
                        $message_for_land_contact .= "<hr>\n";
                        $message_for_land_contact .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_land_contact .= "<hr>\n";

                        $data_email_for_land_contact = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_land_contact,
                            'email_message' => $message_for_land_contact,
                            'email' => $land_share_form_email,
                        );
                        $success_email_for_land_contact = $this->email_send_form_status($data_email_for_land_contact);

                        //3. หัวข้อ และข้อความ ผู้ที่ทำการแชร์ (HOB)
                        $subject_for_land_share = "แจ้งเตือน ประกาศ รหัสทรัพย์ $land_property_id";
                        $message_for_land_share = "<h4>เรียนคุณ, $data_member_share_land_fname $data_member_share_land_lname </h4>\n";
                        $message_for_land_share .= "<p><p>ขณะนี้ มีผู้สนใจ ทรัพย์รหัส '$land_property_id' ที่ท่านได้แชร์ออกไป</p>\n";
                        $message_for_land_share .= "<hr>\n";
                        $message_for_land_share .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_land_share .= "<hr>\n";

                        $data_email_for_land_share = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_land_share,
                            'email_message' => $message_for_land_share,
                            'email' => $data_member_share_land_email,
                        );
                        $success_email_for_land_share = $this->email_send_form_status($data_email_for_land_share);

                        //4. แอดมิน
                        $subject_for_land_admin = "แจ้งเตือน ประกาศ รหัสทรัพย์ $land_property_id ผ่านทางเว็บไซต์";
                        $message_for_land_admin = "<h4>เรียนคุณ, ผู้ดูแลระบบ </h4>\n";
                        $message_for_land_admin .= "<p><p>ขณะนี้ มีผู้สนใจ ทรัพย์รหัส '$land_property_id' ทางเว็บไซต์</p>\n";
                        $message_for_land_admin .= "<hr>\n";
                        $message_for_land_admin .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_land_admin .= "<hr>\n";

                        $data_email_for_land_admin = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_land_admin,
                            'email_message' => $message_for_land_admin,
                            'email' => $site_email,
                        );
                        $success_email_for_land_share = $this->email_send_form_status($data_email_for_land_admin);
                    }
                }
            }//END IF
        }//END IF
        //return $data;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //EMAIL TEMPLATE
    public function email_send_form_status($data)
    {
        $subject = $data['email_subject'];
        $message = $data['email_message'];
        $site_logo = $data['email_logo'];

        //
        // Get full html:
        $body = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
        $body .=  "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n";
        $body .=  "\n";
        $body .=  "  <head>\n";
        $body .=  "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n";
        $body .=  "    <meta name=\"viewport\" content=\"width=device-width\">\n";
        $body .=  "    <title>Condo Compare</title>\n";
        $body .=  "    <style>\n";
        $body .=  "      .wrapper {\n";
        $body .=  "  width: 100%; }\n";
        $body .=  "\n";
        $body .=  "#outlook a {\n";
        $body .=  "  padding: 0; }\n";
        $body .=  "\n";
        $body .=  "body {\n";
        $body .=  "  width: 100% !important;\n";
        $body .=  "  min-width: 100%;\n";
        $body .=  "  -webkit-text-size-adjust: 100%;\n";
        $body .=  "  -ms-text-size-adjust: 100%;\n";
        $body .=  "  margin: 0;\n";
        $body .=  "  Margin: 0;\n";
        $body .=  "  padding: 0;\n";
        $body .=  "  -moz-box-sizing: border-box;\n";
        $body .=  "  -webkit-box-sizing: border-box;\n";
        $body .=  "  box-sizing: border-box; }\n";
        $body .=  "\n";
        $body .=  ".ExternalClass {\n";
        $body .=  "  width: 100%; }\n";
        $body .=  "  .ExternalClass,\n";
        $body .=  "  .ExternalClass p,\n";
        $body .=  "  .ExternalClass span,\n";
        $body .=  "  .ExternalClass font,\n";
        $body .=  "  .ExternalClass td,\n";
        $body .=  "  .ExternalClass div {\n";
        $body .=  "    line-height: 100%; }\n";
        $body .=  "\n";
        $body .=  "#backgroundTable {\n";
        $body .=  "  margin: 0;\n";
        $body .=  "  Margin: 0;\n";
        $body .=  "  padding: 0;\n";
        $body .=  "  width: 100% !important;\n";
        $body .=  "  line-height: 100% !important; }\n";
        $body .=  "\n";
        $body .=  "img {\n";
        $body .=  "  outline: none;\n";
        $body .=  "  text-decoration: none;\n";
        $body .=  "  -ms-interpolation-mode: bicubic;\n";
        $body .=  "  width: auto;\n";
        $body .=  "  max-width: 100%;\n";
        $body .=  "  clear: both;\n";
        $body .=  "  display: block; }\n";
        $body .=  "\n";
        $body .=  "center {\n";
        $body .=  "  width: 100%;\n";
        $body .=  "  min-width: 580px; }\n";
        $body .=  "\n";
        $body .=  "a img {\n";
        $body .=  "  border: none; }\n";
        $body .=  "\n";
        $body .=  "p {\n";
        $body .=  "  margin: 0 0 0 10px;\n";
        $body .=  "  Margin: 0 0 0 10px; }\n";
        $body .=  "\n";
        $body .=  "table {\n";
        $body .=  "  border-spacing: 0;\n";
        $body .=  "  border-collapse: collapse; }\n";
        $body .=  "\n";
        $body .=  "td {\n";
        $body .=  "  word-wrap: break-word;\n";
        $body .=  "  -webkit-hyphens: auto;\n";
        $body .=  "  -moz-hyphens: auto;\n";
        $body .=  "  hyphens: auto;\n";
        $body .=  "  border-collapse: collapse !important; }\n";
        $body .=  "\n";
        $body .=  "table, tr, td {\n";
        $body .=  "  padding: 0;\n";
        $body .=  "  vertical-align: top;\n";
        $body .=  "  text-align: left; }\n";
        $body .=  "\n";
        $body .=  "@media only screen {\n";
        $body .=  "  html {\n";
        $body .=  "    min-height: 100%;\n";
        $body .=  "    background: #f3f3f3; } }\n";
        $body .=  "\n";
        $body .=  "table.body {\n";
        $body .=  "  background: #f3f3f3;\n";
        $body .=  "  height: 100%;\n";
        $body .=  "  width: 100%; }\n";
        $body .=  "\n";
        $body .=  "table.container {\n";
        $body .=  "  background: #fefefe;\n";
        $body .=  "  width: 580px;\n";
        $body .=  "  margin: 0 auto;\n";
        $body .=  "  Margin: 0 auto;\n";
        $body .=  "  text-align: inherit; }\n";
        $body .=  "\n";
        $body .=  "table.row {\n";
        $body .=  "  padding: 0;\n";
        $body .=  "  width: 100%;\n";
        $body .=  "  position: relative; }\n";
        $body .=  "\n";
        $body .=  "table.spacer {\n";
        $body .=  "  width: 100%; }\n";
        $body .=  "  table.spacer td {\n";
        $body .=  "    mso-line-height-rule: exactly; }\n";
        $body .=  "\n";
        $body .=  "table.container table.row {\n";
        $body .=  "  display: table; }\n";
        $body .=  "\n";
        $body .=  "td.columns,\n";
        $body .=  "td.column,\n";
        $body .=  "th.columns,\n";
        $body .=  "th.column {\n";
        $body .=  "  margin: 0 auto;\n";
        $body .=  "  Margin: 0 auto;\n";
        $body .=  "  padding-left: 16px;\n";
        $body .=  "  padding-bottom: 16px; }\n";
        $body .=  "  td.columns .column,\n";
        $body .=  "  td.columns .columns,\n";
        $body .=  "  td.column .column,\n";
        $body .=  "  td.column .columns,\n";
        $body .=  "  th.columns .column,\n";
        $body .=  "  th.columns .columns,\n";
        $body .=  "  th.column .column,\n";
        $body .=  "  th.column .columns {\n";
        $body .=  "    padding-left: 0 !important;\n";
        $body .=  "    padding-right: 0 !important; }\n";
        $body .=  "    td.columns .column center,\n";
        $body .=  "    td.columns .columns center,\n";
        $body .=  "    td.column .column center,\n";
        $body .=  "    td.column .columns center,\n";
        $body .=  "    th.columns .column center,\n";
        $body .=  "    th.columns .columns center,\n";
        $body .=  "    th.column .column center,\n";
        $body .=  "    th.column .columns center {\n";
        $body .=  "      min-width: none !important; }\n";
        $body .=  "\n";
        $body .=  "td.columns.last,\n";
        $body .=  "td.column.last,\n";
        $body .=  "th.columns.last,\n";
        $body .=  "th.column.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.columns table:not(.button),\n";
        $body .=  "td.column table:not(.button),\n";
        $body .=  "th.columns table:not(.button),\n";
        $body .=  "th.column table:not(.button) {\n";
        $body .=  "  width: 100%; }\n";
        $body .=  "\n";
        $body .=  "td.large-1,\n";
        $body .=  "th.large-1 {\n";
        $body .=  "  width: 32.33333px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-1.first,\n";
        $body .=  "th.large-1.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-1.last,\n";
        $body .=  "th.large-1.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-1,\n";
        $body .=  ".collapse > tbody > tr > th.large-1 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 48.33333px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-1.first,\n";
        $body .=  ".collapse th.large-1.first,\n";
        $body .=  ".collapse td.large-1.last,\n";
        $body .=  ".collapse th.large-1.last {\n";
        $body .=  "  width: 56.33333px; }\n";
        $body .=  "\n";
        $body .=  "td.large-1 center,\n";
        $body .=  "th.large-1 center {\n";
        $body .=  "  min-width: 0.33333px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-1,\n";
        $body .=  ".body .column td.large-1,\n";
        $body .=  ".body .columns th.large-1,\n";
        $body .=  ".body .column th.large-1 {\n";
        $body .=  "  width: 8.33333%; }\n";
        $body .=  "\n";
        $body .=  "td.large-2,\n";
        $body .=  "th.large-2 {\n";
        $body .=  "  width: 80.66667px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-2.first,\n";
        $body .=  "th.large-2.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-2.last,\n";
        $body .=  "th.large-2.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-2,\n";
        $body .=  ".collapse > tbody > tr > th.large-2 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 96.66667px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-2.first,\n";
        $body .=  ".collapse th.large-2.first,\n";
        $body .=  ".collapse td.large-2.last,\n";
        $body .=  ".collapse th.large-2.last {\n";
        $body .=  "  width: 104.66667px; }\n";
        $body .=  "\n";
        $body .=  "td.large-2 center,\n";
        $body .=  "th.large-2 center {\n";
        $body .=  "  min-width: 48.66667px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-2,\n";
        $body .=  ".body .column td.large-2,\n";
        $body .=  ".body .columns th.large-2,\n";
        $body .=  ".body .column th.large-2 {\n";
        $body .=  "  width: 16.66667%; }\n";
        $body .=  "\n";
        $body .=  "td.large-3,\n";
        $body .=  "th.large-3 {\n";
        $body .=  "  width: 129px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-3.first,\n";
        $body .=  "th.large-3.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-3.last,\n";
        $body .=  "th.large-3.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-3,\n";
        $body .=  ".collapse > tbody > tr > th.large-3 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 145px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-3.first,\n";
        $body .=  ".collapse th.large-3.first,\n";
        $body .=  ".collapse td.large-3.last,\n";
        $body .=  ".collapse th.large-3.last {\n";
        $body .=  "  width: 153px; }\n";
        $body .=  "\n";
        $body .=  "td.large-3 center,\n";
        $body .=  "th.large-3 center {\n";
        $body .=  "  min-width: 97px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-3,\n";
        $body .=  ".body .column td.large-3,\n";
        $body .=  ".body .columns th.large-3,\n";
        $body .=  ".body .column th.large-3 {\n";
        $body .=  "  width: 25%; }\n";
        $body .=  "\n";
        $body .=  "td.large-4,\n";
        $body .=  "th.large-4 {\n";
        $body .=  "  width: 177.33333px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-4.first,\n";
        $body .=  "th.large-4.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-4.last,\n";
        $body .=  "th.large-4.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-4,\n";
        $body .=  ".collapse > tbody > tr > th.large-4 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 193.33333px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-4.first,\n";
        $body .=  ".collapse th.large-4.first,\n";
        $body .=  ".collapse td.large-4.last,\n";
        $body .=  ".collapse th.large-4.last {\n";
        $body .=  "  width: 201.33333px; }\n";
        $body .=  "\n";
        $body .=  "td.large-4 center,\n";
        $body .=  "th.large-4 center {\n";
        $body .=  "  min-width: 145.33333px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-4,\n";
        $body .=  ".body .column td.large-4,\n";
        $body .=  ".body .columns th.large-4,\n";
        $body .=  ".body .column th.large-4 {\n";
        $body .=  "  width: 33.33333%; }\n";
        $body .=  "\n";
        $body .=  "td.large-5,\n";
        $body .=  "th.large-5 {\n";
        $body .=  "  width: 225.66667px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-5.first,\n";
        $body .=  "th.large-5.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-5.last,\n";
        $body .=  "th.large-5.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-5,\n";
        $body .=  ".collapse > tbody > tr > th.large-5 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 241.66667px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-5.first,\n";
        $body .=  ".collapse th.large-5.first,\n";
        $body .=  ".collapse td.large-5.last,\n";
        $body .=  ".collapse th.large-5.last {\n";
        $body .=  "  width: 249.66667px; }\n";
        $body .=  "\n";
        $body .=  "td.large-5 center,\n";
        $body .=  "th.large-5 center {\n";
        $body .=  "  min-width: 193.66667px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-5,\n";
        $body .=  ".body .column td.large-5,\n";
        $body .=  ".body .columns th.large-5,\n";
        $body .=  ".body .column th.large-5 {\n";
        $body .=  "  width: 41.66667%; }\n";
        $body .=  "\n";
        $body .=  "td.large-6,\n";
        $body .=  "th.large-6 {\n";
        $body .=  "  width: 274px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-6.first,\n";
        $body .=  "th.large-6.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-6.last,\n";
        $body .=  "th.large-6.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-6,\n";
        $body .=  ".collapse > tbody > tr > th.large-6 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 290px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-6.first,\n";
        $body .=  ".collapse th.large-6.first,\n";
        $body .=  ".collapse td.large-6.last,\n";
        $body .=  ".collapse th.large-6.last {\n";
        $body .=  "  width: 298px; }\n";
        $body .=  "\n";
        $body .=  "td.large-6 center,\n";
        $body .=  "th.large-6 center {\n";
        $body .=  "  min-width: 242px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-6,\n";
        $body .=  ".body .column td.large-6,\n";
        $body .=  ".body .columns th.large-6,\n";
        $body .=  ".body .column th.large-6 {\n";
        $body .=  "  width: 50%; }\n";
        $body .=  "\n";
        $body .=  "td.large-7,\n";
        $body .=  "th.large-7 {\n";
        $body .=  "  width: 322.33333px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-7.first,\n";
        $body .=  "th.large-7.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-7.last,\n";
        $body .=  "th.large-7.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-7,\n";
        $body .=  ".collapse > tbody > tr > th.large-7 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 338.33333px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-7.first,\n";
        $body .=  ".collapse th.large-7.first,\n";
        $body .=  ".collapse td.large-7.last,\n";
        $body .=  ".collapse th.large-7.last {\n";
        $body .=  "  width: 346.33333px; }\n";
        $body .=  "\n";
        $body .=  "td.large-7 center,\n";
        $body .=  "th.large-7 center {\n";
        $body .=  "  min-width: 290.33333px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-7,\n";
        $body .=  ".body .column td.large-7,\n";
        $body .=  ".body .columns th.large-7,\n";
        $body .=  ".body .column th.large-7 {\n";
        $body .=  "  width: 58.33333%; }\n";
        $body .=  "\n";
        $body .=  "td.large-8,\n";
        $body .=  "th.large-8 {\n";
        $body .=  "  width: 370.66667px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-8.first,\n";
        $body .=  "th.large-8.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-8.last,\n";
        $body .=  "th.large-8.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-8,\n";
        $body .=  ".collapse > tbody > tr > th.large-8 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 386.66667px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-8.first,\n";
        $body .=  ".collapse th.large-8.first,\n";
        $body .=  ".collapse td.large-8.last,\n";
        $body .=  ".collapse th.large-8.last {\n";
        $body .=  "  width: 394.66667px; }\n";
        $body .=  "\n";
        $body .=  "td.large-8 center,\n";
        $body .=  "th.large-8 center {\n";
        $body .=  "  min-width: 338.66667px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-8,\n";
        $body .=  ".body .column td.large-8,\n";
        $body .=  ".body .columns th.large-8,\n";
        $body .=  ".body .column th.large-8 {\n";
        $body .=  "  width: 66.66667%; }\n";
        $body .=  "\n";
        $body .=  "td.large-9,\n";
        $body .=  "th.large-9 {\n";
        $body .=  "  width: 419px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-9.first,\n";
        $body .=  "th.large-9.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-9.last,\n";
        $body .=  "th.large-9.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-9,\n";
        $body .=  ".collapse > tbody > tr > th.large-9 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 435px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-9.first,\n";
        $body .=  ".collapse th.large-9.first,\n";
        $body .=  ".collapse td.large-9.last,\n";
        $body .=  ".collapse th.large-9.last {\n";
        $body .=  "  width: 443px; }\n";
        $body .=  "\n";
        $body .=  "td.large-9 center,\n";
        $body .=  "th.large-9 center {\n";
        $body .=  "  min-width: 387px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-9,\n";
        $body .=  ".body .column td.large-9,\n";
        $body .=  ".body .columns th.large-9,\n";
        $body .=  ".body .column th.large-9 {\n";
        $body .=  "  width: 75%; }\n";
        $body .=  "\n";
        $body .=  "td.large-10,\n";
        $body .=  "th.large-10 {\n";
        $body .=  "  width: 467.33333px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-10.first,\n";
        $body .=  "th.large-10.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-10.last,\n";
        $body .=  "th.large-10.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-10,\n";
        $body .=  ".collapse > tbody > tr > th.large-10 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 483.33333px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-10.first,\n";
        $body .=  ".collapse th.large-10.first,\n";
        $body .=  ".collapse td.large-10.last,\n";
        $body .=  ".collapse th.large-10.last {\n";
        $body .=  "  width: 491.33333px; }\n";
        $body .=  "\n";
        $body .=  "td.large-10 center,\n";
        $body .=  "th.large-10 center {\n";
        $body .=  "  min-width: 435.33333px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-10,\n";
        $body .=  ".body .column td.large-10,\n";
        $body .=  ".body .columns th.large-10,\n";
        $body .=  ".body .column th.large-10 {\n";
        $body .=  "  width: 83.33333%; }\n";
        $body .=  "\n";
        $body .=  "td.large-11,\n";
        $body .=  "th.large-11 {\n";
        $body .=  "  width: 515.66667px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-11.first,\n";
        $body .=  "th.large-11.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-11.last,\n";
        $body .=  "th.large-11.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-11,\n";
        $body .=  ".collapse > tbody > tr > th.large-11 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 531.66667px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-11.first,\n";
        $body .=  ".collapse th.large-11.first,\n";
        $body .=  ".collapse td.large-11.last,\n";
        $body .=  ".collapse th.large-11.last {\n";
        $body .=  "  width: 539.66667px; }\n";
        $body .=  "\n";
        $body .=  "td.large-11 center,\n";
        $body .=  "th.large-11 center {\n";
        $body .=  "  min-width: 483.66667px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-11,\n";
        $body .=  ".body .column td.large-11,\n";
        $body .=  ".body .columns th.large-11,\n";
        $body .=  ".body .column th.large-11 {\n";
        $body .=  "  width: 91.66667%; }\n";
        $body .=  "\n";
        $body .=  "td.large-12,\n";
        $body .=  "th.large-12 {\n";
        $body .=  "  width: 564px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-12.first,\n";
        $body .=  "th.large-12.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-12.last,\n";
        $body .=  "th.large-12.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-12,\n";
        $body .=  ".collapse > tbody > tr > th.large-12 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 580px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-12.first,\n";
        $body .=  ".collapse th.large-12.first,\n";
        $body .=  ".collapse td.large-12.last,\n";
        $body .=  ".collapse th.large-12.last {\n";
        $body .=  "  width: 588px; }\n";
        $body .=  "\n";
        $body .=  "td.large-12 center,\n";
        $body .=  "th.large-12 center {\n";
        $body .=  "  min-width: 532px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-12,\n";
        $body .=  ".body .column td.large-12,\n";
        $body .=  ".body .columns th.large-12,\n";
        $body .=  ".body .column th.large-12 {\n";
        $body .=  "  width: 100%; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-1,\n";
        $body .=  "td.large-offset-1.first,\n";
        $body .=  "td.large-offset-1.last,\n";
        $body .=  "th.large-offset-1,\n";
        $body .=  "th.large-offset-1.first,\n";
        $body .=  "th.large-offset-1.last {\n";
        $body .=  "  padding-left: 64.33333px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-2,\n";
        $body .=  "td.large-offset-2.first,\n";
        $body .=  "td.large-offset-2.last,\n";
        $body .=  "th.large-offset-2,\n";
        $body .=  "th.large-offset-2.first,\n";
        $body .=  "th.large-offset-2.last {\n";
        $body .=  "  padding-left: 112.66667px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-3,\n";
        $body .=  "td.large-offset-3.first,\n";
        $body .=  "td.large-offset-3.last,\n";
        $body .=  "th.large-offset-3,\n";
        $body .=  "th.large-offset-3.first,\n";
        $body .=  "th.large-offset-3.last {\n";
        $body .=  "  padding-left: 161px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-4,\n";
        $body .=  "td.large-offset-4.first,\n";
        $body .=  "td.large-offset-4.last,\n";
        $body .=  "th.large-offset-4,\n";
        $body .=  "th.large-offset-4.first,\n";
        $body .=  "th.large-offset-4.last {\n";
        $body .=  "  padding-left: 209.33333px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-5,\n";
        $body .=  "td.large-offset-5.first,\n";
        $body .=  "td.large-offset-5.last,\n";
        $body .=  "th.large-offset-5,\n";
        $body .=  "th.large-offset-5.first,\n";
        $body .=  "th.large-offset-5.last {\n";
        $body .=  "  padding-left: 257.66667px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-6,\n";
        $body .=  "td.large-offset-6.first,\n";
        $body .=  "td.large-offset-6.last,\n";
        $body .=  "th.large-offset-6,\n";
        $body .=  "th.large-offset-6.first,\n";
        $body .=  "th.large-offset-6.last {\n";
        $body .=  "  padding-left: 306px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-7,\n";
        $body .=  "td.large-offset-7.first,\n";
        $body .=  "td.large-offset-7.last,\n";
        $body .=  "th.large-offset-7,\n";
        $body .=  "th.large-offset-7.first,\n";
        $body .=  "th.large-offset-7.last {\n";
        $body .=  "  padding-left: 354.33333px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-8,\n";
        $body .=  "td.large-offset-8.first,\n";
        $body .=  "td.large-offset-8.last,\n";
        $body .=  "th.large-offset-8,\n";
        $body .=  "th.large-offset-8.first,\n";
        $body .=  "th.large-offset-8.last {\n";
        $body .=  "  padding-left: 402.66667px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-9,\n";
        $body .=  "td.large-offset-9.first,\n";
        $body .=  "td.large-offset-9.last,\n";
        $body .=  "th.large-offset-9,\n";
        $body .=  "th.large-offset-9.first,\n";
        $body .=  "th.large-offset-9.last {\n";
        $body .=  "  padding-left: 451px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-10,\n";
        $body .=  "td.large-offset-10.first,\n";
        $body .=  "td.large-offset-10.last,\n";
        $body .=  "th.large-offset-10,\n";
        $body .=  "th.large-offset-10.first,\n";
        $body .=  "th.large-offset-10.last {\n";
        $body .=  "  padding-left: 499.33333px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-11,\n";
        $body .=  "td.large-offset-11.first,\n";
        $body .=  "td.large-offset-11.last,\n";
        $body .=  "th.large-offset-11,\n";
        $body .=  "th.large-offset-11.first,\n";
        $body .=  "th.large-offset-11.last {\n";
        $body .=  "  padding-left: 547.66667px; }\n";
        $body .=  "\n";
        $body .=  "td.expander,\n";
        $body .=  "th.expander {\n";
        $body .=  "  visibility: hidden;\n";
        $body .=  "  width: 0;\n";
        $body .=  "  padding: 0 !important; }\n";
        $body .=  "\n";
        $body .=  "table.container.radius {\n";
        $body .=  "  border-radius: 0;\n";
        $body .=  "  border-collapse: separate; }\n";
        $body .=  "\n";
        $body .=  ".block-grid {\n";
        $body .=  "  width: 100%;\n";
        $body .=  "  max-width: 580px; }\n";
        $body .=  "  .block-grid td {\n";
        $body .=  "    display: inline-block;\n";
        $body .=  "    padding: 8px; }\n";
        $body .=  "\n";
        $body .=  ".up-2 td {\n";
        $body .=  "  width: 274px !important; }\n";
        $body .=  "\n";
        $body .=  ".up-3 td {\n";
        $body .=  "  width: 177px !important; }\n";
        $body .=  "\n";
        $body .=  ".up-4 td {\n";
        $body .=  "  width: 129px !important; }\n";
        $body .=  "\n";
        $body .=  ".up-5 td {\n";
        $body .=  "  width: 100px !important; }\n";
        $body .=  "\n";
        $body .=  ".up-6 td {\n";
        $body .=  "  width: 80px !important; }\n";
        $body .=  "\n";
        $body .=  ".up-7 td {\n";
        $body .=  "  width: 66px !important; }\n";
        $body .=  "\n";
        $body .=  ".up-8 td {\n";
        $body .=  "  width: 56px !important; }\n";
        $body .=  "\n";
        $body .=  "table.text-center,\n";
        $body .=  "th.text-center,\n";
        $body .=  "td.text-center,\n";
        $body .=  "h1.text-center,\n";
        $body .=  "h2.text-center,\n";
        $body .=  "h3.text-center,\n";
        $body .=  "h4.text-center,\n";
        $body .=  "h5.text-center,\n";
        $body .=  "h6.text-center,\n";
        $body .=  "p.text-center,\n";
        $body .=  "span.text-center {\n";
        $body .=  "  text-align: center; }\n";
        $body .=  "\n";
        $body .=  "table.text-left,\n";
        $body .=  "th.text-left,\n";
        $body .=  "td.text-left,\n";
        $body .=  "h1.text-left,\n";
        $body .=  "h2.text-left,\n";
        $body .=  "h3.text-left,\n";
        $body .=  "h4.text-left,\n";
        $body .=  "h5.text-left,\n";
        $body .=  "h6.text-left,\n";
        $body .=  "p.text-left,\n";
        $body .=  "span.text-left {\n";
        $body .=  "  text-align: left; }\n";
        $body .=  "\n";
        $body .=  "table.text-right,\n";
        $body .=  "th.text-right,\n";
        $body .=  "td.text-right,\n";
        $body .=  "h1.text-right,\n";
        $body .=  "h2.text-right,\n";
        $body .=  "h3.text-right,\n";
        $body .=  "h4.text-right,\n";
        $body .=  "h5.text-right,\n";
        $body .=  "h6.text-right,\n";
        $body .=  "p.text-right,\n";
        $body .=  "span.text-right {\n";
        $body .=  "  text-align: right; }\n";
        $body .=  "\n";
        $body .=  "span.text-center {\n";
        $body .=  "  display: block;\n";
        $body .=  "  width: 100%;\n";
        $body .=  "  text-align: center; }\n";
        $body .=  "\n";
        $body .=  "@media only screen and (max-width: 596px) {\n";
        $body .=  "  .small-float-center {\n";
        $body .=  "    margin: 0 auto !important;\n";
        $body .=  "    float: none !important;\n";
        $body .=  "    text-align: center !important; }\n";
        $body .=  "  .small-text-center {\n";
        $body .=  "    text-align: center !important; }\n";
        $body .=  "  .small-text-left {\n";
        $body .=  "    text-align: left !important; }\n";
        $body .=  "  .small-text-right {\n";
        $body .=  "    text-align: right !important; } }\n";
        $body .=  "\n";
        $body .=  "img.float-left {\n";
        $body .=  "  float: left;\n";
        $body .=  "  text-align: left; }\n";
        $body .=  "\n";
        $body .=  "img.float-right {\n";
        $body .=  "  float: right;\n";
        $body .=  "  text-align: right; }\n";
        $body .=  "\n";
        $body .=  "img.float-center,\n";
        $body .=  "img.text-center {\n";
        $body .=  "  margin: 0 auto;\n";
        $body .=  "  Margin: 0 auto;\n";
        $body .=  "  float: none;\n";
        $body .=  "  text-align: center; }\n";
        $body .=  "\n";
        $body .=  "table.float-center,\n";
        $body .=  "td.float-center,\n";
        $body .=  "th.float-center {\n";
        $body .=  "  margin: 0 auto;\n";
        $body .=  "  Margin: 0 auto;\n";
        $body .=  "  float: none;\n";
        $body .=  "  text-align: center; }\n";
        $body .=  "\n";
        $body .=  ".hide-for-large {\n";
        $body .=  "  display: none !important;\n";
        $body .=  "  mso-hide: all;\n";
        $body .=  "  overflow: hidden;\n";
        $body .=  "  max-height: 0;\n";
        $body .=  "  font-size: 0;\n";
        $body .=  "  width: 0;\n";
        $body .=  "  line-height: 0; }\n";
        $body .=  "  @media only screen and (max-width: 596px) {\n";
        $body .=  "    .hide-for-large {\n";
        $body .=  "      display: block !important;\n";
        $body .=  "      width: auto !important;\n";
        $body .=  "      overflow: visible !important;\n";
        $body .=  "      max-height: none !important;\n";
        $body .=  "      font-size: inherit !important;\n";
        $body .=  "      line-height: inherit !important; } }\n";
        $body .=  "\n";
        $body .=  "table.body table.container .hide-for-large * {\n";
        $body .=  "  mso-hide: all; }\n";
        $body .=  "\n";
        $body .=  "@media only screen and (max-width: 596px) {\n";
        $body .=  "  table.body table.container .hide-for-large,\n";
        $body .=  "  table.body table.container .row.hide-for-large {\n";
        $body .=  "    display: table !important;\n";
        $body .=  "    width: 100% !important; } }\n";
        $body .=  "\n";
        $body .=  "@media only screen and (max-width: 596px) {\n";
        $body .=  "  table.body table.container .callout-inner.hide-for-large {\n";
        $body .=  "    display: table-cell !important;\n";
        $body .=  "    width: 100% !important; } }\n";
        $body .=  "\n";
        $body .=  "@media only screen and (max-width: 596px) {\n";
        $body .=  "  table.body table.container .show-for-large {\n";
        $body .=  "    display: none !important;\n";
        $body .=  "    width: 0;\n";
        $body .=  "    mso-hide: all;\n";
        $body .=  "    overflow: hidden; } }\n";
        $body .=  "\n";
        $body .=  "body,\n";
        $body .=  "table.body,\n";
        $body .=  "h1,\n";
        $body .=  "h2,\n";
        $body .=  "h3,\n";
        $body .=  "h4,\n";
        $body .=  "h5,\n";
        $body .=  "h6,\n";
        $body .=  "p,\n";
        $body .=  "td,\n";
        $body .=  "th,\n";
        $body .=  "a {\n";
        $body .=  "  color: #0a0a0a;\n";
        $body .=  "  font-family: Helvetica, Arial, sans-serif;\n";
        $body .=  "  font-weight: normal;\n";
        $body .=  "  padding: 0;\n";
        $body .=  "  margin: 0;\n";
        $body .=  "  Margin: 0;\n";
        $body .=  "  text-align: left;\n";
        $body .=  "  line-height: 1.3; }\n";
        $body .=  "\n";
        $body .=  "h1,\n";
        $body .=  "h2,\n";
        $body .=  "h3,\n";
        $body .=  "h4,\n";
        $body .=  "h5,\n";
        $body .=  "h6 {\n";
        $body .=  "  color: inherit;\n";
        $body .=  "  word-wrap: normal;\n";
        $body .=  "  font-family: Helvetica, Arial, sans-serif;\n";
        $body .=  "  font-weight: normal;\n";
        $body .=  "  margin-bottom: 10px;\n";
        $body .=  "  Margin-bottom: 10px; }\n";
        $body .=  "\n";
        $body .=  "h1 {\n";
        $body .=  "  font-size: 34px; }\n";
        $body .=  "\n";
        $body .=  "h2 {\n";
        $body .=  "  font-size: 30px; }\n";
        $body .=  "\n";
        $body .=  "h3 {\n";
        $body .=  "  font-size: 28px; }\n";
        $body .=  "\n";
        $body .=  "h4 {\n";
        $body .=  "  font-size: 24px; }\n";
        $body .=  "\n";
        $body .=  "h5 {\n";
        $body .=  "  font-size: 20px; }\n";
        $body .=  "\n";
        $body .=  "h6 {\n";
        $body .=  "  font-size: 18px; }\n";
        $body .=  "\n";
        $body .=  "body,\n";
        $body .=  "table.body,\n";
        $body .=  "p,\n";
        $body .=  "td,\n";
        $body .=  "th {\n";
        $body .=  "  font-size: 16px;\n";
        $body .=  "  line-height: 1.3; }\n";
        $body .=  "\n";
        $body .=  "p {\n";
        $body .=  "  margin-bottom: 10px;\n";
        $body .=  "  Margin-bottom: 10px; }\n";
        $body .=  "  p.lead {\n";
        $body .=  "    font-size: 20px;\n";
        $body .=  "    line-height: 1.6; }\n";
        $body .=  "  p.subheader {\n";
        $body .=  "    margin-top: 4px;\n";
        $body .=  "    margin-bottom: 8px;\n";
        $body .=  "    Margin-top: 4px;\n";
        $body .=  "    Margin-bottom: 8px;\n";
        $body .=  "    font-weight: normal;\n";
        $body .=  "    line-height: 1.4;\n";
        $body .=  "    color: #8a8a8a; }\n";
        $body .=  "\n";
        $body .=  "small {\n";
        $body .=  "  font-size: 80%;\n";
        $body .=  "  color: #cacaca; }\n";
        $body .=  "\n";
        $body .=  "a {\n";
        $body .=  "  color: #2199e8;\n";
        $body .=  "  text-decoration: none; }\n";
        $body .=  "  a:hover {\n";
        $body .=  "    color: #147dc2; }\n";
        $body .=  "  a:active {\n";
        $body .=  "    color: #147dc2; }\n";
        $body .=  "  a:visited {\n";
        $body .=  "    color: #2199e8; }\n";
        $body .=  "\n";
        $body .=  "h1 a,\n";
        $body .=  "h1 a:visited,\n";
        $body .=  "h2 a,\n";
        $body .=  "h2 a:visited,\n";
        $body .=  "h3 a,\n";
        $body .=  "h3 a:visited,\n";
        $body .=  "h4 a,\n";
        $body .=  "h4 a:visited,\n";
        $body .=  "h5 a,\n";
        $body .=  "h5 a:visited,\n";
        $body .=  "h6 a,\n";
        $body .=  "h6 a:visited {\n";
        $body .=  "  color: #2199e8; }\n";
        $body .=  "\n";
        $body .=  "pre {\n";
        $body .=  "  background: #f3f3f3;\n";
        $body .=  "  margin: 30px 0;\n";
        $body .=  "  Margin: 30px 0; }\n";
        $body .=  "  pre code {\n";
        $body .=  "    color: #cacaca; }\n";
        $body .=  "    pre code span.callout {\n";
        $body .=  "      color: #8a8a8a;\n";
        $body .=  "      font-weight: bold; }\n";
        $body .=  "    pre code span.callout-strong {\n";
        $body .=  "      color: #ff6908;\n";
        $body .=  "      font-weight: bold; }\n";
        $body .=  "\n";
        $body .=  "table.hr {\n";
        $body .=  "  width: 100%; }\n";
        $body .=  "  table.hr th {\n";
        $body .=  "    height: 0;\n";
        $body .=  "    max-width: 580px;\n";
        $body .=  "    border-top: 0;\n";
        $body .=  "    border-right: 0;\n";
        $body .=  "    border-bottom: 1px solid #0a0a0a;\n";
        $body .=  "    border-left: 0;\n";
        $body .=  "    margin: 20px auto;\n";
        $body .=  "    Margin: 20px auto;\n";
        $body .=  "    clear: both; }\n";
        $body .=  "\n";
        $body .=  ".stat {\n";
        $body .=  "  font-size: 40px;\n";
        $body .=  "  line-height: 1; }\n";
        $body .=  "  p + .stat {\n";
        $body .=  "    margin-top: -16px;\n";
        $body .=  "    Margin-top: -16px; }\n";
        $body .=  "\n";
        $body .=  "span.preheader {\n";
        $body .=  "  display: none !important;\n";
        $body .=  "  visibility: hidden;\n";
        $body .=  "  mso-hide: all !important;\n";
        $body .=  "  font-size: 1px;\n";
        $body .=  "  color: #f3f3f3;\n";
        $body .=  "  line-height: 1px;\n";
        $body .=  "  max-height: 0px;\n";
        $body .=  "  max-width: 0px;\n";
        $body .=  "  opacity: 0;\n";
        $body .=  "  overflow: hidden; }\n";
        $body .=  "\n";
        $body .=  "table.button {\n";
        $body .=  "  width: auto;\n";
        $body .=  "  margin: 0 0 16px 0;\n";
        $body .=  "  Margin: 0 0 16px 0; }\n";
        $body .=  "  table.button table td {\n";
        $body .=  "    text-align: left;\n";
        $body .=  "    color: #fefefe;\n";
        $body .=  "    background: #2199e8;\n";
        $body .=  "    border: 2px solid #2199e8; }\n";
        $body .=  "    table.button table td a {\n";
        $body .=  "      font-family: Helvetica, Arial, sans-serif;\n";
        $body .=  "      font-size: 16px;\n";
        $body .=  "      font-weight: bold;\n";
        $body .=  "      color: #fefefe;\n";
        $body .=  "      text-decoration: none;\n";
        $body .=  "      display: inline-block;\n";
        $body .=  "      padding: 8px 16px 8px 16px;\n";
        $body .=  "      border: 0 solid #2199e8;\n";
        $body .=  "      border-radius: 3px; }\n";
        $body .=  "  table.button.radius table td {\n";
        $body .=  "    border-radius: 3px;\n";
        $body .=  "    border: none; }\n";
        $body .=  "  table.button.rounded table td {\n";
        $body .=  "    border-radius: 500px;\n";
        $body .=  "    border: none; }\n";
        $body .=  "\n";
        $body .=  "table.button:hover table tr td a,\n";
        $body .=  "table.button:active table tr td a,\n";
        $body .=  "table.button table tr td a:visited,\n";
        $body .=  "table.button.tiny:hover table tr td a,\n";
        $body .=  "table.button.tiny:active table tr td a,\n";
        $body .=  "table.button.tiny table tr td a:visited,\n";
        $body .=  "table.button.small:hover table tr td a,\n";
        $body .=  "table.button.small:active table tr td a,\n";
        $body .=  "table.button.small table tr td a:visited,\n";
        $body .=  "table.button.large:hover table tr td a,\n";
        $body .=  "table.button.large:active table tr td a,\n";
        $body .=  "table.button.large table tr td a:visited {\n";
        $body .=  "  color: #fefefe; }\n";
        $body .=  "\n";
        $body .=  "table.button.tiny table td,\n";
        $body .=  "table.button.tiny table a {\n";
        $body .=  "  padding: 4px 8px 4px 8px; }\n";
        $body .=  "\n";
        $body .=  "table.button.tiny table a {\n";
        $body .=  "  font-size: 10px;\n";
        $body .=  "  font-weight: normal; }\n";
        $body .=  "\n";
        $body .=  "table.button.small table td,\n";
        $body .=  "table.button.small table a {\n";
        $body .=  "  padding: 5px 10px 5px 10px;\n";
        $body .=  "  font-size: 12px; }\n";
        $body .=  "\n";
        $body .=  "table.button.large table a {\n";
        $body .=  "  padding: 10px 20px 10px 20px;\n";
        $body .=  "  font-size: 20px; }\n";
        $body .=  "\n";
        $body .=  "table.button.expand,\n";
        $body .=  "table.button.expanded {\n";
        $body .=  "  width: 100% !important; }\n";
        $body .=  "  table.button.expand table,\n";
        $body .=  "  table.button.expanded table {\n";
        $body .=  "    width: 100%; }\n";
        $body .=  "    table.button.expand table a,\n";
        $body .=  "    table.button.expanded table a {\n";
        $body .=  "      text-align: center;\n";
        $body .=  "      width: 100%;\n";
        $body .=  "      padding-left: 0;\n";
        $body .=  "      padding-right: 0; }\n";
        $body .=  "  table.button.expand center,\n";
        $body .=  "  table.button.expanded center {\n";
        $body .=  "    min-width: 0; }\n";
        $body .=  "\n";
        $body .=  "table.button:hover table td,\n";
        $body .=  "table.button:visited table td,\n";
        $body .=  "table.button:active table td {\n";
        $body .=  "  background: #147dc2;\n";
        $body .=  "  color: #fefefe; }\n";
        $body .=  "\n";
        $body .=  "table.button:hover table a,\n";
        $body .=  "table.button:visited table a,\n";
        $body .=  "table.button:active table a {\n";
        $body .=  "  border: 0 solid #147dc2; }\n";
        $body .=  "\n";
        $body .=  "table.button.secondary table td {\n";
        $body .=  "  background: #777777;\n";
        $body .=  "  color: #fefefe;\n";
        $body .=  "  border: 0px solid #777777; }\n";
        $body .=  "\n";
        $body .=  "table.button.secondary table a {\n";
        $body .=  "  color: #fefefe;\n";
        $body .=  "  border: 0 solid #777777; }\n";
        $body .=  "\n";
        $body .=  "table.button.secondary:hover table td {\n";
        $body .=  "  background: #919191;\n";
        $body .=  "  color: #fefefe; }\n";
        $body .=  "\n";
        $body .=  "table.button.secondary:hover table a {\n";
        $body .=  "  border: 0 solid #919191; }\n";
        $body .=  "\n";
        $body .=  "table.button.secondary:hover table td a {\n";
        $body .=  "  color: #fefefe; }\n";
        $body .=  "\n";
        $body .=  "table.button.secondary:active table td a {\n";
        $body .=  "  color: #fefefe; }\n";
        $body .=  "\n";
        $body .=  "table.button.secondary table td a:visited {\n";
        $body .=  "  color: #fefefe; }\n";
        $body .=  "\n";
        $body .=  "table.button.success table td {\n";
        $body .=  "  background: #3adb76;\n";
        $body .=  "  border: 0px solid #3adb76; }\n";
        $body .=  "\n";
        $body .=  "table.button.success table a {\n";
        $body .=  "  border: 0 solid #3adb76; }\n";
        $body .=  "\n";
        $body .=  "table.button.success:hover table td {\n";
        $body .=  "  background: #23bf5d; }\n";
        $body .=  "\n";
        $body .=  "table.button.success:hover table a {\n";
        $body .=  "  border: 0 solid #23bf5d; }\n";
        $body .=  "\n";
        $body .=  "table.button.alert table td {\n";
        $body .=  "  background: #ec5840;\n";
        $body .=  "  border: 0px solid #ec5840; }\n";
        $body .=  "\n";
        $body .=  "table.button.alert table a {\n";
        $body .=  "  border: 0 solid #ec5840; }\n";
        $body .=  "\n";
        $body .=  "table.button.alert:hover table td {\n";
        $body .=  "  background: #e23317; }\n";
        $body .=  "\n";
        $body .=  "table.button.alert:hover table a {\n";
        $body .=  "  border: 0 solid #e23317; }\n";
        $body .=  "\n";
        $body .=  "table.button.warning table td {\n";
        $body .=  "  background: #ffae00;\n";
        $body .=  "  border: 0px solid #ffae00; }\n";
        $body .=  "\n";
        $body .=  "table.button.warning table a {\n";
        $body .=  "  border: 0px solid #ffae00; }\n";
        $body .=  "\n";
        $body .=  "table.button.warning:hover table td {\n";
        $body .=  "  background: #cc8b00; }\n";
        $body .=  "\n";
        $body .=  "table.button.warning:hover table a {\n";
        $body .=  "  border: 0px solid #cc8b00; }\n";
        $body .=  "\n";
        $body .=  "table.callout {\n";
        $body .=  "  margin-bottom: 16px;\n";
        $body .=  "  Margin-bottom: 16px; }\n";
        $body .=  "\n";
        $body .=  "th.callout-inner {\n";
        $body .=  "  width: 100%;\n";
        $body .=  "  border: 1px solid #cbcbcb;\n";
        $body .=  "  padding: 10px;\n";
        $body .=  "  background: #fefefe; }\n";
        $body .=  "  th.callout-inner.primary {\n";
        $body .=  "    background: #def0fc;\n";
        $body .=  "    border: 1px solid #444444;\n";
        $body .=  "    color: #0a0a0a; }\n";
        $body .=  "  th.callout-inner.secondary {\n";
        $body .=  "    background: #ebebeb;\n";
        $body .=  "    border: 1px solid #444444;\n";
        $body .=  "    color: #0a0a0a; }\n";
        $body .=  "  th.callout-inner.success {\n";
        $body .=  "    background: #e1faea;\n";
        $body .=  "    border: 1px solid #1b9448;\n";
        $body .=  "    color: #fefefe; }\n";
        $body .=  "  th.callout-inner.warning {\n";
        $body .=  "    background: #fff3d9;\n";
        $body .=  "    border: 1px solid #996800;\n";
        $body .=  "    color: #fefefe; }\n";
        $body .=  "  th.callout-inner.alert {\n";
        $body .=  "    background: #fce6e2;\n";
        $body .=  "    border: 1px solid #b42912;\n";
        $body .=  "    color: #fefefe; }\n";
        $body .=  "\n";
        $body .=  ".thumbnail {\n";
        $body .=  "  border: solid 4px #fefefe;\n";
        $body .=  "  box-shadow: 0 0 0 1px rgba(10, 10, 10, 0.2);\n";
        $body .=  "  display: inline-block;\n";
        $body .=  "  line-height: 0;\n";
        $body .=  "  max-width: 100%;\n";
        $body .=  "  transition: box-shadow 200ms ease-out;\n";
        $body .=  "  border-radius: 3px;\n";
        $body .=  "  margin-bottom: 16px; }\n";
        $body .=  "  .thumbnail:hover, .thumbnail:focus {\n";
        $body .=  "    box-shadow: 0 0 6px 1px rgba(33, 153, 232, 0.5); }\n";
        $body .=  "\n";
        $body .=  "table.menu {\n";
        $body .=  "  width: 580px; }\n";
        $body .=  "  table.menu td.menu-item,\n";
        $body .=  "  table.menu th.menu-item {\n";
        $body .=  "    padding: 10px;\n";
        $body .=  "    padding-right: 10px; }\n";
        $body .=  "    table.menu td.menu-item a,\n";
        $body .=  "    table.menu th.menu-item a {\n";
        $body .=  "      color: #2199e8; }\n";
        $body .=  "\n";
        $body .=  "table.menu.vertical td.menu-item,\n";
        $body .=  "table.menu.vertical th.menu-item {\n";
        $body .=  "  padding: 10px;\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  display: block; }\n";
        $body .=  "  table.menu.vertical td.menu-item a,\n";
        $body .=  "  table.menu.vertical th.menu-item a {\n";
        $body .=  "    width: 100%; }\n";
        $body .=  "\n";
        $body .=  "table.menu.vertical td.menu-item table.menu.vertical td.menu-item,\n";
        $body .=  "table.menu.vertical td.menu-item table.menu.vertical th.menu-item,\n";
        $body .=  "table.menu.vertical th.menu-item table.menu.vertical td.menu-item,\n";
        $body .=  "table.menu.vertical th.menu-item table.menu.vertical th.menu-item {\n";
        $body .=  "  padding-left: 10px; }\n";
        $body .=  "\n";
        $body .=  "table.menu.text-center a {\n";
        $body .=  "  text-align: center; }\n";
        $body .=  "\n";
        $body .=  ".menu[align=\"center\"] {\n";
        $body .=  "  width: auto !important; }\n";
        $body .=  "\n";
        $body .=  "body.outlook p {\n";
        $body .=  "  display: inline !important; }\n";
        $body .=  "\n";
        $body .=  "@media only screen and (max-width: 596px) {\n";
        $body .=  "  table.body img {\n";
        $body .=  "    width: auto;\n";
        $body .=  "    height: auto; }\n";
        $body .=  "  table.body center {\n";
        $body .=  "    min-width: 0 !important; }\n";
        $body .=  "  table.body .container {\n";
        $body .=  "    width: 95% !important; }\n";
        $body .=  "  table.body .columns,\n";
        $body .=  "  table.body .column {\n";
        $body .=  "    height: auto !important;\n";
        $body .=  "    -moz-box-sizing: border-box;\n";
        $body .=  "    -webkit-box-sizing: border-box;\n";
        $body .=  "    box-sizing: border-box;\n";
        $body .=  "    padding-left: 16px !important;\n";
        $body .=  "    padding-right: 16px !important; }\n";
        $body .=  "    table.body .columns .column,\n";
        $body .=  "    table.body .columns .columns,\n";
        $body .=  "    table.body .column .column,\n";
        $body .=  "    table.body .column .columns {\n";
        $body .=  "      padding-left: 0 !important;\n";
        $body .=  "      padding-right: 0 !important; }\n";
        $body .=  "  table.body .collapse .columns,\n";
        $body .=  "  table.body .collapse .column {\n";
        $body .=  "    padding-left: 0 !important;\n";
        $body .=  "    padding-right: 0 !important; }\n";
        $body .=  "  td.small-1,\n";
        $body .=  "  th.small-1 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 8.33333% !important; }\n";
        $body .=  "  td.small-2,\n";
        $body .=  "  th.small-2 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 16.66667% !important; }\n";
        $body .=  "  td.small-3,\n";
        $body .=  "  th.small-3 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 25% !important; }\n";
        $body .=  "  td.small-4,\n";
        $body .=  "  th.small-4 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 33.33333% !important; }\n";
        $body .=  "  td.small-5,\n";
        $body .=  "  th.small-5 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 41.66667% !important; }\n";
        $body .=  "  td.small-6,\n";
        $body .=  "  th.small-6 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 50% !important; }\n";
        $body .=  "  td.small-7,\n";
        $body .=  "  th.small-7 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 58.33333% !important; }\n";
        $body .=  "  td.small-8,\n";
        $body .=  "  th.small-8 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 66.66667% !important; }\n";
        $body .=  "  td.small-9,\n";
        $body .=  "  th.small-9 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 75% !important; }\n";
        $body .=  "  td.small-10,\n";
        $body .=  "  th.small-10 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 83.33333% !important; }\n";
        $body .=  "  td.small-11,\n";
        $body .=  "  th.small-11 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 91.66667% !important; }\n";
        $body .=  "  td.small-12,\n";
        $body .=  "  th.small-12 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 100% !important; }\n";
        $body .=  "  .columns td.small-12,\n";
        $body .=  "  .column td.small-12,\n";
        $body .=  "  .columns th.small-12,\n";
        $body .=  "  .column th.small-12 {\n";
        $body .=  "    display: block !important;\n";
        $body .=  "    width: 100% !important; }\n";
        $body .=  "  table.body td.small-offset-1,\n";
        $body .=  "  table.body th.small-offset-1 {\n";
        $body .=  "    margin-left: 8.33333% !important;\n";
        $body .=  "    Margin-left: 8.33333% !important; }\n";
        $body .=  "  table.body td.small-offset-2,\n";
        $body .=  "  table.body th.small-offset-2 {\n";
        $body .=  "    margin-left: 16.66667% !important;\n";
        $body .=  "    Margin-left: 16.66667% !important; }\n";
        $body .=  "  table.body td.small-offset-3,\n";
        $body .=  "  table.body th.small-offset-3 {\n";
        $body .=  "    margin-left: 25% !important;\n";
        $body .=  "    Margin-left: 25% !important; }\n";
        $body .=  "  table.body td.small-offset-4,\n";
        $body .=  "  table.body th.small-offset-4 {\n";
        $body .=  "    margin-left: 33.33333% !important;\n";
        $body .=  "    Margin-left: 33.33333% !important; }\n";
        $body .=  "  table.body td.small-offset-5,\n";
        $body .=  "  table.body th.small-offset-5 {\n";
        $body .=  "    margin-left: 41.66667% !important;\n";
        $body .=  "    Margin-left: 41.66667% !important; }\n";
        $body .=  "  table.body td.small-offset-6,\n";
        $body .=  "  table.body th.small-offset-6 {\n";
        $body .=  "    margin-left: 50% !important;\n";
        $body .=  "    Margin-left: 50% !important; }\n";
        $body .=  "  table.body td.small-offset-7,\n";
        $body .=  "  table.body th.small-offset-7 {\n";
        $body .=  "    margin-left: 58.33333% !important;\n";
        $body .=  "    Margin-left: 58.33333% !important; }\n";
        $body .=  "  table.body td.small-offset-8,\n";
        $body .=  "  table.body th.small-offset-8 {\n";
        $body .=  "    margin-left: 66.66667% !important;\n";
        $body .=  "    Margin-left: 66.66667% !important; }\n";
        $body .=  "  table.body td.small-offset-9,\n";
        $body .=  "  table.body th.small-offset-9 {\n";
        $body .=  "    margin-left: 75% !important;\n";
        $body .=  "    Margin-left: 75% !important; }\n";
        $body .=  "  table.body td.small-offset-10,\n";
        $body .=  "  table.body th.small-offset-10 {\n";
        $body .=  "    margin-left: 83.33333% !important;\n";
        $body .=  "    Margin-left: 83.33333% !important; }\n";
        $body .=  "  table.body td.small-offset-11,\n";
        $body .=  "  table.body th.small-offset-11 {\n";
        $body .=  "    margin-left: 91.66667% !important;\n";
        $body .=  "    Margin-left: 91.66667% !important; }\n";
        $body .=  "  table.body table.columns td.expander,\n";
        $body .=  "  table.body table.columns th.expander {\n";
        $body .=  "    display: none !important; }\n";
        $body .=  "  table.body .right-text-pad,\n";
        $body .=  "  table.body .text-pad-right {\n";
        $body .=  "    padding-left: 10px !important; }\n";
        $body .=  "  table.body .left-text-pad,\n";
        $body .=  "  table.body .text-pad-left {\n";
        $body .=  "    padding-right: 10px !important; }\n";
        $body .=  "  table.menu {\n";
        $body .=  "    width: 100% !important; }\n";
        $body .=  "    table.menu td,\n";
        $body .=  "    table.menu th {\n";
        $body .=  "      width: auto !important;\n";
        $body .=  "      display: inline-block !important; }\n";
        $body .=  "    table.menu.vertical td,\n";
        $body .=  "    table.menu.vertical th, table.menu.small-vertical td,\n";
        $body .=  "    table.menu.small-vertical th {\n";
        $body .=  "      display: block !important; }\n";
        $body .=  "  table.menu[align=\"center\"] {\n";
        $body .=  "    width: auto !important; }\n";
        $body .=  "  table.button.small-expand,\n";
        $body .=  "  table.button.small-expanded {\n";
        $body .=  "    width: 100% !important; }\n";
        $body .=  "    table.button.small-expand table,\n";
        $body .=  "    table.button.small-expanded table {\n";
        $body .=  "      width: 100%; }\n";
        $body .=  "      table.button.small-expand table a,\n";
        $body .=  "      table.button.small-expanded table a {\n";
        $body .=  "        text-align: center !important;\n";
        $body .=  "        width: 100% !important;\n";
        $body .=  "        padding-left: 0 !important;\n";
        $body .=  "        padding-right: 0 !important; }\n";
        $body .=  "    table.button.small-expand center,\n";
        $body .=  "    table.button.small-expanded center {\n";
        $body .=  "      min-width: 0; } }\n";
        $body .=  "\n";
        $body .=  "    </style>\n";
        $body .=  "\n";
        $body .=  "    <style>\n";
        $body .=  "      body,\n";
        $body .=  "      html,\n";
        $body .=  "      .body {\n";
        $body .=  "        background: #f3f3f3 !important;\n";
        $body .=  "      }\n";
        $body .=  "\n";
        $body .=  "      .container.header {\n";
        $body .=  "        background: #f3f3f3;\n";
        $body .=  "      }\n";
        $body .=  "\n";
        $body .=  "      .body-border {\n";
        $body .=  "        border-top: 8px solid #ef0000;\n";
        $body .=  "      }\n";
        $body .=  "    </style>\n";
        $body .=  "  </head>\n";
        $body .=  "\n";
        $body .=  "  <body>\n";
        $body .=  "    <!-- <style> -->\n";
        $body .=  "    <table class=\"body\" data-made-with-foundation=\"\">\n";
        $body .=  "      <tr>\n";
        $body .=  "        <td class=\"float-center\" align=\"center\" valign=\"top\">\n";
        $body .=  "          <center data-parsed=\"\">\n";
        $body .=  "            <table class=\"spacer float-center\">\n";
        $body .=  "              <tbody>\n";
        $body .=  "                <tr>\n";
        $body .=  "                  <td height=\"16px\" style=\"font-size:16px;line-height:16px;\">&#xA0;</td>\n";
        $body .=  "                </tr>\n";
        $body .=  "              </tbody>\n";
        $body .=  "            </table>\n";
        $body .=  "            <table align=\"center\" class=\"container header float-center\">\n";
        $body .=  "              <tbody>\n";
        $body .=  "                <tr>\n";
        $body .=  "                  <td>\n";
        $body .=  "                    <table class=\"row\">\n";
        $body .=  "                      <tbody>\n";
        $body .=  "                        <tr>\n";
        $body .=  "                          <th class=\"small-12 large-12 columns first last\">\n";
        $body .=  "                            <table>\n";
        $body .=  "                              <tr>\n";
        $body .=  "                                <th>\n";
        $body .=  "                                  <h1 class=\"text-center\">ฟอร์มอีเมล์จาก เว็บไซต์ Condo Compare</h1>\n";
        $body .=  "                                </th>\n";
        $body .=  "                                <th class=\"expander\"></th>\n";
        $body .=  "                              </tr>\n";
        $body .=  "                            </table>\n";
        $body .=  "                          </th>\n";
        $body .=  "                        </tr>\n";
        $body .=  "                      </tbody>\n";
        $body .=  "                    </table>\n";
        $body .=  "                  </td>\n";
        $body .=  "                </tr>\n";
        $body .=  "              </tbody>\n";
        $body .=  "            </table>\n";
        $body .=  "            <table align=\"center\" class=\"container body-border float-center\">\n";
        $body .=  "              <tbody>\n";
        $body .=  "                <tr>\n";
        $body .=  "                  <td>\n";
        $body .=  "                    <table class=\"row\">\n";
        $body .=  "                      <tbody>\n";
        $body .=  "                        <tr>\n";
        $body .=  "                          <th class=\"small-12 large-12 columns first last\">\n";
        $body .=  "                            <table>\n";
        $body .=  "                              <tr>\n";
        $body .=  "                                <th>\n";
        $body .=  "                                  <table class=\"spacer\">\n";
        $body .=  "                                    <tbody>\n";
        $body .=  "                                      <tr>\n";
        $body .=  "                                        <td height=\"32px\" style=\"font-size:32px;line-height:32px;\">&#xA0;</td>\n";
        $body .=  "                                      </tr>\n";
        $body .=  "                                    </tbody>\n";
        $body .=  "                                  </table>\n";
        $body .=  "                                  <center data-parsed=\"\"> <img src=\"$site_logo\" align=\"center\" class=\"float-center\"> </center>\n";
        $body .=  "                                  <table class=\"spacer\">\n";
        $body .=  "                                    <tbody>\n";
        $body .=  "                                      <tr>\n";
        $body .=  "                                        <td height=\"16px\" style=\"font-size:16px;line-height:16px;\">&#xA0;</td>\n";
        $body .=  "                                      </tr>\n";
        $body .=  "                                    </tbody>\n";
        $body .=  "                                  </table>\n";

        $body .=  "                                  $message";

        $body .=  "                                  <center data-parsed=\"\">\n";
        $body .=  "                                    <table align=\"center\" class=\"menu float-center\">\n";
        $body .=  "                                      <tr>\n";
        $body .=  "                                        <td>\n";
        $body .=  "                                          <table>\n";
        $body .=  "                                            <tr>\n";
        $body .=  "                                              <th class=\"menu-item float-center\"><a href=\"http://condo-compare.com/\" target=\"_blank\">ไปที่เว็บไซต์</a></th>\n";
        $body .=  "                                            </tr>\n";
        $body .=  "                                          </table>\n";
        $body .=  "                                        </td>\n";
        $body .=  "                                      </tr>\n";
        $body .=  "                                    </table>\n";
        $body .=  "                                  </center>\n";
        $body .=  "                                </th>\n";
        $body .=  "                                <th class=\"expander\"></th>\n";
        $body .=  "                              </tr>\n";
        $body .=  "                            </table>\n";
        $body .=  "                          </th>\n";
        $body .=  "                        </tr>\n";
        $body .=  "                      </tbody>\n";
        $body .=  "                    </table>\n";
        $body .=  "                    <table class=\"spacer\">\n";
        $body .=  "                      <tbody>\n";
        $body .=  "                        <tr>\n";
        $body .=  "                          <td height=\"16px\" style=\"font-size:16px;line-height:16px;\">&#xA0;</td>\n";
        $body .=  "                        </tr>\n";
        $body .=  "                      </tbody>\n";
        $body .=  "                    </table>\n";
        $body .=  "                  </td>\n";
        $body .=  "                </tr>\n";
        $body .=  "              </tbody>\n";
        $body .=  "            </table>\n";
        $body .=  "          </center>\n";
        $body .=  "        </td>\n";
        $body .=  "      </tr>\n";
        $body .=  "    </table>\n";
        $body .=  "  </body>\n";
        $body .=  "\n";
        $body .=  "</html>\n";
        $body .=  "\n";
        // Also, for getting full html you may use the following internal method:
        //$body = $this->email->full_html($subject, $message);

//echo $body;
        $result = $this->email
            ->from('hob@condo-compare.com')
            ->reply_to('')    // Optional, an account where a human being reads.
            ->to($data['email'])
            ->subject($subject)
            ->message($body)
            ->send();

        // var_dump($result);
        // echo '<br />';
        // echo $this->email->print_debugger();
        //exit;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function view_notification()
    {

        $this->load->view('backoffice/land/datatable-share-notif', array(
            'title' => 'คอนโดรายการติดต่อใหม่',
            'header' => 'yes',
            'url_href' => '#',
            'js' => array(
                'assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
            ),

        ));
    }
    //------------------------------------------------------------------------------------------------

    public function json_notification_land()
    {
        $list = $this->Land_model->get_notif_datatables();
        // $this->primaryclass->pre_var_dump($list);
        $data = array();
        $no = (isset($_POST['start']))? $_POST['start'] : '';
        foreach ($list as $land) {
            $no++;

            $land_share_form_id = $land->land_share_form_id;
            $fk_member_id = $land->fk_member_id;
            $fk_land_id = $land->fk_land_id;

            $output_id = $no;

            $land_title = $this->M->get_name_row('land_title', 'land_id', $fk_land_id, 'land');
            // $this->primaryclass->pre_var_dump($land_title);
            $createdate = $land->createdate;
            $createdate_t = ($createdate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, "Th") : 'ไม่ระบุ';

            $path_land = site_url('land-details/'.$fk_land_id);
            $output_title = '';
            $output_title .= '<a href='. $path_land .' target="_blank">'.$land_title.'</a><br />';
            $output_title .= 'วันที่ติดต่อ : ' . $createdate_t;

            $land_share_form_name = $land->land_share_form_name;
            $land_share_form_phone = $land->land_share_form_phone;
            $land_share_form_email = $land->land_share_form_email;
            $land_share_form_message = $land->land_share_form_message;




            $type_status_id = $land->fk_type_status_id;
            $type_status_title = $this->Configsystem->get_type_status_title($type_status_id);
            $path_share = site_url('land/share/'.$fk_land_id);

            $output_manage = '';
    				$output_manage .= '<a href='. $path_share .' class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5"><i class="fa fa-pencil"></i></a>';

            $row = array();
            $row[] = $output_id;
            $row[] = $output_title;
            $row[] = $land_share_form_name;
            $row[] = $land_share_form_phone;
            $row[] = $land_share_form_email;
            $row[] = $land_share_form_message;
            $row[] = $type_status_title;
            $row[] = $output_manage;
            $data[] = $row;
        }

        $output = array(
                "draw" => (isset($_POST['draw']))? $_POST['draw'] : '',
                "recordsTotal" => $this->Land_model->count_all_notification(),
                "recordsFiltered" => $this->Land_model->count_filtered_notification(),
                "data" => $data,
        );
        //output to json format
        echo json_encode($output);


    }
    //----------------------------------------------------------------------------------------------------------
}
