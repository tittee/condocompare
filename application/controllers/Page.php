<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Page extends CI_Controller
{
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var $site_id = 1;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url', 'string'));                                /***** LOADING HELPER TO AVOID PHP ERROR ****/
        $this->load->library('upload');
        $this->load->library('Primaryclass');
        $this->load->library('dateclass');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->library('encryption');
        $this->load->helper('cookie');
        $this->load->model('Member_model');                                /***** LOADING Controller * Primaryfunc as all ****/
        $this->load->model('M');                                /***** LOADING Controller * Configsystem_model ****/
        $this->load->model('Condominium_model');                                /***** LOADING Controller * Condominium_model ****/
        $this->load->model('Land_model');                                /***** LOADING Controller * Condominium_model ****/
        $this->load->model('Configsystem_model');                                /***** LOADING Controller * Configsystem_model ****/
        $this->load->model('Page_model');                                /***** LOADING Controller * Page_model ****/
        $this->load->model('News_model');                                /***** LOADING Controller * News_model ****/
        $this->load->model('Reviews_model');                                /***** LOADING Controller * Reviews_model ****/
        $this->load->model('Activities_model');                                /***** LOADING Controller * Activities_model ****/


        /* SESSION */
        $this->session->set_userdata('sess_id', session_id());
    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function index()
    {
        $highlight_big = $this->Page_model->highlight_big();
        $highlight = $this->Page_model->highlight($highlight_big->condo_id);
        $newsarrival = $this->Page_model->newsarrival();
        $news = $this->Page_model->get_news_home();
        $reviews = $this->Page_model->get_reviews_home();
        $activities = $this->Page_model->get_activities_home();
        $mass_transportation_category = $this->Condominium_model->get_mass_transportation_category_option();
        $transportation_category = $this->Condominium_model->get_transportation_category_option();

        $type_offering = $this->Configsystem_model->get_type_offering_option();
        $type_suites = $this->Configsystem_model->get_type_suites_option();
        $type_zone = $this->Configsystem_model->get_type_zone_option();

        $this->load->view('frontend/index', array(
            'title' => 'ตารางข้อมูลคอนโด',
            'highlight_big' => $highlight_big,
            'highlight' => $highlight,
            'newsarrival' => $newsarrival,
            'transportation_category' => $transportation_category,
            'mass_transportation_category' => $mass_transportation_category,
            'reviews' => $reviews,
            'news' => $news,
            'activities' => $activities,
            'type_offering' => $type_offering,
            'type_suites' => $type_suites,
            'type_zone' => $type_zone,
            'js' => array(
                'assets/js/filter/filter.js',
            ),
        ));
    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function condo_list()
    {
      // 1. กรณี แสดงผลตามปกติ
      // 2. กรณี แสดงผลจากการค้นหา
      // 		2.1 รวมการแสดงผลจากการฟิลเตอร์
      // 3. กรณี แสดงผลจากการฟิลเตอร์
      // 4. การทำ pagination

      $url_page = base_url().'condominium';
      // ========================================================================
      // 					PAGINATION
      // ========================================================================
      $data['condo_total'] = $this->Page_model->condo_search_record_count();
      if (!empty($this->input->get('order_by_list'))) {
          $per_page = $this->input->get('order_by_list');
      } else {
          $per_page = '15';
      }

      $page_pagination = $this->primaryclass->set_pagination($url_page, $data['condo_total'], $per_page);
      $links = $this->pagination->create_links();

      // DATA #1
      $data['condo'] = $this->Page_model->get_condolist($page_pagination['per_page'], $page_pagination['page']);
      // ========================================================================

      // ========================================================================
      // 					SEARCH
      // ========================================================================

      $breadcrumbs_arr = array(
        '1' => array(
          'title' => 'รายการคอนโด',
          'href' => 'condominium',
        ),
      );
      $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

      $condo_sidebar = $this->Condominium_model->get_condo_sidebar();
      $compare_success = $this->Condominium_model->get_compare_condo($_SESSION['sess_id']);
      $this->load->view('frontend/condo-list', array(
        'title' => 'รายการคอนโด',
        'links' => $links,
        'data' => $data,
        'compare_success' => $compare_success,
        'condo_sidebar' => $condo_sidebar,
        'breadcrumbs' => $breadcrumbs,
      ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function condoinside($id, $member_id = 0)
    {

      $encode_member_id = $this->primaryclass->raw_encode($this->session->mID);


        $data = $this->Page_model->condoinside($id);

        if (empty($data)) {
            redirect('pagenotfound');
        }

        $condo_image = $this->Condominium_model->get_condo_image($id);
        $featured = $this->Condominium_model->get_featured_row_by_condo($id);
        $facilities = $this->Condominium_model->get_facilities_row_by_condo($id);
        $transportation = $this->Condominium_model->get_transportation_relation_row_by_condo($id);
        $update_visited = $this->M->update_visited('condo_id', $id, 'condo');

        //HEADER SEO
        $condo_items = $this->M->get_data_by_id('condo_id, condo_title, pic_thumb, condo_expert', 'condo_id', $id, 'condo');


        $fk_condo_id = $id;
        $fk_member_id = $encode_member_id;

        $breadcrumbs_arr = array(
          '1' => array(
            'title' => 'รายการคอนโด',
            'href' => 'condominium',
          ),
          // '2' => array(
          //   'title' => $data->type_zone_title,
          //   'href' => '../condominium',
          // ),
          '2' => array(
            'title' => $condo_items['condo_title'],
            'href' => '../condominium-details/'.$condo_items['condo_id'],
          ),
        );
        $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

        $this->load->view('frontend/condo-inside', array(
            'title' => 'หน้ารายละเอียดคอนโด',
            'css' => array(
                'assets/css/jssocials-theme-flat.css',
                'assets/css/doughnutit.min.css',
            ),
            'js' => array(
                'assets/js/jquery.uploadPreview.min.js',
                'assets/js/jssocials.js',
                'assets/js/jssocials.shares.js',
            ),
            'data' => $data,
            'condo_image' => $condo_image,
            'featured' => $featured,
            'facilities' => $facilities,
            'transportation' => $transportation,
            'fk_condo_id' => $fk_condo_id,
            'fk_member_id' => $fk_member_id,
            'condo_items' => $condo_items,
            'breadcrumbs' => $breadcrumbs,
            'isslider' => true,
            'sharepage' => true,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        public function search()
        {
            //Seacrh 1

        $data_seach = array();
            $action_search = (!empty($this->input->get('action_search'))) ? $this->input->get('action_search') : 0;
            $keyword = (!empty($this->input->get('search'))) ? $this->input->get('search') : null;
            $min_price = (!empty($this->input->get('min_price'))) ? $this->input->get('min_price') : '';
            $max_price = (!empty($this->input->get('max_price'))) ? $this->input->get('max_price') : '';
            $type_offering = (!empty($this->input->get('type_offering'))) ? $this->input->get('type_offering') : '';
            $type_suites = (!empty($this->input->get('type_suites'))) ? $this->input->get('type_suites') : '';
            $condo_area_apartment = (!empty($this->input->get('condo_area_apartment'))) ? $this->input->get('condo_area_apartment') : '';
            $condo_room = (!empty($this->input->get('condo_room'))) ? $this->input->get('condo_room') : '';

            $condo_owner_name = (!empty($this->input->get('condo_owner_name'))) ? $this->input->get('condo_owner_name') : '';
                //Seacrh 2
                $transportation_category = (!empty($this->input->get('transportation_category'))) ? $this->input->get('transportation_category') : '';
            $bts = (!empty($this->input->get('bts'))) ? $this->input->get('bts') : '';
            $mrt = (!empty($this->input->get('mrt'))) ? $this->input->get('mrt') : '';
            $airportlink = (!empty($this->input->get('airportlink'))) ? $this->input->get('airportlink') : '';

                //Seacrh 3
                $type_zone_id = (!empty($this->input->get('type_zone_id'))) ? $this->input->get('type_zone_id') : '';

                //Seacrh 4
                $transportation_id = (!empty($this->input->get('transportation_id'))) ? $this->input->get('transportation_id') : '';

            $data_seach = array(
                        'keyword' => $keyword,
                        'min_price' => $min_price,
                        'max_price' => $max_price,
                        'type_offering' => $type_offering,
                        'type_suites' => $type_suites,
                        'condo_area_apartment' => $condo_area_apartment,
                        'condo_room' => $condo_room,
                        'transportation_category' => $transportation_category,
                        'bts' => $bts,
                        'mrt' => $mrt,
                        'airportlink' => $airportlink,
                        'type_zone_id' => $type_zone_id,
                        'transportation_id' => $transportation_id,
                        'condo_owner_name' => $condo_owner_name,
                );

                $links = '';


                $data = $this->Page_model->get_search($data_seach,  0, 0);


                $condo_sidebar = $this->Condominium_model->get_condo_sidebar();

            $this->load->view('frontend/search', array(
                        'title' => 'รายการค้นหา',
                        'css' => array(''),
                        'js' => array(''),
                        'data' => $data,
                        'condo_sidebar' => $condo_sidebar,
                        'links' => $links,
                ));
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public function calculate_condo_loan()
  {
      $data = $this->security->xss_clean($this->input->post());

      $loan_price = $data['loan_price'];
      $loan_years = $data['loan_years']; //ปี
      $loan_interest = $data['loan_interest'];
    //F1
      $loan_recovery_period = $loan_years * 12;
      $loan_interest_per_month = $loan_interest / 100;

    //อัตราดอกเบี้ย ต่อปี
    if ($loan_interest === 'years') {
        $loan_interest = ($loan_interest / 100) / 12;
    }

      $loan_per_month = 0;
    //STEP 1
    // echo $step_in = pow((1 + 0.02), 6);
    // echo "<br />";
    // echo $step_inn = 1 / $step_in;
    // echo "<br />";
    // echo $step_inn1 = (1 - $step_inn) / 0.02;
    // echo "<br />";
    // echo $step_pmt = 12000 / $step_inn1;

    //เปลี่ยนก่อน
      // $step_in = pow((1 + $loan_interest_per_month), $loan_recovery_period);
      // $step_inn = 1 / $step_in;
      // $step_inn1 = (1 - $step_inn) / $loan_interest_per_month;
      // $step_pmt = $loan_price / $step_inn1;
      //.................

      $cost_per_periods = $loan_price / $loan_recovery_period;
      //3888.89 + 0.07
      $cost_pay = $cost_per_periods * $loan_interest_per_month;
      $step_pmt = $cost_per_periods + $cost_pay;

      echo number_format(ceil($step_pmt), 2);
    // 1 + 0.02 = 1.02
    // 1.02 ^ 6 = 1.126162419264
    // 1 / 1.126162419264 = 0.88797138218619205681631371948711
    // 1 - 0.88797138218619205681631371948711 = 0.11202861781380794318368628051289
    // 0.11202861781380794318368628051289 / 0.02 = 5.6014308906903971591843140256445
    // 5.6014308906903971591843140256445 / 12000 = 2142.31
  }
  //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function condoinside_share($id, $member_id=0)
    {


        $decode_member_id = $this->primaryclass->raw_decode($member_id);

        $data = $this->Page_model->condoinside($id);
        $condo_image = $this->Condominium_model->get_condo_image($id);
        $featured = $this->Condominium_model->get_featured_row_by_condo($id);
        $facilities = $this->Condominium_model->get_facilities_row_by_condo($id);
        $transportation = $this->Condominium_model->get_transportation_relation_row_by_condo($id);
        $update_visited = $this->M->update_visited('condo_id', $id, 'condo');
        $condo_title = $this->M->get_name_by_id('condo_id', 'condo_title', $id, 'condo');
        $condo_pic_thumb = $this->M->get_name_by_id('condo_id', 'pic_thumb', $id, 'condo');

        //HEADER SEO
        $condo_items = $this->M->get_data_by_id('condo_id, condo_title, pic_thumb, condo_expert', 'condo_id', $id, 'condo');
        $breadcrumbs_arr = array(
          '1' => array(
            'title' => 'รายการคอนโด',
            'href' => 'condominium',
          ),
          '2' => array(
            'title' => $data->type_zone_title,
            'href' => '../condominium',
          ),
          '3' => array(
            'title' => $condo_items['condo_title'],
            'href' => '../condominium-details/'.$condo_items['condo_id'],
          ),
        );
        $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

        $fk_condo_id = $id;
        $fk_member_id = $decode_member_id;
        $this->load->view('frontend/condo-inside', array(
            'title' => 'หน้ารายละเอียดคอนโด',
            'css' => array(
                'assets/css/jssocials.css',
                'assets/css/jssocials-theme-flat.css',
                'assets/css/doughnutit.min.css',
            ),
            'js' => array(
                'assets/js/jquery.uploadPreview.min.js',
            ),
            'data' => $data,
            'breadcrumbs' => $breadcrumbs,
            'condo_image' => $condo_image,
            'featured' => $featured,
            'facilities' => $facilities,
            'transportation' => $transportation,
            'fk_condo_id' => $fk_condo_id,
            'fk_member_id' => $fk_member_id,
            'condo_title' => $condo_title,
            'condo_pic_thumb' => $condo_pic_thumb,
            'isslider' => true,
            'sharepage' => true,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function member_share_facebook()
    {

        $id = $_POST['id'];
        $decode_member_id = $this->primaryclass->raw_decode($this->input->post('member_id'));

        $type_status_id = 1; //AUTO

        $data = array(
            'condo_id' => $id,
            'member_id' => $decode_member_id,
            'fk_type_status_id' => $type_status_id,
            'createdate' => date('Y-m-d H:i:s'),
            'sharedate' => date('Y-m-d H:i:s'),
        );
        // $this->primaryclass->pre_var_dump($data);
        $success = $this->Condominium_model->add_member_share_condo($data);
        $data_status = array(
            'condo_id' => $id,
            'modifydate' => date('Y-m-d H:i:s'),
        );
        $update_status = $this->Condominium_model->set_condo($data_status);


        $this->M->JSON($success);

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function list_wishlist_condo()
    {
        if (isset($this->session->mID) && !empty($this->session->mID)) {
            //$this->output->enable_profiler(TRUE);
            //Success
            $condo_id = $_POST['condo_id'];
            $member_id = $this->session->mID;

            $data = array(
                'condo_id' => $condo_id,
                'member_id' => $member_id,
                'createdate' => date('Y-m-d H:i:s'),
                'updatedate' => date('Y-m-d H:i:s'),
            );
            $update_status = $this->Condominium_model->add_wishlist_condo($data);
        } else {
            //Failed
            redirect('page/m_login');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function wishlist_condo()
    {
        if (isset($this->session->mID) && !empty($this->session->mID)) {
            //$this->output->enable_profiler(TRUE);
            //Success
            $condo_id = $_POST['condo_id'];
            $member_id = $this->session->mID;

            $data = array(
                'condo_id' => $condo_id,
                'member_id' => $member_id,
                'createdate' => date('Y-m-d H:i:s'),
                'updatedate' => date('Y-m-d H:i:s'),
            );
            $update_status = $this->Condominium_model->add_wishlist_condo($data);
        } else {
            //Failed
            redirect('page/m_login');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function list_compare_condo()
    {
        if (isset($this->session->mID) && !empty($this->session->mID)) {
            $condo_id_pricemax = $this->Condominium_model->get_max_price($_SESSION['sess_id']);
            $condo_id_pricemin = $this->Condominium_model->get_min_price($_SESSION['sess_id']);

            $data = $this->Condominium_model->get_compare_condo($_SESSION['sess_id']);

            $breadcrumbs_arr = array(
              '1' => array(
                'title' => 'รายการเปรียเทียบคอนโด',
                'href' => 'condocompare',
              ),
            );
            $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

            $this->load->view('frontend/condo-compare', array(
                'title' => 'รายการเปรียเทียบคอนโด',
                'data' => $data,
                'breadcrumbs' => $breadcrumbs,

            ));
        } else {
            //Failed
            redirect('page/m_login');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function compare_condo()
    {
        //Success
        $condo_id = $_POST['condo_id'];

        $data = array(
                'condo_id' => $condo_id,
                'sess_id' => $_SESSION['sess_id'],
                'createdate' => date('Y-m-d H:i:s'),
                'updatedate' => date('Y-m-d H:i:s'),
            );
        $update_status = $this->Condominium_model->insert_compare_condo($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function remove_compare()
    {
        //Success
        $compare_condo_id = $_POST['compare_condo_id'];
        $delete = $this->Condominium_model->delete_record('compare_condo_id', $compare_condo_id, 'compare_condo');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* Land */
    public function land_list()
    {
        //PAGINATION
      $url_page = base_url().'house-and-land';

        $data['land_total'] = $this->Page_model->land_all_record_count();
        if (!empty($this->input->get('order_by_list'))) {
            $per_page = $this->input->get('order_by_list');
        } else {
            $per_page = '15';
        }

        $page_pagination = $this->primaryclass->set_pagination($url_page, $data['land_total'], $per_page);
        $links = $this->pagination->create_links();

        $data['land'] = $this->Page_model->get_landlist($page_pagination['per_page'], $page_pagination['page']);


        $breadcrumbs_arr = array(
          '1' => array(
            'title' => 'รายการที่ดิน',
            'href' => 'house-and-land',
          ),
        );
        $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);


        $data['provinces'] = $this->M->get_provinces();
        $data['type_offering'] = $this->Configsystem_model->get_type_offering_option();
        $land_sidebar = $this->Land_model->get_land_sidebar();
        $this->load->view('frontend/land-list', array(
          'title' => 'รายการที่ดิน',
          'links' => $links,
          'data' => $data,
          'breadcrumbs' => $breadcrumbs,
          'land_sidebar' => $land_sidebar,
      ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function landinside($id, $member_id = 0)
    {
      $encode_member_id = $this->primaryclass->raw_encode($this->session->mID);
        $data = $this->Page_model->landinside($id);
        $land_image = $this->Land_model->get_land_image($id);
        $transportation = $this->Land_model->get_transportation_relation_row_by_land($id);
        $update_visited = $this->M->update_visited('land_id', $id, 'land');

        //HEADER SEO
        $land_items = $this->M->get_data_by_id('land_id, land_title, pic_thumb, land_expert', 'land_id', $id, 'land');

        $fk_land_id = $id;
        $fk_member_id = $encode_member_id;

        $breadcrumbs_arr = array(
          '1' => array(
            'title' => 'รายการที่ดิน',
            'href' => 'house-and-land',
          ),
        //   '2' => array(
        //     'title' => $data->type_zone_title,
        //     'href' => '../house-and-land',
        //   ),
          '2' => array(
            'title' => $data->land_title,
            'href' => '../../land-details/'.$data->land_id,
          ),
        );
        $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

        $this->load->view('frontend/land-inside', array(
            'title' => 'หน้ารายละเอียดที่ดิน',
            'css' => array(
                'assets/css/jssocials.css',
                'assets/css/jssocials-theme-flat.css',
                'assets/css/doughnutit.min.css',
            ),
            'js' => array(
                'assets/js/jquery.uploadPreview.min.js',
            ),
            'data' => $data,
            'breadcrumbs' => $breadcrumbs,
            'land_image' => $land_image,
            'transportation' => $transportation,
            'fk_land_id' => $fk_land_id,
            'fk_member_id' => $fk_member_id,
            'land_items' => $land_items,
            'isslider' => true,
            'sharepage' => true,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function landinside_share($id, $member_id=0)
    {
        //redirect('page/landinside/'.$id.'/'.$member_id);
        $decode_member_id = $this->primaryclass->raw_decode($member_id);

        $data = $this->Page_model->landinside($id);
        $land_image = $this->Land_model->get_land_image($id);
        $transportation = $this->Land_model->get_transportation_relation_row_by_land($id);
        $update_visited = $this->M->update_visited('land_id', $id, 'land');

        $land_title = $this->M->get_name_by_id('land_id', 'land_title', $id, 'land');
        $land_pic_thumb = $this->M->get_name_by_id('land_id', 'pic_thumb', $id, 'land');

        //HEADER SEO
        $land_items = $this->M->get_data_by_id('land_id, land_title, pic_thumb, land_expert', 'land_id', $id, 'land');

        $fk_land_id = $id;
        $fk_member_id = $decode_member_id;

        $breadcrumbs_arr = array(
          '1' => array(
            'title' => 'รายการที่ดิน',
            'href' => 'house-and-land',
          ),
          '2' => array(
            'title' => $data->type_zone_title,
            'href' => 'house-and-land',
          ),
          '3' => array(
            'title' => $data->land_title,
            'href' => '../../land-details/'.$data->land_id,
          ),
        );
        $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

        $this->load->view('frontend/land-inside', array(
            'title' => 'หน้ารายละเอียดที่ดิน',
            'css' => array(
                'assets/css/jssocials.css',
                'assets/css/jssocials-theme-flat.css',
                'assets/css/doughnutit.min.css',
            ),
            'js' => array(
                'assets/js/jquery.uploadPreview.min.js',
            ),
            'data' => $data,
            'breadcrumbs' => $breadcrumbs,
            'land_image' => $land_image,
            'transportation' => $transportation,
            'fk_land_id' => $fk_land_id,
            'fk_member_id' => $fk_member_id,
            'land_title' => $land_title,
            'land_pic_thumb' => $land_pic_thumb,
            'isslider' => true,
            'sharepage' => true,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function land_member_share_facebook()
    {
        // $data = $this->Page_model->landinside( $id );
        // $this->primaryclass->pre_var_dump($this->input->post());
        $id = $this->input->post('id');
        $decode_member_id = $this->primaryclass->raw_decode($this->input->post('member_id'));

        $type_status_id = 1; //AUTO

        $data = array(
            'land_id' => $id,
            'member_id' => $decode_member_id,
            'fk_type_status_id' => $type_status_id,
            'createdate' => date('Y-m-d H:i:s'),
            'sharedate' => date('Y-m-d H:i:s'),
        );
        $success = $this->Land_model->add_member_share_land($data);


        $data_status = array(
            'land_id' => $id,
            'modifydate' => date('Y-m-d H:i:s'),
        );
        $update_status = $this->Land_model->set_land_by_id($data_status);
        // $this->primaryclass->pre_var_dump();
        $this->M->JSON($success);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function land_list_wishlist_land()
    {
        if (isset($this->session->mID) && !empty($this->session->mID)) {
            //$this->output->enable_profiler(TRUE);
            //Success
            $land_id = $_POST['land_id'];
            $member_id = $this->session->mID;

            $data = array(
                'land_id' => $land_id,
                'member_id' => $member_id,
                'createdate' => date('Y-m-d H:i:s'),
                'updatedate' => date('Y-m-d H:i:s'),
            );
            $update_status = $this->Land_model->add_wishlist_land($data);
        } else {
            //Failed
            redirect('page/m_login');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function wishlist_land()
    {
        if (isset($this->session->mID) && !empty($this->session->mID)) {
            //$this->output->enable_profiler(TRUE);
            //Success
            $land_id = $_POST['land_id'];
            $member_id = $this->session->mID;

            $data = array(
                'land_id' => $land_id,
                'member_id' => $member_id,
                'createdate' => date('Y-m-d H:i:s'),
                'updatedate' => date('Y-m-d H:i:s'),
            );
            $update_status = $this->Land_model->add_wishlist_land($data);
        } else {
            //Failed
            redirect('page/m_login');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function santorini()
    {
        $this->load->view('frontend/santorini', array(
            'title' => 'ซานโตรินี่ หัวหิน',
            'css' => array('assets/css/slippry.min.css'),
            'js' => array('assets/js/slippry.min.js'),
            'data_category' => $data_category,
            'slippry' => 'slippry',
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function contactus()
    {
      $this->load->helper('captcha');
      $this->load->model('Captcha_model');

      // $this->primaryclass->pre_var_dump( $this->input->post('captcha') );

      if (!empty($this->input->post('action'))) {

        $success_captcha = $this->Captcha_model->get_captcha($this->input->post('captcha'));
        // $this->primaryclass->pre_var_dump($success_captcha);
        if( $success_captcha['success'] == TRUE  ){
          //echo '111'.$this->input->post('action');
          $data = $this->security->xss_clean($this->input->post());

          $email_subject = 'ฟอร์มการติดต่อจากสมาชิก';
          $email_message = "<h4>สวัสดีคุณ, $data[fullname]</h4>\n";
          $email_message .= "<p>ระบบได้ทำการบันทึกข้อมูลการติดต่อของท่านเรียบร้อยแล้ว จะมีเจ้าหน้าที่ทำการติดต่อท่านเพื่อทำการตอบ-กลับ เรื่องที่ท่านส่งมาให้ทางเรา,  ขอบคุณค่ะ</p>\n";
          $email_message .= "<hr>\n";
          $email_message .= "<p><storng>หัวข้อการติดต่อ : </storng> $data[subject]</p>\n";
          $email_message .= "<p>ข้อความ : $data[message]</p>\n";
          $email_message .= "<hr>\n";
          $email_message .= "<p><storng>เบอร์โทรศัพท์ : </storng> $data[telephone]</p>\n";

          $data = array(
              'email_subject' => $email_subject,
              'email_message' => $email_message,
              'email_files' => '',
              'email' => $data['email'],
          );
          $send_mail = $this->template_email_form($data);
          $this->M->JSON($send_mail);

        }else{
          $this->M->JSON($success_captcha);
        }
      }else{

        $data['captcha'] = $this->Captcha_model->create_captcha();

        $breadcrumbs_arr = array(
          '1' => array(
            'title' => 'ติดต่อเรา',
            'href' => 'contact-us',
          ),
        );
        $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

        $this->load->view('frontend/contactus', array(
            'title' => 'ติดต่อเรา',
            'css' => array(
              'assets/plugins/is-loading/isloading.css',
            ),
            'js' => array(
              'assets/js/plugins/foundation.abide.js',
              'assets/plugins/is-loading/jquery.isloading.min.js',
            ),
            'data' => $data,
            'breadcrumbs' => $breadcrumbs
        ));
      }

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function consignment_agents()
    {
      $this->load->helper('captcha');
      $this->load->model('Captcha_model');



      if (!empty($this->input->post('action'))) {

        $success_captcha = $this->Captcha_model->get_captcha($this->input->post('captcha'));
        if( $success_captcha['success'] == TRUE  ){

          $sfilepath = './uploads/file_agents/.';
          $paths = 'uploads/file_agents/';
          $smaxsize = 0; //4MB
          $smaxwidth = 0;
          $smaxheight = 0;
          $allowed_types = 'pdf|doc|docx|xls|xlsx|csv';
          $remove_space = 0;


          if ($_FILES['file_agents']) {
              $file_agents = $this->primaryclass->do_upload('file_agents', $sfilepath, $smaxsize, $smaxwidth, $smaxheight, $allowed_types, $remove_space);
              $file_agents_have_file = $file_agents['upload_data']['file_name'];
          } else {
              $pic_large_have_file = '';
          }//IF PIC THUMB=========================================================

          // $this->primaryclass->pre_var_dump(base_url().$sfilepath.$file_agents_have_file);

          $data = $this->security->xss_clean($this->input->post());

          $email_subject = 'ฟอร์มการติดต่อขอฝากรายการทรัพย์';
          $email_message = "<h4>สวัสดีคุณ, $data[fullname]</h4>\n";
          $email_message .= "<p>ระบบได้ทำการบันทึกข้อมูลการฝากทรัพย์ของท่านเรียบร้อยแล้ว จะมีเจ้าหน้าที่ทำการติดต่อท่านเพื่อทำการตอบ-กลับ เรื่องที่ท่านส่งมาให้ทางเรา,  ขอบคุณค่ะ</p>\n";
          $email_message .= "<hr>\n";
          $email_message .= "<p><storng>หัวข้อการติดต่อ : </storng> $data[subject]</p>\n";
          $email_message .= "<p>ข้อความ : $data[message]</p>\n";
          $email_message .= "<p>ไฟล์ทรัพย์ : <a href=\"base_url().$paths.$file_agents_have_file/\" target=\"_blank\">	$file_agents_have_file</p>\n";
          $email_message .= "<hr>\n";
          $email_message .= "<p><storng>เบอร์โทรศัพท์ : </storng> $data[telephone]</p>\n";
          $data = array(
              'email_subject' => $email_subject,
              'email_message' => $email_message,
              'email_files' => base_url().$paths.$file_agents_have_file,
              'email' => $data['email'],
          );
          $send_mail = $this->template_email_form($data);

          // $this->primaryclass->pre_var_dump($send_mail);
          if( $send_mail['success'] == true ){
            $success  = array('success'=> true, 'message' => 'ระบบได้ทำการส่งอีเมล์ การฝากขายทรัพย์ของท่านเรียบร้อยแล้ว..');
          }else{
            $success  = array('success'=> false, 'message' => 'เกิดข้อผิดพลาดในการฝากทรัพย์ กรุณาติดต่อผู้ดูแลระบบ..');
          }
          $this->M->JSON($success);

        }else{
          $this->M->JSON($success_captcha);
        }

      } else {

          $data['captcha'] = $this->Captcha_model->create_captcha();

          $breadcrumbs_arr = array(
            '1' => array(
              'title' => 'ฝากขายทรัพย์',
              'href' => 'consignment-agents',
            ),
          );
          $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

          $this->load->view('frontend/consignment-agents', array(
              'title' => 'ฝากขายทรัพย์',
              'css' => array(
                'assets/plugins/is-loading/isloading.css',
              ),
              'js' => array(
                'assets/js/plugins/foundation.abide.js',
                'assets/plugins/is-loading/jquery.isloading.min.js',
              ),
              'data' => $data,
              'breadcrumbs' => $breadcrumbs,
          ));
      }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    ////////////////////////////////////////***************////////////////////////////////////////
    //																					Frontend																				 //
    ////////////////////////////////////////***************////////////////////////////////////////

    /* LOGIN */
    public function m_login()
    {
      $this->load->helper('security');
      $this->load->library('encryption');
      $this->load->library('encrypt');

        if (isset($this->session->mID) && !empty($this->session->mID)) {
            redirect('member-profile');
        } else {
          $breadcrumbs_arr = array(
            '1' => array(
              'title' => 'เข้าสู่ระบบ Condo Compare',
              'href' => 'member-login',
            ),
          );
          $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

          $this->load->view('frontend/login', array(
              'title' => 'เข้าสู่ระบบ Condo Compare',
              'header' => 'yes',
              'facebook' => true,
              'url_register' => 'member-register',
              'js' => array(
                  'assets/pages/member/login.js',
                  'assets/pages/member/facebook.js',
              ),
              'breadcrumbs' => $breadcrumbs,
          ));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* LOGIN HOB */
    public function m_login_hob()
    {
        $this->load->library('session');
        $this->load->library('encryption');
        $key = bin2hex($this->encryption->create_key(6));

        if (isset($this->session->mID) && !empty($this->session->mID)) {
            redirect('member-profile');
        } else {

          $breadcrumbs_arr = array(
            '1' => array(
              'title' => 'เข้าสู่ระบบ HOB',
              'href' => 'member-login',
            ),
          );
          $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);
            $this->load->view('frontend/login', array(
                'title' => 'เข้าสู่ระบบ HOB',
                'header' => 'yes',
                'facebook' => false,
                'url_register' => 'member-register-hob',
                'js' => array(
                    'assets/pages/member/login.js',
                    'assets/pages/member/facebook.js',
                ),
                'breadcrumbs' => $breadcrumbs,
            ));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function m_login_success()
    {
        $this->load->library('session');
        //Field validation succeeded.  Validate against database
        $member_email = $this->input->post('member_email');
        $member_password = $this->input->post('member_password');

        $data = array(
            'member_email' => $this->input->post('member_email'),
            'member_password' => $this->input->post('member_password'),
        );

        $success = $this->Member_model->login($data);
        $this->M->JSON($success);

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* Forgot Password */
    public function m_forgotpassword()
    {
        $this->load->library('encrypt');

        if ($this->input->post('action') !== null) {
            $data = $this->security->xss_clean($this->input->post());
            $row = $this->Member_model->get_member_by_email($data['member_email']);

            if($row['success'] === TRUE){
                $member_id = (!empty($row->member_id)) ? $row->member_id : '';
                $member_email = (!empty($row->member_email)) ? $row->member_email : '';
                $member_fname = (!empty($row->member_fname)) ? $row->member_fname : '';
                $member_lname = (!empty($row->member_lname)) ? $row->member_lname : '';

                $key = $this->config->item('encryption_key');

                $this->encrypt->set_cipher(MCRYPT_BLOWFISH);
                $encode_member_id = $this->encrypt->encode($member_id, $key);

                $decode_member_id = $this->encrypt->decode($encode_member_id);

                // echo "<br />";
                // echo $encrypted_string = $this->encrypt->decode($encrypted_string, $key);


                // $en_ciphertext = $this->encryption->encrypt($member_id);
                $links_forgotpassword = site_url('form-forgot-password/'.$encode_member_id);

                // $de_ciphertext = $this->encryption->decrypt($en_ciphertext);

                $email_subject = "ระบบแจ้งการแก้ไขรหัสผ่านของคุณ, $member_email";
                $email_message = "<h4>สวัสดีคุณ, $member_fname $member_lname</h4>\n";
                $email_message .= "<p>ระบบได้ทำการติดต่อ เรื่องการลืมรหัสผ่าน การเข้าใช้งานเว็บไซต์ ของท่านแล้ว เราได้ทำการแนบลิ้งค์ มาในอีเมล์ ,</p>\n";
                $email_message .= "<p>คลิ๊กเพื่อทำการแก้ไขรหัสผ่าน <a href=\"$links_forgotpassword\" class=\"color-red\"> แก้ไขรหัสผ่านของท่าน </a> ขอบคุณค่ะ</p>\n";
                $email_message .= "<hr>\n";

                $data_email = array(
                    'email_subject' => $email_subject,
                    'email_message' => $email_message,
                    'email_files' => '',
                    'email' => $member_email,
                );
                // $this->primaryclass->pre_var_dump($data_email);
                $send_mail = $this->template_email_form($data_email);
                // var_dump( $send_mail );
                //return true;
            }else{

            }

        } else {

          $breadcrumbs_arr = array(
            '1' => array(
              'title' => 'ลืมรหัสผ่าน',
              'href' => 'forgot-password',
            ),
          );
          $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

            $this->load->view('frontend/forgotpassword', array(
                'title' => 'ลืมรหัสผ่าน',
                'header' => 'yes',
                'css' => array(
                    'assets/plugins/smoke/css/smoke.min.css',
                ),
                'js' => array(
                    'assets/plugins/smoke/js/smoke.min.js',
                ),
                'breadcrumbs' => $breadcrumbs,
            ));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* Forgot Password */
    public function m_form_forgotpassword($member_id = '')
    {
      $this->load->library('encrypt');
        if ($this->input->post('action') !== null) {
            // $data = $this->security->xss_clean($this->input->post());
            $success = $this->Member_model->set_member();
            $this->M->JSON($success);
            // $member_id = (!empty($row->member_id)) ? $row->member_id : '';
            // $member_email = (!empty($row->member_email)) ? $row->member_email : '';
            // $member_fname = (!empty($row->member_fname)) ? $row->member_fname : '';
            // $member_lname = (!empty($row->member_lname)) ? $row->member_lname : '';
        } else {
            // $decode_member_id = $this->encrypt->decode($encode_member_id);
            // $de_ciphertext = $this->encryption->decrypt($member_id);
            $key = $this->config->item('encryption_key');
            $this->encrypt->set_cipher(MCRYPT_BLOWFISH);
            $data['member_id'] =  $this->encrypt->decode($member_id, $key);
            // $this->primaryclass->pre_var_dump($data['member_id']);

            $breadcrumbs_arr = array(
              '1' => array(
                'title' => 'ตั้งรหัสผ่านใหม่',
                'href' => '#',
              ),
            );
            $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

            $this->load->view('frontend/form-forgotpassword', array(
                'title' => 'ตั้งรหัสผ่านใหม่',
                'header' => 'yes',
                'data' => $data,
                'breadcrumbs' => $breadcrumbs,
            ));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /*<<<<<< LOG OUT >>>>>*/
    public function m_logout()
    {
        $this->logout_facebook();
        $this->Member_model->logout();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* REGISTER */
    public function m_register()
    {
        $this->load->library('session');
        //echo $_COOKIE['mID'];
//		echo $this->session->has_userdata['mID'];
        $provinces = $this->M->get_provinces();
        $membertype = $this->Member_model->get_member_type();
        $member_type_id = 1;
        $approved = 2;

        $breadcrumbs_arr = array(
          '1' => array(
            'title' => 'สมัครสมาชิก Condo',
            'href' => 'member-register',
          ),
        );
        $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

        $this->load->view('frontend/register', array(
            'title' => 'สมัครสมาชิก Condo',
            'header' => 'yes',
            'css' => array(
                'assets/css/croppic.css',
            ),
            'js' => array(
                'assets/js/jquery.uploadPreview.min.js',
                'assets/js/croppic.min.js',
            ),
            'breadcrumbs' => $breadcrumbs,
            'member_type_id' => $member_type_id,
            'provinces' => $provinces,
            'membertype' => $membertype,
            'approved' => $approved,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function m_register_hob()
    {
        $provinces = $this->M->get_provinces();
        $membertype = $this->Member_model->get_member_type();
        $member_type_id = 2;
        $approved = 2;

        $breadcrumbs_arr = array(
          '1' => array(
            'title' => 'สมัครสมาชิก HOB',
            'href' => 'member-register-hob',
          ),
        );
        $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

        $this->load->view('frontend/register-hob', array(
            'title' => 'สมัครสมาชิก HOB',
            'header' => 'yes',
            'css' => array(
                'assets/css/croppic.css',
            ),
            'js' => array(
                'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
                'assets/pages/jquery.sweet-alert.init.js',
                'assets/js/jquery.uploadPreview.min.js',
                'assets/js/croppic.min.js',
            ),
            'breadcrumbs' => $breadcrumbs,
            'member_type_id' => $member_type_id,
            'provinces' => $provinces,
            'membertype' => $membertype,
            'approved' => $approved,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function m_register_success()
    {
        //$this->session->set_flashdata('message', 'คุณได้ทำการลงทะเบียนเรียบร้อยแล้วกรุณาผู้ดูแลระบบทำการอนุมัติการใช้งาน ขอบคุณค่ะ....');
        $sfilepath = './uploads/member/.';

        //$img_full_path = 'uploads/member/member_125523669.jpeg';
        $img_full_path = $this->input->post('member_profile');
        $reset_img_full_path = substr($img_full_path, 15);

        if ($this->input->post('member_password') != $this->input->post('cf_member_password')) {
            echo $msg = 'passwordnotmatch'; //passwordnotmatch
        } else {
            $data = array(
                'member_fname' => $this->input->post('member_fname'),
                'member_lname' => $this->input->post('member_lname'),
                'member_type_id' => $this->input->post('member_type_id'),
                'member_email' => $this->input->post('member_email'),
                'member_profile' => $reset_img_full_path,
                'member_mobileno' => $this->input->post('member_mobileno'),
                'member_password' => md5($this->input->post('member_password')),
                'member_address' => $this->input->post('member_address'),
                'provinces_id' => $this->input->post('provinces_id'),
                'districts_id' => $this->input->post('districts_id'),
                'sub_districts_id' => $this->input->post('sub_districts_id'),
                'zipcode' => $this->input->post('zipcode'),
                'ip' => $this->input->ip_address(),
                'active_id' => '2',
                'approved' => $this->input->post('approved'),
                'createdate' => date('Y-m-d H:i:s'),
            );
            //$this->primaryclass->pre_var_dump($data);
            $insert = $this->Member_model->add($data);
        }

        $success = ($insert == 'success') ? $this->template_email_register($data) : 'email_error';
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //call to fill the second dropdown with the provinces
    public function provinces_districts_relation()
    {
        //set selected country id from POST
        $provinces_id = $this->input->post('id', true);

        $data['districts'] = $this->M->get_districts($provinces_id);
        $output = null;

        //echo count($data['districts']);

        if (count($data['districts']) <= 0) {
            $output .= "<option value='0'>----เลือก----</option>";
        } else {
            $output .= "<option value='0'>----เลือก----</option>";
        }

        foreach ($data['districts'] as $row) {
            if (empty($row)) {
                $output .= "<option value='0'>ไม่มีข้อมูล</option>";
            } else {
                //here we build a dropdown item line for each query result
             $output .= "<option value='".$row->districts_id."'>".$row->districts_name.'</option>';
            }
        }
        echo $output;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //call to fill the second dropdown with the districts
    public function districts_subdistricts_relation()
    {
        //set selected country id from POST
        $subdistricts_id = $this->input->post('id', true);

        $data['sub_districts'] = $this->M->get_subdistricts($subdistricts_id);
        $output = null;
        if (count($data['sub_districts']) <= 0) {
            $output .= "<option value='0'>----เลือก----</option>";
        } else {
            $output .= "<option value='0'>----เลือก----</option>";
        }

        foreach ($data['sub_districts'] as $row) {
            if (empty($row)) {
                $output .= "<option value='0'>ไม่มีข้อมูล</option>";
            } else {
                //here we build a dropdown item line for each query result
                $output .= "<option value='".$row->sub_districts_id."'>".$row->sub_districts_name.'</option>';
            }
        }
        echo $output;

        //var_dump($districts);
        //run the query for the cities we specified earlier
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //call to fill the second dropdown with the provinces
    public function mass_transportation_relation()
    {
        //set selected country id from POST
        $mass_transportation_category_id = $this->input->post('id', true);

        $data['mass_transportation'] = $this->Condominium_model->get_mass_transportation($mass_transportation_category_id);
        $output = null;

        $this->primaryclass->pre_var_dump($data['mass_transportation']);

        if (count($data['mass_transportation']) <= 0) {
            $output .= "<option value='0'>----เลือก----</option>";
        } else {
            $output .= "<option value='0'>----เลือก----</option>";
        }

        foreach ($data['mass_transportation'] as $row) {
            if (empty($row)) {
                $output .= "<option value='0'>ไม่มีข้อมูล</option>";
            } else {
                //here we build a dropdown item line for each query result
             $output .= "<option value='".$row->mass_transportation_id."'>".$row->mass_transportation_title.'</option>';
            }
        }
        echo $output;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* PROFILE */
    public function m_profile()
    {
        if (isset($this->session->mID) && !empty($this->session->mID)) {
            //$this->output->enable_profiler(TRUE);
            //Success
            $data = $this->Member_model->getdata($this->session->mID);
            $provinces = $this->M->get_provinces();
            $breadcrumbs_arr = array(
              '1' => array(
                'title' => 'ข้อมูลส่วนตัว',
                'href' => 'member-profile',
              ),
            );
            $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);
            $this->load->view('frontend/profile', array(
                'title' => 'ข้อมูลส่วนตัว',
                'header' => 'yes',
                'css' => array(
                    'assets/css/croppic.css',
                ),
                'js' => array(
                    'assets/js/croppic.js',
                ),
                'data' => $data,
                'breadcrumbs' => $breadcrumbs,
                'provinces' => $provinces,
            ));
        } else {
            //			//Failed
            redirect('page/m_register');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function m_edit_profile()
    {
        if (isset($this->session->mID) && !empty($this->session->mID)) {

            /* Upload Image */
            $sfilepath = './uploads/member/.';

            $smaxsize = 4096000; //4MB
            $smaxwidth_pic_thumb = 2000;
            $smaxheight_pic_thumb = 2000;

            $smaxwidth_pic_large = 2000;
            $smaxheight_pic_large = 2000;

            if ($_FILES['member_profile']) {
                //			$member_profile = $this->primaryclass->do_upload( 'pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_pic_thumb, $smaxheight_pic_thumb );
                $member_profile = $this->primaryclass->do_upload('member_profile', $sfilepath, $smaxsize, $smaxwidth_pic_thumb, $smaxheight_pic_thumb);
                // $this->primaryclass->pre_var_dump($member_profile);

                if ($member_profile['upload_data']['file_name'] == '') {
                    $member_profile_have_file = $this->input->post('old_member_profile');
                } else {
                    $member_profile_have_file = $member_profile['upload_data']['file_name'];
                }
            } else {
                $member_profile_have_file = '';
            }//IF PIC THUMB=========================================================

            $data = array(
                'member_id' => $this->input->post('member_id'),
                'member_fname' => $this->input->post('member_fname'),
                'member_lname' => $this->input->post('member_lname'),
                'member_mobileno' => $this->input->post('member_mobileno'),
                'member_profile' => $member_profile_have_file,
                'member_address' => $this->input->post('member_address'),
                'provinces_id' => $this->input->post('provinces_id'),
                'districts_id' => $this->input->post('districts_id'),
                'sub_districts_id' => $this->input->post('sub_districts_id'),
                'zipcode' => $this->input->post('zipcode'),
                'ip' => $this->input->ip_address(),
                'updatedate' => date('Y-m-d H:i:s'),
            );
            //$this->primaryclass->pre_var_dump($data);

            //query the database
            $success = $this->Member_model->edit($data);
            // $this->primaryclass->pre_var_dump($success);
            $this->M->JSON($success);

        } else {
            //Failed
            redirect('page/m_register');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function m_change_password()
    {
        if (isset($this->session->mID) && !empty($this->session->mID)) {

            $this->load->library('encrypt');

            if ($this->input->post('action') !== null) {
                $data = $this->security->xss_clean($this->input->post());
                // $this->primaryclass->pre_var_dump($data);
                $password_match = $this->Member_model->get_forgotpassword($this->session->mID, $data['password']);

                if($password_match['success'] === TRUE){

                  $success = $this->Member_model->set_member();
                  $this->M->JSON($success);

                }else{
                  $this->M->JSON($password_match);
                }
            } else {

              $breadcrumbs_arr = array(
                '1' => array(
                  'title' => 'แก้ไขรหัสผ่าน',
                  'href' => 'member-profile',
                ),
              );
              $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);
                $this->load->view('frontend/profile-password', array(
                    'title' => 'แก้ไขรหัสผ่าน',
                    'header' => 'yes',
                    'css' => array(
                      'assets/plugins/is-loading/isloading.css',
                    ),
                    'js' => array(
                      'assets/js/plugins/foundation.abide.js',
                      'assets/plugins/is-loading/jquery.isloading.min.js',
                    ),
                    'breadcrumbs' => $breadcrumbs,
                ));
            }

        } else {
            //Failed
            redirect('member-login');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    public function remove_member_profile()
    {
        //POST
        $member_id = $this->input->post('member_id');
        $member_profile = $this->input->post('member_profile');

        //PATH
        $sfilepath = './uploads/member/.';

        $delete = $this->M->update_record_image('member_id', 'member_profile', $member_id, 'member');
        if ($delete) {
            unlink(base_url($sfilepath.$member_profile));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* MEMBER ALL CONDO */
    public function m_condo_approved()
    {
        if (isset($this->session->mID) && !empty($this->session->mID)) {
            //$this->output->enable_profiler(TRUE);
            //Success
            $member_id = $this->session->mID;
            $page = '';

            $breadcrumbs_arr = array(
              '1' => array(
                'title' => 'ประกาศที่อนุมัติ',
                'href' => 'member-condo-approved',
              ),
            );
            $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

            $condo_list = $this->Condominium_model->get_member_condo_approved($member_id);
            $this->load->view('frontend/member-all-condo', array(
                'title' => 'ประกาศที่อนุมัติ',
                'page' => $page,
                'header' => 'yes',
                'disable_link' => 'no',
                'condo_list' => $condo_list,
                'breadcrumbs' => $breadcrumbs,
            ));
        } else {
            //Failed
            redirect('page/m_register');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* MEMBER ALL CONDO */
    public function m_condo_unapproved()
    {
        if (isset($this->session->mID) && !empty($this->session->mID)) {
            //$this->output->enable_profiler(TRUE);
            //Success
            $member_id = $this->session->mID;
            $page = '';

            $condo_list = $this->Condominium_model->get_member_condo_unapproved($member_id);

            $breadcrumbs_arr = array(
              '1' => array(
                'title' => 'ประกาศที่รออนุมัติ',
                'href' => 'member-condo-unapproved',
              ),
            );
            $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);
            $this->load->view('frontend/member-all-condo', array(
                'title' => 'ประกาศที่รออนุมัติ',
                'page' => $page,
                'header' => 'yes',
                'disable_link' => 'yes',
                'condo_list' => $condo_list,
                'breadcrumbs' => $breadcrumbs,
            ));
        } else {
            //Failed
            redirect('page/m_register');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* MEMBER SHARE LIST CONDO */
    public function m_share_all_condo()
    {
        if (isset($this->session->mID) && !empty($this->session->mID)) {
            //$this->output->enable_profiler(TRUE);
            //Success
            $page = 'share_condo';
            $member_id = $this->session->mID;
            $condo_list = $this->Condominium_model->get_list_member_share_condo($member_id);

            $breadcrumbs_arr = array(
              '1' => array(
                'title' => 'ประกาศทั้งหมดที่ทำการแชร์',
                'href' => 'member-share-condo',
              ),
            );
            $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);
            $this->load->view('frontend/member-condo-list', array(
                'title' => 'ประกาศทั้งหมดที่ทำการแชร์',
                'header' => 'yes',
                'page' => $page,
                'condo_list' => $condo_list,
                'breadcrumbs' => $breadcrumbs,
            ));
        } else {
            //Failed
            redirect('page/m_register');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* MEMBER SHARE RENEWS */
    public function m_share_condo_renew_date()
    {
        //POST
            $member_share_condo_id = $this->input->post('id', true);

        $data = array(
                'member_share_condo_id' => $member_share_condo_id,
                'sharedate' => date('Y-m-d H:i:s'),
            );
            //QUERY
            $update = $this->Condominium_model->update_sharedate($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* MEMBER CONTACT CONDO */
    public function m_contact_share_condo()
    {
        if (isset($this->session->mID) && !empty($this->session->mID)) {
            //$this->output->enable_profiler(TRUE);
            //Success

            $page = 'share_condo';
            $condo_list = $this->Condominium_model->get_condo_share_form($this->session->mID);

            $breadcrumbs_arr = array(
              '1' => array(
                'title' => 'ข้อมูลการแชร์ประกาศ',
                'href' => 'member-contact-share-condo',
              ),
            );
            $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

            $this->load->view('frontend/member-condo-contact', array(
                'title' => 'ข้อมูลการแชร์ประกาศ',
                'header' => 'yes',
                'page' => $page,
                'condo_list' => $condo_list,
                'breadcrumbs' => $breadcrumbs,
            ));
        } else {
            //Failed
            redirect('member-login');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* MEMBER LIST CONTACT CONDO */
    public function m_contact_list_share_condo($condo_id, $member_id = 0)
    {
        if (isset($this->session->mID) && !empty($this->session->mID)) {
            $member_id = $this->session->mID;
            $page = 'share_condo';
            $contact_list = $this->Condominium_model->get_condo_contact_list($condo_id, $member_id);

            $breadcrumbs_arr = array(
              '1' => array(
                'title' => 'รายชื่อผู้ติดต่อการแชร์',
                'href' => 'member-info-share-condo',
              ),
            );
            $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

            $this->load->view('frontend/member-condo-contact-list', array(
                'title' => 'รายชื่อผู้ติดต่อการแชร์',
                'header' => 'yes',
                'page' => $page,
                'contact_list' => $contact_list,
                'breadcrumbs' => $breadcrumbs,
            ));
        } else {
            //Failed
            redirect('member-login');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function m_contact_list_update_status()
    {
        //POST
        $condo_share_form_id = $this->input->post('id', true);
        $condo_share_form_status = $this->input->post('status', true);

        $data = array(
            'condo_share_form_id' => $condo_share_form_id,
            'condo_share_form_status' => $condo_share_form_status,
            'updatedate' => date('Y-m-d H:i:s'),
        );
        //var_dump($data);
        //QUERY
        $update = $this->Condominium_model->update_condo_share_form_status($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* CONDO */
    public function m_add_condo()
    {
      $this->load->model('User_model');                                /***** LOADING Controller * User_model ****/
        //$this->load->library('googlemaps');
        if (isset($this->session->mID) && !empty($this->session->mID)) {
            $provinces = $this->M->get_provinces();
            $type_zone = $this->Configsystem_model->get_type_zone_option();
            $type_offering = $this->Configsystem_model->get_type_offering_option();
            $type_suites = $this->Configsystem_model->get_type_suites_option();
            $type_decoration = $this->Configsystem_model->get_type_decoration_option();
            $facilities = $this->Condominium_model->get_facilities_option();
            $featured = $this->Condominium_model->get_featured_option();
            $transportation_category = $this->Condominium_model->get_transportation_category_option();
            $staff_list = $this->User_model->get_staff_option();

            //$this->output->enable_profiler(TRUE);
            //Success
            $member_id = $this->session->mID;
            $condo_list = $this->Condominium_model->get_condo_by_member($member_id);

            $breadcrumbs_arr = array(
              '1' => array(
                'title' => 'สร้างประกาศคอนโด',
                'href' => 'member-create-post-condo',
              ),
            );
            $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

            $this->load->view('frontend/form-member-condo', array(
                'title' => 'สร้างประกาศคอนโด',
                'header' => 'yes',
                'googlemap' => 'yes',
                'type_zone' => $type_zone,
                'type_offering' => $type_offering,
                'type_suites' => $type_suites,
                'type_decoration' => $type_decoration,
                'facilities' => $facilities,
                'featured' => $featured,
                'transportation_category' => $transportation_category,
                'provinces' => $provinces,
                'breadcrumbs' => $breadcrumbs,
                'css' => array(
                    'assets/plugins/fileuploads/css/dropify.min.css',
                    'assets/css/dropzone.min.css',
                    'assets/plugins/timepicker/bootstrap-timepicker.min.css',
                    'assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                    'assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css',

                ),
                'js' => array(
                    'assets/plugins/tinymce/tinymce.min.js',
                    'assets/js/dropzone.min.js',
                    'assets/plugins/fileuploads/js/dropify.min.js',
                    'assets/plugins/moment/moment.js',
                    'assets/plugins/timepicker/bootstrap-timepicker.min.js',
                    'assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                    'assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js',
                    'assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js',
                ),
            ));
        } else {
            //Failed
            redirect('page/m_register');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //call to fill the second dropdown with the provinces
    public function transportation_category_option()
    {
        //set selected country id from POST
        $transportation_category_id = $this->input->post('id', true);

        $data['transportation_category'] = $this->Condominium_model->get_transportation_category_option();

        //echo count($data['transportation']);
        $output = null;

        $output .= '<div class="row">';
        $output .= '<div class="small-5 medium-4 columns">';
        $output .= '<span class="warning label">เลือกสถานที่ใกล้เคียง</span>';
        $output .= '<select id="transportation_category_id"  name="transportation_category_id[]" onchange="(this)">';
        foreach ($data['transportation_category'] as $row) {
            if (count($data['transportation_category']) <= 0) {
                //here we build a dropdown item line for each query result
                $output .= '<optgroup>';
                $output .= '<option value>---เลือก----</option>';
                $output .= '</optgroup>';
            } else {
                $output .= "<optgroup label='".$row->transportation_category_title."'>";

                //เอาข้อมูล Transportation by ID มา
                $data['transportation'] = $this->Condominium_model->get_transportation_option($row->transportation_category_id);
                foreach ($data['transportation'] as $row_in) {
                    //here we build a dropdown item line for each query result
                    $output .= "<option value='".$row_in->transportation_id."'>".$row_in->transportation_title.'</option>';
                }
                $output .= '</optgroup>';
            }
        }
        $output .= '</select>';
        $output .= '</div>';
        $output .= '<div class="small-5 medium-4 columns">';
        $output .= '<span class="warning label">ตัวอย่าง : 7 นาที &#40;1.46 กิโลเมตร&#41;</span>';
        $output .= '<input type="text" name="transportation_distance[]" class="inputmaxlength form-control" maxlength="50" placeholder="">';
        $output .= '</div>';
        $output .= '<div class="small-2 medium-2 columns">';
        $output .= '<a href="javascript:void(0);" class="remove_button"><i class="remove_button fa fa-minus"></i></a>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
        echo $output;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* MEMBER SHARE LIST CONDO */
    public function m_wishlist_condo()
    {
        if (isset($this->session->mID) && !empty($this->session->mID)) {
            //$this->output->enable_profiler(TRUE);
            //Success
            $member_id = $this->session->mID;
            $condo_list = $this->Condominium_model->get_wishlist_condo($member_id);

            $breadcrumbs_arr = array(
              '1' => array(
                'title' => 'รายการโปรด',
                'href' => 'wishlist-condo',
              ),
            );
            $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);
            $this->load->view('frontend/member-condo-wishlist', array(
                'title' => 'รายการโปรด',
                'header' => 'yes',
                'page' => 'wishlist',
                'condo_list' => $condo_list,
                'breadcrumbs' => $breadcrumbs,
            ));
        } else {
            //Failed
            redirect('page/m_login');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function m_delete_wishlist_condo()
    {
        $id = $_POST['id'];
        $delete = $this->Condominium_model->delete_record('wishlist_condo_id', $id, 'wishlist_condo');
        //$this->session->set_flashdata('message', 'Your data Delete Successfully..');
        redirect($_SERVER['REQUEST_URI'], 'refresh');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function login_facebook_member()
    {
        //query the database
        $success = $this->Member_model->get_login_facebook();
        // $this->primaryclass->pre_var_dump($success);
        $this->M->JSON($success);

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* EMAIL CONTACT CONDO */
    public function m_condo_email_success()
    {
        $insert_condo_share = $this->Condominium_model->insert_condo_share_form();
        // $this->primaryclass->pre_var_dump($insert_condo_share);

        $update_share = $this->Condominium_model->edit_member_share_condo();
        // $this->primaryclass->pre_var_dump($update_share);
        $type_status_id = 2; //AUTO

        $data = $this->security->xss_clean($this->input->post());

        $fk_member_id = (!empty($data['fk_member_id']))? $data['fk_member_id'] : 0;
        $fk_condo_id = (!empty($data['fk_condo_id']))? $data['fk_condo_id'] : 0;
        $fk_staff_id = (!empty($data['fk_staff_id']))? $data['fk_staff_id'] : 0;
        $enewsletter = (!empty($data['enewsletter']))? $data['enewsletter'] : 0;
        $condo_share_form_name = (!empty($data['condo_share_form_name']))? $data['condo_share_form_name'] : '';
        $condo_share_form_phone = (!empty($data['condo_share_form_phone']))? $data['condo_share_form_phone'] : '';
        $condo_share_form_email = (!empty($data['condo_share_form_email']))? $data['condo_share_form_email'] : '';
        $condo_share_form_message = (!empty($data['condo_share_form_message']))? $data['condo_share_form_message'] : '';
        $condo_share_form_status = (!empty($data['condo_share_form_status']))? $data['condo_share_form_status'] : 0;
        $data_email = array(
            'fk_condo_id' => $fk_condo_id,
            'fk_member_id' => $fk_member_id,
            'fk_staff_id' => $fk_staff_id,
            'condo_share_form_name' => $condo_share_form_name,
            'condo_share_form_phone' => $condo_share_form_phone,
            'condo_share_form_email' => $condo_share_form_email,
            'condo_share_form_message' => $condo_share_form_message,
            'fk_type_status_id' => $type_status_id,
            'enewsletter' => $enewsletter,
            'ipaddress' => $this->input->ip_address(),
            'updatedate' => date('Y-m-d H:i:s'),
            'createdate' => date('Y-m-d H:i:s'),
            'condo_share_form_status' => $condo_share_form_status,
        );
        $success = ($insert_condo_share == 'success') ? $this->template_condo_email($data_email) : 'email_error';
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function template_condo_email($data)
    {
      //Load Library
      $web = array();
        //Load Library
        $this->load->library('email');

        $staff = $this->M->get_staff('staff_id, fullname, email, phone', 'staff_id', $data['fk_staff_id'], 'staff');
        if ($data['fk_staff_id'] === '0' || empty($data['fk_staff_id'])) {
            $web['staff_fullname'] = '-';
            $web['staff_email'] = 'tee.emilond@gmail.com';
            $web['staff_phone'] = '-';
        } else {
            $web['staff_fullname'] = $staff->fullname;
            $web['staff_email'] = $staff->email;
            $web['staff_phone'] = $staff->phone;
        }

        $web['condo_title'] = $this->M->get_name_row('condo_title', 'condo_id', $data['fk_condo_id'], 'condo');

        $web['createdate_t'] = (isset($data['createdate']) || !empty($data['createdate'])) ? $this->dateclass->DateTimeShortFormat($data['createdate'], 0, 0, 'Th') : 'ยังไม่มีข้อมูล';

        $member = $this->M->get_email_member('member_id, member_email', 'member_id', $data['fk_member_id'], 'member');

        $site_setting = $this->M->get_main_setting($this->site_id);

        $web['subject'] = 'การติดต่อจากผู้สนใจการแชร์ของสมาชิก';
        $web['logo'] = (!empty($site_setting['site_logo'])) ? base_url().'uploads/site_setting/'.$site_setting['site_logo'] : 'assets/images/logo.jpg';

        $result  = array_merge($data, $web);
        $message = $this->load->view('email/share', $result, TRUE);
        // Also, for getting full html you may use the following internal method:
        //$body = $this->email->full_html($subject, $message);



        $site_email = (!empty($site_setting['site_email'])) ? $site_setting['site_email'] : 'hawk@gmail.com, wut@hosting.co.th, tee.emilond@gmail.com';
        // $this->primaryclass->pre_var_dump($result);

        $this->email->clear();
        $this->email->from('hob@condo-compare.com');
        $this->email->to($data['condo_share_form_email']);
        $this->email->cc($member->member_email);
        $this->email->cc($site_email);
        $this->email->subject($web['subject']);
        $this->email->message($message);

        if ( $this->email->send() )
        {
          $success = array('success'=>true, 'message'=>'ระบบได้ทำการส่งอีเมล์ การฝากขายทรัพย์ของท่านเรียบร้อยแล้ว..');
        }else{
          $success = array('success'=>true, 'message'=>'ระบบได้ทำการส่งอีเมล์ การฝากขายทรัพย์ของท่านเรียบร้อยแล้ว..');
        }
        return $success;

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Logout for web redirect example.
     *
     * @return [type] [description]
     */
    public function logout_facebook()
    {
        $this->facebook->destroy_session();
        //redirect('page/m_login', redirect);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    /* LAND */
    public function m_all_land()
    {
        if (isset($this->session->mID) && !empty($this->session->mID)) {
            //$this->output->enable_profiler(TRUE);
            //Success
            $member_id = $this->session->mID;
            $page = '';

            $land_list = $this->Land_model->get_all_land_by_member($member_id);

            $breadcrumbs_arr = array(
              '1' => array(
                'title' => 'บ้าน/ที่ดิน',
                'href' => 'member-house-and-land',
              ),
            );
            $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

            $this->load->view('frontend/member-all-land', array(
                'title' => 'บ้าน/ที่ดิน',
                'page' => $page,
                'header' => 'yes',
                'land_list' => $land_list,
                'breadcrumbs' => $breadcrumbs,
            ));
        } else {
            //Failed
            redirect('page/m_register');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* MEMBER SHARE LIST land */
    public function m_share_all_land()
    {
        if (isset($this->session->mID) && !empty($this->session->mID)) {
            //$this->output->enable_profiler(TRUE);
            //Success

            $page = 'share_land';
            $member_id = $this->session->mID;
            $land_list = $this->Land_model->get_list_member_share_land($member_id);

            $breadcrumbs_arr = array(
              '1' => array(
                'title' => 'ประกาศบ้าน/ที่ดิน ที่ทำการแชร์',
                'href' => 'member-share-house-and-land',
              ),
            );
            $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);
            $this->load->view('frontend/member-land-list', array(
                'title' => 'ประกาศบ้าน/ที่ดิน ที่ทำการแชร',
                'header' => 'yes',
                'page' => $page,
                'land_list' => $land_list,
                'breadcrumbs' => $breadcrumbs,
            ));
        } else {
            //Failed
            redirect('page/m_register');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* MEMBER SHARE RENEWS */
    public function m_share_land_renew_date()
    {
        //POST
            $member_share_land_id = $this->input->post('id', true);

        $data = array(
                'member_share_land_id' => $member_share_land_id,
                'sharedate' => date('Y-m-d H:i:s'),
            );
            //QUERY
            $update = $this->Land_model->update_sharedate($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* MEMBER CONTACT land */
    public function m_contact_share_land()
    {
        if (isset($this->session->mID) && !empty($this->session->mID)) {
            //$this->output->enable_profiler(TRUE);
            //Success
            $member_id = $this->session->mID;
            $page = 'share_land';
            $land_list = $this->Land_model->get_land_share_form($member_id);

            $breadcrumbs_arr = array(
              '1' => array(
                'title' => 'ประกาศบ้าน/ที่ดิน ได้รับการติดต่อ',
                'href' => 'member-contact-share-house-and-land',
              ),
            );
            $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

            $this->load->view('frontend/member-land-contact', array(
                'title' => 'ประกาศบ้าน/ที่ดิน ได้รับการติดต่อ',
                'header' => 'yes',
                'page' => $page,
                'land_list' => $land_list,
                'breadcrumbs' => $breadcrumbs,
            ));
        } else {
            //Failed
            redirect('member-login');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* MEMBER LIST CONTACT land */
    public function m_contact_list_share_land($land_id, $member_id = 0)
    {
        if (isset($this->session->mID) && !empty($this->session->mID)) {
            //$this->output->enable_profiler(TRUE);
            //Success
            $member_id = $this->session->mID;
            $page = 'share_land';
            $contact_list = $this->Land_model->get_land_contact_list($land_id, $member_id);

            $breadcrumbs_arr = array(
              '1' => array(
                'title' => 'รายชื่อผู้ติดต่อการแชร์ ประกาศบ้าน/ที่ดิน',
                'href' => '#',
              ),
            );
            $breadcrumbs = $this->primaryclass->breadcrumbs_page($breadcrumbs_arr);

            $this->load->view('frontend/member-land-contact-list', array(
                'title' => 'รายชื่อผู้ติดต่อการแชร์ ประกาศบ้าน/ที่ดิน',
                'header' => 'yes',
                'page' => $page,
                'contact_list' => $contact_list,
                'breadcrumbs' => $breadcrumbs,
            ));
        } else {
            //Failed
            redirect('member-login');
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function m_land_contact_list_update_status()
    {
        //POST
        $land_share_form_id = $this->input->post('id', true);
        $land_share_form_status = $this->input->post('status', true);

        $data = array(
            'land_share_form_id' => $land_share_form_id,
            'land_share_form_status' => $land_share_form_status,
            'updatedate' => date('Y-m-d H:i:s'),
        );
        //var_dump($data);
        //QUERY
        $update = $this->Land_model->update_land_share_form_status($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function m_land_email_success()
    {
      $data = $this->security->xss_clean($this->input->post());

        $type_status_id = 2; //AUTO

        $fk_member_id = (!empty($data['fk_member_id']))? $data['fk_member_id'] : 0;
        $fk_land_id = (!empty($data['fk_land_id']))? $data['fk_land_id'] : 0;
        $fk_staff_id = (!empty($data['fk_staff_id']))? $data['fk_staff_id'] : 0;
        $enewsletter = (!empty($data['enewsletter']))? $data['enewsletter'] : 0;
        $land_share_form_name = (!empty($data['land_share_form_name']))? $data['land_share_form_name'] : '';
        $land_share_form_phone = (!empty($data['land_share_form_phone']))? $data['land_share_form_phone'] : '';
        $land_share_form_email = (!empty($data['land_share_form_email']))? $data['land_share_form_email'] : '';
        $land_share_form_message = (!empty($data['land_share_form_message']))? $data['land_share_form_message'] : '';
        $land_share_form_status = (!empty($data['land_share_form_status']))? $data['land_share_form_status'] : 0;
        $data_share_form = array(
            'fk_land_id' => $fk_land_id,
            'fk_member_id' => $fk_member_id,
            'fk_staff_id' => $fk_staff_id,
            'land_share_form_name' => $land_share_form_name,
            'land_share_form_phone' => $land_share_form_phone,
            'land_share_form_email' => $land_share_form_email,
            'land_share_form_message' => $land_share_form_message,
            'fk_type_status_id' => $type_status_id,
            'enewsletter' => $enewsletter,
            'ipaddress' => $this->input->ip_address(),
            'createdate' => date('Y-m-d H:i:s'),
            'updatedate' => date('Y-m-d H:i:s'),
            'land_share_form_status' => $land_share_form_status,
        );
        $insert_share_form = $this->Land_model->insert_land_share_form($data_share_form);

        $data_share = array(
            'land_id' => $fk_land_id,
            'member_id' => $fk_member_id,
            'fk_type_status_id' => $type_status_id,
            'updatedate' => date('Y-m-d H:i:s'),
        );
        $update_share = $this->Land_model->edit_member_share_land($data_share);

        $success = ($insert_share_form == 'success') ? $this->template_land_email($data_share_form) : 'email_error';
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function template_land_email($data)
    {
      $web = array();
        //Load Library
        $this->load->library('email');

        $staff = $this->M->get_staff('staff_id, fullname, email, phone', 'staff_id', $data['fk_staff_id'], 'staff');
        if ($data['fk_staff_id'] === '0' || empty($data['fk_staff_id'])) {
            $web['staff_fullname'] = '-';
            $web['staff_email'] = 'tee.emilond@gmail.com';
            $web['staff_phone'] = '-';
        } else {
            $web['staff_fullname'] = $staff->fullname;
            $web['staff_email'] = $staff->email;
            $web['staff_phone'] = $staff->phone;
        }

        $web['land_title'] = $this->M->get_name_row('land_title', 'land_id', $data['fk_land_id'], 'land');

        $web['subject'] = 'การติดต่อจากผู้สนใจการแชร์ของสมาชิก';

        $web['createdate_t'] = (isset($data['createdate']) || !empty($data['createdate'])) ? $this->dateclass->DateTimeShortFormat($data['createdate'], 0, 0, 'Th') : 'ยังไม่มีข้อมูล';

        $member = $this->M->get_email_member('member_id, member_email', 'member_id', $data['fk_member_id'], 'member');

        $site_setting = $this->M->get_main_setting($this->site_id);

        $web['subject'] = 'การติดต่อจากผู้สนใจการแชร์ของสมาชิก';
        $web['logo'] = (!empty($site_setting['site_logo'])) ? base_url().'uploads/site_setting/'.$site_setting['site_logo'] : 'assets/images/logo.jpg';

        $result  = array_merge($data, $web);
        $message = $this->load->view('email/land-share', $result, TRUE);

        $site_email = (!empty($site_setting['site_email'])) ? $site_setting['site_email'] : 'hawk@gmail.com, wut@hosting.co.th, tee.emilond@gmail.com';

        $this->email->clear();
        $this->email->from('hob@condo-compare.com');
        $this->email->to($data['land_share_form_email']);
        $this->email->cc($member->member_email);
        $this->email->cc($site_email);
        $this->email->subject($web['subject']);
        $this->email->message($message);

        if ( $this->email->send() )
        {
          $success = array('success'=>true, 'message'=>'ระบบได้ทำการส่งอีเมล์ การฝากขายทรัพย์ของท่านเรียบร้อยแล้ว..');
        }else{
          $success = array('success'=>true, 'message'=>'ระบบได้ทำการส่งอีเมล์ การฝากขายทรัพย์ของท่านเรียบร้อยแล้ว..');
        }
        return $success;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //EMAIL TEMPLATE
    public function template_email_register($data)
    {
        //Load Library
        $web = array();
        $this->load->library('email');

        $subject = 'การสมัครสมาชิกเรียบร้อยแล้วค่ะ...';

        $site_setting = $this->M->get_main_setting($this->site_id);

        $web['website_name'] = base_url();
        $web['site_logo'] = (!empty($site_setting['site_logo'])) ? base_url().'uploads/site_setting/'.$site_setting['site_logo'] : base_url().'assets/images/logo.jpg';
        $site_email = (!empty($site_setting['site_email'])) ? $site_setting['site_email'] : 'hawk@gmail.com, wut@hosting.co.th, tee.emilond@gmail.com';

        $result  = array_merge($data, $web);
        $message = $this->load->view('email/register', $result, TRUE);

        $this->email->clear();
        $this->email->from('hob@condo-compare.com');
        $this->email->to($data['member_email']);
        $this->email->subject($subject);
        $this->email->message($message);

        if ( $this->email->send() )
        {
          $success = array('success'=>true, 'message'=>'ระบบได้ทำการส่งอีเมล์ การฝากขายทรัพย์ของท่านเรียบร้อยแล้ว..');
        }else{
          $success = array('success'=>true, 'message'=>'ระบบได้ทำการส่งอีเมล์ การฝากขายทรัพย์ของท่านเรียบร้อยแล้ว..');
        }
        return $success;
        //var_dump($result);
        //echo '<br />';
        //echo $this->email->print_debugger();
        // exit;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //EMAIL TEMPLATE
    public function template_email_form($data = array() )
    {
        //Load Library
        $web = array();
        $this->load->library('email');

        $site_setting = $this->M->get_main_setting($this->site_id);

        $web['website_name'] = base_url();
        $web['site_logo'] = (!empty($site_setting['site_logo'])) ? base_url().'uploads/site_setting/'.$site_setting['site_logo'] : base_url().'assets/images/logo.jpg';
        $site_email = (!empty($site_setting['site_email'])) ? $site_setting['site_email'] : 'hawk@gmail.com, wut@hosting.co.th, tee.emilond@gmail.com';

        $result  = array_merge($data, $web);
        $message = $this->load->view('email/main', $result, TRUE);

        // $this->primaryclass->pre_var_dump($result);
        $this->email->clear();
        $this->email->from('hob@condo-compare.com');
        // $this->email->reply_to('');    // Optional, an account where a human being reads.
        $this->email->to($data['email']);
        $this->email->cc($site_email);
        $this->email->attach($data['email_files']);
        $this->email->subject($data['email_subject']);
        $this->email->message($message);

        if ( $this->email->send() )
        {
          $success = array('success'=>true, 'message'=>'ระบบได้ทำการส่งอีเมล์ การฝากขายทรัพย์ของท่านเรียบร้อยแล้ว..');
        }else{
          $success = array('success'=>true, 'message'=>'ระบบได้ทำการส่งอีเมล์ การฝากขายทรัพย์ของท่านเรียบร้อยแล้ว..');
        }
            // var_dump($result);
        return $success;
        // echo '<br />';
        // echo $this->email->print_debugger();
        //exit;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function banner_clicked()
    {
        $this->load->model('Banner_model');
        $data = $this->security->xss_clean($this->input->post());
        $banner = $this->Banner_model->get_info($data['banner_id']);
        $banner_id = $banner->banner_id;
        $clicked = $banner->clicked;
        $banner_displayed = $this->M->update_clicked('banner_id', $clicked, $banner_id, 'banner');
    }
}
