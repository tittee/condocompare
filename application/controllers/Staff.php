<?php
defined('BASEPATH') OR exit();
class Staff extends CI_Controller {
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));								/***** LOADING HELPER TO AVOID PHP ERROR ****/
		$this->load->helper('date');
		$this->load->library('session');
		$this->load->model('Staff_model'); 										/* LOADING MODEL * Staff_model as staff */
		$this->Staff=$this->Staff_model->info();
		$this->load->library('Dateclass', 'dateclass'); 	/* LOADING Controller * Primaryfunc as all */
		$this->load->library('Primaryclass', 'primaryclass'); 	/* LOADING Controller * Primaryfunc as all */
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public function index(){
		$R=$this->Staff_model->info();
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function staff(){
		$data = $this->Staff_model->viewdata();
		$this->load->view('backoffice/staff/datatable',array(
			'title'=>'ตารางข้อมูลผู้ดูแลระบบ',
			'header'=>'yes',
			'url_href'=>'add_staff',
			'css'=>array(
				'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				'assets/plugins/datatables/jquery.dataTables.min.css',
				'assets/plugins/datatables/responsive.bootstrap.min.css',
			),
			'js'=>array(
				'assets/plugins/tinymce/tinymce.min.js',
				'assets/js/dropzone.min.js',
				'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
				'assets/pages/jquery.sweet-alert.init.js',
				'assets/plugins/datatables/jquery.dataTables.min.js',
				'assets/plugins/datatables/dataTables.bootstrap.js',
				'assets/plugins/datatables/dataTables.buttons.min.js',
				'assets/plugins/datatables/buttons.bootstrap.min.js',
				'assets/plugins/datatables/jszip.min.js',
				'assets/plugins/datatables/pdfmake.min.js',
				'assets/plugins/datatables/vfs_fonts.js',
				'assets/plugins/datatables/buttons.html5.min.js',
				'assets/plugins/datatables/buttons.print.min.js',
				'assets/plugins/datatables/dataTables.responsive.min.js',
				'assets/plugins/datatables/responsive.bootstrap.min.js',
				),
			'data'=>$data
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_staff(){

		$type_staff = $this->Staff_model->get_type_staff_option();
		$this->load->view('backoffice/staff/formadmin',array(
			'title'=>'เพิ่มข้อมูลผู้ดูแลระบบ',
			'header'=>'yes',
			'url_href'=>'add_staff',
			'formup'=>'add',
			'css'=>array(
				'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
			),
			'js'=>array(
				'assets/plugins/tinymce/tinymce.min.js',
				'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
				'assets/pages/jquery.sweet-alert.init.js',
			),
			'wysiwigeditor'=>'wysiwigeditor',
			'type_staff'=>$type_staff,
		));
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_data_staff(){

		$sfilepath = './uploads/staff/.';
		$smaxsize = 2048000; //2MB
		$smaxwidth = 200;
		$smaxheight = 200;

		if( $_FILES['avatar'] )
		{
			$avatar = $this->primaryclass->do_upload( 'avatar' , $sfilepath, $smaxsize, $smaxwidth, $smaxheight );
			if( $avatar['upload_data']['file_name'] == '' )
			{
				$avatar_have_file = $this->input->post('old_avatar');
			}else{
				$avatar_have_file = $avatar['upload_data']['file_name'];
			}
		}
		else
		{
			$avatar_have_file = '';
		}//IF PIC THUMB=========================================================


		$data=array(
			'username'=>$this->input->post('username'),
			'password'=>md5($this->input->post('password')),
			'email'=>$this->input->post('email'),
			'phone'=>$this->input->post('phone'),
			'avatar'=>$avatar_have_file,
			'fullname'=>$this->input->post('fullname'),
			'staff_group_id'=>$this->input->post('staff_group_id'),
			'approved'=>$this->input->post('approved'),
			'createdate'=>date("Y-m-d H:i:s"),
			'createby'=>$_SESSION['zID'],
			'ip'=>$this->input->ip_address(),
		);
//		var_dump($data);

		$insert = $this->Staff_model->insert_staff($data);
//		$this->session->set_flashdata('message', 'Your data inserted Successfully..');
		//redirect('staff/staff');
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function edit_staff($id)
	{
		$type_staff = $this->Staff_model->get_type_staff_option();

		$row = $this->Staff_model->getdata($id);

		$data['row'] = $row;
		//var_dump($row);
		($this->input->post())?$this->Staff_model->edit($data):$this->load->view('backoffice/staff/formadmin', array(
			'title'=>'เพิ่มดูแลระบบ',
			'header'=>'yes',
			'url_href'=>'edit_staff',
			'formup'=>'edit',
			'css'=>array(
				'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
			),
			'js'=>array(
				'assets/plugins/tinymce/tinymce.min.js',
				'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
				'assets/pages/jquery.sweet-alert.init.js',
			),
			'wysiwigeditor'=>'wysiwigeditor',
			'data'=>$data,
			'type_staff'=>$type_staff,
		));
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public function edit_data_staff()
	{
		$staff_id = $this->input->post('staff_id');
		/* Upload Image */
		$sfilepath = './uploads/staff/.';
		$smaxsize = 2048000; //2MB
		$smaxwidth_pic_thumb = 200;
		$smaxheight_pic_thumb = 200;

		// PIC THUMB===========================================================
		if( $_FILES['avatar'] )
		{
			$avatar = $this->primaryclass->do_upload( 'avatar' , $sfilepath, $smaxsize, $smaxwidth, $smaxheight );
			if( $avatar['upload_data']['file_name'] == '' )
			{
				$avatar_have_file = $this->input->post('old_avatar');
			}else{
				$avatar_have_file = $avatar['upload_data']['file_name'];
//				$this->primaryclass->pre_var_dump($avatar);
			}
		}
		else
		{
			$avatar_have_file = '';
		}//IF PIC THUMB=========================================================

		$data=$this->security->xss_clean($this->input->post());

		if( $data['chk_pwd'] === '1' )//เปลี่ยนรหัสผ่าน
		{
			$data=array(
				'staff_id'=>$data['staff_id'],
				'username'=>$data['username'],
				'password'=>md5($this->input->post('password')),
				'avatar'=>$avatar_have_file,
				'email'=>$data['email'],
				'phone'=>$data['phone'],
				'avatar'=>$avatar_have_file,
				'fullname'=>$data['fullname'],
				'staff_group_id'=>$data['staff_group_id'],
				'approved'=>$data['approved'],
				'updatedate'=>date("Y-m-d H:i:s"),
				'updateby'=>$_SESSION['zID'],
				'ip'=>$this->input->ip_address()
			);
		}else{
			$data=array(
				'staff_id'=>$data['staff_id'],
				'username'=>$data['username'],
				'avatar'=>$avatar_have_file,
				'email'=>$data['email'],
				'phone'=>$data['phone'],
				'avatar'=>$avatar_have_file,
				'fullname'=>$data['fullname'],
				'staff_group_id'=>$data['staff_group_id'],
				'approved'=>$data['approved'],
				'updatedate'=>date("Y-m-d H:i:s"),
				'updateby'=>$_SESSION['zID'],
				'ip'=>$this->input->ip_address()
			);
		}



		$update = $this->Staff_model->update_staff($data);
		//$this->session->set_flashdata('message', 'Your data Updated Successfully..');
		redirect('staff/staff');
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function delete_staff($id)
	{
		$delete = $this->M->delete_record('staff_id', $id, 'staff');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function approved_staff($id)
	{
		$data=array(
			'staff_id'=>$this->input->post('staff_id'),
			'approved'=>2,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);
		$publish = $this->Staff_model->update_staff($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function unapproved_staff($id)
	{
		$data=array(
			'staff_id'=>$this->input->post('staff_id'),
			'approved'=>1,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);
		$publish = $this->Staff_model->update_staff($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	public function do_upload($sfilename, $sfilepath)
	{
		$config['upload_path']          = $sfilepath;
		$config['allowed_types']        = 'jpeg|jpg|png';
		$config['max_size']             = 2048000;
		$config['max_width']            = 2400;
		$config['max_height']           = 1400;

		$this->load->library('upload', $config);

		if( $this->upload->do_upload( $sfilename ) )
		{
			return $data = array('upload_data' => $this->upload->data());

		}
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	public function check_login(){
		//Field validation succeeded.  Validate against database
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$data=array(
			'email'=>$this->input->post('email'),
			'password'=>$this->input->post('password'),
		);

		//query the database
		$result = $this->Staff_model->login($data);
		//echo $result;
//		var_dump($result);
		if($result === TRUE )
		{
			//Update Lastlogin
			$result = $this->Staff_model->update_lastlogin($data['email']);
			//redirect('backoffice/dashboard');
			echo '1';
		}
		else
		{
			echo '0';
			//$this->form_validation->set_message('check_database', 'Invalid email or user_password');
			return false;
		}
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	/* staff Category */
	public function type_staff()
	{
		//var_dump( $this->Staff );
		$data= $this->Staff_model->get_type_staff();

		$this->load->view('backoffice/staff/datatable-type-staff',array(
			'title'=>'ตารางประเภทผู้ดูแลระบบ',
			'header'=>'yes',
			'url_href'=>'add_type_staff',
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_type_staff()
	{
		if($this->input->post('action'))
		{
			// $data=$this->security->xss_clean($this->input->post());
			// $data = array(
			// 	'type_staff_title'=>$data['type_staff_title'],
			// 	'type_staff_detail'=>$data['type_staff_detail'],
			// 	'publish'=>$data['publish'],
			// 	'createdate'=>date("Y-m-d H:i:s"),
			// );
      //$this->primaryclass->pre_var_dump($data);
			$insert = $this->Staff_model->insert_type_staff();

		}
		else
		{
      $data['display_permission_rel'] = $this->display_permission_rel();
			$this->load->view('backoffice/staff/formadmin-type-staff',array(
				'title'=>'เพิ่มข้อมูลประเภทผู้ดูแลระบบ',
				'header'=>'yes',
				'formup'=>'add',
        'css'=>array(
          'assets/plugins/multiselect/css/multi-select.css'
        ),
        'js'=>array(
          'assets/plugins/multiselect/js/jquery.multi-select.js'
        ),
				'wysiwigeditor'=>'wysiwigeditor',
        'data'=>$data,
			));
		}

	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	public function edit_type_staff($id=0)
	{
		if($this->input->post('action'))
		{
			// $data=$this->security->xss_clean($this->input->post());
			// $data=array(
			// 	'type_staff_id'=>$data['type_staff_id'],
			// 	'type_staff_title'=>$data['type_staff_title'],
			// 	'type_staff_detail'=>$data['type_staff_detail'],
			// 	'publish'=>$data['publish'],
			// 	'updatedate'=>date("Y-m-d H:i:s"),
			// );
			//echo $this->Primaryclass->pre_var_dump($data);
			$update = $this->Staff_model->update_type_staff($id);
		}
		else
		{
			$row = $this->Staff_model->get_type_staff_record($id);
			$data['row'] = $row;
      $data['display_permission_rel'] = $this->display_permission_rel_edit($id);
	//		var_dump($row);

			//($this->input->post())?$this->Staff_model->edit_data_type_staff($data):$this->load->view('backoffice/staff/formadmin-type-staff', array(
			$this->load->view('backoffice/staff/formadmin-type-staff', array(
				'title'=>'แก้ไขข้อมูลประเภทผู้ดูแลระบบ',
				'header'=>'yes',
				'url_href'=>'edit',
				'formup'=>'edit',
        'css'=>array(
          'assets/plugins/multiselect/css/multi-select.css'
        ),
        'js'=>array(
          'assets/plugins/multiselect/js/jquery.multi-select.js'
        ),
				'wysiwigeditor'=>'wysiwigeditor',
				'data'=>$data,
			));
		}
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	public function display_type_staff($id)
	{
		$data=array(
			'type_staff_id'=>$this->input->post('type_staff_id'),
			'publish'=>2,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);

		//echo $this->primaryclass->pre_var_dump($_POST['id']);
		$publish = $this->Staff_model->update_type_staff($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function undisplay_type_staff($id)
	{
		$data=array(
			'type_staff_id'=>$this->input->post('type_staff_id'),
			'publish'=>1,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);
		//echo $_POST['type_staff_id'];
		//echo $this->primaryclass->pre_var_dump($data);
		$publish = $this->Staff_model->update_type_staff($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function delete_type_staff($id)
	{
		$delete = $this->Staff_model->delete_record('type_staff_id', $id, 'type_staff');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public function permissions_category()
  {
    $data['data'] = $this->Staff_model->get_permissions_category();
		$this->load->view('backoffice/staff/datatable-permissions-category',array(
			'title'=>'รายการหมวดสิทธิ์',
      'header'=>'yes',
			'url_href'=>'create_permissions_category',
			'data'=>$data,
		));
  }
  //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public function create_permissions_category()
  {
		($this->input->post('action'))? $this->Staff_model->set_permissions_category() : $this->load->view('backoffice/staff/formadmin-permissions-category',array(
			'title'=>'เพิ่มรายการหมวดสิทธิ์',
      'header'=>'yes',
		));
  }
  //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public function update_permissions_category($id = 0)
  {
    $data['row'] = $this->Staff_model->get_permissions_category_by_id($id);
		
    ($this->input->post('action'))? $this->Staff_model->set_permissions_category($id) : $this->load->view('backoffice/staff/formadmin-permissions-category',array(
      'title'=>'แก้ไขรายการหมวดสิทธิ์',
      'header'=>'yes',
      'data'=>$data
    ));
  }
  //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public function permissions()
  {
    $data['data'] = $this->Staff_model->get_permissions();
		$this->load->view('backoffice/staff/datatable-permissions',array(
			'title'=>'รายการสิทธิ์',
      'header'=>'yes',
			'url_href'=>'create_permissions',
			'data'=>$data,
		));
  }
  //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public function create_permissions()
  {
    $data['permissions_category'] = $this->Staff_model->get_permissions_category();
		($this->input->post('action'))? $this->Staff_model->set_permissions() : $this->load->view('backoffice/staff/formadmin-permissions',array(
			'title'=>'เพิ่มรายการสิทธิ์',
      'header'=>'yes',
      'data'=>$data,
		));
  }
  //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public function update_permissions($id = 0)
  {
    $data['row'] = $this->Staff_model->get_permissions_by_id($id);
    $data['permissions_category'] = $this->Staff_model->get_permissions_category();
    ($this->input->post('action'))? $this->Staff_model->set_permissions($id) : $this->load->view('backoffice/staff/formadmin-permissions',array(
      'title'=>'แก้ไขรายการสิทธิ์',
      'header'=>'yes',
      'data'=>$data
    ));
  }
  //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public function display_permission_rel()
  {
    $category = $this->Staff_model->get_permissions_category();

    $html = '';
    // $this->primaryclass->pre_var_dump($category);
    foreach( $category as $key1 => $value1)
    {
      $permissions_category_id = $value1->permissions_category_id;
      $permissions_category_title = $value1->permissions_category_title;
      $html .= '<optgroup value="'. $permissions_category_id .'" label="'.$permissions_category_title.'">';
      $permissions = $this->Staff_model->get_permissions_by_category_id($permissions_category_id);

      //$this->primaryclass->pre_var_dump($permissions);
      foreach ($permissions as $key2 => $value2)
      {
        $permissions_id = $value2->permissions_id;
        $permissions_title = $value2->permissions_title;
        $html .= '<option value="'. $permissions_id .'">'. $permissions_title . '</option>';
      }
      $html .= '</optgroup>';
    }
    return $html;
    # code...
  }
  //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public function display_permission_rel_edit($id = 0)
  {
    $category = $this->Staff_model->get_permissions_category();

    $html = '';
    // $this->primaryclass->pre_var_dump($category);

    if($id !== 0 )
    {
      foreach( $category as $key1 => $value1)
      {
        $permissions_category_id = $value1->permissions_category_id;
        $permissions_category_title = $value1->permissions_category_title;
        $html .= '<optgroup value="'. $permissions_category_id .'" label="'.$permissions_category_title.'">';
        $permissions = $this->Staff_model->get_permissions_by_category_id($permissions_category_id);


        //$this->primaryclass->pre_var_dump($permissions);
        foreach ($permissions as $key2 => $value2)
        {
          $permissions_id = $value2->permissions_id;
          $permissions_title = $value2->permissions_title;

          $rel_permissions = $this->Staff_model->get_permissions_relation_by_id($permissions_id, $id);
          if( !empty($rel_permissions) )
          {
            // echo $fk_permissions_id = $rel_permissions. '<br />';

            $fk_permissions_id = ( $permissions_id === $rel_permissions )? "selected" : '';
            // $this->primaryclass->pre_var_dump($rel_permissions);

            $html .= '<option value="'. $permissions_id .'" '.$fk_permissions_id.'>'. $permissions_title . '</option>';
          }
          else
          {
            # code...
            $html .= '<option value="'. $permissions_id .'">'. $permissions_title . '</option>';
          }
        }
        $html .= '</optgroup>';
      }
    }

    return $html;
    # code...
  }

}
