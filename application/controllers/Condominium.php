<?php

defined('BASEPATH') or exit();
class Condominium extends CI_Controller
{
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));                                /***** LOADING HELPER TO AVOID PHP ERROR ****/
        $this->load->library('encryption');
        $this->load->library('primaryclass');
        $this->load->library('dateclass');
        $this->load->library('upload');
        $this->load->model('Staff_model');                                        /* LOADING MODEL * Staff_model as staff */
        $this->Staff = $this->Staff_model->info();
        $this->load->model('Condominium_model');                                /***** LOADING Controller * Primaryfunc as all ****/
        $this->load->model('Member_model', 'Member');                                /***** LOADING Controller * Primaryfunc as all ****/
        $this->load->model('Configsystem_model', 'Configsystem');                                /***** LOADING Controller * Configsystem_model ****/
        $this->load->model('M');                                /***** LOADING Controller * Configsystem_model ****/
//			$this->output->enable_profiler(TRUE);
    }
    //-----------------share_with_contact-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function index()
    {
        $R = $this->Staff_model->info();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function view()
    {
        $data = array();
        $data = $this->Condominium_model->get_condo();
        $this->load->view('backoffice/condominium/__datatable', array(
            'title' => 'ตารางข้อมูลคอนโด',
            'header' => 'yes',
            'url_href' => 'add',
            'js' => array(
                'assets/plugins/moment/moment.js',
                'assets/plugins/moment/min/locales.js',
                'assets/plugins/moment/min/moment-with-locales.min.js',
            ),
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public function json_condo()
    {
        $list = $this->Condominium_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $condo) {
          $no++;
            $id = $condo->condo_id;
            $condo_title = $condo->condo_title;
            $visited = $condo->visited;

            $output_id = '<input type="checkbox" name="condo_id[]" class="condo_id" value="'.$id.'"> '. $no;

            $pic_thumb = base_url().'uploads/condominium/'.$condo->pic_thumb;
            $pic_thumb_path = '<img src="'.$pic_thumb.'" style="width: 100px;" class="img-thumbnail" alt="">';

            $output_title = '';
            $output_title .= $condo_title . '<br />';
    				$output_title .= 'จำนวนคนดู : ' . $visited;

            $condo_property_id = ( !empty($condo->condo_property_id) )? $condo->condo_property_id : 'ยังไม่ทำการระบุ';

            $createdate = $condo->createdate;
            $modifydate = $condo->modifydate;
            $createdate_t = ($createdate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
            $modifydate_t = ($modifydate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($modifydate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
            $approved = $this->primaryclass->get_approved($condo->approved);

            $output_date = '';
            $output_date .= 'วันที่สร้าง : ' . $createdate_t . '<br />';
            $output_date .= 'วันที่แก้ไข : ' . $modifydate_t . '<br />';
    				$output_date .= 'การแสดงผล : ' . $approved;

            $path_edit = site_url('condominium/edit/'.$id);
    				$path_images = site_url('condominium/images/'.$id);
    				$path_share = site_url('condominium/share/'.$id);
    				$output_manage = '';
    				$output_manage .= '<a href='. $path_edit .' class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5"><i class="fa fa-pencil"></i></a>';
    				$output_manage .= '<a href='. $path_images .' class="btn btn-icon waves-effect waves-light btn-purple m-b-5 m-r-5"><i class="zmdi zmdi-collection-image-o"></i></a>';
            if( $approved == 1){
    				$output_manage .= '<a href="#" class="btn btn-approved btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id='. $id .'><i class="zmdi zmdi-lock-open"></i></a>';
            }else {
              $output_manage .= '<a href="#" class="btn btn-unapproved btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id='. $id .'><i class="zmdi zmdi-lock-outline"></i></a>';
            }
    				$output_manage .= '<a href="#" id='. $id .' class="btn btn-delete btn-icon waves-effect waves-light btn-danger m-b-5 m-r-5"><i class="ti-trash"></i></a>';
    				$output_manage .= '<a href='.$path_share.' class="btn btn-icon waves-effect waves-light btn-pink m-b-5 m-r-5"><i class="zmdi zmdi-share"></i></a>';


            $row = array();
            $row[] = $output_id;
            $row[] = $pic_thumb_path;
            $row[] = $output_title;
            $row[] = $condo_property_id;
            $row[] = $output_date;
            $row[] = $output_manage;

            $data[] = $row;
        }

        $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->Condominium_model->count_all(),
                "recordsFiltered" => $this->Condominium_model->count_filtered(),
                "data" => $data,
        );
        //output to json format
        echo json_encode($output);


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function json_notification_condo()
    {
        $list = $this->Condominium_model->get_notif_datatables();
        $data = array();
        $no = (isset($_POST['start']))? $_POST['start'] : '';
        foreach ($list as $condo) {
            $no++;

            $condo_share_form_id = $condo->condo_share_form_id;
            $fk_member_id = $condo->fk_member_id;
            $fk_condo_id = $condo->fk_condo_id;

            $output_id = $no;

            $condo_title = $this->M->get_name_row('condo_title', 'condo_id', $fk_condo_id, 'condo');
            // $this->primaryclass->pre_var_dump($condo_title);
            $createdate = $condo->createdate;
            $createdate_t = ($createdate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, "Th") : 'ไม่ระบุ';

            $path_condo = site_url('condominium-details/'.$fk_condo_id);
            $output_title = '';
            $output_title .= '<a href='. $path_condo .' target="_blank">'.$condo_title.'</a><br />';
            $output_title .= 'วันที่ติดต่อ : ' . $createdate_t;

            $condo_share_form_name = $condo->condo_share_form_name;
            $condo_share_form_phone = $condo->condo_share_form_phone;
            $condo_share_form_email = $condo->condo_share_form_email;
            $condo_share_form_message = $condo->condo_share_form_message;




            $type_status_id = $condo->fk_type_status_id;
            $type_status_title = $this->Configsystem->get_type_status_title($type_status_id);
            $path_share = site_url('condominium/share/'.$fk_condo_id);

            $output_manage = '';
    				$output_manage .= '<a href='. $path_share .' class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5"><i class="fa fa-pencil"></i></a>';

            $row = array();
            $row[] = $output_id;
            $row[] = $output_title;
            $row[] = $condo_share_form_name;
            $row[] = $condo_share_form_phone;
            $row[] = $condo_share_form_email;
            $row[] = $condo_share_form_message;
            $row[] = $type_status_title;
            $row[] = $output_manage;

            $data[] = $row;
        }

        $output = array(
                "draw" => (isset($_POST['draw']))? $_POST['draw'] : '',
                "recordsTotal" => $this->Condominium_model->count_all_notification(),
                "recordsFiltered" => $this->Condominium_model->count_filtered_notification(),
                "data" => $data,
        );
        //output to json format
        echo json_encode($output);


    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add()
    {
        $provinces = $this->M->get_provinces();
        $type_zone = $this->Configsystem->get_type_zone_option();
        $type_offering = $this->Configsystem->get_type_offering_option();
        $type_suites = $this->Configsystem->get_type_suites_option();
        $type_decoration = $this->Configsystem->get_type_decoration_option();
        $facilities = $this->Condominium_model->get_facilities_option();
        $featured = $this->Condominium_model->get_featured_option();
        $transportation_category = $this->Condominium_model->get_transportation_category_option();
        $mass_transportation_category = $this->Condominium_model->get_mass_transportation_category_option();
        $staff_list = $this->User_model->get_staff_option();

        $data = array();

        //$transportation = $this->Condominium_model->get_transportation_option(0);
        $member = $this->Member->get_fullname();

        $this->load->view('backoffice/condominium/formadmin', array(
            'title' => 'เพิ่มข้อมูลคอนโด',
            'googlemap' => 'yes',
            'header' => 'yes',
            'css' => array(
                'assets/plugins/fileuploads/css/dropify.min.css',
                'assets/plugins/timepicker/bootstrap-timepicker.min.css',
                'assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
            ),
            'js' => array(
                'assets/plugins/moment/moment.js',
                'assets/plugins/fileuploads/js/dropify.min.js',
                'assets/plugins/timepicker/bootstrap-timepicker.min.js',
                'assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
            ),
            'wysiwigeditor' => 'wysiwigeditor',
            'provinces' => $provinces,
            'type_zone' => $type_zone,
            'type_offering' => $type_offering,
            'type_suites' => $type_suites,
            'type_decoration' => $type_decoration,
            'facilities' => $facilities,
            'featured' => $featured,
            'transportation_category' => $transportation_category,
            'member' => $member,
            'staff_list' => $staff_list,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function images($condo_id = 0)
    {
        $data = $this->Condominium_model->get_condo_image($condo_id);

        $this->load->view('backoffice/condominium/datatable-image', array(
            'title' => 'รูปภาพคอนโดเพิ่มเติม',
            'header' => 'yes',
            'url_href' => '../add_image/'.$condo_id,
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_image($condo_id = 0, $id = 0)
    {
        $R = $this->Staff_model->info();
        if ($this->input->post('action')) {
            //มี action หลังจากการ Submit
            $sfilepath = './uploads/condominium/.';
            $smaxsize = 4096000; //4MB
            $smaxwidth = 2000;
            $smaxheight = 2000;

            for ($i = 1; $i <= 5; ++$i) {
                // PIC LARGE===========================================================
                if ($_FILES['pic_large_'.$i]) {
                    $pic_large[$i] = $this->primaryclass->do_upload('pic_large_'.$i, $sfilepath, $smaxsize, $smaxwidth, $smaxheight);
                    //$this->primaryclass->pre_var_dump($pic_large[$i]['upload_data']['file_name']);

                    if (!empty($pic_large[$i])) {
                        $data = $this->security->xss_clean($this->input->post());
                        $data = array(
                            'condo_id' => $this->input->post('condo_id'),
                            'condo_images_title' => $this->input->post('condo_images_title'),
                            'condo_images_description' => $this->input->post('condo_images_description'),
                            'pic_large' => $pic_large[$i]['upload_data']['file_name'],
                            'condo_images_url' => $this->input->post('condo_images_url'),
                            'createdate' => date('Y-m-d H:i:s'),
                            'publish' => $this->input->post('publish'),
                        );
                        $insert = $this->Condominium_model->set_image_condominium(0, $data);
                    }
                } else {
                    $pic_large[$i] = '';
                }
                // END PIC LARGE=======================================================
            }
        } else {
            //ไม่มี ACTION

            $this->load->view('backoffice/condominium/formadmin-image', array(
                'title' => 'เพิ่มรูปภาพข้อมูลคอนโด',
                'header' => 'yes',
                'BodyClass' => 'fixed-left',
                'wysiwigeditor' => 'wysiwigeditor',
            ));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_image($condo_id = 0, $id = 0)
    {
        $condo_id = $this->uri->segment(3);
        $id = $this->uri->segment(4);
        if (empty($id)) {
            show_404();
        }

        $R = $this->Staff_model->info();
        if ($this->input->post('action')) {
            //มี action หลังจากการ Submit
            $sfilepath = './uploads/condominium/.';
            $smaxsize = 4096000; //4MB
            $smaxwidth = 2000;
            $smaxheight = 2000;
            // PIC THUMB===========================================================
            if ($_FILES['pic_large']) {
                $pic_large = $this->primaryclass->do_upload('pic_large', $sfilepath, $smaxsize, $smaxwidth, $smaxheight);

                if ($pic_large['upload_data']['file_name'] == '') {
                    $pic_large_have_file = $this->input->post('old_pic_large');
                } else {
                    $pic_large_have_file = $pic_large['upload_data']['file_name'];
                }
            } else {
                $pic_large_have_file = '';
            }//IF PIC THUMB=========================================================

            $data = $this->security->xss_clean($this->input->post());
            $this->primaryclass->pre_var_dump($data);
            $data = array(
                'condo_id' => $data['condo_id'],
                'condo_images_title' => $data['condo_images_title'],
                'condo_images_description' => $data['condo_images_description'],
                'pic_large' => $pic_large_have_file,
                'updatedate' => date('Y-m-d H:i:s'),
                'publish' => $this->input->post('publish'),
            );
            $this->primaryclass->pre_var_dump($data);
            echo $update = $this->Condominium_model->set_image_condominium($id, $data);
        } else {
            $data = array();
            $row = $this->Condominium_model->get_condo_image_by_id($condo_id, $id);

            $data['row'] = $row;
            //ไม่มี ACTION
            $this->load->view('backoffice/condominium/formadmin-image', array(
                'title' => 'แก้ไขรูปภาพข้อมูลคอนโด',

                'header' => 'yes',
                'BodyClass' => 'fixed-left',
                'wysiwigeditor' => 'wysiwigeditor',
                'data' => $data,
            ));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function remove_condo_images_update()
    {
        $data = $this->security->xss_clean($this->input->post());
        $data = array(
            'condo_images_id' => $data['condo_images_id'],
            $this->input->post('field_name') => null,
        );
            //$this->primaryclass->pre_var_dump($data);

        $sfilepath = 'uploads/condominium/';
        $imagefile = $sfilepath.$this->input->post('imagefile');
        $this->M->update_record_dalete_image($data, $imagefile, 'condo_images');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function display_condo_images($id)
    {
        $data = array(
            'condo_images_id' => $this->input->post('condo_images_id'),
            'publish' => 2,
            'updatedate' => date('Y-m-d H:i:s'),
        );

        //echo $this->primaryclass->pre_var_dump($_POST['id']);
        $publish = $this->Condominium_model->update_condo_images($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function undisplay_condo_images($id)
    {
        $data = array(
            'condo_images_id' => $this->input->post('condo_images_id'),
            'publish' => 1,
            'updatedate' => date('Y-m-d H:i:s'),
        );
        //echo $_POST['condo_images_id'];
        //echo $this->primaryclass->pre_var_dump($data);
        $publish = $this->Condominium_model->update_condo_images($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function share($id)
    {
        $data['data'] = $this->Condominium_model->get_share_condo($id);
        $data['type_status'] = $this->Configsystem->get_type_status_option();

        $this->load->view('backoffice/condominium/datatable-share', array(
            'title' => 'การแชร์คอนโด',
            'header' => 'yes',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function share_with_contact($id = 0, $member_share_id = 0)
    {
        //$data= $this->Condominium_model->get_share_condo($id);
        $data['data'] = $this->Condominium_model->get_condo_contact_list($id, $member_share_id);
        $data['type_status'] = $this->Configsystem->get_type_status_option();

        $this->load->view('backoffice/condominium/datatable-share-form', array(
            'title' => 'การแชร์คอนโดที่ได้รับการติดต่อ',
            'header' => 'yes',
            'url_href' => 'add',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function view_notification()
    {
        //$data= $this->Condominium_model->get_share_condo($id);
        // $data['data'] = $this->Condominium_model->get_condo_contact_list($id, $member_share_id);

        $this->load->view('backoffice/condominium/datatable-share-notif', array(
            'title' => 'คอนโดรายการติดต่อใหม่',
            'header' => 'yes',
            'url_href' => '#',
            'js' => array(
                'assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',

            ),
            // 'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_all_status_share() //AJAX
    {

//		$data =  json_encode($this->security->xss_clean($this->input->post()));
        $data = $this->security->xss_clean($this->input->raw_input_stream);
            //$this->primaryclass->pre_var_dump($data);

//		var_dump(json_decode($data, true) );

        $decoded = json_decode($data, true);
        foreach ($decoded as $value) {
            $$value['name'] = $value['value']."\n";
//				echo $value["name"] . "=" . $$value["name"]."\n";
//				echo "";

            $data = array(
                $value['name'] => $$value['name'],
            );
            $this->primaryclass->pre_var_dump($data);
        }
        //$member_share_condo_id =  $this->security->xss_clean($this->input->post('member_share_condo_id'));

//		$json = json_decode($data);
//		foreach($json->entries as $row)
//		{
//				foreach($row as $key => $val)
//				{
//						echo $key . ': ' . $val;
//						echo '<br>';
//				}
//		}

//		$array1 = array();
//		foreach($member_share_condo_id as $row => $val)
//		{
//			$arr_member_share_condo_id = array(
//				'member_share_condo_id'=>$val
//			); // $val . "\n"
//		}
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_status_share($id)
    {
        $data['data'] = $this->Condominium_model->get_share_condo($id);
        $data['type_status'] = $this->Configsystem->get_type_status_option();

        $this->load->view('backoffice/condominium/datatable-share', array(
            'title' => 'การแชร์คอนโด',
            'header' => 'yes',
            'css' => array(
                'assets/plugins/custombox/dist/custombox.min.css',
            ),
            'js' => array(
                'assets/plugins/custombox/dist/custombox.min.js',
                'assets/plugins/custombox/dist/legacy.min.js',
            ),
            'data' => $data,
        ));
//		$this->output->enable_profiler(TRUE);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_modal_status_share()
    {
        //Load Library
        $this->load->library('email');

        $data = $this->security->xss_clean($this->input->post());

        //$this->primaryclass->pre_var_dump($data);
        $data = array(
            'member_share_condo_id' => $data['member_share_condo_id'],
            'fk_type_status_id' => $data['fk_type_status_id'],
        );
        if (!empty($data)) {
            if ($this->input->post('action') === 'form-share') {
                $update = $this->M->update_record($data, 'member_share_condo');
                if ($update) {
                    $subject = 'อีเมล์ของระบบ';

                    $data_email = array();
                    $data_email = array(
                        'condo_id' => $this->input->post('condo_id'),
                        'member_share_condo_id' => $this->input->post('member_share_condo_id'),
                        'fk_type_status_id' => $this->input->post('fk_type_status_id'),
                    );
                    $message = $this->email_send_status($data_email);

                    $result = $this->email
                        ->from('hob@condo-compare.com')
                        ->reply_to('supawat@atmoststudio.com')    // Optional, an account where a human being reads.
                        ->to('tee.emilond@gmail.com')
                        ->subject($subject)
                        ->message($message['message'])
                        ->send();
                }
            }//END IF
        }//END IF
        return $data;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_modal_status_share_form()
    {
        //Load Library
        $this->load->library('email');

        $data = $this->security->xss_clean($this->input->post());

        // $this->primaryclass->pre_var_dump($data);

        if (!empty($data)) {
            if ($this->input->post('action') === 'form-share') {
                $data = array(
                    'condo_share_form_id' => $data['condo_share_form_id'],
                    'fk_condo_id' => $data['fk_condo_id'],
                    'fk_member_id' => $data['fk_member_id'],
                    'fk_type_status_id' => $data['fk_type_status_id'],
                );

                $update = $this->Condominium_model->update_condo_share_form_status($data);
                if ($update) {

                    ///////// EMAIL /////////

                    $fk_condo_id = $data['fk_condo_id'];
                    $fk_member_id = $data['fk_member_id'];
                    //สถานะ
                    $fk_type_status_id = (!empty($data['fk_type_status_id'])) ? $data['fk_type_status_id'] : '';
                    $fk_type_status_title = (!empty($fk_type_status_id)) ? $this->Configsystem->get_type_status_title($fk_type_status_id) : '';

                    //1. Staff เจ้าของ Stock
                    $data_staff_owner = $this->Condominium_model->get_staff_owner_condo($fk_condo_id);
                    $condo_property_id = (!empty($data_staff_owner->condo_property_id)) ? $data_staff_owner->condo_property_id : '';
                    $data_staff_owner_name = (!empty($data_staff_owner->fullname)) ? $data_staff_owner->fullname : '';
                    $data_staff_owner_email = (!empty($data_staff_owner->email)) ? $data_staff_owner->email : '';
                    $data_staff_owner_mobileno = (!empty($data_staff_owner->phone)) ? $data_staff_owner->phone : '';

                    //2. ผู้ที่สนใจ (คนที่เมล์ติดต่อมา)
                    $data_member_share_condo = $this->Condominium_model->get_condo_contact_by_id($fk_condo_id, $fk_member_id);
                    $condo_share_form_name = (!empty($data_member_share_condo->condo_share_form_name)) ? $data_member_share_condo->condo_share_form_name : '';
                    $condo_share_form_phone = (!empty($data_member_share_condo->condo_share_form_phone)) ? $data_member_share_condo->condo_share_form_phone : '';
                    $condo_share_form_email = (!empty($data_member_share_condo->condo_share_form_email)) ? $data_member_share_condo->condo_share_form_email : '';
                    $condo_share_form_message = (!empty($data_member_share_condo->condo_share_form_message)) ? $data_member_share_condo->condo_share_form_message : '';

                    //3. ผู้ที่ทำการแชร์ (HOB)
                    //$this->primaryclass->pre_var_dump($data_member_share_condo);
                    $data_member_share_condo_fname = (!empty($data_member_share_condo->member_fname)) ? $data_member_share_condo->member_fname : '';
                    $data_member_share_condo_lname = (!empty($data_member_share_condo->member_lname)) ? $data_member_share_condo->member_lname : '';
                    $data_member_share_condo_email = (!empty($data_member_share_condo->member_email)) ? $data_member_share_condo->member_email : '';
                    $data_member_share_condo_mobileno = (!empty($data_member_share_condo->member_mobileno)) ? $data_member_share_condo->member_mobileno : '';

                    //4. Admin
                    $site_id = 1;
                    $site_setting = $this->M->get_main_setting($site_id);


                    $site_logo = (!empty($site_setting['site_logo'])) ? $site_setting['site_logo'] : base_url().'assets/images/logo.jpg';
                    $site_email = (!empty($site_setting['site_email'])) ? $site_setting['site_email'] : 'tee.emilond@gmail.com';

                    //ข้อความของสถานะอีเมล์
                    if ($fk_type_status_id === 2) {
                        //โทรนัด, มีคนสนใจคอนโด

                        //1. หัวข้อ และข้อความ เจ้าของ Stock
                        $subject_for_condo_owner = "แจ้งเตือน ประกาศ รหัสทรัพย์ $condo_property_id";
                        $message_for_condo_owner = "<h4>สวัสดีคุณ, $data_staff_owner_name </h4>\n";
                        $message_for_condo_owner .= "<p>ขณะนี้ มีผู้สนใจประกาศ รหัสทรัพย์ '$condo_property_id' ของท่าน</p>\n";
                        $message_for_condo_owner .= "<hr>\n";
                        $message_for_condo_owner .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_condo_owner .= "<hr>\n";

                        $data_email_for_condo_owner = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_condo_owner,
                            'email_message' => $message_for_condo_owner,
                            'email_cc' => $site_email,
                            'email' => $data_staff_owner_email,
                        );
                        $success_email_for_condo_owner = $this->email_send_form_status($data_email_for_condo_owner);

                        //2. หัวข้อ และข้อความ ผู้ที่สนใจ (คนที่เมล์ติดต่อมา)
                        $subject_for_condo_contact = "แจ้งเตือน ประกาศ รหัสทรัพย์ $condo_property_id";
                        $message_for_condo_contact = "<h4>เรียนคุณ, $condo_share_form_name </h4>\n";
                        $message_for_condo_contact .= "<p>ขอบคุณครับ คุณ $condo_share_form_name เจ้าหน้าที่ของเราจะรีบติดต่อกลับท่านโดยเร็วที่สุด</p>\n";
                        $message_for_condo_contact .= "<hr>\n";
                        $message_for_condo_contact .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_condo_contact .= "<hr>\n";

                        $data_email_for_condo_contact = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_condo_contact,
                            'email_message' => $message_for_condo_contact,
                            'email_cc' => $site_email,
                            'email' => $condo_share_form_email,
                        );
                        $success_email_for_condo_contact = $this->email_send_form_status($data_email_for_condo_contact);

                        //3. หัวข้อ และข้อความ ผู้ที่ทำการแชร์ (HOB)
                        $subject_for_condo_share = "แจ้งเตือน ประกาศ รหัสทรัพย์ $condo_property_id";
                        $message_for_condo_share = "<h4>เรียนคุณ, $data_member_share_condo_fname </h4>\n";
                        $message_for_condo_share .= "<p><p>ขณะนี้ มีผู้สนใจ ทรัพย์รหัส '$condo_property_id' ที่ท่านได้แชร์ออกไป</p>\n";
                        $message_for_condo_share .= "<hr>\n";
                        $message_for_condo_share .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_condo_share .= "<hr>\n";

                        $data_email_for_condo_share = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_condo_share,
                            'email_message' => $message_for_condo_share,
                            'email_cc' => $site_email,
                            'email' => $data_member_share_condo_email,
                        );
                        $success_email_for_condo_share = $this->email_send_form_status($data_email_for_condo_share);

                        //4. แอดมิน
                        $subject_for_condo_admin = "แจ้งเตือน ประกาศ รหัสทรัพย์ $condo_property_id ผ่านทางเว็บไซต์";
                        $message_for_condo_admin = "<h4>เรียนคุณ, ผู้ดูแลระบบ </h4>\n";
                        $message_for_condo_admin .= "<p><p>ขณะนี้ มีผู้สนใจ ทรัพย์รหัส '$condo_property_id' ทางเว็บไซต์</p>\n";
                        $message_for_condo_admin .= "<hr>\n";
                        $message_for_condo_admin .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_condo_admin .= "<hr>\n";
                        $message_for_condo_admin .= "<h6>ผู้แชร์</h6>\n";
                        $message_for_condo_admin .= "<p><storng>ชื่อผู้แชร์ : </storng> $data_member_share_condo_fname $data_member_share_condo_lname</p>\n";
                        $message_for_condo_admin .= "<p><storng>อีเมล์ : </storng> $data_staff_owner_email</p>\n";
                        $message_for_condo_admin .= "<p><storng>เบอร์ติดต่อ : </storng> $data_member_share_condo_mobileno</p>\n";
                        $message_for_condo_admin .= "<hr>\n";
                        $message_for_condo_admin .= "<h6>ผู้สนใจ</h6>\n";
                        $message_for_condo_admin .= "<p><storng>ชื่อผู้สนใจ : </storng> $condo_share_form_name</p>\n";
                        $message_for_condo_admin .= "<p><storng>อีเมล์ : </storng> $condo_share_form_email</p>\n";
                        $message_for_condo_admin .= "<p><storng>เบอร์ติดต่อ : </storng> $condo_share_form_phone</p>\n";
                        $message_for_condo_admin .= "<p><storng>ข้อความ : </storng> $condo_share_form_message</p>\n";
                        $message_for_condo_admin .= "<hr>\n";

                        $data_email_for_condo_admin = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_condo_admin,
                            'email_message' => $message_for_condo_admin,
                            'email_cc' => $site_email,
                            'email' => $site_email,
                        );
                        $success_email_for_condo_share = $this->email_send_form_status($data_email_for_condo_admin);
                    }
                    // END IF

                    else {
                        //นัดดูสถานที่ | //เสนอราคา / ต่อรองราคา | //ทำสัญญา | //นัดวันทำสัญญา | //ซื้อขายสมบูรณ์

                        //1. หัวข้อ และข้อความ เจ้าของ Stock
                        $subject_for_condo_owner = "แจ้งเตือน ประกาศ รหัสทรัพย์ $condo_property_id";
                        $message_for_condo_owner = "<h4>สวัสดีคุณ, $data_staff_owner_name </h4>\n";
                        $message_for_condo_owner .= "<p>ขณะนี้ มีผู้สนใจประกาศ รหัสทรัพย์ '$condo_property_id' ของท่าน</p>\n";
                        $message_for_condo_owner .= "<hr>\n";
                        $message_for_condo_owner .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_condo_owner .= "<hr>\n";

                        $data_email_for_condo_owner = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_condo_owner,
                            'email_message' => $message_for_condo_owner,
                            'email_cc' => $site_email,
                            'email' => $data_staff_owner_email,
                        );
                        $success_email_for_condo_owner = $this->email_send_form_status($data_email_for_condo_owner);

                        //2. หัวข้อ และข้อความ ผู้ที่สนใจ (คนที่เมล์ติดต่อมา)
                        $subject_for_condo_contact = "แจ้งเตือน ประกาศ รหัสทรัพย์ $condo_property_id";
                        $message_for_condo_contact = "<h4>เรียนคุณ, $condo_share_form_name </h4>\n";
                        $message_for_condo_contact .= "<p>ขอบคุณครับ คุณ $condo_share_form_name เจ้าหน้าที่ของเราจะรีบติดต่อกลับท่านโดยเร็วที่สุด</p>\n";
                        $message_for_condo_contact .= "<hr>\n";
                        $message_for_condo_contact .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_condo_contact .= "<hr>\n";

                        $data_email_for_condo_contact = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_condo_contact,
                            'email_message' => $message_for_condo_contact,
                            'email_cc' => $site_email,
                            'email' => $condo_share_form_email,
                        );
                        $success_email_for_condo_contact = $this->email_send_form_status($data_email_for_condo_contact);

                        //3. หัวข้อ และข้อความ ผู้ที่ทำการแชร์ (HOB)
                        $subject_for_condo_share = "แจ้งเตือน ประกาศ รหัสทรัพย์ $condo_property_id";
                        $message_for_condo_share = "<h4>เรียนคุณ, $data_member_share_condo_fname $data_member_share_condo_lname </h4>\n";
                        $message_for_condo_share .= "<p><p>ขณะนี้ มีผู้สนใจ ทรัพย์รหัส '$condo_property_id' ที่ท่านได้แชร์ออกไป</p>\n";
                        $message_for_condo_share .= "<hr>\n";
                        $message_for_condo_share .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_condo_share .= "<hr>\n";

                        $data_email_for_condo_share = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_condo_share,
                            'email_message' => $message_for_condo_share,
                            'email_cc' => $site_email,
                            'email' => $data_member_share_condo_email,
                        );
                        $success_email_for_condo_share = $this->email_send_form_status($data_email_for_condo_share);

                        //4. แอดมิน
                        $subject_for_condo_admin = "แจ้งเตือน ประกาศ รหัสทรัพย์ $condo_property_id ผ่านทางเว็บไซต์";
                        $message_for_condo_admin = "<h4>เรียนคุณ, ผู้ดูแลระบบ </h4>\n";
                        $message_for_condo_admin .= "<p><p>ขณะนี้ มีผู้สนใจ ทรัพย์รหัส '$condo_property_id' ทางเว็บไซต์</p>\n";
                        $message_for_condo_admin .= "<hr>\n";
                        $message_for_condo_admin .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_condo_admin .= "<hr>\n";

                        $data_email_for_condo_admin = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_condo_admin,
                            'email_message' => $message_for_condo_admin,
                            'email_cc' => $site_email,
                            'email' => $site_email,
                        );
                        $success_email_for_condo_share = $this->email_send_form_status($data_email_for_condo_admin);
                    }
                }
            }//END IF
        }//END IF
        //return $data;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function email_send_status($data = '')
    {
        $get_member_share = '';
        $data['name'] = 'Wittawat Kittiwarabud';
        $data['email'] = 'tee.emilond@gmail.com';
        //$data['message_body'] = "any message body you want to send";

        $data['row'] = $this->Condominium_model->get_share_condo($data['condo_id']);

        //$data = '';
        $data['body'] = 'จะให้ใส่ข้อความว่าอะไรดีครับ';
        $data['condo_id'] = '';
        $data['condo_title'] = '';
        $data['pic_thumb'] = '';
        $data['type_status_title'] = '';
        $data['message'] = $this->load->view('backoffice/condominium/formadmin-email', $data, true); // this will return you html data as message
        //$this->email->message($message);
        return $data['message'];
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



    //EMAIL TEMPLATE
    public function email_send_form_status($data)
    {
        $message = $this->load->view('email/change-status', $data, TRUE);


        $this->email->clear();
        $this->email->from('hob@condo-compare.com');
        $this->email->to($data['email']);
        $this->email->cc($data['email_cc']);
        $this->email->subject($data['email_subject']);
        $this->email->message($message);

        if ( $this->email->send() )
        {
          $success = array('success'=>true, 'message'=>'ระบบได้ทำการส่งอีเมล์ การฝากขายทรัพย์ของท่านเรียบร้อยแล้ว..');
        }else{
          $success = array('success'=>true, 'message'=>'ระบบได้ทำการส่งอีเมล์ การฝากขายทรัพย์ของท่านเรียบร้อยแล้ว..');
        }
        return $success;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_upload()
    {
        $sfilepath = './uploads/condominium/.';
        $smaxsize = 4096000; //4MB
        $smaxwidth = 2000;
        $smaxheight = 2000;

        $pic_large = $this->primaryclass->do_upload('file', $sfilepath, $smaxsize, $smaxwidth, $smaxheight);

//		if (!empty($_FILES)) {
//			$tempFile = $_FILES['file']['tmp_name'];          //3
//			$targetPath = $sfilepath;  //4
//			$targetFile =  $targetPath. $_FILES['file']['name'];  //5
//			move_uploaded_file($tempFile,$targetFile); //6
//		}
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function canceled_upload()
    {
        //echo $this->primaryclass->pre_var_dump($_POST['file_name']);
        //$sfilepath = base_url().'uploads/condominium/';
        //$pic_thumb = $this->do_upload('file', $sfilepath);
        //echo $this->primaryclass->pre_var_dump($pic_thumb);
        //$file_name = json_decode($_POST['file_name']);
        //echo $sfilepath;
        $t = $_POST['file_name'];
        //echo $t;
        //unlink($sfilepath.$t);
        unlink('uploads/condominium/'.$t);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_condominium()
    {
        $sfilepath = './uploads/condominium/.';
        $smaxsize = 4096000; //4MB
        $smaxwidth_pic_thumb = 2000;
        $smaxheight_pic_thumb = 2000;

        $smaxwidth_pic_large = 2000;
        $smaxheight_pic_large = 2000;

        // PIC THUMB===========================================================
        if ($_FILES['pic_thumb']) {
            $pic_thumb = $this->primaryclass->do_upload('pic_thumb', $sfilepath, $smaxsize, $smaxwidth_pic_thumb, $smaxheight_pic_thumb);
            if ($pic_thumb['upload_data']['file_name'] == '') {
                $pic_thumb_have_file = $this->input->post('old_pic_thumb');
            } else {
                $pic_thumb_have_file = $pic_thumb['upload_data']['file_name'];
            }
        } else {
            $pic_thumb_have_file = '';
        }//IF PIC THUMB=========================================================

        // PIC LARGE=============================================================
        if ($_FILES['pic_large']) {
            $pic_large = $this->primaryclass->do_upload('pic_large', $sfilepath, $smaxsize, $smaxwidth_pic_large, $smaxheight_pic_large);
            if ($pic_large['upload_data']['file_name'] == '') {
                $pic_large_have_file = $this->input->post('old_pic_large');
            } else {
                $pic_large_have_file = $pic_large['upload_data']['file_name'];
            }
        } else {
            $pic_large_have_file = '';
        }//IF PIC LARGE==========================================================

        $createdate = $this->primaryclass->set_explode_date_time($this->input->post('createdate_date'), $this->input->post('createdate_time'));
        $modifydate = $this->primaryclass->set_explode_date_time($this->input->post('modifydate_date'), $this->input->post('modifydate_time'));
        $data = array(
            'file_name_' => $this->input->post('file_name_'),
            'condo_property_id' => $this->input->post('condo_property_id'),
            'member_id' => $this->input->post('member_id'),
            'staff_id' => $this->input->post('staff_id'),
            'condo_title' => $this->input->post('condo_title'),
            'condo_owner_name' => $this->input->post('condo_owner_name'),
            'condo_holder_name' => $this->input->post('condo_holder_name'),
            'condo_builing_finish' => $this->input->post('condo_builing_finish'),
            'property_type_id' => $this->input->post('property_type_id'),
            'type_zone_id' => $this->input->post('type_zone_id'),
            'zone_id' => $this->input->post('zone_id'),
            'type_offering_id' => $this->input->post('type_offering_id'),
            'type_suites_id' => $this->input->post('type_suites_id'),
            'type_decoration_id' => $this->input->post('type_decoration_id'),
            'condo_all_room' => $this->input->post('condo_all_room'),
            'condo_all_toilet' => $this->input->post('condo_all_toilet'),
            'condo_interior_room' => $this->input->post('condo_interior_room'),
            'condo_direction_room' => $this->input->post('condo_direction_room'),
            'condo_scenery_room' => $this->input->post('condo_scenery_room'),
            'condo_living_area' => $this->input->post('condo_living_area'),
            'condo_electricity' => $this->input->post('condo_electricity'),
            'condo_expert' => trim($this->input->post('condo_expert')),
            'condo_description' => trim($this->input->post('condo_description')),
            'pic_thumb' => $pic_thumb_have_file,
            'pic_large' => $pic_large_have_file,
            'condo_no' => $this->input->post('condo_no'),
            'condo_floor' => $this->input->post('condo_floor'),
            'condo_area_apartment' => $this->input->post('condo_area_apartment'),
            'condo_alley' => $this->input->post('condo_alley'),
            'condo_road' => $this->input->post('condo_road'),
            'condo_address' => trim($this->input->post('condo_address')),
            'provinces_id' => $this->input->post('provinces_id'),
            'districts_id' => $this->input->post('districts_id'),
            'sub_districts_id' => $this->input->post('sub_districts_id'),
            'condo_zipcode' => $this->input->post('condo_zipcode'),
            'condo_total_price' => $this->input->post('condo_total_price'),
            'condo_price_per_square_meter' => $this->input->post('condo_price_per_square_meter'),
            'condo_googlemaps' => trim($this->input->post('condo_googlemaps')),
            'latitude' => trim($this->input->post('latitude')),
            'longitude' => trim($this->input->post('longitude')),
            'condo_tag' => $this->input->post('condo_tag'),
            'createdate' => $createdate,
            'modifydate' => $modifydate,
            'type_status_id' => 0,
            'visited' => 0,
            'newsarrival' => $this->input->post('newsarrival'),
            'highlight' => $this->input->post('highlight'),
//			'publish'=>$this->input->post('publish'),
            'approved' => $this->input->post('approved'),
            'facilities_id' => $this->input->post('facilities_id'),
            'featured_id' => $this->input->post('featured_id'),
            'transportation_category_id' => $this->input->post('transportation_category_id'),
            'transportation_distance' => $this->input->post('transportation_distance'),
            'transportation_id' => $this->input->post('transportation_id'),
        );

//		$this->primaryclass->pre_var_dump($this->input->post('file_name_'));
        $insert = $this->Condominium_model->add($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit($id)
    {
        $row = $this->Condominium_model->get_info($id);

        $provinces = $this->M->get_provinces();
        //$districts 		= $this->Member->get_districts();
        //$subdistricts = $this->Member->get_subdistricts();
        $staff_list = $this->User_model->get_staff_option();
        $type_zone = $this->Configsystem->get_type_zone_option();
        $type_offering = $this->Configsystem->get_type_offering_option();
        $type_status = $this->Configsystem->get_type_status_option();
        $type_suites = $this->Configsystem->get_type_suites_option();
        $type_decoration = $this->Configsystem->get_type_decoration_option();
        $facilities = $this->Condominium_model->get_facilities_option();
        $relation_facilities = $this->Condominium_model->get_facilities_option_by_condo($id);
        $featured = $this->Condominium_model->get_featured_option();
        $relation_featured = $this->Condominium_model->get_featured_row_by_condo($id);

        $condo_relation_transportation = $this->Condominium_model->get_transportation_relation_row_by_condo($id);
        $transportation_category = $this->Condominium_model->get_transportation_category_option();
        $transportation = $this->Condominium_model->get_transportation_option();
        $member = $this->Member->get_fullname();
//		$this->output->enable_profiler(TRUE);
        $data['row'] = $row;
        ($this->input->post()) ? $this->Condominium_model->edit($data) : $this->load->view('backoffice/condominium/formadmin', array(
            'title' => 'แก้ไขข้อมูลคอนโด',
            'googlemap' => 'yes',
            'header' => 'yes',
            'url_href' => 'edit',
            'css' => array(
                'assets/plugins/fileuploads/css/dropify.min.css',
                'assets/plugins/timepicker/bootstrap-timepicker.min.css',
                'assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
            ),
            'js' => array(
                'assets/plugins/fileuploads/js/dropify.min.js',
                'assets/plugins/moment/moment.js',
                'assets/plugins/timepicker/bootstrap-timepicker.min.js',
                'assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
            ),
            'wysiwigeditor' => 'wysiwigeditor',
            'data' => $data,
            'provinces' => $provinces,
            'type_zone' => $type_zone,
            'type_offering' => $type_offering,
            'type_status' => $type_status,
            'type_suites' => $type_suites,
            'type_decoration' => $type_decoration,
            'facilities' => $facilities,
            'relation_facilities' => $relation_facilities,
            'featured' => $featured,
            'relation_featured' => $relation_featured,
            'condo_relation_transportation' => $condo_relation_transportation,
            'transportation_category' => $transportation_category,
            'transportation' => $transportation,
            'member' => $member,
            'staff_list' => $staff_list,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_condominium()
    {
        //		/* Upload Image */
        $sfilepath = './uploads/condominium/.';
        $smaxsize = 4096000; //4MB
        $smaxwidth_pic_thumb = 2000;
        $smaxheight_pic_thumb = 2000;

        $smaxwidth_pic_large = 2000;
        $smaxheight_pic_large = 2000;

        // PIC THUMB===========================================================
        if ($_FILES['pic_thumb']) {
            //			$pic_thumb = $this->primaryclass->do_upload( 'pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_pic_thumb, $smaxheight_pic_thumb );
            $pic_thumb = $this->primaryclass->do_upload('pic_thumb', $sfilepath, $smaxsize, $smaxwidth_pic_thumb, $smaxheight_pic_thumb);
            // $this->primaryclass->pre_var_dump($pic_thumb);

            if ($pic_thumb['upload_data']['file_name'] == '') {
                $pic_thumb_have_file = $this->input->post('old_pic_thumb');
            } else {
                $pic_thumb_have_file = $pic_thumb['upload_data']['file_name'];
            }
        } else {
            $pic_thumb_have_file = '';
        }//IF PIC THUMB=========================================================

        // PIC LARGE=============================================================
        if ($_FILES['pic_large']) {
            $pic_large = $this->primaryclass->do_upload('pic_large', $sfilepath, $smaxsize, $smaxwidth_pic_large, $smaxheight_pic_large);
            if ($pic_large['upload_data']['file_name'] == '') {
                $pic_large_have_file = $this->input->post('old_pic_large');
            } else {
                $pic_large_have_file = $pic_large['upload_data']['file_name'];
            }
        } else {
            $pic_large_have_file = '';
        }//IF PIC LARGE==========================================================

//		$this->primaryclass->pre_var_dump( $_FILES['pic_large'] );

        $createdate = $this->primaryclass->set_explode_date_time($this->input->post('createdate_date'), $this->input->post('createdate_time'));
        $modifydate = $this->primaryclass->set_explode_date_time($this->input->post('modifydate_date'), $this->input->post('modifydate_time'));

        $data = array(
            'condo_id' => $this->input->post('condo_id'),
            'file_name_' => $this->input->post('file_name_'),
            'condo_property_id' => $this->input->post('condo_property_id'),
            'member_id' => $this->input->post('member_id'),
            'staff_id' => $this->input->post('staff_id'),
            'condo_title' => $this->input->post('condo_title'),
            'condo_owner_name' => $this->input->post('condo_owner_name'),
            'condo_holder_name' => $this->input->post('condo_holder_name'),
            'condo_builing_finish' => $this->input->post('condo_builing_finish'),
            'property_type_id' => $this->input->post('property_type_id'),
            'type_zone_id' => $this->input->post('type_zone_id'),
            'zone_id' => $this->input->post('zone_id'),
            'type_offering_id' => $this->input->post('type_offering_id'),
            'type_suites_id' => $this->input->post('type_suites_id'),
            'type_decoration_id' => $this->input->post('type_decoration_id'),
            'condo_all_room' => $this->input->post('condo_all_room'),
            'condo_all_toilet' => $this->input->post('condo_all_toilet'),
            'condo_interior_room' => $this->input->post('condo_interior_room'),
            'condo_direction_room' => $this->input->post('condo_direction_room'),
            'condo_scenery_room' => $this->input->post('condo_scenery_room'),
            'condo_living_area' => $this->input->post('condo_living_area'),
            'condo_electricity' => $this->input->post('condo_electricity'),
            'condo_expert' => trim($this->input->post('condo_expert')),
            'condo_description' => trim($this->input->post('condo_description')),
            'pic_thumb' => $pic_thumb_have_file,
            'pic_large' => $pic_large_have_file,
            'condo_no' => $this->input->post('condo_no'),
            'condo_floor' => $this->input->post('condo_floor'),
            'condo_area_apartment' => $this->input->post('condo_area_apartment'),
            'condo_alley' => $this->input->post('condo_alley'),
            'condo_road' => $this->input->post('condo_road'),
            'condo_address' => trim($this->input->post('condo_address')),
            'provinces_id' => $this->input->post('provinces_id'),
            'districts_id' => $this->input->post('districts_id'),
            'sub_districts_id' => $this->input->post('sub_districts_id'),
            'condo_zipcode' => $this->input->post('condo_zipcode'),
            'condo_total_price' => $this->input->post('condo_total_price'),
            'condo_price_per_square_meter' => $this->input->post('condo_price_per_square_meter'),
            'condo_googlemaps' => trim($this->input->post('condo_googlemaps')),
            'latitude' => trim($this->input->post('latitude')),
            'longitude' => trim($this->input->post('longitude')),
            'condo_tag' => $this->input->post('condo_tag'),
            'createdate' => $createdate,
            'modifydate' => $modifydate,
            'type_status_id' => $this->input->post('type_status_id'),
            'newsarrival' => $this->input->post('newsarrival'),
            'highlight' => $this->input->post('highlight'),
            'approved' => $this->input->post('approved'),
            'facilities_id' => $this->input->post('facilities_id'),
            'featured_id' => $this->input->post('featured_id'),
            'transportation_category_id' => $this->input->post('transportation_category_id'),
            'transportation_distance' => $this->input->post('transportation_distance'),
            'transportation_id' => $this->input->post('transportation_id'),
        );
        //echo $this->primaryclass->pre_var_dump($data);
        $update = $this->Condominium_model->edit($data);
        //redirect('condominium/view');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function remove_image_update()
    {
        $data = $this->security->xss_clean($this->input->post());
        $data = array(
            'condo_id' => $this->input->post('condo_id'),
            $this->input->post('field_name') => null,
        );
        $sfilepath = 'uploads/condominium/';
        $imagefile = $sfilepath.$this->input->post('imagefile');
        $this->M->update_record_dalete_image($data, $imagefile, 'condo');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public function delete_condo($id)
    {
        $delete = $this->Condominium_model->delete_record('condo_id', $id, 'condo');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function delete_multi_condo()
    {
        $data = $this->security->xss_clean($this->input->post());
    // $this->primaryclass->pre_var_dump($data);
    if (isset($data['condo_id'])) {
        //echo "yes";
      foreach ($data as $key => $value) {
          if ($key === 'condo_id') {
              foreach ($value as $condo_id) {
                  $delete = $this->Condominium_model->delete_record('condo_id', $condo_id, 'condo');
              }
          }
      }
    }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function delete_image($id)
    {
        $delete = $this->Condominium_model->delete_record('condo_images_id', $id, 'condo_images');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function approved_condo($id = 0)
    {
        $data = $this->security->xss_clean($this->input->post());
        $data = array(
            'condo_id' => $this->input->post('condo_id'),
            'approved' => $this->input->post('approved'),
            'modifydate' => date('Y-m-d H:i:s'),
        );

        $approved = $this->Condominium_model->set_condo($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function do_upload($sfilename, $sfilepath)
    {
        $config['upload_path'] = $sfilepath;
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['max_size'] = 2048000;
        $config['max_width'] = 2400;
        $config['max_height'] = 1400;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload($sfilename)) {
            return $data = array('upload_data' => $this->upload->data());
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public function approved_multi_condo()
  {
      $data = $this->security->xss_clean($this->input->post());
    // $this->primaryclass->pre_var_dump($data);
    if (isset($data['condo_id'])) {
        //echo "yes";
      foreach ($data as $key => $value) {
          if ($key === 'condo_id') {
              foreach ($value as $condo_id) {
                  $data_update = array(
              'condo_id' => $condo_id,
              'approved' => '2',
              'modifydate' => date('Y-m-d H:i:s'),
            );
            //$this->primaryclass->pre_var_dump($data_update);
            $approved = $this->Condominium_model->set_condo($data_update);
              }
          }
      }
    }
  }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  public function unapproved_multi_condo()
  {
      $data = $this->security->xss_clean($this->input->post());
    // $this->primaryclass->pre_var_dump($data);
    if (isset($data['condo_id'])) {
        //echo "yes";
      foreach ($data as $key => $value) {
          if ($key === 'condo_id') {
              foreach ($value as $condo_id) {
                  $data_update = array(
              'condo_id' => $condo_id,
              'approved' => '1',
              'modifydate' => date('Y-m-d H:i:s'),
            );
            // $this->primaryclass->pre_var_dump($data_update);
            $approved = $this->Condominium_model->set_condo($data_update);
              }
          }
      }
    }
  }
  //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* Facilities */
    public function view_facilities()
    {
        $data = $this->Condominium_model->get_facilities();

        $this->load->view('backoffice/condominium/datatable-facilities', array(
            'title' => 'ตารางข้อมูลสิ่งอำนวยความสะดวก',
            'header' => 'yes',
            'url_href' => 'add_facilities',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_facilities()
    {
        $this->load->view('backoffice/condominium/formadmin-facilities', array(
            'title' => 'เพิ่มข้อมูลสิ่งอำนวยความสะดวก',
            'header' => 'yes',

        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_data_facilities()
    {
        $data = array(
            'facilities_title' => $this->input->post('facilities_title'),
            'facilities_title_en' => $this->input->post('facilities_title_en'),
            'createdate' => date('Y-m-d H:i:s'),
        );

        //var_dump($data);
        $insert = $this->Condominium_model->set_facilities($data);
        $this->session->set_flashdata('message', 'Your data inserted Successfully..');
        redirect('condominium/view_facilities');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_facilities($id)
    {
        $row = $this->Condominium_model->get_facilities_record($id);

        $data['row'] = $row;
        //var_dump($row);
        ($this->input->post()) ? $this->Condominium_model->edit($data) : $this->load->view('backoffice/condominium/formadmin-facilities', array(
            'title' => 'แก้ไขข้อมูลสิ่งอำนวยความสะดวก',
            'header' => 'yes',
            'url_href' => 'edit',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_data_facilities()
    {
        $facilities_id = $this->input->post('facilities_id');

        $data = array(
            'facilities_id' => $this->input->post('facilities_id'),
            'facilities_title' => $this->input->post('facilities_title'),
            'facilities_title_en' => $this->input->post('facilities_title_en'),
            'updatedate' => date('Y-m-d H:i:s'),
        );
        //echo $this->Primaryclass->pre_var_dump($data);

        $update = $this->Condominium_model->update_facilities($data);
        $this->session->set_flashdata('message', 'Your data Updated Successfully..');
        redirect('condominium/view_facilities');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function delete_facilities($id)
    {
        $delete = $this->Condominium_model->delete_record('facilities_id', $id, 'facilities');

        $this->session->set_flashdata('message', 'Your data Delete Successfully..');
        redirect('condominium/view_facilities');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* Feathured */
    public function view_featured()
    {
        $data = $this->Condominium_model->get_featured();

        $this->load->view('backoffice/condominium/datatable-featured', array(
            'title' => 'ตารางข้อมูลจุดเด่น',
            'header' => 'yes',
            'url_href' => 'add_featured',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_featured()
    {
        $this->load->view('backoffice/condominium/formadmin-featured', array(
            'title' => 'เพิ่มข้อมูลจุดเด่น',
            'header' => 'yes',
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_data_featured()
    {
        $data = array(
            'featured_title' => $this->input->post('featured_title'),
            'featured_title_en' => $this->input->post('featured_title_en'),
            'createdate' => date('Y-m-d H:i:s'),
        );

        //var_dump($data);
        $insert = $this->Condominium_model->set_featured($data);
        $this->session->set_flashdata('message', 'Your data inserted Successfully..');
        redirect('condominium/view_featured');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_featured($id)
    {
        $row = $this->Condominium_model->get_featured_record($id);

        $data['row'] = $row;
        //var_dump($row);
        ($this->input->post()) ? $this->Condominium_model->edit($data) : $this->load->view('backoffice/condominium/formadmin-featured', array(
            'title' => 'แก้ไขรายการจุดเด่น',
            'header' => 'yes',
            'url_href' => 'edit',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_data_featured()
    {
        $featured_id = $this->input->post('featured_id');

        $data = array(
            'featured_id' => $this->input->post('featured_id'),
            'featured_title' => $this->input->post('featured_title'),
            'featured_title_en' => $this->input->post('featured_title_en'),
            'updatedate' => date('Y-m-d H:i:s'),
        );
        //echo $this->Primaryclass->pre_var_dump($data);

        $update = $this->Condominium_model->update_featured($data);
        $this->session->set_flashdata('message', 'Your data Updated Successfully..');
        redirect('condominium/view_featured');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function delete_featured($id)
    {
        $delete = $this->Condominium_model->delete_record('featured_id', $id, 'featured');

        $this->session->set_flashdata('message', 'Your data Delete Successfully..');
        redirect('condominium/view_featured');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* Transportation Category */
    public function view_transportation_category()
    {
        $data = $this->Condominium_model->get_transportation_category();
        //
        $this->load->view('backoffice/condominium/datatable-transportation-category', array(
            'title' => 'ตารางข้อมูลหมวดหมู่สถานที่ใกล้เคียง',
            'header' => 'yes',
            'url_href' => 'add_transportation_category',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_transportation_category()
    {
        $this->load->view('backoffice/condominium/formadmin-transportation-category', array(
            'title' => 'เพิ่มข้อมูลหมวดหมู่สถานที่ใกล้เคียง',
            'header' => 'yes',
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_data_transportation_category()
    {
        $data = array(
            'transportation_category_title' => $this->input->post('transportation_category_title'),
            'createdate' => date('Y-m-d H:i:s'),
            'publish' => $this->input->post('publish'),
        );

        //var_dump($data);
        $insert = $this->Condominium_model->set_transportation_category($data);
        $this->session->set_flashdata('message', 'Your data inserted Successfully..');
        redirect('condominium/view_transportation_category');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_transportation_category($id)
    {
        $row = $this->Condominium_model->get_transportation_category_record($id);

        $data['row'] = $row;
        //var_dump($row);
        ($this->input->post()) ? $this->Condominium_model->edit($data) : $this->load->view('backoffice/condominium/formadmin-transportation-category', array(
            'title' => 'แก้ไขรายการหมวดหมู่สถานที่ใกล้เคียง',
            'header' => 'yes',
            'url_href' => 'edit',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_data_transportation_category()
    {
        $transportation_id = $this->input->post('transportation_category_id');

        $data = array(
            'transportation_category_id' => $this->input->post('transportation_category_id'),
            'transportation_category_title' => $this->input->post('transportation_category_title'),
            'updatedate' => date('Y-m-d H:i:s'),
            'publish' => $this->input->post('publish'),
        );
        //echo $this->Primaryclass->pre_var_dump($data);

        $update = $this->Condominium_model->update_transportation_category($data);
        $this->session->set_flashdata('message', 'Your data Updated Successfully..');
        redirect('condominium/view_transportation_category');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function delete_transportation_category($id)
    {
        $delete = $this->Condominium_model->delete_category_record('transportation_category_id', $id, 'transportation_category');

        $this->session->set_flashdata('message', 'Your data Delete Successfully..');
        redirect('condominium/view_transportation_category');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //call to fill the second dropdown with the provinces
    public function transportation_category_relation()
    {
        //set selected country id from POST
        $transportation_category_id = $this->input->post('id', true);

        $data['transportation'] = $this->Condominium_model->get_transportation_option($transportation_category_id);

        //echo count($data['transportation']);
        $output = null;
        foreach ($data['transportation'] as $row) {
            if (count($data['transportation']) == 0) {
                //here we build a dropdown item line for each query result
             $output .= '<option value>---เลือก----</option>';
            } else {
                //here we build a dropdown item line for each query result
             $output .= "<option value='".$row->transportation_id."'>".$row->transportation_title.'</option>';
            }
        }
        echo $output;

        //var_dump($districts);
        //run the query for the cities we specified earlier
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //call to fill the second dropdown with the provinces
    public function transportation_category_option()
    {
        //set selected country id from POST
        $transportation_category_id = $this->input->post('id', true);

        $data['transportation_category'] = $this->Condominium_model->get_transportation_category_option();

        //echo count($data['transportation']);
        $output = null;

        $output .= '<div class="form-group">';
        $output .= '<label class="col-md-2 control-label m-t-10">สถานที่ใกล้เคียง</label>';
        $output .= '<div class="col-sm-12 col-md-4">';
        $output .= '<select id="transportation_category_id" class="form-control" name="transportation_category_id[]" onchange="(this)">';
        foreach ($data['transportation_category'] as $row) {
            if (count($data['transportation_category']) <= 0) {
                //here we build a dropdown item line for each query result
                $output .= '<optgroup>';
                $output .= '<option value>---เลือก----</option>';
                $output .= '</optgroup>';
            } else {
                $output .= "<optgroup label='".$row->transportation_category_title."'>";

                //เอาข้อมูล Transportation by ID มา
                $data['transportation'] = $this->Condominium_model->get_transportation_option($row->transportation_category_id);
                foreach ($data['transportation'] as $row_in) {
                    //here we build a dropdown item line for each query result
                    $output .= "<option value='".$row_in->transportation_id."'>".$row_in->transportation_title.'</option>';
                }
                $output .= '</optgroup>';
            }
        }
        $output .= '</select>';
        $output .= '</div>';
        $output .= '<div class="col-sm-10 col-md-4">';
        $output .= '<input type="text" name="transportation_distance[]" class="inputmaxlength form-control" maxlength="50" placeholder="">';
        $output .= '<span class="font-13 text-muted">ตัวอย่าง : 7 นาที &#40;1.46 กิโลเมตร&#41;</span>';
        $output .= '</div>';
        $output .= '<div class="col-sm-2 col-md-2">';
        $output .= '<div class="field_wrapper">';
        $output .= '<div>';
        $output .= '<a href="javascript:void(0);" class="remove_button"><i class="remove_button fa fa-minus"></i></a>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
        echo $output;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* Transportation */
    public function view_transportation()
    {
        $data = $this->Condominium_model->get_transportation();
        //
        $this->load->view('backoffice/condominium/datatable-transportation', array(
            'title' => 'ตารางข้อมูลสถานที่ใกล้เคียง',
            'header' => 'yes',
            'url_href' => 'add_transportation',

            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_transportation()
    {
        $transportation_category = $this->Condominium_model->get_transportation_category_option();
        $this->load->view('backoffice/condominium/formadmin-transportation', array(
            'title' => 'เพิ่มข้อมูลสถานที่ใกล้เคียง',
            'header' => 'yes',
            'transportation_category' => $transportation_category,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_data_transportation()
    {
        $data = array(
            'transportation_title' => $this->input->post('transportation_title'),
            'transportation_category_id' => $this->input->post('transportation_category_id'),
            'createdate' => date('Y-m-d H:i:s'),
            'publish' => $this->input->post('publish'),
        );

        //var_dump($data);
        $insert = $this->Condominium_model->set_transportation($data);
        $this->session->set_flashdata('message', 'Your data inserted Successfully..');
        redirect('condominium/view_transportation');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_transportation($id)
    {
        $row = $this->Condominium_model->get_transportation_record($id);
        $transportation_category = $this->Condominium_model->get_transportation_category_option();
        $data['row'] = $row;
        //var_dump($row);
        ($this->input->post()) ? $this->Condominium_model->edit($data) : $this->load->view('backoffice/condominium/formadmin-transportation', array(
            'title' => 'แก้ไขรายการสถานที่ใกล้เคียง',
            'header' => 'yes',
            'url_href' => 'edit',
            'data' => $data,
            'transportation_category' => $transportation_category,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_data_transportation()
    {
        $transportation_id = $this->input->post('transportation_id');

        $data = array(
            'transportation_id' => $this->input->post('transportation_id'),
            'transportation_title' => $this->input->post('transportation_title'),
            'transportation_category_id' => $this->input->post('transportation_category_id'),
            'updatedate' => date('Y-m-d H:i:s'),
            'publish' => $this->input->post('publish'),
        );
        //echo $this->Primaryclass->pre_var_dump($data);
        $update = $this->Condominium_model->update_transportation($data);
        $this->session->set_flashdata('message', 'Your data Updated Successfully..');
        redirect('condominium/view_transportation');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function delete_transportation($id)
    {
        $delete = $this->Condominium_model->delete_record('transportation_id', $id, 'transportation');

        $this->session->set_flashdata('message', 'Your data Delete Successfully..');
        redirect('condominium/view_transportation');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function delete_transportation_condominium($data = null)
    {
        //$data=$this->security->xss_clean($this->input->post());
        $delete = $this->M->delete_record('condo_relation_transportation_id', $this->input->post('id'), 'condo_relation_transportation');
        // $this->primaryclass->pre_var_dump($delete);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* Mass Transportation Category */
    public function view_mass_transportation_category()
    {
        $data = $this->Condominium_model->get_mass_transportation_category();
        //
        $this->load->view('backoffice/condominium/datatable-mass-transportation-category', array(
            'title' => 'ตารางข้อมูลหมวดหมู่ขนส่งมวลชน',
            'header' => 'yes',
            'url_href' => 'add_mass_transportation_category',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_mass_transportation_category()
    {
        if ($this->input->post('action')) {
            $data = $this->security->xss_clean($this->input->post());
            $data = array(
                'mass_transportation_category_title' => $data['mass_transportation_category_title'],
                'createdate' => date('Y-m-d H:i:s'),
                'publish' => $data['publish'],
            );
            $insert = $this->Condominium_model->set_mass_transportation_category($data);
        } else {
            $this->load->view('backoffice/condominium/formadmin-mass-transportation-category', array(
                'title' => 'เพิ่มข้อมูลหมวดหมู่ขนส่งมวลชน',
                'header' => 'yes',
            ));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_mass_transportation_category($id = 0)
    {
        if ($this->input->post('action')) {
            $data = $this->security->xss_clean($this->input->post());
            $data = array(
                'mass_transportation_category_id' => $data['mass_transportation_category_id'],
                'mass_transportation_category_title' => $data['mass_transportation_category_title'],
                'updatedate' => date('Y-m-d H:i:s'),
                'publish' => $data['publish'],
            );
            $insert = $this->Condominium_model->update_mass_transportation_category($data);
        } else {
            $data['row'] = $this->Condominium_model->get_mass_transportation_category_by_id($id);
            $this->load->view('backoffice/condominium/formadmin-mass-transportation-category', array(
                'title' => 'แก้ไขข้อมูลหมวดหมู่ขนส่งมวลชน',
                'header' => 'yes',
                'data' => $data,
            ));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function mass_transportation_category_publish()
    {
        $data = $this->security->xss_clean($this->input->post());
        $data = array(
            'mass_transportation_category_id' => $data['mass_transportation_category_id'],
            'publish' => $data['publish'],
            'updatedate' => date('Y-m-d H:i:s'),
        );

        $approved = $this->Condominium_model->update_mass_transportation_category($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function delete_mass_transportation_category($id)
    {
        $delete = $this->Condominium_model->delete_record('mass_transportation_category_id', $id, 'mass_transportation_category');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* Mass Transportation */
    public function view_mass_transportation()
    {
        $data = $this->Condominium_model->get_mass_transportation();
        //
        $this->load->view('backoffice/condominium/datatable-mass-transportation', array(
            'title' => 'ตารางข้อมูลขนส่งมวลชน',
            'header' => 'yes',
            'url_href' => 'add_mass_transportation',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_mass_transportation()
    {
        if ($this->input->post('action')) {
            $data = $this->security->xss_clean($this->input->post());
            $data = array(
                'mass_transportation_title' => $data['mass_transportation_title'],
                'mass_transportation_category_id' => $data['mass_transportation_category_id'],
                'createdate' => date('Y-m-d H:i:s'),
                'publish' => $data['publish'],
            );
            $insert = $this->Condominium_model->set_mass_transportation($data);
        } else {
            $data['mass_transportation_category'] = $this->Condominium_model->get_mass_transportation_category_option();
            $this->load->view('backoffice/condominium/formadmin-mass-transportation', array(
                'title' => 'เพิ่มข้อมูลขนส่งมวลชน',
                'header' => 'yes',
                'data' => $data,
            ));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_mass_transportation($id = 0)
    {
        if ($this->input->post('action')) {
            $data = $this->security->xss_clean($this->input->post());
            $data = array(
                'mass_transportation_id' => $data['mass_transportation_id'],
                'mass_transportation_category_id' => $data['mass_transportation_category_id'],
                'mass_transportation_title' => $data['mass_transportation_title'],
                'updatedate' => date('Y-m-d H:i:s'),
                'publish' => $data['publish'],
            );
            $insert = $this->Condominium_model->update_mass_transportation($data);
        } else {
            $data['row'] = $this->Condominium_model->get_mass_transportation_by_id($id);
            $data['mass_transportation_category'] = $this->Condominium_model->get_mass_transportation_category_option();
            $this->load->view('backoffice/condominium/formadmin-mass-transportation', array(
                'title' => 'แก้ไขข้อมูลหมวดหมู่ขนส่งมวลชน',
                'header' => 'yes',
                'data' => $data,
            ));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function mass_transportation_publish()
    {
        $data = $this->security->xss_clean($this->input->post());
        $data = array(
            'mass_transportation_id' => $data['mass_transportation_id'],
            'publish' => $data['publish'],
            'updatedate' => date('Y-m-d H:i:s'),
        );

        $approved = $this->Condominium_model->update_mass_transportation($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function delete_mass_transportation($id)
    {
        $delete = $this->Condominium_model->delete_record('mass_transportation_id', $id, 'mass_transportation');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




    public function view_highlight()
    {
        $data = $this->Condominium_model->get_condo_highlight();

    // var_dump($data['data']);
    $this->load->view('backoffice/condominium/datatable', array(
      'title' => 'รายการคอนโดแนะนำ',
      'header' => 'yes',
      'url_href' => 'add',
      'data' => $data,
    ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
}
