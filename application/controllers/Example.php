<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Example extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Member_model'); 	/* LOADING MODEL * Member_model as user */

		// Load library and url helper
		$this->load->library('facebook');
		$this->load->helper('url');
	}

	// ------------------------------------------------------------------------

	/**
	 * Index page
	 */
	public function index()
	{
		$this->load->view('examples/start');
	}

	// ------------------------------------------------------------------------

	/**
	 * Web redirect login example page
	 */
	public function web_login()
	{
		$data['user'] = array();

		// Check if user is logged in
		if ($this->facebook->is_authenticated())
		{
			echo '555555';
			// User logged in, get user details
			$user = $this->facebook->request('get', '/me?fields=id,name,email');
			if (!isset($user['error']))
			{
				$data['user'] = $user;
				//print_r($user);
			}

		}

		// display view
		$this->load->view('examples/web', $data);
	}

	// ------------------------------------------------------------------------

	/**
	 * JS SDK login example
	 */
	public function js_login()
	{
		// Load view
		$this->load->view('examples/js');
	}

	// ------------------------------------------------------------------------

	/**
	 * AJAX request method for positing to facebook feed
	 */
	public function post()
	{
		header('Content-Type: application/json');
		$result = $this->facebook->request('post','/me/feed',['message' => $this->input->post('message')]);
		//var_dump($result);
		echo json_encode($result);


	}
	// ------------------------------------------------------------------------

	public function add_facebook_member(){

		//echo $this->Primaryclass->pre_var_dump($member_profile);
		$data=array(
			'member_fname'=>$this->input->post('first_name'),
			'member_lname'=>$this->input->post('last_name'),
			'member_type_id'=>1,
			'member_email'=>$this->input->post('email'),
			'member_profile'=>$this->input->post('picture'),
			'ip'=>$this->input->ip_address(),
			'facebook_id'=>$this->input->post('facebook_id'),
			'approved'=>1,
			'createdate'=>date("Y-m-d H:i:s")
		);

		$insert = $this->Member_model->add($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	/**
	 * Logout for web redirect example
	 *
	 * @return  [type]  [description]
	 */
	public function logout()
	{
		$this->facebook->destroy_session();
		redirect('example/js_login', redirect);
	}
}
