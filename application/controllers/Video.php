<?php
defined('BASEPATH') OR exit();
class Video extends CI_Controller {
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));								/***** LOADING HELPER TO AVOID PHP ERROR ****/
		$this->load->library('primaryclass');
		$this->load->library('dateclass');
		$this->load->model('Staff_model');								/***** LOADING Controller * Staff_model as all ****/
		$this->Staff=$this->Staff_model->info();
		$this->load->model('Video_model');								/***** LOADING Controller * Primaryfunc as all ****/
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function index()
	{
		$R=$this->Staff_model->info();
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function video()//List video
	{
		$data= $this->Video_model->get_video();
		$this->load->view('backoffice/video/datatable',array(
			'title'=>'ตารางข้อมูลวีดีโอ',
			'header'=>'yes',
			'url_href'=>'add_video',
			'data'=>$data,
		));
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function add_video()
	{

		if($this->input->post('action'))
		{
			$sfilepath = './uploads/video/.';
			$smaxsize = 4096000; //4MB
			$smaxwidth_video_pic_thumb = 1200;
			$smaxheight_video_pic_thumb = 1200;

			$smaxwidth_video_pic_large = 1200;
			$smaxheight_video_pic_large = 1200;

			// PIC THUMB===========================================================
			if( $_FILES['video_pic_thumb'] )
			{
				$video_pic_thumb = $this->primaryclass->do_upload( 'video_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_video_pic_thumb, $smaxheight_video_pic_thumb );
				if( $video_pic_thumb['upload_data']['file_name'] == '' )
				{
					$video_pic_thumb_have_file = $this->input->post('old_video_pic_thumb');
				}else{
					$video_pic_thumb_have_file = $video_pic_thumb['upload_data']['file_name'];
				}
			}
			else
			{
				$video_pic_thumb_have_file = '';
			}//IF PIC THUMB=========================================================

			// PIC LARGE=============================================================
			if( $_FILES['video_pic_large'] )
			{
				$video_pic_large = $this->primaryclass->do_upload('video_pic_large', $sfilepath, $smaxsize, $smaxwidth_video_pic_large, $smaxheight_video_pic_large );
				if( $video_pic_large['upload_data']['file_name'] == '' )
				{
					$video_pic_large_have_file = $this->input->post('old_video_pic_large');
				}else{
					$video_pic_large_have_file = $video_pic_large['upload_data']['file_name'];
				}
			}
			else
			{
				$video_pic_large_have_file = '';
			}//IF PIC LARGE==========================================================

			$data=array(
				'video_title'=>$this->input->post('video_title'),
				'video_caption'=>$this->input->post('video_caption'),
				'video_description'=>$this->input->post('video_description'),
				'video_pic_thumb'=>$video_pic_thumb_have_file,
				'video_pic_large'=>$video_pic_large_have_file,
				// 'video_slug'=>$this->input->post('video_slug'),
				// 'language_slug'=>$this->input->post('language_slug'),
				'meta_title'=>$this->input->post('meta_title'),
				'meta_keyword'=>$this->input->post('meta_keyword'),
				'meta_description'=>$this->input->post('meta_description'),
				'highlight'=>$this->input->post('highlight'),
				'recommended'=>$this->input->post('recommended'),
				'publish'=>$this->input->post('publish'),
				'createdate'=>date("Y-m-d H:i:s"),
				'createby'=>$_SESSION['zID'],
			);

			$insert = $this->Video_model->insert_video($data);

		}
		else
		{


			$this->load->view('backoffice/video/formadmin',array(
				'title'=>'เพิ่มข้อมูลวีดีโอ',
				'header'=>'yes',
				'formup'=>'add',
				'css'=>array(
					'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				),
				'js'=>array(
					'assets/plugins/tinymce/tinymce.min.js',
					'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
					'assets/pages/jquery.sweet-alert.init.js',
				),
				'wysiwigeditor'=>'wysiwigeditor',
			));
		}

	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	public function edit_video($id=0)
	{
		if($this->input->post('action'))
		{
			/* Upload Image */
			$sfilepath = './uploads/video/.';
			$smaxsize = 4096000; //4MB
			$smaxwidth_video_pic_thumb = 1000;
			$smaxheight_video_pic_thumb = 1000;

			$smaxwidth_video_pic_large = 1200;
			$smaxheight_video_pic_large = 1000;

			// PIC THUMB===========================================================
			if( $_FILES['video_pic_thumb'] )
			{
	//			$video_pic_thumb = $this->primaryclass->do_upload( 'video_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_video_pic_thumb, $smaxheight_video_pic_thumb );
				$video_pic_thumb = $this->primaryclass->do_upload( 'video_pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_video_pic_thumb, $smaxheight_video_pic_thumb );
				//$this->primaryclass->pre_var_dump($video_pic_thumb);

				if( $video_pic_thumb['upload_data']['file_name'] == '' )
				{
					$video_pic_thumb_have_file = $this->input->post('old_video_pic_thumb');
				}else{
					$video_pic_thumb_have_file = $video_pic_thumb['upload_data']['file_name'];

				}
			}
			else
			{
				$video_pic_thumb_have_file = '';
			}//IF PIC THUMB=========================================================

			// PIC LARGE=============================================================
			if( $_FILES['video_pic_large'] )
			{
				$video_pic_large = $this->primaryclass->do_upload('video_pic_large', $sfilepath, $smaxsize, $smaxwidth_video_pic_large, $smaxheight_video_pic_large );
				if( $video_pic_large['upload_data']['file_name'] == '' )
				{
					$video_pic_large_have_file = $this->input->post('old_video_pic_large');
				}else{
					$video_pic_large_have_file = $video_pic_large['upload_data']['file_name'];
				}
			}
			else
			{
				$video_pic_large_have_file = '';
			}//IF PIC LARGE==========================================================

			$data=$this->security->xss_clean($this->input->post());
			$data=array(
				'video_id'=>$data['video_id'],
				'video_title'=>$data['video_title'],
				'video_caption'=>$data['video_caption'],
				'video_description'=>$data['video_description'],
				'video_pic_thumb'=>$video_pic_thumb_have_file,
				'video_pic_large'=>$video_pic_large_have_file,
				//'video_slug'=>$data['video_slug'],
				//'language_slug'=>$data['language_slug'],
				'meta_title'=>$data['meta_title'],
				'meta_keyword'=>$data['meta_keyword'],
				'meta_description'=>$data['meta_description'],
				'highlight'=>$data['highlight'],
				'recommended'=>$data['recommended'],
				'publish'=>$data['publish'],
				'updatedate'=>date("Y-m-d H:i:s"),
				'createby'=>$_SESSION['zID'],
			);
			$update = $this->Video_model->update_video($data);
		}
		else
		{
			$row = $this->Video_model->get_info($id);
			$data['row'] = $row;
			$this->load->view('backoffice/video/formadmin', array(
				'title'=>'แก้ไขข้อมูลวีดีโอ',
				'header'=>'yes',
				'url_href'=>'edit',
				'formup'=>'edit',
				'css'=>array(
					'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
				),
				'js'=>array(
					'assets/plugins/tinymce/tinymce.min.js',
					'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
					'assets/pages/jquery.sweet-alert.init.js',
				),
				'wysiwigeditor'=>'wysiwigeditor',
				'data'=>$data,
			));
		}
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	public function display_video($id)
	{
		$data=array(
			'video_id'=>$this->input->post('video_id'),
			'publish'=>2,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);

		//echo $this->primaryclass->pre_var_dump($_POST['id']);
		$publish = $this->Video_model->update_video($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function undisplay_video($id)
	{
		$data=array(
			'video_id'=>$this->input->post('video_id'),
			'publish'=>1,
			'updatedate'=>date("Y-m-d H:i:s"),
			'updateby'=>$_SESSION['zID'],
		);
		//echo $_POST['video_id'];
		//echo $this->primaryclass->pre_var_dump($data);
		$publish = $this->Video_model->update_video($data);
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function delete_video($id)
	{
		$delete = $this->Video_model->delete_record('video_id', $id, 'video');
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


}
