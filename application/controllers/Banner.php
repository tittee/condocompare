<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Banner extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));                /***** LOADING HELPER TO AVOID PHP ERROR ****/
        $this->load->library('encryption');
        $this->load->library('primaryclass');
        $this->load->library('dateclass');
        $this->load->library('upload');
        $this->load->model('Staff_model');                        /* LOADING MODEL * Staff_model as staff */
        $this->Staff = $this->Staff_model->info();
        $this->load->model('Banner_model');                    /***** LOADING Controller * Primaryfunc as all ****/
        $this->load->model('Member_model', 'Member');               /***** LOADING Controller * Primaryfunc as all ****/
        $this->load->model('Configsystem_model', 'Configsystem');  /***** LOADING Controller * Configsystem_model ****/
        $this->load->model('M');                                   /***** LOADING Controller * Configsystem_model ****/
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function index($category_id = 0)
    {
        $data = $this->Banner_model->get_banner($category_id);

        $this->load->view('backoffice/banner/datatable', array(
        'title' => 'ตารางข้อมูลแบนเนอร์',
        'header' => 'yes',
        'url_href' => '#',
        'data' => $data,
        ));
    }
    //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function banner_category($category_id = 0)
    {
        $data = $this->Banner_model->get_category_banner($category_id);

        if ($category_id === 0) {
            $this->load->view('backoffice/banner/datatable-category', array(
                    'title' => 'ตารางหน้าข้อมูลแบนเนอร์',
                    'header' => 'yes',
                    'url_href' => '#',
                    'data' => $data,
                ));
        } else {
            $banner_category_name = $this->M->get_name_row('banner_category_name', 'banner_category_id', $category_id, 'banner_category');
            $data = $this->Banner_model->get_banner($category_id);
            $url_href = site_url('backoffice/banner/add/'.$category_id);
            $this->load->view('backoffice/banner/datatable', array(
            'title' => 'ตารางข้อมูลแบนเนอร์ : '.$banner_category_name,
            'header' => 'yes',
            'url_href' => $url_href,
            'data' => $data,
        ));
        }
    }
        //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function get_size_banner()
    {
        $banner_category_id = $this->input->post('banner_category_id', true);
        $data = $this->M->get_size_banner($banner_category_id);
        // $this->primaryclass->pre_var_dump($data);
        $output = '';
        $output .= 'กว้าง : '.$data->banner_width.'px X ยาว : '.$data->banner_height.'px';
        echo  $output;
    }
        //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add($category_id = 0)
    {

    //echo $category_id;
      if ($this->input->post('action')) {
          $data = $this->security->xss_clean($this->input->post());
          $insert = $this->Banner_model->insert_banner($data);
      } else {
          $data['banner_category'] = $this->Banner_model->get_category_banner($category_id);
          $data['banner'] = $this->Banner_model->get_banner($category_id);
            // $banner_category = $this->Banner_model->get_banner_category_option();
            $this->load->view('backoffice/banner/formadmin', array(
              'title' => 'เพิ่มข้อมูลแบนเนอร์',
              'header' => 'yes',
              'css' => array(
                'assets/plugins/fileuploads/css/dropify.min.css',
                'assets/plugins/timepicker/bootstrap-timepicker.min.css',
                'assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
              ),
              'js' => array(
                'assets/plugins/moment/moment.js',
                'assets/plugins/fileuploads/js/dropify.min.js',
                'assets/plugins/timepicker/bootstrap-timepicker.min.js',
                'assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
              ),
            //   'banner_category'=>$banner_category,
              'data' => $data,
            ));
      }
    }

        //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public function edit($id)
    {
        if ($this->input->post('action')) {
            $banner_category_id = $this->input->post('banner_category_id');
            $sizebanner = $this->M->get_size_banner($banner_category_id);
            $sfilepath = './uploads/banner/.';
            $smaxsize = 4096000; //4MB
        $smaxwidth_pic_thumb = $sizebanner->banner_width;
            $smaxheight_pic_thumb = $sizebanner->banner_height;
        // $smaxwidth_pic_thumb = 1600;
        // $smaxheight_pic_thumb = 1000;
        // PIC THUMB===========================================================
            if ($_FILES['pic_thumb']) {
                $pic_thumb = $this->primaryclass->do_upload('pic_thumb', $sfilepath, $smaxsize, $smaxwidth_pic_thumb, $smaxheight_pic_thumb);
                if ($pic_thumb['upload_data']['file_name'] == '') {
                    $pic_thumb_have_file = $this->input->post('old_pic_thumb');
                } else {
                    $pic_thumb_have_file = $pic_thumb['upload_data']['file_name'];
                }
            } else {
                $pic_thumb_have_file = '';
            }//IF PIC THUMB=========================================================

        $createdate = $this->primaryclass->set_explode_date_time($this->input->post('createdate_date'), $this->input->post('createdate_time'));
            $updatedate = $this->primaryclass->set_explode_date_time($this->input->post('updatedate_date'), $this->input->post('updatedate_time'));

            $data = $this->security->xss_clean($this->input->post());
            $data = array(
                'banner_id' => $data['banner_id'],
                'banner_category_id' => $data['banner_category_id'],
                'banner_title' => $data['banner_title'],
                'pic_thumb' => $pic_thumb_have_file,
                'banner_url' => $data['banner_url'],
                'publish' => $data['publish'],
                'createdate' => $createdate,
                'updatedate' => $updatedate,
            );
        // $this->primaryclass->pre_var_dump($data);
            $insert = $this->Banner_model->update_banner($data);
        } else {
            $data['row'] = $this->Banner_model->get_category_banner($id);
            $data['banner'] = $this->Banner_model->get_banner($id);
        // $this->primaryclass->pre_var_dump($data['row']);

        // $banner_category_id =  $data['row']->banner_category_id;
        $datasize = $this->M->get_size_banner($id);
            $size_banner = 'กว้าง : '.$datasize->banner_width.'px X ยาว : '.$datasize->banner_height.'px';

            $banner_category = $this->Banner_model->get_banner_category_option();
            $this->load->view('backoffice/banner/formadmin', array(
        'title' => 'แก้ไขข้อมูลแบนเนอร์',
        'header' => 'yes',
        'css' => array(
          'assets/plugins/fileuploads/css/dropify.min.css',
          'assets/plugins/timepicker/bootstrap-timepicker.min.css',
          'assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
        ),
        'js' => array(
          'assets/plugins/moment/moment.js',
          'assets/plugins/fileuploads/js/dropify.min.js',
          'assets/plugins/timepicker/bootstrap-timepicker.min.js',
          'assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
        ),
        'banner_category' => $banner_category,
        'size_banner' => $size_banner,
        'data' => $data,
        ));
        }
    }
        //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

      public function display_banner($id)
      {
          $data = array(
                'banner_id' => $this->input->post('banner_id'),
                'publish' => 2,
                'updatedate' => date('Y-m-d H:i:s'),
            );

            // echo $this->primaryclass->pre_var_dump($data);
            $publish = $this->Banner_model->update_banner($data);
      }
        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        public function undisplay_banner($id)
        {
            $data = array(
                'banner_id' => $this->input->post('banner_id'),
                'publish' => 1,
                'updatedate' => date('Y-m-d H:i:s'),
            );
            //echo $_POST['banner_id'];
            // echo $this->primaryclass->pre_var_dump($data);
            $publish = $this->Banner_model->update_banner($data);
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        public function delete_banner($id)
        {
            $delete = $this->Banner_model->delete_record('banner_id', $id, 'banner');
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

      public function banner_setting()
      {
          $this->load->model('Backoffice_model');
          if ($this->input->post('action')) {
              $data = $this->security->xss_clean($this->input->post());

              $data = array(
                'site_option_id' => $data['site_option_id'],
                'site_banner_autoplay' => $data['site_banner_autoplay'],
            );
              $update = $this->M->edit($this->input->post('site_option_id'), $data, 'site_option', 'site_option_id');
          } else {
              $id = 1;
              $data['row'] = $this->Backoffice_model->get_banner_autoplay_by_id($id);
              $this->load->view('backoffice/banner/formadmin-setting', array(
                    'title' => 'การตั้งค่าแบนเนอร์',
                    'header' => 'yes',
                    'BodyClass' => 'fixed-left',
                    'css' => array(
                        'assets/plugins/bootstrap-sweetalert/sweet-alert.css',
                    ),
                    'js' => array(
                        'assets/plugins/tinymce/tinymce.min.js',
                        'assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
                        'assets/pages/jquery.sweet-alert.init.js',
                    ),
                    'data' => $data,
                ));
          }
      }
        //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
}

/* End of file Banner.php */
