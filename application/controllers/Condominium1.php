<?php

defined('BASEPATH') or exit();
class Condominium extends CI_Controller
{
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));                                /***** LOADING HELPER TO AVOID PHP ERROR ****/
        $this->load->library('encryption');
        $this->load->library('primaryclass');
        $this->load->library('dateclass');
        $this->load->library('upload');
        $this->load->model('Staff_model');                                        /* LOADING MODEL * Staff_model as staff */
        $this->Staff = $this->Staff_model->info();
        $this->load->model('Condominium_model');                                /***** LOADING Controller * Primaryfunc as all ****/
        $this->load->model('Member_model', 'Member');                                /***** LOADING Controller * Primaryfunc as all ****/
        $this->load->model('Configsystem_model', 'Configsystem');                                /***** LOADING Controller * Configsystem_model ****/
        $this->load->model('M');                                /***** LOADING Controller * Configsystem_model ****/
//			$this->output->enable_profiler(TRUE);
    }
    //-----------------share_with_contact-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function index()
    {
        $R = $this->Staff_model->info();
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function view()
    {
        $data = $this->Condominium_model->get_condo();
        $this->load->view('backoffice/condominium/datatable', array(
            'title' => 'ตารางข้อมูลคอนโด',
            'header' => 'yes',
            'url_href' => 'add',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add()
    {
        $provinces = $this->M->get_provinces();
        $type_zone = $this->Configsystem->get_type_zone_option();
        $type_offering = $this->Configsystem->get_type_offering_option();
        $type_suites = $this->Configsystem->get_type_suites_option();
        $type_decoration = $this->Configsystem->get_type_decoration_option();
        $facilities = $this->Condominium_model->get_facilities_option();
        $featured = $this->Condominium_model->get_featured_option();
        $transportation_category = $this->Condominium_model->get_transportation_category_option();
        $mass_transportation_category = $this->Condominium_model->get_mass_transportation_category_option();
        $staff_list = $this->User_model->get_staff_option();

        $data = array();

        //$transportation = $this->Condominium_model->get_transportation_option(0);
        $member = $this->Member->get_fullname();

        $this->load->view('backoffice/condominium/formadmin', array(
            'title' => 'เพิ่มข้อมูลคอนโด',
            'googlemap' => 'yes',
            'header' => 'yes',
            'css' => array(
                'assets/plugins/fileuploads/css/dropify.min.css',
                'assets/plugins/timepicker/bootstrap-timepicker.min.css',
                'assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
            ),
            'js' => array(
                'assets/plugins/moment/moment.js',
                'assets/plugins/fileuploads/js/dropify.min.js',
                'assets/plugins/timepicker/bootstrap-timepicker.min.js',
                'assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
            ),
            'wysiwigeditor' => 'wysiwigeditor',
            'provinces' => $provinces,
            'type_zone' => $type_zone,
            'type_offering' => $type_offering,
            'type_suites' => $type_suites,
            'type_decoration' => $type_decoration,
            'facilities' => $facilities,
            'featured' => $featured,
            'transportation_category' => $transportation_category,
            'member' => $member,
            'staff_list' => $staff_list,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function images($condo_id = 0)
    {
        $data = $this->Condominium_model->get_condo_image($condo_id);

        $this->load->view('backoffice/condominium/datatable-image', array(
            'title' => 'รูปภาพคอนโดเพิ่มเติม',
            'header' => 'yes',
            'url_href' => '../add_image/'.$condo_id,
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_image($condo_id = 0, $id = 0)
    {
        $R = $this->Staff_model->info();
        if ($this->input->post('action')) {
            //มี action หลังจากการ Submit
            $sfilepath = './uploads/condominium/.';
            $smaxsize = 4096000; //4MB
            $smaxwidth = 2000;
            $smaxheight = 2000;

            for ($i = 1; $i <= 5; ++$i) {
                // PIC LARGE===========================================================
                if ($_FILES['pic_large_'.$i]) {
                    $pic_large[$i] = $this->primaryclass->do_upload('pic_large_'.$i, $sfilepath, $smaxsize, $smaxwidth, $smaxheight);
                    //$this->primaryclass->pre_var_dump($pic_large[$i]['upload_data']['file_name']);

                    if (!empty($pic_large[$i])) {
                        $data = $this->security->xss_clean($this->input->post());
                        $data = array(
                            'condo_id' => $this->input->post('condo_id'),
                            'condo_images_title' => $this->input->post('condo_images_title'),
                            'condo_images_description' => $this->input->post('condo_images_description'),
                            'pic_large' => $pic_large[$i]['upload_data']['file_name'],
                            'condo_images_url' => $this->input->post('condo_images_url'),
                            'createdate' => date('Y-m-d H:i:s'),
                            'publish' => $this->input->post('publish'),
                        );
                        $insert = $this->Condominium_model->set_image_condominium(0, $data);
                    }
                } else {
                    $pic_large[$i] = '';
                }
                // END PIC LARGE=======================================================
            }
        } else {
            //ไม่มี ACTION

            $this->load->view('backoffice/condominium/formadmin-image', array(
                'title' => 'เพิ่มรูปภาพข้อมูลคอนโด',
                'header' => 'yes',
                'BodyClass' => 'fixed-left',
                'wysiwigeditor' => 'wysiwigeditor',
            ));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_image($condo_id = 0, $id = 0)
    {
        $condo_id = $this->uri->segment(3);
        $id = $this->uri->segment(4);
        if (empty($id)) {
            show_404();
        }

        $R = $this->Staff_model->info();
        if ($this->input->post('action')) {
            //มี action หลังจากการ Submit
            $sfilepath = './uploads/condominium/.';
            $smaxsize = 4096000; //4MB
            $smaxwidth = 2000;
            $smaxheight = 2000;
            // PIC THUMB===========================================================
            if ($_FILES['pic_large']) {
                $pic_large = $this->primaryclass->do_upload('pic_large', $sfilepath, $smaxsize, $smaxwidth, $smaxheight);

                if ($pic_large['upload_data']['file_name'] == '') {
                    $pic_large_have_file = $this->input->post('old_pic_large');
                } else {
                    $pic_large_have_file = $pic_large['upload_data']['file_name'];
                }
            } else {
                $pic_large_have_file = '';
            }//IF PIC THUMB=========================================================

            $data = $this->security->xss_clean($this->input->post());
            $this->primaryclass->pre_var_dump($data);
            $data = array(
                'condo_id' => $data['condo_id'],
                'condo_images_title' => $data['condo_images_title'],
                'condo_images_description' => $data['condo_images_description'],
                'pic_large' => $pic_large_have_file,
                'updatedate' => date('Y-m-d H:i:s'),
                'publish' => $this->input->post('publish'),
            );
            $this->primaryclass->pre_var_dump($data);
            echo $update = $this->Condominium_model->set_image_condominium($id, $data);
        } else {
            $data = array();
            $row = $this->Condominium_model->get_condo_image_by_id($condo_id, $id);

            $data['row'] = $row;
            //ไม่มี ACTION
            $this->load->view('backoffice/condominium/formadmin-image', array(
                'title' => 'แก้ไขรูปภาพข้อมูลคอนโด',

                'header' => 'yes',
                'BodyClass' => 'fixed-left',
                'wysiwigeditor' => 'wysiwigeditor',
                'data' => $data,
            ));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function remove_condo_images_update()
    {
        $data = $this->security->xss_clean($this->input->post());
        $data = array(
            'condo_images_id' => $data['condo_images_id'],
            $this->input->post('field_name') => null,
        );
            //$this->primaryclass->pre_var_dump($data);

        $sfilepath = 'uploads/condominium/';
        $imagefile = $sfilepath.$this->input->post('imagefile');
        $this->M->update_record_dalete_image($data, $imagefile, 'condo_images');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function display_condo_images($id)
    {
        $data = array(
            'condo_images_id' => $this->input->post('condo_images_id'),
            'publish' => 2,
            'updatedate' => date('Y-m-d H:i:s'),
        );

        //echo $this->primaryclass->pre_var_dump($_POST['id']);
        $publish = $this->Condominium_model->update_condo_images($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function undisplay_condo_images($id)
    {
        $data = array(
            'condo_images_id' => $this->input->post('condo_images_id'),
            'publish' => 1,
            'updatedate' => date('Y-m-d H:i:s'),
        );
        //echo $_POST['condo_images_id'];
        //echo $this->primaryclass->pre_var_dump($data);
        $publish = $this->Condominium_model->update_condo_images($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function share($id)
    {
        $data['data'] = $this->Condominium_model->get_share_condo($id);
        $data['type_status'] = $this->Configsystem->get_type_status_option();

        $this->load->view('backoffice/condominium/datatable-share', array(
            'title' => 'การแชร์คอนโด',
            'header' => 'yes',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function share_with_contact($id = 0, $member_share_id = 0)
    {
        //$data= $this->Condominium_model->get_share_condo($id);
        $data['data'] = $this->Condominium_model->get_condo_contact_list($id, $member_share_id);
        $data['type_status'] = $this->Configsystem->get_type_status_option();

        $this->load->view('backoffice/condominium/datatable-share-form', array(
            'title' => 'การแชร์คอนโดที่ได้รับการติดต่อ',
            'header' => 'yes',
            'url_href' => 'add',
            'data' => $data,
        ));
//		$this->output->enable_profiler(TRUE);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_all_status_share() //AJAX
    {

//		$data =  json_encode($this->security->xss_clean($this->input->post()));
        $data = $this->security->xss_clean($this->input->raw_input_stream);
            //$this->primaryclass->pre_var_dump($data);

//		var_dump(json_decode($data, true) );

        $decoded = json_decode($data, true);
        foreach ($decoded as $value) {
            $$value['name'] = $value['value']."\n";
//				echo $value["name"] . "=" . $$value["name"]."\n";
//				echo "";

            $data = array(
                $value['name'] => $$value['name'],
            );
            $this->primaryclass->pre_var_dump($data);
        }
        //$member_share_condo_id =  $this->security->xss_clean($this->input->post('member_share_condo_id'));

//		$json = json_decode($data);
//		foreach($json->entries as $row)
//		{
//				foreach($row as $key => $val)
//				{
//						echo $key . ': ' . $val;
//						echo '<br>';
//				}
//		}

//		$array1 = array();
//		foreach($member_share_condo_id as $row => $val)
//		{
//			$arr_member_share_condo_id = array(
//				'member_share_condo_id'=>$val
//			); // $val . "\n"
//		}
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_status_share($id)
    {
        $data['data'] = $this->Condominium_model->get_share_condo($id);
        $data['type_status'] = $this->Configsystem->get_type_status_option();

        $this->load->view('backoffice/condominium/datatable-share', array(
            'title' => 'การแชร์คอนโด',
            'header' => 'yes',
            'css' => array(
                'assets/plugins/custombox/dist/custombox.min.css',
            ),
            'js' => array(
                'assets/plugins/custombox/dist/custombox.min.js',
                'assets/plugins/custombox/dist/legacy.min.js',
            ),
            'data' => $data,
        ));
//		$this->output->enable_profiler(TRUE);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_modal_status_share()
    {
        //Load Library
        $this->load->library('email');

        $data = $this->security->xss_clean($this->input->post());

        //$this->primaryclass->pre_var_dump($data);
        $data = array(
            'member_share_condo_id' => $data['member_share_condo_id'],
            'fk_type_status_id' => $data['fk_type_status_id'],
        );
        if (!empty($data)) {
            if ($this->input->post('action') === 'form-share') {
                $update = $this->M->update_record($data, 'member_share_condo');
                if ($update) {
                    $subject = 'อีเมล์ของระบบ';

                    $data_email = array();
                    $data_email = array(
                        'condo_id' => $this->input->post('condo_id'),
                        'member_share_condo_id' => $this->input->post('member_share_condo_id'),
                        'fk_type_status_id' => $this->input->post('fk_type_status_id'),
                    );
                    $message = $this->email_send_status($data_email);

                    $result = $this->email
                        ->from('hob@condo-compare.com')
                        ->reply_to('supawat@atmoststudio.com')    // Optional, an account where a human being reads.
                        ->to('tee.emilond@gmail.com')
                        ->subject($subject)
                        ->message($message['message'])
                        ->send();
                }
            }//END IF
        }//END IF
        return $data;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_modal_status_share_form()
    {
        //Load Library
        $this->load->library('email');

        $data = $this->security->xss_clean($this->input->post());

        // $this->primaryclass->pre_var_dump($data);

        if (!empty($data)) {
            if ($this->input->post('action') === 'form-share') {
                $data = array(
                    'condo_share_form_id' => $data['condo_share_form_id'],
                    'fk_condo_id' => $data['fk_condo_id'],
                    'fk_member_id' => $data['fk_member_id'],
                    'fk_type_status_id' => $data['fk_type_status_id'],
                );

                $update = $this->Condominium_model->update_condo_share_form_status($data);
                if ($update) {

                    ///////// EMAIL /////////

                    $fk_condo_id = $data['fk_condo_id'];
                    $fk_member_id = $data['fk_member_id'];
                    //สถานะ
                    $fk_type_status_id = (!empty($data['fk_type_status_id'])) ? $data['fk_type_status_id'] : '';
                    $fk_type_status_title = (!empty($fk_type_status_id)) ? $this->Configsystem->get_type_status_title($fk_type_status_id) : '';

                    //1. เจ้าของ Stock
                    $data_member_owner = $this->Condominium_model->get_member_owner_condo($fk_condo_id);
                    $condo_property_id = (!empty($data_member_owner->condo_property_id)) ? $data_member_owner->condo_property_id : '';
                    $data_member_owner_fname = (!empty($data_member_owner->member_fname)) ? $data_member_owner->member_fname : '';
                    $data_member_owner_lname = (!empty($data_member_owner->member_lname)) ? $data_member_owner->member_lname : '';
                    $data_member_owner_email = (!empty($data_member_owner->member_email)) ? $data_member_owner->member_email : '';
                    $data_member_owner_mobileno = (!empty($data_member_owner->member_mobileno)) ? $data_member_owner->member_mobileno : '';

                    //2. ผู้ที่สนใจ (คนที่เมล์ติดต่อมา)
                    $data_member_share_condo = $this->Condominium_model->get_condo_contact_by_id($fk_condo_id, $fk_member_id);
                    $condo_share_form_name = (!empty($data_member_share_condo->condo_share_form_name)) ? $data_member_share_condo->condo_share_form_name : '';
                    $condo_share_form_phone = (!empty($data_member_share_condo->condo_share_form_phone)) ? $data_member_share_condo->condo_share_form_phone : '';
                    $condo_share_form_email = (!empty($data_member_share_condo->condo_share_form_email)) ? $data_member_share_condo->condo_share_form_email : '';
                    $condo_share_form_message = (!empty($data_member_share_condo->condo_share_form_message)) ? $data_member_share_condo->condo_share_form_message : '';

                    //3. ผู้ที่ทำการแชร์ (HOB)
                    //$this->primaryclass->pre_var_dump($data_member_share_condo);
                    $data_member_share_condo_fname = (!empty($data_member_share_condo->member_fname)) ? $data_member_share_condo->member_fname : '';
                    $data_member_share_condo_lname = (!empty($data_member_share_condo->member_lname)) ? $data_member_share_condo->member_lname : '';
                    $data_member_share_condo_email = (!empty($data_member_share_condo->member_email)) ? $data_member_share_condo->member_email : '';
                    $data_member_share_condo_mobileno = (!empty($data_member_share_condo->member_mobileno)) ? $data_member_share_condo->member_mobileno : '';

                    //4. Admin
                    $site_id = 1;
                    $site_setting = $this->M->get_main_setting($site_id);

                    $website_name = base_url();
                    $site_logo = (!empty($site_setting['site_logo'])) ? $site_setting['site_logo'] : $website_name.'assets/images/logo.jpg';
                    $site_email = (!empty($site_setting['site_email'])) ? $site_setting['site_email'] : 'hawk@gmail.com, wut@hosting.co.th, tee.emilond@gmail.com';

                    //ข้อความของสถานะอีเมล์
                    if ($fk_type_status_id === 2) {
                        //โทรนัด, มีคนสนใจคอนโด

                        //1. หัวข้อ และข้อความ เจ้าของ Stock
                        $subject_for_condo_owner = "แจ้งเตือน ประกาศ รหัสทรัพย์ $condo_property_id";
                        $message_for_condo_owner = "<h4>สวัสดีคุณ, $data_member_owner_fname $data_member_owner_lname </h4>\n";
                        $message_for_condo_owner .= "<p>ขณะนี้ มีผู้สนใจประกาศ รหัสทรัพย์ '$condo_property_id' ของท่าน</p>\n";
                        $message_for_condo_owner .= "<hr>\n";
                        $message_for_condo_owner .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_condo_owner .= "<hr>\n";

                        $data_email_for_condo_owner = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_condo_owner,
                            'email_message' => $message_for_condo_owner,
                            'email' => $data_member_owner_email,
                        );
                        $success_email_for_condo_owner = $this->email_send_form_status($data_email_for_condo_owner);

                        //2. หัวข้อ และข้อความ ผู้ที่สนใจ (คนที่เมล์ติดต่อมา)
                        $subject_for_condo_contact = "แจ้งเตือน ประกาศ รหัสทรัพย์ $condo_property_id";
                        $message_for_condo_contact = "<h4>เรียนคุณ, $condo_share_form_name </h4>\n";
                        $message_for_condo_contact .= "<p>ขอบคุณครับ คุณ $condo_share_form_name เจ้าหน้าที่ของเราจะรีบติดต่อกลับท่านโดยเร็วที่สุด</p>\n";
                        $message_for_condo_contact .= "<hr>\n";
                        $message_for_condo_contact .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_condo_contact .= "<hr>\n";

                        $data_email_for_condo_contact = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_condo_contact,
                            'email_message' => $message_for_condo_contact,
                            'email' => $condo_share_form_email,
                        );
                        $success_email_for_condo_contact = $this->email_send_form_status($data_email_for_condo_contact);

                        //3. หัวข้อ และข้อความ ผู้ที่ทำการแชร์ (HOB)
                        $subject_for_condo_share = "แจ้งเตือน ประกาศ รหัสทรัพย์ $condo_property_id";
                        $message_for_condo_share = "<h4>เรียนคุณ, $data_member_share_condo_fname </h4>\n";
                        $message_for_condo_share .= "<p><p>ขณะนี้ มีผู้สนใจ ทรัพย์รหัส '$condo_property_id' ที่ท่านได้แชร์ออกไป</p>\n";
                        $message_for_condo_share .= "<hr>\n";
                        $message_for_condo_share .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_condo_share .= "<hr>\n";

                        $data_email_for_condo_share = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_condo_share,
                            'email_message' => $message_for_condo_share,
                            'email' => $data_member_share_condo_email,
                        );
                        $success_email_for_condo_share = $this->email_send_form_status($data_email_for_condo_share);

                        //4. แอดมิน
                        $subject_for_condo_admin = "แจ้งเตือน ประกาศ รหัสทรัพย์ $condo_property_id ผ่านทางเว็บไซต์";
                        $message_for_condo_admin = "<h4>เรียนคุณ, ผู้ดูแลระบบ </h4>\n";
                        $message_for_condo_admin .= "<p><p>ขณะนี้ มีผู้สนใจ ทรัพย์รหัส '$condo_property_id' ทางเว็บไซต์</p>\n";
                        $message_for_condo_admin .= "<hr>\n";
                        $message_for_condo_admin .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_condo_admin .= "<hr>\n";
                        $message_for_condo_admin .= "<h6>ผู้แชร์</h6>\n";
                        $message_for_condo_admin .= "<p><storng>ชื่อผู้แชร์ : </storng> $data_member_share_condo_fname $data_member_share_condo_lname</p>\n";
                        $message_for_condo_admin .= "<p><storng>อีเมล์ : </storng> $data_member_owner_email</p>\n";
                        $message_for_condo_admin .= "<p><storng>เบอร์ติดต่อ : </storng> $data_member_share_condo_mobileno</p>\n";
                        $message_for_condo_admin .= "<hr>\n";
                        $message_for_condo_admin .= "<h6>ผู้สนใจ</h6>\n";
                        $message_for_condo_admin .= "<p><storng>ชื่อผู้สนใจ : </storng> $condo_share_form_name</p>\n";
                        $message_for_condo_admin .= "<p><storng>อีเมล์ : </storng> $condo_share_form_email</p>\n";
                        $message_for_condo_admin .= "<p><storng>เบอร์ติดต่อ : </storng> $condo_share_form_phone</p>\n";
                        $message_for_condo_admin .= "<p><storng>ข้อความ : </storng> $condo_share_form_message</p>\n";
                        $message_for_condo_admin .= "<hr>\n";

                        $data_email_for_condo_admin = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_condo_admin,
                            'email_message' => $message_for_condo_admin,
                            'email' => $site_email,
                        );
                        $success_email_for_condo_share = $this->email_send_form_status($data_email_for_condo_admin);
                    }
                    // END IF

                    else {
                        //นัดดูสถานที่ | //เสนอราคา / ต่อรองราคา | //ทำสัญญา | //นัดวันทำสัญญา | //ซื้อขายสมบูรณ์

                        //1. หัวข้อ และข้อความ เจ้าของ Stock
                        $subject_for_condo_owner = "แจ้งเตือน ประกาศ รหัสทรัพย์ $condo_property_id";
                        $message_for_condo_owner = "<h4>สวัสดีคุณ, $data_member_owner_fname $data_member_owner_lname </h4>\n";
                        $message_for_condo_owner .= "<p>ขณะนี้ มีผู้สนใจประกาศ รหัสทรัพย์ '$condo_property_id' ของท่าน</p>\n";
                        $message_for_condo_owner .= "<hr>\n";
                        $message_for_condo_owner .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_condo_owner .= "<hr>\n";

                        $data_email_for_condo_owner = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_condo_owner,
                            'email_message' => $message_for_condo_owner,
                            'email' => $data_member_owner_email,
                        );
                        $success_email_for_condo_owner = $this->email_send_form_status($data_email_for_condo_owner);

                        //2. หัวข้อ และข้อความ ผู้ที่สนใจ (คนที่เมล์ติดต่อมา)
                        $subject_for_condo_contact = "แจ้งเตือน ประกาศ รหัสทรัพย์ $condo_property_id";
                        $message_for_condo_contact = "<h4>เรียนคุณ, $condo_share_form_name </h4>\n";
                        $message_for_condo_contact .= "<p>ขอบคุณครับ คุณ $condo_share_form_name เจ้าหน้าที่ของเราจะรีบติดต่อกลับท่านโดยเร็วที่สุด</p>\n";
                        $message_for_condo_contact .= "<hr>\n";
                        $message_for_condo_contact .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_condo_contact .= "<hr>\n";

                        $data_email_for_condo_contact = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_condo_contact,
                            'email_message' => $message_for_condo_contact,
                            'email' => $condo_share_form_email,
                        );
                        $success_email_for_condo_contact = $this->email_send_form_status($data_email_for_condo_contact);

                        //3. หัวข้อ และข้อความ ผู้ที่ทำการแชร์ (HOB)
                        $subject_for_condo_share = "แจ้งเตือน ประกาศ รหัสทรัพย์ $condo_property_id";
                        $message_for_condo_share = "<h4>เรียนคุณ, $data_member_share_condo_fname $data_member_share_condo_lname </h4>\n";
                        $message_for_condo_share .= "<p><p>ขณะนี้ มีผู้สนใจ ทรัพย์รหัส '$condo_property_id' ที่ท่านได้แชร์ออกไป</p>\n";
                        $message_for_condo_share .= "<hr>\n";
                        $message_for_condo_share .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_condo_share .= "<hr>\n";

                        $data_email_for_condo_share = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_condo_share,
                            'email_message' => $message_for_condo_share,
                            'email' => $data_member_share_condo_email,
                        );
                        $success_email_for_condo_share = $this->email_send_form_status($data_email_for_condo_share);

                        //4. แอดมิน
                        $subject_for_condo_admin = "แจ้งเตือน ประกาศ รหัสทรัพย์ $condo_property_id ผ่านทางเว็บไซต์";
                        $message_for_condo_admin = "<h4>เรียนคุณ, ผู้ดูแลระบบ </h4>\n";
                        $message_for_condo_admin .= "<p><p>ขณะนี้ มีผู้สนใจ ทรัพย์รหัส '$condo_property_id' ทางเว็บไซต์</p>\n";
                        $message_for_condo_admin .= "<hr>\n";
                        $message_for_condo_admin .= "<p><storng>สถานะการติดต่อ : </storng> $fk_type_status_title</p>\n";
                        $message_for_condo_admin .= "<hr>\n";

                        $data_email_for_condo_admin = array(
                            'email_logo' => $site_logo,
                            'email_subject' => $subject_for_condo_admin,
                            'email_message' => $message_for_condo_admin,
                            'email' => $site_email,
                        );
                        $success_email_for_condo_share = $this->email_send_form_status($data_email_for_condo_admin);
                    }
                }
            }//END IF
        }//END IF
        //return $data;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function email_send_status($data = '')
    {
        $get_member_share = '';
        $data['name'] = 'Wittawat Kittiwarabud';
        $data['email'] = 'tee.emilond@gmail.com';
        //$data['message_body'] = "any message body you want to send";

        $data['row'] = $this->Condominium_model->get_share_condo($data['condo_id']);

        //$data = '';
        $data['body'] = 'จะให้ใส่ข้อความว่าอะไรดีครับ';
        $data['condo_id'] = '';
        $data['condo_title'] = '';
        $data['pic_thumb'] = '';
        $data['type_status_title'] = '';
        $data['message'] = $this->load->view('backoffice/condominium/formadmin-email', $data, true); // this will return you html data as message
        //$this->email->message($message);
        return $data['message'];
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function email_send_form_stat__us($data = '')
    {
        $get_member_share = '';
        $fk_condo_id = $data['fk_condo_id'];
        $fk_member_id = $data['fk_member_id'];

        //1. เจ้าของ Stock
        $data_member_owner = $this->Condominium_model->get_member_owner_condo($fk_condo_id);
        $data_member_owner_fname = (!empty($data_member_owner->member_fname)) ? $data_member_owner->member_fname : '';
        $data_member_owner_lname = (!empty($data_member_owner->member_lname)) ? $data_member_owner->member_lname : '';
        $data_member_owner_lname = (!empty($data_member_owner->member_email)) ? $data_member_owner->member_email : '';
        $data_member_owner_mobileno = (!empty($data_member_owner->member_mobileno)) ? $data_member_owner->member_mobileno : '';

        //2. ผู้ที่สนใจ (คนที่เมล์ติดต่อมา)

        $data_member_share_condo = $this->Condominium_model->get_condo_contact_by_id($fk_condo_id, $fk_member_id);
        //$this->primaryclass->pre_var_dump($data_member_share_condo);
        $data_member_share_condo_fname = (!empty($data_member_share_condo->member_fname)) ? $data_member_share_condo->member_fname : '';
        $data_member_share_condo_lname = (!empty($data_member_share_condo->member_lname)) ? $data_member_share_condo->member_lname : '';
        $data_member_share_condo_email = (!empty($data_member_share_condo->member_email)) ? $data_member_share_condo->member_email : '';
        $data_member_share_condo_mobileno = (!empty($data_member_share_condo->member_mobileno)) ? $data_member_share_condo->member_mobileno : '';

        //3. ผู้ที่ทำการแชร์ (HOB)
        $condo_share_form_name = (!empty($data_member_share_condo->condo_share_form_name)) ? $data_member_share_condo->condo_share_form_name : '';
        $condo_share_form_phone = (!empty($data_member_share_condo->condo_share_form_phone)) ? $data_member_share_condo->condo_share_form_phone : '';
        $condo_share_form_email = (!empty($data_member_share_condo->condo_share_form_email)) ? $data_member_share_condo->condo_share_form_email : '';
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //EMAIL TEMPLATE
    public function email_send_form_status($data)
    {
        $subject = $data['email_subject'];
        $message = $data['email_message'];
        $site_logo = $data['email_logo'];

        //
        // Get full html:
        $body = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
        $body .=  "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n";
        $body .=  "\n";
        $body .=  "  <head>\n";
        $body .=  "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n";
        $body .=  "    <meta name=\"viewport\" content=\"width=device-width\">\n";
        $body .=  "    <title>Condo Compare</title>\n";
        $body .=  "    <style>\n";
        $body .=  "      .wrapper {\n";
        $body .=  "  width: 100%; }\n";
        $body .=  "\n";
        $body .=  "#outlook a {\n";
        $body .=  "  padding: 0; }\n";
        $body .=  "\n";
        $body .=  "body {\n";
        $body .=  "  width: 100% !important;\n";
        $body .=  "  min-width: 100%;\n";
        $body .=  "  -webkit-text-size-adjust: 100%;\n";
        $body .=  "  -ms-text-size-adjust: 100%;\n";
        $body .=  "  margin: 0;\n";
        $body .=  "  Margin: 0;\n";
        $body .=  "  padding: 0;\n";
        $body .=  "  -moz-box-sizing: border-box;\n";
        $body .=  "  -webkit-box-sizing: border-box;\n";
        $body .=  "  box-sizing: border-box; }\n";
        $body .=  "\n";
        $body .=  ".ExternalClass {\n";
        $body .=  "  width: 100%; }\n";
        $body .=  "  .ExternalClass,\n";
        $body .=  "  .ExternalClass p,\n";
        $body .=  "  .ExternalClass span,\n";
        $body .=  "  .ExternalClass font,\n";
        $body .=  "  .ExternalClass td,\n";
        $body .=  "  .ExternalClass div {\n";
        $body .=  "    line-height: 100%; }\n";
        $body .=  "\n";
        $body .=  "#backgroundTable {\n";
        $body .=  "  margin: 0;\n";
        $body .=  "  Margin: 0;\n";
        $body .=  "  padding: 0;\n";
        $body .=  "  width: 100% !important;\n";
        $body .=  "  line-height: 100% !important; }\n";
        $body .=  "\n";
        $body .=  "img {\n";
        $body .=  "  outline: none;\n";
        $body .=  "  text-decoration: none;\n";
        $body .=  "  -ms-interpolation-mode: bicubic;\n";
        $body .=  "  width: auto;\n";
        $body .=  "  max-width: 100%;\n";
        $body .=  "  clear: both;\n";
        $body .=  "  display: block; }\n";
        $body .=  "\n";
        $body .=  "center {\n";
        $body .=  "  width: 100%;\n";
        $body .=  "  min-width: 580px; }\n";
        $body .=  "\n";
        $body .=  "a img {\n";
        $body .=  "  border: none; }\n";
        $body .=  "\n";
        $body .=  "p {\n";
        $body .=  "  margin: 0 0 0 10px;\n";
        $body .=  "  Margin: 0 0 0 10px; }\n";
        $body .=  "\n";
        $body .=  "table {\n";
        $body .=  "  border-spacing: 0;\n";
        $body .=  "  border-collapse: collapse; }\n";
        $body .=  "\n";
        $body .=  "td {\n";
        $body .=  "  word-wrap: break-word;\n";
        $body .=  "  -webkit-hyphens: auto;\n";
        $body .=  "  -moz-hyphens: auto;\n";
        $body .=  "  hyphens: auto;\n";
        $body .=  "  border-collapse: collapse !important; }\n";
        $body .=  "\n";
        $body .=  "table, tr, td {\n";
        $body .=  "  padding: 0;\n";
        $body .=  "  vertical-align: top;\n";
        $body .=  "  text-align: left; }\n";
        $body .=  "\n";
        $body .=  "@media only screen {\n";
        $body .=  "  html {\n";
        $body .=  "    min-height: 100%;\n";
        $body .=  "    background: #f3f3f3; } }\n";
        $body .=  "\n";
        $body .=  "table.body {\n";
        $body .=  "  background: #f3f3f3;\n";
        $body .=  "  height: 100%;\n";
        $body .=  "  width: 100%; }\n";
        $body .=  "\n";
        $body .=  "table.container {\n";
        $body .=  "  background: #fefefe;\n";
        $body .=  "  width: 580px;\n";
        $body .=  "  margin: 0 auto;\n";
        $body .=  "  Margin: 0 auto;\n";
        $body .=  "  text-align: inherit; }\n";
        $body .=  "\n";
        $body .=  "table.row {\n";
        $body .=  "  padding: 0;\n";
        $body .=  "  width: 100%;\n";
        $body .=  "  position: relative; }\n";
        $body .=  "\n";
        $body .=  "table.spacer {\n";
        $body .=  "  width: 100%; }\n";
        $body .=  "  table.spacer td {\n";
        $body .=  "    mso-line-height-rule: exactly; }\n";
        $body .=  "\n";
        $body .=  "table.container table.row {\n";
        $body .=  "  display: table; }\n";
        $body .=  "\n";
        $body .=  "td.columns,\n";
        $body .=  "td.column,\n";
        $body .=  "th.columns,\n";
        $body .=  "th.column {\n";
        $body .=  "  margin: 0 auto;\n";
        $body .=  "  Margin: 0 auto;\n";
        $body .=  "  padding-left: 16px;\n";
        $body .=  "  padding-bottom: 16px; }\n";
        $body .=  "  td.columns .column,\n";
        $body .=  "  td.columns .columns,\n";
        $body .=  "  td.column .column,\n";
        $body .=  "  td.column .columns,\n";
        $body .=  "  th.columns .column,\n";
        $body .=  "  th.columns .columns,\n";
        $body .=  "  th.column .column,\n";
        $body .=  "  th.column .columns {\n";
        $body .=  "    padding-left: 0 !important;\n";
        $body .=  "    padding-right: 0 !important; }\n";
        $body .=  "    td.columns .column center,\n";
        $body .=  "    td.columns .columns center,\n";
        $body .=  "    td.column .column center,\n";
        $body .=  "    td.column .columns center,\n";
        $body .=  "    th.columns .column center,\n";
        $body .=  "    th.columns .columns center,\n";
        $body .=  "    th.column .column center,\n";
        $body .=  "    th.column .columns center {\n";
        $body .=  "      min-width: none !important; }\n";
        $body .=  "\n";
        $body .=  "td.columns.last,\n";
        $body .=  "td.column.last,\n";
        $body .=  "th.columns.last,\n";
        $body .=  "th.column.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.columns table:not(.button),\n";
        $body .=  "td.column table:not(.button),\n";
        $body .=  "th.columns table:not(.button),\n";
        $body .=  "th.column table:not(.button) {\n";
        $body .=  "  width: 100%; }\n";
        $body .=  "\n";
        $body .=  "td.large-1,\n";
        $body .=  "th.large-1 {\n";
        $body .=  "  width: 32.33333px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-1.first,\n";
        $body .=  "th.large-1.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-1.last,\n";
        $body .=  "th.large-1.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-1,\n";
        $body .=  ".collapse > tbody > tr > th.large-1 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 48.33333px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-1.first,\n";
        $body .=  ".collapse th.large-1.first,\n";
        $body .=  ".collapse td.large-1.last,\n";
        $body .=  ".collapse th.large-1.last {\n";
        $body .=  "  width: 56.33333px; }\n";
        $body .=  "\n";
        $body .=  "td.large-1 center,\n";
        $body .=  "th.large-1 center {\n";
        $body .=  "  min-width: 0.33333px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-1,\n";
        $body .=  ".body .column td.large-1,\n";
        $body .=  ".body .columns th.large-1,\n";
        $body .=  ".body .column th.large-1 {\n";
        $body .=  "  width: 8.33333%; }\n";
        $body .=  "\n";
        $body .=  "td.large-2,\n";
        $body .=  "th.large-2 {\n";
        $body .=  "  width: 80.66667px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-2.first,\n";
        $body .=  "th.large-2.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-2.last,\n";
        $body .=  "th.large-2.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-2,\n";
        $body .=  ".collapse > tbody > tr > th.large-2 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 96.66667px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-2.first,\n";
        $body .=  ".collapse th.large-2.first,\n";
        $body .=  ".collapse td.large-2.last,\n";
        $body .=  ".collapse th.large-2.last {\n";
        $body .=  "  width: 104.66667px; }\n";
        $body .=  "\n";
        $body .=  "td.large-2 center,\n";
        $body .=  "th.large-2 center {\n";
        $body .=  "  min-width: 48.66667px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-2,\n";
        $body .=  ".body .column td.large-2,\n";
        $body .=  ".body .columns th.large-2,\n";
        $body .=  ".body .column th.large-2 {\n";
        $body .=  "  width: 16.66667%; }\n";
        $body .=  "\n";
        $body .=  "td.large-3,\n";
        $body .=  "th.large-3 {\n";
        $body .=  "  width: 129px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-3.first,\n";
        $body .=  "th.large-3.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-3.last,\n";
        $body .=  "th.large-3.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-3,\n";
        $body .=  ".collapse > tbody > tr > th.large-3 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 145px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-3.first,\n";
        $body .=  ".collapse th.large-3.first,\n";
        $body .=  ".collapse td.large-3.last,\n";
        $body .=  ".collapse th.large-3.last {\n";
        $body .=  "  width: 153px; }\n";
        $body .=  "\n";
        $body .=  "td.large-3 center,\n";
        $body .=  "th.large-3 center {\n";
        $body .=  "  min-width: 97px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-3,\n";
        $body .=  ".body .column td.large-3,\n";
        $body .=  ".body .columns th.large-3,\n";
        $body .=  ".body .column th.large-3 {\n";
        $body .=  "  width: 25%; }\n";
        $body .=  "\n";
        $body .=  "td.large-4,\n";
        $body .=  "th.large-4 {\n";
        $body .=  "  width: 177.33333px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-4.first,\n";
        $body .=  "th.large-4.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-4.last,\n";
        $body .=  "th.large-4.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-4,\n";
        $body .=  ".collapse > tbody > tr > th.large-4 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 193.33333px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-4.first,\n";
        $body .=  ".collapse th.large-4.first,\n";
        $body .=  ".collapse td.large-4.last,\n";
        $body .=  ".collapse th.large-4.last {\n";
        $body .=  "  width: 201.33333px; }\n";
        $body .=  "\n";
        $body .=  "td.large-4 center,\n";
        $body .=  "th.large-4 center {\n";
        $body .=  "  min-width: 145.33333px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-4,\n";
        $body .=  ".body .column td.large-4,\n";
        $body .=  ".body .columns th.large-4,\n";
        $body .=  ".body .column th.large-4 {\n";
        $body .=  "  width: 33.33333%; }\n";
        $body .=  "\n";
        $body .=  "td.large-5,\n";
        $body .=  "th.large-5 {\n";
        $body .=  "  width: 225.66667px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-5.first,\n";
        $body .=  "th.large-5.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-5.last,\n";
        $body .=  "th.large-5.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-5,\n";
        $body .=  ".collapse > tbody > tr > th.large-5 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 241.66667px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-5.first,\n";
        $body .=  ".collapse th.large-5.first,\n";
        $body .=  ".collapse td.large-5.last,\n";
        $body .=  ".collapse th.large-5.last {\n";
        $body .=  "  width: 249.66667px; }\n";
        $body .=  "\n";
        $body .=  "td.large-5 center,\n";
        $body .=  "th.large-5 center {\n";
        $body .=  "  min-width: 193.66667px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-5,\n";
        $body .=  ".body .column td.large-5,\n";
        $body .=  ".body .columns th.large-5,\n";
        $body .=  ".body .column th.large-5 {\n";
        $body .=  "  width: 41.66667%; }\n";
        $body .=  "\n";
        $body .=  "td.large-6,\n";
        $body .=  "th.large-6 {\n";
        $body .=  "  width: 274px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-6.first,\n";
        $body .=  "th.large-6.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-6.last,\n";
        $body .=  "th.large-6.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-6,\n";
        $body .=  ".collapse > tbody > tr > th.large-6 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 290px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-6.first,\n";
        $body .=  ".collapse th.large-6.first,\n";
        $body .=  ".collapse td.large-6.last,\n";
        $body .=  ".collapse th.large-6.last {\n";
        $body .=  "  width: 298px; }\n";
        $body .=  "\n";
        $body .=  "td.large-6 center,\n";
        $body .=  "th.large-6 center {\n";
        $body .=  "  min-width: 242px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-6,\n";
        $body .=  ".body .column td.large-6,\n";
        $body .=  ".body .columns th.large-6,\n";
        $body .=  ".body .column th.large-6 {\n";
        $body .=  "  width: 50%; }\n";
        $body .=  "\n";
        $body .=  "td.large-7,\n";
        $body .=  "th.large-7 {\n";
        $body .=  "  width: 322.33333px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-7.first,\n";
        $body .=  "th.large-7.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-7.last,\n";
        $body .=  "th.large-7.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-7,\n";
        $body .=  ".collapse > tbody > tr > th.large-7 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 338.33333px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-7.first,\n";
        $body .=  ".collapse th.large-7.first,\n";
        $body .=  ".collapse td.large-7.last,\n";
        $body .=  ".collapse th.large-7.last {\n";
        $body .=  "  width: 346.33333px; }\n";
        $body .=  "\n";
        $body .=  "td.large-7 center,\n";
        $body .=  "th.large-7 center {\n";
        $body .=  "  min-width: 290.33333px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-7,\n";
        $body .=  ".body .column td.large-7,\n";
        $body .=  ".body .columns th.large-7,\n";
        $body .=  ".body .column th.large-7 {\n";
        $body .=  "  width: 58.33333%; }\n";
        $body .=  "\n";
        $body .=  "td.large-8,\n";
        $body .=  "th.large-8 {\n";
        $body .=  "  width: 370.66667px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-8.first,\n";
        $body .=  "th.large-8.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-8.last,\n";
        $body .=  "th.large-8.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-8,\n";
        $body .=  ".collapse > tbody > tr > th.large-8 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 386.66667px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-8.first,\n";
        $body .=  ".collapse th.large-8.first,\n";
        $body .=  ".collapse td.large-8.last,\n";
        $body .=  ".collapse th.large-8.last {\n";
        $body .=  "  width: 394.66667px; }\n";
        $body .=  "\n";
        $body .=  "td.large-8 center,\n";
        $body .=  "th.large-8 center {\n";
        $body .=  "  min-width: 338.66667px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-8,\n";
        $body .=  ".body .column td.large-8,\n";
        $body .=  ".body .columns th.large-8,\n";
        $body .=  ".body .column th.large-8 {\n";
        $body .=  "  width: 66.66667%; }\n";
        $body .=  "\n";
        $body .=  "td.large-9,\n";
        $body .=  "th.large-9 {\n";
        $body .=  "  width: 419px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-9.first,\n";
        $body .=  "th.large-9.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-9.last,\n";
        $body .=  "th.large-9.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-9,\n";
        $body .=  ".collapse > tbody > tr > th.large-9 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 435px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-9.first,\n";
        $body .=  ".collapse th.large-9.first,\n";
        $body .=  ".collapse td.large-9.last,\n";
        $body .=  ".collapse th.large-9.last {\n";
        $body .=  "  width: 443px; }\n";
        $body .=  "\n";
        $body .=  "td.large-9 center,\n";
        $body .=  "th.large-9 center {\n";
        $body .=  "  min-width: 387px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-9,\n";
        $body .=  ".body .column td.large-9,\n";
        $body .=  ".body .columns th.large-9,\n";
        $body .=  ".body .column th.large-9 {\n";
        $body .=  "  width: 75%; }\n";
        $body .=  "\n";
        $body .=  "td.large-10,\n";
        $body .=  "th.large-10 {\n";
        $body .=  "  width: 467.33333px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-10.first,\n";
        $body .=  "th.large-10.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-10.last,\n";
        $body .=  "th.large-10.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-10,\n";
        $body .=  ".collapse > tbody > tr > th.large-10 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 483.33333px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-10.first,\n";
        $body .=  ".collapse th.large-10.first,\n";
        $body .=  ".collapse td.large-10.last,\n";
        $body .=  ".collapse th.large-10.last {\n";
        $body .=  "  width: 491.33333px; }\n";
        $body .=  "\n";
        $body .=  "td.large-10 center,\n";
        $body .=  "th.large-10 center {\n";
        $body .=  "  min-width: 435.33333px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-10,\n";
        $body .=  ".body .column td.large-10,\n";
        $body .=  ".body .columns th.large-10,\n";
        $body .=  ".body .column th.large-10 {\n";
        $body .=  "  width: 83.33333%; }\n";
        $body .=  "\n";
        $body .=  "td.large-11,\n";
        $body .=  "th.large-11 {\n";
        $body .=  "  width: 515.66667px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-11.first,\n";
        $body .=  "th.large-11.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-11.last,\n";
        $body .=  "th.large-11.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-11,\n";
        $body .=  ".collapse > tbody > tr > th.large-11 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 531.66667px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-11.first,\n";
        $body .=  ".collapse th.large-11.first,\n";
        $body .=  ".collapse td.large-11.last,\n";
        $body .=  ".collapse th.large-11.last {\n";
        $body .=  "  width: 539.66667px; }\n";
        $body .=  "\n";
        $body .=  "td.large-11 center,\n";
        $body .=  "th.large-11 center {\n";
        $body .=  "  min-width: 483.66667px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-11,\n";
        $body .=  ".body .column td.large-11,\n";
        $body .=  ".body .columns th.large-11,\n";
        $body .=  ".body .column th.large-11 {\n";
        $body .=  "  width: 91.66667%; }\n";
        $body .=  "\n";
        $body .=  "td.large-12,\n";
        $body .=  "th.large-12 {\n";
        $body .=  "  width: 564px;\n";
        $body .=  "  padding-left: 8px;\n";
        $body .=  "  padding-right: 8px; }\n";
        $body .=  "\n";
        $body .=  "td.large-12.first,\n";
        $body .=  "th.large-12.first {\n";
        $body .=  "  padding-left: 16px; }\n";
        $body .=  "\n";
        $body .=  "td.large-12.last,\n";
        $body .=  "th.large-12.last {\n";
        $body .=  "  padding-right: 16px; }\n";
        $body .=  "\n";
        $body .=  ".collapse > tbody > tr > td.large-12,\n";
        $body .=  ".collapse > tbody > tr > th.large-12 {\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  padding-left: 0;\n";
        $body .=  "  width: 580px; }\n";
        $body .=  "\n";
        $body .=  ".collapse td.large-12.first,\n";
        $body .=  ".collapse th.large-12.first,\n";
        $body .=  ".collapse td.large-12.last,\n";
        $body .=  ".collapse th.large-12.last {\n";
        $body .=  "  width: 588px; }\n";
        $body .=  "\n";
        $body .=  "td.large-12 center,\n";
        $body .=  "th.large-12 center {\n";
        $body .=  "  min-width: 532px; }\n";
        $body .=  "\n";
        $body .=  ".body .columns td.large-12,\n";
        $body .=  ".body .column td.large-12,\n";
        $body .=  ".body .columns th.large-12,\n";
        $body .=  ".body .column th.large-12 {\n";
        $body .=  "  width: 100%; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-1,\n";
        $body .=  "td.large-offset-1.first,\n";
        $body .=  "td.large-offset-1.last,\n";
        $body .=  "th.large-offset-1,\n";
        $body .=  "th.large-offset-1.first,\n";
        $body .=  "th.large-offset-1.last {\n";
        $body .=  "  padding-left: 64.33333px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-2,\n";
        $body .=  "td.large-offset-2.first,\n";
        $body .=  "td.large-offset-2.last,\n";
        $body .=  "th.large-offset-2,\n";
        $body .=  "th.large-offset-2.first,\n";
        $body .=  "th.large-offset-2.last {\n";
        $body .=  "  padding-left: 112.66667px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-3,\n";
        $body .=  "td.large-offset-3.first,\n";
        $body .=  "td.large-offset-3.last,\n";
        $body .=  "th.large-offset-3,\n";
        $body .=  "th.large-offset-3.first,\n";
        $body .=  "th.large-offset-3.last {\n";
        $body .=  "  padding-left: 161px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-4,\n";
        $body .=  "td.large-offset-4.first,\n";
        $body .=  "td.large-offset-4.last,\n";
        $body .=  "th.large-offset-4,\n";
        $body .=  "th.large-offset-4.first,\n";
        $body .=  "th.large-offset-4.last {\n";
        $body .=  "  padding-left: 209.33333px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-5,\n";
        $body .=  "td.large-offset-5.first,\n";
        $body .=  "td.large-offset-5.last,\n";
        $body .=  "th.large-offset-5,\n";
        $body .=  "th.large-offset-5.first,\n";
        $body .=  "th.large-offset-5.last {\n";
        $body .=  "  padding-left: 257.66667px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-6,\n";
        $body .=  "td.large-offset-6.first,\n";
        $body .=  "td.large-offset-6.last,\n";
        $body .=  "th.large-offset-6,\n";
        $body .=  "th.large-offset-6.first,\n";
        $body .=  "th.large-offset-6.last {\n";
        $body .=  "  padding-left: 306px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-7,\n";
        $body .=  "td.large-offset-7.first,\n";
        $body .=  "td.large-offset-7.last,\n";
        $body .=  "th.large-offset-7,\n";
        $body .=  "th.large-offset-7.first,\n";
        $body .=  "th.large-offset-7.last {\n";
        $body .=  "  padding-left: 354.33333px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-8,\n";
        $body .=  "td.large-offset-8.first,\n";
        $body .=  "td.large-offset-8.last,\n";
        $body .=  "th.large-offset-8,\n";
        $body .=  "th.large-offset-8.first,\n";
        $body .=  "th.large-offset-8.last {\n";
        $body .=  "  padding-left: 402.66667px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-9,\n";
        $body .=  "td.large-offset-9.first,\n";
        $body .=  "td.large-offset-9.last,\n";
        $body .=  "th.large-offset-9,\n";
        $body .=  "th.large-offset-9.first,\n";
        $body .=  "th.large-offset-9.last {\n";
        $body .=  "  padding-left: 451px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-10,\n";
        $body .=  "td.large-offset-10.first,\n";
        $body .=  "td.large-offset-10.last,\n";
        $body .=  "th.large-offset-10,\n";
        $body .=  "th.large-offset-10.first,\n";
        $body .=  "th.large-offset-10.last {\n";
        $body .=  "  padding-left: 499.33333px; }\n";
        $body .=  "\n";
        $body .=  "td.large-offset-11,\n";
        $body .=  "td.large-offset-11.first,\n";
        $body .=  "td.large-offset-11.last,\n";
        $body .=  "th.large-offset-11,\n";
        $body .=  "th.large-offset-11.first,\n";
        $body .=  "th.large-offset-11.last {\n";
        $body .=  "  padding-left: 547.66667px; }\n";
        $body .=  "\n";
        $body .=  "td.expander,\n";
        $body .=  "th.expander {\n";
        $body .=  "  visibility: hidden;\n";
        $body .=  "  width: 0;\n";
        $body .=  "  padding: 0 !important; }\n";
        $body .=  "\n";
        $body .=  "table.container.radius {\n";
        $body .=  "  border-radius: 0;\n";
        $body .=  "  border-collapse: separate; }\n";
        $body .=  "\n";
        $body .=  ".block-grid {\n";
        $body .=  "  width: 100%;\n";
        $body .=  "  max-width: 580px; }\n";
        $body .=  "  .block-grid td {\n";
        $body .=  "    display: inline-block;\n";
        $body .=  "    padding: 8px; }\n";
        $body .=  "\n";
        $body .=  ".up-2 td {\n";
        $body .=  "  width: 274px !important; }\n";
        $body .=  "\n";
        $body .=  ".up-3 td {\n";
        $body .=  "  width: 177px !important; }\n";
        $body .=  "\n";
        $body .=  ".up-4 td {\n";
        $body .=  "  width: 129px !important; }\n";
        $body .=  "\n";
        $body .=  ".up-5 td {\n";
        $body .=  "  width: 100px !important; }\n";
        $body .=  "\n";
        $body .=  ".up-6 td {\n";
        $body .=  "  width: 80px !important; }\n";
        $body .=  "\n";
        $body .=  ".up-7 td {\n";
        $body .=  "  width: 66px !important; }\n";
        $body .=  "\n";
        $body .=  ".up-8 td {\n";
        $body .=  "  width: 56px !important; }\n";
        $body .=  "\n";
        $body .=  "table.text-center,\n";
        $body .=  "th.text-center,\n";
        $body .=  "td.text-center,\n";
        $body .=  "h1.text-center,\n";
        $body .=  "h2.text-center,\n";
        $body .=  "h3.text-center,\n";
        $body .=  "h4.text-center,\n";
        $body .=  "h5.text-center,\n";
        $body .=  "h6.text-center,\n";
        $body .=  "p.text-center,\n";
        $body .=  "span.text-center {\n";
        $body .=  "  text-align: center; }\n";
        $body .=  "\n";
        $body .=  "table.text-left,\n";
        $body .=  "th.text-left,\n";
        $body .=  "td.text-left,\n";
        $body .=  "h1.text-left,\n";
        $body .=  "h2.text-left,\n";
        $body .=  "h3.text-left,\n";
        $body .=  "h4.text-left,\n";
        $body .=  "h5.text-left,\n";
        $body .=  "h6.text-left,\n";
        $body .=  "p.text-left,\n";
        $body .=  "span.text-left {\n";
        $body .=  "  text-align: left; }\n";
        $body .=  "\n";
        $body .=  "table.text-right,\n";
        $body .=  "th.text-right,\n";
        $body .=  "td.text-right,\n";
        $body .=  "h1.text-right,\n";
        $body .=  "h2.text-right,\n";
        $body .=  "h3.text-right,\n";
        $body .=  "h4.text-right,\n";
        $body .=  "h5.text-right,\n";
        $body .=  "h6.text-right,\n";
        $body .=  "p.text-right,\n";
        $body .=  "span.text-right {\n";
        $body .=  "  text-align: right; }\n";
        $body .=  "\n";
        $body .=  "span.text-center {\n";
        $body .=  "  display: block;\n";
        $body .=  "  width: 100%;\n";
        $body .=  "  text-align: center; }\n";
        $body .=  "\n";
        $body .=  "@media only screen and (max-width: 596px) {\n";
        $body .=  "  .small-float-center {\n";
        $body .=  "    margin: 0 auto !important;\n";
        $body .=  "    float: none !important;\n";
        $body .=  "    text-align: center !important; }\n";
        $body .=  "  .small-text-center {\n";
        $body .=  "    text-align: center !important; }\n";
        $body .=  "  .small-text-left {\n";
        $body .=  "    text-align: left !important; }\n";
        $body .=  "  .small-text-right {\n";
        $body .=  "    text-align: right !important; } }\n";
        $body .=  "\n";
        $body .=  "img.float-left {\n";
        $body .=  "  float: left;\n";
        $body .=  "  text-align: left; }\n";
        $body .=  "\n";
        $body .=  "img.float-right {\n";
        $body .=  "  float: right;\n";
        $body .=  "  text-align: right; }\n";
        $body .=  "\n";
        $body .=  "img.float-center,\n";
        $body .=  "img.text-center {\n";
        $body .=  "  margin: 0 auto;\n";
        $body .=  "  Margin: 0 auto;\n";
        $body .=  "  float: none;\n";
        $body .=  "  text-align: center; }\n";
        $body .=  "\n";
        $body .=  "table.float-center,\n";
        $body .=  "td.float-center,\n";
        $body .=  "th.float-center {\n";
        $body .=  "  margin: 0 auto;\n";
        $body .=  "  Margin: 0 auto;\n";
        $body .=  "  float: none;\n";
        $body .=  "  text-align: center; }\n";
        $body .=  "\n";
        $body .=  ".hide-for-large {\n";
        $body .=  "  display: none !important;\n";
        $body .=  "  mso-hide: all;\n";
        $body .=  "  overflow: hidden;\n";
        $body .=  "  max-height: 0;\n";
        $body .=  "  font-size: 0;\n";
        $body .=  "  width: 0;\n";
        $body .=  "  line-height: 0; }\n";
        $body .=  "  @media only screen and (max-width: 596px) {\n";
        $body .=  "    .hide-for-large {\n";
        $body .=  "      display: block !important;\n";
        $body .=  "      width: auto !important;\n";
        $body .=  "      overflow: visible !important;\n";
        $body .=  "      max-height: none !important;\n";
        $body .=  "      font-size: inherit !important;\n";
        $body .=  "      line-height: inherit !important; } }\n";
        $body .=  "\n";
        $body .=  "table.body table.container .hide-for-large * {\n";
        $body .=  "  mso-hide: all; }\n";
        $body .=  "\n";
        $body .=  "@media only screen and (max-width: 596px) {\n";
        $body .=  "  table.body table.container .hide-for-large,\n";
        $body .=  "  table.body table.container .row.hide-for-large {\n";
        $body .=  "    display: table !important;\n";
        $body .=  "    width: 100% !important; } }\n";
        $body .=  "\n";
        $body .=  "@media only screen and (max-width: 596px) {\n";
        $body .=  "  table.body table.container .callout-inner.hide-for-large {\n";
        $body .=  "    display: table-cell !important;\n";
        $body .=  "    width: 100% !important; } }\n";
        $body .=  "\n";
        $body .=  "@media only screen and (max-width: 596px) {\n";
        $body .=  "  table.body table.container .show-for-large {\n";
        $body .=  "    display: none !important;\n";
        $body .=  "    width: 0;\n";
        $body .=  "    mso-hide: all;\n";
        $body .=  "    overflow: hidden; } }\n";
        $body .=  "\n";
        $body .=  "body,\n";
        $body .=  "table.body,\n";
        $body .=  "h1,\n";
        $body .=  "h2,\n";
        $body .=  "h3,\n";
        $body .=  "h4,\n";
        $body .=  "h5,\n";
        $body .=  "h6,\n";
        $body .=  "p,\n";
        $body .=  "td,\n";
        $body .=  "th,\n";
        $body .=  "a {\n";
        $body .=  "  color: #0a0a0a;\n";
        $body .=  "  font-family: Helvetica, Arial, sans-serif;\n";
        $body .=  "  font-weight: normal;\n";
        $body .=  "  padding: 0;\n";
        $body .=  "  margin: 0;\n";
        $body .=  "  Margin: 0;\n";
        $body .=  "  text-align: left;\n";
        $body .=  "  line-height: 1.3; }\n";
        $body .=  "\n";
        $body .=  "h1,\n";
        $body .=  "h2,\n";
        $body .=  "h3,\n";
        $body .=  "h4,\n";
        $body .=  "h5,\n";
        $body .=  "h6 {\n";
        $body .=  "  color: inherit;\n";
        $body .=  "  word-wrap: normal;\n";
        $body .=  "  font-family: Helvetica, Arial, sans-serif;\n";
        $body .=  "  font-weight: normal;\n";
        $body .=  "  margin-bottom: 10px;\n";
        $body .=  "  Margin-bottom: 10px; }\n";
        $body .=  "\n";
        $body .=  "h1 {\n";
        $body .=  "  font-size: 34px; }\n";
        $body .=  "\n";
        $body .=  "h2 {\n";
        $body .=  "  font-size: 30px; }\n";
        $body .=  "\n";
        $body .=  "h3 {\n";
        $body .=  "  font-size: 28px; }\n";
        $body .=  "\n";
        $body .=  "h4 {\n";
        $body .=  "  font-size: 24px; }\n";
        $body .=  "\n";
        $body .=  "h5 {\n";
        $body .=  "  font-size: 20px; }\n";
        $body .=  "\n";
        $body .=  "h6 {\n";
        $body .=  "  font-size: 18px; }\n";
        $body .=  "\n";
        $body .=  "body,\n";
        $body .=  "table.body,\n";
        $body .=  "p,\n";
        $body .=  "td,\n";
        $body .=  "th {\n";
        $body .=  "  font-size: 16px;\n";
        $body .=  "  line-height: 1.3; }\n";
        $body .=  "\n";
        $body .=  "p {\n";
        $body .=  "  margin-bottom: 10px;\n";
        $body .=  "  Margin-bottom: 10px; }\n";
        $body .=  "  p.lead {\n";
        $body .=  "    font-size: 20px;\n";
        $body .=  "    line-height: 1.6; }\n";
        $body .=  "  p.subheader {\n";
        $body .=  "    margin-top: 4px;\n";
        $body .=  "    margin-bottom: 8px;\n";
        $body .=  "    Margin-top: 4px;\n";
        $body .=  "    Margin-bottom: 8px;\n";
        $body .=  "    font-weight: normal;\n";
        $body .=  "    line-height: 1.4;\n";
        $body .=  "    color: #8a8a8a; }\n";
        $body .=  "\n";
        $body .=  "small {\n";
        $body .=  "  font-size: 80%;\n";
        $body .=  "  color: #cacaca; }\n";
        $body .=  "\n";
        $body .=  "a {\n";
        $body .=  "  color: #2199e8;\n";
        $body .=  "  text-decoration: none; }\n";
        $body .=  "  a:hover {\n";
        $body .=  "    color: #147dc2; }\n";
        $body .=  "  a:active {\n";
        $body .=  "    color: #147dc2; }\n";
        $body .=  "  a:visited {\n";
        $body .=  "    color: #2199e8; }\n";
        $body .=  "\n";
        $body .=  "h1 a,\n";
        $body .=  "h1 a:visited,\n";
        $body .=  "h2 a,\n";
        $body .=  "h2 a:visited,\n";
        $body .=  "h3 a,\n";
        $body .=  "h3 a:visited,\n";
        $body .=  "h4 a,\n";
        $body .=  "h4 a:visited,\n";
        $body .=  "h5 a,\n";
        $body .=  "h5 a:visited,\n";
        $body .=  "h6 a,\n";
        $body .=  "h6 a:visited {\n";
        $body .=  "  color: #2199e8; }\n";
        $body .=  "\n";
        $body .=  "pre {\n";
        $body .=  "  background: #f3f3f3;\n";
        $body .=  "  margin: 30px 0;\n";
        $body .=  "  Margin: 30px 0; }\n";
        $body .=  "  pre code {\n";
        $body .=  "    color: #cacaca; }\n";
        $body .=  "    pre code span.callout {\n";
        $body .=  "      color: #8a8a8a;\n";
        $body .=  "      font-weight: bold; }\n";
        $body .=  "    pre code span.callout-strong {\n";
        $body .=  "      color: #ff6908;\n";
        $body .=  "      font-weight: bold; }\n";
        $body .=  "\n";
        $body .=  "table.hr {\n";
        $body .=  "  width: 100%; }\n";
        $body .=  "  table.hr th {\n";
        $body .=  "    height: 0;\n";
        $body .=  "    max-width: 580px;\n";
        $body .=  "    border-top: 0;\n";
        $body .=  "    border-right: 0;\n";
        $body .=  "    border-bottom: 1px solid #0a0a0a;\n";
        $body .=  "    border-left: 0;\n";
        $body .=  "    margin: 20px auto;\n";
        $body .=  "    Margin: 20px auto;\n";
        $body .=  "    clear: both; }\n";
        $body .=  "\n";
        $body .=  ".stat {\n";
        $body .=  "  font-size: 40px;\n";
        $body .=  "  line-height: 1; }\n";
        $body .=  "  p + .stat {\n";
        $body .=  "    margin-top: -16px;\n";
        $body .=  "    Margin-top: -16px; }\n";
        $body .=  "\n";
        $body .=  "span.preheader {\n";
        $body .=  "  display: none !important;\n";
        $body .=  "  visibility: hidden;\n";
        $body .=  "  mso-hide: all !important;\n";
        $body .=  "  font-size: 1px;\n";
        $body .=  "  color: #f3f3f3;\n";
        $body .=  "  line-height: 1px;\n";
        $body .=  "  max-height: 0px;\n";
        $body .=  "  max-width: 0px;\n";
        $body .=  "  opacity: 0;\n";
        $body .=  "  overflow: hidden; }\n";
        $body .=  "\n";
        $body .=  "table.button {\n";
        $body .=  "  width: auto;\n";
        $body .=  "  margin: 0 0 16px 0;\n";
        $body .=  "  Margin: 0 0 16px 0; }\n";
        $body .=  "  table.button table td {\n";
        $body .=  "    text-align: left;\n";
        $body .=  "    color: #fefefe;\n";
        $body .=  "    background: #2199e8;\n";
        $body .=  "    border: 2px solid #2199e8; }\n";
        $body .=  "    table.button table td a {\n";
        $body .=  "      font-family: Helvetica, Arial, sans-serif;\n";
        $body .=  "      font-size: 16px;\n";
        $body .=  "      font-weight: bold;\n";
        $body .=  "      color: #fefefe;\n";
        $body .=  "      text-decoration: none;\n";
        $body .=  "      display: inline-block;\n";
        $body .=  "      padding: 8px 16px 8px 16px;\n";
        $body .=  "      border: 0 solid #2199e8;\n";
        $body .=  "      border-radius: 3px; }\n";
        $body .=  "  table.button.radius table td {\n";
        $body .=  "    border-radius: 3px;\n";
        $body .=  "    border: none; }\n";
        $body .=  "  table.button.rounded table td {\n";
        $body .=  "    border-radius: 500px;\n";
        $body .=  "    border: none; }\n";
        $body .=  "\n";
        $body .=  "table.button:hover table tr td a,\n";
        $body .=  "table.button:active table tr td a,\n";
        $body .=  "table.button table tr td a:visited,\n";
        $body .=  "table.button.tiny:hover table tr td a,\n";
        $body .=  "table.button.tiny:active table tr td a,\n";
        $body .=  "table.button.tiny table tr td a:visited,\n";
        $body .=  "table.button.small:hover table tr td a,\n";
        $body .=  "table.button.small:active table tr td a,\n";
        $body .=  "table.button.small table tr td a:visited,\n";
        $body .=  "table.button.large:hover table tr td a,\n";
        $body .=  "table.button.large:active table tr td a,\n";
        $body .=  "table.button.large table tr td a:visited {\n";
        $body .=  "  color: #fefefe; }\n";
        $body .=  "\n";
        $body .=  "table.button.tiny table td,\n";
        $body .=  "table.button.tiny table a {\n";
        $body .=  "  padding: 4px 8px 4px 8px; }\n";
        $body .=  "\n";
        $body .=  "table.button.tiny table a {\n";
        $body .=  "  font-size: 10px;\n";
        $body .=  "  font-weight: normal; }\n";
        $body .=  "\n";
        $body .=  "table.button.small table td,\n";
        $body .=  "table.button.small table a {\n";
        $body .=  "  padding: 5px 10px 5px 10px;\n";
        $body .=  "  font-size: 12px; }\n";
        $body .=  "\n";
        $body .=  "table.button.large table a {\n";
        $body .=  "  padding: 10px 20px 10px 20px;\n";
        $body .=  "  font-size: 20px; }\n";
        $body .=  "\n";
        $body .=  "table.button.expand,\n";
        $body .=  "table.button.expanded {\n";
        $body .=  "  width: 100% !important; }\n";
        $body .=  "  table.button.expand table,\n";
        $body .=  "  table.button.expanded table {\n";
        $body .=  "    width: 100%; }\n";
        $body .=  "    table.button.expand table a,\n";
        $body .=  "    table.button.expanded table a {\n";
        $body .=  "      text-align: center;\n";
        $body .=  "      width: 100%;\n";
        $body .=  "      padding-left: 0;\n";
        $body .=  "      padding-right: 0; }\n";
        $body .=  "  table.button.expand center,\n";
        $body .=  "  table.button.expanded center {\n";
        $body .=  "    min-width: 0; }\n";
        $body .=  "\n";
        $body .=  "table.button:hover table td,\n";
        $body .=  "table.button:visited table td,\n";
        $body .=  "table.button:active table td {\n";
        $body .=  "  background: #147dc2;\n";
        $body .=  "  color: #fefefe; }\n";
        $body .=  "\n";
        $body .=  "table.button:hover table a,\n";
        $body .=  "table.button:visited table a,\n";
        $body .=  "table.button:active table a {\n";
        $body .=  "  border: 0 solid #147dc2; }\n";
        $body .=  "\n";
        $body .=  "table.button.secondary table td {\n";
        $body .=  "  background: #777777;\n";
        $body .=  "  color: #fefefe;\n";
        $body .=  "  border: 0px solid #777777; }\n";
        $body .=  "\n";
        $body .=  "table.button.secondary table a {\n";
        $body .=  "  color: #fefefe;\n";
        $body .=  "  border: 0 solid #777777; }\n";
        $body .=  "\n";
        $body .=  "table.button.secondary:hover table td {\n";
        $body .=  "  background: #919191;\n";
        $body .=  "  color: #fefefe; }\n";
        $body .=  "\n";
        $body .=  "table.button.secondary:hover table a {\n";
        $body .=  "  border: 0 solid #919191; }\n";
        $body .=  "\n";
        $body .=  "table.button.secondary:hover table td a {\n";
        $body .=  "  color: #fefefe; }\n";
        $body .=  "\n";
        $body .=  "table.button.secondary:active table td a {\n";
        $body .=  "  color: #fefefe; }\n";
        $body .=  "\n";
        $body .=  "table.button.secondary table td a:visited {\n";
        $body .=  "  color: #fefefe; }\n";
        $body .=  "\n";
        $body .=  "table.button.success table td {\n";
        $body .=  "  background: #3adb76;\n";
        $body .=  "  border: 0px solid #3adb76; }\n";
        $body .=  "\n";
        $body .=  "table.button.success table a {\n";
        $body .=  "  border: 0 solid #3adb76; }\n";
        $body .=  "\n";
        $body .=  "table.button.success:hover table td {\n";
        $body .=  "  background: #23bf5d; }\n";
        $body .=  "\n";
        $body .=  "table.button.success:hover table a {\n";
        $body .=  "  border: 0 solid #23bf5d; }\n";
        $body .=  "\n";
        $body .=  "table.button.alert table td {\n";
        $body .=  "  background: #ec5840;\n";
        $body .=  "  border: 0px solid #ec5840; }\n";
        $body .=  "\n";
        $body .=  "table.button.alert table a {\n";
        $body .=  "  border: 0 solid #ec5840; }\n";
        $body .=  "\n";
        $body .=  "table.button.alert:hover table td {\n";
        $body .=  "  background: #e23317; }\n";
        $body .=  "\n";
        $body .=  "table.button.alert:hover table a {\n";
        $body .=  "  border: 0 solid #e23317; }\n";
        $body .=  "\n";
        $body .=  "table.button.warning table td {\n";
        $body .=  "  background: #ffae00;\n";
        $body .=  "  border: 0px solid #ffae00; }\n";
        $body .=  "\n";
        $body .=  "table.button.warning table a {\n";
        $body .=  "  border: 0px solid #ffae00; }\n";
        $body .=  "\n";
        $body .=  "table.button.warning:hover table td {\n";
        $body .=  "  background: #cc8b00; }\n";
        $body .=  "\n";
        $body .=  "table.button.warning:hover table a {\n";
        $body .=  "  border: 0px solid #cc8b00; }\n";
        $body .=  "\n";
        $body .=  "table.callout {\n";
        $body .=  "  margin-bottom: 16px;\n";
        $body .=  "  Margin-bottom: 16px; }\n";
        $body .=  "\n";
        $body .=  "th.callout-inner {\n";
        $body .=  "  width: 100%;\n";
        $body .=  "  border: 1px solid #cbcbcb;\n";
        $body .=  "  padding: 10px;\n";
        $body .=  "  background: #fefefe; }\n";
        $body .=  "  th.callout-inner.primary {\n";
        $body .=  "    background: #def0fc;\n";
        $body .=  "    border: 1px solid #444444;\n";
        $body .=  "    color: #0a0a0a; }\n";
        $body .=  "  th.callout-inner.secondary {\n";
        $body .=  "    background: #ebebeb;\n";
        $body .=  "    border: 1px solid #444444;\n";
        $body .=  "    color: #0a0a0a; }\n";
        $body .=  "  th.callout-inner.success {\n";
        $body .=  "    background: #e1faea;\n";
        $body .=  "    border: 1px solid #1b9448;\n";
        $body .=  "    color: #fefefe; }\n";
        $body .=  "  th.callout-inner.warning {\n";
        $body .=  "    background: #fff3d9;\n";
        $body .=  "    border: 1px solid #996800;\n";
        $body .=  "    color: #fefefe; }\n";
        $body .=  "  th.callout-inner.alert {\n";
        $body .=  "    background: #fce6e2;\n";
        $body .=  "    border: 1px solid #b42912;\n";
        $body .=  "    color: #fefefe; }\n";
        $body .=  "\n";
        $body .=  ".thumbnail {\n";
        $body .=  "  border: solid 4px #fefefe;\n";
        $body .=  "  box-shadow: 0 0 0 1px rgba(10, 10, 10, 0.2);\n";
        $body .=  "  display: inline-block;\n";
        $body .=  "  line-height: 0;\n";
        $body .=  "  max-width: 100%;\n";
        $body .=  "  transition: box-shadow 200ms ease-out;\n";
        $body .=  "  border-radius: 3px;\n";
        $body .=  "  margin-bottom: 16px; }\n";
        $body .=  "  .thumbnail:hover, .thumbnail:focus {\n";
        $body .=  "    box-shadow: 0 0 6px 1px rgba(33, 153, 232, 0.5); }\n";
        $body .=  "\n";
        $body .=  "table.menu {\n";
        $body .=  "  width: 580px; }\n";
        $body .=  "  table.menu td.menu-item,\n";
        $body .=  "  table.menu th.menu-item {\n";
        $body .=  "    padding: 10px;\n";
        $body .=  "    padding-right: 10px; }\n";
        $body .=  "    table.menu td.menu-item a,\n";
        $body .=  "    table.menu th.menu-item a {\n";
        $body .=  "      color: #2199e8; }\n";
        $body .=  "\n";
        $body .=  "table.menu.vertical td.menu-item,\n";
        $body .=  "table.menu.vertical th.menu-item {\n";
        $body .=  "  padding: 10px;\n";
        $body .=  "  padding-right: 0;\n";
        $body .=  "  display: block; }\n";
        $body .=  "  table.menu.vertical td.menu-item a,\n";
        $body .=  "  table.menu.vertical th.menu-item a {\n";
        $body .=  "    width: 100%; }\n";
        $body .=  "\n";
        $body .=  "table.menu.vertical td.menu-item table.menu.vertical td.menu-item,\n";
        $body .=  "table.menu.vertical td.menu-item table.menu.vertical th.menu-item,\n";
        $body .=  "table.menu.vertical th.menu-item table.menu.vertical td.menu-item,\n";
        $body .=  "table.menu.vertical th.menu-item table.menu.vertical th.menu-item {\n";
        $body .=  "  padding-left: 10px; }\n";
        $body .=  "\n";
        $body .=  "table.menu.text-center a {\n";
        $body .=  "  text-align: center; }\n";
        $body .=  "\n";
        $body .=  ".menu[align=\"center\"] {\n";
        $body .=  "  width: auto !important; }\n";
        $body .=  "\n";
        $body .=  "body.outlook p {\n";
        $body .=  "  display: inline !important; }\n";
        $body .=  "\n";
        $body .=  "@media only screen and (max-width: 596px) {\n";
        $body .=  "  table.body img {\n";
        $body .=  "    width: auto;\n";
        $body .=  "    height: auto; }\n";
        $body .=  "  table.body center {\n";
        $body .=  "    min-width: 0 !important; }\n";
        $body .=  "  table.body .container {\n";
        $body .=  "    width: 95% !important; }\n";
        $body .=  "  table.body .columns,\n";
        $body .=  "  table.body .column {\n";
        $body .=  "    height: auto !important;\n";
        $body .=  "    -moz-box-sizing: border-box;\n";
        $body .=  "    -webkit-box-sizing: border-box;\n";
        $body .=  "    box-sizing: border-box;\n";
        $body .=  "    padding-left: 16px !important;\n";
        $body .=  "    padding-right: 16px !important; }\n";
        $body .=  "    table.body .columns .column,\n";
        $body .=  "    table.body .columns .columns,\n";
        $body .=  "    table.body .column .column,\n";
        $body .=  "    table.body .column .columns {\n";
        $body .=  "      padding-left: 0 !important;\n";
        $body .=  "      padding-right: 0 !important; }\n";
        $body .=  "  table.body .collapse .columns,\n";
        $body .=  "  table.body .collapse .column {\n";
        $body .=  "    padding-left: 0 !important;\n";
        $body .=  "    padding-right: 0 !important; }\n";
        $body .=  "  td.small-1,\n";
        $body .=  "  th.small-1 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 8.33333% !important; }\n";
        $body .=  "  td.small-2,\n";
        $body .=  "  th.small-2 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 16.66667% !important; }\n";
        $body .=  "  td.small-3,\n";
        $body .=  "  th.small-3 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 25% !important; }\n";
        $body .=  "  td.small-4,\n";
        $body .=  "  th.small-4 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 33.33333% !important; }\n";
        $body .=  "  td.small-5,\n";
        $body .=  "  th.small-5 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 41.66667% !important; }\n";
        $body .=  "  td.small-6,\n";
        $body .=  "  th.small-6 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 50% !important; }\n";
        $body .=  "  td.small-7,\n";
        $body .=  "  th.small-7 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 58.33333% !important; }\n";
        $body .=  "  td.small-8,\n";
        $body .=  "  th.small-8 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 66.66667% !important; }\n";
        $body .=  "  td.small-9,\n";
        $body .=  "  th.small-9 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 75% !important; }\n";
        $body .=  "  td.small-10,\n";
        $body .=  "  th.small-10 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 83.33333% !important; }\n";
        $body .=  "  td.small-11,\n";
        $body .=  "  th.small-11 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 91.66667% !important; }\n";
        $body .=  "  td.small-12,\n";
        $body .=  "  th.small-12 {\n";
        $body .=  "    display: inline-block !important;\n";
        $body .=  "    width: 100% !important; }\n";
        $body .=  "  .columns td.small-12,\n";
        $body .=  "  .column td.small-12,\n";
        $body .=  "  .columns th.small-12,\n";
        $body .=  "  .column th.small-12 {\n";
        $body .=  "    display: block !important;\n";
        $body .=  "    width: 100% !important; }\n";
        $body .=  "  table.body td.small-offset-1,\n";
        $body .=  "  table.body th.small-offset-1 {\n";
        $body .=  "    margin-left: 8.33333% !important;\n";
        $body .=  "    Margin-left: 8.33333% !important; }\n";
        $body .=  "  table.body td.small-offset-2,\n";
        $body .=  "  table.body th.small-offset-2 {\n";
        $body .=  "    margin-left: 16.66667% !important;\n";
        $body .=  "    Margin-left: 16.66667% !important; }\n";
        $body .=  "  table.body td.small-offset-3,\n";
        $body .=  "  table.body th.small-offset-3 {\n";
        $body .=  "    margin-left: 25% !important;\n";
        $body .=  "    Margin-left: 25% !important; }\n";
        $body .=  "  table.body td.small-offset-4,\n";
        $body .=  "  table.body th.small-offset-4 {\n";
        $body .=  "    margin-left: 33.33333% !important;\n";
        $body .=  "    Margin-left: 33.33333% !important; }\n";
        $body .=  "  table.body td.small-offset-5,\n";
        $body .=  "  table.body th.small-offset-5 {\n";
        $body .=  "    margin-left: 41.66667% !important;\n";
        $body .=  "    Margin-left: 41.66667% !important; }\n";
        $body .=  "  table.body td.small-offset-6,\n";
        $body .=  "  table.body th.small-offset-6 {\n";
        $body .=  "    margin-left: 50% !important;\n";
        $body .=  "    Margin-left: 50% !important; }\n";
        $body .=  "  table.body td.small-offset-7,\n";
        $body .=  "  table.body th.small-offset-7 {\n";
        $body .=  "    margin-left: 58.33333% !important;\n";
        $body .=  "    Margin-left: 58.33333% !important; }\n";
        $body .=  "  table.body td.small-offset-8,\n";
        $body .=  "  table.body th.small-offset-8 {\n";
        $body .=  "    margin-left: 66.66667% !important;\n";
        $body .=  "    Margin-left: 66.66667% !important; }\n";
        $body .=  "  table.body td.small-offset-9,\n";
        $body .=  "  table.body th.small-offset-9 {\n";
        $body .=  "    margin-left: 75% !important;\n";
        $body .=  "    Margin-left: 75% !important; }\n";
        $body .=  "  table.body td.small-offset-10,\n";
        $body .=  "  table.body th.small-offset-10 {\n";
        $body .=  "    margin-left: 83.33333% !important;\n";
        $body .=  "    Margin-left: 83.33333% !important; }\n";
        $body .=  "  table.body td.small-offset-11,\n";
        $body .=  "  table.body th.small-offset-11 {\n";
        $body .=  "    margin-left: 91.66667% !important;\n";
        $body .=  "    Margin-left: 91.66667% !important; }\n";
        $body .=  "  table.body table.columns td.expander,\n";
        $body .=  "  table.body table.columns th.expander {\n";
        $body .=  "    display: none !important; }\n";
        $body .=  "  table.body .right-text-pad,\n";
        $body .=  "  table.body .text-pad-right {\n";
        $body .=  "    padding-left: 10px !important; }\n";
        $body .=  "  table.body .left-text-pad,\n";
        $body .=  "  table.body .text-pad-left {\n";
        $body .=  "    padding-right: 10px !important; }\n";
        $body .=  "  table.menu {\n";
        $body .=  "    width: 100% !important; }\n";
        $body .=  "    table.menu td,\n";
        $body .=  "    table.menu th {\n";
        $body .=  "      width: auto !important;\n";
        $body .=  "      display: inline-block !important; }\n";
        $body .=  "    table.menu.vertical td,\n";
        $body .=  "    table.menu.vertical th, table.menu.small-vertical td,\n";
        $body .=  "    table.menu.small-vertical th {\n";
        $body .=  "      display: block !important; }\n";
        $body .=  "  table.menu[align=\"center\"] {\n";
        $body .=  "    width: auto !important; }\n";
        $body .=  "  table.button.small-expand,\n";
        $body .=  "  table.button.small-expanded {\n";
        $body .=  "    width: 100% !important; }\n";
        $body .=  "    table.button.small-expand table,\n";
        $body .=  "    table.button.small-expanded table {\n";
        $body .=  "      width: 100%; }\n";
        $body .=  "      table.button.small-expand table a,\n";
        $body .=  "      table.button.small-expanded table a {\n";
        $body .=  "        text-align: center !important;\n";
        $body .=  "        width: 100% !important;\n";
        $body .=  "        padding-left: 0 !important;\n";
        $body .=  "        padding-right: 0 !important; }\n";
        $body .=  "    table.button.small-expand center,\n";
        $body .=  "    table.button.small-expanded center {\n";
        $body .=  "      min-width: 0; } }\n";
        $body .=  "\n";
        $body .=  "    </style>\n";
        $body .=  "\n";
        $body .=  "    <style>\n";
        $body .=  "      body,\n";
        $body .=  "      html,\n";
        $body .=  "      .body {\n";
        $body .=  "        background: #f3f3f3 !important;\n";
        $body .=  "      }\n";
        $body .=  "\n";
        $body .=  "      .container.header {\n";
        $body .=  "        background: #f3f3f3;\n";
        $body .=  "      }\n";
        $body .=  "\n";
        $body .=  "      .body-border {\n";
        $body .=  "        border-top: 8px solid #ef0000;\n";
        $body .=  "      }\n";
        $body .=  "    </style>\n";
        $body .=  "  </head>\n";
        $body .=  "\n";
        $body .=  "  <body>\n";
        $body .=  "    <!-- <style> -->\n";
        $body .=  "    <table class=\"body\" data-made-with-foundation=\"\">\n";
        $body .=  "      <tr>\n";
        $body .=  "        <td class=\"float-center\" align=\"center\" valign=\"top\">\n";
        $body .=  "          <center data-parsed=\"\">\n";
        $body .=  "            <table class=\"spacer float-center\">\n";
        $body .=  "              <tbody>\n";
        $body .=  "                <tr>\n";
        $body .=  "                  <td height=\"16px\" style=\"font-size:16px;line-height:16px;\">&#xA0;</td>\n";
        $body .=  "                </tr>\n";
        $body .=  "              </tbody>\n";
        $body .=  "            </table>\n";
        $body .=  "            <table align=\"center\" class=\"container header float-center\">\n";
        $body .=  "              <tbody>\n";
        $body .=  "                <tr>\n";
        $body .=  "                  <td>\n";
        $body .=  "                    <table class=\"row\">\n";
        $body .=  "                      <tbody>\n";
        $body .=  "                        <tr>\n";
        $body .=  "                          <th class=\"small-12 large-12 columns first last\">\n";
        $body .=  "                            <table>\n";
        $body .=  "                              <tr>\n";
        $body .=  "                                <th>\n";
        $body .=  "                                  <h1 class=\"text-center\">ฟอร์มอีเมล์จาก เว็บไซต์ Condo Compare</h1>\n";
        $body .=  "                                </th>\n";
        $body .=  "                                <th class=\"expander\"></th>\n";
        $body .=  "                              </tr>\n";
        $body .=  "                            </table>\n";
        $body .=  "                          </th>\n";
        $body .=  "                        </tr>\n";
        $body .=  "                      </tbody>\n";
        $body .=  "                    </table>\n";
        $body .=  "                  </td>\n";
        $body .=  "                </tr>\n";
        $body .=  "              </tbody>\n";
        $body .=  "            </table>\n";
        $body .=  "            <table align=\"center\" class=\"container body-border float-center\">\n";
        $body .=  "              <tbody>\n";
        $body .=  "                <tr>\n";
        $body .=  "                  <td>\n";
        $body .=  "                    <table class=\"row\">\n";
        $body .=  "                      <tbody>\n";
        $body .=  "                        <tr>\n";
        $body .=  "                          <th class=\"small-12 large-12 columns first last\">\n";
        $body .=  "                            <table>\n";
        $body .=  "                              <tr>\n";
        $body .=  "                                <th>\n";
        $body .=  "                                  <table class=\"spacer\">\n";
        $body .=  "                                    <tbody>\n";
        $body .=  "                                      <tr>\n";
        $body .=  "                                        <td height=\"32px\" style=\"font-size:32px;line-height:32px;\">&#xA0;</td>\n";
        $body .=  "                                      </tr>\n";
        $body .=  "                                    </tbody>\n";
        $body .=  "                                  </table>\n";
        $body .=  "                                  <center data-parsed=\"\"> <img src=\"$site_logo\" align=\"center\" class=\"float-center\"> </center>\n";
        $body .=  "                                  <table class=\"spacer\">\n";
        $body .=  "                                    <tbody>\n";
        $body .=  "                                      <tr>\n";
        $body .=  "                                        <td height=\"16px\" style=\"font-size:16px;line-height:16px;\">&#xA0;</td>\n";
        $body .=  "                                      </tr>\n";
        $body .=  "                                    </tbody>\n";
        $body .=  "                                  </table>\n";

        $body .=  "                                  $message";

        $body .=  "                                  <center data-parsed=\"\">\n";
        $body .=  "                                    <table align=\"center\" class=\"menu float-center\">\n";
        $body .=  "                                      <tr>\n";
        $body .=  "                                        <td>\n";
        $body .=  "                                          <table>\n";
        $body .=  "                                            <tr>\n";
        $body .=  "                                              <th class=\"menu-item float-center\"><a href=\"http://condo-compare.com/\" target=\"_blank\">ไปที่เว็บไซต์</a></th>\n";
        $body .=  "                                            </tr>\n";
        $body .=  "                                          </table>\n";
        $body .=  "                                        </td>\n";
        $body .=  "                                      </tr>\n";
        $body .=  "                                    </table>\n";
        $body .=  "                                  </center>\n";
        $body .=  "                                </th>\n";
        $body .=  "                                <th class=\"expander\"></th>\n";
        $body .=  "                              </tr>\n";
        $body .=  "                            </table>\n";
        $body .=  "                          </th>\n";
        $body .=  "                        </tr>\n";
        $body .=  "                      </tbody>\n";
        $body .=  "                    </table>\n";
        $body .=  "                    <table class=\"spacer\">\n";
        $body .=  "                      <tbody>\n";
        $body .=  "                        <tr>\n";
        $body .=  "                          <td height=\"16px\" style=\"font-size:16px;line-height:16px;\">&#xA0;</td>\n";
        $body .=  "                        </tr>\n";
        $body .=  "                      </tbody>\n";
        $body .=  "                    </table>\n";
        $body .=  "                  </td>\n";
        $body .=  "                </tr>\n";
        $body .=  "              </tbody>\n";
        $body .=  "            </table>\n";
        $body .=  "          </center>\n";
        $body .=  "        </td>\n";
        $body .=  "      </tr>\n";
        $body .=  "    </table>\n";
        $body .=  "  </body>\n";
        $body .=  "\n";
        $body .=  "</html>\n";
        $body .=  "\n";
        // Also, for getting full html you may use the following internal method:
        //$body = $this->email->full_html($subject, $message);

//echo $body;
        $result = $this->email
            ->from('hob@condo-compare.com')
            ->reply_to('')    // Optional, an account where a human being reads.
            ->to($data['email'])
            ->subject($subject)
            ->message($body)
            ->send();

        // var_dump($result);
        // echo '<br />';
        // echo $this->email->print_debugger();
        //exit;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_upload()
    {
        $sfilepath = './uploads/condominium/.';
        $smaxsize = 4096000; //4MB
        $smaxwidth = 2000;
        $smaxheight = 2000;

        $pic_large = $this->primaryclass->do_upload('file', $sfilepath, $smaxsize, $smaxwidth, $smaxheight);

//		if (!empty($_FILES)) {
//			$tempFile = $_FILES['file']['tmp_name'];          //3
//			$targetPath = $sfilepath;  //4
//			$targetFile =  $targetPath. $_FILES['file']['name'];  //5
//			move_uploaded_file($tempFile,$targetFile); //6
//		}
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function canceled_upload()
    {
        //echo $this->primaryclass->pre_var_dump($_POST['file_name']);
        //$sfilepath = base_url().'uploads/condominium/';
        //$pic_thumb = $this->do_upload('file', $sfilepath);
        //echo $this->primaryclass->pre_var_dump($pic_thumb);
        //$file_name = json_decode($_POST['file_name']);
        //echo $sfilepath;
        $t = $_POST['file_name'];
        //echo $t;
        //unlink($sfilepath.$t);
        unlink('uploads/condominium/'.$t);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_condominium()
    {
        $sfilepath = './uploads/condominium/.';
        $smaxsize = 4096000; //4MB
        $smaxwidth_pic_thumb = 2000;
        $smaxheight_pic_thumb = 2000;

        $smaxwidth_pic_large = 2000;
        $smaxheight_pic_large = 2000;

        // PIC THUMB===========================================================
        if ($_FILES['pic_thumb']) {
            $pic_thumb = $this->primaryclass->do_upload('pic_thumb', $sfilepath, $smaxsize, $smaxwidth_pic_thumb, $smaxheight_pic_thumb);
            if ($pic_thumb['upload_data']['file_name'] == '') {
                $pic_thumb_have_file = $this->input->post('old_pic_thumb');
            } else {
                $pic_thumb_have_file = $pic_thumb['upload_data']['file_name'];
            }
        } else {
            $pic_thumb_have_file = '';
        }//IF PIC THUMB=========================================================

        // PIC LARGE=============================================================
        if ($_FILES['pic_large']) {
            $pic_large = $this->primaryclass->do_upload('pic_large', $sfilepath, $smaxsize, $smaxwidth_pic_large, $smaxheight_pic_large);
            if ($pic_large['upload_data']['file_name'] == '') {
                $pic_large_have_file = $this->input->post('old_pic_large');
            } else {
                $pic_large_have_file = $pic_large['upload_data']['file_name'];
            }
        } else {
            $pic_large_have_file = '';
        }//IF PIC LARGE==========================================================

        $createdate = $this->primaryclass->set_explode_date_time($this->input->post('createdate_date'), $this->input->post('createdate_time'));
        $modifydate = $this->primaryclass->set_explode_date_time($this->input->post('modifydate_date'), $this->input->post('modifydate_time'));
        $data = array(
            'file_name_' => $this->input->post('file_name_'),
            'condo_property_id' => $this->input->post('condo_property_id'),
            'member_id' => $this->input->post('member_id'),
            'staff_id' => $this->input->post('staff_id'),
            'condo_title' => $this->input->post('condo_title'),
            'condo_owner_name' => $this->input->post('condo_owner_name'),
            'condo_holder_name' => $this->input->post('condo_holder_name'),
            'condo_builing_finish' => $this->input->post('condo_builing_finish'),
            'property_type_id' => $this->input->post('property_type_id'),
            'type_zone_id' => $this->input->post('type_zone_id'),
            'zone_id' => $this->input->post('zone_id'),
            'type_offering_id' => $this->input->post('type_offering_id'),
            'type_suites_id' => $this->input->post('type_suites_id'),
            'type_decoration_id' => $this->input->post('type_decoration_id'),
            'condo_all_room' => $this->input->post('condo_all_room'),
            'condo_all_toilet' => $this->input->post('condo_all_toilet'),
            'condo_interior_room' => $this->input->post('condo_interior_room'),
            'condo_direction_room' => $this->input->post('condo_direction_room'),
            'condo_scenery_room' => $this->input->post('condo_scenery_room'),
            'condo_living_area' => $this->input->post('condo_living_area'),
            'condo_electricity' => $this->input->post('condo_electricity'),
            'condo_expert' => trim($this->input->post('condo_expert')),
            'condo_description' => trim($this->input->post('condo_description')),
            'pic_thumb' => $pic_thumb_have_file,
            'pic_large' => $pic_large_have_file,
            'condo_no' => $this->input->post('condo_no'),
            'condo_floor' => $this->input->post('condo_floor'),
            'condo_area_apartment' => $this->input->post('condo_area_apartment'),
            'condo_alley' => $this->input->post('condo_alley'),
            'condo_road' => $this->input->post('condo_road'),
            'condo_address' => trim($this->input->post('condo_address')),
            'provinces_id' => $this->input->post('provinces_id'),
            'districts_id' => $this->input->post('districts_id'),
            'sub_districts_id' => $this->input->post('sub_districts_id'),
            'condo_zipcode' => $this->input->post('condo_zipcode'),
            'condo_total_price' => $this->input->post('condo_total_price'),
            'condo_price_per_square_meter' => $this->input->post('condo_price_per_square_meter'),
            'condo_googlemaps' => trim($this->input->post('condo_googlemaps')),
            'latitude' => trim($this->input->post('latitude')),
            'longitude' => trim($this->input->post('longitude')),
            'condo_tag' => $this->input->post('condo_tag'),
            'createdate' => $createdate,
            'modifydate' => $modifydate,
            'type_status_id' => 0,
            'visited' => 0,
            'newsarrival' => $this->input->post('newsarrival'),
            'highlight' => $this->input->post('highlight'),
//			'publish'=>$this->input->post('publish'),
            'approved' => $this->input->post('approved'),
            'facilities_id' => $this->input->post('facilities_id'),
            'featured_id' => $this->input->post('featured_id'),
            'transportation_category_id' => $this->input->post('transportation_category_id'),
            'transportation_distance' => $this->input->post('transportation_distance'),
            'transportation_id' => $this->input->post('transportation_id'),
        );

//		$this->primaryclass->pre_var_dump($this->input->post('file_name_'));
        $insert = $this->Condominium_model->add($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit($id)
    {
        $row = $this->Condominium_model->get_info($id);

        $provinces = $this->M->get_provinces();
        //$districts 		= $this->Member->get_districts();
        //$subdistricts = $this->Member->get_subdistricts();
        $staff_list = $this->User_model->get_staff_option();
        $type_zone = $this->Configsystem->get_type_zone_option();
        $type_offering = $this->Configsystem->get_type_offering_option();
        $type_status = $this->Configsystem->get_type_status_option();
        $type_suites = $this->Configsystem->get_type_suites_option();
        $type_decoration = $this->Configsystem->get_type_decoration_option();
        $facilities = $this->Condominium_model->get_facilities_option();
        $relation_facilities = $this->Condominium_model->get_facilities_option_by_condo($id);
        $featured = $this->Condominium_model->get_featured_option();
        $relation_featured = $this->Condominium_model->get_featured_row_by_condo($id);

        $condo_relation_transportation = $this->Condominium_model->get_transportation_relation_row_by_condo($id);
        $transportation_category = $this->Condominium_model->get_transportation_category_option();
        $transportation = $this->Condominium_model->get_transportation_option();
        $member = $this->Member->get_fullname();
//		$this->output->enable_profiler(TRUE);
        $data['row'] = $row;
        ($this->input->post()) ? $this->Condominium_model->edit($data) : $this->load->view('backoffice/condominium/formadmin', array(
            'title' => 'แก้ไขข้อมูลคอนโด',
            'googlemap' => 'yes',
            'header' => 'yes',
            'url_href' => 'edit',
            'css' => array(
                'assets/plugins/fileuploads/css/dropify.min.css',
                'assets/plugins/timepicker/bootstrap-timepicker.min.css',
                'assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
            ),
            'js' => array(
                'assets/plugins/fileuploads/js/dropify.min.js',
                'assets/plugins/moment/moment.js',
                'assets/plugins/timepicker/bootstrap-timepicker.min.js',
                'assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
            ),
            'wysiwigeditor' => 'wysiwigeditor',
            'data' => $data,
            'provinces' => $provinces,
            'type_zone' => $type_zone,
            'type_offering' => $type_offering,
            'type_status' => $type_status,
            'type_suites' => $type_suites,
            'type_decoration' => $type_decoration,
            'facilities' => $facilities,
            'relation_facilities' => $relation_facilities,
            'featured' => $featured,
            'relation_featured' => $relation_featured,
            'condo_relation_transportation' => $condo_relation_transportation,
            'transportation_category' => $transportation_category,
            'transportation' => $transportation,
            'member' => $member,
            'staff_list' => $staff_list,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_condominium()
    {
        //		/* Upload Image */
        $sfilepath = './uploads/condominium/.';
        $smaxsize = 4096000; //4MB
        $smaxwidth_pic_thumb = 2000;
        $smaxheight_pic_thumb = 2000;

        $smaxwidth_pic_large = 2000;
        $smaxheight_pic_large = 2000;

        // PIC THUMB===========================================================
        if ($_FILES['pic_thumb']) {
            //			$pic_thumb = $this->primaryclass->do_upload( 'pic_thumb' , $sfilepath, $smaxsize, $smaxwidth_pic_thumb, $smaxheight_pic_thumb );
            $pic_thumb = $this->primaryclass->do_upload('pic_thumb', $sfilepath, $smaxsize, $smaxwidth_pic_thumb, $smaxheight_pic_thumb);
            // $this->primaryclass->pre_var_dump($pic_thumb);

            if ($pic_thumb['upload_data']['file_name'] == '') {
                $pic_thumb_have_file = $this->input->post('old_pic_thumb');
            } else {
                $pic_thumb_have_file = $pic_thumb['upload_data']['file_name'];
            }
        } else {
            $pic_thumb_have_file = '';
        }//IF PIC THUMB=========================================================

        // PIC LARGE=============================================================
        if ($_FILES['pic_large']) {
            $pic_large = $this->primaryclass->do_upload('pic_large', $sfilepath, $smaxsize, $smaxwidth_pic_large, $smaxheight_pic_large);
            if ($pic_large['upload_data']['file_name'] == '') {
                $pic_large_have_file = $this->input->post('old_pic_large');
            } else {
                $pic_large_have_file = $pic_large['upload_data']['file_name'];
            }
        } else {
            $pic_large_have_file = '';
        }//IF PIC LARGE==========================================================

//		$this->primaryclass->pre_var_dump( $_FILES['pic_large'] );

        $createdate = $this->primaryclass->set_explode_date_time($this->input->post('createdate_date'), $this->input->post('createdate_time'));
        $modifydate = $this->primaryclass->set_explode_date_time($this->input->post('modifydate_date'), $this->input->post('modifydate_time'));

        $data = array(
            'condo_id' => $this->input->post('condo_id'),
            'file_name_' => $this->input->post('file_name_'),
            'condo_property_id' => $this->input->post('condo_property_id'),
            'member_id' => $this->input->post('member_id'),
            'staff_id' => $this->input->post('staff_id'),
            'condo_title' => $this->input->post('condo_title'),
            'condo_owner_name' => $this->input->post('condo_owner_name'),
            'condo_holder_name' => $this->input->post('condo_holder_name'),
            'condo_builing_finish' => $this->input->post('condo_builing_finish'),
            'property_type_id' => $this->input->post('property_type_id'),
            'type_zone_id' => $this->input->post('type_zone_id'),
            'zone_id' => $this->input->post('zone_id'),
            'type_offering_id' => $this->input->post('type_offering_id'),
            'type_suites_id' => $this->input->post('type_suites_id'),
            'type_decoration_id' => $this->input->post('type_decoration_id'),
            'condo_all_room' => $this->input->post('condo_all_room'),
            'condo_all_toilet' => $this->input->post('condo_all_toilet'),
            'condo_interior_room' => $this->input->post('condo_interior_room'),
            'condo_direction_room' => $this->input->post('condo_direction_room'),
            'condo_scenery_room' => $this->input->post('condo_scenery_room'),
            'condo_living_area' => $this->input->post('condo_living_area'),
            'condo_electricity' => $this->input->post('condo_electricity'),
            'condo_expert' => trim($this->input->post('condo_expert')),
            'condo_description' => trim($this->input->post('condo_description')),
            'pic_thumb' => $pic_thumb_have_file,
            'pic_large' => $pic_large_have_file,
            'condo_no' => $this->input->post('condo_no'),
            'condo_floor' => $this->input->post('condo_floor'),
            'condo_area_apartment' => $this->input->post('condo_area_apartment'),
            'condo_alley' => $this->input->post('condo_alley'),
            'condo_road' => $this->input->post('condo_road'),
            'condo_address' => trim($this->input->post('condo_address')),
            'provinces_id' => $this->input->post('provinces_id'),
            'districts_id' => $this->input->post('districts_id'),
            'sub_districts_id' => $this->input->post('sub_districts_id'),
            'condo_zipcode' => $this->input->post('condo_zipcode'),
            'condo_total_price' => $this->input->post('condo_total_price'),
            'condo_price_per_square_meter' => $this->input->post('condo_price_per_square_meter'),
            'condo_googlemaps' => trim($this->input->post('condo_googlemaps')),
            'latitude' => trim($this->input->post('latitude')),
            'longitude' => trim($this->input->post('longitude')),
            'condo_tag' => $this->input->post('condo_tag'),
            'createdate' => $createdate,
            'modifydate' => $modifydate,
            'type_status_id' => $this->input->post('type_status_id'),
            'newsarrival' => $this->input->post('newsarrival'),
            'highlight' => $this->input->post('highlight'),
            'approved' => $this->input->post('approved'),
            'facilities_id' => $this->input->post('facilities_id'),
            'featured_id' => $this->input->post('featured_id'),
            'transportation_category_id' => $this->input->post('transportation_category_id'),
            'transportation_distance' => $this->input->post('transportation_distance'),
            'transportation_id' => $this->input->post('transportation_id'),
        );
        //echo $this->primaryclass->pre_var_dump($data);
        $update = $this->Condominium_model->edit($data);
        //redirect('condominium/view');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function remove_image_update()
    {
        $data = $this->security->xss_clean($this->input->post());
        $data = array(
            'condo_id' => $this->input->post('condo_id'),
            $this->input->post('field_name') => null,
        );
        $sfilepath = 'uploads/condominium/';
        $imagefile = $sfilepath.$this->input->post('imagefile');
        $this->M->update_record_dalete_image($data, $imagefile, 'condo');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public function delete_condo($id)
    {
        $delete = $this->Condominium_model->delete_record('condo_id', $id, 'condo');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function delete_image($id)
    {
        $delete = $this->Condominium_model->delete_record('condo_images_id', $id, 'condo_images');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function approved_condo($id = 0)
    {
        $data = $this->security->xss_clean($this->input->post());
        $data = array(
            'condo_id' => $this->input->post('condo_id'),
            'approved' => $this->input->post('approved'),
            'modifydate' => date('Y-m-d H:i:s'),
        );

        $approved = $this->Condominium_model->set_condo($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function do_upload($sfilename, $sfilepath)
    {
        $config['upload_path'] = $sfilepath;
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['max_size'] = 2048000;
        $config['max_width'] = 2400;
        $config['max_height'] = 1400;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload($sfilename)) {
            return $data = array('upload_data' => $this->upload->data());
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* Facilities */
    public function view_facilities()
    {
        $data = $this->Condominium_model->get_facilities();

        $this->load->view('backoffice/condominium/datatable-facilities', array(
            'title' => 'ตารางข้อมูลสิ่งอำนวยความสะดวก',
            'header' => 'yes',
            'url_href' => 'add_facilities',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_facilities()
    {
        $this->load->view('backoffice/condominium/formadmin-facilities', array(
            'title' => 'เพิ่มข้อมูลสิ่งอำนวยความสะดวก',
            'header' => 'yes',

        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_data_facilities()
    {
        $data = array(
            'facilities_title' => $this->input->post('facilities_title'),
            'facilities_title_en' => $this->input->post('facilities_title_en'),
            'createdate' => date('Y-m-d H:i:s'),
        );

        //var_dump($data);
        $insert = $this->Condominium_model->set_facilities($data);
        $this->session->set_flashdata('message', 'Your data inserted Successfully..');
        redirect('condominium/view_facilities');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_facilities($id)
    {
        $row = $this->Condominium_model->get_facilities_record($id);

        $data['row'] = $row;
        //var_dump($row);
        ($this->input->post()) ? $this->Condominium_model->edit($data) : $this->load->view('backoffice/condominium/formadmin-facilities', array(
            'title' => 'แก้ไขข้อมูลสิ่งอำนวยความสะดวก',
            'header' => 'yes',
            'url_href' => 'edit',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_data_facilities()
    {
        $facilities_id = $this->input->post('facilities_id');

        $data = array(
            'facilities_id' => $this->input->post('facilities_id'),
            'facilities_title' => $this->input->post('facilities_title'),
            'facilities_title_en' => $this->input->post('facilities_title_en'),
            'updatedate' => date('Y-m-d H:i:s'),
        );
        //echo $this->Primaryclass->pre_var_dump($data);

        $update = $this->Condominium_model->update_facilities($data);
        $this->session->set_flashdata('message', 'Your data Updated Successfully..');
        redirect('condominium/view_facilities');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function delete_facilities($id)
    {
        $delete = $this->Condominium_model->delete_record('facilities_id', $id, 'facilities');

        $this->session->set_flashdata('message', 'Your data Delete Successfully..');
        redirect('condominium/view_facilities');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* Feathured */
    public function view_featured()
    {
        $data = $this->Condominium_model->get_featured();

        $this->load->view('backoffice/condominium/datatable-featured', array(
            'title' => 'ตารางข้อมูลจุดเด่น',
            'header' => 'yes',
            'url_href' => 'add_featured',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_featured()
    {
        $this->load->view('backoffice/condominium/formadmin-featured', array(
            'title' => 'เพิ่มข้อมูลจุดเด่น',
            'header' => 'yes',
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_data_featured()
    {
        $data = array(
            'featured_title' => $this->input->post('featured_title'),
            'featured_title_en' => $this->input->post('featured_title_en'),
            'createdate' => date('Y-m-d H:i:s'),
        );

        //var_dump($data);
        $insert = $this->Condominium_model->set_featured($data);
        $this->session->set_flashdata('message', 'Your data inserted Successfully..');
        redirect('condominium/view_featured');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_featured($id)
    {
        $row = $this->Condominium_model->get_featured_record($id);

        $data['row'] = $row;
        //var_dump($row);
        ($this->input->post()) ? $this->Condominium_model->edit($data) : $this->load->view('backoffice/condominium/formadmin-featured', array(
            'title' => 'แก้ไขรายการจุดเด่น',
            'header' => 'yes',
            'url_href' => 'edit',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_data_featured()
    {
        $featured_id = $this->input->post('featured_id');

        $data = array(
            'featured_id' => $this->input->post('featured_id'),
            'featured_title' => $this->input->post('featured_title'),
            'featured_title_en' => $this->input->post('featured_title_en'),
            'updatedate' => date('Y-m-d H:i:s'),
        );
        //echo $this->Primaryclass->pre_var_dump($data);

        $update = $this->Condominium_model->update_featured($data);
        $this->session->set_flashdata('message', 'Your data Updated Successfully..');
        redirect('condominium/view_featured');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function delete_featured($id)
    {
        $delete = $this->Condominium_model->delete_record('featured_id', $id, 'featured');

        $this->session->set_flashdata('message', 'Your data Delete Successfully..');
        redirect('condominium/view_featured');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* Transportation Category */
    public function view_transportation_category()
    {
        $data = $this->Condominium_model->get_transportation_category();
        //
        $this->load->view('backoffice/condominium/datatable-transportation-category', array(
            'title' => 'ตารางข้อมูลหมวดหมู่สถานที่ใกล้เคียง',
            'header' => 'yes',
            'url_href' => 'add_transportation_category',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_transportation_category()
    {
        $this->load->view('backoffice/condominium/formadmin-transportation-category', array(
            'title' => 'เพิ่มข้อมูลหมวดหมู่สถานที่ใกล้เคียง',
            'header' => 'yes',
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_data_transportation_category()
    {
        $data = array(
            'transportation_category_title' => $this->input->post('transportation_category_title'),
            'createdate' => date('Y-m-d H:i:s'),
            'publish' => $this->input->post('publish'),
        );

        //var_dump($data);
        $insert = $this->Condominium_model->set_transportation_category($data);
        $this->session->set_flashdata('message', 'Your data inserted Successfully..');
        redirect('condominium/view_transportation_category');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_transportation_category($id)
    {
        $row = $this->Condominium_model->get_transportation_category_record($id);

        $data['row'] = $row;
        //var_dump($row);
        ($this->input->post()) ? $this->Condominium_model->edit($data) : $this->load->view('backoffice/condominium/formadmin-transportation-category', array(
            'title' => 'แก้ไขรายการหมวดหมู่สถานที่ใกล้เคียง',
            'header' => 'yes',
            'url_href' => 'edit',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_data_transportation_category()
    {
        $transportation_id = $this->input->post('transportation_category_id');

        $data = array(
            'transportation_category_id' => $this->input->post('transportation_category_id'),
            'transportation_category_title' => $this->input->post('transportation_category_title'),
            'updatedate' => date('Y-m-d H:i:s'),
            'publish' => $this->input->post('publish'),
        );
        //echo $this->Primaryclass->pre_var_dump($data);

        $update = $this->Condominium_model->update_transportation_category($data);
        $this->session->set_flashdata('message', 'Your data Updated Successfully..');
        redirect('condominium/view_transportation_category');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function delete_transportation_category($id)
    {
        $delete = $this->Condominium_model->delete_category_record('transportation_category_id', $id, 'transportation_category');

        $this->session->set_flashdata('message', 'Your data Delete Successfully..');
        redirect('condominium/view_transportation_category');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //call to fill the second dropdown with the provinces
    public function transportation_category_relation()
    {
        //set selected country id from POST
        $transportation_category_id = $this->input->post('id', true);

        $data['transportation'] = $this->Condominium_model->get_transportation_option($transportation_category_id);

        //echo count($data['transportation']);
        $output = null;
        foreach ($data['transportation'] as $row) {
            if (count($data['transportation']) == 0) {
                //here we build a dropdown item line for each query result
             $output .= '<option value>---เลือก----</option>';
            } else {
                //here we build a dropdown item line for each query result
             $output .= "<option value='".$row->transportation_id."'>".$row->transportation_title.'</option>';
            }
        }
        echo $output;

        //var_dump($districts);
        //run the query for the cities we specified earlier
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //call to fill the second dropdown with the provinces
    public function transportation_category_option()
    {
        //set selected country id from POST
        $transportation_category_id = $this->input->post('id', true);

        $data['transportation_category'] = $this->Condominium_model->get_transportation_category_option();

        //echo count($data['transportation']);
        $output = null;

        $output .= '<div class="form-group">';
        $output .= '<label class="col-md-2 control-label m-t-10">สถานที่ใกล้เคียง</label>';
        $output .= '<div class="col-sm-12 col-md-4">';
        $output .= '<select id="transportation_category_id" class="form-control" name="transportation_category_id[]" onchange="(this)">';
        foreach ($data['transportation_category'] as $row) {
            if (count($data['transportation_category']) <= 0) {
                //here we build a dropdown item line for each query result
                $output .= '<optgroup>';
                $output .= '<option value>---เลือก----</option>';
                $output .= '</optgroup>';
            } else {
                $output .= "<optgroup label='".$row->transportation_category_title."'>";

                //เอาข้อมูล Transportation by ID มา
                $data['transportation'] = $this->Condominium_model->get_transportation_option($row->transportation_category_id);
                foreach ($data['transportation'] as $row_in) {
                    //here we build a dropdown item line for each query result
                    $output .= "<option value='".$row_in->transportation_id."'>".$row_in->transportation_title.'</option>';
                }
                $output .= '</optgroup>';
            }
        }
        $output .= '</select>';
        $output .= '</div>';
        $output .= '<div class="col-sm-10 col-md-4">';
        $output .= '<input type="text" name="transportation_distance[]" class="inputmaxlength form-control" maxlength="50" placeholder="">';
        $output .= '<span class="font-13 text-muted">ตัวอย่าง : 7 นาที &#40;1.46 กิโลเมตร&#41;</span>';
        $output .= '</div>';
        $output .= '<div class="col-sm-2 col-md-2">';
        $output .= '<div class="field_wrapper">';
        $output .= '<div>';
        $output .= '<a href="javascript:void(0);" class="remove_button"><i class="remove_button fa fa-minus"></i></a>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
        echo $output;
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* Transportation */
    public function view_transportation()
    {
        $data = $this->Condominium_model->get_transportation();
        //
        $this->load->view('backoffice/condominium/datatable-transportation', array(
            'title' => 'ตารางข้อมูลสถานที่ใกล้เคียง',
            'header' => 'yes',
            'url_href' => 'add_transportation',

            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_transportation()
    {
        $transportation_category = $this->Condominium_model->get_transportation_category_option();
        $this->load->view('backoffice/condominium/formadmin-transportation', array(
            'title' => 'เพิ่มข้อมูลสถานที่ใกล้เคียง',
            'header' => 'yes',
            'transportation_category' => $transportation_category,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_data_transportation()
    {
        $data = array(
            'transportation_title' => $this->input->post('transportation_title'),
            'transportation_category_id' => $this->input->post('transportation_category_id'),
            'createdate' => date('Y-m-d H:i:s'),
            'publish' => $this->input->post('publish'),
        );

        //var_dump($data);
        $insert = $this->Condominium_model->set_transportation($data);
        $this->session->set_flashdata('message', 'Your data inserted Successfully..');
        redirect('condominium/view_transportation');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_transportation($id)
    {
        $row = $this->Condominium_model->get_transportation_record($id);
        $transportation_category = $this->Condominium_model->get_transportation_category_option();
        $data['row'] = $row;
        //var_dump($row);
        ($this->input->post()) ? $this->Condominium_model->edit($data) : $this->load->view('backoffice/condominium/formadmin-transportation', array(
            'title' => 'แก้ไขรายการสถานที่ใกล้เคียง',
            'header' => 'yes',
            'url_href' => 'edit',
            'data' => $data,
            'transportation_category' => $transportation_category,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_data_transportation()
    {
        $transportation_id = $this->input->post('transportation_id');

        $data = array(
            'transportation_id' => $this->input->post('transportation_id'),
            'transportation_title' => $this->input->post('transportation_title'),
            'transportation_category_id' => $this->input->post('transportation_category_id'),
            'updatedate' => date('Y-m-d H:i:s'),
            'publish' => $this->input->post('publish'),
        );
        //echo $this->Primaryclass->pre_var_dump($data);
        $update = $this->Condominium_model->update_transportation($data);
        $this->session->set_flashdata('message', 'Your data Updated Successfully..');
        redirect('condominium/view_transportation');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function delete_transportation($id)
    {
        $delete = $this->Condominium_model->delete_record('transportation_id', $id, 'transportation');

        $this->session->set_flashdata('message', 'Your data Delete Successfully..');
        redirect('condominium/view_transportation');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function delete_transportation_condominium($data = null)
    {
        //$data=$this->security->xss_clean($this->input->post());
        $delete = $this->M->delete_record('condo_relation_transportation_id', $this->input->post('id'), 'condo_relation_transportation');
        // $this->primaryclass->pre_var_dump($delete);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* Mass Transportation Category */
    public function view_mass_transportation_category()
    {
        $data = $this->Condominium_model->get_mass_transportation_category();
        //
        $this->load->view('backoffice/condominium/datatable-mass-transportation-category', array(
            'title' => 'ตารางข้อมูลหมวดหมู่ขนส่งมวลชน',
            'header' => 'yes',
            'url_href' => 'add_mass_transportation_category',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_mass_transportation_category()
    {
        if ($this->input->post('action')) {
            $data = $this->security->xss_clean($this->input->post());
            $data = array(
                'mass_transportation_category_title' => $data['mass_transportation_category_title'],
                'createdate' => date('Y-m-d H:i:s'),
                'publish' => $data['publish'],
            );
            $insert = $this->Condominium_model->set_mass_transportation_category($data);
        } else {
            $this->load->view('backoffice/condominium/formadmin-mass-transportation-category', array(
                'title' => 'เพิ่มข้อมูลหมวดหมู่ขนส่งมวลชน',
                'header' => 'yes',
            ));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_mass_transportation_category($id = 0)
    {
        if ($this->input->post('action')) {
            $data = $this->security->xss_clean($this->input->post());
            $data = array(
                'mass_transportation_category_id' => $data['mass_transportation_category_id'],
                'mass_transportation_category_title' => $data['mass_transportation_category_title'],
                'updatedate' => date('Y-m-d H:i:s'),
                'publish' => $data['publish'],
            );
            $insert = $this->Condominium_model->update_mass_transportation_category($data);
        } else {
            $data['row'] = $this->Condominium_model->get_mass_transportation_category_by_id($id);
            $this->load->view('backoffice/condominium/formadmin-mass-transportation-category', array(
                'title' => 'แก้ไขข้อมูลหมวดหมู่ขนส่งมวลชน',
                'header' => 'yes',
                'data' => $data,
            ));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function mass_transportation_category_publish()
    {
        $data = $this->security->xss_clean($this->input->post());
        $data = array(
            'mass_transportation_category_id' => $data['mass_transportation_category_id'],
            'publish' => $data['publish'],
            'updatedate' => date('Y-m-d H:i:s'),
        );

        $approved = $this->Condominium_model->update_mass_transportation_category($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function delete_mass_transportation_category($id)
    {
        $delete = $this->Condominium_model->delete_record('mass_transportation_category_id', $id, 'mass_transportation_category');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /* Mass Transportation */
    public function view_mass_transportation()
    {
        $data = $this->Condominium_model->get_mass_transportation();
        //
        $this->load->view('backoffice/condominium/datatable-mass-transportation', array(
            'title' => 'ตารางข้อมูลขนส่งมวลชน',
            'header' => 'yes',
            'url_href' => 'add_mass_transportation',
            'data' => $data,
        ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function add_mass_transportation()
    {
        if ($this->input->post('action')) {
            $data = $this->security->xss_clean($this->input->post());
            $data = array(
                'mass_transportation_title' => $data['mass_transportation_title'],
                'mass_transportation_category_id' => $data['mass_transportation_category_id'],
                'createdate' => date('Y-m-d H:i:s'),
                'publish' => $data['publish'],
            );
            $insert = $this->Condominium_model->set_mass_transportation($data);
        } else {
            $data['mass_transportation_category'] = $this->Condominium_model->get_mass_transportation_category_option();
            $this->load->view('backoffice/condominium/formadmin-mass-transportation', array(
                'title' => 'เพิ่มข้อมูลขนส่งมวลชน',
                'header' => 'yes',
                'data' => $data,
            ));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function edit_mass_transportation($id = 0)
    {
        if ($this->input->post('action')) {
            $data = $this->security->xss_clean($this->input->post());
            $data = array(
                'mass_transportation_id' => $data['mass_transportation_id'],
                'mass_transportation_category_id' => $data['mass_transportation_category_id'],
                'mass_transportation_title' => $data['mass_transportation_title'],
                'updatedate' => date('Y-m-d H:i:s'),
                'publish' => $data['publish'],
            );
            $insert = $this->Condominium_model->update_mass_transportation($data);
        } else {
            $data['row'] = $this->Condominium_model->get_mass_transportation_by_id($id);
            $data['mass_transportation_category'] = $this->Condominium_model->get_mass_transportation_category_option();
            $this->load->view('backoffice/condominium/formadmin-mass-transportation', array(
                'title' => 'แก้ไขข้อมูลหมวดหมู่ขนส่งมวลชน',
                'header' => 'yes',
                'data' => $data,
            ));
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function mass_transportation_publish()
    {
        $data = $this->security->xss_clean($this->input->post());
        $data = array(
            'mass_transportation_id' => $data['mass_transportation_id'],
            'publish' => $data['publish'],
            'updatedate' => date('Y-m-d H:i:s'),
        );

        $approved = $this->Condominium_model->update_mass_transportation($data);
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function delete_mass_transportation($id)
    {
        $delete = $this->Condominium_model->delete_record('mass_transportation_id', $id, 'mass_transportation');
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public function view_highlight()
    {
        $data = $this->Condominium_model->get_condo_highlight();

    // var_dump($data['data']);
    $this->load->view('backoffice/condominium/datatable', array(
      'title' => 'รายการคอนโดแนะนำ',
      'header' => 'yes',
      'url_href' => 'add',
      'data' => $data,
    ));
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
}
