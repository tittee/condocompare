<?php
defined('BASEPATH') OR exit();
class Staff_model extends CI_Model {
	//....................................................................................................................................

	function __construct()
	{
		parent::__construct();
	}
	//....................................................................................................................................

  public function get_staff_by_id()
	{
		$staff_id=$this->session->userdata('zID');
		$staff=$this->db->select('avatar');
		$staff=$this->db->where('staff_id',$staff_id);
		$staff=$this->db->where('approved',2);
		$staff=$this->db->get('staff');
		// echo $this->db->last_query();

		if($staff->num_rows() > 0){
			return $row=$staff->row(0);
		}
  }
  //.................................................................................................

	public function info($r=0)
	{
		$staff_id=$this->session->userdata('zID');
		$staff_id=($staff_id)?$staff_id:0;

		if($r)
		{
			//echo 'STEP 1';
			return ($staff_id)?$this->login_module($staff_id):0;
		}
		elseif($staff_id)
		{
			$staff=$this->db
				->select('
					staff_id,
					username,
					email,
					approved,
					fullname,
					staff_group_id,
					lastlogin,
					')
				->where('staff_id',$staff_id)
				->where('approved',2)
				->get('staff')
				->first_row('array');
				//$staff['avatar']=$this->M->avatar($staff['avatar']);

			$this->input->set_cookie(array('name'=>'zID','value'=>$staff_id,'expire'=>(time()+60*60*24*30*365)));
			$this->session->set_userdata('zID',$staff_id);
			$this->session->set_userdata('zUSER',$staff['username']);
			$this->session->set_userdata('zGROUPID',$staff['staff_group_id']);
			//echo 'STEP 2';
			return $staff;
		}
		elseif($this->uri->uri_string()!='staff/check_login'){
//			echo 'STEP 3';
			//echo $this->uri->uri_string();
			redirect('backoffice/login');
		}
		else
		{
			//echo 'STEP 4';
			return array('id'=>0);
		}
	}
	//....................................................................................................................................

	public function login(){

		$data=$this->security->xss_clean($this->input->post());
		$return=array('success'=>false,'message'=>'');

		$staff=$this->db
			->select('staff_id, email, staff_group_id, password')
			->where('email',$data['email'])
			->where('password',md5($data['password']))
			->get('staff');

//				echo $this->db->queries[0];
//print_r($staff);
//$this->output->enable_profiler(TRUE);

		if($staff->num_rows()>0){
			//return true;
//			echo 'ssss';

			$staff=$staff->row_array();
//var_dump($staff);
			$this->session->set_userdata('zID',$staff['staff_id']);
			$this->session->set_userdata('zUSER',$staff['email']);
			$this->input->set_cookie(array('name'=>'zID','value'=>$staff['staff_id'],'expire'=>(time()+60*60*24*30*365)));
			$this->input->set_cookie(array('name'=>'zUSER','value'=>$staff['email'],'expire'=>(time()+60*60*24*30*365)));
			$this->input->set_cookie(array('name'=>'zGROUPID','value'=>$staff['staff_group_id'],'expire'=>(time()+60*60*24*30*365)));

			//$return=array('success'=>true,'module'=>$this->login_module($staff['staff_id']));
//			echo 'Success Email or Password';
			return true;
		}else{

//			echo 'Invalid Email or Password';
			return false;
			//return $return=array('success'=>false);
		}

	}
	//....................................................................................................................................

	private function login_module($id){
		return $this->db
			->select('*')
			->where('staff_id',$id)
			->from('staff')
			->get()
			->row(0);
	}
	//....................................................................................................................................

	public function logout($redirect=true){
		$this->session->unset_userdata('zID');
		$this->session->unset_userdata('zUSER');
		$this->session->unset_userdata('zGROUPID');
		$this->session->sess_destroy();
		@session_destroy();
		delete_cookie("zID");
		delete_cookie("zUSER");
		delete_cookie("zGROUPID");
		//$this->M->LOG('Sign Out');
		if($redirect)
		{
			redirect('backoffice/login');
		}
	}
	//....................................................................................................................................

	public function group_staff($id = 0)
	{
    if( $id !== 0 )
    {
      $query = $this->db
       ->select('
         type_staff_title,
       ')
       ->where('type_staff_id', $id)
       ->get('type_staff');
       if( $query->num_rows() > 0 )
       {
         $result = $query->row();
       }
    }
    else
    {
      $query = $this->db
       ->select('
         type_staff_title,
       ')
       ->where('type_staff_id', 1)
       ->get('type_staff');
       if( $query->num_rows() > 0 )
       {
         $result = $query->row();
       }
    }

   return $result;
	}
	//....................................................................................................................................

	public function update_lastlogin($email)
	{
		$update = $this->db
			->set('lastlogin', date("Y-m-d H:i:s"))
			->where('email', $email)
			->update('staff');
		return true;
	}
	//....................................................................................................................................

	public function viewdata()
	{
		$query = $this->db
			->select('*')
			->get('staff');
		return $query->result();
	}
	//....................................................................................................................................

	public function insert_staff($data)
	{

		$staff=$this->db
			->select('email')
			->where('email',$data['email'])
			->get('staff');

		if($staff->num_rows()>0){
			echo 'duplicate'; //repeated_member
			return false;
		}else{
			$insert = $this->db->insert('staff', $data);
			//var_dump($insert);
			echo 'success'; //repeated_member
			return true;
		}
	}
	//....................................................................................................................................

	public function update_staff($data)
	{
//		var_dump($data);
		$staff_id = $data['staff_id'];
		$update = $this->db
			->where('staff_id', $staff_id)
			->update('staff',$data);
		return true;
	}
	//....................................................................................................................................

	public function getdata($staff_id)
	{
		$this->db->where('staff_id',$staff_id);
		$query=$this->db->get('staff');
		return $query->row();
	}
	//....................................................................................................................................

	public function get_provinces()
	{
		$query = $this->db
			->select('provinces_id, provinces_name')
			->order_by('provinces_id', 'ASC')
			->get('provinces');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_districts()
	{
		$query = $this->db
			->select('districts_id, districts_name')
			->order_by('districts_id', 'ASC')
			->get('districts');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_subdistricts()
	{
		$query = $this->db
			->select('sub_districts_id, sub_districts_name')
			->order_by('sub_districts_id', 'ASC')
			->get('sub_districts');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_fullname()
	{
		$query = $this->db
			->select('staff_id, staff_fname, staff_lname')
			->order_by('staff_id', 'DESC')
			->get('staff');
		return $query->result();
	}
	//....................................................................................................................................

		public function get_fullname_id($id){

		$query = $this->db
			->select('staff_id, staff_fname, staff_lname')
			->where('staff_id', $id)
			->get('staff');
		return $query->result();

	}
	//....................................................................................................................................

	public function delete($id)
	{
		$this->db->delete('staff', array('staff_id' => $id));
		return true;
	}
	//....................................................................................................................................

	/* Type Staff */
	public function get_type_staff_option()
	{
		$query = $this->db
			->select('type_staff_id, type_staff_title')
			->order_by('type_staff_id', 'ASC')
			->get('type_staff');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_type_staff_record($id)
	{
		$this->db->where('type_staff_id',$id);
		$query=$this->db->get('type_staff');
		return $query->row();
	}
	//....................................................................................................................................

	public function insert_type_staff_record($data)
	{
		$id = $data['type_staff_id'];
		$update = $this->db
			->where('type_staff_id', $id)
			->update('type_staff',$data);
		return true;
	}
	//....................................................................................................................................

	public function get_type_staff()
	{
		$query = $this->db
			->select('
				type_staff_id,
				type_staff_title,
				createdate,
				updatedate,
				publish,
				type_staff_order
			')
			->get('type_staff');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_type_staff_publish()
	{
		$query = $this->db
			->select('
				type_staff_id,
				type_staff_title,
				createdate,
				updatedate,
				publish,
				type_staff_order
			')
			->where('publish', 2)
			->get('type_staff');
		return $query->result();
	}
	//....................................................................................................................................

	public function insert_type_staff()
	{

    $data=$this->security->xss_clean($this->input->post());
    $permissions_id = $data['permissions_id'];
    $data = array(
      'type_staff_title'=>$data['type_staff_title'],
      'type_staff_detail'=>$data['type_staff_detail'],
      'publish'=>$data['publish'],
      'createdate'=>date("Y-m-d H:i:s"),
    );
		$insert = $this->db->insert('type_staff', $data);
    $type_staff_id = $this->db->insert_id();
    //var_dump($data);

    if( count($permissions_id) > 0 )
    {
      foreach ($permissions_id as $key => $value) {
        # code...
        $data_permissions = array(
          'permissions_id'=>$value,
          'type_staff_id'=>$type_staff_id,
        );
        $insert_rel = $this->db->insert('staff_permissions', $data_permissions);
      }
    }
		return true;
	}
	//....................................................................................................................................

	public function update_type_staff($id = 0)
	{
		$data=$this->security->xss_clean($this->input->post());
		$permissions_id = $data['permissions_id'];
    $data=array(
    	'type_staff_id'=>$data['type_staff_id'],
    	'type_staff_title'=>$data['type_staff_title'],
    	'type_staff_detail'=>$data['type_staff_detail'],
    	'publish'=>$data['publish'],
    	'updatedate'=>date("Y-m-d H:i:s"),
    );

    if( $id !== 0 )
    {
      $update = $this->db
  			->where('type_staff_id', $id)
  			->update('type_staff',$data);
    }

    //SELECT ตารางความสัมพันธ์เดิมมาก่อน
    $count_old_permissions_id = $this->get_rows_permissions_relation($id);


    if( count($permissions_id) > 0 )
    {
      $count_new_permissions_id = count($permissions_id);

      $row_relation = $this->get_permissions_relation_by_type_staff_id($id);

      $stack_old_permissions_id = array();
      foreach ($row_relation as $key_rel => $value_rel) {
        //echo $value_rel->permissions_id;
        array_push($stack_old_permissions_id, $value_rel->permissions_id);
        //echo "<br />";
      }
      // print_r($stack_old_permissions_id);

      $stack_new_permissions_id = array();
			// $this->primaryclass->pre_var_dump( $permissions_id );
      foreach ($permissions_id as $key => $value)
      {
        // echo $value;
        array_push($stack_new_permissions_id,  $value);
      }
      // print_r($stack_new_permissions_id);

      // ตัวที่เอาออก
      $result1 = array_diff($stack_old_permissions_id, $stack_new_permissions_id);
      // ตัวที่เพิ่มเข้ามา
      $result2 = array_diff($stack_new_permissions_id , $stack_old_permissions_id);
      $this->primaryclass->pre_var_dump($result1);
      $this->primaryclass->pre_var_dump($result2);


      foreach ($result1 as $key_r1 => $value_r1)
      {
        //echo 'delete : '. $value_r1;
        $this->db->where('permissions_id', $value_r1);
        $this->db->where('type_staff_id', $id);
        $this->db->delete('staff_permissions');
      }
      foreach ($result2 as $key_r2 => $value_r2)
      {
        // echo 'insert : '. $value_r2;
        $data_permissions = array(
          'permissions_id'=>$value_r2,
          'type_staff_id'=>$id,
        );
        $this->db->insert('staff_permissions', $data_permissions);
      }

    }

		return $update;
	}
	//....................................................................................................................................

	public function publish_record($field_id, $field_name, $id, $f_table, $p=1)
	{
		//$p = 0 : ไม่ระบบ | 1 : ไม่แสดงผล | 2 : แสดงผล
		$query = $this->db
			->set($field_name, $p)
			->set($field_name, $p)
			->where($field_id, $id)
			->update($f_table);
		//echo $query = $this->db->queries[0];
		return $query;
	}
	//....................................................................................................................................

	public function delete_record($field_id, $id, $f_table)
	{
		//$query = $this->db->delete($f_table, array($field_id => $id));

		$query =  $this->db
			->where($field_id, $id)
			->delete($f_table);
		//echo $query = $this->db->queries[0];
		return $query;
	}
	//....................................................................................................................................

  public function get_permissions_category()
  {
    $result = array();
    $query = $this->db->get('permissions_category');
    if( $query->num_rows() > 0 )
    {
      $result = $query->result();
    }
		return $result;
  }
  //....................................................................................................................................

  public function get_permissions_category_by_id($id=0)
  {
    $result = array();
    if ($id === 0)
    {
      $query = $this->db->get('permissions_category');
      $result = $query->result_array();
    }
    $query = $this->db->get_where('permissions_category', array('permissions_category_id' => $id));
    if( $query->num_rows() > 0 )
    {
      $result = $query->row_array();
    }
		return $result;
  }
  //....................................................................................................................................

  public function set_permissions_category( $id = 0 )
  {
    $this->load->helper('url');

    $data_security = $this->security->xss_clean( $this->input->post() );
    $slug = url_title($data_security['permissions_category_title'], 'dash', TRUE);

    $data = array(
        'permissions_category_title' => $data_security['permissions_category_title'],
        'slug' => $slug,
    );

    if ($id === 0)
    {
        return $this->db->insert('permissions_category', $data);
    }
    else
    {
        $this->db->where('permissions_category_id', $id);
        return $this->db->update('permissions_category', $data);
    }
  }
  //....................................................................................................................................

  public function get_permissions()
  {
    $result = array();
    $query = $this->db->get('permissions');
    if( $query->num_rows() > 0 )
    {
      $result = $query->result();
    }
		return $result;
  }
  //....................................................................................................................................


  public function get_permissions_by_id($id=0)
  {
    $result = array();
    if ($id === 0)
    {
      $query = $this->db->get('permissions');
      $result = $query->result();
    }
    $query = $this->db->get_where('permissions', array('permissions_id' => $id));
    if( $query->num_rows() > 0 )
    {
      $result = $query->row();
    }
		return $result;
  }
  //....................................................................................................................................

  public function get_permissions_by_category_id($category_id=0)
  {
    $result = array();
    if ($category_id === 0)
    {
      $query = $this->db->get('permissions');
      $result = $query->result();
    }
    $query = $this->db->get_where('permissions', array('permissions_category_id' => $category_id));
    if( $query->num_rows() > 0 )
    {
      $result = $query->result();
    }
		return $result;
  }
  //....................................................................................................................................

  public function set_permissions( $id = 0 )
  {
    $this->load->helper('url');

    $data_security = $this->security->xss_clean( $this->input->post() );
    $slug = url_title($data_security['permissions_title'], 'dash', TRUE);

    $data = array(
        'permissions_title' => $data_security['permissions_title'],
        'slug' => $slug,
        'permissions_category_id' => $data_security['permissions_category_id']
    );

    if ($id === 0)
    {
        return $this->db->insert('permissions', $data);
    }
    else
    {
        $this->db->where('permissions_id', $id);
        return $this->db->update('permissions', $data);
    }
  }
  //....................................................................................................................................

  public function permissions_type($type_id)
  {
    switch ($type_id)
    {
      case 0:
        $data = "ไม่กำหนด";
        break;
      case 1:
        $data = "หมวดหมู่";
        break;
      case 2:
        $data = "รายการย่อย";
        break;
      default:
        $data = "ไม่กำหนด";
        break;
    }
    return $data;
  }
  //....................................................................................................................................

  public function get_permissions_relation($permissions_id = 0, $type_staff_id = 0)
  {
    $result = array();
    if ($permissions_id === 0 && $type_staff_id === 0)
    {
      $query = $this->db->get('staff_permissions');
      $result = $query->result();
    }
    $query = $this->db->get_where('staff_permissions',
      array(
        'permissions_id' => $permissions_id,
        'type_staff_id' => $type_staff_id
      ));

    if( $query->num_rows() > 0 )
    {
      $result = $query->result();
      // echo $this->db->last_query();
    }
    // echo $result;
		return $result;
  }
  //....................................................................................................................................

  public function get_rows_permissions_relation($type_staff_id = 0)
  {
    $result = 0;
    if ($type_staff_id === 0)
    {
      $query = $this->db->get('staff_permissions');
      $result = $query->num_rows();
    }
    $query = $this->db->get_where('staff_permissions',
      array(
        'type_staff_id' => $type_staff_id
      ));

    if( $query->num_rows() > 0 )
    {
      $result = $query->num_rows();
      // echo $this->db->last_query();
    }
    // echo $result;
		return $result;
  }
  //....................................................................................................................................

  public function get_permissions_relation_by_id($permissions_id = 0, $type_staff_id = 0)
  {
    $result = '';
    if ($permissions_id === 0 && $type_staff_id === 0)
    {
      $query = $this->db->get('staff_permissions');
      $result = $query->result();
      $result = $result[0]->permissions_id;
    }
    $query = $this->db->get_where('staff_permissions',
      array(
        'permissions_id' => $permissions_id,
        'type_staff_id' => $type_staff_id
      ));

    if( $query->num_rows() > 0 )
    {
      $result = $query->result();
      $result = $result[0]->permissions_id;
      // echo $this->db->last_query();
    }
    // echo $result;
		return $result;
  }
  //....................................................................................................................................

  public function get_permissions_relation_by_type_staff_id( $type_staff_id = 0)
  {
    $result = array();
    if ($type_staff_id !== 0)
    {
			$query = $this->db->get_where('staff_permissions',
			array(
				'type_staff_id' => $type_staff_id
			));
    }

			// echo $this->db->last_query();
    if( $query->num_rows() > 0 )
    {
      $result = $query->result() ;
    }
		return $result;
  }
  //....................................................................................................................................

	public function get_staff_permissions_by_id( $permissions_id=0,$type_staff_id = 0)
  {
    $result = array('is_permission'=>false);

		$where_array = array('type_staff_id =' => $type_staff_id, 'permissions_id =' => $permissions_id);
    $query = $this->db->where($where_array);
    $query = $this->db->get('staff_permissions');

		// echo $this->db->last_query();
    if( $query->num_rows() > 0 )
    {
      $result = array('is_permission'=>true);
    }
		return $result;
  }
  //....................................................................................................................................

	public function get_permissions_category_sidebar( $type_staff_id = 0 )
	{
		$result = array();
		$query = $this->db->select('staff.*, permissions.*, category.*');
		$query = $this->db->from('permissions_category category');

		$query = $this->db->join('permissions', 'permissions.permissions_category_id=category.permissions_category_id', 'right');
		$query = $this->db->join('staff_permissions staff', 'permissions.permissions_id=staff.permissions_id', 'right');

		$query = $this->db->where('staff.type_staff_id' , $type_staff_id);
		$query = $this->db->group_by('category.permissions_category_id');
		$query = $this->db->get();
		// echo $this->db->last_query();
		if( $query->num_rows() > 0 )
    {
      $result = $query->result_array() ;
    }

	 	return $result;
	}
	//....................................................................................................................................

  public function get_permissions_sidebar( $type_staff_id = 0, $category_id = 0)
  {
    $result = array();
		$query = $this->db->select('staff_permissions.*,permissions.*');
		$query = $this->db->from('staff_permissions');
    if ($type_staff_id !== 0 && $category_id !== 0 )
    {
			$query = $this->db->where('type_staff_id' , $type_staff_id);
			$query = $this->db->where('permissions.permissions_category_id', $category_id);
    }
		$query = $this->db->join('permissions', 'permissions.permissions_id=staff_permissions.permissions_id' );
		$query = $this->db->order_by('staff_permissions.permissions_id', 'ASC');
		$query = $this->db->group_by('staff_permissions.permissions_id');
		$query = $this->db->get();
			// echo $this->db->last_query();
    if( $query->num_rows() > 0 )
    {
      $result = $query->result_array() ;
    }
		return $result;
  }
  //....................................................................................................................................
}
