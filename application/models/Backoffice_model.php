<?php

defined('BASEPATH') or exit();
class Backoffice_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    //....................................................................................................................................

    public function get_genaral_by_id($id = 0)
    {
        if ($id === 0) {
            $query = $this->db
                ->select('site_logo, site_favicon')
                ->where('site_option_id', $id)
                ->get('site_option');

            return $query->result_array();
        }

        $query = $this->db->get_where('site_option', array('site_option_id' => $id));

        return $query->row_array();
    }
    //....................................................................................................................................

    public function get_email_by_id($id = 0)
    {
        if ($id === 0) {
            $query = $this->db
                ->select('site_email')
                ->where('site_option_id', $id)
                ->get('site_option');

            return $query->result_array();
        }

        $query = $this->db->get_where('site_option', array('site_option_id' => $id));

        return $query->row_array();
    }
    //....................................................................................................................................

  public function get_banner_autoplay_by_id($id = 0)
  {
      if ($id === 0) {
          $query = $this->db
                ->select('site_banner_autoplay')
                ->where('site_option_id', $id)
                ->get('site_option');

          return $query->result_array();
      }

      $query = $this->db->get_where('site_option', array('site_option_id' => $id));

      return $query->row_array();
  }
    //....................................................................................................................................

    public function get_seo_by_id($id = 0)
    {
        if ($id === 0) {
            $query = $this->db
                ->select('site_seo_title,
					site_seo_meta,
					site_seo_description
					')
                ->where('site_option_id', $id)
                ->get('site_option');

            return $query->result_array();
        }

        $query = $this->db->get_where('site_option', array('site_option_id' => $id));

        return $query->row_array();
    }
    //....................................................................................................................................

    public function get_social_by_id($id = 0)
    {
        if ($id === 0) {
            $query = $this->db
                ->select('site_facebook,
					site_twitter,
					site_youtube
					')
                ->where('site_option_id', $id)
                ->get('site_option');

            return $query->result_array();
        }

        $query = $this->db->get_where('site_option', array('site_option_id' => $id));

        return $query->row_array();
    }
    //....................................................................................................................................

    public function get_footer_by_id($id = 0)
    {
        if ($id === 0) {
            $query = $this->db
                ->select('site_copyright,
					site_footer,
					')
                ->where('site_option_id', $id)
                ->get('site_option');

            return $query->result_array();
        }

        $query = $this->db->get_where('site_option', array('site_option_id' => $id));

        return $query->row_array();
    }
    //....................................................................................................................................

  public function get_loan_by_id($id = 0)
  {
      if ($id === 0) {
          $query = $this->db
                ->select('site_mrl,
          site_mor,
          site_mrr,
          site_cpr
          ')
                ->where('site_option_id', $id)
                ->get('site_option');

          return $query->result_array();
      }

      $query = $this->db->get_where('site_option', array('site_option_id' => $id));

      return $query->row_array();
  }
    //....................................................................................................................................

  public function get_member_type($type = 0)
  {
      $result = 0;

      if ($type !== '') {
          $query = $this->db
            ->select('member_id')
            ->where('member_type_id', $type)
            ->where('approved', 2)
            ->get('member');
      // echo $this->db->last_query();
      if ($query->num_rows() > 0) {
          $result = $query->num_rows();
      }
      } else {
          $query = $this->db
            ->select('member_id')
        ->where('approved', 2)
            ->get('member');
          if ($query->num_rows() > 0) {
              $result = $query->num_rows();
          }
      }

      return $result;
  }
    //....................................................................................................................................

  public function get_member_type_now($type = 0)
  {
      $result = 0;

      if ($type !== '') {
          $query = $this->db
            ->select('member_id')
        ->where('member_type_id', $type)
            ->where('approved', 2)
            ->where('createdate', date('Y-m-d H:i:s'))
            ->get('member');
        // echo $this->db->last_query();
      if ($query->num_rows() > 0) {
          $result = $query->num_rows();
      }
      } else {
          $query = $this->db
            ->select('member_id')
        ->where('approved', 2)
            ->where('createdate', date('Y-m-d H:i:s'))
            ->get('member');
        // echo $this->db->last_query();
      if ($query->num_rows() > 0) {
          $result = $query->num_rows();
      }
      }

      return $result;
  }
    //....................................................................................................................................

  public function get_staff_most_condo()
  {
      $result = array();
      $staff_admin = 1;

      $query = $this->db
            ->select('
        c.staff_id,
        COUNT(c.staff_id) as most_staff,
        s.fullname,
        t.type_staff_title
        ')
      ->from('condo c')
      ->join('staff s', 's.staff_id = c.staff_id', 'left')
      ->join('type_staff t', 't.type_staff_id = s.staff_group_id', 'left')
            ->where('s.staff_group_id !=', $staff_admin)
            ->where('s.approved', 2)
      ->group_by('c.staff_id')
      ->order_by('most_staff', 'DESC')
      ->limit(1)
            ->get();
    // echo $this->db->last_query();
    if ($query->num_rows() > 0) {
        $result = $query->row_array();
    }

      return $result;
  }
    //....................................................................................................................................

  public function get_staff_most_land()
  {
      $result = array();
      $staff_admin = 1;

      $query = $this->db
            ->select('
        l.staff_id,
        COUNT(l.staff_id) as most_staff,
        s.fullname,
        t.type_staff_title
        ')
      ->from('land l')
      ->join('staff s', 's.staff_id = l.staff_id', 'left')
      ->join('type_staff t', 't.type_staff_id = s.staff_group_id', 'left')
            ->where('s.staff_group_id !=', $staff_admin)
            ->where('s.approved', 2)
      ->group_by('l.staff_id')
      ->order_by('most_staff', 'DESC')
      ->limit(1)
            ->get();
    // echo $this->db->last_query();
    if ($query->num_rows() > 0) {
        $result = $query->row_array();
    }

      return $result;
  }
    //....................................................................................................................................
}
