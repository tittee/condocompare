<?php
defined('BASEPATH') OR exit();
class News_model extends CI_Model {
	//....................................................................................................................................

	function __construct(){
		parent::__construct();
		// set all to utf-8
    $this->output->set_header('Content-Type: text/html; charset=utf-8');
	}
	//....................................................................................................................................\

	public function get_news()
	{
		$query = $this->db
			->select('
				news_id,
				fk_news_category_id,
				news_title,
				news_pic_thumb,
				highlight,
				recommended,
				createdate,
				updatedate,
				publish,
        visited,
				news_order
			')
      ->order_by('news_order', 'ASC')
			->get('news');
		return $query->result();
	}
	//...................................................................................................................................

  public function get_newslist($limit, $start, $category_id = 0)
	{
		$result = array();

		// CLEAR DATA
		$data = $this->security->xss_clean($this->input->get());
		$keyword = (!empty($data['keyword'])) ? $data['keyword'] : '';

		$query = $this->db
			->select('
				news_id,
				fk_news_category_id,
				news_title,
				news_slug,
				news_caption,
				news_description,
				news_pic_thumb,
				highlight,
				recommended,
				createdate,
				updatedate,
				publish,
        visited,
				news_order
			');
			if( $category_id !== 0 ){
				$query = $this->db->where('fk_news_category_id', $category_id);
			}
			if( $keyword !== '' ){
				$query = $this->db->like('news_title', $keyword, 'both');
				$query = $this->db->or_like('news_caption', $keyword, 'both');
				$query = $this->db->or_like('news_description', $keyword, 'both');
			}

			$query = $this->db->where('publish', 2);
      $query = $this->db->order_by('news_order', 'ASC');
			$query = $this->db->limit($limit, $start);
			$query = $this->db->get('news');
			// echo $this->db->last_query();
			if($query->num_rows() > 0 )
			{
				$result = $query->result();
			}
		return $result;
	}
	//...................................................................................................................................


	public function get_news_highlight()
	{
		$result = array();
		$query = $this->db
			->select('
				news_id,
				fk_news_category_id,
				news_title,
				news_caption,
				news_pic_large,
				highlight,
				recommended,
				publish,
        visited,
				news_order
			')
			->where('publish', 2)
			->where('highlight', 2)
      ->order_by('news_order', 'ASC')
			->get('news');
			if($query->num_rows() > 0 )
			{
				$result = $query->result();
			}
		return $result;
	}
	//...................................................................................................................................

	public function get_news_mostread()
	{
		$result = array();
		$query = $this->db
			->select('
				news_id,
				news_title,
				news_slug,
				visited,
				createdate,
				publish,
        visited,
				news_order
			')
			->where('publish', 2)
			->order_by('visited', 'desc')
			->limit(5)
			->get('news');
			if($query->num_rows() > 0 )
			{
				$result = $query->result();
			}
		return $result;
	}
	//...................................................................................................................................


	public function get_info($id)
	{
		$this->db->where('news_id',$id);
		$query=$this->db->get('news');
		return $query->row();
	}
	//....................................................................................................................................

	public function get_news_by_title($title='')
	{
		if( $title !== ''){
			$query = $this->db->where('news_title', $title);
		}
		$query = $this->db->get('news');

		if( $query->num_rows() > 0 ){
			return true;
		}else{
			return false;
		}
	}
	//....................................................................................................................................

	public function get_news_by_slug($slug='')
	{
		if( $slug !== ''){
			$query = $this->db->where('news_slug', $slug);
		}
		$query = $this->db->get('news');
		return $query->row();
	}
	//....................................................................................................................................

	public function get_news_record( $category_id = 0 )
	{
		$total_rows = 0;
		$data = $this->security->xss_clean($this->input->get());
		$keyword = (!empty($data['keyword'])) ? $data['keyword'] : '';
		if( $category_id !== 0 ){
			$query = $this->db->where('fk_news_category_id', $category_id);
		}
		if( $keyword !== '' ){
			$query = $this->db->like('news_title', $keyword, 'both');
			$query = $this->db->or_like('news_caption', $keyword, 'both');
			$query = $this->db->or_like('news_description', $keyword, 'both');
		}
		$query = $this->db->where('publish', 2);
		$query = $this->db->get('news');
		// echo $this->db->last_query();
		$total_rows = $query->num_rows();

		return $total_rows;
	}
	//....................................................................................................................................

	public function insert_news($data)
	{
		$insert = $this->db->insert('news', $data);
		$news_id =  $this->db->insert_id();

		/* INSERT TO table : news_taxonomy */
		if( isset($data['fk_news_category_id']) && isset($data['fk_news_subcategory_id']) )
		{
			$data_taxonomy = array(
				'news_category_id'=>$data['fk_news_category_id'],
				'news_subcategory_id'=>$data['fk_news_subcategory_id'],
				'news_id'=>$news_id
			);
			$insert_taxonomy = $this->insert_taxonomy_news($data_taxonomy);
		}
		return true;
	}
	//....................................................................................................................................


	public function insert_taxonomy_news($data)
	{
		$insert = $this->db->insert('news_taxonomy', $data);
		return true;
	}
	//....................................................................................................................................

	public function update_news($data)
	{
		$id = $data['news_id'];
		$update = $this->db
			->where('news_id', $id)
			->update('news',$data);

		/* Update TO table : member_owner_news */

		if( isset($data['fk_news_category_id']) && isset($data['fk_news_subcategory_id']) )
		{
			$data_taxonomy = array(
				'news_category_id'=>$data['fk_news_category_id'],
				'news_subcategory_id'=>$data['fk_news_subcategory_id'],
				'news_id'=>$id
			);
			$update_taxonomy = $this->edit_relation_news( $data_taxonomy);
		}
		return $update;
	}
	//....................................................................................................................................

	public function update_slug(){
		// $this->load->helper('url');
		// $this->load->helper('string');
		$row = $this->get_news();
		foreach($row as $key => $value)
		{//START FOREACH#1
			// $slug = url_title($value->news_title, 'dash', TRUE);
			// $slug = increment_string($value->news_title, '-');
			$slug = str_replace(" ", "-", $value->news_title);
			$id = $value->news_id;
			$update = $this->db->set('news_slug', $slug);
			$update = $this->db->where('news_id', $id);
			$update = $this->db->update('news');
		}
		return true;
	}
	//....................................................................................................................................

	public function update_taxonomy_news($data)
	{
		$update = $this->db
			->where('news_id', $data['news_id'])
			->update('news_taxonomy',$data);
		return true;
	}
	//....................................................................................................................................


	/* news Category */
	public function get_news_category_option()
	{
		$query = $this->db
			->select('news_category_id, news_category_name')
			->order_by('news_category_id', 'ASC')
			->get('news_category');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_news_category_record($id)
	{
		$this->db->where('news_category_id',$id);
		$query=$this->db->get('news_category');
		return $query->row();
	}
	//....................................................................................................................................

	public function insert_news_category_record($data)
	{
		$id = $data['news_category_id'];
		$update = $this->db
			->where('news_category_id', $id)
			->update('news_category',$data);
		return true;
	}
	//....................................................................................................................................

	public function get_news_category()
	{
		$query = $this->db
			->select('
				news_category_id,
				news_category_name,
				news_category_pic_thumb,
				createby,
				updateby,
				createdate,
				updatedate,
				publish,
				news_category_order
			')
			->get('news_category');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_news_category_publish()
	{
		$result  = array();
		$query = $this->db
			->select('
				news_category_id,
				news_category_name,
				news_category_pic_thumb,
				createdate,
				updatedate,
				publish,
				news_category_order
			')
			->where('publish', 2)
			->get('news_category');
			if($query->num_rows() > 0 )
			{
				$result = $query->result();
			}
		return $result;
	}
	//....................................................................................................................................

	public function insert_news_category($data)
	{
		$insert = $this->db->insert('news_category', $data);
		return true;
	}
	//....................................................................................................................................

	public function update_news_category($data)
	{
		$id = $data['news_category_id'];
		$update = $this->db
			->where('news_category_id', $id)
			->update('news_category',$data);
		return $update;
	}
	//....................................................................................................................................

	public function publish_record($field_id, $field_name, $id, $f_table, $p=1)
	{
		//$p = 0 : ไม่ระบบ | 1 : ไม่แสดงผล | 2 : แสดงผล
		$query = $this->db
			->set($field_name, $p)
			->set($field_name, $p)
			->where($field_id, $id)
			->update($f_table);
		//echo $query = $this->db->queries[0];
		return $query;
	}
	//....................................................................................................................................

	public function delete_record($field_id, $id, $f_table)
	{
		//$query = $this->db->delete($f_table, array($field_id => $id));

		$query =  $this->db
			->where($field_id, $id)
			->delete($f_table);
		//echo $query = $this->db->queries[0];
		return $query;
	}
	//....................................................................................................................................

	/* news Subcategory */
	public function get_news_subcategory_option()
	{
		$query = $this->db
			->select('news_subcategory_id, news_subcategory_name')
			->order_by('news_subcategory_id', 'ASC')
			->get('news_subcategory');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_news_subcategory_record($id)
	{
		$this->db->where('news_subcategory_id',$id);
		$query=$this->db->get('news_subcategory');
		return $query->row();
	}
	//....................................................................................................................................

	public function insert_news_subcategory_record($data)
	{
		$id = $data['news_subcategory_id'];
		$update = $this->db
			->where('news_subcategory_id', $id)
			->update('news_subcategory',$data);
		return true;
	}
	//....................................................................................................................................

	public function get_news_subcategory()
	{
		$query = $this->db
			->select('
				news_subcategory_id,
				fk_news_category_id,
				news_subcategory_name,
				news_subcategory_expert,
				news_subcategory_description,
				news_subcategory_pic_thumb,
				createby,
				updateby,
				createdate,
				updatedate,
				publish,
				news_subcategory_order
			')
			->get('news_subcategory');
		return $query->result();
	}
	//....................................................................................................................................

	public function insert_news_subcategory($data)
	{
		$insert = $this->db->insert('news_subcategory', $data);
		return true;
	}
	//....................................................................................................................................

	public function update_news_subcategory($data)
	{
		$id = $data['news_subcategory_id'];
		$update = $this->db
			->where('news_subcategory_id', $id)
			->update('news_subcategory',$data);
		return true;
	}
	//....................................................................................................................................

}
