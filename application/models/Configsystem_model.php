<?php
defined('BASEPATH') OR exit();
class Configsystem_model extends CI_Model {
	//....................................................................................................................................

	function __construct(){
		parent::__construct();
	}
	//....................................................................................................................................\

	public function delete_record($field_id, $id, $f_table)
	{
		$this->db->delete($f_table, array($field_id => $id));
		return true;
	}
	//....................................................................................................................................

	/* Type Offering */
	//....................................................................................................................................

	public function set_type_offering_record($data)
	{
		$id = $data['type_offering_id'];
		$update = $this->db
			->where('type_offering_id', $id)
			->update('type_offering',$data);
		return true;
	}
	//....................................................................................................................................

	/*สำหรับ Select Option*/
	public function get_type_offering_option()
	{
		$query = $this->db
			->select('type_offering_id, type_offering_title')
			->order_by('type_offering_id', 'ASC')
			->get('type_offering');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_type_offering()
	{
		$query = $this->db
			->select('
				type_offering_id,
				type_offering_title,
				type_offering_detail,
				createdate,
				updatedate,
				publish
			')
			->get('type_offering');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_type_offering_record($id)
	{
		$this->db->where('type_offering_id',$id);
		$query=$this->db->get('type_offering');
		return $query->row();
	}
	//....................................................................................................................................

	public function set_type_offering($data)
	{
		$insert = $this->db->insert('type_offering', $data);
		return true;
	}
	//....................................................................................................................................

	public function update_type_offering($data)
	{
		$id = $data['type_offering_id'];
		$update = $this->db
			->where('type_offering_id', $id)
			->update('type_offering',$data);
		return true;
	}
	//....................................................................................................................................

	/* Type Zone */
	//....................................................................................................................................

	public function set_type_zone_record($data)
	{
		$id = $data['type_zone_id'];
		$update = $this->db
			->where('type_zone_id', $id)
			->update('type_zone',$data);
		return true;
	}
	//....................................................................................................................................

	/*สำหรับ Select Option*/
	public function get_type_zone_option()
	{
		$query = $this->db
			->select('type_zone_id, type_zone_title')
			->order_by('type_zone_id', 'ASC')
			->get('type_zone');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_type_zone()
	{
		$query = $this->db
			->select('
				type_zone_id,
				type_zone_title,
				createdate,
				updatedate,
				publish
			')
			->get('type_zone');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_type_zone_title($id)
	{
		$query = $this->db
			->select('type_zone_title,')
			->where('type_zone_id', $id)
			->get('type_zone');
		if ( $query->num_rows() > 0 )
		{
			return $query->row()->type_zone_title;
		}
		else
		{
			return 'ไม่ระบุ';
		}
	}
	//....................................................................................................................................

	public function get_type_zone_record($id)
	{
		$this->db->where('type_zone_id',$id);
		$query=$this->db->get('type_zone');
		return $query->row();
	}
	//....................................................................................................................................

	public function set_type_zone($data)
	{
		$insert = $this->db->insert('type_zone', $data);
		return true;
	}
	//....................................................................................................................................

	public function update_type_zone($data)
	{
		$id = $data['type_zone_id'];
		$update = $this->db
			->where('type_zone_id', $id)
			->update('type_zone',$data);
		return true;
	}
	//....................................................................................................................................

	/* Type Property */
	//....................................................................................................................................
	public function get_type_property($id = 0)
	{
		$result = array();
		if ($id === 0)
		{
			$query = $this->db->get('type_property');
			$result = $query->result_array();
			return $result;
		}
		$query = $this->db->get_where('type_property', array('type_property_id' => $id));
		$result = $query->row_array();
		return $result;
	}
	//....................................................................................................................................

	public function get_type_property_by_id($id = 0)
	{
		$result = array();
		if ($id === 0)
		{
			$query = $this->db->get('type_property');
			$result = $query->result_array();
			return $result;
		}

		$query = $this->db->get_where('type_property', array('type_property_id' => $id));
		$result = $query->row_array();
		return $result;
	}
	//....................................................................................................................................

	public function set_type_property($id = 0)
	{
		$data=$this->security->xss_clean($this->input->post());
		if ($id === 0)
		{
			$data=array(
				'type_property_title'=>$data['type_property_title'],
				'type_property_detail'=>$data['type_property_detail'],
				'createdate'=>date("Y-m-d H:i:s"),
				'publish'=>$data['publish'],
			);
			return $this->db->insert('type_property', $data);
		}
		else
		{
			$data=array(
				'type_property_title'=>$data['type_property_title'],
				'type_property_detail'=>$data['type_property_detail'],
				'updatedate'=>date("Y-m-d H:i:s"),
				'publish'=>$data['publish'],
			);
			$this->db->where('type_property_id', $id);
			return $this->db->update('type_property', $data);
		}
	}
	//....................................................................................................................................

	/*สำหรับ Select Option*/
	public function get_type_property_option()
	{
		$result = array();
		$query = $this->db
			->select('type_property_id, type_property_title')
			->order_by('type_property_id', 'ASC')
			->get('type_property');
		if($query->num_rows() > 0)
		{
			$result = $query->result();
		}
		return $result;
	}
	//....................................................................................................................................


	/* Type Suites */
	//....................................................................................................................................

	public function set_type_suites_record($data)
	{
		$id = $data['type_suites_id'];
		$update = $this->db
			->where('type_suites_id', $id)
			->update('type_suites',$data);
		return true;
	}
	//....................................................................................................................................

	/*สำหรับ Select Option*/
	public function get_type_suites_option()
	{
		$query = $this->db
			->select('type_suites_id, type_suites_title')
			->order_by('type_suites_id', 'ASC')
			->get('type_suites');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_type_suites()
	{
		$query = $this->db
			->select('
				type_suites_id,
				type_suites_title,
				createdate,
				updatedate,
				publish
			')
			->get('type_suites');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_type_suites_record($id)
	{
		$this->db->where('type_suites_id',$id);
		$query=$this->db->get('type_suites');
		return $query->row();
	}
	//....................................................................................................................................

	public function set_type_suites($data)
	{
		$insert = $this->db->insert('type_suites', $data);
		return true;
	}
	//....................................................................................................................................

	public function update_type_suites($data)
	{
		$id = $data['type_suites_id'];
		$update = $this->db
			->where('type_suites_id', $id)
			->update('type_suites',$data);
		return true;
	}
	//....................................................................................................................................

	/* Type Decoration */
	//....................................................................................................................................

	public function set_type_decoration_record($data)
	{
		$id = $data['type_decoration_id'];
		$update = $this->db
			->where('type_decoration_id', $id)
			->update('type_decoration',$data);
		return true;
	}
	//....................................................................................................................................

	/*สำหรับ Select Option*/
	public function get_type_decoration_option()
	{
		$query = $this->db
			->select('type_decoration_id, type_decoration_title')
			->order_by('type_decoration_id', 'ASC')
			->get('type_decoration');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_type_decoration()
	{
		$query = $this->db
			->select('
				type_decoration_id,
				type_decoration_title,
				createdate,
				updatedate,
				publish
			')
			->get('type_decoration');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_type_decoration_record($id)
	{
		$this->db->where('type_decoration_id',$id);
		$query=$this->db->get('type_decoration');
		return $query->row();
	}
	//....................................................................................................................................

	public function set_type_decoration($data)
	{
		$insert = $this->db->insert('type_decoration', $data);
		return true;
	}
	//....................................................................................................................................

	public function update_type_decoration($data)
	{
		$id = $data['type_decoration_id'];
		$update = $this->db
			->where('type_decoration_id', $id)
			->update('type_decoration',$data);
		return true;
	}
	//....................................................................................................................................

	/* CONDO STATUS */
	//....................................................................................................................................

	public function set_type_status_record($data)
	{
		$id = $data['type_status_id'];
		$update = $this->db
			->where('type_status_id', $id)
			->update('type_status',$data);
		return true;
	}
	//....................................................................................................................................

	/*สำหรับ Select Option*/
	public function get_type_status_option()
	{
		$query = $this->db
			->select('type_status_id, type_status_title')
			->order_by('type_status_id', 'ASC')
			->get('type_status');
		return $query->result();
	}
	//....................................................................................................................................

	/*สำหรับ Select Option*/
	public function get_type_status_title($id)
	{
		$query = $this->db
			->select('type_status_title')
			->where('type_status_id', $id)
			->get('type_status');
		if ( $query->num_rows() > 0 )
		{
			return $query->row()->type_status_title;
		}
		else
		{
			return 'ประกาศใหม่';
		}
	}
	//....................................................................................................................................

	public function get_type_status()
	{
		$query = $this->db
			->select('
				type_status_id,
				type_status_title,
				type_status_detail,
				createdate,
				updatedate,
				publish
			')
			->get('type_status');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_type_status_record($id)
	{
		$this->db->where('type_status_id',$id);
		$query=$this->db->get('type_status');
		return $query->row();
	}
	//....................................................................................................................................

	public function set_type_status($data)
	{
		$insert = $this->db->insert('type_status', $data);
		return true;
	}
	//....................................................................................................................................

	public function update_type_status($data)
	{
		$id = $data['type_status_id'];
		$update = $this->db
			->where('type_status_id', $id)
			->update('type_status',$data);
		return true;
	}
	//....................................................................................................................................

	public function get_status_title($id=0)
	{
		if( $id == 0 )
		{
			return 'ประกาศได้ถูกสร้างขึ้นแล้ว';
		}
		else
		{
			$query = $this->db
				->select('
					type_status_id,
					type_status_title
				')
				->where('type_status_id', $id)
				->get('type_status');

			//print_r($query);
			return $query->row()->type_status_title;
		}

	}
	//....................................................................................................................................
}
