<?php
defined('BASEPATH') OR exit();
class User_model extends CI_Model {
	//....................................................................................................................................

	function __construct()
	{
		parent::__construct();
	}
	//....................................................................................................................................

	public function info($r=0)
	{
		$staff_id=$this->session->userdata('zID');
		$staff_id=($staff_id)?$staff_id:0;
//echo $staff_id;
		if($r){
			return ($staff_id)?$this->login_module($staff_id):0;
		}elseif($staff_id){

			$this->input->set_cookie(array('name'=>'zID','value'=>$staff_id,'expire'=>(time()+60*60*24*30*365)));
			$this->session->set_userdata('zID',$staff_id);

			$user=$this->db
				->select('
					staff.staff_id,
					staff.username,
					staff.password,
					staff.avatar,
					staff.phone,
					staff.fullname,
					staff.lastlogin,
					staff.approved,
					staff.updatedate,
					staff.createdate,
					staff.ip
					')
				->where('staff.staff_id',$staff_id)
				->where('staff.approved','1')
				->from('staff')
				->get()
				->first_row('array');

				$user['avatar']=$this->M->avatar($user['avatar']);
				return $user;
			}elseif($this->uri->uri_string()!='user/login'){
				redirect('user/login');
			}else{
				return array('id'=>0);
			}

	}
	//....................................................................................................................................

	public function add_data($data){
		$this->db->insert('staff', $data);
	return TRUE;
	}
	//....................................................................................................................................

	public function login(){

		$data=$this->security->xss_clean($this->input->post());
		$return=array('success'=>false,'message'=>'');

		$user=$this->db
			->select('staff_id,username,password')
			->where('username',$data['username'])
			->where('password',md5($data['password']))
			->get('staff');

		if($user->num_rows()>0){

			$user=$user->row_array();
			$this->session->set_userdata('zID',$user['staff_id']);
			$this->session->set_userdata('zUSER',$user['username']);
			$this->input->set_cookie(array('name'=>'zID','value'=>$user['staff_id'],'expire'=>(time()+60*60*24*30*365)));
			$this->input->set_cookie(array('name'=>'zUSER','value'=>$user['username'],'expire'=>(time()+60*60*24*30*365)));
			return true;
		}else{
			$this->form_validation->set_message('check_database', 'Invalid username or password');
			echo 'Invalid username or password';
			return false;
		}
	}
	//....................................................................................................................................

//public function login(){
//	if(!$this->input->is_ajax_request() || !$this->input->post()){
//		exit;
//	}
//	$data=$this->input->post(NULL,TRUE);
//	$return=array('success'=>false,'message'=>'');
//
//	$user=$this->db
//		->select('staff_id,username,password,email,avatar')
//		->where('username',$data['username'])
//		->get('staff');
//
//	$Login=false;
//	if($this->encryption->decrypt($data['captcha'])=='Lockscreen'){
//		$Login=true;
//	}
//
//	if(!$Login && !$this->M->check_captcha($data['captcha'])){
//		$return['message']=lang2(26);
//	}else{
//		if($user->num_rows()>0){
//			$user=$user->row_array();
//			if($data['password']!==$this->encryption->decrypt($user['password'])){
//				$return['message']=lang2(27);
//			}elseif(!$user['tb1f_status']){
//				$return['message']=lang2(28);
//			}else{
//				$this->session->set_userdata('zUSER',$user['tb1f_id']);
//				$this->session->set_userdata('zLANG',$data['lang']);
//				$this->input->set_cookie(array('name'=>'zUSER','value'=>$user['tb1f_id'],'expire'=>(time()+60*60*24*30*365)));
//				$this->input->set_cookie(array('name'=>'zLANG','value'=>$data['lang'],'expire'=>(time()+60*60*24*30*365)));
//				$this->M->LOG('Sign In',$user['tb1f_id']);
//				$return=array('success'=>true,'module'=>$this->login_module($user['tb1f_id']));
//			}
//		}else{
//			$return['message']=lang2(29);
//		}
//	}
//	$this->M->JSON($return);
//}
//....................................................................................................................................
	private function login_module($id){
		return $this->db
			->select('*')
			->where('staff.staff_id',$id)
			->from('staff')
			->get()
			->row(0);
	}
	//....................................................................................................................................

	public function logout($redirect=true){
		$this->session->unset_userdata('zID');
		$this->session->unset_userdata('zUSER');
		$this->session->sess_destroy();
		@session_destroy();
		delete_cookie("zID");
		delete_cookie("zUSER");
		//$this->M->LOG('Sign Out');
		if($redirect){redirect('/');}
	}
	//....................................................................................................................................

	public function viewdata()
	{
		$user = $this->db
			->select('*')
			->get('staff');
		return $user->result();
	}
	//....................................................................................................................................

	public function add($data)
	{
		$insert = $this->db->insert('staff', $data);
		return true;
	}
	//....................................................................................................................................

	public function edit($data)
	{
		//var_dump($data);
		$staff_id = $data['staff_id'];
		$update = $this->db
			->where('staff_id', $staff_id)
			->update('staff',$data);
		return true;
	}
	//....................................................................................................................................

	public function getdata($staff_id)
	{
		$this->db->where('staff_id',$staff_id);
		$query=$this->db->get('staff');
		return $query->row();
	}
	//....................................................................................................................................

	public function delete($id)
	{
		$this->db->delete('staff', array('staff_id' => $id));
		return true;
	}
	//....................................................................................................................................

	public function _getgroup_name($staff_group_id)
	{
		$staff_group_name ='';
		switch ($staff_group_id)
		{
			case 0:
				$staff_group_name = 'ข้อมูลไม่ถูกต้อง';
				break;
			case 1:
				$staff_group_name = 'ผู้ดูแลระบบ';
				break;
			case 2:
				$staff_group_name = 'พนักงานขาย';
				break;
			default:
				$staff_group_name = 'ข้อมูลไม่ถูกต้อง';
				break;
		}
		return $staff_group_name;
	}
	//....................................................................................................................................

	/*สำหรับ Select Option*/
	public function get_staff_option()
	{
		$query = $this->db
			->select('staff_id, fullname')
			->where('staff_group_id =', '2')
			->order_by('staff_id', 'ASC')
			->get('staff');
		return $query->result();
	}
	//....................................................................................................................................

	/*สำหรับ Fullname */
	public function get_staff_fullname($id)
	{
		$query = $this->db
			->select('fullname')
			->where('staff_id', $id)
			->get('staff');
		if ( $query->num_rows() > 0 )
		{
			return $query->row()->fullname;
		}
		else
		{
			return 'ไม่ระบุ';
		}
	}
	//....................................................................................................................................

}
