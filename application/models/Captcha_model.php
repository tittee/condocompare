<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

	class Captcha_model extends CI_Model
	{
		function __construct()
		{
				parent::__construct();
		}
	  //......................................................................................................

		function create_captcha(){

      $this->delete_captcha();

			$random_number = substr(number_format(time() * rand(), 0, '', ''), 0, 6);
      $vals = array(
        'word' => $random_number,
        'img_path' => './uploads/captcha/',
        'img_url' => base_url().'./uploads/captcha/',
        'font_path' => './path/to/fonts/texb.ttf',
        'img_width' => '150',
        'img_height' => 30,
        'expiration' => 7200,
        'word_length' => 8,
        'font_size' => 16,
        'img_id' => 'Imageid',
        'pool' => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

        // White background and border, black text and red grid
        'colors' => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 40, 40),
        ),
      );

      $cap = create_captcha($vals);
      // var_dump($cap);
      $data = array(
              'captcha_time' => $cap['time'],
              'ip_address' => $this->input->ip_address(),
              'word' => $cap['word'],
      );

      $query = $this->db->insert_string('captcha', $data);
      $this->db->query($query);

      return $cap;
		}
    //......................................................................................................

    function delete_captcha(){
      // First, delete old captchas
      $expiration = time() - 7200; // Two hour limit
      $this->db->where('captcha_time < ', $expiration)
              ->delete('captcha');
      return $expiration;
		}
    //......................................................................................................

    function get_captcha( $captcha ){
      $results = array('success' => false);
      // $data=$this->security->xss_clean( $this->input->post() );

      // First, delete old captchas
      $expiration = $this->delete_captcha();

      // Then see if a captcha exists:
      $sql = 'SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?';
      $binds = array($captcha, $this->input->ip_address(), $expiration);
      $query = $this->db->query($sql, $binds);
      // echo $this->db->last_query();
      $row = $query->row();

      if ($row->count > 0)
      {
        $results = array('success' => true, 'message' => 'คุณกรอกรหัสระบุตัวตนถูกต้อง...');
      }else{
        $results = array('success' => false, 'message' => 'ท่านกรอกรหัสระบุตัวตนไม่ถูกต้องค่ะ กรูณาลองใหม่อีกครั้ง...');
      }

      return $results;
		}
    //......................................................................................................
}
?>
