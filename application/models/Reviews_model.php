<?php
defined('BASEPATH') OR exit();
class Reviews_model extends CI_Model {
	//....................................................................................................................................

	function __construct(){
		parent::__construct();
	}
	//....................................................................................................................................\

	public function get_reviews()
	{
		$query = $this->db
			->select('
				reviews_id,
				fk_reviews_category_id,
				reviews_title,
				reviews_slug,
				reviews_pic_thumb,
				highlight,
				recommended,
				createdate,
				updatedate,
				publish,
        visited,
				reviews_order
			')
			->get('reviews');
		return $query->result();
	}
	//...................................................................................................................................

	public function get_reviewslist($limit, $start, $category_id = 0)
	{
		$result = array();

		// CLEAR DATA
		$data = $this->security->xss_clean($this->input->get());
		$keyword = (!empty($data['keyword'])) ? $data['keyword'] : '';

		$query = $this->db
			->select('
				reviews_id,
				fk_reviews_category_id,
				reviews_title,
				reviews_slug,
				reviews_caption,
				reviews_description,
				reviews_pic_thumb,
				highlight,
				recommended,
				createdate,
				updatedate,
				publish,
        visited,
				reviews_order
			');
			if( $category_id !== 0 ){
				$query = $this->db->where('fk_reviews_category_id', $category_id);
			}
			if( $keyword !== '' ){
				$query = $this->db->like('reviews_title', $keyword, 'both');
				$query = $this->db->or_like('reviews_caption', $keyword, 'both');
				$query = $this->db->or_like('reviews_description', $keyword, 'both');
			}

			$query = $this->db->where('publish', 2);
      $query = $this->db->order_by('reviews_order', 'ASC');
			$query = $this->db->limit($limit, $start);
			$query = $this->db->get('reviews');
			// echo $this->db->last_query();
			if($query->num_rows() > 0 )
			{
				$result = $query->result();
			}
		return $result;
	}
	//...................................................................................................................................

	public function get_reviews_mostread()
	{
		$result = array();
		$query = $this->db
			->select('
				reviews_id,
				reviews_title,
				reviews_slug,
				visited,
				createdate,
				publish,
        visited,
				reviews_order
			')
			->where('publish', 2)
			->order_by('visited', 'desc')
      ->limit(5)
			->get('reviews');
			if($query->num_rows() > 0 )
			{
				$result = $query->result();
			}
		return $result;
	}
	//...................................................................................................................................

	public function get_reviews_search($keyword = null)
	{
		$result = array();

		if( $keyword !== '')
		{
			$query = $this->db
				->select('
					reviews_id,
					fk_reviews_category_id,
					reviews_title,
					reviews_slug,
					reviews_caption,
					reviews_description,
					reviews_pic_thumb,
					highlight,
					recommended,
					createdate,
					updatedate,
					publish,
          visited,
					reviews_order
				')
				->where('publish', 2)
				->like('reviews_title', $keyword, 'both')
				->or_like('reviews_caption', $keyword, 'both')
				->or_like('reviews_description', $keyword, 'both')
				->get('reviews');
			if($query->num_rows() > 0 )
			{
				$result = $query->result();
			}
		}
		else
		{
			return $result = $this->get_reviews_category_publish();
		}

		return $result;
	}
	//...................................................................................................................................

	public function get_info($id)
	{
		$this->db->where('reviews_id',$id);
		$query=$this->db->get('reviews');
		return $query->row();
	}
	//....................................................................................................................................

  public function get_reviews_record( $category_id = 0 )
	{
		$total_rows = 0;
		$data = $this->security->xss_clean($this->input->get());
		$keyword = (!empty($data['keyword'])) ? $data['keyword'] : '';
		if( $category_id !== 0 ){
			$query = $this->db->where('fk_reviews_category_id', $category_id);
		}
		if( $keyword !== '' ){
			$query = $this->db->like('reviews_title', $keyword, 'both');
			$query = $this->db->or_like('reviews_caption', $keyword, 'both');
			$query = $this->db->or_like('reviews_description', $keyword, 'both');
		}
		$query = $this->db->where('publish', 2);
		$query = $this->db->get('reviews');
		// echo $this->db->last_query();
		$total_rows = $query->num_rows();

		return $total_rows;
	}
	//.....................................................................................................................................


	public function get_reviews_id($id)
	{
		$query = $this->db
			->select('*')
			->where('reviews_id', $id)
			->get('reviews');
		return $query->row();
	}
	//....................................................................................................................................



	public function insert_reviews($data)
	{
		$insert = $this->db->insert('reviews', $data);
		$reviews_id =  $this->db->insert_id();

		/* INSERT TO table : reviews_taxonomy */
		if( isset($data['fk_reviews_category_id']) && isset($data['fk_reviews_subcategory_id']) )
		{
			$data_taxonomy = array(
				'reviews_category_id'=>$data['fk_reviews_category_id'],
				'reviews_subcategory_id'=>$data['fk_reviews_subcategory_id'],
				'reviews_id'=>$reviews_id
			);
			$insert_taxonomy = $this->insert_taxonomy_reviews($data_taxonomy);
		}
		return true;
	}
	//....................................................................................................................................


	public function update_reviews($data)
	{
		$id = $data['reviews_id'];
		$update = $this->db
			->where('reviews_id', $id)
			->update('reviews',$data);

		/* Update TO table : member_owner_reviews */

		if( isset($data['fk_reviews_category_id']) && isset($data['fk_reviews_subcategory_id']) )
		{
			$data_taxonomy = array(
				'reviews_category_id'=>$data['fk_reviews_category_id'],
				'reviews_subcategory_id'=>$data['fk_reviews_subcategory_id'],
				'reviews_id'=>$id
			);
			$update_taxonomy = $this->edit_relation_reviews( $data_taxonomy);
		}
		return true;
	}
	//....................................................................................................................................

	public function update_taxonomy_reviews($data)
	{
		$update = $this->db
			->where('reviews_id', $data['reviews_id'])
			->update('reviews_taxonomy',$data);
		return true;
	}
	//....................................................................................................................................


	/* reviews Category */
	public function get_reviews_category_option()
	{
		$query = $this->db
			->select('reviews_category_id, reviews_category_name')
			->order_by('reviews_category_id', 'ASC')
			->get('reviews_category');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_reviews_category_record($id)
	{
		$this->db->where('reviews_category_id',$id);
		$query=$this->db->get('reviews_category');
		return $query->row();
	}
	//....................................................................................................................................

	public function insert_reviews_category_record($data)
	{
		$id = $data['reviews_category_id'];
		$update = $this->db
			->where('reviews_category_id', $id)
			->update('reviews_category',$data);
		return true;
	}
	//....................................................................................................................................

	public function get_reviews_category()
	{
		$query = $this->db
			->select('
				reviews_category_id,
				reviews_category_name,
				reviews_category_pic_thumb,
				createby,
				updateby,
				createdate,
				updatedate,
				publish,
				reviews_category_order
			')
			->get('reviews_category');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_reviews_category_publish()
	{
		$query = $this->db
			->select('
				reviews_category_id,
				reviews_category_name,
				reviews_category_pic_thumb,
				createdate,
				updatedate,
				publish,
				reviews_category_order
			')
			->where('publish', 2)
			->get('reviews_category');
		return $query->result();
	}
	//....................................................................................................................................

	public function insert_reviews_category($data)
	{
		$insert = $this->db->insert('reviews_category', $data);
		return true;
	}
	//....................................................................................................................................

	public function update_reviews_category($data)
	{
		$id = $data['reviews_category_id'];
		$update = $this->db
			->where('reviews_category_id', $id)
			->update('reviews_category',$data);
		return true;
	}
	//....................................................................................................................................

	public function publish_record($field_id, $field_name, $id, $f_table, $p=1)
	{
		//$p = 0 : ไม่ระบบ | 1 : ไม่แสดงผล | 2 : แสดงผล
		$query = $this->db
			->set($field_name, $p)
			->set($field_name, $p)
			->where($field_id, $id)
			->update($f_table);
		echo $query = $this->db->queries[0];
		return $query;
	}
	//....................................................................................................................................

	public function delete_record($field_id, $id, $f_table)
	{
		//$query = $this->db->delete($f_table, array($field_id => $id));

		$query =  $this->db
			->where($field_id, $id)
			->delete($f_table);
		//echo $query = $this->db->queries[0];
		return $query;
	}
	//....................................................................................................................................

	/* reviews Subcategory */
	public function get_reviews_subcategory_option()
	{
		$query = $this->db
			->select('reviews_subcategory_id, reviews_subcategory_name')
			->order_by('reviews_subcategory_id', 'ASC')
			->get('reviews_subcategory');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_reviews_subcategory_record($id)
	{
		$this->db->where('reviews_subcategory_id',$id);
		$query=$this->db->get('reviews_subcategory');
		return $query->row();
	}
	//....................................................................................................................................

	public function insert_reviews_subcategory_record($data)
	{
		$id = $data['reviews_subcategory_id'];
		$update = $this->db
			->where('reviews_subcategory_id', $id)
			->update('reviews_subcategory',$data);
		return true;
	}
	//....................................................................................................................................

	public function get_reviews_subcategory()
	{
		$query = $this->db
			->select('
				reviews_subcategory_id,
				fk_reviews_category_id,
				reviews_subcategory_name,
				reviews_subcategory_expert,
				reviews_subcategory_description,
				reviews_subcategory_pic_thumb,
				createby,
				updateby,
				createdate,
				updatedate,
				publish,
				reviews_subcategory_order
			')
			->get('reviews_subcategory');
		return $query->result();
	}
	//....................................................................................................................................

	public function insert_reviews_subcategory($data)
	{
		$insert = $this->db->insert('reviews_subcategory', $data);
		return true;
	}
	//....................................................................................................................................

	public function update_reviews_subcategory($data)
	{
		$id = $data['reviews_subcategory_id'];
		$update = $this->db
			->where('reviews_subcategory_id', $id)
			->update('reviews_subcategory',$data);
		return true;
	}
	//....................................................................................................................................

}
