<?php
defined('BASEPATH') OR exit();
class Member_model extends CI_Model {
	//....................................................................................................................................

  var $table = 'member';
  var $column_order = array(null, 'member_id','member_fname','member_email','member_mobileno','member_type_id','approved'); //set column field database for datatable orderable
  var $column_search = array('member_fname','member_lname','member_email','member_mobileno'); //s
  var $order = array('member_id' => 'desc'); // default order

	function __construct()
	{
		parent::__construct();
		$this->load->helper('cookie');
	}
	//....................................................................................................................................

	public function logout($redirect=true){
		$this->session->unset_userdata('mID');
		$this->session->unset_userdata('mUSER');
		$this->session->unset_userdata('mFBID');
		$this->session->unset_userdata('mTID');
		$this->session->sess_destroy();
		@session_destroy();
		delete_cookie("mID");
		delete_cookie("mUSER");
		delete_cookie("mFBID");
		delete_cookie("mTID");
//		$this->M->LOG('Sign Out');
		if($redirect){redirect('/');}
	}
	//....................................................................................................................................


  public function get_json_land()
  {
      // storing  request (ie, get/post) global array to a variable
      $requestData = $_REQUEST;
      // echo "<pre>";
      // print_r($_REQUEST);
      // echo "</pre>";

      $columns = array(
      // datatable column index  => database column name
          0 => 'member_id',
          1 => 'member_fname',
          2 => 'member_email',
          3 => 'member_mobileno',
          4 => 'member_type_id',
          5 => 'approved',
      );

      $queryF1 = $this->db->get('land');
      $totalData = $queryF1->num_rows();
      $totalFiltered = $totalData;

      $query = $this->db
        ->select('
          land_id,
          member_id,
          land_property_id,
          staff_id,
          land_title,
          pic_thumb,
          createdate,
          modifydate,
          visited,
          type_status_id,
          approved');
          $query = $this->db->get('land');
      $query = $this->db->where('1=1');
      if( !empty($requestData['search']['value']) ) {
        $query = $this->db->like('land_title', $requestData['search']['value'], 'both');
        $query = $this->db->or_like('land_property_id', $requestData['search']['value'], 'both');
      }
      $requestData['start'] = (isset($requestData['start']))? $requestData['start'] : 0 ;
      $requestData['length'] = (isset($requestData['length']))? $requestData['length'] : 50 ;

      $query = $this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
      $query = $this->db->limit($requestData['start'], $requestData['length']);
      $query = $this->db->get('land');
      // $sqltext = $this->db->last_query();
      $totalFiltered = $query->num_rows();


      $data = array();
      while( $row=$query->unbuffered_row() )
      {
        $nestedData=array();
        foreach($row as $index=>$value) {
      		$nestedData[$index] = $value;
      	}
        $data[] = $nestedData;
      }
      //END WHILE
      // $row = $query->result();
      $requestData['draw'] = (isset($requestData['draw']))? $requestData['draw'] : 1 ;


      $json_data = array(
        'draw' => intval($requestData['draw']),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
        'recordsTotal' => intval($totalData),  // total number of records
        'recordsFiltered' => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
        'records' => $data,   // total data array
        // 'sql' => $sqltext,   // total data array
      );

      return $json_data;
  }
    //....................................................................................................................................


    private function _get_datatables_query()
    {

          $this->db->from($this->table);

          $i = 0;

          foreach ($this->column_search as $item) // loop column
          {
              if($_POST['search']['value']) // if datatable send POST for search
              {

                  if($i===0) // first loop
                  {
                      $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                      $this->db->like($item, $_POST['search']['value']);
                  }
                  else
                  {
                      $this->db->or_like($item, $_POST['search']['value']);
                  }

                  if(count($this->column_search) - 1 == $i) //last loop
                      $this->db->group_end(); //close bracket
              }
              $i++;
          }

          if(isset($_POST['order'])) // here order processing
          {
              $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
          }
          else if(isset($this->order))
          {
              $order = $this->order;
              $this->db->order_by(key($order), $order[key($order)]);
          }
      }
      //....................................................................................................................................


      function get_datatables()
      {
          $this->_get_datatables_query();
          if($_POST['length'] != -1)
          $this->db->limit($_POST['length'], $_POST['start']);
          $query = $this->db->get();
          return $query->result();
      }
      //....................................................................................................................................

      function count_filtered()
      {
          $this->_get_datatables_query();
          $query = $this->db->get();
          return $query->num_rows();
      }
      //....................................................................................................................................

      public function count_all()
      {
          $this->db->from($this->table);
          return $this->db->count_all_results();
      }
      //....................................................................................................................................

	public function viewdata()
	{
		$query = $this->db
			->select('*')
			->get('member');
		return $query->result();
	}
	//....................................................................................................................................

	public function add($data)
	{
		$user=$this->db
			->select('member_email')
			->where('member_email',$data['member_email'])
			->get('member');

		if($user->num_rows()>0){
			echo 'duplicate'; //repeated_member
			return false;
		}else{
			$insert = $this->db->insert('member', $data);
			//var_dump($insert);
			echo 'success'; //repeated_member
			return true;
		}

	}
	//....................................................................................................................................

	public function insert_facebook_member($data)// อันนี้สำหรับ Facebook
	{
		$insert = $this->db->insert('member', $data);
		return true;
	}
	//....................................................................................................................................

	public function edit($data)
	{
		$results = array('success'=>false, 'message'=>'เกิดข้อผิดพลาด');
		$member_id = $data['member_id'];
		$update = $this->db
			->where('member_id', $member_id)
			->update('member',$data);
		if( $this->db->affected_rows() ){
			$results = array('success'=>true, 'message'=>'บันทีกสำเร็จ');
		}
		return $results;
	}
	//....................................................................................................................................


	public function update_facebook_member($data)// อันนี้สำหรับ Facebook
	{
		$user=$this->db
			->select('member_email')
			->where('member_email',$data['member_email'])
			->get('member');

		if($user->num_rows()>0){
			$row=$user->row_array();
			$update = $this->db
				->where('member_email', $row['member_email'])
				->update('member',$data);
			//echo $this->db->last_query();
				return true;
		}
	}
	//....................................................................................................................................

	public function getdata($member_id = 0)
	{
		$result = array();
		if($member_id !== 0)
		{
			$query = $this->db
				->select('*')
				->where('member_id',$member_id)
				->get('member');
			$result = $query->row();
		}
		return $result;
	}
	//....................................................................................................................................


	public function get_member_by_email($member_email = "")
	{
		$result = array('success' => FALSE);
		if($member_email !== "")
		{
			$query = $this->db
				->select('*')
				->where('member_email',$member_email)
				->get('member');
			if( $query->num_rows() > 0 )
			{
				$row = $query->row();
				$result = array('success' => TRUE, 'member' => $row);
			}
		}
		return $result;
	}
	//....................................................................................................................................

  public function get_forgotpassword($member_id=0, $member_password = "")
	{
		$result = array('success' => FALSE, 'message' => 'รหัสผ่านเดิมไม่ถูกต้อง..');
		if($member_id !== 0)
		{
			$query = $this->db
				->select('member_password')
				->where('member_id',$member_id)
				->get('member');
			if( $query->num_rows() > 0 )
			{
				$result = array('success' => TRUE, 'message' => 'รหัสผ่านถูกต้อง...');
			}
		}
		return $result;
	}
	//....................................................................................................................................

  public function get_member_by_id($member_id = "")
	{
		$result = array();
		if($member_id !== 0)
		{
			$query = $this->db
				->select('*')
				->where('member_id',$member_id)
				->get('member');
			if( $query->num_rows() > 0 )
			{
				$result = $query->row();
			}
		}
		return $result;
	}
	//....................................................................................................................................

  public function set_member()
	{
    $results = array('success'=>false, 'message'=>'เกิดข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ...');

    $data = $this->security->xss_clean($this->input->post());
    $data = array(
      'member_id' => $data['member_id'],
      'member_password' => md5($data['new_password']),
      'updatedate' => date('Y-m-d H:i:s'),
    );

    $update = $this->db
      ->where('member_id', $data['member_id'] )
      ->update('member',$data);
			// echo $this->db->last_query();
    if($this->db->affected_rows()){
			$results = array('success'=>true, 'message'=>'แก้ไขข้อมูลเรียบร้อยแล้วค่ะ...');
		}

		return $results;
	}
	//....................................................................................................................................

	public function approved($data)
	{
		$member_id = $data['member_id'];
		$approved = $data['approved'];
		$update = $this->db
			->set('approved', $approved)
			->where('member_id', $member_id)
			->update('member');
		echo $this->db->queries[0];
		return true;
	}
	//....................................................................................................................................


	public function get_member_type()
	{
		$query = $this->db
			->select('member_type_id, member_type_name')
			->where('published !=', 0)
			->order_by('member_type_id', 'ASC')
			->get('member_type');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_fullname()
	{
		$query = $this->db
			->select('member_id, member_fname, member_lname')
			->order_by('member_id', 'DESC')
			->get('member');
		return $query->result();
	}
	//....................................................................................................................................

		public function get_fullname_id($id){

		$query = $this->db
			->select('member_id, member_fname, member_lname')
			->where('member_id', $id)
			->get('member');
		return $query->result();

	}
	//....................................................................................................................................

	public function delete($id)
	{
		$this->db->delete('member', array('member_id' => $id));
		return true;
	}
	//....................................................................................................................................

	////////////////////////////////////////***************////////////////////////////////////////
	//																					Frontend																				 //
	////////////////////////////////////////***************////////////////////////////////////////

	public function login(){

		$data=$this->security->xss_clean($this->input->post());

		$results  = array(
			'success'=>FALSE,
			'message'=>'เกิดข้อผิด'
		);


		$user=$this->db
			->select('member_id, member_type_id, member_email, member_password')
			->where('member_email',$data['member_email'])
			->where('member_password',md5($data['member_password']))
			->where('approved', '2')
			->get('member');
//		echo $this->db->queries[0];

		if($user->num_rows()>0){
//echo 'sssss';


			$user=$user->row_array();

			// Add user data in session
			$this->session->set_userdata('mID',$user['member_id']);
			$this->session->set_userdata('mUSER',$user['member_email']);
			$this->session->set_userdata('mTID',$user['member_type_id']);
			$this->input->set_cookie(array('name'=>'mID','value'=>$user['member_id'],'expire'=>(time()+60*60*24*30*365)));
			$this->input->set_cookie(array('name'=>'mUSER','value'=>$user['member_email'],'expire'=>(time()+60*60*24*30*365)));
			$this->input->set_cookie(array('name'=>'mTID','value'=>$user['member_type_id'],'expire'=>(time()+60*60*24*30*365)));

			$results = array(
				'success'=>TRUE,
				'message'=>'ท่านได้ทำการเข้าสู่ระบบเรียบร้อย..'
			);
		}
		return $results;
	}
	//....................................................................................................................................

	public function get_login_facebook(){

    $data=$this->security->xss_clean($this->input->post());
		$results  = array(
			'success'=>FALSE,
			'message'=>'Not found..'
		);


		if( $data['email'] === '' && empty($data['email']) ){
			$results = array(
				'success'=>FALSE,
				'message'=>'ไม่สามารถทำการ Login ได้เนื่องจากการเข้าถึงข้อมูลถูกปิดกั้น'
			);
		}else{

			$member_fname = $this->primaryclass->set_fields($data['first_name'] , NULL);
			$member_lname = $this->primaryclass->set_fields($data['last_name'] , NULL);
			$member_email = $this->primaryclass->set_fields($data['email'] , NULL);
			$member_profile = $this->primaryclass->set_fields($data['picture'] , NULL);
			$facebook_id = $this->primaryclass->set_fields($data['facebook_id'] , NULL);

			$data = array(
	        'member_fname' => $member_fname,
	        'member_lname' => $member_lname,
	        'member_type_id' => 3,
	        'member_email' => $member_email,
	        'member_profile' => $member_profile,
	        'ip' => $this->input->ip_address(),
	        'facebook_id' => $facebook_id,
	        'approved' => 2,
	        'createdate' => date('Y-m-d H:i:s'),
	    );

			$user=$this->db
				->select('member_id, member_type_id, member_email, facebook_id')
				->where('member_email =', $data['member_email'])
				->get('member');
			//echo $user->num_rows();

			if($user->num_rows()>0){
				//Update
				$this->update_facebook_member($data);

				$user=$user->row_array();
				$this->session->set_userdata('mID',$user['member_id']);
				$this->session->set_userdata('mUSER',$user['member_email']);
				$this->session->set_userdata('mFBID',$user['facebook_id']);
				$this->session->set_userdata('mTID',$user['member_type_id']);
				$this->input->set_cookie(array('name'=>'mID','value'=>$user['member_id'],'expire'=>(time()+60*60*24*30*365)));
				$this->input->set_cookie(array('name'=>'mUSER','value'=>$user['member_email'],'expire'=>(time()+60*60*24*30*365)));
				$this->input->set_cookie(array('name'=>'mFBID','value'=>$user['facebook_id'],'expire'=>(time()+60*60*24*30*365)));
				$this->input->set_cookie(array('name'=>'mTID','value'=>$user['member_type_id'],'expire'=>(time()+60*60*24*30*365)));
				//echo 'UPDATE';
				$results = array(
					'success'=>TRUE,
					'message'=>'ท่านได้ทำการเข้าสู่ระบบเรียบร้อย..'
				);

			}else{
				//INSERT
				$this->insert_facebook_member($data);

				$user=$user->row_array();
				$this->session->set_userdata('mID',$user['member_id']);
				$this->session->set_userdata('mUSER',$user['member_email']);
				$this->session->set_userdata('mFBID',$user['facebook_id']);
				$this->session->set_userdata('mTID',$user['member_type_id']);
				$this->input->set_cookie(array('name'=>'mID','value'=>$user['member_id'],'expire'=>(time()+60*60*24*30*365)));
				$this->input->set_cookie(array('name'=>'mUSER','value'=>$user['member_email'],'expire'=>(time()+60*60*24*30*365)));
				$this->input->set_cookie(array('name'=>'mFBID','value'=>$user['facebook_id'],'expire'=>(time()+60*60*24*30*365)));
				$this->input->set_cookie(array('name'=>'mTID','value'=>$user['member_type_id'],'expire'=>(time()+60*60*24*30*365)));
				//echo 'INSERT';
				$results = array(
					'success'=>TRUE,
					'message'=>'ท่านได้ทำการเข้าสู่ระบบเรียบร้อย..'
				);
			}
		}

		return $results;

	}
	//....................................................................................................................................


}
