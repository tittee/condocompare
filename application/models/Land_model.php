<?php

if (!defined('BASEPATH'))
		exit('No direct script access allowed');

class Land_model extends CI_Model
{

  var $table = 'land';
  var $column_order = array(null, 'land_id','pic_thumb','land_title','land_property_id','createdate','approved'); //set column field database for datatable orderable
  var $column_search = array('land_property_id','land_title','land_expert','land_description'); //s
  var $order = array('land_id' => 'desc'); // default order


  var $table_notification = 'land_share_form';
  var $column_order_notification = array(null,'land_share_form_id','fk_land_id', 'land_share_form_name','land_share_form_phone','land_share_form_email','land_share_form_message'); //set column
  var $column_search_notification = array('land_share_form_id','fk_land_id','land_share_form_name','land_share_form_phone','land_share_form_email','land_share_form_message'); //s
  var $order_notification = array('fk_land_id' => 'desc'); // default order

	function __construct()
	{
			parent::__construct();
	}
	//....................................................................................................................................

	public function get_land()
	{
			$query = $this->db
					->select('
			land.land_id,
			land.land_title,
			land.property_type_id,
			land.type_zone_id,
			land.zone_id,
			land.pic_thumb,
			land.createdate,
			land.modifydate,
			land.approved,
			land.visited,
			staff.fullname')
					->from('land')
					->join('member', 'member.member_id = land.member_id', 'left')
					->join('staff', 'staff.staff_id = land.staff_id', 'left')
					->order_by('land.createdate', 'DESC' )
					->get();
			return $query->result();
	}
	//....................................................................................................................................

  function get_notif_datatables()
  {
      $this->_get_datatables_notification();

      if(isset($_POST['length']) && $_POST['length'] != -1){
        $this->db->limit($_POST['length'], $_POST['start']);
      }
      $query = $this->db->get();
      // echo $this->db->last_query();
      // var_dump($query->result());
      return $query->result();
  }
  //....................................................................................................................................

  public function get_json_land()
  {
      // storing  request (ie, get/post) global array to a variable
      $requestData = $_REQUEST;
      // echo "<pre>";
      // print_r($_REQUEST);
      // echo "</pre>";

      $columns = array(
      // datatable column index  => database column name
          0 => 'land_id',
          1 => 'pic_thumb',
          2 => 'land_title',
          3 => 'land_property_id',
          4 => 'createdate',
          5 => 'approved',
      );

      $queryF1 = $this->db->get('land');
      $totalData = $queryF1->num_rows();
      $totalFiltered = $totalData;

      $query = $this->db
        ->select('
          land_id,
          member_id,
          land_property_id,
          staff_id,
          land_title,
          pic_thumb,
          createdate,
          modifydate,
          visited,
          type_status_id,
          approved');
          $query = $this->db->get('land');
      $query = $this->db->where('1=1');
      if( !empty($requestData['search']['value']) ) {
        $query = $this->db->like('land_title', $requestData['search']['value'], 'both');
        $query = $this->db->or_like('land_property_id', $requestData['search']['value'], 'both');
      }
      $requestData['start'] = (isset($requestData['start']))? $requestData['start'] : 0 ;
      $requestData['length'] = (isset($requestData['length']))? $requestData['length'] : 50 ;

      $query = $this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
      $query = $this->db->limit($requestData['start'], $requestData['length']);
      $query = $this->db->get('land');
      // $sqltext = $this->db->last_query();
      $totalFiltered = $query->num_rows();


      $data = array();
      while( $row=$query->unbuffered_row() )
      {
        $nestedData=array();
        foreach($row as $index=>$value) {
      		$nestedData[$index] = $value;
      	}
        $data[] = $nestedData;
      }
      //END WHILE
      // $row = $query->result();
      $requestData['draw'] = (isset($requestData['draw']))? $requestData['draw'] : 1 ;


      $json_data = array(
        'draw' => intval($requestData['draw']),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
        'recordsTotal' => intval($totalData),  // total number of records
        'recordsFiltered' => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
        'records' => $data,   // total data array
        // 'sql' => $sqltext,   // total data array
      );

      return $json_data;
  }
    //....................................................................................................................................


    private function _get_datatables_query()
    {

          $this->db->from($this->table);

          $i = 0;

          foreach ($this->column_search as $item) // loop column
          {
              if($_POST['search']['value']) // if datatable send POST for search
              {

                  if($i===0) // first loop
                  {
                      $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                      $this->db->like($item, $_POST['search']['value']);
                  }
                  else
                  {
                      $this->db->or_like($item, $_POST['search']['value']);
                  }

                  if(count($this->column_search) - 1 == $i) //last loop
                      $this->db->group_end(); //close bracket
              }
              $i++;
          }

          if(isset($_POST['order'])) // here order processing
          {
              $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
          }
          else if(isset($this->order))
          {
              $order = $this->order;
              $this->db->order_by(key($order), $order[key($order)]);
          }
      }
      //....................................................................................................................................

      private function _get_datatables_notification()
      {


            $this->db->from($this->table_notification);
            $this->db->where('createdate >= CURDATE()');

            $i = 0;

            foreach ($this->column_search_notification as $item) // loop column
            {


              if(isset($_POST['search']['value'])) // if datatable send POST for search
                {

                    if($i===0) // first loop
                    {
                        $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }

                    if(count($this->column_search_notification) - 1 == $i) //last loop
                        $this->db->group_end(); //close bracket
                }
                $i++;
            }

            if(isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($this->column_order_notification[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);

            }
            else if(isset($this->order_notification))
            {
                $order = $this->order_notification;
                $this->db->order_by(key($order), $order[key($order)]);

            }


        }
        //..................................................................................................................................

      function get_datatables()
      {
          $this->_get_datatables_query();
          if($_POST['length'] != -1)
          $this->db->limit($_POST['length'], $_POST['start']);
          $query = $this->db->get();
          return $query->result();
      }
      //....................................................................................................................................

      function count_filtered()
      {
          $this->_get_datatables_query();
          $query = $this->db->get();
          return $query->num_rows();
      }
      //....................................................................................................................................

      public function count_all()
      {
          $this->db->from($this->table);
          return $this->db->count_all_results();
      }
      //....................................................................................................................................

      public function count_filtered_notification()
      {
          $this->_get_datatables_notification();
          $query = $this->db->get();

          return $query->num_rows();
      }
      //....................................................................................................................................

      public function count_all_notification()
      {

          $this->db->from($this->table_notification);
          return $this->db->count_all_results();
      }
      //....................................................................................................................................

	public function land_all_record_count($keyword = null) {
		$result = 0;

		$query = $this->db
			->select('land_id,')
			->where('approved', '2');

		if( isset($keyword) && !empty($keyword['submit']) )
		{
			// STEP PROVINCES
			if( !empty($keyword['provinces_id'])  )
			{
				$query = $this->db
					->where('provinces_id', $keyword['provinces_id']);
			}
			if ( !empty($keyword['type_offering']) ) {
				$query = $this->db
					->where('type_offering_id', $keyword['type_offering']);
			}
			if ( !empty($keyword['min_price']) && !empty($keyword['max_price']) ) {
				$query = $this->db->where("land_total_price BETWEEN $keyword[min_price] AND $keyword[max_price] ", null, false);
			}
			if ( !empty($keyword['min_price']) && empty($keyword['max_price']) ) {
				$query = $this->db->where("land_total_price <=", $keyword['min_price']);
			}
			if ( empty($keyword['min_price']) && !empty($keyword['max_price']) ) {
				if ( $keyword['max_price'] >= '20000001' )
				{
					$query = $this->db->where("land_total_price >=", $keyword['max_price']);
				}
				else
				{
					$query = $this->db->where("land_total_price <=", $keyword['max_price']);
				}
			}


		}

		$query = $this->db->get('land');
		$result = $query->num_rows();

		return $result;
	}
	//....................................................................................................................................

	public function get_land_sidebar()
	{
		$result = array();
		$query = $this->db
			->select('
				cd.land_id,
				cd.type_zone_id,
				cd.zone_id,
				cd.land_title,
				cd.pic_thumb,
				cd.land_alley,
				cd.land_road,
				tz.type_zone_title
			')
			->join('type_zone tz', 'tz.type_zone_id = cd.type_zone_id' , 'left' )
			->where('cd.highlight', '2')
			->where('cd.approved', '2')
			->order_by('cd.land_id', 'DESC')
			->limit(6)
			->get('land cd');
		if( $query->num_rows() > 0 )
		{
			$result = $query->result();
		}
		return $result;
	}
	//....................................................................................................................................


		public function get_info($id)
		{
				$this->db->where('land_id', $id);
				$query = $this->db->get('land');
				return $query->row();
		}
	//....................................................................................................................................

		public function delete($land_id)
		{
				if ($this->db->delete("land_for_sale", "land_id = " . $land_id)) {
						return true;
				}
		}
	//....................................................................................................................................


	public function set_land($id = 0, $data = '')
	{
		if ($id === 0)
		{
			$query = $this->db->insert('land', $data);
			$land_id = $this->db->insert_id();

			if (sizeof($this->input->post('file_name_')) > 0) {
				foreach ($this->input->post('file_name_') as $row_img => $val_img) {
					$data_land_images = array(
						'land_id' => $land_id,
						'land_images_title' => $data['land_title'],
						'pic_large' => $val_img,
						'createdate' => $data['createdate'],
						'publish' => 2,
					);
					$insert_land_images = $this->add_data_land_images($data_land_images);
				}
			}
			//End

			$data_member_owner_land = array(
				'land_id' => $land_id,
				'member_id' => $data['member_id'],
			);
			$insert_member_owner_lando = $this->add_member_owner_land($data_member_owner_land);
			//End

			if (sizeof($this->input->post('transportation_category_id')) > 0) {
				foreach ($this->input->post('transportation_category_id') as $row_cate => $val_cate) {
					$transportation_distance = $this->input->post('transportation_distance');
					$data_land_relation_transportation = array(
						'land_id' => $land_id,
						'transportation_id' => $val_cate,
						'transportation_distance' => $transportation_distance[$row_cate],
					);
					$insert_land_relation_transportation = $this->add_land_relation_transportation($data_land_relation_transportation);
				}
			}
			//End
			return true;
		}
		else
		{
			$query = $this->db
				->where('land_id', $id)
				->update('land', $data);
			// echo $this->db->last_query();

			/* Update TO table : land_relation_transportation */
			if (sizeof($this->input->post('transportation_category_id')) > 0)
			{
				foreach ($this->input->post('transportation_category_id') as $row_cate => $val_cate)
				{
					$query = $this->db
						->select('transportation_id')
						->where('transportation_id', $val_cate)
						->where('land_id', $id)
						->get('land_relation_transportation');
					if( $query->num_rows() > 0 )
					{
						//echo 'update naja : ' .$val_cate. '\n';
						$transportation_distance = $this->input->post('transportation_distance');
						$data_land_relation_transportation = array(
							'land_id'=>$id,
							'transportation_id'=>$val_cate,
							'transportation_distance'=>$transportation_distance[$row_cate],
						);
						$this->db->where('land_id', $id);
						$this->db->where('transportation_id', $val_cate);
						$this->db->update('land_relation_transportation', $data_land_relation_transportation);
					}
					else
					{
						//echo 'insert naja : ' .$val_cate. '\n';
						$transportation_distance = $this->input->post('transportation_distance');
						$data_land_relation_transportation = array(
							'land_id'=>$id,
							'transportation_id'=>$val_cate,
							'transportation_distance'=>$transportation_distance[$row_cate],
						);
						$this->db->insert('land_relation_transportation', $data_land_relation_transportation);
					}
				}
				/* END FOREACH */
			}
			return true;
			/* END IF */
		}
	}
	//....................................................................................................................................

	public function set_land_by_id($data)
	{
		$result = array();
		$update = $this->db
			->where('land_id', $data['land_id'])
			->update('land',$data);

		return true;
	}
	//...................................................................................................................................

	public function add($data)
	{
		$insert = $this->db->insert('land', $data_land);
		$land_id = $this->db->insert_id();
		if (sizeof($data['file_name_']) > 0) {
				foreach ($data['file_name_'] as $row_img => $val_img) {
						$data_land_images = array(
								'land_id' => $land_id,
								'land_images_title' => $data['land_title'],
								'pic_large' => $val_img,
								'createdate' => $data['createdate'],
								'publish' => $data['publish'],
						);
						$insert_land_images = $this->add_data_land_images($data_land_images);
				}
		}
		$data_member_owner_land = array(
				'land_id' => $land_id,
				'member_id' => $data['member_id'],
		);
		$insert_member_owner_lando = $this->add_member_owner_land($data_member_owner_land);
		foreach ($data['transportation_category_id'] as $row_cate => $val_cate) {
				$transportation_distance = $data['transportation_distance'];
				$data_land_relation_transportation = array(
						'land_id' => $land_id,
						'transportation_id' => $val_cate,
						'transportation_distance' => $transportation_distance[$row_cate],
				);
				$insert_land_relation_transportation = $this->add_land_relation_transportation($data_land_relation_transportation);
		}
	}
	//....................................................................................................................................

		public function add_data_land_images($data)
		{
				$insert = $this->db->insert('land_images', $data);
				return true;
		}



		public function add_land_relation_transportation($data)
		{
				$insert = $this->db->insert('land_relation_transportation', $data);
				return true;
		}

		public function edit($data)
		{
				$id = $data['land_id'];
				$data_land = [
						'land_id' => $id,
						'land_property_id' => $this->input->post('land_property_id'),
						'member_id' => $this->input->post('member_id'),
						'staff_id' => $this->input->post('staff_id'),
						'land_title' => $this->input->post('land_title'),
						'land_owner_name' => $this->input->post('land_owner_name'),
						'land_holder_name' => $this->input->post('land_holder_name'),
						'property_type_id' => $this->input->post('property_type_id'),
						'type_zone_id' => $this->input->post('type_zone_id'),
						'zone_id' => $this->input->post('zone_id'),
						'land_expert' => trim($this->input->post('land_expert')),
						'land_description' => trim($this->input->post('land_description')),
						'pic_thumb' => $data['pic_thumb'],
						'pic_large' => $data['pic_large'],
						'land_no' => $this->input->post('land_no'),
						'land_alley' => $this->input->post('land_alley'),
						'land_road' => $this->input->post('land_road'),
						'land_address' => trim($this->input->post('land_address')),
						'provinces_id' => $this->input->post('provinces_id'),
						'districts_id' => $this->input->post('districts_id'),
						'sub_districts_id' => $this->input->post('sub_districts_id'),
						'land_zipcode' => $this->input->post('land_zipcode'),
						// 'land_size' => $this->input->post('land_size'),
						'land_total_price' => $this->input->post('land_total_price'),
						'land_price_per_square_meter' => $this->input->post('land_price_per_square_meter'),
						'land_price_per_square_mortgage' => $this->input->post('land_price_per_square_mortgage'),
						'land_googlemaps' => trim($this->input->post('land_googlemaps')),
						'latitude' => trim($this->input->post('latitude')),
						'longitude' => trim($this->input->post('longitude')),
						'land_tag' => $this->input->post('land_tag'),
						'createdate' => date("Y-m-d H:i:s"), 'visited' => 0,
						'type_status_id' => 0,
						'newsarrival' => $this->input->post('newsarrival'),
						'highlight' => $this->input->post('highlight'),
						'approved' => $this->input->post('approved'),
						'type_offering_id' => $this->input->post('type_offering_id')
				];
				$update = $this->db->where('land_id', $id)->update('land', $data_land);
				$data_member_owner_land = array(
						'land_id' => $id,
						'member_id' => $data['member_id'],
				);
				$this->edit_member_owner_land($data_member_owner_land);
				/* Update TO table : land_relation_transportation */
				foreach ($data['transportation_category_id'] as $row_cate => $val_cate) {
						$transportation_distance = $data['transportation_distance'];
						$data_land_relation_transportation = array(
								'land_id' => $id,
								'transportation_id' => $val_cate,
								'transportation_distance' => $transportation_distance[$row_cate],
						);
						$update_land_relation_transportation = $this->edit_land_relation_transportation($data_land_relation_transportation);
				}
				/* END FOREACH */
				return true;
		}
	//....................................................................................................................................

	public function delete_record($field_id, $id, $f_table)
	{
		$this->db->delete($f_table, array($field_id => $id));
		return true;
	}
	//....................................................................................................................................

	public function edit_land_relation_transportation($data)
	{
		$update = $this->db
			->where('land_id', $data['land_id'])
			->where('transportation_id', $data['transportation_id'])
			->update('land_relation_transportation', $data);
		return true;
	}
	//....................................................................................................................................

	public function get_transportation_relation_row_by_land($id = 0)
	{
		$result = array();
		$query = $this->db
			->select('
				crt.land_relation_transportation_id,
				crt.land_id,
				crt.transportation_id,
				crt.transportation_distance,
				t.transportation_title,
				tc.transportation_category_id,
				tc.transportation_category_title,
				')
			->from('land_relation_transportation crt')
			->join('transportation t', 't.transportation_id = crt.transportation_id', 'left')
			->join('transportation_category tc', 'tc.transportation_category_id = t.transportation_category_id', 'left')
			->where('crt.land_id', $id)
			->order_by('tc.transportation_category_id', 'ASC')
			->get();
		// echo $this->db->last_query();
		if( $query->num_rows() > 0 )
		{
			$result = $query->result();
		}
		return $result;
	}
	//....................................................................................................................................

	/* IMAGE */
	public function get_land_image($land_id)
	{
		$result = array();
		$query = $this->db
			->select('
				land_images_id,
				land_id,
				land_images_title,
				land_images_description,
				pic_large,
				land_images_url,
				createdate,
				updatedate,
				publish
			')
			->where('land_id', $land_id)
			->get('land_images');
		if( $query->num_rows() > 0 )
		{
			$result = $query->result();
		}
		return $result;
	}
	//....................................................................................................................................

	public function get_land_image_by_id($land_id = 0, $id = 0)
	{
		$result = array();
		if ($id === 0)
		{
			$query = $this->db
				->select('
					land_images_id,
					land_id,
					land_images_title,
					land_images_description,
					pic_thumb,
					pic_large,
					land_images_url,
					createdate,
					updatedate,
					publish
				')
				->where('land_id', $land_id)
				->get('land_images');
			if( $query->num_rows() > 0 )
			{
				$result = $query->result();
			}
			return $result;
		}
	}
	//....................................................................................................................................

	public function set_image_land($land_images_id = 0, $data)
	{
		if ($land_images_id === 0)
		{
			$query = $this->db->insert('land_images', $data);
			return $query;
		}
		else
		{
			$this->db->where('land_images_id', $land_images_id);
			$query = $this->db->update('land_images', $data);
			// echo $this->db->last_query(0);
			return $query;
		}
	}
	//....................................................................................................................................

	public function check_member_share($land_id, $member_id)
	{
		$query = $this->db
			->select('
				land_id
				member_id,
			')
			->where('land_id', $land_id)
			->where('member_id', $member_id)
			->get('member_share_land');
		return $query->num_rows();
	}
	//....................................................................................................................................

	public function add_member_owner_land($data)
	{
		$insert = $this->db->insert('member_owner_land', $data);
		return true;
	}
	//....................................................................................................................................

	public function edit_member_owner_land($data)
	{
		//ต้องใช้ ID ของตารางนั้นๆ
		$update = $this->db
			->where('land_id', $data['land_id'])
			->where('member_id', $data['member_id'])
			->update('member_owner_land',$data);
		return true;
	}
	//....................................................................................................................................

	public function add_member_share_land($data)
	{
		// $this->primaryclass->pre_var_dump($data);
    $results = array('success'=>false, 'message'=>'เกิดข้อผิดพลาด');
		$query = $this->db
			->select('
				land_id
				member_id,
			')
			->where('land_id', $data['land_id'])
			->where('member_id', $data['member_id'])
			->get('member_share_land');
		$sql_query =  $this->db->last_query();
		if ($query->num_rows() > 0)
		{
			//UPDATE
			$update = $this->db
				->set('fk_type_status_id', $data['fk_type_status_id'])
				->set('updatedate', date("Y-m-d H:i:s"))
				->where('land_id', $data['land_id'])
				->where('member_id', $data['member_id'])
				->update('member_share_land',$data);
			$results = array('success'=>true, 'message'=>'ท่านได้ทำการแชร์เรียบร้อยแล้วค่ะ....', 'query' => $sql_query );
		}
		else
		{
			//INSERT
			$insert = $this->db->insert('member_share_land', $data);
			$results = array('success'=>true, 'message'=>'ท่านได้ทำการแชร์เรียบร้อยแล้วค่ะ....');
		}

		return $results;
	}
	//....................................................................................................................................

		public function insert_land_share_form($data)
	{
		$query = $this->db
			->select('
				fk_land_id,
				land_share_form_email')
			->where('fk_land_id', $data['fk_land_id'])
			->where('land_share_form_email', $data['land_share_form_email'])
			->get('land_share_form');
		if ($query->num_rows() > 0)
		{
			//UPDATE เคยติดต่อมาแล้ว
      $update = $this->db->where('fk_land_id', $data['fk_land_id']);
      $update = $this->db->where('land_share_form_email', $data['land_share_form_email']);
      $update = $this->db->update('land_share_form', $data);
			$update_status = $this->update_land_by_status_contact($data['fk_land_id']);
			echo 'success';
			return true;
		}
		else
		{
			//INSERT รายการติดต่อใหม่
			$insert = $this->db->insert('land_share_form', $data);

			$update_status = $this->update_land_by_status_contact($data['fk_land_id']);
			echo 'success';
			return true;
		}
	}
	//....................................................................................................................................

	public function update_land_by_status_contact($id)
	{
		$update = $this->db
			->set('type_status_id', 2)
			->where('land_id', $id)
			->update('land');
	}
	//....................................................................................................................................

		public function edit_member_share_land($data)
		{
			$update = $this->db
				->where('land_id', $data['land_id'])
				->where('member_id', $data['member_id'])
				->update('member_share_land',$data);
			return true;
		}
		//....................................................................................................................................

	/************ *********** ************/
	/************ SHARE LAND ************/
	//....................................................................................................................................
	public function get_share_land($id)
	{
		$results = array();
		$query = $this->db
			->select('
				land.land_id,
				land.land_title,
				member_share_land.member_share_land_id,
				member_share_land.member_id,
				member_share_land.fk_type_status_id,
				member_share_land.createdate
			')
			->from('land')
			->join('member_share_land', 'member_share_land.land_id = land.land_id' , 'right' )
			->where('land.land_id', $id)
			->get();
		//echo $this->db->last_query();
		if ($query->num_rows() > 0)
		{
			return $results = $query->result();
		}
		return $results;
	}
	//....................................................................................................................................

	public function get_all_land_by_member($m_id)
	{
		$query = $this->db
			->select('
				land.land_id,
				land.member_id,
				land.land_title,
				land.pic_thumb,
				land.createdate,
				land.modifydate,
				land.visited,
				land.publish,
				land.approved,
			')
			->where('land.member_id', $m_id)
			->get('land');
		return $query->result();
	}
	//....................................................................................................................................

	/************ *********** ************/
	/******** MEMBER SHARE land *********/
	//....................................................................................................................................
	public function get_list_member_share_land($id)
	{
		$query = $this->db
			->select('
				land.land_id,
				land.zone_id,
				land.land_title,
				land.pic_thumb,
				land.land_total_price,
				land.land_price_per_square_meter,
				land.approved,
				member_share_land.member_share_land_id,
				member_share_land.land_id,
				member_share_land.member_id,
				member_share_land.fk_type_status_id,
				member_share_land.createdate,
				member_share_land.sharedate,
				member_share_land.updatedate,
			')
			->from('land')
			->join('member_share_land', 'member_share_land.land_id = land.land_id' , 'left' )
			->where('land.approved', '2')
			->where('member_share_land.member_id', $id)
			->group_by("member_share_land.land_id")
			->group_by("member_share_land.member_id")
			->get();
		//echo $this->db->queries[0];
		return $query->result();
	}
	//....................................................................................................................................

	public function update_sharedate($data)
	{
		$update = $this->db
			->where('member_share_land_id', $data['member_share_land_id'])
			->update('member_share_land', $data);
		echo 'success';
		return true;
	}
	//....................................................................................................................................


	/* GET LIST CONTACT BY LAND */
	public function get_land_contact_list($land_id, $member_id)
	{
		$result  = array();
		$query = $this->db
			->select('
				csf.land_share_form_id,
				csf.fk_land_id,
				csf.fk_member_id,
				csf.land_share_form_name,
				csf.land_share_form_phone,
				csf.land_share_form_email,
				csf.land_share_form_message,
				csf.createdate,
				csf.updatedate,
				csf.land_share_form_status,
				csf.fk_type_status_id,
			')
			->where('fk_land_id', $land_id)
			->where('fk_member_id', $member_id)
			->get('land_share_form csf');
//		echo $this->db->queries[0];
		if ($query->num_rows() > 0)
		{
			return $result = $query->result();
		}
		else
		{
			return $result;
		}
	}
	//....................................................................................................................................

	public function get_land_contact_by_id($land_id = 0, $member_id = 0)
	{
		$result  = array();
		if( $land_id !== 0 && $member_id !== 0)
		{
			$query = $this->db
				->select('
					csf.land_share_form_id,
					csf.fk_land_id,
					csf.fk_member_id,
					csf.land_share_form_name,
					csf.land_share_form_phone,
					csf.land_share_form_email,
					csf.land_share_form_message,
					csf.createdate,
					csf.updatedate,
					m.member_fname,
					m.member_lname,
					m.member_email,
					m.member_mobileno,
				')
				->join('member m', 'm.member_id = csf.fk_member_id'  )
				->where('csf.fk_land_id', $land_id)
				->where('csf.fk_member_id', $member_id)
				->get('land_share_form csf');
	//		echo $this->db->queries[0];
			if ($query->num_rows() > 0)
			{
				return $result = $query->result_array();
			}
			else
			{
				return $result;
			}
		}
		else
		{
			return $result;
		}

	}
	//....................................................................................................................................

	/* UPDATE STATUS CONTACT FROM MEMBER */
	public function update_land_share_form_status($data)
	{
		$update = $this->db
			->where('land_share_form_id', $data['land_share_form_id'])
			->update('land_share_form', $data);
		echo 'success';
		return true;
	}
	//....................................................................................................................................

	public function get_member_owner_land($land_id = 0)
	{
		$result = array();
		if( $land_id !== 0)
		{
			$query = $this->db
				->select('
					c.land_id,
					c.land_property_id,
					c.member_id,
					m.member_fname,
					m.member_lname,
					m.member_email,
					m.member_mobileno,
				')
				->join('member m', 'm.member_id = c.member_id'  )
				->where('c.land_id', $land_id)
				->get('land c');
			if ($query->num_rows() > 0)
			{
				return $result = $query->result_array();
			}
			else
			{
				return $result;
			}
		}
		else
		{
			return $result;
		}
	}
	//....................................................................................................................................


	/************ *********** ************/
	/********* SHARE LAND FORM **********/
	//....................................................................................................................................
	public function get_land_share_form($id)
	{
		$query = $this->db
			->select('
				land_share_form.fk_land_id,
				land_share_form.fk_member_id,
				land_share_form.createdate,


				member_share_land.member_share_land_id,
				member_share_land.land_id,
				member_share_land.member_id,
				member_share_land.fk_type_status_id,
				member_share_land.createdate,
				member_share_land.sharedate,
				member_share_land.updatedate,

				land.land_id,
				land.zone_id,
				land.land_title,
				land.pic_thumb,
				land.approved,


			')
			->from('land_share_form')
			->join('land', 'land_share_form.fk_land_id = land.land_id' , 'left' )
			->join('member_share_land', 'land_share_form.fk_member_id = member_share_land.member_id' , 'left' )
			->where('land_share_form.fk_member_id', $id)
			->where('member_share_land.fk_type_status_id', 2)
			->where('land.approved', '2')
			->group_by("land_share_form.fk_land_id")
			->get();
		// echo $this->db->last_query();
		return $query->result();
	}
	//....................................................................................................................................

	public function get_land_share_form_by_id($id = 0)
	{
		$query = $this->db
			->select('
				land_share_form.fk_land_id,
				land_share_form.fk_member_id,
				land_share_form.createdate,

				member_share_land.member_share_land_id,
				member_share_land.land_id,
				member_share_land.member_id,
				member_share_land.fk_type_status_id,
				member_share_land.createdate,
				member_share_land.sharedate,
				member_share_land.updatedate,

				land.land_id,
				land.zone_id,
				land.land_title,
				land.pic_thumb,
				land.approved,


			')
			->from('land_share_form')
			->join('land', 'land_share_form.fk_land_id = land.land_id' , 'left' )
			->join('member_share_land', 'land_share_form.fk_member_id = member_share_land.member_id' , 'left' )
			->where('land_share_form.fk_member_id', $id)
			->where('member_share_land.fk_type_status_id', 2)
			->where('land.approved', '2')
			->group_by("land_share_form.fk_land_id")
			->get();
//		echo $this->db->queries[0];
		return $query->result();
	}
	//....................................................................................................................................

}
