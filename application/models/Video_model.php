<?php
defined('BASEPATH') OR exit();
class Video_model extends CI_Model {
	//....................................................................................................................................

	function __construct(){
		parent::__construct();
	}
	//....................................................................................................................................\

	public function get_video()
	{
		$query = $this->db
			->select('
				video_id,
				video_title,
				video_pic_thumb,
				highlight,
				recommended,
				createdate,
				updatedate,
				publish,
        visited,
				video_order
			')
			->get('video');
		return $query->result();
	}
	//...................................................................................................................................

	public function get_video_publish($category_id)
	{
		$result = array();
		$query = $this->db
			->select('
				video_id,
				video_title,
				video_caption,
				video_pic_thumb,
				highlight,
				recommended,
				createdate,
				updatedate,
				publish,
        visited,
				video_order
			')
			->where('publish', 2)
			->get('video');
			if($query->num_rows() > 0 )
			{
        // echo $this->db->last_query();
				$result = $query->result();
			}
		return $result;
	}
	//...................................................................................................................................


	public function get_video_highlight()
	{
		$result = array();
		$query = $this->db
			->select('
				video_id,
				video_title,
				video_caption,
				video_pic_large,
				highlight,
				recommended,
				publish,
        visited,
				video_order
			')
			->where('publish', 2)
			->where('highlight', 2)
			->get('video');
			if($query->num_rows() > 0 )
			{
				$result = $query->result();
			}
		return $result;
	}
	//...................................................................................................................................

	public function get_video_mostread()
	{
		$result = array();
		$query = $this->db
			->select('
				video_id,
				video_title,
				visited,
				createdate,
				publish,
        visited,
				video_order
			')
			->where('publish', 2)
			->order_by('visited', 'desc')
			->get('video');
			if($query->num_rows() > 0 )
			{
				$result = $query->result();
			}
		return $result;
	}
	//...................................................................................................................................

	public function get_video_search($keyword = null)
	{
		$result = array();

		if( $keyword !== '')
		{
			$query = $this->db
				->select('
					video_id,
					video_title,
					video_caption,
					video_description,
					video_pic_thumb,
					highlight,
					recommended,
					createdate,
					updatedate,
					publish,
          visited,
					video_order
				')
				->where('publish', 2)
				->like('video_title', $keyword, 'both')
				->or_like('video_caption', $keyword, 'both')
				->or_like('video_description', $keyword, 'both')
				->get('video');
			if($query->num_rows() > 0 )
			{
				$result = $query->result();
			}
		}
		else
		{
			return $result = $this->get_video_category_publish();
		}

		return $result;
	}
	//...................................................................................................................................

	public function get_info($id)
	{
		$this->db->where('video_id',$id);
		$query=$this->db->get('video');
		return $query->row();
	}
	//....................................................................................................................................

	public function get_video_id($id)
	{
		$query = $this->db
			->select('*')
			->where('video_id', $id)
			->get('video');
		return $query->row();
	}
	//....................................................................................................................................

	public function get_video_record()
	{
		$this->db->where('publish', 2);
		$query = $this->db->get('video');
		return $query->num_rows();
	}
	//....................................................................................................................................

	public function insert_video($data)
	{
		$insert = $this->db->insert('video', $data);
		$video_id =  $this->db->insert_id();
	}
	//....................................................................................................................................


	public function insert_taxonomy_video($data)
	{
		$insert = $this->db->insert('video_taxonomy', $data);
		return true;
	}
	//....................................................................................................................................

	public function update_video($data)
	{
		$id = $data['video_id'];
		$update = $this->db
			->where('video_id', $id)
			->update('video',$data);

		return $update;
	}
	//....................................................................................................................................

	public function update_taxonomy_video($data)
	{
		$update = $this->db
			->where('video_id', $data['video_id'])
			->update('video_taxonomy',$data);
		return true;
	}
	//....................................................................................................................................


	/* video Category */
	public function get_video_category_option()
	{
		$query = $this->db
			->select('video_category_id, video_category_name')
			->order_by('video_category_id', 'ASC')
			->get('video_category');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_video_category_record($id)
	{
		$this->db->where('video_category_id',$id);
		$query=$this->db->get('video_category');
		return $query->row();
	}
	//....................................................................................................................................

	public function insert_video_category_record($data)
	{
		$id = $data['video_category_id'];
		$update = $this->db
			->where('video_category_id', $id)
			->update('video_category',$data);
		return true;
	}
	//....................................................................................................................................

	public function get_video_category()
	{
		$query = $this->db
			->select('
				video_category_id,
				video_category_name,
				video_category_pic_thumb,
				createby,
				updateby,
				createdate,
				updatedate,
				publish,
				video_category_order
			')
			->get('video_category');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_video_category_publish()
	{
		$result  = array();
		$query = $this->db
			->select('
				video_category_id,
				video_category_name,
				video_category_pic_thumb,
				createdate,
				updatedate,
				publish,
				video_category_order
			')
			->where('publish', 2)
			->get('video_category');
			if($query->num_rows() > 0 )
			{
				$result = $query->result();
			}
		return $result;
	}
	//....................................................................................................................................

	public function insert_video_category($data)
	{
		$insert = $this->db->insert('video_category', $data);
		return true;
	}
	//....................................................................................................................................

	public function update_video_category($data)
	{
		$id = $data['video_category_id'];
		$update = $this->db
			->where('video_category_id', $id)
			->update('video_category',$data);
		return $update;
	}
	//....................................................................................................................................

	public function publish_record($field_id, $field_name, $id, $f_table, $p=1)
	{
		//$p = 0 : ไม่ระบบ | 1 : ไม่แสดงผล | 2 : แสดงผล
		$query = $this->db
			->set($field_name, $p)
			->set($field_name, $p)
			->where($field_id, $id)
			->update($f_table);
		//echo $query = $this->db->queries[0];
		return $query;
	}
	//....................................................................................................................................

	public function delete_record($field_id, $id, $f_table)
	{
		//$query = $this->db->delete($f_table, array($field_id => $id));

		$query =  $this->db
			->where($field_id, $id)
			->delete($f_table);
		//echo $query = $this->db->queries[0];
		return $query;
	}
	//....................................................................................................................................

}
