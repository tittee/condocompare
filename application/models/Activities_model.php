<?php
defined('BASEPATH') OR exit();
class Activities_model extends CI_Model {
	//....................................................................................................................................

	function __construct(){
		parent::__construct();
	}
	//....................................................................................................................................\

	public function get_activities()
	{
		$query = $this->db
			->select('
				activities_id,
				fk_activities_category_id,
				activities_title,
				activities_slug,
				activities_date,
				activities_days,
				activities_pic_thumb,
				highlight,
				recommended,
				createdate,
				updatedate,
				publish,
				activities_order
			')
			->get('activities');
		return $query->result();
	}
	//...................................................................................................................................

	public function get_activities_publish()
	{
		$result = array();
		$query = $this->db
			->select('
				activities_id,
				fk_activities_category_id,
				activities_title,
				activities_slug,
				activities_date,
				activities_days,
				activities_caption,
				activities_pic_thumb,
				highlight,
				recommended,
				createdate,
				updatedate,
				publish,
				activities_order
			')
			->where('publish', 2)
			->get('activities');
			if($query->num_rows() > 0 )
			{
				$result = $query->result();
			}
		return $result;
	}
	//...................................................................................................................................


	public function get_activities_highlight()
	{
		$result = array();
		$query = $this->db
			->select('
				activities_id,
				fk_activities_category_id,
				activities_title,
				activities_slug,
				activities_date,
				activities_days,
				activities_caption,
				activities_pic_thumb,
				highlight,
				recommended,
				publish,
				activities_order
			')
			->where('publish', 2)
			->where('highlight', 2)
			->order_by('createdate', 'DESC')
			->get('activities');
			if($query->num_rows() > 0 )
			{
				$result = $query->result();
			}
		return $result;
	}
	//...................................................................................................................................

	public function get_activities_mostread()
	{
		$result = array();
		$query = $this->db
			->select('
				activities_id,
				activities_title,
				activities_slug,
				activities_date,
				activities_days,
				visited,
				createdate,
				publish,
				activities_order
			')
			->where('publish', 2)
			->order_by('visited', 'desc')
			->get('activities');
			if($query->num_rows() > 0 )
			{
				$result = $query->result();
			}
		return $result;
	}
	//...................................................................................................................................

	public function get_activitieslist($limit, $start, $category_id = 0)
	{
		$result = array();

		// CLEAR DATA
		$data = $this->security->xss_clean($this->input->get());
		$keyword = (!empty($data['keyword'])) ? $data['keyword'] : '';

		$query = $this->db
			->select('
				activities_id,
				fk_activities_category_id,
				activities_title,
				activities_caption,
				activities_description,
				activities_pic_thumb,
				highlight,
				recommended,
				createdate,
				updatedate,
				publish,
        visited,
				activities_order
			');
			if( $category_id !== 0 ){
				$query = $this->db->where('fk_activities_category_id', $category_id);
			}
			if( $keyword !== '' ){
				$query = $this->db->like('activities_title', $keyword, 'both');
				$query = $this->db->or_like('activities_caption', $keyword, 'both');
				$query = $this->db->or_like('activities_description', $keyword, 'both');
			}

			$query = $this->db->where('publish', 2);
      $query = $this->db->order_by('activities_order', 'ASC');
			$query = $this->db->limit($limit, $start);
			$query = $this->db->get('activities');
			// echo $this->db->last_query();
			if($query->num_rows() > 0 )
			{
				$result = $query->result();
			}
		return $result;
	}
	//...................................................................................................................................


	public function get_activities_search($keyword = null)
	{
		$result = array();

		if( $keyword !== '')
		{
			$query = $this->db
				->select('
					activities_id,
					fk_activities_category_id,
					activities_title,
					activities_date,
					activities_days,
					activities_caption,
					activities_description,
					activities_pic_thumb,
					highlight,
					recommended,
					createdate,
					updatedate,
					publish,
					activities_order
				')
				->where('publish', 2)
				->like('activities_title', $keyword, 'both')
				->or_like('activities_caption', $keyword, 'both')
				->or_like('activities_description', $keyword, 'both')
				->get('activities');
			if($query->num_rows() > 0 )
			{
				$result = $query->result();
			}
		}
		else
		{
			return $result = $this->get_activities_category_publish();
		}

		return $result;
	}
	//...................................................................................................................................

	public function get_info($id)
	{
		$this->db->where('activities_id',$id);
		$query=$this->db->get('activities');
		return $query->row();
	}
	//....................................................................................................................................

	public function get_activities_id($id)
	{
		$query = $this->db
			->select('*')
			->where('activities_id', $id)
			->get('activities');
		return $query->row();
	}
	//....................................................................................................................................

	public function get_activities_record(  $category_id = 0 )
	{
		$total_rows = 0;
		$data = $this->security->xss_clean($this->input->get());
		$keyword = (!empty($data['keyword'])) ? $data['keyword'] : '';
		if( $category_id !== 0 ){
			$query = $this->db->where('fk_activities_category_id', $category_id);
		}
		if( $keyword !== '' ){
			$query = $this->db->like('activities_title', $keyword, 'both');
			$query = $this->db->or_like('activities_caption', $keyword, 'both');
			$query = $this->db->or_like('activities_description', $keyword, 'both');
		}
		$query = $this->db->where('publish', 2);
		$query = $this->db->get('activities');
		// echo $this->db->last_query();
		$total_rows = $query->num_rows();

		return $total_rows;
	}
	//....................................................................................................................................

	public function insert_activities($data)
	{
		$insert = $this->db->insert('activities', $data);
		$activities_id =  $this->db->insert_id();

		/* INSERT TO table : activities_taxonomy */
		if( isset($data['fk_activities_category_id']) && isset($data['fk_activities_subcategory_id']) )
		{
			$data_taxonomy = array(
				'activities_category_id'=>$data['fk_activities_category_id'],
				'activities_subcategory_id'=>$data['fk_activities_subcategory_id'],
				'activities_id'=>$activities_id
			);
			$insert_taxonomy = $this->insert_taxonomy_activities($data_taxonomy);
		}
		return true;
	}
	//....................................................................................................................................


	public function insert_taxonomy_activities($data)
	{
		$insert = $this->db->insert('activities_taxonomy', $data);
		return true;
	}
	//....................................................................................................................................

	public function update_activities($data)
	{
		$id = $data['activities_id'];
		$update = $this->db
			->where('activities_id', $id)
			->update('activities',$data);

		/* Update TO table : member_owner_activities */

		if( isset($data['fk_activities_category_id']) && isset($data['fk_activities_subcategory_id']) )
		{
			$data_taxonomy = array(
				'activities_category_id'=>$data['fk_activities_category_id'],
				'activities_subcategory_id'=>$data['fk_activities_subcategory_id'],
				'activities_id'=>$id
			);
			$update_taxonomy = $this->edit_relation_activities( $data_taxonomy);
		}
		return $update;
	}
	//....................................................................................................................................

	public function update_taxonomy_activities($data)
	{
		$update = $this->db
			->where('activities_id', $data['activities_id'])
			->update('activities_taxonomy',$data);
		return true;
	}
	//....................................................................................................................................


	/* activities Category */
	public function get_activities_category_option()
	{
		$query = $this->db
			->select('activities_category_id, activities_category_name')
			->order_by('activities_category_id', 'ASC')
			->get('activities_category');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_activities_category_record($id)
	{
		$this->db->where('activities_category_id',$id);
		$query=$this->db->get('activities_category');
		return $query->row();
	}
	//....................................................................................................................................

	public function insert_activities_category_record($data)
	{
		$id = $data['activities_category_id'];
		$update = $this->db
			->where('activities_category_id', $id)
			->update('activities_category',$data);
		return true;
	}
	//....................................................................................................................................

	public function get_activities_category()
	{
		$query = $this->db
			->select('
				activities_category_id,
				activities_category_name,
				activities_category_pic_thumb,
				createby,
				updateby,
				createdate,
				updatedate,
				publish,
				activities_category_order
			')
			->get('activities_category');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_activities_category_publish()
	{
		$result  = array();
		$query = $this->db
			->select('
				activities_category_id,
				activities_category_name,
				activities_category_pic_thumb,
				createdate,
				updatedate,
				publish,
				activities_category_order
			')
			->where('publish', 2)
			->get('activities_category');
			if($query->num_rows() > 0 )
			{
				$result = $query->result();
			}
		return $result;
	}
	//....................................................................................................................................

	public function insert_activities_category($data)
	{
		$insert = $this->db->insert('activities_category', $data);
		return true;
	}
	//....................................................................................................................................

	public function update_activities_category($data)
	{
		$id = $data['activities_category_id'];
		$update = $this->db
			->where('activities_category_id', $id)
			->update('activities_category',$data);
		return $update;
	}
	//....................................................................................................................................

	public function publish_record($field_id, $field_name, $id, $f_table, $p=1)
	{
		//$p = 0 : ไม่ระบบ | 1 : ไม่แสดงผล | 2 : แสดงผล
		$query = $this->db
			->set($field_name, $p)
			->set($field_name, $p)
			->where($field_id, $id)
			->update($f_table);
		//echo $query = $this->db->queries[0];
		return $query;
	}
	//....................................................................................................................................

	public function delete_record($field_id, $id, $f_table)
	{
		//$query = $this->db->delete($f_table, array($field_id => $id));

		$query =  $this->db
			->where($field_id, $id)
			->delete($f_table);
		//echo $query = $this->db->queries[0];
		return $query;
	}
	//....................................................................................................................................

	/* activities Subcategory */
	public function get_activities_subcategory_option()
	{
		$query = $this->db
			->select('activities_subcategory_id, activities_subcategory_name')
			->order_by('activities_subcategory_id', 'ASC')
			->get('activities_subcategory');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_activities_subcategory_record($id)
	{
		$this->db->where('activities_subcategory_id',$id);
		$query=$this->db->get('activities_subcategory');
		return $query->row();
	}
	//....................................................................................................................................

	public function insert_activities_subcategory_record($data)
	{
		$id = $data['activities_subcategory_id'];
		$update = $this->db
			->where('activities_subcategory_id', $id)
			->update('activities_subcategory',$data);
		return true;
	}
	//....................................................................................................................................

	public function get_activities_subcategory()
	{
		$query = $this->db
			->select('
				activities_subcategory_id,
				fk_activities_category_id,
				activities_subcategory_name,
				activities_subcategory_expert,
				activities_subcategory_description,
				activities_subcategory_pic_thumb,
				createby,
				updateby,
				createdate,
				updatedate,
				publish,
				activities_subcategory_order
			')
			->get('activities_subcategory');
		return $query->result();
	}
	//....................................................................................................................................

	public function insert_activities_subcategory($data)
	{
		$insert = $this->db->insert('activities_subcategory', $data);
		return true;
	}
	//....................................................................................................................................

	public function update_activities_subcategory($data)
	{
		$id = $data['activities_subcategory_id'];
		$update = $this->db
			->where('activities_subcategory_id', $id)
			->update('activities_subcategory',$data);
		return true;
	}
	//....................................................................................................................................

}
