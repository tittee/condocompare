<?php

defined('BASEPATH') or exit();
class Condominium_model extends CI_Model
{
    //....................................................................................................................................
    var $table = 'condo';
    var $column_order = array(null, 'condo_id','pic_thumb','condo_title','condo_property_id','createdate','approved'); //set column field database for datatable orderable
    var $column_search = array('condo_property_id','condo_title','condo_expert','condo_description'); //s
    var $order = array('condo_id' => 'desc'); // default order

    var $table_notification = 'condo_share_form';
    var $column_order_notification = array(null,'condo_share_form_id','fk_condo_id', 'condo_share_form_name','condo_share_form_phone','condo_share_form_email','condo_share_form_message'); //set column
    var $column_search_notification = array('condo_share_form_id','fk_condo_id','condo_share_form_name','condo_share_form_phone','condo_share_form_email','condo_share_form_message'); //s
    var $order_notification = array('fk_condo_id' => 'desc'); // default order

    public function __construct()
    {
        parent::__construct();
    }
    //....................................................................................................................................\

    public function get_condo()
    {
        $query = $this->db
            ->select('
				condo.condo_id,
				condo.member_id,
				condo.condo_property_id,
				condo.staff_id,
				condo.condo_title,
				condo.pic_thumb,
				condo.createdate,
				condo.modifydate,
				condo.visited,
				condo.type_status_id,
				condo.approved,
				member.member_fname,
				member.member_lname,
				staff.fullname
			')
            ->from('condo')
            ->join('member', 'member.member_id = condo.member_id', 'left')
            ->join('staff', 'staff.staff_id = condo.staff_id', 'left')
            ->order_by('condo.createdate', 'DESC')
            ->get();

        return $query->result();
    }
    //....................................................................................................................................



    private function _get_datatables_query()
    {

          $this->db->from($this->table);

          $i = 0;

          foreach ($this->column_search as $item) // loop column
          {
              if($_POST['search']['value']) // if datatable send POST for search
              {

                  if($i===0) // first loop
                  {
                      $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                      $this->db->like($item, $_POST['search']['value']);
                  }
                  else
                  {
                      $this->db->or_like($item, $_POST['search']['value']);
                  }

                  if(count($this->column_search) - 1 == $i) //last loop
                      $this->db->group_end(); //close bracket
              }
              $i++;
          }

          if(isset($_POST['order'])) // here order processing
          {
              $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
          }
          else if(isset($this->order))
          {
              $order = $this->order;
              $this->db->order_by(key($order), $order[key($order)]);
          }
      }
      //....................................................................................................................................

      private function _get_datatables_notification()
      {


            $this->db->from($this->table_notification);
            $this->db->where('createdate >= CURDATE()');

            $i = 0;

            foreach ($this->column_search_notification as $item) // loop column
            {


              if(isset($_POST['search']['value'])) // if datatable send POST for search
                {

                    if($i===0) // first loop
                    {
                        $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }

                    if(count($this->column_search_notification) - 1 == $i) //last loop
                        $this->db->group_end(); //close bracket
                }
                $i++;
            }

            if(isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($this->column_order_notification[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);

            }
            else if(isset($this->order_notification))
            {
                $order = $this->order_notification;
                $this->db->order_by(key($order), $order[key($order)]);

            }


        }
        //....................................................................................................................................


      function get_datatables()
      {
          $this->_get_datatables_query();
          if($_POST['length'] != -1)
          $this->db->limit($_POST['length'], $_POST['start']);
          $query = $this->db->get();
          return $query->result();
      }
      //....................................................................................................................................

      function get_notif_datatables()
      {
          $this->_get_datatables_notification();

          if(isset($_POST['length']) && $_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);
          }

          $query = $this->db->get();
          // echo $this->db->last_query();
          return $query->result();
      }
      //....................................................................................................................................

      function count_filtered()
      {
          $this->_get_datatables_query();
          $query = $this->db->get();
          return $query->num_rows();
      }
      //....................................................................................................................................

      public function count_all()
      {
          $this->db->from($this->table);
          return $this->db->count_all_results();
      }
      //....................................................................................................................................

      public function count_filtered_notification()
      {
          $this->_get_datatables_notification();
          $query = $this->db->get();

          return $query->num_rows();
      }
      //....................................................................................................................................

      public function count_all_notification()
      {

          $this->db->from($this->table_notification);
          return $this->db->count_all_results();
      }
      //....................................................................................................................................

  public function get_condo_highlight()
  {
      $result = array();

      $query = $this->db
      ->select('
        condo.condo_id,
        condo.member_id,
        condo.staff_id,
        condo.condo_title,
        condo.pic_thumb,
        condo.createdate,
        condo.modifydate,
        condo.visited,
        condo.type_status_id,
        condo.approved,
        member.member_fname,
        member.member_lname,
        staff.fullname
      ')
      ->from('condo')
      ->join('member', 'member.member_id = condo.member_id', 'left')
      ->join('staff', 'staff.staff_id = condo.staff_id', 'left')
      ->where('condo.highlight', 2)
      ->get();
      // echo $this->db->last_query();
      if ($query->num_rows >= 0) {
          $result = $query->result();
      }

      return $result;
  }
  //....................................................................................................................................

    public function add($data)
    {
        $this->load->library('primaryclass');
        $new_condo_description = $data['condo_description'];
    // $new_condo_description = $this->primaryclass->encode_htmlspecialchars($data['condo_description']);
        $data_condo = array(
            'condo_property_id' => $data['condo_property_id'],
            'member_id' => $data['member_id'],
            'staff_id' => $data['staff_id'],
            'condo_title' => $data['condo_title'],
            'condo_owner_name' => $data['condo_owner_name'],
            'condo_holder_name' => $data['condo_holder_name'],
            'condo_builing_finish' => $data['condo_builing_finish'],
            'property_type_id' => $data['property_type_id'],
            'type_zone_id' => $data['type_zone_id'],
            'zone_id' => $data['zone_id'],
            'type_offering_id' => $data['type_offering_id'],
            'type_suites_id' => $data['type_suites_id'],
            'type_decoration_id' => $data['type_decoration_id'],
            'condo_all_room' => $data['condo_all_room'],
            'condo_all_toilet' => $data['condo_all_toilet'],
            'condo_interior_room' => $data['condo_interior_room'],
            'condo_direction_room' => $data['condo_direction_room'],
            'condo_scenery_room' => $data['condo_scenery_room'],
            'condo_living_area' => $data['condo_living_area'],
            'condo_electricity' => $data['condo_electricity'],
            'condo_expert' => trim($data['condo_expert']),
            'condo_description' => $new_condo_description,
            'pic_thumb' => $data['pic_thumb'],
            'pic_large' => $data['pic_large'],
            'condo_no' => $data['condo_no'],
            'condo_floor' => $data['condo_floor'],
            'condo_area_apartment' => $data['condo_area_apartment'],
            'condo_alley' => trim($data['condo_alley']),
            'condo_road' => trim($data['condo_road']),
            'provinces_id' => $data['provinces_id'],
            'districts_id' => $data['districts_id'],
            'sub_districts_id' => $data['sub_districts_id'],
            'condo_zipcode' => $data['condo_zipcode'],
            'condo_total_price' => $data['condo_total_price'],
            'condo_price_per_square_meter' => $data['condo_price_per_square_meter'],
            'condo_googlemaps' => $data['condo_googlemaps'],
            'latitude' => trim($data['latitude']),
            'longitude' => trim($data['longitude']),
            'condo_tag' => $data['condo_tag'],
            'createdate' => $data['createdate'],
            'type_status_id' => trim($data['type_status_id']),
            'visited' => trim($data['visited']),
            'newsarrival' => $data['newsarrival'],
            'highlight' => $data['highlight'],
//			'publish'=>$data['publish'],
            'approved' => $data['approved'],
        );

        $insert = $this->db->insert('condo', $data_condo);
        echo $this->db->last_query();
        $condo_id = $this->db->insert_id();

        /* INSERT TO table : condo_images */
        if (!empty($data['file_name_'])) {
            foreach ($data['file_name_'] as $row_img => $val_img) {
                $data_condo_images = array(
                    'condo_id' => $condo_id,
                    'condo_images_title' => $data['condo_title'],
                    'pic_large' => $val_img,
                    'createdate' => $data['createdate'],
                    'publish' => 2,
                );
                $insert_condo_images = $this->add_data_condo_images($data_condo_images);
            }
        }

        /* INSERT TO table : member_owner_condo */
        $data_member_owner_condo = array(
            'condo_id' => $condo_id,
            'member_id' => $data['member_id'],
        );
        $insert_member_owner_condo = $this->add_member_owner_condo($data_member_owner_condo);

        /* INSERT TO table : condo_relation_facilities */
        foreach ($data['facilities_id'] as $row_faci => $val_faci) {
            $data_condo_relation_facilities = array(
                'condo_id' => $condo_id,
                'facilities_id' => $val_faci,
            );
            $insert_condo_relation_facilities = $this->add_condo_relation_facilities($data_condo_relation_facilities);
        }

        /* INSERT TO table : condo_relation_featured */
        foreach ($data['featured_id'] as $row_feat => $val_feat) {
            $data_condo_relation_featured = array(
                'condo_id' => $condo_id,
                'featured_id' => $val_feat,
            );
            $insert_condo_relation_featured = $this->add_condo_relation_featured($data_condo_relation_featured);
        }

        /* INSERT TO table : condo_relation_transportation */
        foreach ($data['transportation_category_id'] as $row_cate => $val_cate) {
            if ($val_cate !== '0') {
                $transportation_distance = $data['transportation_distance'];
                $data_condo_relation_transportation = array(
                    'condo_id' => $condo_id,
                    'transportation_id' => $val_cate,
                    'transportation_distance' => $transportation_distance[$row_cate],
                );
                $insert_condo_relation_transportation = $this->add_condo_relation_transportation($data_condo_relation_transportation);
            }
        }

        return true;
    }
    //....................................................................................................................................

    public function add_member_owner_condo($data)
    {
        $insert = $this->db->insert('member_owner_condo', $data);

        return true;
    }
    //....................................................................................................................................

    public function edit_member_owner_condo($data)
    {
        //ต้องใช้ ID ของตารางนั้นๆ
        $update = $this->db
            ->where('condo_id', $data['condo_id'])
            ->where('member_id', $data['member_id'])
            ->update('member_owner_condo', $data);

        return true;
    }
    //....................................................................................................................................

    public function check_member_share($condo_id, $member_id)
    {
        $query = $this->db
            ->select('
				condo_id
				member_id,
			')
            ->where('condo_id', $condo_id)
            ->where('member_id', $member_id)
            ->get('member_share_condo');

        return $query->num_rows();
    }
    //....................................................................................................................................

    public function add_member_share_condo($data)
    {
        $results = array('success'=>false, 'message'=>'เกิดข้อผิดพลาด');
        $query = $this->db
            ->select('
      				condo_id
      				member_id,
      			')
            ->where('condo_id', $data['condo_id'])
            ->where('member_id', $data['member_id'])
            ->get('member_share_condo');

        if ($query->num_rows() > 0) {
            //UPDATE
            $update = $this->db
                ->set('fk_type_status_id', $data['fk_type_status_id'])
                ->set('updatedate', date('Y-m-d H:i:s'))
                ->where('condo_id', $data['condo_id'])
                ->where('member_id', $data['member_id'])
                ->update('member_share_condo', $data);

            $results = array('success'=>true, 'message'=>'ท่านได้ทำการแชร์เรียบร้อยแล้วค่ะ....');

        } else {
            //INSERT
            $insert = $this->db->insert('member_share_condo', $data);
            $results = array('success'=>true, 'message'=>'ท่านได้ทำการแชร์เรียบร้อยแล้วค่ะ....');
        }

        return $results;
    }
    //....................................................................................................................................

    public function edit_member_share_condo()
    {

      $data = $this->security->xss_clean($this->input->post());


      $fk_member_id = (!empty($data['fk_member_id']))? $data['fk_member_id'] : 0;
      $fk_condo_id = (!empty($data['fk_condo_id']))? $data['fk_condo_id'] : 0;
      $type_status_id = 2; //AUTO


      $data_share = array(
          'condo_id' => $fk_condo_id,
          'member_id' => $fk_member_id,
          'fk_type_status_id' => $type_status_id,
          'updatedate' => date('Y-m-d H:i:s'),
      );

        $update = $this->db
            ->where('condo_id', $fk_condo_id)
            ->where('member_id', $fk_member_id)
            ->update('member_share_condo', $data_share);
      //  echo $this->db->last_query();
        return true;
    }
    //....................................................................................................................................

    public function add_condo_relation_facilities($data)
    {
        $insert = $this->db->insert('condo_relation_facilities', $data);

        return true;
    }
    //....................................................................................................................................

    public function edit_condo_relation_facilities($data)
    {
        $update = $this->db
            ->where('condo_id', $data['condo_id'])
            ->where('facilities_id', $data['facilities_id'])
            ->update('condo_relation_facilities', $data);

        return true;
    }
    //....................................................................................................................................

    public function add_condo_relation_featured($data)
    {
        $insert = $this->db->insert('condo_relation_featured', $data);

        return true;
    }
    //....................................................................................................................................

    public function edit_condo_relation_featured($data)
    {
        $query = $this->db
            ->select('*')
            ->where('condo_id', $data['condo_id'])
            ->where('featured_id', $data['featured_id'])
            ->get('condo_relation_featured');

        if ($query->num_rows() > 0) {
            //UPDATE
            $update = $this->db
                ->where('condo_id', $data['condo_id'])
                ->where('featured_id', $data['featured_id'])
                ->update('condo_relation_featured', $data);
        } else {
            //INSERT
            $insert = $this->db->insert('condo_relation_featured', $data);
        }

        return true;
    }
    //....................................................................................................................................

    public function add_condo_relation_transportation($data)
    {
        $insert = $this->db->insert('condo_relation_transportation', $data);

        return true;
    }
    //....................................................................................................................................

    public function get_condo_relation_transportation($id)
    {
        $query = $this->db
            ->select('*')
            ->where('condo_id', $id)
            ->get('condo_relation_transportation');

        return $query->result();
    }
    //....................................................................................................................................

    public function set_condo_relation_transportation($data)
    {
        if (!empty($data)) {
            $query = $this->db
                ->select('transportation_id')
                ->where('condo_id', $data['condo_id'])
                ->get('condo_relation_transportation');

            echo $this->db->last_query();
            echo '<br>';
            echo $query->num_rows();
            if ($query->num_rows() > 0) {
                return $this->db->update('condo_relation_transportation', $data);
            } else {
                return $this->db->insert('condo_relation_transportation', $data);
            }
        } else {
            return $this->db->insert('condo_relation_transportation', $data);
        }
    }
    //....................................................................................................................................

    public function edit($data)
    {
        $this->load->library('primaryclass');
        $id = $data['condo_id'];

        $new_condo_description = $data['condo_description'];
        $data_condo = array(
            'condo_property_id' => $data['condo_property_id'],
            'member_id' => $data['member_id'],
            'staff_id' => $data['staff_id'],
            'condo_title' => $data['condo_title'],
            'condo_owner_name' => $data['condo_owner_name'],
            'condo_holder_name' => $data['condo_holder_name'],
            'condo_builing_finish' => $data['condo_builing_finish'],
            'property_type_id' => $data['property_type_id'],
            'type_zone_id' => $data['type_zone_id'],
            'zone_id' => $data['zone_id'],
            'type_offering_id' => $data['type_offering_id'],
            'type_suites_id' => $data['type_suites_id'],
            'type_decoration_id' => $data['type_decoration_id'],
            'condo_all_room' => $data['condo_all_room'],
            'condo_all_toilet' => $data['condo_all_toilet'],
            'condo_interior_room' => $data['condo_interior_room'],
            'condo_direction_room' => $data['condo_direction_room'],
            'condo_scenery_room' => $data['condo_scenery_room'],
            'condo_living_area' => $data['condo_living_area'],
            'condo_electricity' => $data['condo_electricity'],
            'condo_expert' => trim($data['condo_expert']),
            'condo_description' => $new_condo_description,
            'pic_thumb' => $data['pic_thumb'],
            'pic_large' => $data['pic_large'],
            'condo_no' => $data['condo_no'],
            'condo_floor' => $data['condo_floor'],
            'condo_area_apartment' => $data['condo_area_apartment'],
            'condo_alley' => trim($data['condo_alley']),
            'condo_road' => trim($data['condo_road']),
            'provinces_id' => $data['provinces_id'],
            'districts_id' => $data['districts_id'],
            'sub_districts_id' => $data['sub_districts_id'],
            'condo_zipcode' => $data['condo_zipcode'],
            'condo_total_price' => $data['condo_total_price'],
            'condo_price_per_square_meter' => $data['condo_price_per_square_meter'],
            'condo_googlemaps' => $data['condo_googlemaps'],
            'latitude' => trim($data['latitude']),
            'longitude' => trim($data['longitude']),
            'condo_tag' => $data['condo_tag'],
            'createdate' => $data['createdate'],
            'modifydate' => $data['modifydate'],
            'type_status_id' => trim($data['type_status_id']),
            'newsarrival' => $data['newsarrival'],
            'highlight' => $data['highlight'],
//			'publish'=>$data['publish'],
            'approved' => $data['approved'],
        );
        $update = $this->db
            ->where('condo_id', $id)
            ->update('condo', $data_condo);

        //PRINT Queries
        //echo $this->db->last_query();

        /* Update TO table : member_owner_condo */
        $data_member_owner_condo = array(
            'condo_id' => $id,
            'member_id' => $data['member_id'],
        );
        $update_member_owner_condo = $this->edit_member_owner_condo($data_member_owner_condo);

        /* FACILITIES */
        //$featured_id = array();
        /* Update TO table : land_relation_transportation */
        if (sizeof($data['facilities_id']) > 0) {
            $facilities_rows = count($data['facilities_id']);
            //DELETE
            $query = $this->db
                ->select('*')
                ->where('condo_id', $id)
                ->get('condo_relation_facilities');
            if ($query->num_rows() > 0) {
                $this->delete_record('condo_id', $id, 'condo_relation_facilities');
            }

            foreach ($data['facilities_id'] as $row_faci => $val_faci) {
                $data_condo_relation_facilities = array(
                    'condo_id' => $id,
                    'facilities_id' => $val_faci,
                );
                $update_condo_relation_facilities = $this->add_condo_relation_facilities($data_condo_relation_facilities);
            }
            /* END FOREACH */
        }
        /* END IF */

        /* Update TO table : condo_relation_featured */
        //$featured_id = array();
        if (sizeof($data['featured_id']) > 0) {
            $featured_rows = count($data['featured_id']);
            //DELETE
            $query = $this->db
                ->select('*')
                ->where('condo_id', $id)
                ->get('condo_relation_featured');
            if ($query->num_rows() > 0) {
                $this->delete_record('condo_id', $id, 'condo_relation_featured');
            }

            foreach ($data['featured_id'] as $row_feat => $val_feat) {
                $data_condo_relation_featured = array(
                    'condo_id' => $id,
                    'featured_id' => $val_feat,
                );
                //var_dump($val_feat);
                $update_condo_relation_featured = $this->add_condo_relation_featured($data_condo_relation_featured);
            }
            /* END FOREACH */
        }
        /* END IF */

        /* Update TO table : condo_relation_transportation */

        if (sizeof($data['transportation_category_id']) > 0) {
            foreach ($data['transportation_category_id'] as $row_cate => $val_cate) {
                if ($val_cate !== '0') {
                    $query = $this->db
                        ->select('transportation_id')
                        ->where('transportation_id', $val_cate)
                        ->where('condo_id', $id)
                        ->get('condo_relation_transportation');
                    if ($query->num_rows() > 0) {
                        //echo 'update naja : ' .$val_cate. '\n';
                        $transportation_distance = $data['transportation_distance'];
                        $data_condo_relation_transportation = array(
                            'condo_id' => $id,
                            'transportation_id' => $val_cate,
                            'transportation_distance' => $transportation_distance[$row_cate],
                        );
                        $this->db->where('condo_id', $id);
                        $this->db->where('transportation_id', $val_cate);
                        $this->db->update('condo_relation_transportation', $data_condo_relation_transportation);
                    } else {
                        //echo 'insert naja : ' .$val_cate. '\n';
                        $transportation_distance = $data['transportation_distance'];
                        $data_condo_relation_transportation = array(
                            'condo_id' => $id,
                            'transportation_id' => $val_cate,
                            'transportation_distance' => $transportation_distance[$row_cate],
                        );
                        $this->db->insert('condo_relation_transportation', $data_condo_relation_transportation);
                    }
                }
            }
            /* END FOREACH */
        }
        /* END IF */

        return true;
    }
    //....................................................................................................................................

    public function set_condo($data)
    {
        $update = $this->db
            ->where('condo_id', $data['condo_id'])
            ->update('condo', $data);

        return true;
    }
    //....................................................................................................................................

    public function get_info($id)
    {
        $this->db->where('condo_id', $id);
        $query = $this->db->get('condo');

        return $query->row();
    }
    //....................................................................................................................................

    public function delete($id)
    {
        $this->db->delete('condo', array('condo_id' => $id));

        return true;
    }
    //....................................................................................................................................

    /* IMAGE */
    public function get_condo_image($condo_id)
    {
        $query = $this->db
            ->select('
				condo_images_id,
				condo_id,
				condo_images_title,
				condo_images_description,
				pic_large,
				condo_images_url,
				createdate,
				updatedate,
				publish
			')
            ->where('condo_id', $condo_id)
            ->get('condo_images');

        return $query->result();
    }
    //....................................................................................................................................

    public function get_condo_image_by_id($condo_id = 0, $id = 0)
    {
        if ($id === 0) {
            $query = $this->db
                ->select('
					condo_images_id,
					condo_id,
					condo_images_title,
					condo_images_description,
					pic_thumb,
					pic_large,
					condo_images_url,
					createdate,
					updatedate,
					publish
				')
                ->where('condo_id', $condo_id)
                ->get('condo_images');

            return $query->result();
        }

        $query = $this->db
            ->select('
				condo_images_id,
				condo_id,
				condo_images_title,
				condo_images_description,
				pic_thumb,
				pic_large,
				condo_images_url,
				createdate,
				updatedate,
				publish
			')
            ->where('condo_images_id', $id)
            ->where('condo_id', $condo_id)
            ->get('condo_images');
        //$query = $this->db->get_where('condo_images', array('condo_images_id' => $id,'condo_id' => $condo_id));
        return $query->result();
    }
    //....................................................................................................................................

    public function set_image_condominium($condo_images_id = 0, $data)
    {
        if ($condo_images_id === 0) {
            $query = $this->db->insert('condo_images', $data);

            return $query;
        } else {
            $this->db->where('condo_images_id', $condo_images_id);
            $query = $this->db->update('condo_images', $data);
            echo $this->db->last_query(0);

            return $query;
        }
    }
    //....................................................................................................................................

    /* Facilities */
    public function get_facilities_option()
    {
        $query = $this->db
            ->select('facilities_id, facilities_title')
            ->order_by('facilities_id', 'ASC')
            ->get('facilities');

        return $query->result();
    }
    //....................................................................................................................................

    public function get_facilities_option_by_condo($id)
    {
        $query = $this->db
            ->select('facilities_id')
            ->where('condo_id', $id)
            ->get('condo_relation_facilities');

        return $query->result();
    }
    //....................................................................................................................................

    public function get_facilities_row_by_condo($id = 0)
    {
        $result = array();
        $query = $this->db
            ->select('
				facilities.facilities_id,
				facilities.facilities_title,
				condo_relation_facilities.condo_id')
            ->from('facilities')
            ->join('condo_relation_facilities', 'condo_relation_facilities.facilities_id = facilities.facilities_id', 'right')
            ->where('condo_relation_facilities.condo_id', $id)
            ->order_by('facilities.facilities_id', 'ASC')
            ->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
        }

        return $result;
        /* END WHILE */
    }
    //....................................................................................................................................

    public function get_facilities_record($id)
    {
        $this->db->where('facilities_id', $id);
        $query = $this->db->get('facilities');

        return $query->row();
    }
    //....................................................................................................................................

    public function set_facilities_record($data)
    {
        $id = $data['facilities_id'];
        $update = $this->db
            ->where('facilities_id', $id)
            ->update('facilities', $data);

        return true;
    }
    //....................................................................................................................................

    public function get_facilities()
    {
        $query = $this->db
            ->select('
				facilities_id,
				facilities_title,
				createdate,
				updatedate
			')
            ->get('facilities');

        return $query->result();
    }
    //....................................................................................................................................

    public function set_facilities($data)
    {
        $insert = $this->db->insert('facilities', $data);

        return true;
    }
    //....................................................................................................................................

    public function update_facilities($data)
    {
        $id = $data['facilities_id'];
        $update = $this->db
            ->where('facilities_id', $id)
            ->update('facilities', $data);

        return true;
    }
    //....................................................................................................................................

    public function delete_record($field_id, $id, $f_table)
    {
        $this->db->delete($f_table, array($field_id => $id));

        return true;
    }
    //....................................................................................................................................

    /* Featured */
    public function get_featured_record($id)
    {
        $this->db->where('featured_id', $id);
        $query = $this->db->get('featured');

        return $query->row();
    }
    //....................................................................................................................................

    public function set_featured_record($data)
    {
        $id = $data['featured_id'];
        $update = $this->db
            ->where('featured_id', $id)
            ->update('featured', $data);

        return true;
    }
    //....................................................................................................................................

    public function get_featured_option()
    {
        $query = $this->db
            ->select('featured_id, featured_title')
            ->order_by('featured_id', 'ASC')
            ->get('featured');

        return $query->result();
    }
    //....................................................................................................................................

    public function get_featured_row_by_condo($id)
    {
        $result = array();
        $query = $this->db
            ->select('
				featured.featured_id,
				featured.featured_title,
				condo_relation_featured.condo_id')
            ->from('featured')
            ->join('condo_relation_featured', 'condo_relation_featured.featured_id = featured.featured_id')
            ->where('condo_relation_featured.condo_id', $id)
            ->order_by('featured.featured_id', 'ASC')
            ->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
        }

        return $result;
        /* END WHILE */
    }
    //....................................................................................................................................

    public function get_featured_option_by_condo($id)
    {
        $query = $this->db
            ->select('*')
            ->where('condo_id', $id)
            ->get('condo_relation_featured');
//		print_r($query);
        return $query->result();
    }
    //....................................................................................................................................

    public function get_featured()
    {
        $query = $this->db
            ->select('
				featured_id,
				featured_title,
				createdate,
				updatedate
			')
            ->get('featured');

        return $query->result();
    }
    //....................................................................................................................................

    public function set_featured($data)
    {
        $insert = $this->db->insert('featured', $data);

        return true;
    }
    //....................................................................................................................................

    public function update_featured($data)
    {
        $id = $data['featured_id'];
        $update = $this->db
            ->where('featured_id', $id)
            ->update('featured', $data);

        return true;
    }
    //....................................................................................................................................

    //....................................................................................................................................
    /************* Transportation Category *************/
    //....................................................................................................................................
    public function get_transportation_category_record($id)
    {
        $this->db->where('transportation_category_id', $id);
        $query = $this->db->get('transportation_category');

        return $query->row();
    }
    //....................................................................................................................................

    public function get_transportation_category_name($id)
    {
        $this->db
            ->select('transportation_category_title')
            ->where('transportation_category_id', $id);
        $query = $this->db->get('transportation_category');
        //var_dump($query->row('transportation_category_title'));
        return $query->row('transportation_category_title');
    }
    //....................................................................................................................................

    public function set_transportation_category_record($data)
    {
        $id = $data['transportation_id'];
        $update = $this->db
            ->where('transportation_category_id', $id)
            ->update('transportation_category', $data);

        return true;
    }
    //....................................................................................................................................

    public function get_transportation_category_option()
    {
        $query = $this->db
            ->select('transportation_category_id, transportation_category_title')
            ->order_by('transportation_category_id', 'ASC')
            ->get('transportation_category');

        return $query->result();
    }
    //....................................................................................................................................

    public function get_transportation_relation_row_by_condo($id = 0)
    {
        $result = array();
        $query = $this->db
            ->select('
				crt.condo_relation_transportation_id,
				crt.condo_id,
				crt.transportation_id,
				crt.transportation_distance,
				t.transportation_title,
				tc.transportation_category_id,
				tc.transportation_category_title,
				')
            ->from('condo_relation_transportation crt')
            ->join('transportation t', 't.transportation_id = crt.transportation_id', 'left')
            ->join('transportation_category tc', 'tc.transportation_category_id = t.transportation_category_id', 'left')
            ->where('crt.condo_id', $id)
            ->order_by('tc.transportation_category_id', 'ASC')
            ->get();
//		echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->result();
        }

        return $result;
    }
    //....................................................................................................................................

    public function get_transportation_category()
    {
        $query = $this->db
            ->select('
				transportation_category_id,
				transportation_category_title,
				createdate,
				updatedate,
				publish
			')
            ->get('transportation_category');

        return $query->result();
    }
    //....................................................................................................................................

    public function set_transportation_category($data)
    {
        $insert = $this->db->insert('transportation_category', $data);

        return true;
    }
    //....................................................................................................................................

    public function update_transportation_category($data)
    {
        $id = $data['transportation_category_id'];
        $update = $this->db
            ->where('transportation_category_id', $id)
            ->update('transportation_category', $data);

        return true;
    }
    //....................................................................................................................................

    //....................................................................................................................................
    /************* Transportation *************/
    //....................................................................................................................................
    public function get_transportation_record($id)
    {
        $this->db->where('transportation_id', $id);
        $query = $this->db->get('transportation');

        return $query->row();
    }
    //....................................................................................................................................

    public function set_transportation_record($data)
    {
        $id = $data['transportation_id'];
        $update = $this->db
            ->where('transportation_id', $id)
            ->update('transportation', $data);

        return true;
    }
    //....................................................................................................................................

    public function get_transportation_option($id = 0)
    {
        $query = $this->db
            ->select('transportation_id, transportation_title')
            ->where('transportation_category_id', $id)
            ->order_by('transportation_id', 'ASC')
            ->get('transportation');

//		print_r($query);
        return $query->result();
    }
    //....................................................................................................................................

    public function get_transportation()
    {
        $query = $this->db
            ->select('
				transportation_id,
				transportation_title,
				transportation_category_id,
				createdate,
				updatedate,
				publish
			')
            ->get('transportation');

        return $query->result();
    }
    //....................................................................................................................................

    public function set_transportation($data)
    {
        $insert = $this->db->insert('transportation', $data);

        return true;
    }
    //....................................................................................................................................

    public function update_transportation($data)
    {
        $id = $data['transportation_id'];
        $update = $this->db
            ->where('transportation_id', $id)
            ->update('transportation', $data);

        return true;
    }
    //....................................................................................................................................

        public function get_transportation_row_by_condo($condo_id, $category_id)
        {
            $query = $this->db
            ->select('
				trans.transportation_id,
				trans.transportation_category_id,
				trans.transportation_title,
				condo_rela.condo_id,
				condo_rela.transportation_id,
				condo_rela.transportation_distance,
				')
            ->from('condo_relation_transportation condo_rela')
            ->join('transportation trans', 'trans.transportation_id = condo_rela.transportation_id')
            ->where('trans.transportation_category_id', $category_id)
            ->where('condo_rela.condo_id', $condo_id)
            ->order_by('condo_rela.condo_relation_transportation_id', 'ASC')
            ->get();

            return $query->result();
        }
    //....................................................................................................................................

    //....................................................................................................................................
    /************* Mass Transportation Category *************/
    //....................................................................................................................................
    public function get_mass_transportation_category()
    {
        $result = array();
        $query = $this->db
            ->select('
      				mass_transportation_category_id,
      				mass_transportation_category_title,
      				createdate,
      				updatedate,
      				publish
      			')
         ->get('mass_transportation_category');
        if ($query->num_rows() > 0) {
            $result = $query->result();
        }

        return $result;
    }
    //....................................................................................................................................

    public function get_mass_transportation_category_by_id($id = 0)
    {
        $result = array();
        if ($id !== 0) {
            $this->db->where('mass_transportation_category_id', $id);
            $query = $this->db->get('mass_transportation_category');
            $result = $query->row();
        }

        return $result;
    }
    //....................................................................................................................................

    public function get_mass_transportation_category_option()
    {
        $result = array();
        $query = $this->db
            ->select('mass_transportation_category_id, mass_transportation_category_title')
            ->order_by('mass_transportation_category_id', 'ASC')
            ->get('mass_transportation_category');

        if ($query->num_rows() > 0) {
            $result = $query->result();
        }

        return $result;
    }
    //....................................................................................................................................

    public function set_mass_transportation_category($data = '')
    {
        $result = '';
        if (!empty($data)) {
            $result = $this->db->insert('mass_transportation_category', $data);
        }

        return $result;
    }
    //....................................................................................................................................

    public function update_mass_transportation_category($data = '')
    {
        $result = array();
        if (!empty($data)) {
            $result = $this->db
                ->where('mass_transportation_category_id', $data['mass_transportation_category_id'])
                ->update('mass_transportation_category', $data);
        }

        return $result;
    }
    //....................................................................................................................................

    //....................................................................................................................................
    /************* Mass Transportation *************/
    //....................................................................................................................................
    public function get_mass_transportation($category_id = 0)
    {
        $result = array();
        $query = $this->db
            ->select('
      				mass_transportation_id,
      				mass_transportation_category_id,
      				mass_transportation_title,
      				createdate,
      				updatedate,
      				publish
    			  ');
        if($category_id !== 0){
          $query = $this->db->where('mass_transportation_category_id', $category_id);
        }
        $query = $this->db->get('mass_transportation');
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $result = $query->result();
        }

        return $result;
    }
    //....................................................................................................................................

    public function get_mass_transportation_by_id($id = 0)
    {
        $result = array();
        if ($id !== 0) {
            $this->db->where('mass_transportation_id', $id);
            $query = $this->db->get('mass_transportation');
            $result = $query->row();
        }

        return $result;
    }
    //....................................................................................................................................

    public function get_mass_transportation_option()
    {
        $result = array();
        $query = $this->db
            ->select('mass_transportation_id, mass_transportation_title')
            ->order_by('mass_transportation_id', 'ASC')
            ->get('mass_transportation');

        if ($query->num_rows() > 0) {
            $result = $query->result();
        }

        return $result;
    }
    //....................................................................................................................................

    public function set_mass_transportation($data = '')
    {
        $result = '';
        if (!empty($data)) {
            $result = $this->db->insert('mass_transportation', $data);
        }

        return $result;
    }
    //....................................................................................................................................

    public function update_mass_transportation($data = '')
    {
        $result = array();
        if (!empty($data)) {
            $result = $this->db
                ->where('mass_transportation_id', $data['mass_transportation_id'])
                ->update('mass_transportation', $data);
        }

        return $result;
    }
    //....................................................................................................................................

    /************ *********** ************/
    /************ SHARE CONDO ************/
    //....................................................................................................................................
    public function get_share_condo($id)
    {
        $results = array();
        $query = $this->db
            ->select('
				condo.condo_id,
				condo.condo_title,
				member_share_condo.member_share_condo_id,
				member_share_condo.member_id,
				member_share_condo.fk_type_status_id,
				member_share_condo.createdate
			')
            ->from('condo')
            ->join('member_share_condo', 'member_share_condo.condo_id = condo.condo_id', 'right')
            ->where('condo.condo_id', $id)
            ->get();
        //echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            return $results = $query->result();
        }

        return $results;
    }
    //....................................................................................................................................

    public function get_condo_by_member($m_id)
    {
        $query = $this->db
            ->select('
				condo.condo_id,
				condo.member_id,
				condo.condo_title,
				condo.pic_thumb,
				condo.createdate,
				condo.modifydate,
				condo.visited,
				condo.type_status_id,
				condo.publish,
				condo.approved
			')
            ->where('condo.member_id', $m_id)
            ->where('condo.approved =', '2') // 2 = update_sharedate
            ->where('condo.publish =', '2') // 2 = PUBLISHED
            ->get('condo');

        return $query->result();
    }
    //....................................................................................................................................

    public function get_member_condo_approved($m_id)
    {
        $query = $this->db
            ->select('
				condo.condo_id,
				condo.member_id,
				condo.condo_title,
				condo.pic_thumb,
				condo.createdate,
				condo.modifydate,
				condo.visited,
				condo.publish,
				condo.approved,
			')
            ->where('condo.member_id', $m_id)
            ->where('condo.approved =', '2') // 2 = update_sharedate
            ->get('condo');

        return $query->result();
    }
    //....................................................................................................................................

    public function get_member_condo_unapproved($m_id)
    {
        $query = $this->db
            ->select('
				condo.condo_id,
				condo.member_id,
				condo.condo_title,
				condo.pic_thumb,
				condo.createdate,
				condo.modifydate,
				condo.visited,
				condo.publish,
				condo.approved,
			')
            ->where('condo.member_id', $m_id)
            ->where('condo.approved =', '1') // 2 = update_sharedate
            ->get('condo');

        return $query->result();
    }
    //....................................................................................................................................

    /************ *********** ************/
    /******** MEMBER SHARE CONDO *********/
    //....................................................................................................................................
    public function get_list_member_share_condo($id)
    {
        $query = $this->db
            ->select('
				condo.condo_id,
				condo.zone_id,
				condo.condo_title,
				condo.pic_thumb,
				condo.condo_total_price,
				condo.condo_price_per_square_meter,
				condo.approved,
				member_share_condo.member_share_condo_id,
				member_share_condo.condo_id,
				member_share_condo.member_id,
				member_share_condo.fk_type_status_id,
				member_share_condo.createdate,
				member_share_condo.sharedate,
				member_share_condo.updatedate,
			')
            ->from('condo')
            ->join('member_share_condo', 'member_share_condo.condo_id = condo.condo_id', 'left')
            ->where('condo.approved', '2')
            ->where('member_share_condo.member_id', $id)
            ->group_by('member_share_condo.condo_id')
            ->group_by('member_share_condo.member_id')
            ->get();
        //echo $this->db->queries[0];
        return $query->result();
    }
    //....................................................................................................................................

    /************ *********** ************/
    /************ DATA IMAGES ************/
    public function add_data_condo_images($data)
    {
        $insert = $this->db->insert('condo_images', $data);

        return true;
    }
    //....................................................................................................................................

    /************ *********** ************/
    /********** WISHLIST CONDO ***********/
    public function get_wishlist_condo($id)
    {
        $query = $this->db
            ->select('
				condo.*,
				wishlist_condo.*,
				')
            ->from('condo')
            ->join('wishlist_condo', 'wishlist_condo.condo_id = condo.condo_id', 'left')
            ->where('wishlist_condo.member_id', $id)
            ->get();
        //echo $this->db->queries[0];
        return $query->result();
    }
    //....................................................................................................................................

    public function add_wishlist_condo($data)
    {
        $query = $this->db
            ->select('
				condo_id,
				member_id')
            ->where('condo_id', $data['condo_id'])
            ->where('member_id', $data['member_id'])
            ->get('wishlist_condo');
        if ($query->num_rows() > 0) {
            //UPDATE
//			$update = $this->db
//				->set('condo_id', $data['condo_id'])
//				->set('member_id', $data['member_id'])
//				->set('updatedate', $data['updatedate'])
//				->where('condo_id', $data['condo_id'])
//				->where('member_id', $data['member_id'])
//				->update('wishlist_condo');
//			return true;
            //DELETE
            echo 'delete';
            $delete = $this->delete_record('condo_id', $data['condo_id'], 'wishlist_condo');
        } else {
            //INSERT
            echo 'insert';
            $insert = $this->db->insert('wishlist_condo', $data);

            return true;
        }
    }
    //....................................................................................................................................

    public function update_condo_images($data)
    {
        $id = $data['condo_images_id'];
        $update = $this->db
            ->where('condo_images_id', $id)
            ->update('condo_images', $data);

        return true;
    }
    //....................................................................................................................................

    /************ *********** ************/
    /********** COMPARE CONDO ***********/
    public function get_compare_condo($sess_id)
    {
        $query = $this->db
            ->select('
				condo.condo_id,
				condo.member_id,
				condo.condo_title,
				condo.type_zone_id,
				condo.zone_id,
				condo.pic_thumb,
				condo.condo_all_room,
				condo.condo_all_toilet,
				condo.condo_interior_room,
				condo.condo_direction_room,
				condo.condo_scenery_room,
				condo.condo_living_area,
				condo.condo_electricity,
				condo.condo_all_room,
				condo.condo_all_toilet,
				condo.condo_living_area,
				condo.condo_no,
				condo.condo_floor,
				condo.condo_area_apartment,
				condo.condo_alley,
				condo.condo_road,
				condo.condo_address,
				condo.provinces_id,
				condo.districts_id,
				condo.sub_districts_id,
				condo.condo_zipcode,
				condo.condo_total_price,
				condo.condo_price_per_square_meter,
				condo.type_decoration_id,
				condo.createdate,
				condo.modifydate,
				condo.zone_id,
				condo.type_status_id,
				condo.publish,
				condo.approved,
				compare_condo.*,
				')
            ->from('condo')
            ->join('compare_condo', 'compare_condo.condo_id = condo.condo_id', 'left')
            ->where('condo.approved', 2)
            ->where('compare_condo.sess_id', $sess_id)
            ->get();
        // echo $this->db->last_query();
        return $query->result_array();
    }
    //....................................................................................................................................

    public function get_max_price($sess_id)
    {
        $query = $this->db
            ->select('
				condo.condo_id,
				MAX(condo.condo_total_price) AS highestprice,
				compare_condo.sess_id
				')
            ->from('compare_condo')
            ->join('condo', 'condo.condo_id = compare_condo.condo_id', 'left')
            ->where('compare_condo.sess_id', $sess_id)
            ->get();
        //echo $this->db->queries[0];
        $row = $query->row();
        //echo $row->condo_id;
        return $row->condo_id;
    }
    //....................................................................................................................................

    public function get_min_price($sess_id)
    {
        $query = $this->db
            ->select('
				condo.condo_id,
				MIN(condo.condo_total_price),
				compare_condo.sess_id
				')
            ->from('compare_condo')
            ->join('condo', 'condo.condo_id = compare_condo.condo_id', 'left')
            ->where('compare_condo.sess_id', $sess_id)
            ->group_by('condo.condo_id')
            ->get();
        //echo $this->db->queries[0];
        if ($query->num_rows() > 0) {
            $row = $query->row();

            return $row->condo_id;
        } else {
            return 0;
        }

        //echo $row->condo_id;
    }
    //....................................................................................................................................

    public function insert_compare_condo($data)
    {
        $query = $this->db
            ->select('
				condo_id,
				member_id')
            ->where('condo_id', $data['condo_id'])
            ->where('sess_id', $data['sess_id'])
            ->get('compare_condo');
        if ($query->num_rows() > 0) {
            //UPDATE
//			$update = $this->db
//				->set('condo_id', $data['condo_id'])
//				->set('sess_id', $data['sess_id'])
//				->set('updatedate', $data['updatedate'])
//				->where('condo_id', $data['condo_id'])
//				->where('sess_id', $data['sess_id'])
//				->update('compare_condo');
//			return true;

            //DELETE
            echo 'delete';
            $delete = $this->delete_record('condo_id', $data['condo_id'], 'compare_condo');
        } else {
            //INSERT
            echo 'insert';
            $insert = $this->db->insert('compare_condo', $data);
        }

        return true;
    }
    //....................................................................................................................................

    /************ *********** ************/
    /********* SHARE CONDO FORM **********/
    //....................................................................................................................................
    public function get_condo_share_form($id)
    {
        $query = $this->db
            ->select('
				condo_share_form.fk_condo_id,
				condo_share_form.fk_member_id,
				condo_share_form.createdate,
				member_share_condo.member_share_condo_id,
				member_share_condo.condo_id,
				member_share_condo.member_id,
				member_share_condo.fk_type_status_id,
				condo.condo_id,
				condo.zone_id,
				condo.condo_title,
				condo.pic_thumb,
				condo.approved,
			')
            ->from('condo_share_form')
            ->join('condo', 'condo_share_form.fk_condo_id = condo.condo_id', 'left')
            ->join('member_share_condo', 'condo_share_form.fk_member_id = member_share_condo.member_id', 'left')
            ->where('condo_share_form.fk_member_id', $id)
            ->where('member_share_condo.fk_type_status_id', 2)
            ->where('condo.approved', '2')
            ->group_by('condo_share_form.fk_condo_id')
            ->get();
            // echo $this->db->last_query();
        return $query->result();
    }
    //....................................................................................................................................

    public function get_condo_share_form_by_id($id = 0)
    {
        $query = $this->db
            ->select('
				condo_share_form.fk_condo_id,
				condo_share_form.fk_member_id,
				condo_share_form.createdate,

				member_share_condo.member_share_condo_id,
				member_share_condo.condo_id,
				member_share_condo.member_id,
				member_share_condo.fk_type_status_id,
				member_share_condo.createdate,
				member_share_condo.sharedate,
				member_share_condo.updatedate,

				condo.condo_id,
				condo.zone_id,
				condo.condo_title,
				condo.pic_thumb,
				condo.approved,


			')
            ->from('condo_share_form')
            ->join('condo', 'condo_share_form.fk_condo_id = condo.condo_id', 'left')
            ->join('member_share_condo', 'condo_share_form.fk_member_id = member_share_condo.member_id', 'left')
            ->where('condo_share_form.fk_member_id', $id)
            ->where('member_share_condo.fk_type_status_id', 2)
            ->where('condo.approved', '2')
            ->group_by('condo_share_form.fk_condo_id')
            ->get();
//		echo $this->db->queries[0];
        return $query->result();
    }
    //....................................................................................................................................

    public function insert_condo_share_form()
    {
      $type_status_id = 2; //AUTO

      $data = $this->security->xss_clean($this->input->post());
      // $decode_member_id = $this->primaryclass->raw_decode($data['fk_member_id']);
      $fk_member_id = (!empty($data['fk_member_id']))? $data['fk_member_id'] : 0;
      $fk_condo_id = (!empty($data['fk_condo_id']))? $data['fk_condo_id'] : 0;
      $fk_staff_id = (!empty($data['fk_staff_id']))? $data['fk_staff_id'] : 0;
      $enewsletter = (!empty($data['enewsletter']))? $data['enewsletter'] : 0;
      $condo_share_form_name = (!empty($data['condo_share_form_name']))? $data['condo_share_form_name'] : '';
      $condo_share_form_phone = (!empty($data['condo_share_form_phone']))? $data['condo_share_form_phone'] : '';
      $condo_share_form_email = (!empty($data['condo_share_form_email']))? $data['condo_share_form_email'] : '';
      $condo_share_form_message = (!empty($data['condo_share_form_message']))? $data['condo_share_form_message'] : '';
      $condo_share_form_status = (!empty($data['condo_share_form_status']))? $data['condo_share_form_status'] : 0;
      $data_from = array(
          'fk_condo_id' => $fk_condo_id,
          'fk_member_id' => $fk_member_id,
          'fk_staff_id' => $fk_staff_id,
          'condo_share_form_name' => $condo_share_form_name,
          'condo_share_form_phone' => $condo_share_form_phone,
          'condo_share_form_email' => $condo_share_form_email,
          'condo_share_form_message' => $condo_share_form_message,
          'fk_type_status_id' => $type_status_id,
          'enewsletter' => $enewsletter,
          'ipaddress' => $this->input->ip_address(),
          'updatedate' => date('Y-m-d H:i:s'),
          'createdate' => date('Y-m-d H:i:s'),
          'condo_share_form_status' => $condo_share_form_status,
      );
      // $this->primaryclass->pre_var_dump($data_from);

        $query = $this->db
            ->select('
				        fk_condo_id,
				        condo_share_form_email')
            ->where('fk_condo_id', $fk_condo_id)
            ->where('condo_share_form_email', $condo_share_form_email)
            ->get('condo_share_form');
            // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            //UPDATE เคยติดต่อมาแล้ว

            $update = $this->db->where('fk_condo_id', $fk_condo_id);
            $update = $this->db->where('condo_share_form_email', $condo_share_form_email);
            $update = $this->db->update('condo_share_form', $data_from);
            // echo $this->db->last_query();
            $update_status = $this->update_condo_by_status_contact($fk_condo_id);

            echo 'success';

            return true;
        } else {
            //INSERT รายการติดต่อใหม่
            $insert = $this->db->insert('condo_share_form', $data_from);
            // echo $this->db->last_query();
            $update_status = $this->update_condo_by_status_contact($fk_condo_id);
            echo 'success';

            return true;
        }
    }
    //....................................................................................................................................

    public function update_condo_by_status_contact($id)
    {
        $update = $this->db
            ->set('type_status_id', 2)
            ->where('condo_id', $id)
            ->update('condo');
    }
    //....................................................................................................................................

    public function update_sharedate($data)
    {
        $update = $this->db
            ->where('member_share_condo_id', $data['member_share_condo_id'])
            ->update('member_share_condo', $data);
        echo 'success';

        return true;
    }
    //....................................................................................................................................

    /* GET LIST CONTACT BY CONDO */
    public function get_condo_contact_list($condo_id, $member_id)
    {
        $result = array();
        $query = $this->db
            ->select('
        				csf.condo_share_form_id,
        				csf.fk_condo_id,
        				csf.fk_member_id,
        				csf.condo_share_form_name,
        				csf.condo_share_form_phone,
        				csf.condo_share_form_email,
        				csf.condo_share_form_message,
        				csf.createdate,
        				csf.updatedate,
        				csf.condo_share_form_status,
        				csf.fk_type_status_id,
        			')
            ->where('fk_condo_id', $condo_id)
            ->where('fk_member_id', $member_id)
            ->get('condo_share_form csf');
//		echo $this->db->queries[0];
        if ($query->num_rows() > 0) {
            return $result = $query->result();
        } else {
            return $result;
        }
    }
    //....................................................................................................................................

    /* GET LIST CONTACT BY NOTIFICATION */
    public function get_condo_notification()
    {
        $result = array();
        $query = $this->db
            ->select('
        				csf.condo_share_form_id,
        				csf.fk_condo_id,
        				csf.fk_member_id,
        				csf.condo_share_form_name,
        				csf.condo_share_form_phone,
        				csf.condo_share_form_email,
        				csf.condo_share_form_message,
        				csf.createdate,
        				csf.updatedate,
        				csf.condo_share_form_status,
        				csf.fk_type_status_id,
        			')
            ->where('fk_condo_id', $condo_id)
            ->where('fk_member_id', $member_id)
            ->get('condo_share_form csf');
//		echo $this->db->queries[0];
        if ($query->num_rows() > 0) {
            return $result = $query->result();
        } else {
            return $result;
        }
    }
    //....................................................................................................................................

    public function get_condo_contact_by_id($condo_id = 0, $member_id = 0)
    {
        $result = array();
        if ($condo_id !== 0 && $member_id !== 0) {
            $query = $this->db
                ->select('
        					csf.condo_share_form_id,
        					csf.fk_condo_id,
        					csf.fk_member_id,
        					csf.condo_share_form_name,
        					csf.condo_share_form_phone,
        					csf.condo_share_form_email,
        					csf.condo_share_form_message,
        					csf.createdate,
        					csf.updatedate,
        					m.member_fname,
        					m.member_lname,
        					m.member_email,
        					m.member_mobileno,
        				')
                ->join('member m', 'm.member_id = csf.fk_member_id')
                ->where('csf.fk_condo_id', $condo_id)
                ->where('csf.fk_member_id', $member_id)
                ->get('condo_share_form csf');
    //		echo $this->db->queries[0];
            if ($query->num_rows() > 0) {
                return $result = $query->result_array();
            } else {
                return $result;
            }
        } else {
            return $result;
        }
    }
    //....................................................................................................................................

    /* UPDATE STATUS CONTACT FROM MEMBER */
    public function update_condo_share_form_status($data)
    {
        $update = $this->db
            ->where('condo_share_form_id', $data['condo_share_form_id'])
            ->update('condo_share_form', $data);
        echo 'success';

        return true;
    }
    //....................................................................................................................................

    public function get_member_owner_condo($condo_id = 0)
    {
        $result = array();

          $query = $this->db
            ->select('
    					c.condo_id,
    					c.condo_property_id,
    					c.member_id,
    					m.member_fname,
    					m.member_lname,
    					m.member_email,
    					m.member_mobileno,
    				');
          $query = $this->db->join('member m', 'm.member_id = c.member_id');
          if ($condo_id !== 0) {
            $query = $this->db->where('c.condo_id', $condo_id);
          }
          $query = $this->db->get('condo c');
          // echo $this->db->last_query();
          if ($query->num_rows() > 0) {
            $result = $query->result_array();
          }

        return $result;
    }
    //....................................................................................................................................
    public function get_staff_owner_condo($condo_id = 0)
    {
        $result = array();

          $query = $this->db
            ->select('
    					c.condo_id,
    					c.condo_property_id,
    					c.staff_id,
    					s.fullname,
    					s.email,
    					s.phone,
    				');
          $query = $this->db->join('staff s', 's.staff_id = c.staff_id');
          if ($condo_id !== 0) {
            $query = $this->db->where('c.condo_id', $condo_id);
          }
          $query = $this->db->get('condo c');
          echo $this->db->last_query();
          if ($query->num_rows() > 0) {
            $result = $query->result_array();
          }

        return $result;
    }
    //....................................................................................................................................

    /************ *********** ************/
    /*********** CONDO SIDEBAR ***********/
    //....................................................................................................................................
    public function get_condo_sidebar()
    {
        $result = array();
        $query = $this->db
            ->select('
				cd.condo_id,
				cd.type_zone_id,
				cd.zone_id,
				cd.condo_title,
				cd.pic_thumb,
				cd.condo_alley,
				cd.condo_road,
				cd.highlight,
				tz.type_zone_title
			')
            ->join('type_zone tz', 'tz.type_zone_id = cd.type_zone_id', 'left')
            ->where('cd.highlight', '2')
            ->where('cd.approved', '2')
            ->order_by('cd.condo_id', 'DESC')
            ->limit(6)
            ->get('condo cd');
        if ($query->num_rows() > 0) {
            $result = $query->result();
        }

        return $result;
    }
    //....................................................................................................................................

    public function condo_all_record_count()
    {
        $query = $this->db
            ->select('condo_id')
            ->where('approved', '2')
            ->get('condo');
        $record = $query->num_rows();

        return ($record > 0) ? $record : 0;
    }
    //....................................................................................................................................
}
