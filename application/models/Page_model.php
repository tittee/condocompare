<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Page_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }    //....................................................................................................................................

    public function highlight_big()
    {
        $query = $this->db
            ->select('
				condo.condo_id,
				condo.condo_title,
				condo.type_zone_id,
				condo.pic_thumb,
				condo.pic_large,
				condo.condo_alley,
				condo.condo_road,
				condo.createdate,
				condo.modifydate,
				condo.highlight,
				condo.publish,
				condo.approved,
				type_zone.type_zone_title
			')
            ->from('condo')
            ->where('condo.approved', 2)
            ->where('condo.highlight', 2)
            ->limit(1)
            ->order_by('condo.createdate', 'DESC')
            ->join('type_zone', 'type_zone.type_zone_id = condo.type_zone_id', 'left')
            ->get();

        return $query->row();
    }
    //....................................................................................................................................

    public function highlight($id)
    {
        $query = $this->db
            ->select('
				condo.condo_id,
				condo.condo_title,
				condo.type_zone_id,
				condo.type_offering_id,
				condo.pic_thumb,
        condo.condo_alley,
				condo.condo_road,
				condo.createdate,
				condo.modifydate,
				condo.highlight,
				condo.publish,
				condo.approved,
				type_zone.type_zone_title
			')
            ->from('condo')
            ->where('condo.condo_id !=', $id)
            ->where('condo.approved', 2)
            ->where('condo.highlight', 2)
            ->limit(2)
            ->order_by('condo.createdate', 'DESC')
            ->join('type_zone', 'type_zone.type_zone_id = condo.type_zone_id', 'left')
            ->get();

        return $query->result();
    }
    //....................................................................................................................................

    public function newsarrival()
    {
        $query = $this->db
            ->select('
				condo.condo_id,
				condo.condo_title,
				condo.type_zone_id,
				condo.type_offering_id,
				condo.pic_thumb,
        condo.condo_alley,
				condo.condo_road,
				condo.createdate,
				condo.modifydate,
				condo.highlight,
				condo.publish,
				condo.approved,
				type_zone.type_zone_title
			')
            ->from('condo')
            ->where('condo.approved', 2)
            ->where('condo.newsarrival', 2)
            ->order_by('condo.createdate', 'DESC')
            ->join('type_zone', 'type_zone.type_zone_id = condo.type_zone_id', 'left')
            ->limit(12)
            ->get();

        return $query->result();
    }
    //....................................................................................................................................

    public function get_condolist($limit, $start, $order_by = null)
    {
        // CLEAR DATA
        $data = $this->security->xss_clean($this->input->get());
        // $this->primaryclass->pre_var_dump($data);
        // KEYWORD : SEARCH
        // ต้องค้นหาซอย ถนน จังหวัด ได้
        // SEARCH TABS : 1
        $keyword = (!empty($data['search'])) ? $data['search'] : null;
        $min_price = (!empty($data['min_price'])) ? $data['min_price'] : '';
        $max_price = (!empty($data['max_price'])) ? $data['max_price'] : '';
        $type_offering = (!empty($data['type_offering'])) ? $data['type_offering'] : '';
        $type_suites = (!empty($data['type_suites'])) ? $data['type_suites'] : '';
        $condo_room = (!empty($data['condo_room'])) ? $data['condo_room'] : '';
        $condo_area_apartment = (!empty($data['condo_area_apartment'])) ? $data['condo_area_apartment'] : '';
        //---------------------------------------------
        // SEARCH TABS : 2
        $mass_transportation_id = (!empty($data['mass_transportation_id'])) ? $data['mass_transportation_id'] : '';
        $transportation_category = (!empty($data['transportation_category'])) ? $data['transportation_category'] : '';
        $bts = (!empty($data['bts'])) ? $data['bts'] : '';
        $mrt = (!empty($data['mrt'])) ? $data['mrt'] : '';
        $airportlink = (!empty($data['airportlink'])) ? $data['airportlink'] : '';
        //---------------------------------------------
        // SEARCH TABS : 3
        $type_zone_id = (!empty($data['type_zone_id'])) ? $data['type_zone_id'] : '';
        //---------------------------------------------
        // SEARCH TABS :  4
        $transportation_id = (!empty($data['transportation_id'])) ? $data['transportation_id'] : '';
        //---------------------------------------------
        // SEARCH FOOTER :  1
        $condo_owner_name = (!empty($data['condo_owner_name'])) ? $data['condo_owner_name'] : '';
        //---------------------------------------------

        // ACTION SORT :  1
        $order_by_list = (!empty($data['order_by_list'])) ? $data['order_by_list'] : '';
        $order_by_price_visited = (!empty($data['order_by_price_visited'])) ? $data['order_by_price_visited'] : '';

        $result = array();

        //---------------------------------------------
        // ไม่มีการค้นหา
        //---------------------------------------------

        //---------------------------------------------
        // SEARCH TAB : 2 ค้นหาจากระบบขนส่งมวลชน
        // SEARCH : MASS TRANSPORT CATEGORY
        //---------------------------------------------
        if (!empty($mass_transportation_id)) {
            $query_rel = $this->db
                ->select('
      					rel_mass_trans_condo.condo_id,
      					rel_mass_trans_condo.mass_transportation_id,
      					mass_trans.mass_transportation_id,
      					mass_trans.mass_transportation_title,
      					mass_trans.publish,
      					')
                ->join('mass_transportation mass_trans', 'mass_trans.mass_transportation_id = rel_mass_trans_condo.mass_transportation_id', 'left')
                ->where('mass_trans.publish', 2);

            $query_rel = $this->db->where('mass_trans.mass_transportation_id', $mass_transportation_id);
            $query_rel = $this->db->get('condo_relation_mass_transportation rel_mass_trans_condo');

            if ($query_rel->num_rows() > 0) {
                $string_arr = array();
                foreach ($query_rel->result() as $row_rel) {
                    $query_parts[] = $row_rel->condo_id;
                }
                /* STEP 1 */
                $string_arr = implode("'".','."'", $query_parts);
                $condo_id = array($string_arr);
            }
            else{
              $string_arr = '';
            }
        }
        // END IF SEARCH : MASS TRANSPORT CATEGORY
        //------------------------------------------

        //---------------------------------------------
        // SEARCH TAB : 2 ค้นหาจากระบบขนส่งมวลชน
        // SEARCH : TRANSPORT CATEGORY
        //---------------------------------------------
        if (!empty($transportation_category)) {
            $query_rel = $this->db
                ->select('
					rel_trans_condo.condo_id,
					rel_trans_condo.transportation_id,
					trans.transportation_category_id,
					trans.transportation_title,
					trans.publish,
					')
                ->join('transportation trans', 'trans.transportation_id = rel_trans_condo.transportation_id', 'left')
                ->where('trans.publish', 2);

            if (!empty($bts)) {
                $query_rel = $this->db->like('trans.transportation_title', trim($bts), 'both');
            } elseif (!empty($data['mrt'])) {
                $query_rel = $this->db->like('trans.transportation_title', trim($mrt), 'both');
            } elseif (!empty($data['airportlink'])) {
                $query_rel = $this->db->like('trans.transportation_title', trim($airportlink), 'both');
            } else {
                $query_rel = $this->db->like('trans.transportation_title', trim($transportation_category), 'both');
            }

            $query_rel = $this->db->get('condo_relation_transportation rel_trans_condo');

            if ($query_rel->num_rows() > 0) {
                $string_arr = array();
                foreach ($query_rel->result() as $row_rel) {
                    $query_parts[] = $row_rel->condo_id;
                }
                /* STEP 1 */
                $string_arr = implode("'".','."'", $query_parts);
                $condo_id = array($string_arr);
            }else{
              $string_arr = '';
            }
        }
        // END IF SEARCH : TRANSPORT CATEGORY
        //------------------------------------------

        //---------------------------------------------
        // SEARCH TAB : 4 ค้นหาจากสถานที่สำคัญ
        // SEARCH : TRANSPORTATION_ID
        //---------------------------------------------
        if (!empty($transportation_id)) {
            $query_rel = $this->db
                ->select('
					rel_trans_condo.condo_id,
					rel_trans_condo.transportation_id,
					trans.transportation_category_id,
					trans.transportation_id,
					trans.transportation_title,
					trans.publish,
				')
                ->join('transportation trans', 'trans.transportation_id = rel_trans_condo.transportation_id', 'left')
                ->where('trans.transportation_id', $transportation_id)
                ->where('trans.publish', 2);
            $query_rel = $this->db->get('condo_relation_transportation rel_trans_condo');
            if ($query_rel->num_rows() > 0) {
                $string_arr = array();
                foreach ($query_rel->result() as $row_rel) {
                    $query_parts[] = $row_rel->condo_id;
                }
                /* STEP 1 */
                $string_arr = implode("'".','."'", $query_parts);
                $condo_id = array($string_arr);
            }else{
              $string_arr = '';
            }
        }
        // END IF SEARCH : TRANSPORTATION_ID
        //------------------------------------------

        $query = $this->db
            ->select('
				condo.condo_id,
				condo.staff_id,
				condo.condo_property_id,
				condo.zone_id,
				condo.condo_title,
				condo.type_offering_id,
				condo.type_suites_id,
				condo.provinces_id,
				condo.districts_id,
				condo.sub_districts_id,
				condo.condo_all_room,
				condo.condo_all_toilet,
				condo.condo_interior_room,
				condo.condo_direction_room,
				condo.condo_scenery_room,
				condo.condo_expert,
				condo.condo_description,
				condo.pic_thumb,
				condo.condo_area_apartment,
				condo.condo_alley,
				condo.condo_road,
				condo.condo_total_price,
				condo.condo_price_per_square_meter,
				condo.condo_tag,
				condo.visited,
				condo.createdate,
				condo.modifydate,
			');

        //---------------------------------------------
        // SEARCH TAB : 3 ค้นหาจากทำเล
        // SEARCH : TYPE ZONE
        //---------------------------------------------
        if (!empty($type_zone_id)) {
            $query = $this->db->select('type_zone.type_zone_title');
            $query = $this->db->join('type_zone', 'type_zone.type_zone_id = condo.type_zone_id', 'left');
        }
        // END IF SEARCH : TYPE ZONE
        //------------------------------------------



        //------------------------------------------
        //		WHERE MAIN
        $query = $this->db->where('condo.approved', 2);
        //------------------------------------------

        // ๋JOIN TABLE
        if (!empty($min_price) || !empty($max_price) || !empty($type_offering) || !empty($type_suites) || !empty($condo_area_apartment) || !empty($keyword) || !empty($condo_room)) {
            $query = $this->db
                ->join('type_zone', 'type_zone.type_zone_id = condo.type_zone_id', 'left')
                ->join('provinces', 'provinces.provinces_id = condo.provinces_id', 'left')
                ->join('districts', 'districts.districts_id = condo.districts_id', 'left')
                ->join('sub_districts', 'sub_districts.sub_districts_id = condo.sub_districts_id', 'left');
        }//END IF JOIN TABLE
        // -----------------------------------------------------

        //---------------------------------------------
        // SEARCH FOOTER : 1 ค้นหาจากชื่อเจ้าของโครงการ
        // SEARCH : CONDO OWNER NAME
        //---------------------------------------------
        if (!empty($condo_owner_name)) {
          $query = $this->db->like('condo.condo_owner_name', trim($condo_owner_name), 'both');
        }
        // END IF SEARCH : TYPE ZONE
        //------------------------------------------

        if (!empty($keyword)) {
            $query = $this->db
                ->like('condo.condo_title', trim($keyword), 'both')
                ->or_like('condo.condo_expert', trim($keyword), 'both')
                ->or_like('condo.condo_description', trim($keyword), 'both')
                ->or_like('condo.condo_alley', trim($keyword), 'both')
                ->or_like('condo.condo_road', trim($keyword), 'both')
                ->or_like('condo.condo_address', trim($keyword), 'both')
                ->or_like('condo.condo_tag', trim($keyword), 'both')
                ->or_like('condo.condo_total_price', trim($keyword), 'both')
                ->or_like('type_zone.type_zone_title', trim($keyword), 'both')
                ->or_like('provinces.provinces_name', trim($keyword), 'both')
                ->or_like('districts.districts_name', trim($keyword), 'both')
                ->or_like('sub_districts.sub_districts_name', trim($keyword), 'both');

            if (!empty($min_price) && !empty($max_price)) {
                $query = $this->db->having("condo.condo_total_price BETWEEN $min_price AND $max_price ", null, false);
            } elseif (!empty($min_price) || !empty($max_price)) {
                if (!empty($min_price)) {
                    $query = $this->db->having('condo.condo_total_price <=', $min_price);
                }
                if (!empty($max_price)) {
                    if ($max_price === '20000001') {
                        $query = $this->db->where('condo.condo_total_price >=', $max_price);
                    } else {
                        $query = $this->db->where('condo.condo_total_price <=', $max_price);
                    }
                }
            }

            if (!empty($type_offering)) {
                $query = $this->db->where('condo.type_offering_id =', $type_offering);
            }

            if (!empty($type_suites)) {
                $query = $this->db->where('condo.type_suites_id =', $type_suites);
            }

            if (!empty($condo_area_apartment)) {
                if ($condo_area_apartment === '1') {
                    $query = $this->db->where('condo.condo_area_apartment <= 25 ');
                }
                if ($condo_area_apartment === '2') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 26.00 AND 50.00 ', null, false);
                }
                if ($condo_area_apartment === '3') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 51.00  AND 75.00 ', null, false);
                }
                if ($condo_area_apartment === '4') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 76.00 AND 100.00 ', null, false);
                }
                if ($condo_area_apartment === '5') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 101.00  AND 125.00 ', null, false);
                }
                if ($condo_area_apartment === '6') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 126.00  AND 150.00 ', null, false);
                }
                if ($condo_area_apartment === '7') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 151.00 AND 175.00 ', null, false);
                }
                if ($condo_area_apartment === '8') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 176.00  AND 200.00 ', null, false);
                }
                if ($condo_area_apartment === '9') {
                    $query = $this->db->where('condo.condo_area_apartment >= 201 ');
                }
            }

            //CONDO ROOM
            if (!empty($condo_room)) {
                if ($condo_room === '1') {
                    $query = $this->db->where('condo.condo_all_room = 1 ', null, false);
                } elseif ($condo_room === '2') {
                    $query = $this->db->where('condo.condo_all_room = 2 ', null, false);
                } else {
                    $query = $this->db->where('condo.condo_all_room = 3 ', null, false);
                }
            }
            $query = $this->db->group_by('condo.condo_total_price');
            // END IF KEYWORD
            // -----------------------------------------------------
        } else {
            //ELSE KEYWORD

            if (!empty($min_price) && !empty($max_price)) {
                $query = $this->db->where("condo.condo_total_price BETWEEN $min_price AND $max_price ", null, false);
            } elseif (!empty($min_price) || !empty($max_price)) {
                if (!empty($min_price)) {
                    $query = $this->db->where('condo.condo_total_price <=', $min_price);
                }
                if (!empty($max_price)) {
                    if ($max_price === '20000001') {
                        $query = $this->db->where('condo.condo_total_price >=', $max_price);
                    } else {
                        $query = $this->db->where('condo.condo_total_price <=', $max_price);
                    }
                }
            }
            if (!empty($type_offering)) {
                $query = $this->db->where('condo.type_offering_id =', $type_offering);
            }

            if (!empty($type_suites)) {
                $query = $this->db->where('condo.type_suites_id =', $type_suites);
            }

            if (!empty($condo_area_apartment)) {
                if ($condo_area_apartment === '1') {
                    $query = $this->db->where('condo.condo_area_apartment <= 25 ');
                }
                if ($condo_area_apartment === '2') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 26.00 AND 50.00 ', null, false);
                }
                if ($condo_area_apartment === '3') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 51.00  AND 75.00 ', null, false);
                }
                if ($condo_area_apartment === '4') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 76.00 AND 100.00 ', null, false);
                }
                if ($condo_area_apartment === '5') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 101.00  AND 125.00 ', null, false);
                }
                if ($condo_area_apartment === '6') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 126.00  AND 150.00 ', null, false);
                }
                if ($condo_area_apartment === '7') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 151.00 AND 175.00 ', null, false);
                }
                if ($condo_area_apartment === '8') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 176.00  AND 200.00 ', null, false);
                }
                if ($condo_area_apartment === '9') {
                    $query = $this->db->where('condo.condo_area_apartment >= 201 ');
                }
            }

            //CONDO ROOM
            if (!empty($condo_room)) {
                if ($condo_room === '1') {
                    $query = $this->db->where('condo.condo_all_room = 1 ', null, false);
                } elseif ($condo_room === '2') {
                    $query = $this->db->where('condo.condo_all_room = 2 ', null, false);
                } else {
                    $query = $this->db->where('condo.condo_all_room = 3 ', null, false);
                }
            }
            // END IF MIN PRICE & MAX PRICE
            // -----------------------------------------------------
        }

        //---------------------------------------------
        // SEARCH TAB : 2 ค้นหาจากระบบขนส่งมวลชน
        // SEARCH : MASS TRANSPORT CATEGORY
        // และ
        // SEARCH TAB : 4 ค้นหาจากสถานที่สำคัญ
        // SEARCH : TRANSPORTATION_ID
        //---------------------------------------------
        if (!empty($transportation_category) || !empty($transportation_id) || !empty($mass_transportation_id)) {
            $query = $this->db->where("condo.condo_id in ('$string_arr')");
        }
        // END IF SEARCH : MASS TRANSPORT CATEGORY
        // END IF SEARCH : TRANSPORT CATEGORY
        // END IF SEARCH : TRANSPORTATION_ID
        //------------------------------------------

        //---------------------------------------------
        // SEARCH TAB : 3 ค้นหาจากทำเล
        // SEARCH : TYPE ZONE
        //---------------------------------------------
        if (!empty($type_zone_id)) {
            $query = $this->db->where('condo.type_zone_id=', $type_zone_id);
        }
        // END IF SEARCH : TYPE ZONE
        //------------------------------------------

        // SEARCH TAB : 4 ค้นหาจากสถานที่สำคัญ
        // SEARCH : TRANSPORTATION_ID
        // ไปรวมเงื่อนไขกับ SEARCH 2

        // SORT ORDER : ตามผู้ใช้
        if ($order_by_price_visited === '1') {
            $query = $this->db->order_by('condo.condo_total_price', 'DESC');
        } elseif ($order_by_price_visited === '2') {
            $query = $this->db->order_by('condo.condo_total_price', 'ASC');
        } elseif ($order_by_price_visited === '3') {
            $query = $this->db->order_by('condo.visited', 'DESC');
        } else {
            $query = $this->db->order_by('condo.createdate', 'DESC');
        }

        $query = $this->db
                ->limit($limit, $start)
                ->get('condo');
        // echo $this->db->last_query();

        if ($query->num_rows() > 0) {
            // $result = array('success'=>TRUE, 'data'=>$query, 'condo_total'=>$query->num_rows());
            $result = $query;
        }

        return $result;
    }
    //....................................................................................................................................

    public function get_search($data, $limit, $start)
    {
        // $this->primaryclass->pre_var_dump($data);
        $result = array();
        if (empty($data)) {
            return array();
        }
// echo $data['keyword'];
        //$query = '';

        if (empty($data)) {
            $query = $this->db
                ->select('
					cd.condo_id,
					cd.staff_id,
					cd.condo_property_id,
					cd.zone_id,
					cd.condo_title,
					cd.type_offering_id,
					cd.type_suites_id,
					cd.provinces_id,
					cd.districts_id,
					cd.sub_districts_id,
					cd.condo_all_room,
					cd.condo_all_toilet,
					cd.condo_interior_room,
					cd.condo_direction_room,
					cd.condo_scenery_room,
					cd.condo_expert,
					cd.condo_description,
					cd.pic_thumb,
					cd.condo_area_apartment,
					cd.condo_total_price,
					cd.condo_price_per_square_meter,
					cd.condo_tag,
					cd.visited,
					cd.createdate,
					cd.modifydate,
				')
                ->where('cd.approved', 2)
                ->get('condo cd');
                // echo $this->db->last_query();
                //echo $query->num_rows();
                if ($query->num_rows() > 0) {
                    return $result = $query;
                } else {
                    return $result;
                }
        } elseif (!empty($data['min_price'])
            || !empty($data['max_price'])
            || !empty($data['type_offering'])
            || !empty($data['type_suites'])
            || !empty($data['condo_area_apartment'])
            || !empty($data['keyword'])
            || !empty($data['condo_room'])) {
            $query = $this->db
                ->select('
					cd.condo_id,
					cd.staff_id,
					cd.condo_property_id,
					cd.type_zone_id,
					cd.zone_id,
					cd.condo_title,
					cd.type_offering_id,
					cd.type_suites_id,
					cd.provinces_id,
					cd.districts_id,
					cd.sub_districts_id,
					cd.condo_all_room,
					cd.condo_all_toilet,
					cd.condo_interior_room,
					cd.condo_direction_room,
					cd.condo_scenery_room,
					cd.condo_expert,
					cd.condo_description,
					cd.pic_thumb,
					cd.condo_floor,
					cd.condo_area_apartment,
					cd.condo_total_price,
					cd.condo_price_per_square_meter,
					cd.condo_tag,
					cd.visited,
					cd.createdate,
					cd.modifydate,
					tz.type_zone_title,
					prov.provinces_name,
					dist.districts_name,
					subdist.sub_districts_name,
					')
                ->join('type_zone tz', 'tz.type_zone_id = cd.type_zone_id', 'left')
                ->join('provinces prov', 'prov.provinces_id = cd.provinces_id', 'left')
                ->join('districts dist', 'dist.districts_id = cd.districts_id', 'left')
                ->join('sub_districts subdist', 'subdist.sub_districts_id = cd.sub_districts_id', 'left')
                ->where('cd.approved', 2);

            if (!empty($data['keyword'])) {
                $query = $this->db
                        ->like('cd.condo_title', trim($data['keyword']), 'both')
                        ->or_like('cd.condo_expert', trim($data['keyword']), 'both')
                        ->or_like('cd.condo_description', trim($data['keyword']), 'both')
                        ->or_like('cd.condo_tag', trim($data['keyword']), 'both')
                        ->or_like('tz.type_zone_title', trim($data['keyword']), 'both')
                        ->or_like('prov.provinces_name', trim($data['keyword']), 'both')
                        ->or_like('dist.districts_name', trim($data['keyword']), 'both')
                        ->or_like('subdist.sub_districts_name', trim($data['keyword']), 'both');
                        //->limit($limit, $start)

                    if (!empty($data['min_price']) && !empty($data['max_price'])) {
                        // $query = $this->db->where("cd.condo_total_price BETWEEN $data[min_price] AND $data[max_price] ", null, false);
                        $query = $this->db->having("cd.condo_total_price BETWEEN $data[min_price] AND $data[max_price] ", null, false);
                    } elseif (!empty($data['min_price']) || !empty($data['max_price'])) {
                        if (!empty($data['min_price'])) {
                            // $query = $this->db->where("cd.condo_total_price >=", $data['min_price']);
                            $query = $this->db->having('cd.condo_total_price <=', $data['min_price']);
                        }
                        if (!empty($data['max_price'])) {
                            // $query = $this->db->where("cd.condo_total_price <=", $data['max_price']);
                            // $query = $this->db->having("cd.condo_total_price >=", $data['max_price']);
                            if ($data['max_price'] === '20000001') {
                                $query = $this->db->where('cd.condo_total_price >=', $data['max_price']);
                            } else {
                                $query = $this->db->where('cd.condo_total_price <=', $data['max_price']);
                            }
                        }
                    }

                if (!empty($data['type_offering'])) {
                    $query = $this->db->where('cd.type_offering_id =', $data['type_offering']);
                }

                if (!empty($data['type_suites'])) {
                    $query = $this->db->where('cd.type_suites_id =', $data['type_suites']);
                }

                if (!empty($data['condo_area_apartment'])) {
                    $condo_area_apartment = $data['condo_area_apartment'];
                    if ($condo_area_apartment === '1') {
                        $query = $this->db->where('cd.condo_area_apartment <= 25 ');
                    }
                    if ($condo_area_apartment === '2') {
                        $query = $this->db->where('cd.condo_area_apartment BETWEEN 26.00 AND 50.00 ', null, false);
                    }
                    if ($condo_area_apartment === '3') {
                        $query = $this->db->where('cd.condo_area_apartment BETWEEN 51.00  AND 75.00 ', null, false);
                    }
                    if ($condo_area_apartment === '4') {
                        $query = $this->db->where('cd.condo_area_apartment BETWEEN 76.00 AND 100.00 ', null, false);
                    }
                    if ($condo_area_apartment === '5') {
                        $query = $this->db->where('cd.condo_area_apartment BETWEEN 101.00  AND 125.00 ', null, false);
                    }
                    if ($condo_area_apartment === '6') {
                        $query = $this->db->where('cd.condo_area_apartment BETWEEN 126.00  AND 150.00 ', null, false);
                    }
                    if ($condo_area_apartment === '7') {
                        $query = $this->db->where('cd.condo_area_apartment BETWEEN 151.00 AND 175.00 ', null, false);
                    }
                    if ($condo_area_apartment === '8') {
                        $query = $this->db->where('cd.condo_area_apartment BETWEEN 176.00  AND 200.00 ', null, false);
                    }
                    if ($condo_area_apartment === '9') {
                        $query = $this->db->where('cd.condo_area_apartment >= 201 ');
                    }
                }

                    //Condo Floor
                    if (!empty($data['condo_room'])) {
                        $condo_room = $data['condo_room'];
                        if ($condo_room === '1') {
                            $query = $this->db->where('cd.condo_all_room = 1 ', null, false);
                        } elseif ($condo_room === '2') {
                            $query = $this->db->where('cd.condo_all_room = 2 ', null, false);
                        } else {
                            $query = $this->db->where('cd.condo_all_room = 3 ', null, false);
                        }
                    }

                $query = $this->db->group_by('cd.condo_total_price');
            }//ELSE KEYWORD
                else {
                    if (!empty($data['min_price']) && !empty($data['max_price'])) {
                        $query = $this->db->where("cd.condo_total_price BETWEEN $data[min_price] AND $data[max_price] ", null, false);
                    } elseif (!empty($data['min_price']) || !empty($data['max_price'])) {
                        if (!empty($data['min_price'])) {
                            $query = $this->db->where('cd.condo_total_price <=', $data['min_price']);
                        }
                        if (!empty($data['max_price'])) {
                            if ($data['max_price'] === '20000001') {
                                $query = $this->db->where('cd.condo_total_price >=', $data['max_price']);
                            } else {
                                $query = $this->db->where('cd.condo_total_price <=', $data['max_price']);
                            }
                        }
                    }

                    if (!empty($data['type_offering'])) {
                        $query = $this->db->where('cd.type_offering_id =', $data['type_offering']);
                    }

                    if (!empty($data['type_suites'])) {
                        $query = $this->db->where('cd.type_suites_id =', $data['type_suites']);
                    }

                    //CONDO Area Apartment : รับ parameter ต่างๆมา
                    if (!empty($data['condo_area_apartment'])) {
                        $condo_area_apartment = $data['condo_area_apartment'];
                        if ($condo_area_apartment === '1') {
                            $query = $this->db->where('cd.condo_area_apartment <= 25 ');
                        }
                        if ($condo_area_apartment === '2') {
                            $query = $this->db->where('cd.condo_area_apartment BETWEEN 26.00 AND 50.00 ', null, false);
                        }
                        if ($condo_area_apartment === '3') {
                            $query = $this->db->where('cd.condo_area_apartment BETWEEN 51.00  AND 75.00 ', null, false);
                        }
                        if ($condo_area_apartment === '4') {
                            $query = $this->db->where('cd.condo_area_apartment BETWEEN 76.00 AND 100.00 ', null, false);
                        }
                        if ($condo_area_apartment === '5') {
                            $query = $this->db->where('cd.condo_area_apartment BETWEEN 101.00  AND 125.00 ', null, false);
                        }
                        if ($condo_area_apartment === '6') {
                            $query = $this->db->where('cd.condo_area_apartment BETWEEN 126.00  AND 150.00 ', null, false);
                        }
                        if ($condo_area_apartment === '7') {
                            $query = $this->db->where('cd.condo_area_apartment BETWEEN 151.00 AND 175.00 ', null, false);
                        }
                        if ($condo_area_apartment === '8') {
                            $query = $this->db->where('cd.condo_area_apartment BETWEEN 176.00  AND 200.00 ', null, false);
                        }
                        if ($condo_area_apartment === '9') {
                            $query = $this->db->where('cd.condo_area_apartment >= 201 ');
                        }
                    }

                    //Condo Floor
                    if (!empty($data['condo_room'])) {
                        $condo_room = $data['condo_room'];
                        if ($condo_room === '1') {
                            $query = $this->db->where('cd.condo_all_room = 1 ', null, false);
                        } elseif ($condo_room === '2') {
                            $query = $this->db->where('cd.condo_all_room = 2 ', null, false);
                        } else {
                            $query = $this->db->where('cd.condo_all_room = 3 ', null, false);
                        }
                    }
                }//END IF KEYWORD

                $query = $this->db
                    ->get('condo cd');

    // echo $this->db->last_query();
    //echo $query->num_rows();
            if ($query->num_rows() > 0) {
                return $result = $query;
            } else {
                return $result;
            }
        }

        // SEARCH : TRANSPORT CATEGORY
        if (!empty($data['transportation_category'])) {
            $query_rel = $this->db
                ->select('
					rel_trans_condo.condo_id,
					rel_trans_condo.transportation_id,
					trans.transportation_category_id,
					trans.transportation_title,
					trans.publish,
					')
                ->join('transportation trans', 'trans.transportation_id = rel_trans_condo.transportation_id', 'left')
                ->where('trans.publish', 2);

            if (!empty($data['bts'])) {
                $query_rel = $this->db
                    ->like('trans.transportation_title', trim($data['bts']), 'both');
            } elseif (!empty($data['mrt'])) {
                $query_rel = $this->db
                    ->like('trans.transportation_title', trim($data['mrt']), 'both');
            } elseif (!empty($data['airportlink'])) {
                $query_rel = $this->db
                    ->like('trans.transportation_title', trim($data['airportlink']), 'both');
            } else {
                $query_rel = $this->db
                    ->like('trans.transportation_title', trim($data['transportation_category']), 'both');
            }
            $query_rel = $this->db->get('condo_relation_transportation rel_trans_condo');

            if ($query_rel->num_rows() > 0) {
                $string_arr = array();
                foreach ($query_rel->result() as $row_rel) {
                    $query_parts[] = $row_rel->condo_id;
                }
                /* STEP 1 */
                $string_arr = implode("'".','."'", $query_parts);
                // var_dump($string_arr);
                // while ($row_rel = $query_rel->unbuffered_row())
                // {

                    // $rel_trans_condo_id =  $row_rel->condo_id;
                    $query = $this->db
                        ->select('
							cd.condo_id,
							cd.staff_id,
							cd.condo_property_id,
							cd.zone_id,
							cd.condo_title,
							cd.type_offering_id,
							cd.provinces_id,
							cd.districts_id,
							cd.sub_districts_id,
							cd.condo_all_room,
							cd.condo_all_toilet,
							cd.condo_interior_room,
							cd.condo_direction_room,
							cd.condo_scenery_room,
							cd.condo_expert,
							cd.condo_description,
							cd.pic_thumb,
							cd.condo_area_apartment,
							cd.condo_total_price,
							cd.condo_price_per_square_meter,
							cd.condo_tag,
							cd.visited,
							cd.createdate,
							cd.modifydate,
						');

                $condo_id = array($string_arr);
                $query = $this->db
                        ->where("cd.condo_id in ('$string_arr')");

                $query = $this->db
                        ->where('cd.approved', 2)
                        ->get('condo cd');
                        //echo $this->db->last_query();
                // }
                return $result = $query;
            } else {
                return $result;
            }
        }

        //Zone id : Search 3
        if (!empty($data['type_zone_id'])) {
            $query = $this->db
                ->select('
					cd.condo_id,
					cd.staff_id,
					cd.condo_property_id,
					cd.type_zone_id,
					cd.zone_id,
					cd.condo_title,
					cd.type_offering_id,
					cd.provinces_id,
					cd.districts_id,
					cd.sub_districts_id,
					cd.condo_all_room,
					cd.condo_all_toilet,
					cd.condo_interior_room,
					cd.condo_direction_room,
					cd.condo_scenery_room,
					cd.condo_expert,
					cd.condo_description,
					cd.pic_thumb,
					cd.condo_floor,
					cd.condo_area_apartment,
					cd.condo_total_price,
					cd.condo_price_per_square_meter,
					cd.condo_tag,
					cd.visited,
					cd.createdate,
					cd.modifydate,
					tz.type_zone_title,
					')
                ->join('type_zone tz', 'tz.type_zone_id = cd.type_zone_id', 'left')
                ->where('cd.type_zone_id =', $data['type_zone_id'])
                ->where('cd.approved', 2)
                ->get('condo cd');

                // echo $this->db->last_query();
                //echo $query->num_rows();
                if ($query->num_rows() > 0) {
                    return $result = $query;
                } else {
                    return $result;
                }
        }

        //Transport : Search 4
        if (!empty($data['transportation_id'])) {
            $query_rel = $this->db
                ->select('
					rel_trans_condo.condo_id,
					rel_trans_condo.transportation_id,
					trans.transportation_category_id,
					trans.transportation_id,
					trans.transportation_title,
					trans.publish,
					')
                ->join('transportation trans', 'trans.transportation_id = rel_trans_condo.transportation_id', 'left')
                ->where('trans.transportation_id', $data['transportation_id'])
                ->where('trans.publish', 2);
            $query_rel = $this->db->get('condo_relation_transportation rel_trans_condo');

            if ($query_rel->num_rows() > 0) {
                $string_arr = array();
                foreach ($query_rel->result() as $row_rel) {
                    $query_parts[] = $row_rel->condo_id;
                }
                /* STEP 1 */
                $string_arr = implode("'".','."'", $query_parts);

                $query = $this->db
                        ->select('
							cd.condo_id,
							cd.staff_id,
							cd.condo_property_id,
							cd.zone_id,
							cd.condo_title,
							cd.type_offering_id,
							cd.type_suites_id,
							cd.provinces_id,
							cd.districts_id,
							cd.sub_districts_id,
							cd.condo_all_room,
							cd.condo_all_toilet,
							cd.condo_interior_room,
							cd.condo_direction_room,
							cd.condo_scenery_room,
							cd.condo_expert,
							cd.condo_description,
							cd.pic_thumb,
							cd.condo_area_apartment,
							cd.condo_total_price,
							cd.condo_price_per_square_meter,
							cd.condo_tag,
							cd.visited,
							cd.createdate,
							cd.modifydate,
						');

                $condo_id = array($string_arr);
                $query = $this->db
                        ->where("cd.condo_id in ('$string_arr')");

                $query = $this->db
                        ->where('cd.approved', 2)
                        ->get('condo cd');
                        //echo $this->db->last_query();
                // }
                return $result = $query;
            } else {
                return $result;
            }
        }

        //CONDO OWNER NAME Search 5
        if (!empty($data['condo_owner_name'])) {
            $query = $this->db
                ->select('
					cd.condo_id,
					cd.staff_id,
					cd.condo_property_id,
					cd.zone_id,
					cd.condo_title,
					cd.condo_owner_name,
					cd.type_offering_id,
					cd.type_suites_id,
					cd.provinces_id,
					cd.districts_id,
					cd.sub_districts_id,
					cd.condo_all_room,
					cd.condo_all_toilet,
					cd.condo_interior_room,
					cd.condo_direction_room,
					cd.condo_scenery_room,
					cd.condo_expert,
					cd.condo_description,
					cd.pic_thumb,
					cd.condo_area_apartment,
					cd.condo_total_price,
					cd.condo_price_per_square_meter,
					cd.condo_tag,
					cd.visited,
					cd.createdate,
					cd.modifydate,
				')
                ->where('cd.approved', 2)
                ->like('cd.condo_owner_name', trim($data['condo_owner_name']), 'both')
                ->get('condo cd');

                // echo $this->db->last_query();
                //echo $query->num_rows();
                if ($query->num_rows() > 0) {
                    return $result = $query;
                } else {
                    return $result;
                }
        }
    }
    //....................................................................................................................................

    public function condo_search_record_count()
    {

        // CLEAR DATA
        $data = $this->security->xss_clean($this->input->get());
        // $this->primaryclass->pre_var_dump($data);
        // KEYWORD : SEARCH
        // ต้องค้นหาซอย ถนน จังหวัด ได้
        // SEARCH TABS : 1
        $keyword = (!empty($data['search'])) ? $data['search'] : null;
        $min_price = (!empty($data['min_price'])) ? $data['min_price'] : '';
        $max_price = (!empty($data['max_price'])) ? $data['max_price'] : '';
        $type_offering = (!empty($data['type_offering'])) ? $data['type_offering'] : '';
        $type_suites = (!empty($data['type_suites'])) ? $data['type_suites'] : '';
        $condo_room = (!empty($data['condo_room'])) ? $data['condo_room'] : '';
        $condo_area_apartment = (!empty($data['condo_area_apartment'])) ? $data['condo_area_apartment'] : '';
        //---------------------------------------------
        // SEARCH TABS : 2
        $mass_transportation_id = (!empty($data['mass_transportation_id'])) ? $data['mass_transportation_id'] : '';
        $transportation_category = (!empty($data['transportation_category'])) ? $data['transportation_category'] : '';
        $bts = (!empty($data['bts'])) ? $data['bts'] : '';
        $mrt = (!empty($data['mrt'])) ? $data['mrt'] : '';
        $airportlink = (!empty($data['airportlink'])) ? $data['airportlink'] : '';
        //---------------------------------------------
        // SEARCH TABS : 3
        $type_zone_id = (!empty($data['type_zone_id'])) ? $data['type_zone_id'] : '';
        //---------------------------------------------
        // SEARCH TABS :  4
        $transportation_id = (!empty($data['transportation_id'])) ? $data['transportation_id'] : '';
        //---------------------------------------------
        // SEARCH FOOTER :
        $condo_owner_name = (!empty($data['condo_owner_name'])) ? $data['condo_owner_name'] : '';
        //---------------------------------------------

        // ACTION SORT :  1
        $order_by_list = (!empty($data['order_by_list'])) ? $data['order_by_list'] : '';
        $order_by_price_visited = (!empty($data['order_by_price_visited'])) ? $data['order_by_price_visited'] : '';

        $result = 0;

        //---------------------------------------------
        // ไม่มีการค้นหา
        //---------------------------------------------

        //---------------------------------------------
        // SEARCH TAB : 2 ค้นหาจากระบบขนส่งมวลชน
        // SEARCH : MASS TRANSPORT CATEGORY
        //---------------------------------------------
        if (!empty($mass_transportation_id)) {
            $query_rel = $this->db
                ->select('
      					rel_mass_trans_condo.condo_id,
      					rel_mass_trans_condo.mass_transportation_id,
      					mass_trans.mass_transportation_id,
      					mass_trans.mass_transportation_title,
      					mass_trans.publish,
      					')
                ->join('mass_transportation mass_trans', 'mass_trans.mass_transportation_id = rel_mass_trans_condo.mass_transportation_id', 'left')
                ->where('mass_trans.publish', 2);

            $query_rel = $this->db->where('mass_trans.mass_transportation_id', $mass_transportation_id);
            $query_rel = $this->db->get('condo_relation_mass_transportation rel_mass_trans_condo');
            // echo $this->db->last_query();
            if ($query_rel->num_rows() > 0) {
              // echo $query_rel->num_rows();
                $string_arr = array();
                foreach ($query_rel->result() as $row_rel) {
                    $query_parts[] = $row_rel->condo_id;
                }
                /* STEP 1 */
                $string_arr = implode("'".','."'", $query_parts);
                $condo_id = array($string_arr);
            }
            else{
              $string_arr = '';
            }

        }
        // END IF SEARCH : MASS TRANSPORT CATEGORY
        //------------------------------------------

        //---------------------------------------------
        // SEARCH TAB : 2 ค้นหาจากระบบขนส่งมวลชน
        // SEARCH : TRANSPORT CATEGORY
        //---------------------------------------------
        if (!empty($transportation_category)) {
            $query_rel = $this->db
                ->select('
					rel_trans_condo.condo_id,
					rel_trans_condo.transportation_id,
					trans.transportation_category_id,
					trans.transportation_title,
					trans.publish,
					')
                ->join('transportation trans', 'trans.transportation_id = rel_trans_condo.transportation_id', 'left')
                ->where('trans.publish', 2);

            if (!empty($bts)) {
                $query_rel = $this->db->like('trans.transportation_title', trim($bts), 'both');
            } elseif (!empty($data['mrt'])) {
                $query_rel = $this->db->like('trans.transportation_title', trim($mrt), 'both');
            } elseif (!empty($data['airportlink'])) {
                $query_rel = $this->db->like('trans.transportation_title', trim($airportlink), 'both');
            } else {
                $query_rel = $this->db->like('trans.transportation_title', trim($transportation_category), 'both');
            }

            $query_rel = $this->db->get('condo_relation_transportation rel_trans_condo');

            if ($query_rel->num_rows() > 0) {
                $string_arr = array();
                foreach ($query_rel->result() as $row_rel) {
                    $query_parts[] = $row_rel->condo_id;
                }
                /* STEP 1 */
                $string_arr = implode("'".','."'", $query_parts);
                $condo_id = array($string_arr);
            }else{
              $string_arr = '';
            }
        }
        // END IF SEARCH : TRANSPORT CATEGORY
        //------------------------------------------

        //---------------------------------------------
        // SEARCH TAB : 4 ค้นหาจากสถานที่สำคัญ
        // SEARCH : TRANSPORTATION_ID
        //---------------------------------------------
        if (!empty($transportation_id)) {
            $query_rel = $this->db
                ->select('
					rel_trans_condo.condo_id,
					rel_trans_condo.transportation_id,
					trans.transportation_category_id,
					trans.transportation_id,
					trans.transportation_title,
					trans.publish,
				')
                ->join('transportation trans', 'trans.transportation_id = rel_trans_condo.transportation_id', 'left')
                ->where('trans.transportation_id', $transportation_id)
                ->where('trans.publish', 2);
            $query_rel = $this->db->get('condo_relation_transportation rel_trans_condo');
            if ($query_rel->num_rows() > 0) {
                $string_arr = array();
                foreach ($query_rel->result() as $row_rel) {
                    $query_parts[] = $row_rel->condo_id;
                }
                /* STEP 1 */
                $string_arr = implode("'".','."'", $query_parts);
                $condo_id = array($string_arr);
            }else{
              $string_arr = '';
            }
        }
        // END IF SEARCH : TRANSPORTATION_ID
        //------------------------------------------

        $query = $this->db
            ->select('
				condo.condo_id,
				condo.staff_id,
				condo.condo_property_id,
				condo.zone_id,
				condo.condo_title,
				condo.type_offering_id,
				condo.type_suites_id,
				condo.provinces_id,
				condo.districts_id,
				condo.sub_districts_id,
				condo.condo_all_room,
				condo.condo_all_toilet,
				condo.condo_interior_room,
				condo.condo_direction_room,
				condo.condo_scenery_room,
				condo.condo_expert,
				condo.condo_description,
				condo.pic_thumb,
				condo.condo_area_apartment,
				condo.condo_alley,
				condo.condo_road,
				condo.condo_total_price,
				condo.condo_price_per_square_meter,
				condo.condo_tag,
				condo.visited,
				condo.createdate,
				condo.modifydate,
			');

        //---------------------------------------------
        // SEARCH TAB : 3 ค้นหาจากทำเล
        // SEARCH : TYPE ZONE
        //---------------------------------------------
        if (!empty($type_zone_id)) {
            $query = $this->db->select('type_zone.type_zone_title');
            $query = $this->db->join('type_zone', 'type_zone.type_zone_id = condo.type_zone_id', 'left');
        }
        // END IF SEARCH : TYPE ZONE
        //------------------------------------------

        //------------------------------------------
        //		WHERE MAIN
        $query = $this->db->where('condo.approved', 2);
        //------------------------------------------

        // ๋JOIN TABLE
        if (!empty($min_price) || !empty($max_price) || !empty($type_offering) || !empty($type_suites) || !empty($condo_area_apartment) || !empty($keyword) || !empty($condo_room)) {
            $query = $this->db
                ->join('type_zone', 'type_zone.type_zone_id = condo.type_zone_id', 'left')
                ->join('provinces', 'provinces.provinces_id = condo.provinces_id', 'left')
                ->join('districts', 'districts.districts_id = condo.districts_id', 'left')
                ->join('sub_districts', 'sub_districts.sub_districts_id = condo.sub_districts_id', 'left');
        }//END IF JOIN TABLE
        // -----------------------------------------------------

        //---------------------------------------------
        // SEARCH FOOTER : 1 ค้นหาจากชื่อเจ้าของโครงการ
        // SEARCH : CONDO OWNER NAME
        //---------------------------------------------
        if (!empty($condo_owner_name)) {
          $query = $this->db->like('condo.condo_owner_name', trim($condo_owner_name), 'both');
        }

        if (!empty($keyword)) {
            $query = $this->db
                ->like('condo.condo_title', trim($keyword), 'both')
                ->or_like('condo.condo_expert', trim($keyword), 'both')
                ->or_like('condo.condo_description', trim($keyword), 'both')
                ->or_like('condo.condo_alley', trim($keyword), 'both')
                ->or_like('condo.condo_road', trim($keyword), 'both')
                ->or_like('condo.condo_address', trim($keyword), 'both')
                ->or_like('condo.condo_tag', trim($keyword), 'both')
                ->or_like('condo.condo_total_price', trim($keyword), 'both')
                ->or_like('type_zone.type_zone_title', trim($keyword), 'both')
                ->or_like('provinces.provinces_name', trim($keyword), 'both')
                ->or_like('districts.districts_name', trim($keyword), 'both')
                ->or_like('sub_districts.sub_districts_name', trim($keyword), 'both');

            if (!empty($min_price) && !empty($max_price)) {
                $query = $this->db->having("condo.condo_total_price BETWEEN $min_price AND $max_price ", null, false);
            } elseif (!empty($min_price) || !empty($max_price)) {
                if (!empty($min_price)) {
                    $query = $this->db->having('condo.condo_total_price <=', $min_price);
                }
                if (!empty($max_price)) {
                    if ($max_price === '20000001') {
                        $query = $this->db->where('condo.condo_total_price >=', $max_price);
                    } else {
                        $query = $this->db->where('condo.condo_total_price <=', $max_price);
                    }
                }
            }

            if (!empty($type_offering)) {
                $query = $this->db->where('condo.type_offering_id =', $type_offering);
            }

            if (!empty($type_suites)) {
                $query = $this->db->where('condo.type_suites_id =', $type_suites);
            }

            if (!empty($condo_area_apartment)) {
                if ($condo_area_apartment === '1') {
                    $query = $this->db->where('condo.condo_area_apartment <= 25 ');
                }
                if ($condo_area_apartment === '2') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 26.00 AND 50.00 ', null, false);
                }
                if ($condo_area_apartment === '3') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 51.00  AND 75.00 ', null, false);
                }
                if ($condo_area_apartment === '4') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 76.00 AND 100.00 ', null, false);
                }
                if ($condo_area_apartment === '5') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 101.00  AND 125.00 ', null, false);
                }
                if ($condo_area_apartment === '6') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 126.00  AND 150.00 ', null, false);
                }
                if ($condo_area_apartment === '7') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 151.00 AND 175.00 ', null, false);
                }
                if ($condo_area_apartment === '8') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 176.00  AND 200.00 ', null, false);
                }
                if ($condo_area_apartment === '9') {
                    $query = $this->db->where('condo.condo_area_apartment >= 201 ');
                }
            }

            //CONDO ROOM
            if (!empty($condo_room)) {
                if ($condo_room === '1') {
                    $query = $this->db->where('condo.condo_all_room = 1 ', null, false);
                } elseif ($condo_room === '2') {
                    $query = $this->db->where('condo.condo_all_room = 2 ', null, false);
                } else {
                    $query = $this->db->where('condo.condo_all_room = 3 ', null, false);
                }
            }
            $query = $this->db->group_by('condo.condo_total_price');
            // END IF KEYWORD
            // -----------------------------------------------------
        } else {
            //ELSE KEYWORD

            if (!empty($min_price) && !empty($max_price)) {
                $query = $this->db->where("condo.condo_total_price BETWEEN $min_price AND $max_price ", null, false);
            } elseif (!empty($min_price) || !empty($max_price)) {
                if (!empty($min_price)) {
                    $query = $this->db->where('condo.condo_total_price <=', $min_price);
                }
                if (!empty($max_price)) {
                    if ($max_price === '20000001') {
                        $query = $this->db->where('condo.condo_total_price >=', $max_price);
                    } else {
                        $query = $this->db->where('condo.condo_total_price <=', $max_price);
                    }
                }
            }
            if (!empty($type_offering)) {
                $query = $this->db->where('condo.type_offering_id =', $type_offering);
            }

            if (!empty($type_suites)) {
                $query = $this->db->where('condo.type_suites_id =', $type_suites);
            }

            if (!empty($condo_area_apartment)) {
                if ($condo_area_apartment === '1') {
                    $query = $this->db->where('condo.condo_area_apartment <= 25 ');
                }
                if ($condo_area_apartment === '2') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 26.00 AND 50.00 ', null, false);
                }
                if ($condo_area_apartment === '3') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 51.00  AND 75.00 ', null, false);
                }
                if ($condo_area_apartment === '4') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 76.00 AND 100.00 ', null, false);
                }
                if ($condo_area_apartment === '5') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 101.00  AND 125.00 ', null, false);
                }
                if ($condo_area_apartment === '6') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 126.00  AND 150.00 ', null, false);
                }
                if ($condo_area_apartment === '7') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 151.00 AND 175.00 ', null, false);
                }
                if ($condo_area_apartment === '8') {
                    $query = $this->db->where('condo.condo_area_apartment BETWEEN 176.00  AND 200.00 ', null, false);
                }
                if ($condo_area_apartment === '9') {
                    $query = $this->db->where('condo.condo_area_apartment >= 201 ');
                }
            }

            //CONDO ROOM
            if (!empty($condo_room)) {
                if ($condo_room === '1') {
                    $query = $this->db->where('condo.condo_all_room = 1 ', null, false);
                } elseif ($condo_room === '2') {
                    $query = $this->db->where('condo.condo_all_room = 2 ', null, false);
                } else {
                    $query = $this->db->where('condo.condo_all_room = 3 ', null, false);
                }
            }
            // END IF MIN PRICE & MAX PRICE
            // -----------------------------------------------------
        }




        //---------------------------------------------
        // SEARCH TAB : 2 ค้นหาจากระบบขนส่งมวลชน
        // SEARCH : MASS TRANSPORT CATEGORY
        // และ
        // SEARCH TAB : 4 ค้นหาจากสถานที่สำคัญ
        // SEARCH : TRANSPORTATION_ID
        //---------------------------------------------
        if (!empty($transportation_category) || !empty($transportation_id) || !empty($mass_transportation_id)) {
            $query = $this->db->where("condo.condo_id in ('$string_arr')");
        }
        // END IF SEARCH : MASS TRANSPORT CATEGORY
        // END IF SEARCH : TRANSPORT CATEGORY
        // END IF SEARCH : TRANSPORTATION_ID
        //------------------------------------------

        //---------------------------------------------
        // SEARCH TAB : 3 ค้นหาจากทำเล
        // SEARCH : TYPE ZONE
        //---------------------------------------------
        if (!empty($type_zone_id)) {
            $query = $this->db->where('condo.type_zone_id=', $type_zone_id);
        }
        // END IF SEARCH : TYPE ZONE
        //------------------------------------------

        // SEARCH TAB : 4 ค้นหาจากสถานที่สำคัญ
        // SEARCH : TRANSPORTATION_ID
        // ไปรวมเงื่อนไขกับ SEARCH 2

        // SORT ORDER : ตามผู้ใช้
        if ($order_by_price_visited === '1') {
            $query = $this->db->order_by('condo.condo_total_price', 'DESC');
        } elseif ($order_by_price_visited === '2') {
            $query = $this->db->order_by('condo.condo_total_price', 'ASC');
        } elseif ($order_by_price_visited === '3') {
            $query = $this->db->order_by('condo.visited', 'DESC');
        } else {
            $query = $this->db->order_by('condo.createdate', 'DESC');
        }

        $query = $this->db->get('condo');
        // echo $this->db->last_query();

        if ($query->num_rows() > 0) {
            // $result = array('success'=>TRUE, 'data'=>$query, 'condo_total'=>$query->num_rows());
            $result = $query->num_rows();
        }

        return $result;
    }
    //....................................................................................................................................

    public function get_landlist($limit, $start)
    {
        $data = $this->security->xss_clean($this->input->get());
        // $this->primaryclass->pre_var_dump($data);

        // ACTION SEARCH
        $provinces_id = (!empty($data['provinces_id'])) ? $data['provinces_id'] : null;
        $min_price = (!empty($data['min_price'])) ? $data['min_price'] : '';
        $max_price = (!empty($data['max_price'])) ? $data['max_price'] : '';
        $type_offering = (!empty($data['type_offering'])) ? $data['type_offering'] : '';

        // ACTION SORT
        $order_by_list = (!empty($data['order_by_list'])) ? $data['order_by_list'] : '';
        $order_by_price_visited = (!empty($data['order_by_price_visited'])) ? $data['order_by_price_visited'] : '';

        $result = array();

        // STEP 1
        $query = $this->db
            ->select('land.*')
            ->where('land.approved', 2);

        // ---------------------------------------
        // SEARCH PROVINCES
        // ---------------------------------------
        if (!empty($provinces_id)) {
            $query = $this->db->where('provinces_id', $provinces_id);
        }

        // ---------------------------------------
        // SEARCH TYPE_OFFERING_ID
        // ---------------------------------------
        if (!empty($type_offering)) {
            $query = $this->db->where('land.type_offering_id', $type_offering);
        }

        // ---------------------------------------
        // SEARCH MIN PRINCE & MAX PRICE
        // ---------------------------------------
        if (!empty($min_price) && !empty($max_price)) {
            $query = $this->db->having("land.land_total_price BETWEEN $min_price AND $max_price ", null, false);
        }

        if (!empty($min_price) && empty($max_price)) {
            $query = $this->db->where('land.land_total_price <=', $min_price);
        }
        if (empty($min_price) && !empty($max_price)) {
            if ($max_price >= '20000001') {
                $query = $this->db->where('land.land_total_price >=', $max_price);
            } else {
                $query = $this->db->where('land.land_total_price <=', $max_price);
            }
        }

        // STEP 1
        $query = $this->db->limit($limit, $start);

        // echo $this->db->last_query();

        // SORT ORDER : ตามผู้ใช้
        if ($order_by_price_visited === '1') {
            $query = $this->db->order_by('land.land_total_price', 'DESC');
        } elseif ($order_by_price_visited === '2') {
            $query = $this->db->order_by('land.land_total_price', 'ASC');
        } elseif ($order_by_price_visited === '3') {
            $query = $this->db->order_by('land.visited', 'DESC');
        } else {
            $query = $this->db->order_by('land.createdate', 'DESC');
        }
        //	------------------------------------------
        //END QUERY
        //	------------------------------------------
        $query = $this->db->get('land');

        if ($query->num_rows() > 0) {
            $result = $query;
        }

        return $result;
    }
    //....................................................................................................................................

    public function land_all_record_count()
    {
        $data = $this->security->xss_clean($this->input->get());
        // $this->primaryclass->pre_var_dump($data);

        // ACTION SEARCH
        $provinces_id = (!empty($data['provinces_id'])) ? $data['provinces_id'] : null;
        $min_price = (!empty($data['min_price'])) ? $data['min_price'] : '';
        $max_price = (!empty($data['max_price'])) ? $data['max_price'] : '';
        $type_offering = (!empty($data['type_offering'])) ? $data['type_offering'] : '';

        // ACTION SORT
        $order_by_list = (!empty($data['order_by_list'])) ? $data['order_by_list'] : '';
        $order_by_price_visited = (!empty($data['order_by_price_visited'])) ? $data['order_by_price_visited'] : '';

        $result = 0;

        // STEP 1
        $query = $this->db
            ->select('land.*')
            ->where('land.approved', 2);

        // ---------------------------------------
        // SEARCH PROVINCES
        // ---------------------------------------
        if (!empty($provinces_id)) {
            $query = $this->db->where('land.provinces_id', $provinces_id);
        }

        // ---------------------------------------
        // SEARCH TYPE_OFFERING_ID
        // ---------------------------------------
        if (!empty($type_offering)) {
            $query = $this->db->where('land.type_offering_id', $type_offering);
        }

        // ---------------------------------------
        // SEARCH MIN PRINCE & MAX PRICE
        // ---------------------------------------
        if (!empty($min_price) && !empty($max_price)) {
            $query = $this->db->having("land.land_total_price BETWEEN $min_price AND $max_price ", null, false);
        }

        if (!empty($min_price) && empty($max_price)) {
            $query = $this->db->where('land.land_total_price <=', $min_price);
        }
        if (empty($min_price) && !empty($max_price)) {
            if ($max_price >= '20000001') {
                $query = $this->db->where('land.land_total_price >=', $max_price);
            } else {
                $query = $this->db->where('land.land_total_price <=', $max_price);
            }
        }

        // STEP 1
        $query = $this->db->order_by('land.createdate', 'DESC');

        // echo $this->db->last_query();

        // SORT ORDER : ตามผู้ใช้
        if ($order_by_price_visited === '1') {
            $query = $this->db->order_by('land.land_total_price', 'DESC');
        } elseif ($order_by_price_visited === '2') {
            $query = $this->db->order_by('land.land_total_price', 'ASC');
        } elseif ($order_by_price_visited === '3') {
            $query = $this->db->order_by('land.visited', 'DESC');
        } else {
            $query = $this->db->order_by('land.createdate', 'DESC');
        }
        //	------------------------------------------
        //END QUERY
        //	------------------------------------------
        $query = $this->db->get('land');

        if ($query->num_rows() > 0) {
            $result = $query->num_rows();
        }

        return $result;
    }
    //....................................................................................................................................

    public function condoinside($id)
    {
        $query = $this->db
            ->select('
                condo.*,
                type_zone.type_zone_title,
                type_decoration.type_decoration_title,
                type_offering.type_offering_title,
                type_offering.type_offering_images,
                type_suites.type_suites_title
            ')
            ->from('condo')
            ->where('condo.condo_id', $id)
            ->where('condo.approved', 2)
            ->limit(1)
            ->order_by('condo.createdate', 'DESC')
            ->join('type_zone', 'type_zone.type_zone_id = condo.type_zone_id', 'left')
            ->join('type_decoration', 'type_decoration.type_decoration_id = condo.type_decoration_id', 'left')
            ->join('type_offering', 'type_offering.type_offering_id = condo.type_offering_id', 'left')
            ->join('type_suites', 'type_suites.type_suites_id = condo.type_suites_id', 'left')
            ->get();

//		echo $this->db->queries[0];
        return $query->row();
    }
    //....................................................................................................................................

    public function landinside($id)
    {
        $query = $this->db
            ->select('land.*, type_zone.type_zone_title')
            ->from('land')
            ->where('land.land_id', $id)
            ->where('land.approved', 2)
            ->join('type_zone', 'type_zone.type_zone_id = land.type_zone_id', 'left')
            ->get();
		// echo $this->db->last_query();
        return $query->row();
    }
    //....................................................................................................................................

    public function get_news_home()
    {
        $result = array();
        $query = $this->db
            ->select('
				news_id,
				news_title,
				news_slug,
				news_caption,
				news_pic_thumb,
				createdate,
				updatedate,
				publish,
				news_order
			')
            ->where('publish', 2)
            ->where('recommended', 2)
            ->limit(3)
            ->get('news');
        if ($query->num_rows() > 0) {
            $result = $query->result();
        }

        return $result;
    }
    //....................................................................................................................................

    public function get_reviews_home()
    {
        $result = array();
        $query = $this->db
            ->select('
				reviews_id,
				reviews_title,
        reviews_slug,
				reviews_caption,
				reviews_pic_large,
				createdate,
				updatedate,
				publish,
				reviews_order
			')
            ->where('highlight', 2)
            ->where('publish', 2)
            ->order_by('createdate', 'desc')
            ->get('reviews');
        if ($query->num_rows() > 0) {
            $result = $query->row();
        }

        return $result;
    }
    //....................................................................................................................................

    public function get_activities_home()
    {
        $result = array();
        $query = $this->db
            ->select('
				activities_id,
				fk_activities_category_id,
				activities_title,
				activities_date,
				activities_days,
				activities_caption,
				activities_pic_thumb,
				highlight,
				recommended,
				publish,
				activities_order
			')
            ->where('publish', 2)
            ->order_by('createdate', 'DESC')
            ->limit(2)
            ->get('activities');
        if ($query->num_rows() > 0) {
            $result = $query->result();
        }

        return $result;
    }
    //...................................................................................................................................

    public function get_banner_location($banner_category_id)
    {
        $result = array();
        if ($banner_category_id !== '') {
            $query = $this->db
                ->select('
					banner.banner_id,
					banner.banner_title,
					banner.pic_thumb,
					banner.banner_url,
					banner.visited,
					banner.clicked,
					banner.displayed,
					banner.publish
				')
                ->where('banner.banner_category_id', $banner_category_id)
                ->where('banner.publish', 2)
                ->order_by('rand()') //RANDOM
                // ->order_by('banner.banner_id', 'ASC') //RANDOM
                // ->limit(1)
                ->get('banner');
            if ($query->num_rows() > 0) {
                $result = $query->result();
            }

            // var_dump($banner_displayed);
        } else {
            $query = $this->db->where('publish', 2);
            $query = $this->db->order_by('banner_id', 'ASC'); //RANDOM
            // $query =  $this->db->limit(1);
            $query = $this->db->get('banner');
            if ($query->num_rows() > 0) {
                $result = $query->result();
            }
        }

        return $result;
    }
    //....................................................................................................................................

    public function get_offering($type_offering_id = null)
    {
        $result = array();
        if ($type_offering_id !== '') {
            $query = $this->db
                ->select('
					type_offering_id,
					type_offering_title,
					type_offering_images,
					publish
				')
                ->where('publish', 2)
                ->where('type_offering_id', $type_offering_id)
                ->get('type_offering');
            // echo $this->db->last_query();
            $result = $query->row();
        } else {
            $query = $this->db->where('publish', 2);
            $query = $this->db->get('type_offering');
            $result = $query->row();
        }

        return $result;
    }
    //....................................................................................................................................
}
