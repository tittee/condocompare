<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner_model extends CI_Model {
    function __construct(){
        parent::__construct();
    }
    //....................................................................................................................................
    public function get_info( $banner_id = 0 )
    {
      $result = array();
      if( $banner_id !== 0)
      {
        $query = $this->db
          ->select('
            banner_id,
            clicked,
            visited,
            displayed
          ')
          ->where('banner_id', $banner_id)
          ->get('banner');
        $result = $query->row();
      }
      else
      {
          $query = $this->db
            ->select('
              banner_id,
              clicked,
              visited,
              displayed
            ')
            ->get('banner');
          $result = $query->row();
      }
      return $result;
    }
    //....................................................................................................................................

    public function get_banner( $category_id = 0 )
    {
      $result = array();
      if( $category_id === 0)
      {

        $query = $this->db
          ->select('
            banner.banner_id,
            banner_category.banner_category_id,
            banner_category.banner_category_name,
            banner.banner_title,
            banner.banner_url,
            banner.pic_thumb,
            banner.publish,
            banner.clicked,
            banner.visited,
            banner.displayed,
            banner.createdate,
            banner.updatedate
          ')
          ->from('banner')
          ->join('banner_category', 'banner.banner_category_id=banner_category.banner_category_id ' , 'left' )
          ->get();
        $result = $query->result();
      }
      else
      {
        $query = $this->db
          ->select('
            banner.banner_id,
            banner_category.banner_category_id,
            banner_category.banner_category_name,
            banner.banner_title,
            banner.banner_url,
            banner.pic_thumb,
            banner.publish,
            banner.clicked,
            banner.visited,
            banner.displayed,
            banner.createdate,
            banner.updatedate
          ')
          ->from('banner')
          ->join('banner_category', 'banner.banner_category_id=banner_category.banner_category_id ' , 'left' )
          ->where('banner_category.banner_category_id',$category_id)
          ->get();
        $result = $query->result();
      }
      return $result;
    }
    //....................................................................................................................................

    public function get_banner_info($category_id = 0)
    {
      $result = array();
      if( $category_id === 0)
      {
        $query = $this->db->get('banner_category');
        $result = $query->result();
      }
      else
      {
        $query =  $this->db
          ->where('banner_category_id',$category_id)
          ->get('banner_category');
        $result = $query->result();
      }
      return $result;
    }
    //....................................................................................................................................

    /*สำหรับ Select Option*/
    public function get_banner_category_option()
    {
      $query = $this->db
        ->select('banner_category_id, banner_category_name')
        ->order_by('banner_category_id', 'ASC')
        ->get('banner_category');
      return $query->result();
    }
    //....................................................................................................................................

  public function insert_banner($data)
	{
    if( $data !== '')
    {
      $banner_category_id = (!empty($data['banner_category_id']))? $data['banner_category_id'] : '0';
      $result = array();
      for ($i=0; $i < 5; $i++)
      {
        $banner_id = (!empty($data['banner_id'][$i]))? $data['banner_id'][$i] : '';
        $banner_title = (!empty($data['banner_title'][$i]))? $data['banner_title'][$i] : '';
        $banner_url = (!empty($data['banner_url'][$i]))? $data['banner_url'][$i] : '';
        // $pic_thumb = ($data['pic_thumb'][$i])? $data['pic_thumb'][$i] : '';
        $publish = (!empty($data['publish_'.$i]))? $data['publish_'.$i] : '';
        $createdate_date = (!empty($data['createdate_date'][$i]))? $data['createdate_date'][$i] : '';
        $createdate_time = (!empty($data['createdate_time'][$i]))? $data['createdate_time'][$i] : '';
        $updatedate_date = (!empty($data['updatedate_date'][$i]))? $data['updatedate_date'][$i] : '';
        $updatedate_time = (!empty($data['updatedate_time'][$i]))? $data['updatedate_time'][$i] : '';

        $createdate = $this->primaryclass->set_explode_date_time( $createdate_date, $createdate_time);
        $updatedate = $this->primaryclass->set_explode_date_time( $updatedate_date, $updatedate_time);

        $sizebanner = $this->M->get_size_banner($data['banner_category_id']);
        $sfilepath = './uploads/banner/.';
        $smaxsize = 4096000; //4MB
        $smaxwidth_pic_thumb = $sizebanner->banner_width;
        $smaxheight_pic_thumb = $sizebanner->banner_height;
        // PIC THUMB===========================================================

        if( $_FILES['pic_thumb_'.$i] )
        {
          $pic_thumb[$i] = $this->primaryclass->do_upload( 'pic_thumb_'.$i , $sfilepath, $smaxsize, $smaxwidth_pic_thumb, $smaxheight_pic_thumb );

          if( $pic_thumb[$i]['upload_data']['file_name'] == '' )
          {
            $pic_thumb_have_file = $this->input->post('old_pic_thumb');
          }
          else
          {
            $pic_thumb_have_file[$i] = $pic_thumb[$i]['upload_data']['file_name'];

          }
        }
        else
        {
          $pic_thumb_have_file = '';
        }//IF PIC THUMB=========================================================

        //---------------------- PROCESS
        if( !empty($data['banner_id'][$i]) )
        {
          // echo "UPDATE";
          // echo "\n";
          $update = $this->db
            ->set('banner_title', $banner_title)
            ->set('banner_category_id', $banner_category_id)
            ->set('banner_url', $banner_url)
            ->set('pic_thumb', $pic_thumb_have_file[$i])
            ->set('publish', $publish)
            ->set('createdate', $createdate)
            ->set('updatedate', $updatedate)
            ->where('banner_id', $banner_id)
            ->update('banner');
            // echo $this->db->last_query();
          $result = array('success'=>TRUE);
        }
        else
        {
          if( !empty($data['banner_title'][$i]) )
          {

            $insert = $this->db
              ->set('banner_title', $banner_title)
              ->set('banner_category_id', $banner_category_id)
              ->set('banner_url', $banner_url)
              ->set('pic_thumb', $pic_thumb_have_file[$i])
              ->set('publish', $publish)
              ->set('createdate', $createdate)
              ->set('updatedate', $updatedate)
              ->insert('banner');
            // echo $this->db->last_query();
            $result = array('success'=>TRUE);
          }
          else
          {
            $result = array('success'=>FALSE);
          }
        }
        //---------------------- END PROCESS
      }//END FOR
    }//END IF
    return $result;
	}
	//....................................................................................................................................

  public function update_banner($data)
	{
    if( $data !== '')
    {
      $id = $data['banner_id'];
      $update = $this->db
        ->where('banner_id', $id)
        ->update('banner',$data);
      return $update;
    }
    return;
	}
	//....................................................................................................................................

  public function delete_record($field_id, $id, $f_table)
  {
    //$query = $this->db->delete($f_table, array($field_id => $id));

    $query =  $this->db
      ->where($field_id, $id)
      ->delete($f_table);
    //echo $query = $this->db->queries[0];
    return $query;
  }
  //....................................................................................................................................

  public function get_category_banner( $id = 0)
  {
    $result = array();
    if( $id === 0 )
    {
      $query = $this->db
      ->select('
        banner_category_id,
        banner_category_name,
        banner_category_position,
      ')
      ->get('banner_category');
      $result = $query->result();
    }
    else
    {
      $query = $this->db
      ->select('
        banner_category_id,
        banner_category_name,
        banner_category_position,
      ')
      ->where('banner_category_id' , $id)
      ->get('banner_category');
      $result = $query->row();
    }

    return $result;
  }
  //....................................................................................................................................

}

/* End of file Banner.php */
