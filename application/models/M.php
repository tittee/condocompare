<?php
defined('BASEPATH') OR exit();
class M extends CI_Model {
//....................................................................................................................................

	function __construct(){
		parent::__construct();
	}
//....................................................................................................................................
//	public function avatar(){return 'img/avatar/'.rand(1,22).'.png';}
//	public function cache(){if(ENVIRONMENT==='production'){$this->output->cache(60);}}
//	public function decimal($data){return number_format($data,2,'.','');}
//	public function strip_whitespace($data){return trim(preg_replace('/\s\s+/',' ',$data));}
//	public function is_post(){$user_id=$this->User_model->id();if(ENVIRONMENT==='production' && (empty($user_id) || !$this->input->is_ajax_request() || !$this->input->post())){exit();}}
public function JSON($data=array()){

	$this->output
	->set_content_type('application/json;charset=utf-8')
	->set_header('X-Content-Type-Options: nosniff')
	->set_output(json_encode($data,JSON_UNESCAPED_UNICODE));

}

//	public function get($table=''){$this->grid(array('select'=>implode(',',$this->db->list_fields($table)),'from'=>$table));}
//	public function demo(){$this->M->grid(array('select'=>'id,demo_s,demo_n,demo_f,demo_d,demo_t,demo_b','from'=>'demo'));}
	//....................................................................................................................................

	public function alert_redirect($msg, $redirect_url)
	{
		$this->load->helper('url');
		echo "<script type='javascript/text'>";
		echo "alert('There are no fields to generate a report');";
		echo "window.location.href = '" . base_url() . "admin/ahm/panel';";
		echo "</script>";
	}
	//....................................................................................................................................

	public function add($table,$pf){
		$this->is_post();
		$data=$this->security->xss_clean($this->input->post());
		$id=$data['id'];unset($data['id']);
		$user_id=$this->User_model->id();
		if(empty($id)){
			$data[$pf.'f_user_added']=$user_id;
			$data[$pf.'f_added']=date('Y-m-d H:i:s');
			$data[$pf.'f_user_modified']=$user_id;
			$this->db->insert($table,$data);
		}else{
			$data[$pf.'f_user_modified']=$user_id;
			$this->db->update($table,$data,$pf."f_id=".$id);
		}
		return array('success'=>true);
	}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public function set($table,$id){
		$data=$this->security->xss_clean($this->input->post());
//		var_dump($data);
		reset($data);
		//echo '<br>';
		$pk = key($data);
		return $this->db->update($table, $data, $pk."=".$id);
	}
	//....................................................................................................................................

	public function edit($id=0, $data=null,$table,$pk){
//		$data=$this->security->xss_clean($this->input->post());
//		var_dump($data);
		return (empty($id)?$this->db->insert($table,$data):$this->db->update($table,$data,$pk."=".$id));
	}
	//....................................................................................................................................

	public function delete($table,$pk,$id){
		$this->db->delete($table, array( $pk=>$this->security->xss_clean($id) ));
		return array('success'=>true);
	}
	//....................................................................................................................................

	public function delete_record($field_id, $id, $f_table)
	{
		$this->db->delete($f_table, array($field_id => $this->security->xss_clean($id)));
		return true;
	}
	//....................................................................................................................................

  public function delete_multi_record($field_id, $id, $f_table)
	{
    foreach ($variable as $key => $value) {
      # code...
    }
		$this->db->delete($f_table, array($field_id => $this->security->xss_clean($id)));
		return true;
	}
	//....................................................................................................................................

	public function update_record_dalete_image($data, $field_img, $f_table)
	{
		//$data=$this->security->xss_clean($this->input->post());
		reset($data);
		$first_key = key($data); //Return KEY
		$first_value = reset($data); //Return VALUE

		$this->db->update($f_table, $data, array($first_key => $first_value));
//		echo $this->db->last_query();
		if( $this->db->affected_rows() )
		{
			unlink(FCPATH . $field_img);
		}
		return true;
	}
	//....................................................................................................................................

	public function update_record($data, $f_table)
	{
		reset($data);
		$first_key = key($data); //Return KEY
		$first_value = reset($data); //Return VALUE

		$this->db->update($f_table, $data, array($first_key => $first_value));
		return true;
	}
	//....................................................................................................................................

	public function get_name($field_sql, $field_id, $field_name, $id, $f_table)
	{
		$query = $this->db
			->select($field_sql)
			->where($field_id, $id )
			->order_by($field_id, 'ASC')
			->get($f_table);
		return $query->result();

	}
	//....................................................................................................................................

	public function get_name_row($field_sql, $field_id, $id, $f_table)
	{
		$query = $this->db
			->select($field_sql)
			->where($field_id, $id )
			->get($f_table);
      // echo $this->db->last_query();
		$row = $query->row();
		return ($row->$field_sql)? $row->$field_sql : '';
	}
	//....................................................................................................................................


	 public function get_id_by_title($field_id, $field_title, $title, $f_table)
	{
		$query = $this->db
			->select($field_id)
			->where($field_title, $title )
			->order_by($field_title, 'ASC')
			->get($f_table);
      // echo $this->db->last_query();
		return $query->row();

	}
	//....................................................................................................................................

	public function get_name_by_id($field_id, $field_name, $id, $f_table)
	{
		$query = $this->db
			->select($field_name)
			->where($field_id, $id )
			->order_by($field_id, 'ASC')
			->get($f_table);
		return $query->result_array();

	}
	//....................................................................................................................................

	public function get_data_by_id($field_sql, $field_id, $id = 0, $f_table)
	{

			$query = $this->db
				->select($field_sql)
				->where($field_id, $id )
				->get($f_table)
				->result_array(0);
			return $query[0];


	}
	//....................................................................................................................................

	public function get_email_member($field_sql, $field_id, $id, $f_table)
	{
		$row = array();
		$query = $this->db
			->select($field_sql)
			->where($field_id, $id )
			->order_by($field_id, 'ASC')
			->get($f_table);
		if($query->num_rows() > 0)
		{
			return $row = $query->row();
		}
		else
		{
			return $row;
		}
	}
	//....................................................................................................................................

	public function get_staff($field_sql, $field_id, $id, $f_table)
	{
		$query = $this->db
			->select($field_sql)
			->where($field_id, $id )
			->order_by($field_id, 'ASC')
			->get($f_table);
		return $query->row();
	}
	//....................................................................................................................................

	public function get_provinces()
	{
		$query = $this->db
			->select('provinces_id, provinces_name')
			->order_by('CONVERT (provinces_name USING tis620)', 'ASC')
			->get('provinces');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_districts($provinces_id)
	{
		$query = $this->db
			->select('districts_id, districts_name')
			->where('provinces_id', $provinces_id)
			->order_by('CONVERT (districts_id USING tis620)', 'ASC')
			->get('districts');
		return $query->result();
	}
	//....................................................................................................................................

	public function get_subdistricts($districts_id)
	{
		$query = $this->db
			->select('sub_districts_id, sub_districts_name')
			->where('districts_id', $districts_id)
			->order_by('CONVERT (sub_districts_id USING tis620)', 'ASC')
			->get('sub_districts');
		return $query->result();
	}
	//....................................................................................................................................

	public function update_record_image($field_id, $field_name, $id, $f_table)
	{
		$this->db
			->set($field_name, '')
			->where($field_id, $id)
			->update($f_table);
		return true;
	}
	//....................................................................................................................................

	public function update_visited( $field_id, $id, $f_table)
	{
		$update = $this->db
			->set('visited', 'visited+1', FALSE)
			->where($field_id, $id)
			->update($f_table);
		return true;
	}
	//....................................................................................................................................

	public function update_clicked( $field_id, $field_update, $id, $f_table)
	{
		$field_update = $field_update + 1;
		$update = $this->db
			->set('clicked', $field_update)
			->where($field_id, $id)
			->update($f_table);
		return true;
	}
	//....................................................................................................................................

	public function update_displayed( $field_id, $field_update, $id, $f_table)
	{
		$field_update = $field_update + 1;
		$update = $this->db
			->set('displayed', $field_update)
			->where($field_id, $id)
			->update($f_table);
		// echo $this->db->last_query();
		return true;
	}
	//....................................................................................................................................

	function get_main_setting($id = 0)
	{
		if ($id === 0)
		{
			$query = $this->db
				->select('*')
				->where('site_option_id', $id)
				->get('site_option');
			return $query->result_array();
		}

		$query = $this->db->get_where('site_option', array('site_option_id' => $id));
		return $query->row_array();
	}
	//....................................................................................................................................

	function get_video_setting($id = 0)
	{
		if ($id === 0)
		{
			$query = $this->db
				->select('site_video_youtube,
				site_video_text,
				site_video_url')
				->where('site_option_id', $id)
				->get('site_option');
			return $query->result_array();
		}

		$query = $this->db->get_where('site_option', array('site_option_id' => $id));
		return $query->row_array();
	}
	//....................................................................................................................................

  function get_loan_setting($id = 0)
	{
		if ($id === 0)
		{
			$query = $this->db
				->select('site_mrl,
				site_mor,
				site_mrr,
        site_cpr')
				->where('site_option_id', $id)
				->get('site_option');
			return $query->result_array();
		}

		$query = $this->db->get_where('site_option', array('site_option_id' => $id));
		return $query->row_array();
	}
	//....................................................................................................................................

	public function get_size_banner($banner_category_id)
	{
		$result = array();
		if ($banner_category_id !== 0)
		{
			$query = $this->db
				->select('banner_width, banner_height')
				->where('banner_category_id', $banner_category_id)
				->order_by('banner_category_id', 'ASC')
				->get('banner_category');
			// echo $this->db->last_query();
			$result = $query->row();
		}
		return  $result;
	}
	//....................................................................................................................................

  public function set_order( $table,$id,$field_id,$field_order,$news_order ){ //

		// echo $table .' '. $id .' '. $field_order .' '. $news_order;

    $this->db->set($field_order, $news_order);
    $this->db->where($field_id, $id);
    echo $this->db->last_query();
    return $this->db->update($table);

    // return $this->db->update($table, $data, $pk."=".$id);
  }
  //....................................................................................................................................



}
