<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Primaryclass {
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	protected $CI;

		// We'll use a constructor, as you can't directly call a function
		// from a property definition.
		public function __construct()
		{
			// Assign the CodeIgniter super-object
			$this->CI =& get_instance();
			$this->CI->load->helper(array('form', 'url'));
		}
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		public function pre_var_dump($data)
		{
			echo '<pre>';
			echo var_dump($data);
			echo '</pre>';
		}
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public function set_fields($field, $field_empty){
			$results = '';
			$results = (is_string($field) === TRUE)? $field : $field_empty;
			return $results;
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


		public function get_publish($publish)
		{
			switch ($publish)
			{
				case 0:
					$data = "ไม่กำหนด";
					break;
				case 1:
					$data = "ปิดการเผยแพร่";
					break;
				case 2:
					$data = "เผยแพร่";
					break;
				default:
					$data = "ไม่พบข้อมูล";
					break;
			}
			return $data;
		}
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		public function get_approved($approved)
		{
			switch ($approved)
			{
				case 0:
					$data = "ยังไม่ระบุค่า";
					break;
				case 1:
					$data = "รอการอนุมัติ";
					break;
				case 2:
					$data = "อนุมัติเรียบร้อย";
					break;
				default:
					$data = "ยังไม่ระบุค่า";
					break;
			}
			return $data;
		}
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		public function get_member_type($member_type_id)
		{
			switch ($member_type_id)
			{
				case 0:
					$data = "ยังไม่ระบุค่า";
					break;
				case 1:
					$data = "Condo Compare | แชร์ไม่ได้";
					break;
				case 2:
					$data = "HOB | แชร์ได้";
					break;
				case 3:
					$data = "Facebook";
					break;
				default:
					$data = "ยังไม่ระบุค่า";
					break;
			}
			return $data;
		}
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		public function get_condo_share_form_status($condo_share_form_status)
		{
			switch ($condo_share_form_status)
			{
				case 0:
					$data = "ยังไม่ติดต่อกลับ";
					break;
				case 1:
					$data = "ดำเนินการติดต่อแล้ว";
					break;
				default:
					$data = "ยังไม่ระบุค่า";
					break;
			}
			return $data;
		}
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		public function set_explode_date_time( $create_date = null, $create_time = null)
		{
			$createdate_date = ( !empty( $create_date ) )? $create_date : date("Y-m-d");
			$createdate_time = ( !empty( $create_time ) )? $create_time : date("H:i:s");
			return $createdate = $createdate_date . ' ' .$createdate_time;
		}
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		public function do_upload($sfilename, $sfilepath, $smaxsize, $smaxwidth, $smaxheight , $allowed_types = "jpeg|jpg|png|gif", $remove_space = 0)
		{
			//$this->CI->load->library('upload');
			$config = array();
			$config['upload_path']          = $sfilepath;
			//$config['upload_url']          = base_url().$sfilepath;
			$config['allowed_types']        = $allowed_types;
			$config['encrypt_name'] 				= TRUE;
			$config['overwrite'] 						= TRUE;
			$config['remove_spaces'] 				= ($remove_space === 0)? FALSE : TRUE;
			$config['max_size']             = (!empty($smaxsize))? $smaxsize : 4096000; //4MB , 2MB = 2048000
			$config['max_width']            = (!empty($smaxwidth))? $smaxwidth : 1000;
			$config['max_height']           = (!empty($smaxheight))? $smaxheight : 1000;
			//$config['file_name'] 						= (!empty($snewfilename))? $snewfilename : $sfilename;



			$this->CI->load->library('upload', $config);

			$this->CI->upload->initialize($config);

			echo $this->CI->upload->display_errors('<p>', '</p>');

			if( $this->CI->upload->do_upload( $sfilename ) )
			{
				return $data = array('upload_data' => $this->CI->upload->data());
			}
		}
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    public function encode_htmlspecialchars($html_tag)
    {
      $new_html_tag = htmlspecialchars($html_tag, ENT_NOQUOTES);
      return $new_html_tag; // &lt;a href=&#039;test&#039;&gt;Test&lt;/a&gt;
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public function decode_htmlspecialchars($html_tag)
    {
      $new_html_tag = htmlspecialchars_decode($html_tag);
      return $new_html_tag;
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public function fn_addslashes($html_tag)
    {
      $new_html_tag = addslashes($html_tag);
      return $new_html_tag;
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public function fn_stripslashes($html_tag)
    {
      $new_html_tag = stripslashes($html_tag);
      return $new_html_tag;
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		public function condo_road_soi( $condo_road, $condo_alley )
		{
			$condo_road = (!empty($condo_road))? 'ถนน '.$condo_road : '';
			$condo_alley = (!empty($condo_alley))? 'ซอย '.$condo_alley : '';

			$msg_text = $condo_road. ' '. $condo_alley;

			return $msg_text;
		}
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public function land_road_provinces( $condo_road, $provinces_name )
		{
			$condo_road = (!empty($condo_road))? 'ถนน '.$condo_road : '';
			$provinces_name = (!empty($provinces_name))? 'จังหวัด '.$provinces_name : '';

			$msg_text = $condo_road. ' '. $provinces_name;

			return $msg_text;
		}
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		public function get_square($square)
		{
			switch ($square)
			{
				case "SQUARE WAH":
					$msg_text = "ตารางวา";
					break;
				case "ACRE":
					$msg_text = "ไร่";
					break;
				default:
					$msg_text = "ตารางวา";
					break;
			}
			return $msg_text;
		}
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		public function get_size_square( $land_size, $land_size_square_rai, $land_size_square_ngan, $land_size_square_yard, $land_size_unit )
		{
			$land_size_unit = (!empty($land_size_unit))? $land_size_unit : '';
			$msg_text = '';
			// $msg_text = (!empty($land_size))? 'ขนาด '. number_format($land_size, 0, '.', ',') .' '. $land_size_unit : '';
			$msg_text .= (!empty($land_size_square_rai))? ' '. number_format($land_size_square_rai, 0, '.', ',') .' '. 'ไร่' : '';
			$msg_text .= (!empty($land_size_square_ngan))? ' '. number_format($land_size_square_ngan, 0, '.', ',') .' '. 'งาน' : '';
			$msg_text .= (!empty($land_size_square_yard))? ' '. number_format($land_size_square_yard, 0, '.', ',') .' '. 'ตารางวา' : '';

			// if( $land_size !== '' )
			// {
			// 	$msg_text = 'ขนาด '. number_format($land_size, 0, '.', ',') .' '. $land_size_unit;
			// }
			// elseif( $land_size_square_rai !== '' )
			// {
			// 	$msg_text .= ' '. number_format($land_size_square_rai, 0, '.', ',') .' '. 'ไร่';
			// }
			// elseif( $land_size_square_ngan !== '' )
			// {
			// 	$msg_text .= ' '. number_format($land_size_square_ngan, 0, '.', ',') .' '. 'งาน';
			// }
			// elseif( $land_size_square_yard !== '' )
			// {
			// 	$msg_text .= ' '. number_format($land_size_square_yard, 0, '.', ',') .' '. 'วา';
			// }
			// else
			// {
			// 	$msg_text = 'ไม่ระบุ';
			// }
			return $msg_text;
		}
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		public function get_price_per_square( $display_rakha = 1, $condo_total_price, $condo_area_apartment, $condo_price_per_square_meter )
		{
			$display_rakha = ($display_rakha === 1)? 'ราคา ' : '';

      $condo_total_price = ( $condo_total_price !== "0" || $condo_total_price !== "0.00" )? $condo_total_price : '0.00';
      $condo_area_apartment = ( $condo_area_apartment !== "0" )? $condo_area_apartment : '0.00';
      $condo_price_per_square_meter = ( $condo_price_per_square_meter !== "0" || $condo_price_per_square_meter !== "0.00" )? $condo_price_per_square_meter : '0.00';

			if( $condo_total_price !== '0.00' && $condo_area_apartment !== '0.00' )
			{
				$condo_total_price_meter =  $condo_total_price / $condo_area_apartment;
				$msg_text = $display_rakha. number_format($condo_total_price_meter, 0, '.', ',') .' บาท / ตร.ม.';
			}
			elseif( $condo_price_per_square_meter !== '0' )
			{
				$msg_text = $display_rakha. number_format($condo_price_per_square_meter, 0, '.', ',') .' บาท / ตร.ม.';
			}
			else
			{
				$msg_text = 'ไม่ระบุ';
			}
			return $msg_text;
		}
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		public function get_land_price_per_square( $display_rakha = 1, $land_total_price, $land_size, $land_size_unit , $land_price_per_square_meter )
		{
			$display_rakha = ($display_rakha === 1)? 'ราคา ' : '';
			if( $land_total_price !== '0.00' && $land_size !== '0.00' )
			{
				$land_total_price_meter =  $land_total_price / $land_size;
				$msg_text = $display_rakha. number_format($land_total_price_meter, 0, '.', ',') .' บาท / '. $land_size_unit;
			}
			elseif( $land_price_per_square_meter !== '0.00' )
			{
				$msg_text = $display_rakha. number_format($land_price_per_square_meter, 0, '.', ',') .' บาท /'. $land_size_unit;
			}
			else
			{
				$msg_text = 'ไม่ระบุ';
			}
			return $msg_text;
		}
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public function breadcrumbs_page( $page_url = array() )
		{
		  $this->CI->load->library('breadcrumbs');
		   	// add breadcrumbs

			foreach ($page_url as $key => $value) {
				# code...
				// print_r($value['title']);
				$this->CI->breadcrumbs->add($value['title'], $value['href'], $key);
			}
		   	// $this->breadcrumbs->add('second crumb', 'the_test', 0);
		   	// $this->breadcrumbs->add('test','http://google.com');
		   	// output
				return $this->CI->breadcrumbs->output();
		}
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public function set_pagination($url_page, $all_record, $per_page)
    {
			$this->CI->load->library('pagination');
        $config = array();
        $config['base_url'] = $url_page;
        // if (count($_GET) > 0) $config['suffix'] = '&' . http_build_query($_GET, '', "&");
        // if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET);
        $config['use_global_url_suffix'] = false;
        $config['display_pages'] = true;
        $config['total_rows'] = $all_record;
        $config['per_page'] = $per_page;
        $config['uri_segment'] = 3;
        $config['num_links'] = 4;
        $config['enable_query_strings'] = true; //TRUE //FALSE
        $config['page_query_string'] = true; //TRUE //FALSE
        $config['reuse_query_string'] = true;

        $config['full_tag_open'] = '<ul role="navigation" aria-label="Pagination" class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="first">';
        $config['first_tag_close'] = '</li>';
				//
				// // $config['first_url'] = '?'.http_build_query($_GET);
        // $config['first_url'] = $url_page;


        $config['cur_tag_open'] = '<li class="current"><span class="show-for-sr">You are on page</span>';
        $config['cur_tag_close'] = '</li>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="last">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="pagination-next">';
        $config['next_tag_close'] = '<span class="show-for-sr">page</span></a></li>';

        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="pagination-previous">';
        $config['prev_tag_close'] = '<span class="show-for-sr">page</span></li>';

        $this->CI->pagination->initialize($config);

        //echo $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $page = ($this->CI->input->get('per_page')) ? $this->CI->input->get('per_page') : 0;

        return $page = array(
            'per_page' => $config['per_page'],
            'page' => $page,

        );
    }
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    function raw_encode($str=''){
			$this->CI->load->library('encrypt');

      $key = $this->CI->config->item('encryption_key');
			$this->CI->encrypt->set_cipher(MCRYPT_BLOWFISH);

			$encoded_url_safe_string = urlencode($this->CI->encrypt->encode($str, $key, true));

      return $encoded_url_safe_string;


		}
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    function raw_decode($str=''){
			$this->CI->load->library('encrypt');

      $key = $this->CI->config->item('encryption_key');
			$this->CI->encrypt->set_cipher(MCRYPT_BLOWFISH);

			// echo $encoded_url_safe_string = urlencode($this->encrypt->encode($this->session->mID, $key, true));
      // echo $this->encrypt->encode($this->session->mID, $key, true);
      // echo "<br />";

      return urldecode($this->CI->encrypt->decode(rawurldecode($str), $key));

    }
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}
