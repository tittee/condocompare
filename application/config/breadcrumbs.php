<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| BREADCRUMB CONFIG
| -------------------------------------------------------------------
| This file will contain some breadcrumbs' settings.
|
| $config['crumb_divider']		The string used to divide the crumbs
| $config['tag_open'] 			The opening tag for breadcrumb's holder.
| $config['tag_close'] 			The closing tag for breadcrumb's holder.
| $config['crumb_open'] 		The opening tag for breadcrumb's holder.
| $config['crumb_close'] 		The closing tag for breadcrumb's holder.
|
| Defaults provided for twitter bootstrap 2.0
*/



// $config['include_home'] will tell the library if the first element should be the homepage. You only put the title of the first crumb. If you leave it blank it will not put homepage as first crumb
$config['include_home'] = 'หน้าแรก';
// $config['divider'] is the divider you want between the crumbs. Leave blank if you don't want a divider;
$config['divider'] = '';
// $config['container_open'] is the opening tag of the breadcrumb container
$config['container_open'] = '<ul class="breadcrumbs">';
// $config['container_close'] is the closing tag of the breadcrumb container
$config['container_close'] = '</ul>';
// $config['crumb_open'] is the opening tag of the crumb container
$config['crumb_open'] = '<li>';
// $config['crumb_close'] is the closing tag of the crumb container
$config['crumb_close'] = '</li>';

/* End of file breadcrumbs.php */
/* Location: ./application/config/breadcrumbs.php */
