<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'Land_Controller';
//$route['land/add'] = "land_Controller/add";
//$route['land/edit/(:num)'] = "land_Controller/edit/$1";
//$route['land/saveland'] = "land_Controller/saveland";
//$route['land/getdistricts/(:num)/(:num)'] ="land_Controller/getdistricts/$1/$2";
//$route['land/getamphures/(:num)'] ="land_Controller/getamphures/$1";

//$route['frontend/(:any)'] = 'condo/view/$1';
//$route['condo'] = 'condo';
//$route['(:any)'] = 'frontend/$1';

$route['default_controller'] = 'page';
$route['404_override'] = 'pagenotfound'; //my404 is class name.
$route['translate_uri_dashes'] = false;
/* */
$route['home'] = 'page/index';

/* MEMBER */
$route['member-login'] = 'page/m_login';
$route['member-login-hob'] = 'page/m_login_hob';
$route['member-register'] = 'page/m_register';
$route['member-register-hob'] = 'page/m_register_hob';
$route['member-condo-approved'] = 'page/m_condo_approved';
$route['member-condo-unapproved'] = 'page/m_condo_unapproved';
$route['member-create-post-condo'] = 'page/m_add_condo';
$route['member-share-condo'] = 'page/m_share_all_condo';
$route['member-contact-share-condo'] = 'page/m_contact_share_condo';
$route['member-info-share-condo'] = 'page/m_contact_list_share_condo';
$route['wishlist-condo'] = 'page/m_wishlist_condo';

$route['member-house-and-land'] = 'page/m_all_land';
$route['member-share-house-and-land'] = 'page/m_share_all_land';
$route['member-contact-share-house-and-land'] = 'page/m_share_all_land';

$route['change-password'] = 'page/m_change_password';
$route['forgot-password'] = 'page/m_forgotpassword';
$route['form-forgot-password/(:any)'] = 'page/m_form_forgotpassword/$1';
$route['member-profile'] = 'page/m_profile';
$route['condo-share-contact-list/(:any)/(:any)'] = 'page/m_contact_list_share_condo/$1/$2';

/* CONDOMINIUM */
$route['condominium'] = 'page/condo_list';
$route['condominium-details/(:any)'] = 'page/condoinside/$1';
$route['condominium-share/(:any)/(:any)'] = 'page/condoinside_share/$1/$2';

$route['condocompare'] = 'page/list_compare_condo';

/* LAND */
$route['house-and-land'] = 'page/land_list';
$route['land-details/(:any)'] = 'page/landinside/$1';
$route['land-share/(:any)/(:any)'] = 'page/landinside_share/$1/$2';
$route['land-share-contact-list/(:any)/(:any)'] = 'page/m_contact_list_share_land/$1/$2';

/* NEWS */
$route['news'] = 'news';
$route['news-category/(:any)'] = 'news/index/$1';
$route['news-details/(:any)'] = 'news/newsinside/$1';

/* REVIEWS */
$route['reviews'] = 'reviews';
$route['reviews-category/(:any)'] = 'reviews/index/$1';
$route['reviews-details/(:any)'] = 'reviews/reviewsinside/$1';

/* ACTIVITIES */
$route['activities'] = 'activities/home';
$route['activities-category/(:any)'] = 'activities/index/$1';
$route['activities-details/(:any)'] = 'activities/activitiesinside/$1';

$route['santorini-hua-hin'] = 'page/santorini';
$route['contact-us'] = 'page/contactus';

$route['consignment-agents'] = 'page/consignment_agents';

//BACKOFFICE
$route['backoffice/banner-category'] = 'banner/banner_category';
$route['backoffice/banner-category/(:num)'] = 'banner/banner_category/$1';
$route['backoffice/banner'] = 'banner/index';
$route['backoffice/banner/(:num)'] = 'banner/index/$1';
$route['backoffice/banner/add/(:num)'] = 'banner/add/$1'; //1 = category_id | 2 = banner_id
$route['backoffice/banner/edit/(:num)/(:num)'] = 'banner/edit/$1/$2'; //1 = category_id | 2 = banner_id
$route['backoffice/banner-setting'] = 'banner/banner_setting';
