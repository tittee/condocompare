<?php
    $site_id = 1;
    $site_setting = $this->M->get_main_setting($site_id);
    $logo = (!empty($site_setting['site_logo'])) ? 'uploads/site_setting/'.$site_setting['site_logo'] : 'assets/images/logo.jpg';
    $site_footer = (!empty($site_setting['site_footer'])) ? $site_setting['site_footer'] : '';
    $site_copyright = (!empty($site_setting['site_copyright'])) ? $site_setting['site_copyright'] : '';
    $site_facebook = (!empty($site_setting['site_facebook'])) ? $site_setting['site_facebook'] : '#';
    $site_twitter = (!empty($site_setting['site_twitter'])) ? $site_setting['site_twitter'] : '#';
    $site_youtube = (!empty($site_setting['site_youtube'])) ? $site_setting['site_youtube'] : '#';
    $site_banner_autoplay = (!empty($site_setting['site_banner_autoplay'])) ? $site_setting['site_banner_autoplay'] : '4000';
?>
<div class="container bg-cs-blue">
	<div id="footer" class="row">
		<div class="footer-menu" style="width: 100%; padding-bottom: 1rem;">
			<div class="small-12 medium-12 large-2 columns">
				<div class="logo"><a href="<?php echo base_url('home')?>" class="logo-small.show-for-large"><img src="<?php echo base_url().$logo?>"></a></div>
			</div>
			<div class="small-12 medium-12 large-8 columns">
				<ul class="vertical footer medium-horizontal menu">
					<li><a href="<?php echo base_url('condominium')?>" title="คอนโดมิเนียม">คอนโดมิเนียม</a>
					</li>
					<li><a href="<?php echo base_url('house-and-land')?>" title="บ้าน/ที่ดิน">บ้าน/ที่ดิน</a>
					</li>
					<li><a href="<?php echo base_url('news')?>" title="ข่าวสารอัพเดท">ข่าวสารอัพเดท</a>
					</li>
					<li><a href="<?php echo base_url('reviews')?>" title="รีวิวอัพเดท">รีวิวอัพเดท</a>
					</li>
					<li><a href="<?php echo base_url('contact-us')?>" title="ติตต่อเรา">ติตต่อเรา</a>
					</li>
          <li><a href="<?php echo base_url('consignment-agents')?>" title="ฝากขายทรัพย์">ฝากขายทรัพย์</a>
          </li>
					<!-- <li><a href="<?php echo base_url('santorini-hua-hin')?>" title="Santorini Hua Hin">Santorini Hua Hin</a>
					</li> -->
					<!-- <li><a href="#">Santorini Hua Hin</a>
					</li> -->
				</ul>
			</div>
			<div class="small-12 medium-12 large-2 columns">
				<ul class="horizontal menu socials">
					<li><a href="<?php echo $site_facebook; ?>" target="_blank"><i class="fa fa-round fa-facebook"></i></a></li>
					<li><a href="<?php echo $site_twitter; ?>" target="_blank"><i class="fa fa-round fa-twitter"></i></a></li>
					<li><a href="<?php echo $site_youtube; ?>" target="_blank"><i class="fa fa-round fa-youtube"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="hide-for-small-only">
			<?php echo $site_footer; ?>
		</div>
	</div>
	<div class=" text-center color-white" style="padding: 1rem 0;"><?php echo $site_copyright; ?></div>
</div>



<input type="hidden" class="pathname" value="<?php echo site_url('page/banner_clicked'); ?>" />
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.min.js?v<?php echo VERSION; ?>"></script>
<script src="<?php echo base_url()?>assets/js/foundation.min.js?v<?php echo VERSION; ?>"></script>
<!-- <script src="<?php echo base_url()?>assets/js/plugins/foundation.core.js?v<?php echo VERSION; ?>"></script> -->
<script src="<?php echo base_url()?>assets/js/plugins/foundation.tooltip.js?v<?php echo VERSION; ?>"></script>
<script src="<?php echo base_url()?>assets/js/plugins/foundation.util.box.js?v<?php echo VERSION; ?>"></script>
<script src="<?php echo base_url()?>assets/js/plugins/foundation.responsiveMenu.js?v<?php echo VERSION; ?>"></script>
<script src="<?php echo base_url()?>assets/js/plugins/foundation.util.triggers.js?v<?php echo VERSION; ?>"></script>
<script src="<?php echo base_url()?>assets/js/plugins/foundation.util.mediaQuery.js?v<?php echo VERSION; ?>"></script>
<script src="<?php echo base_url()?>assets/js/plugins/foundation.offcanvas.js?v<?php echo VERSION; ?>"></script>
<script src="<?php echo base_url()?>assets/js/plugins/foundation.util.motion.js?v<?php echo VERSION; ?>"></script>
<script src="<?php echo base_url()?>assets/plugins/motion-ui/motion-ui.min.js?v<?php echo VERSION; ?>"></script>
<script src="<?php echo base_url()?>assets/js/Chart.js?v<?php echo VERSION; ?>"></script>
<script src="<?php echo base_url()?>assets/js/owl.carousel.js?v<?php echo VERSION; ?>"></script>
<script src="<?php echo base_url()?>assets/js/jquery.mousewheel.min.js?v<?php echo VERSION; ?>"></script>
<script src="<?php echo base_url()?>assets/js/banner.js?v<?php echo VERSION; ?>"></script>
<script src="<?php echo base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js?v<?php echo VERSION; ?>"></script>
<script src="<?php echo base_url()?>assets/pages/jquery.sweet-alert.init.js?v<?php echo VERSION; ?>"></script>

<!-- Modal-Effect -->

<!-- script(src="js/plugins/foundation.util.mediaQuery.js?v<?php echo VERSION; ?>")-->
<script>
	$(document).foundation();
</script>
<?php if (isset($isslider)) {  //OWL CAROUSEL ?>
<script type="text/javascript">

// CREDIT : https://codepen.io/washaweb/pen/KVRxRW
	$(document).ready(function () {

		var sync1 = $("#sync1");
		  var sync2 = $("#sync2");
		  var slidesPerPage = 4; //globaly define number of elements per page
		  var syncedSecondary = true;

		  sync1.owlCarousel({
		    items : 1,
		    slideSpeed : 2000,
		    nav: true,
		    autoplay: true,
		    dots: false,
		    loop: true,
		    responsiveRefreshRate : 200,
		  }).on('changed.owl.carousel', syncPosition);

		  sync2
		    .on('initialized.owl.carousel', function () {
		      sync2.find(".owl-item").eq(0).addClass("current");
		    })
		    .owlCarousel({
		    items : slidesPerPage,
		    dots: false,
		    nav: false,
		    smartSpeed: 200,
		    slideSpeed : 500,
		    slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
		    responsiveRefreshRate : 100
		  }).on('changed.owl.carousel', syncPosition2);

		  function syncPosition(el) {
		    //if you set loop to false, you have to restore this next line
		    //var current = el.item.index;

		    //if you disable loop you have to comment this block
		    var count = el.item.count-1;
		    var current = Math.round(el.item.index - (el.item.count/2) - .5);

		    if(current < 0) {
		      current = count;
		    }
		    if(current > count) {
		      current = 0;
		    }

		    //end block

		    sync2
		      .find(".owl-item")
		      .removeClass("current")
		      .eq(current)
		      .addClass("current");
		    var onscreen = sync2.find('.owl-item.active').length - 1;
		    var start = sync2.find('.owl-item.active').first().index();
		    var end = sync2.find('.owl-item.active').last().index();

		    if (current > end) {
		      sync2.data('owl.carousel').to(current, 100, true);
		    }
		    if (current < start) {
		      sync2.data('owl.carousel').to(current - onscreen, 100, true);
		    }
		  }

		  function syncPosition2(el) {
		    if(syncedSecondary) {
		      var number = el.item.index;
		      sync1.data('owl.carousel').to(number, 100, true);
		    }
		  }

		  sync2.on("click", ".owl-item", function(e){
		    e.preventDefault();
		    var number = $(this).index();
		    sync1.data('owl.carousel').to(number, 300, true);
		  });

	});
</script>
<?php

}//END IF OWL CAROUSEL ?>
<?php
    //FILE JS
    if (isset($js)) {
        foreach ($js as $datajs) {
            $filejs = base_url().$datajs.'?'.VERSION;
            echo "<script src=\"$filejs\"></script>\n";
        }
    }
    ?>
<script type="text/javascript">
$(document).ready(function () {
	$(".banner-carousel").owlCarousel({
		items						: 1,
		loop						: true,
		responsiveClass	: true,
		center					: true,
		autoWidth				: false,

		autoplay				: true,
		autoplayTimeout	: <?php echo (!empty($site_banner_autoplay)) ? $site_banner_autoplay : '4000'; ?>,
		autoplayHoverPause	: false,

		nav							: false,
		dots						: false,
	});
});
</script>




	</body>

	</html>
