<li class="accordion-item" data-accordion-item>
  <a href="#tab-2m" class="accordion-title"><i class="fa fa-bus color-red"></i> ค้นหาจากระบบขนส่งมวลชน</a>
  <div id="tab-2m" class="accordion-content" data-tab-content>
    <form class="container" id="form_1" method="get" action="<?php echo site_url('condominium'); ?>">
      <div class="row">
        <div class="small-3 columns">
          <label for="mass_transportation_category_id" class="text-right">ระบบขนส่งมวลชน</label>
        </div>
        <div class="small-9 columns">
          <select name="mass_transportation_category_id" id="mass_transportation_category_id">
            <option value="">----เลือก----</option>
            <option value="bts">BTS Sky Train</option>
            <option value="mrt">MRT Metro system</option>
            <option value="airportlink">Airport Rail link</option>
          </select>
        </div>
      </div>
      <div class="clearfix"></div>
      <!-- END -->

      <div class="row" id="bts_mobile">
        <div class="small-3 columns">
          <label for="bts" class="text-right">สถานีรถไฟฟ้า บีทีเอส</label>
        </div>
        <div class="small-9 columns">
          <select name="bts" class="bts_mobile">
            <option value="">----เลือก----</option>
            <optgroup label="สายสุขุมวิท">
              <option value="หมอชิต">สถานี หมอชิต</option>
              <option value="สะพานควาย">สถานี สะพานควาย</option>
              <option value="อารีย์">สถานี อารีย์</option>
              <option value="สนามเป้า">สถานี สนามเป้า</option>
              <option value="อนุเสาวรีย">สถานี อนุเสาวรีย์</option>
              <option value="พญาไท">สถานี พญาไท</option>
              <option value="ราชเทวี">สถานี ราชเทวี</option>
              <option value="สยาม">สถานี สยาม</option>
              <option value="ชิดลม">สถานี ชิดลม</option>
              <option value="เพลินจิต">สถานี เพลินจิต</option>
              <option value="นานา">สถานี นานา</option>
              <option value="อโศก">สถานี อโศก</option>
              <option value="พร้อมพงษ์">สถานี พร้อมพงษ์</option>
              <option value="ทองหล่อ">สถานี ทองหล่อ</option>
              <option value="เอกมัย">สถานี เอกมัย</option>
              <option value="พระโขนง">สถานี พระโขนง</option>
              <option value="อ่อนนุช">สถานี อ่อนนุช</option>
              <option value="บางจาก">สถานี บางจาก</option>
              <option value="ปุณณวิถี">สถานี ปุณณวิถี</option>
              <option value="อุดมสุข">สถานี อุดมสุข</option>
              <option value="บางนา">สถานี บางนา</option>
              <option value="แบริ่ง">สถานี แบริ่ง</option>
            </optgroup>
            <optgroup label="สายสีลม">
              <option value="สนามกีฬาแห่งชาติ">สถานี สนามกีฬาแห่งชาติ</option>
              <option value="สยาม">สถานี สยาม</option>
              <option value="ราชดำริ">สถานี ราชดำริ</option>
              <option value="ศาลาแดง">สถานี ศาลาแดง</option>
              <option value="ช่องนนทรี">สถานี ช่องนนทรี</option>
              <option value="สุรศักดิ์">สถานี สุรศักดิ์</option>
              <option value="สะพานตากสิน">สถานี สะพานตากสิน</option>
              <option value="กรุงธนบุรี">สถานี กรุงธนบุรี</option>
              <option value="วงเวียนใหญ่">สถานี วงเวียนใหญ่</option>
              <option value="โพธินิมิตร">สถานี โพธินิมิตร</option>
              <option value="ตลาดพูล">สถานี ตลาดพูล</option>
              <option value="วุฒากาศ">สถานี วุฒากาศ</option>
              <option value="บางหว้า">สถานี บางหว้า</option>
            </optgroup>
          </select>
        </div>
      </div>
      <div class="clearfix"></div>
      <!-- END -->

      <div class="row" style="display: none;" id="mrt_mobile">
        <div class="small-3 columns">
          <label for="mrt" class="text-right">สถานีรถไฟฟ้า เอ็มอาร์ที</label>
        </div>
        <div class="small-9 columns">
          <select name="mrt" class="mrt">
            <option value="">----เลือก----</option>
            <optgroup label="สายสุขุมวิท">
              <option value="1">สถานี บางซื่อ</option>
              <option value="2">สถานี กำแพงเพชร</option>
              <option value="3">สถานี สวนจตุจักร</option>
              <option value="4">สถานี พหลโยธิน</option>
              <option value="5">สถานี ลาดพร้าว</option>
              <option value="6">สถานี รัชดาภิเษก</option>
              <option value="7">สถานี สุทธิสาร</option>
              <option value="8">สถานี ห้วยขวาง</option>
              <option value="9">สถานี ศูย์วัฒนธรรมฯ</option>
              <option value="10">สถานี พระราม 9</option>
              <option value="11">สถานี เพชรบุรี</option>
              <option value="12">สถานี สุขุมวิท</option>
              <option value="13">สถานี ศูนย์การประชุมแห่งชาติฯ</option>
              <option value="14">สถานี คลองเตย</option>
              <option value="15">สถานี ลุมพินี</option>
              <option value="16">สถานี สีลม</option>
              <option value="17">สถานี สามย่าน</option>
              <option value="18">สถานี หัวลำโพง</option>
            </optgroup>

          </select>
        </div>
      </div>
      <div class="clearfix"></div>
      <!-- END -->

      <div class="row" style="display: none;" id="airportlink_mobile">
        <div class="small-3 columns">
          <label for="mrt" class="text-right">สถานีรถไฟฟ้า แอร์พอร์ต เรล ลิงค์</label>
        </div>
        <div class="small-9 columns">
          <select name="airportlink" class="airportlink">
            <option value="">----เลือก----</option>
            <optgroup label="สายสุขุมวิท">
              <option value="1">สถานี พญาไท</option>
              <option value="2">สถานี ราชปรารภ</option>
              <option value="3">สถานี มักกะสัน</option>
              <option value="4">สถานี รามคำแหง</option>
              <option value="5">สถานี หัวหมาก</option>
              <option value="6">สถานี บ้านทับช้าง</option>
              <option value="7">สถานี ลาดกระบัง</option>
              <option value="8">สถานี สุวรรณภูมิ</option>
            </optgroup>
          </select>
        </div>
      </div>
      <div class="clearfix"></div>
      <!-- END -->

      <div class="row">
        <div class="columns small-12">
          <input type="submit" name="submit" class="button expanded button-red-white" value="ค้นหา" >
        </div>
      </div>
      <!-- END -->
    </form>
    <!-- END FORM -->
  </div>
</li>
