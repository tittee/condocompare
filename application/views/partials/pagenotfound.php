<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'js'=>isset($js)?$js:array(),'BodyClass'=>''));

$sfilepath = base_url().'uploads/news';
?>
		<div class="container">
			<div class="bg-cs-gray-second">
				<div class="row align-middle">
					<div class="columns">
						<nav aria-label="You are here:" role="navigation">
							<ul class="breadcrumbs">
								<li><a href="<?php echo base_url(); ?>">หน้าแรก</a></li>
								<li><span class="show-for-sr">Current:</span> <?php echo $title; ?></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
			<div class="bg-cs-gray clearfix">
				<h3 class="text-center color-blue page-title"><?php echo $title; ?></h3>
			</div>

			<!--NEWS SLIDER-->

      <div class="row mar-top-large align-middle">
        <div class="title small-12 medium-2 medium-centered column">
          <h1 class="color-red f_size1_25 text-center ">Oops ! Page not found</h1>
        </div>
      </div>

      <p><img src="<?php echo base_url('assets/images/bg-header.jpg') ?>" alt=""></p>
			<div class="row mar-top-large clearfix">
				<!--LEFT-->
				<div class="small-12 medium-12 column">

					<!--TITLE NEWS-->

          <div class="mar-top-large mar-bott-large clearfix">
            <div class="">

              <h1 class="color-blue f_size1_25 text-center ">หน้าเว็บที่ท่านทำการเรียก ไม่สามารถแสดงข้อมูลได้</h1>
              <div class="well border-none bg-cs-skyblue text-center">เนื่องจากติดแคลชของ การค้นหาจาก Search Engine หรือ การเรียกใช้งานไม่ถูกต้อง</div>
            </div>
          </div>

					<div class="row mar-top-large align-middle">
						<div class="title small-12 medium-2 medium-centered column">
							<a href="#" class="hollow button" href="<?php echo base_url(); ?>">กลับไปที่หน้าแรก</a>
						</div>
					</div>

				</div>
			</div>

			<div class="row clearfix"></div>
		</div>
<?php $this->load->view('partials/footer');?>
<?php
if(isset($slippry))
{
?>
<!--NEWS SLIDER : SLIPPRY-->
<script type="text/javascript">
	$(document).ready(function() {
		jQuery('#out-of-the-box-demo').slippry({
			// general elements & wrapper
			slippryWrapper: '<div class="sy-box out-of-the-box-demo" />', // wrapper to wrap everything, including pager
			elements: 'article', // elments cointaining slide content

			// options
			adaptiveHeight: false, // height of the sliders adapts to current
			captions: false,


			// transitions
			transition: 'horizontal', // fade, horizontal, kenburns, false
			speed: 1200,
			pause: 8000,

			// slideshow
			autoDirection: 'prev'
		});

		jQuery('#pictures-demo').slippry({
		  // general elements & wrapper
		  slippryWrapper: '<div class="sy-box pictures-slider" />', // wrapper to wrap everything, including pager

		  // options
		  adaptiveHeight: false, // height of the sliders adapts to current slide
		  captions: false, // Position: overlay, below, custom, false

		  // pager
		  pager: true,

		  // controls
		  controls: true,
		  autoHover: false,

		  // transitions
		  transition: 'fade', // fade, horizontal, kenburns, false
		  kenZoom: 140,
		  speed: 2000 // time the transition takes (ms)
		});

	});

</script>
<?php }//END IF SLIPPY ?>
