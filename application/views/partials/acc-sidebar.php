<?php defined('BASEPATH') OR exit(); ?>
<ul class="accordion" data-accordion>

	<?php

	if( isset($_COOKIE['mID']) && !empty($_COOKIE['mID']) )
	{
		if( isset($_COOKIE['mTID']) && !empty($_COOKIE['mTID']) && ($_COOKIE['mTID'] == '2') )
		{
	?>

	<li class="accordion-item is-active" data-accordion-item>
		<a href="#" class="accordion-title">ประกาศขายคอนโด</a>
		<div class="accordion-content" data-tab-content>
			<ul class="no-bullet">
				<li><a href="<?php echo site_url('member-create-post-condo'); ?>"><i class="fa fa-building-o" aria-hidden="true"></i> สร้างประกาศ </a></li>
				<li><a href="<?php echo site_url('member-condo-approved'); ?>"><i class="fa fa-building-o" aria-hidden="true"></i> ประกาศที่อนุมัติ </a></li>
				<li><a href="<?php echo site_url('member-condo-unapproved'); ?>"><i class="fa fa-building-o" aria-hidden="true"></i> ประกาศที่รออนุมัติ </a></li>
			</ul>
		</div>
	</li>
	<?php
		}
	}
	?>

	<?php
  /*
	if( isset($_COOKIE['mID']) && !empty($_COOKIE['mID']) )
	{
		if( isset($_COOKIE['mTID']) && !empty($_COOKIE['mTID']) && ($_COOKIE['mTID'] == '2') )
		{
	?>
	<li class="accordion-item" data-accordion-item>
		<a href="#" class="accordion-title">ประกาศขายที่ดิน</a>
		<div class="accordion-content" data-tab-content>
			<ul class="no-bullet">
				<li><a href="<?php echo base_url('page/m_add_land'); ?>"><i class="fa fa-building-o" aria-hidden="true"></i> สร้างประกาศ </a></li>
				<li><a href="<?php echo base_url('page/m_all_land'); ?>"><i class="fa fa-building-o" aria-hidden="true"></i> ประกาศที่อนุมัติ </a></li>
				<li><a href="#"><i class="fa fa-building-o" aria-hidden="true"></i> ประกาศที่รออนุมัติ </a></li>
			</ul>
		</div>
	</li>
	<?php
		}
	}
  */
	?>

	<?php
	if( isset($_COOKIE['mID']) && !empty($_COOKIE['mID']) )
	{
		if( isset($_COOKIE['mTID']) && !empty($_COOKIE['mTID']) && ($_COOKIE['mTID'] == '2') )
		{
	?>
	<li class="accordion-item" data-accordion-item>
		<a href="#" class="accordion-title">การแชร์ประกาศคอนโด</a>
		<div class="accordion-content" data-tab-content>
			<ul class="no-bullet">
				<li><a href="<?php echo site_url('member-share-condo'); ?>"><i class="fa fa-code" aria-hidden="true"></i> รายการประกาศ</a></li>
				<li><a href="<?php echo site_url('member-contact-share-condo'); ?>"><i class="fa fa-code" aria-hidden="true"></i> รายการติดต่อของท่าน</a></li>
			</ul>
		</div>
	</li>
	<?php
		}
	}
	?>

	<?php
	if( isset($_COOKIE['mID']) && !empty($_COOKIE['mID']) )
	{
		if( isset($_COOKIE['mTID']) && !empty($_COOKIE['mTID']) && ($_COOKIE['mTID'] == '2') )
		{
	?>
	<li class="accordion-item" data-accordion-item>
		<a href="#" class="accordion-title">การแชร์ประกาศที่ดิน</a>
		<div class="accordion-content" data-tab-content>
			<ul class="no-bullet">
				<li><a href="<?php echo site_url('member-share-house-and-land'); ?>"><i class="fa fa-code" aria-hidden="true"></i> รายการประกาศ</a></li>
				<li><a href="<?php echo site_url('member-contact-share-house-and-land'); ?>"><i class="fa fa-code" aria-hidden="true"></i> รายการติดต่อของท่าน</a></li>
			</ul>
		</div>
	</li>
	<?php
		}
	}
	?>


	<li class="accordion-item" data-accordion-item>
		<a href="#" class="accordion-title">รายการโปรด</a>
		<div class="accordion-content" data-tab-content>
			<ul class="no-bullet">
				<li><a href="<?php echo site_url('wishlist-condo'); ?>"><i class="fa fa-code" aria-hidden="true"></i> รายการโปรด</a></li>
			</ul>
		</div>
	</li>


	<?php
//	if( isset($_COOKIE['mID']) && !empty($_COOKIE['mID']) )
//	{
//		if( isset($_COOKIE['mTID']) && !empty($_COOKIE['mTID']) && ($_COOKIE['mTID'] != '3') )
//		{
	?>
<!--
	<li class="accordion-item" data-accordion-item>
		<a href="#" class="accordion-title">รายการเปรียบเทียบ</a>
		<div class="accordion-content" data-tab-content>
			<ul class="no-bullet">
				<li><a href="<?php echo base_url('condocompare'); ?>"><i class="fa fa-code" aria-hidden="true"></i> รายการเปรียบเทียบ</a></li>
			</ul>
		</div>
	</li>
-->
	<?php
//		}
//	}
	?>

	<li class="accordion-item" data-accordion-item>
		<a href="#" class="accordion-title">ข้อมูลส่วนตัว</a>
		<div class="accordion-content" data-tab-content>
			<ul class="no-bullet">
				<li><a href="<?php echo base_url('member-profile'); ?>"><i class="fa fa-cogs" aria-hidden="true"></i> ข้อมูลสมาชิก </a></li>
				<li><a href="<?php echo base_url('change-password'); ?>"><i class="fa fa-cogs" aria-hidden="true"></i> แก้ไขรหัสผ่าน </a></li>
			</ul>
		</div>
	</li>
</ul>
