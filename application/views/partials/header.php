<?php
    $site_id = 1;
    $site_setting = $this->M->get_main_setting($site_id);
    $logo = (!empty($site_setting['site_logo'])) ? 'uploads/site_setting/'.$site_setting['site_logo'] : 'assets/images/logo.jpg';
    $favicon = (!empty($site_setting['site_favicon'])) ? 'uploads/site_setting/'.$site_setting['site_favicon'] : 'assets/images/favicon.jpg';
    $site_seo_title = (!empty($site_setting['site_seo_title'])) ? $site_setting['site_seo_title'] : '';
    $site_seo_keyword = (!empty($site_setting['site_seo_meta'])) ? $site_setting['site_seo_meta'] : '';
    $site_seo_description = (!empty($site_setting['site_seo_description'])) ? $site_setting['site_seo_description'] : '';

    $condo_title = (!empty($condo_items['condo_title'])) ? $condo_items['condo_title'] : '';
    $condo_expert = (!empty($condo_items['condo_expert'])) ? $condo_items['condo_expert'] : $condo_title;
    $condo_pic_thumb = (!empty($condo_items['pic_thumb'])) ? base_url().'uploads/condominium/'.$condo_items['pic_thumb'] : '';

    $land_title = (!empty($land_items['land_title'])) ? $land_items['land_title'] : '';
    $land_expert = (!empty($land_items['land_expert'])) ? $land_items['land_expert'] : $land_title;
    $land_pic_thumb = (!empty($land_items['pic_thumb'])) ? base_url().'uploads/land/'.$land_items['pic_thumb'] : '';

    $news_title = (!empty($news_items['news_title'])) ? $news_items['news_title'] : '';
    $news_caption = (!empty($news_items['news_caption'])) ? $news_items['news_caption'] : $news_title;
    $news_pic_thumb = (!empty($news_items['news_pic_thumb'])) ? base_url().'uploads/news/'.$news_items['news_pic_thumb'] : '';

    $reviews_title = (!empty($reviews_items['reviews_title'])) ? $reviews_items['reviews_title'] : '';
    $reviews_caption = (!empty($reviews_items['reviews_caption'])) ? $reviews_items['reviews_caption'] : $reviews_title;
    $reviews_pic_thumb = (!empty($reviews_items['reviews_pic_thumb'])) ? base_url().'uploads/reviews/'.$reviews_items['reviews_pic_thumb'] : '';
?>
	<!DOCTYPE html>
  <html class="no-js" lang="th" dir="ltr">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta charset="UTF-8">
		<?php
        if ($news_title !== '') {
            ?>
			<meta property="fb:app_id" content="176921789401784" />
			<meta property="og:title" content="<?php echo $news_title;
            ?>" />
			<meta property="og:description" content="<?php echo $news_caption;
            ?>" />
			<meta property="og:type" content="website" />
			<meta property="og:url" content="<?php echo current_url();
            ?>" />
			<meta property="og:image" content="<?php echo $news_pic_thumb;
            ?>" />
			<meta property="og:image:width" content="600" />
			<meta property="og:image:height" content="315" />
			<meta property="og:locale" content="th_TH" />
			<meta property="og:locale" content="th_TH" />
			<meta name="description" content="<?php echo $news_caption;
            ?>">
			<title><?php echo $news_title;
            ?></title>
		<?php

        } elseif ($reviews_title != '') {
            ?>
		<meta property="fb:app_id" content="176921789401784" />
		<meta property="og:title" content="<?php echo $reviews_title;
            ?>" />
		<meta property="og:description" content="<?php echo $reviews_caption;
            ?>" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="<?php echo current_url();
            ?>" />
		<meta property="og:image" content="<?php echo $reviews_pic_thumb;
            ?>" />
		<meta property="og:image:width" content="600" />
		<meta property="og:image:height" content="315" />
		<meta property="og:locale" content="th_TH" />
		<meta name="description" content="<?php echo $reviews_caption;
            ?>">
		<title><?php echo $reviews_title;
            ?></title>
		<?php

        } elseif ($land_title != '') {
            ?>
    <!-- The URL of the web version of your article -->
    <link rel="canonical" href="<?php echo current_url(); ?>">
		<meta property="fb:app_id" content="176921789401784" />
    <!-- <meta property="fb:admins" content="176921789401784" /> -->
		<meta property="og:title" content="<?php echo $land_title; ?>" />
		<meta property="og:description" content="<?php echo $land_expert; ?>" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="<?php echo current_url(); ?>" />
		<meta property="og:image" content="<?php echo $land_pic_thumb;?>" />
		<meta property="og:image:width" content="600" />
		<meta property="og:image:height" content="315" />
		<meta property="og:locale" content="th_TH" />
		<meta name="description" content="<?php echo $land_expert;
            ?>">
		<title><?php echo $land_title;
            ?></title>
		<?php

        } elseif ($condo_title !== '') {
            ?>
    <!-- The URL of the web version of your article -->
    <link rel="canonical" href="<?php echo current_url(); ?>">
    <!-- <meta property="fb:admins" content="176921789401784" /> -->
    <meta property="fb:app_id" content="176921789401784" />
		<meta property="og:title" content="<?php echo $condo_title;
            ?>" />
		<meta property="og:description" content="<?php echo $condo_expert;
            ?>" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="<?php echo current_url();
            ?>" />
		<meta property="og:image" content="<?php echo $condo_pic_thumb;
            ?>" />
		<meta property="og:image:width" content="600" />
		<meta property="og:image:height" content="315" />
		<meta property="og:locale" content="th_TH" />
		<meta name="description" content="<?php echo $condo_expert;
            ?>">
		<title><?php echo $condo_title;
            ?></title>
		<?php

        } else {
            ?>
		<meta property="fb:app_id" content="176921789401784" />
		<!-- <meta property="fb:admins" content="176921789401784" /> -->
		<meta property="og:title" content="<?php echo $site_seo_title;
            ?>" />
		<meta property="og:description" content="<?php echo $site_seo_description;
            ?>" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="<?php echo current_url();
            ?>" />
		<meta property="og:image" content="<?php echo base_url().'assets/images/bannerHOB compare-2.jpg';
            ?>" />
		<meta property="og:image:width" content="600" />
		<meta property="og:image:height" content="315" />
		<meta property="og:locale" content="th_TH" />
		<meta name="description" content="<?php echo $site_seo_description;
            ?>">
		<title><?php echo $site_seo_title;
            ?></title>
		<?php

        }
        ?>
        <meta name="keywords" content="<?php echo $site_seo_keyword; ?>">
        <meta name="author" content="CONDO-COMPARE.COM">

		<?php
        //FILE JS
        if (isset($css)) {
            foreach ($css as $datacss) {
                $file_css = base_url().$datacss;
                echo "<link href=\"$file_css\" rel=\"stylesheet\" type=\"text/css\" />\n";
            }
        }
        ?>

			<link rel="shortcut icon" type="image/jpg" href="<?php echo base_url($favicon)?>" />

			<!-- Custom box css -->
			<!-- <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/custombox/dist/custombox.min.css"> -->

			<!-- link(rel = 'stylesheet' href = 'style/foundation-rtl.css')-->
			<link rel="stylesheet" href="<?php echo base_url()?>assets/css/foundation.min.css?v<?php echo VERSION; ?>">
			<!--<link rel="stylesheet" href="<?php echo base_url()?>assets/css/foundation-flex.css">-->
			<link rel="stylesheet" href="<?php echo base_url()?>assets/css/styles.css?v<?php echo VERSION; ?>">
			<link rel="stylesheet" href="<?php echo base_url()?>assets/css/tabs.css?v<?php echo VERSION; ?>">
			<!-- <link rel="stylesheet" href="<?php echo base_url()?>assets/css/owl.theme.css"> -->
			<link rel="stylesheet" href="<?php echo base_url()?>assets/css/owl.theme.default.min.css?v<?php echo VERSION; ?>">
			<link rel="stylesheet" href="<?php echo base_url()?>assets/css/owl.carousel.min.css?v<?php echo VERSION; ?>">
			<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/motion-ui/motion-ui.min.css?v<?php echo VERSION; ?>">
			<link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css?v<?php echo VERSION; ?>">


			<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			  ga('create', 'UA-87277218-1', 'auto');
			  ga('send', 'pageview');

			</script>

			<?php
    if (!empty($googlemap)) {
        ?>
				<style>
					html,
					body,
					#map-canvas {
						height: 100%;
						margin: 0;
						padding: 0;
					}

					#map-canvas {
						width: 100%;
						height: 400px;
					}
				</style>
				<?php

    }
    ?>


					<?php
    if (!empty($googlemap)) {
        ?>
						<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtK77y4LHtYZ5ZoVeVVwpkMUoy-Eq9JRA&sensor=false"></script>
						<script>
							// In the following example, markers appear when the user clicks on the map.
							// Each marker is labeled with a single alphabetical character.
							var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
							var labelIndex = 0;
							var map;

							function initialize() {
								var bangkok = {
									lat: 13.7563,
									lng: 100.5018
								};
								map = new google.maps.Map(document.getElementById('map-canvas'), {
									zoom: 11,
									center: bangkok
								});

								//						var marker = new google.maps.Marker({
								//							position: bangkok,
								//							map: map,
								//							title: 'Set lat/lon values for this property',
								//							draggable: true
								//						});

								var marker = new google.maps.Marker({
									draggable: true,
									position: bangkok,
									map: map,
									title: "Your location"
								});

								// This event listener calls addMarker() when the map is clicked.
								//						google.maps.event.addListener(map, 'click', function(event) {
								//							addMarker(event.latLng, map);
								//						});

								// This event listener calls addMarker() when the map is clicked.
								//addMarker(bangkok, map);

								google.maps.event.addListener(marker, 'dragend', function (event) {
									//console.log( 'latitude : ' + event.latLng.lat()  );
									//console.log( 'longtitude : ' + event.latLng.lng()  );
									$('#latitude').val(event.latLng.lat());
									$('#longitude').val(event.latLng.lng());
								});
							}

							// Adds a marker to the map.
							var i = 0;

							function addMarker(location, map) {
								// Add the marker at the clicked location, and add the next-available label
								// from the array of alphabetical characters.
								var iMax = 1;
								if (i < iMax) {
									var marker = new google.maps.Marker({
										draggable: true,
										position: location,
										label: labels[labelIndex++ % labels.length],
										map: map,
										title: "Your location"
									});
									i++;
								} else {
									console.log('you can only post', i - 1, 'markers');
								}
								//map.panTo(location);
							}

							google.maps.event.addDomListener(window, 'load', initialize);

							function loadScript() {
								var script = document.createElement('script');
								script.type = 'text/javascript';
								script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDtK77y4LHtYZ5ZoVeVVwpkMUoy-Eq9JRA&v=3.exp&sensor=false&callback=initialize';
								document.body.appendChild(script);
							}

							window.onload = loadScript;
						</script>
						<?php

    }
    ?>

	</head>

	<body>



		<div class="container bg-cs-blue">
			<div class="row align-middle">
				<div class="columns">
					<div class="title-bar show-for-small-only" data-hide-for="medium" data-responsive-toggle="nav-menu">
						<!-- <a href="<?php echo base_url('home'); ?>" class="logo-small  show-for-small-only"><img src="<?php echo base_url().$logo?>"></a> -->
              <div class="float-left">
                <button type="button" data-toggle class="menu-icon"></button>
                <div class="title-bar-title">MENU</div>
              </div>

              <!-- Close button -->
              <div class="float-right">
                <a href="<?php echo base_url('home')?>" class="logo-small"><img src="<?php echo base_url().'assets/images/mobile-logo.jpg';?>"></a>
              </div>

					</div>

					<nav id="nav-menu" class="top-bar">
						<div class="top-bar-left logo-wrapper hide-for-small-only">
							<div class="logo"><a href="<?php echo base_url('home')?>" class="logo-small"><img src="<?php echo base_url().$logo?>"></a></div>
						</div>
						<div class="top-bar-left">
							<ul class="vertical medium-horizontal menu">
								<li><a href="<?php echo base_url('condominium')?>" title="คอนโดมิเนียม">คอนโดมิเนียม</a>
								</li>
								<li><a href="<?php echo base_url('house-and-land')?>" title="บ้าน/ที่ดิน">บ้าน/ที่ดิน</a>
								</li>
								<li><a href="<?php echo base_url('news')?>" title="ข่าวสารอัพเดท">ข่าวสารอัพเดท</a>
								</li>
								<li><a href="<?php echo base_url('reviews')?>" title="Reviews">รีวิวอัพเดท</a>
								</li>
								<!-- <li><a href="<?php echo base_url('contact-us')?>" title="Contact Us">Contact Us</a>
								</li> -->
								<li><a href="<?php echo base_url('consignment-agents')?>" title="ฝากขายทรัพย์">ฝากขายทรัพย์</a>
								</li>
								<!-- <li><a href="<?php echo base_url('santorini-hua-hin')?>" title="Santorini Hua Hin">Santorini Hua Hin</a>
								</li> -->

								<?php if (isset($_SESSION['mID'])) { ?>
									<li><a href="<?php echo base_url('member-profile'); ?>" class="show-for-small  hide-for-large"><i class="fa fa-user"></i> โปรไฟล์</a></li>

                  <li><a href="<?php echo site_url('member-share-condo'); ?>" class="show-for-small  hide-for-large"><i class="fa fa-code" aria-hidden="true"></i> รายการแชร์คอนโด</a></li>
          				<li><a href="<?php echo site_url('member-contact-share-condo'); ?>" class="show-for-small  hide-for-large"><i class="fa fa-code" aria-hidden="true"></i> รายการติดต่อแชร์คอนโด</a></li>
                  <li><a href="<?php echo site_url('member-share-house-and-land'); ?>" class="show-for-small  hide-for-large"><i class="fa fa-code" aria-hidden="true"></i> รายการแชร์ที่ดิน</a></li>
          				<li><a href="<?php echo site_url('member-contact-share-house-and-land'); ?>" class="show-for-small  hide-for-large"><i class="fa fa-code" aria-hidden="true"></i> รายการติดต่อแชร์ที่ดิน</a></li>
                  <li><a href="<?php echo site_url('wishlist-condo'); ?>" class="show-for-small  hide-for-large"><i class="fa fa-code" aria-hidden="true"></i> รายการโปรด</a></li>
									<li><a href="<?php echo base_url('page/m_logout'); ?>" class="show-for-small  hide-for-large"><i class="fa fa-sign-out"></i> ออกจากระบบ</a></li>
		<?php

} else {
    ?>
    <li><a href="<?php echo base_url(); ?>member-login" class="show-for-small hide-for-large"><i class="fa fa-sign-in"></i> <span>เข้าสู่ระบบ Condo</span></a></li>
    <li><a href="<?php echo base_url(); ?>member-login-hob" class="show-for-small hide-for-large"><i class="fa fa-sign-in"></i> <span>เข้าสู่ระบบ HOB</span></a></li>
    <li><a href="<?php echo site_url('member-register'); ?>" class="show-for-small hide-for-large"><i class="fa fa-user-plus"></i> <span>สมัครสมาชิก Condo</span></a></li>
    <li><a href="<?php echo site_url('member-register-hob'); ?>" class="show-for-small hide-for-large"><i class="fa fa-user-plus"></i> <span>สมัครสมาชิก HOB</span></a></li>
<?php

}
?>
							</ul>
						</div>
						<div class="top-bar-right show-for-large">
							<ul class="vertical menu medium-horizontal ">
								<?php if (isset($_SESSION['mID'])) {
    ?>
									<li><a href="<?php echo base_url('member-profile');
    ?>"><i class="fa fa-user"></i> โปรไฟล์</a></li>
									<li><a href="<?php echo base_url('page/m_logout');
    ?>"><i class="fa fa-sign-out"></i> ออกจากระบบ</a></li>
              </ul>
									<?php

} else {
    ?>
										<ul class="dropdown menu " data-dropdown-menu>
											<li>
												<a href="#"><i class="fa fa-sign-in"></i> <span>เข้าสู่ระบบ</span></a>
												<ul class="menu  dropdown_right">
													<li><a href="<?php echo base_url();
    ?>member-login"><i class="fa fa-sign-in"></i> <span>เข้าสู่ระบบ Condo</span></a></li>
													<li><a href="<?php echo base_url();
    ?>member-login-hob"><i class="fa fa-sign-in"></i> <span>เข้าสู่ระบบ HOB</span></a></li>
													<!-- ... -->
												</ul>
											</li>



											<li>
												<a href="#"><i class="fa fa-user-plus"></i> <span>สมัครสมาชิก</span></a>
												<ul class="menu  dropdown_right">
													<li><a href="<?php echo site_url('member-register'); ?>"><i class="fa fa-user-plus"></i> <span>สมัครสมาชิก Condo</span></a></li>
													<li><a href="<?php echo site_url('member-register-hob'); ?>"><i class="fa fa-user-plus"></i> <span>สมัครสมาชิก HOB</span></a></li>
													<!-- ... -->
												</ul>
											</li>
										</ul>

										<?php

}

    ?>

						</div>
					</nav>
				</div>
			</div>
		</div>
