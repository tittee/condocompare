<?php
defined('BASEPATH') OR exit;
$this->load->view('header',array('title'=>(isset($title)?$title:''),'css'=>isset($css)?$css:array()));
$url=str_replace("/","-",$this->uri->uri_string());
?>
<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
	<div class="sidebar-inner slimscrollleft">

		<!-- User -->
		<div class="user-box">
			<div class="user-img">
				<img src="<?php echo base_url() ?>assets/images/wittawat2.jpg" alt="user-img" title="Administator" class="img-circle img-thumbnail img-responsive">
				<div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>
			</div>
			<h5><a href="#">Administator</a> </h5>
			<ul class="list-inline">
				<li>
					<a href="#">
						<i class="zmdi zmdi-settings"></i>
					</a>
				</li>

				<li>
					<a href="#" class="text-custom">
						<i class="zmdi zmdi-power"></i>
					</a>
				</li>
			</ul>
		</div>
		<!-- End User -->

		<!--- Sidemenu -->
		<div id="sidebar-menu">
			<ul>
				<li class="text-muted menu-title">Main Menu</li>

				<li>
					<a href="dashboard" class="waves-effect active"><i class="zmdi zmdi-view-dashboard"></i> <span> Dashboard </span> </a>
				</li>

				<li>
					<a href="condo" class="waves-effect"><i class="fa fa-building-o"></i> <span> Condominium </span> </a>
				</li>

				<li>
					<a href="land" class="waves-effect"><i class="zmdi zmdi-landscape"></i> <span> บ้าน/ที่ดิน </span> </a>
				</li>

				<li>
					<a href="member" class="waves-effect"><i class="zmdi zmdi-account"></i> <span> Member </span> </a>
				</li>

				<li>
					<a href="staff" class="waves-effect"><i class="zmdi zmdi-key"></i> <span> Staff </span> </a>
				</li>

			</ul>
			<div class="clearfix"></div>
		</div>
		<!-- Sidebar -->
		<div class="clearfix"></div>

	</div>

</div>
<!-- Left Sidebar End -->
