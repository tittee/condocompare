<?php
defined('BASEPATH') or exit();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
		<title><?php echo html_escape($subject); ?></title>
		<style type="text/css">

.header {
background: #8a8a8a;
}
.header .columns {
padding-bottom: 0;
}
.header p {
color: #fff;
padding-top: 15px;
}
.header .wrapper-inner {
padding: 20px;
}
.header .container {
background: transparent;
}
table.button.facebook table td {
background: #3B5998 !important;
border-color: #3B5998;
}
table.button.twitter table td {
background: #1daced !important;
border-color: #1daced;
}
table.button.google table td {
background: #DB4A39 !important;
border-color: #DB4A39;
}
.wrapper.secondary {
background: #f3f3f3;
}

</style>
</head>
<body>

<wrapper class="header">
<container>
<row class="collapse">
  <columns small="6">
    <img src="<?php echo $logo; ?>">
  </columns>
  <columns small="6">
    <p class="text-right">ข้อมูลการติดต่อจากสมาชิก &#40; การแชร์ของสมาชิก &#41;</p>
  </columns>
</row>
</container>
</wrapper>
<container>
<spacer size="16"></spacer>
<row>
<columns small="12">
  <h1>ขอบคุณ, คุณ <?php echo $land_share_form_name; ?>  ที่ได้ทำการติดต่อเข้ามาในระบบ</h1>
  <p class="lead">ขณะนี้ระบบได้รับข้อมูลการติดต่อของท่านเรียบร้อยแล้ว ทางทีมงานจะรีบติดต่อกลับโดยเร็วที่สุด</p>
  <p>ข้อมูลที่ท่านได้ทำการติดต่อเข้ามา คือ ชื่อทรัพย์ที่ติดต่อ <?php echo $land_title; ?> ณ วันที่  </p>
  <p>ณ วันที่ <?php echo $createdate_t; ?> </p>
  <callout class="primary">
    <p>เบอร์ติดต่อ : <?php echo $land_share_form_phone; ?></p>
    <p>รายละเอียดการติดต่อ : <?php echo $land_share_form_message; ?></p>
  </callout>
</columns>
</row>
<wrapper class="secondary">
<spacer size="16"></spacer>
<row>
  <columns large="6">
    <h5>Connect With Us:</h5>
    <p>คุณ: <?php echo $staff_fullname; ?></p>
  </columns>
  <columns large="6">
    <h5>ตัวแทนอสังหาริมทรัพย์</h5>
    <p>โทร: <?php echo $staff_phone; ?></p>
    <p>อีเมล์: <a href="mailto:<?php echo $staff_email; ?>"><?php echo $staff_email; ?></a></p>
  </columns>
</row>
</wrapper>
</container>

</body>
</html>
