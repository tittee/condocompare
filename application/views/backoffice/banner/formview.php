<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));
/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');



if(isset( $data['row'] ))
{
    $action = 'edit';
    // $this->primaryclass->pre_var_dump($data['row']);
    $banner_category_id = $data['row']->banner_category_id;
    $banner_category_name = $data['row']->banner_category_name;
}
else
{
    $action = 'add';
    $banner_category_id ='';
    $banner_category_name ='';

    // $size_banner='กว้าง : X ยาว :';
}?>
  <div class="content-page">
    <!-- Start content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
              <form action="" id="frmdata" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                <?php if( $action === 'edit'){?>
                  <input type="hidden" name="banner_category_id" id="banner_category_id" value="<?php echo $banner_category_id; ?>">
                  <?php } ?>
                      <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30">รายละเอียดแบนเนอร์ : <u><?php echo $this->uri->segment(3); ?></u></h4>
                      </div>

                        

                      <!-- end box -->
                      <div class="card-box">
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="col-sm-offset-2 col-sm-10 m-t-15">
                                <button id="submit" type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light" onclick="tinyMCE.triggerSave(true,true);">Submit</button>
                                <a href="<?php echo site_url('banner/banner'); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5"> Cancel </a>
                                <input type="hidden" name="action" value="action">
                              </div>
                            </div>
                          </div>
                          <!-- end col -->
                        </div>
                        <!-- end row -->
                      </div>
                      <!-- end box -->
                  </form>
                  <!-- end form -->
          </div>
          <!-- end col -->
        </div>
        <!-- end row -->
      </div>
      <!-- container -->
    </div>
    <!-- end content -->
    <?php $this->load->view('backoffice/partials/footer')?>
      <script>
      $(document).ready(function(){
        //Datepicker
        jQuery('.datepicker-autoclose').datepicker({
            autoclose: true,
            defaultDate: new Date(),
            format: 'yyyy-mm-dd',
            todayHighlight: true,
        });
        // Time Picker
        jQuery('.timepicker').timepicker({
          maxHours		: 12,
          defaultTIme : 'current',
          showSeconds : true,
          showMeridian: false,
        });
      });

        $(document).ready(function() {
          $("#banner_category_id").change(function() {
            /*dropdown post */ //
            $.ajax({
              url: "<?php echo base_url(); ?>banner/get_size_banner",
              data: {
                banner_category_id : $(this).val()
              },
              type: "POST",
              success: function(data) {
                $("#size_banner").html(data);
              }
            });
          });


          $("form#frmdata").submit(function(event) {
            var formData = new FormData($(this)[0]);

            var id = $('#banner_id').val();
            <?php
        $ajax_url = ($action === 'add')? "banner/add" :  "banner/edit/<?php echo $banner_id ?>";
        $msg = ($action === 'add')? "Insert" :  "Update";
        ?>
            var url = "<?php echo base_url().$ajax_url; ?>";
            var formData = new FormData($(this)[0]);
            $.ajax({
              url: url,
              type: "POST",
              data: formData,
              cache: false,
              processData: false,
              contentType: false,
              context: this,
              success: function(data, status) {
                console.log( data );
                swal({
                  title: "Success Data!",
                  text: "Confirm you save",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, <?php echo $msg; ?> it!",
                  cancelButtonText: "No, cancel plx!",
                  closeOnConfirm: true,
                  closeOnCancel: true
                }, function(isConfirm) {
                  if (isConfirm) {
                    window.location.replace('<?php echo base_url(); ?>banner/banner');
                  } else {
                    window.location.reload();
                  }
                });
              },
              error: function(xhr, desc, err) {
                console.log(err);
              },
            });
            event.preventDefault();
            return false;
          });
        })
      </script>
