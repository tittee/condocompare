<?php
defined('BASEPATH') OR exit();
/* HEADER */
$this->load->view('backoffice/partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'BodyClass' => 'fixed-left'));
/* SIDEBAR */
$this->load->view('backoffice/partials/sidebar');
?>
  <!-- ============================================================== -->
  <!-- Start right Content here -->
  <!-- ============================================================== -->
  <div class="content-page">
    <!-- Start content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="card-box">
              <a href="<?php echo $url_href ?>" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">
                <i class="zmdi zmdi-account-add"></i> ADD</a>
              <table id="datatable-responsive" class="table table-striped table-bordered display responsive nowrap" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th class="not-mobile">ลำดับ</th>
                    <!-- <th class="not-mobile">รูปแบนเนอร์</th> -->
                    <th class="">ตำแหน่ง</th>
                    <!-- <th class="">จำนวน</th> -->
                    <!-- <th class="not-mobile">วันที่ / แสดงผล</th> -->
                    <th>จัดการ</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
$i = 1;
foreach($data as $row => $value)
{//START FOREACH#1
    $id = $value->banner_category_id;
    $sfilepath = base_url().'uploads/banner';


    $banner_category_name = $value->banner_category_name;

    // $pic_thumb = $sfilepath.'/'.$value->pic_thumb;
    // $createdate = $value->createdate;
    // $createdate_t = ($createdate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
    ?>
                    <tr>
                      <th>
                        <?php echo $i; ?>
                      </th>

                        <td>
                        <?php echo $banner_category_name; ?>
                      </td>

                      <td>
                        <input type="hidden" name="id" value="<?php echo $id; ?>">
                        <a href="<?php echo site_url('backoffice/banner-category/'.$id); ?>" class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5"><i class="fa fa-eye"></i></a>


                        <a href="#" id="<?php echo $id; ?>" class="btn btn-delete btn-icon waves-effect waves-light btn-danger m-b-5 m-r-5"><i class="ti-trash"></i></a>

                      </td>
                    </tr>
                    <?php $i++;}//END FOREACH#1 ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- End row -->
      </div>
      <!-- container -->
    </div>
    <!-- content -->
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
    <?php $this->load->view('backoffice/partials/footer') ?>
    <script>
    		$(".btn-delete").click(function(event){
    			var id = $(this).attr("id");
    			swal({
    				title: "Warning Data!",
    				text: "Confirm you delete",
    				type: "warning",
    				showCancelButton: true,
    				confirmButtonColor: "#DD6B55",
    				confirmButtonText: "Yes, Delete it!",
    				cancelButtonText: "No, Cancel plx!",
    				closeOnConfirm: false,
    				closeOnCancel: false }, function(isConfirm){
    				if (isConfirm)
    				{
    					$.ajax({
    						url: "<?php echo base_url('banner/delete_banner/'); ?>"+id,
    						type: "POST",
    						data: id,
    						cache:false,
    						success: function (data)
    						{
    							//console.log(data);
    							window.location.reload();
    						},
    						error: function (xhr, desc, err)
    						{
    							console.log( err );
    						}
    					});
    					return false;
    				}
    				else
    				{
    					swal.close();
    					return false;
    				}
    				event.preventDefault();
    				return false;
    			});

    		});

    		$(".btn-publish").click(function(event){
    			event.preventDefault();
    			var id = $(this).attr("id");
    			swal({
    				title: "Warning Data!",
    				text: "Confirm you publish",
    				type: "warning",
    				showCancelButton: true,
    				confirmButtonColor: "#DD6B55",
    				confirmButtonText: "Yes, Publish it!",
    				cancelButtonText: "No, Cancel plx!",
    				closeOnConfirm: false,
    				closeOnCancel: false }, function(isConfirm){
    				if (isConfirm)
    				{
    					$.ajax({
    						url: "<?php echo base_url('banner/display_banner/'); ?>"+id,
    						type: "POST",
    						data: {
    							banner_id : id,
    							publish : '2',
    						},
    						cache:false,
    						success: function (data)
    						{
    //							console.log(data);
    							window.location.reload();
    						},
    						error: function (xhr, desc, err)
    						{
    							console.log( err );
    						}
    					});
    					return false;
    				}
    				else
    				{
    					swal.close();
    					return false;
    				}
    				return false;
    			});
    		});

    		$(".btn-unpublish").click(function(event){
    			event.preventDefault();
    			var id = $(this).attr("id");
    			swal({
    				title: "Warning Data!",
    				text: "Confirm you unpublish",
    				type: "warning",
    				showCancelButton: true,
    				confirmButtonColor: "#DD6B55",
    				confirmButtonText: "Yes, Unpublish it!",
    				cancelButtonText: "No, Cancel plx!",
    				closeOnConfirm: false,
    				closeOnCancel: false }, function(isConfirm){
    				if (isConfirm)
    				{
    					$.ajax({
    						url: "<?php echo base_url('banner/undisplay_banner/'); ?>"+id,
    						type: "POST",
    						data: {
    							banner_id : id,
    							publish : '1',
    						},
    						cache:false,
    						success: function (data)
    						{
    //							console.log(data);
    							window.location.reload();
    						},
    						error: function (xhr, desc, err)
    						{
    							console.log( err );
    						}
    					});
    					return false;
    				}
    				else
    				{
    					swal.close();
    					return false;
    				}

    				return false;
    			});
    		});
    	</script>
