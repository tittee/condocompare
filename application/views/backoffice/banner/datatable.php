<?php
defined('BASEPATH') OR exit();
/* HEADER */
$this->load->view('backoffice/partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'BodyClass' => 'fixed-left'));
/* SIDEBAR */
$this->load->view('backoffice/partials/sidebar');
?>
  <!-- ============================================================== -->
  <!-- Start right Content here -->
  <!-- ============================================================== -->
  <div class="content-page">
    <!-- Start content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="card-box">
              <a href="<?php echo $url_href ?>" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">
                <i class="zmdi zmdi-image"></i> เพิ่ม / แก้ไข แบนเนอร์</a>

              <table id="datatable-export" class="table table-striped table-bordered display responsive nowrap" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th class="not-mobile">ลำดับ</th>
                    <th class="not-mobile">รูปแบนเนอร์</th>
                    <th class="not-mobile">ลิ้งค์แบนเนอร์</th>
                    <th class="">ชื่อแบนเนอร์</th>
                    <th class="not-mobile">แสดงผล</th>
                    <th class="not-mobile">คลิ๊ก</th>
                    <th class="not-mobile">วันที่ / แสดงผล</th>
                    <th>จัดการ</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
$i = 1;
foreach($data as $row => $value)
{//START FOREACH#1
    $id = $value->banner_id;
    $banner_category_id = $value->banner_category_id;
    $sfilepath = base_url().'uploads/banner';

    $pubilsh = $value->publish;
    $displayed = ( !empty($value->displayed) )? $value->displayed.' แสดงผล' : 0;
    $visited = ( !empty($value->visited) )? $value->visited .' วิว' : 0;
    $clicked = ( !empty($value->clicked) )? $value->clicked .' คลิ๊ก': 0;

    $pic_thumb = $sfilepath.'/'.$value->pic_thumb;
    $createdate = $value->createdate;
    $createdate_t = ($createdate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
    ?>
                    <tr>
                      <th>
                        <?php echo $i; ?>
                      </th>
                      <td>
                        <img src="<?php echo $pic_thumb;?>" style="width: 150px;" class="img-thumbnail" alt="">
                      </td>
                      <td><?php echo $pic_thumb ?></td>
                      <td>
                        <?php echo $value->banner_title; ?>
                      </td>
                        <td>
                        <?php echo $displayed; ?>
                      </td>
                      <td>
                        <?php echo $clicked; ?>
                      </td>

                      <td>
                        วันที่สร้าง :
                        <?php echo $createdate_t; ?> <br>
                        <?php echo $this->primaryclass->get_publish($pubilsh); ?>
                      </td>
                      <td>
                        <input type="hidden" name="id" value="<?php echo $value->banner_id; ?>">
                        <!-- <a href="<?php echo site_url('banner/add/'.$banner_category_id); ?>" class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5"><i class="fa fa-eye"></i></a> -->

                        <?php if( $pubilsh == 1){ //ไม่แสดงผล ต้องเปลี่ยนเป็นแสดงผล ?>
                        <a href="#" class="btn btn-publish btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="<?php echo $id; ?>"><i class="zmdi zmdi-globe-alt"></i></a>
                        <?php } else {//แสดงผล ต้องเปลี่ยนเป็นไม่แสดงผล ?>
                        <a href="#" class="btn btn-unpublish btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="<?php echo $id; ?>"><i class="zmdi zmdi-globe-lock"></i></a>
                        <?php }//END IF PUBLISH ?>

                        <a href="#" id="<?php echo $id; ?>" class="btn btn-delete btn-icon waves-effect waves-light btn-danger m-b-5 m-r-5"><i class="ti-trash"></i></a>

                      </td>
                    </tr>
                    <?php $i++;}//END FOREACH#1 ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- End row -->
      </div>
      <!-- container -->
    </div>
    <!-- content -->
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
    <?php $this->load->view('backoffice/partials/footer') ?>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
    <script>

    $(document).ready(function() {
        $('#datatable-export').DataTable({
            dom: 'Bfrtip',
            buttons: [
              {
                extend: 'csvHtml5',
                exportOptions: {
                  // columns: [ 0, 2, 3, 4, 5, 6 ]
                  columns: ':visible'
                }
              },
              {
                extend: 'excelHtml5',
                exportOptions: {
                  // columns: [ 0, 2, 3, 4, 5, 6 ]
                  columns: ':visible'
                }
              },
              'colvis'
            ],
            responsive: true,
      			lengthMenu: [[20, 35, 60, -1], [20, 35, 60, "All"]],
            columnDefs: [
              {
              targets: 0,
              orderable: false
              },
              {
                  "targets": [ 2 ],
                  "visible": false,
                  "searchable": false
              },
            ],
        });
    });

    		$(".btn-delete").click(function(event){
    			var id = $(this).attr("id");
    			swal({
    				title: "Warning Data!",
    				text: "Confirm you delete",
    				type: "warning",
    				showCancelButton: true,
    				confirmButtonColor: "#DD6B55",
    				confirmButtonText: "Yes, Delete it!",
    				cancelButtonText: "No, Cancel plx!",
    				closeOnConfirm: false,
    				closeOnCancel: false }, function(isConfirm){
    				if (isConfirm)
    				{
    					$.ajax({
    						url: "<?php echo base_url('banner/delete_banner/'); ?>"+id,
    						type: "POST",
    						data: id,
    						cache:false,
    						success: function (data)
    						{
    							//console.log(data);
    							window.location.reload();
    						},
    						error: function (xhr, desc, err)
    						{
    							console.log( err );
    						}
    					});
    					return false;
    				}
    				else
    				{
    					swal.close();
    					return false;
    				}
    				event.preventDefault();
    				return false;
    			});

    		});

    		$(".btn-publish").click(function(event){
    			event.preventDefault();
    			var id = $(this).attr("id");
    			swal({
    				title: "Warning Data!",
    				text: "Confirm you publish",
    				type: "warning",
    				showCancelButton: true,
    				confirmButtonColor: "#DD6B55",
    				confirmButtonText: "Yes, Publish it!",
    				cancelButtonText: "No, Cancel plx!",
    				closeOnConfirm: false,
    				closeOnCancel: false }, function(isConfirm){
    				if (isConfirm)
    				{
    					$.ajax({
    						url: "<?php echo base_url('banner/display_banner/'); ?>"+id,
    						type: "POST",
    						data: {
    							banner_id : id,
    							publish : '2',
    						},
    						cache:false,
    						success: function (data)
    						{
    //							console.log(data);
    							window.location.reload();
    						},
    						error: function (xhr, desc, err)
    						{
    							console.log( err );
    						}
    					});
    					return false;
    				}
    				else
    				{
    					swal.close();
    					return false;
    				}
    				return false;
    			});
    		});

    		$(".btn-unpublish").click(function(event){
    			event.preventDefault();
    			var id = $(this).attr("id");
    			swal({
    				title: "Warning Data!",
    				text: "Confirm you unpublish",
    				type: "warning",
    				showCancelButton: true,
    				confirmButtonColor: "#DD6B55",
    				confirmButtonText: "Yes, Unpublish it!",
    				cancelButtonText: "No, Cancel plx!",
    				closeOnConfirm: false,
    				closeOnCancel: false }, function(isConfirm){
    				if (isConfirm)
    				{
    					$.ajax({
    						url: "<?php echo base_url('banner/undisplay_banner/'); ?>"+id,
    						type: "POST",
    						data: {
    							banner_id : id,
    							publish : '1',
    						},
    						cache:false,
    						success: function (data)
    						{
    //							console.log(data);
    							window.location.reload();
    						},
    						error: function (xhr, desc, err)
    						{
    							console.log( err );
    						}
    					});
    					return false;
    				}
    				else
    				{
    					swal.close();
    					return false;
    				}

    				return false;
    			});
    		});
    	</script>
