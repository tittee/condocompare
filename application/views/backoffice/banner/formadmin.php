<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));
/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

$banner_category_id = $data['banner_category']->banner_category_id;
$banner_category_name = $data['banner_category']->banner_category_name;

if(isset( $data['banner'] ))
{
    $action = 'add';
    // $banner_id = $data['banner'][$i]->banner_id;

    // $banner_title = $data['banner'][$i]->banner_title;
    // $pic_thumb = $data['banner'][$i]->pic_thumb;
    // $banner_url = $data['banner'][$i]->banner_url;
    // //CreateDate
    // $createdate = (!empty($data['banner'][$i]->createdate))? $data['banner'][$i]->createdate : date("Y-m-d H:i:s");
    // $createdate_t = explode(" ", $createdate);
    // $createdate_date = $createdate_t[0];
    // $createdate_time = $createdate_t[1];
    // //Modify Date
    // $updatedate = (!empty($data['banner'][$i]->updatedate))? $data['banner'][$i]->updatedate : date("Y-m-d H:i:s");
    // $updatedate_t = explode(" ", $updatedate);
    // $updatedate_date = $updatedate_t[0];
    // $updatedate_time = $updatedate_t[1];
    //
    // $publish 					= $data['banner'][$i]->publish;

}
else
{
    $action = 'add';


    //$size_banner='กว้าง : X ยาว :';
}?>
  <div class="content-page">
    <!-- Start content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
              <form action="" id="frmdata" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                <input type="hidden" name="banner_category_id" id="banner_category_id" value="<?php echo $banner_category_id; ?>">
                  <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">รายละเอียดแบนเนอร์ : <u><?php echo $banner_category_name; ?></u></h4>
                  </div>

                  <?php
                  $j = 1;
                  for ($i=0; $i < 5 ; $i++)
                  {
                    // var_dump($data['banner'][0]);

                    $banner_id = ( !empty($data['banner'][$i]->banner_id) )? $data['banner'][$i]->banner_id : '';
                    $banner_title = ( !empty($data['banner'][$i]->banner_title) )? $data['banner'][$i]->banner_title : '';
                    $banner_url = ( !empty($data['banner'][$i]->banner_url) )? $data['banner'][$i]->banner_url : '';
                    $pic_thumb = ( !empty($data['banner'][$i]->pic_thumb) )? $data['banner'][$i]->pic_thumb : '';
                    $publish = ( !empty($data['banner'][$i]->publish) )? $data['banner'][$i]->publish : 2;

                    //CreateDate
                    $createdate = (!empty($data['banner'][$i]->createdate))? $data['banner'][$i]->createdate : date("Y-m-d H:i:s");
                    $createdate_t = explode(" ", $createdate);
                    $createdate_date = $createdate_t[0];
                    $createdate_time = $createdate_t[1];
                    //Modify Date

                    $updatedate = (!empty($data['banner'][$i]->updatedate))? $data['banner'][$i]->updatedate : date("Y-m-d H:i:s");
                    $updatedate_t = explode(" ", $updatedate);
                    $updatedate_date = $updatedate_t[0];
                    $updatedate_time = $updatedate_t[1];
                  ?>
                  <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-30">แบนเนอร์ : <u><?php echo $j; ?></u></h4>
                  <div class="row">
                    <div class="col-lg-12">

                      <div class="form-group">
                        <label class="col-md-2 control-label">ชื่อแบนเนอร์ </label>
                        <div class="col-md-6">

                          <input type="hidden" id="old_pic_thumb" name="old_pic_thumb[]" value="<?php echo $pic_thumb; ?>">

                          <input type="hidden" name="banner_id[]" id="banner_id" value="<?php echo $banner_id; ?>">
                          <input class="form-control" type="text" value="<?php echo $banner_title; ?>" name="banner_title[]" >
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">ลิ้งแบนเนอร์ </label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" value="<?php echo $banner_url; ?>" name="banner_url[]">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-md-2 control-label" for="pic_thumb">รูปแบนเนอร์ </label>
                        <div class="col-md-6">
                          <input type="file" name="pic_thumb_<?php echo $i; ?>"  class="dropify" data-max-file-size="4M" data-default-file="<?php echo (!empty($pic_thumb))? base_url().'uploads/banner/'.$pic_thumb : '';?>" />
                          <span class="help-block"><small>ขนาดรูปภาพไม่เกิน 4 MB และ <span class="label label-warning size_banner"> <?php //echo $size_banner;?> </span></small>
                          </span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-md-2 control-label">การแสดงผล </label>
                        <div class="col-md-2">
                          <div class="radio-inline radio radio-success radio-single">
                            <input type="radio" name="publish_<?php echo $i; ?>" value="2"  <?php echo ($publish === "2" || $publish === "" )? 'checked': ''; ?>>
                            <label for="">
                              แสดงผล
                            </label>
                          </div>
                        </div>

                        <div class="col-md-2">
                          <div class="radio-inline radio radio-success radio-single">
                            <input type="radio" name="publish_<?php echo $i; ?>"  value="1"  <?php echo ($publish === "1")? 'checked': ''; ?>>
                            <label for="">
                              ไม่แสดงผล
                            </label>
                          </div>
                        </div>
                      </div>
                      <!-- end from-group -->

                      <div class="form-group">
                        <label class="control-label col-sm-2">วันที่สร้าง</label>
                        <div class="col-sm-2">
                          <div class="input-group">
                            <input type="text" name="createdate_date[]" class="form-control datepicker-autoclose" placeholder="" value="<?php echo $createdate_date; ?>" id="">
                            <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                          </div>
                        </div>
                        <div class="col-sm-2">
                          <div class="input-group">
                            <div class="bootstrap-timepicker">
                              <input id="" type="text" name="createdate_time[]" class="timepicker form-control" value="<?php echo $createdate_time; ?>">
                            </div>
                            <span class="input-group-addon bg-primary b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>
                          </div>
                        </div>
                      </div>
                      <!--form-group -->
                      <div class="form-group">
                        <label class="control-label col-sm-2">วันที่แก้ไข</label>
                        <div class="col-sm-2">
                          <div class="input-group">
                            <input type="text" name="updatedate_date[]" class="form-control datepicker-autoclose" placeholder="" value="<?php echo $updatedate_date; ?>" id="">
                            <span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
                          </div>
                        </div>
                        <div class="col-sm-2">
                          <div class="input-group">
                            <div class="bootstrap-timepicker">
                              <input id="" name="updatedate_time[]" type="text" class="timepicker form-control" value="<?php echo $updatedate_time; ?>">
                            </div>
                            <span class="input-group-addon bg-primary b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>
                          </div>
                        </div>
                      </div>
                      <!-- form-group -->

                    </div>
                    <!-- end col -->
                  </div>
                  <!-- end row -->
                </div>
                  <?php $j++; }//END FOR ?>

                      <div class="card-box">
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="col-sm-offset-2 col-sm-10 m-t-15">
                                <button id="submit" type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light" onclick="tinyMCE.triggerSave(true,true);">Submit</button>
                                <a href="<?php echo site_url('backoffice/banner-category/'.$banner_category_id); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5"> Cancel </a>
                                <input type="hidden" name="action" value="action">
                              </div>
                            </div>
                          </div>
                          <!-- end col -->
                        </div>
                        <!-- end row -->
                      </div>
                      <!-- end box -->
                  </form>
                  <!-- end form -->
          </div>
          <!-- end col -->
        </div>
        <!-- end row -->
      </div>
      <!-- container -->
    </div>
    <!-- end content -->
    <?php $this->load->view('backoffice/partials/footer')?>
      <script>
      $(document).ready(function(){
        //Datepicker
        jQuery('.datepicker-autoclose').datepicker({
            autoclose: true,
            defaultDate: new Date(),
            format: 'yyyy-mm-dd',
            todayHighlight: true,
        });
        // Time Picker
        jQuery('.timepicker').timepicker({
          maxHours		: 12,
          defaultTIme : 'current',
          showSeconds : true,
          showMeridian: false,
        });
      });


        $(document).ready(function() {

          var banner_category_id = $("#banner_category_id").val();
          if( banner_category_id !== 0 )
          {
            $.ajax({
              url: "<?php echo base_url(); ?>banner/get_size_banner",
              data: {
                banner_category_id : banner_category_id
              },
              type: "POST",
              success: function(data) {
                // console.log(data);
                $(".size_banner").html(data);
              }
            });
          }

          $("#banner_category_id").change(function() {
            /*dropdown post */ //
            $.ajax({
              url: "<?php echo base_url(); ?>banner/get_size_banner",
              data: {
                banner_category_id : $(this).val()
              },
              type: "POST",
              success: function(data) {
                $(".size_banner").html(data);
              }
            });
          });




          $("form#frmdata").submit(function(event) {
            var formData = new FormData($(this)[0]);

            var id = $('#banner_id').val();
            <?php
        $ajax_url = ($action === 'add')? "banner/add/<?php echo $banner_category_id ?>" :  "banner/edit/<?php echo $banner_category_id ?>";

        $url_redirect = site_url('backoffice/banner-category/'.$banner_category_id);

        $msg = ($action === 'add')? "Insert" :  "Update";
        ?>
            var url = "<?php echo base_url().$ajax_url; ?>";
            var formData = new FormData($(this)[0]);
            $.ajax({
              url: url,
              type: "POST",
              data: formData,
              cache: false,
              processData: false,
              contentType: false,
              context: this,
              success: function(data, status) {
                console.log( data );
                swal({
                  title: "Success Data!",
                  text: "Confirm you save",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, <?php echo $msg; ?> it!",
                  cancelButtonText: "No, cancel plx!",
                  closeOnConfirm: true,
                  closeOnCancel: true
                }, function(isConfirm) {
                  if (isConfirm) {
                    window.location.replace('<?php echo $url_redirect; ?>');
                  } else {
                    window.location.reload();
                  }
                });
              },
              error: function(xhr, desc, err) {
                console.log(err);
              },
            });
            event.preventDefault();
            return false;
          });
        })
      </script>
