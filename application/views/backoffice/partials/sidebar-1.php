<?php defined('BASEPATH') OR exit(); ?>
	<!-- ========== Left Sidebar Start ========== -->
	<div class="left side-menu">
		<div class="sidebar-inner slimscrollleft">

			<!-- User -->
			<div class="user-box">
				<div class="user-img">
					<img src="http://placekitten.com/140/140" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
					<div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>
				</div>
				<h5><a href="#">Administator</a> </h5>
				<ul class="list-inline">
					<li>
						<a href="#">
							<i class="zmdi zmdi-settings"></i>
						</a>
					</li>

					<li>
						<a href="<?php echo site_url('backoffice/logout'); ?>" class="text-custom">
							<i class="zmdi zmdi-power"></i>
						</a>
					</li>
				</ul>
			</div>
			<!-- End User -->

			<!--- Sidemenu -->
			<div id="sidebar-menu">
				<ul>
					<li class="text-muted menu-title">Main Menu</li>

					<li>
						<a href="<?php echo site_url('backoffice/dashboard'); ?>" class="waves-effect active"><i class="zmdi zmdi-view-dashboard"></i> <span> หน้าแรก </span> </a>
					</li>

          <?php
          if( !empty($this->input->cookie('zGROUPID')) )
          {
            $row_permission = $this->Staff_model->get_permissions_relation_by_type_staff_id( $this->input->cookie('zGROUPID') );

            //$this->primaryclass->pre_var_dump( $row_permission );
            foreach ($row_permission as $key => $value) {
              # code...
              $permissions_id =  $value->permissions_id;
              // echo "<br />";
              $display_on = '';
              $display_off = 'style="display: none;"';
          ?>

					<li  class="has_sub">
						<a href="javascript:void(0);" class="waves-effect"><i class="fa fa-building-o"></i> <span> คอนโด </span> <span class="menu-arrow"></span></a>
						<ul class="list-unstyled">
							<li <?php echo ($permissions_id === '1')? $display_on : $display_off; ?> ><a href="<?php echo site_url('condominium/view'); ?>">รายการคอนโด</a></li>
							<li <?php echo ($permissions_id === '2')? $display_on : $display_off; ?>><a href="<?php echo site_url('condominium/view_highlight'); ?>">รายการคอนโดแนะนำ</a></li>
							<li <?php echo ($permissions_id === '3')? $display_on : $display_off; ?>><a href="<?php echo site_url('condominium/view_facilities'); ?>">รายการสิ่งอำนวยความสะดวก</a></li>
							<li <?php echo ($permissions_id === '4')? $display_on : $display_off; ?>><a href="<?php echo site_url('condominium/view_featured'); ?>">รายการจุดเด่น</a></li>
							<li <?php echo ($permissions_id === '5')? $display_on : $display_off; ?>><a href="<?php echo site_url('condominium/view_transportation_category'); ?>">รายการหมวดหมู่สถานที่ใกล้เคียง</a></li>
							<li <?php echo ($permissions_id === '6')? $display_on : $display_off; ?> ><a href="<?php echo site_url('condominium/view_transportation'); ?>">รายการสถานที่ใกล้เคียง</a></li>
							<li <?php echo ($permissions_id === '7')? $display_on : $display_off; ?> ><a href="<?php echo site_url('condominium/view_mass_transportation_category'); ?>">รายการหมวดหมู่ขนส่งมวลชน</a></li>
							<li <?php echo ($permissions_id === '8')? $display_on : $display_off; ?> ><a href="<?php echo site_url('condominium/view_mass_transportation'); ?>">รายการขนส่งมวลชน</a></li>
						</ul>
					</li>



					<li <?php echo ($permissions_id === 9 )? $display_on : $display_off; ?> class="has_sub">
						<a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-landscape"></i> <span> ที่ดิน </span> <span class="menu-arrow"></span></a>
						<ul class="list-unstyled">
							<li <?php echo ($permissions_id === 9)? $display_on : $display_off; ?> ><a href="<?php echo site_url('land/view'); ?>">รายการที่ดิน</a></li>
						</ul>
					</li>


					<li <?php echo ($permissions_id === 10 )? $display_on : $display_off; ?> >
						<a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-labels"></i> <span> ซานโตรินี่ </span> <span class="menu-arrow"></span></a>
						<ul class="list-unstyled">
							<li <?php echo ($permissions_id === 10)? $display_on : $display_off; ?> ><a href="<?php echo site_url(''); ?>">รายการซานโตรินี่</a></li>
						</ul>
					</li>

					<li <?php echo ($permissions_id === 11 || $permissions_id === 12 )? $display_on : $display_off; ?> class="has_sub">
						<a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-account"></i> <span> สมาชิก </span> <span class="menu-arrow"></span></a>
						<ul class="list-unstyled">
							<li <?php echo ($permissions_id === 11)? $display_on : $display_off; ?> ><a href="<?php echo site_url('member/member'); ?>">รายการสมาชิก</a></li>
							<li <?php echo ($permissions_id === 12)? $display_on : $display_off; ?> ><a href="<?php echo site_url('member/groupview'); ?>">รายกลุ่มสมาชิก</a></li>
						</ul>
					</li>



					<li <?php echo ($permissions_id === 13 || $permissions_id === 14 )? $display_on : $display_off; ?> class="has_sub">
						<a href="javascript:void(0);"  class="waves-effect"><i class="zmdi zmdi-book"></i> <span> ข่าวสาร </span> <span class="menu-arrow"></span></a>
						<ul class="list-unstyled">
							<li <?php echo ($permissions_id === 13)? $display_on : $display_off; ?> ><a href="<?php echo site_url('news/news'); ?>">รายการข่าวสาร</a></li>
							<li <?php echo ($permissions_id === 14)? $display_on : $display_off; ?> ><a href="<?php echo site_url('news/category'); ?>">รายการหมวดหมู่</a></li>
						</ul>
					</li>



					<li <?php echo ($permissions_id === 15)? $display_on : $display_off; ?> >
						<a href="javascript:void(0);"  class="waves-effect"><i class="zmdi zmdi-local-activity"></i> <span> กิจกรรม </span> <span class="menu-arrow"></span></a>
						<ul class="list-unstyled">
							<li <?php echo ($permissions_id === 15)? $display_on : $display_off; ?> ><a href="<?php echo site_url('activities/activities'); ?>">รายการกิจกรรม</a></li>
						</ul>
					</li>



					<li <?php echo ($permissions_id === 16 || $permissions_id === 17 )? $display_on : $display_off; ?> >
						<a href="javascript:void(0);"  class="waves-effect"><i class="zmdi zmdi-collection-case-play"></i> <span> รีวิว </span> <span class="menu-arrow"></span></a>
						<ul class="list-unstyled">
							<li <li <?php echo ($permissions_id === 16)? $display_on : $display_off; ?> > ><a href="<?php echo site_url('reviews/reviews'); ?>">รายการรีวิว</a></li>
							<li <li <?php echo ($permissions_id === 17)? $display_on : $display_off; ?> > ><a href="<?php echo site_url('reviews/category'); ?>">รายการหมวดหมู่</a></li>
						</ul>
					</li>


					<!-- <li>
						<a href="javascript:void(0);"  class="waves-effect"><i class="zmdi zmdi-accounts"></i> <span> กูรู </span> <span class="menu-arrow"></span></a>
						<ul class="list-unstyled">
							<li><a href="<?php echo site_url('guru/guru'); ?>">กูรู</a></li>
						</ul>
					</li> -->


					<li <?php echo ($permissions_id === 18 )? $display_on : $display_off; ?> >
						<a href="javascript:void(0);"  class="waves-effect"><i class="zmdi zmdi-image-o"></i> <span> แบนเนอร์ </span> <span class="menu-arrow"></span></a>
						<ul class="list-unstyled">
							<li <?php echo ($permissions_id === 18 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('banner/banner'); ?>">รายการแบนเนอร์</a></li>
						</ul>
					</li>



					<li <?php echo ($permissions_id === 19 )? $display_on : $display_off; ?> >
						<a href="javascript:void(0);"  class="waves-effect"><i class="zmdi zmdi-movie"></i> <span> วีดีโอโครงการใหม่ </span> <span class="menu-arrow"></span></a>
						<ul class="list-unstyled">
							<li <?php echo ($permissions_id === 19 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('backoffice/site_video'); ?>">วีดีโอโครงการใหม่</a></li>
						</ul>
					</li>

					<li <?php echo ($permissions_id === 20 )? $display_on : $display_off; ?> >
						<a href="javascript:void(0);"  class="waves-effect"><i class="zmdi zmdi-movie"></i> <span> วีดีโอ </span> <span class="menu-arrow"></span></a>
						<ul class="list-unstyled">
							<li <?php echo ($permissions_id === 20 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('backoffice/site_video'); ?>">รายการวีดีโอ</a></li>
						</ul>
					</li>

					<li <?php echo ($permissions_id === 21 || $permissions_id === 22 || $permissions_id === 23 || $permissions_id === 24 || $permissions_id === 25 )? $display_on : $display_off; ?> >
						<a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-key"></i> <span> ผู้ดูแลระบบ </span> <span class="menu-arrow"></span></a>
						<ul class="list-unstyled">
							<li <?php echo ($permissions_id === 21 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('staff/staff'); ?>">รายการผู้ดูแลระบบ</a></li>
							<li <?php echo ($permissions_id === 22 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('staff/type_staff'); ?>">รายการประเภทผู้ดูแลระบบ</a></li>
							<li <?php echo ($permissions_id === 23 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('staff/permissions_category'); ?>">รายการหมวดสิทธิ์</a></li>
							<li <?php echo ($permissions_id === 24 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('staff/permissions'); ?>">รายการสิทธิ์</a></li>
							<li <?php echo ($permissions_id === 25 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('staff/permissions'); ?>">สิทธิ์การเข้าใช้งาน</a></li>
						</ul>
					</li>


					<li <?php echo ($permissions_id === 27 || $permissions_id === 28 || $permissions_id === 29 || $permissions_id === 30 || $permissions_id === 31 || $permissions_id === 32 )? $display_on : $display_off; ?> class="has_sub">
						<a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-settings"></i> <span> ข้อมูลหลักของระบบ </span> <span class="menu-arrow"></span></a>
						<ul class="list-unstyled">
							<li <?php echo ($permissions_id === 27 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('configsystem/view_type_status'); ?>">รายการสถานะประกาศ</a></li>
							<li <?php echo ($permissions_id === 28 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('configsystem/view_type_property'); ?>">รายการประเภททรัพย์</a></li>
							<li <?php echo ($permissions_id === 29 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('configsystem/view_type_offering'); ?>">รายการประเภทการเสนอขาย</a></li>
							<li <?php echo ($permissions_id === 30 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('configsystem/view_type_zone'); ?>">รายการโซน</a></li>
							<li <?php echo ($permissions_id === 31 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('configsystem/view_type_suites'); ?>">รายการแบบห้องชุด</a></li>
							<li <?php echo ($permissions_id === 32 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('configsystem/view_type_decoration'); ?>">รายการการตกแต่ง</a></li>
						</ul>
					</li>


					<li <?php echo ($permissions_id === 34 || $permissions_id === 35 || $permissions_id === 36 || $permissions_id === 37 || $permissions_id === 38 || $permissions_id === 39 )? $display_on : $display_off; ?> class="has_sub">
						<a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-wrench"></i> <span> ตั้งค่า </span> <span class="menu-arrow"></span></a>
						<ul class="list-unstyled">
							<li <?php echo ($permissions_id === 34 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('backoffice/site_general'); ?>">รายการตั้งค่าหลักเว็บไซต์</a></li>
							<li <?php echo ($permissions_id === 35 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('backoffice/site_email'); ?>">ตั้งค่าอีเมล์</a></li>
							<li <?php echo ($permissions_id === 36 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('backoffice/site_seo'); ?>">ตั้งค่าการค้นหา SEO</a></li>
							<li <?php echo ($permissions_id === 37 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('backoffice/site_social'); ?>">ตั้งค่าโซเชี่ยล</a></li>
							<li <?php echo ($permissions_id === 38 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('backoffice/site_footer'); ?>">ตั้งค่ารายการ footer</a></li>
							<li <?php echo ($permissions_id === 39 )? $display_on : $display_off; ?> ><a href="<?php echo site_url('backoffice/site_loan'); ?>">ตั้งค่าอัตราดอกเบี้ย</a></li>
						</ul>
					</li>


				</ul>
				<div class="clearfix"></div>
			</div>
			<!-- Sidebar -->
			<div class="clearfix"></div>

		</div>

	</div>
	<!-- Left Sidebar End -->
