<?php defined('BASEPATH') or exit(); ?>

	<footer class="footer">
		2016 © Harrison Co., Ltd
	</footer>

	</div>



	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->



	<script>
		var resizefunc = [];
	</script>

	<!-- jQuery  -->
	<script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/detect.js"></script>
	<script src="<?php echo base_url()?>assets/js/fastclick.js"></script>
	<script src="<?php echo base_url()?>assets/js/jquery.slimscroll.js"></script>
	<script src="<?php echo base_url()?>assets/js/jquery.blockUI.js"></script>
	<script src="<?php echo base_url()?>assets/js/waves.js"></script>
	<script src="<?php echo base_url()?>assets/js/jquery.nicescroll.js"></script>
	<script src="<?php echo base_url()?>assets/js/jquery.scrollTo.min.js"></script>

	<!-- TinyMCE -->
	<script src="<?php echo base_url()?>assets/plugins/tinymce/tinymce.min.js"></script>

	<!-- Dropzone -->
	<script src="<?php echo base_url()?>assets/js/dropzone.min.js"></script>

	<!-- Sweet Alert -->
	<script src="<?php echo base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
	<script src="<?php echo base_url()?>assets/pages/jquery.sweet-alert.init.js"></script>

	<script src="<?php echo base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url()?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
	<script src="<?php echo base_url()?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
	<script src="<?php echo base_url()?>assets/plugins/datatables/buttons.bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/plugins/datatables/jszip.min.js"></script>
	<script src="<?php echo base_url()?>assets/plugins/datatables/pdfmake.min.js"></script>
	<script src="<?php echo base_url()?>assets/plugins/datatables/vfs_fonts.js"></script>
	<script src="<?php echo base_url()?>assets/plugins/datatables/buttons.html5.min.js"></script>
	<script src="<?php echo base_url()?>assets/plugins/datatables/buttons.print.min.js"></script>
	<script src="<?php echo base_url()?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
	<script src="<?php echo base_url()?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
	<script src="<?php echo base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
	<script src="<?php echo base_url()?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/plugins/datatables/dataTables.rowReorder.min.js"></script>
	<script src="<?php echo base_url()?>assets/plugins/datatables/dataTables.scroller.min.js"></script>

	<!-- file uploads js -->
	<script src="<?php echo base_url()?>assets/plugins/fileuploads/js/dropify.min.js"></script>




	<!-- Inputmask js -->
	<script src="<?php echo base_url()?>assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>

	<!-- Touchspin js-->
	<script src="<?php echo base_url()?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>

	<!--Max Length-->
	<script src="<?php echo base_url()?>assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>



	<!-- Include Code Mirror. -->

	<!--form wysiwigeditor js-->
	<?php
    if (isset($wysiwigeditor)) {
        ?>
		<script type="text/javascript">
			$(document).ready(function () {
				if ($(".wysiwigeditor").length > 0) {
					tinymce.init({
						selector: "textarea.wysiwigeditor",
						theme: "modern",

						//entity_encoding:"raw",
						forced_root_block : " ",
						//valid_elements :"a[href|target=_blank],strong,u,p,iframe[src|frameborder|style|scrolling|class|width|height|name|align]",
    				//extended_valid_elements : "iframe[src|frameborder|style|scrolling|class|width|height|name|align]",
    				media_strict: false,
						paste_data_images: true,
						height: 300,
						plugins: [
											"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
											"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
											"save table contextmenu directionality emoticons template paste textcolor jbimages youtube"
									],
						toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages | print preview fullpage | forecolor backcolor emoticons | youtube",
						image_advtab: true,
						relative_urls: false,
						extended_valid_elements: "+iframe[src|width|height|name|align|class]",
						file_picker_callback: function(callback, value, meta) {
							if (meta.filetype == 'image') {
								$('#upload_wysiwigeditor, #upload_wysiwigeditor2').trigger('click');
								$('#upload_wysiwigeditor, #upload_wysiwigeditor2').on('change', function() {
									var file = this.files[0];
									var reader = new FileReader();
									reader.onload = function(e) {
										callback(e.target.result, {
											alt: ''
										});
									};
									reader.readAsDataURL(file);
								});
							}
						},
						style_formats: [
							{
								title: 'Bold text',
								inline: 'b'
						},
							{
								title: 'Red text',
								inline: 'span',
								styles: {
									color: '#ff0000'
								}
						},
							{
								title: 'Red header',
								block: 'h1',
								styles: {
									color: '#ff0000'
								}
						},
							{
								title: 'Example 1',
								inline: 'span',
								classes: 'example1'
						},
							{
								title: 'Example 2',
								inline: 'span',
								classes: 'example2'
						},
							{
								title: 'Table styles'
						},
							{
								title: 'Table row 1',
								selector: 'tr',
								classes: 'tablerow1'
						}
									]
					});
				}
			});
		</script>
		<?php
    }//END IF ?>



			<script type="text/javascript">
				$(document).ready(function () {

					//			TableManageButtons.init();


					$('.dropify').dropify({
						messages: {
							'default': 'Drag and drop a file here or click',
							'replace': 'Drag and drop or click to replace',
							'remove': 'Remove',
							'error': 'Ooops, something wrong appended.'
						},
						error: {
							'fileSize': 'The file size is too big (1M max).'
						}
					});



					$('input#yearsoption').maxlength({
						alwaysShow: true,
						warningClass: "label label-success",
						limitReachedClass: "label label-danger",
						separator: ' out of ',
						preText: 'You typed ',
						postText: ' chars available.',
						validate: true
					});

					$('.zipcode, .inputmaxlength').maxlength({
						alwaysShow: true,
						warningClass: "label label-success",
						limitReachedClass: "label label-danger",
						separator: ' out of ',
						preText: 'You typed ',
						postText: ' chars available.',
						validate: true
					});


					$(".numeric_picker").TouchSpin({
						buttondown_class: "btn btn-primary",
						buttonup_class: "btn btn-primary",
						forcestepdivisibility: 'none'
					});

					$(".float_price").TouchSpin({
						min: 0,
						max: 10000000000,
						decimals: 2,
						stepinterval: 50,
						forcestepdivisibility: 'none',
						buttondown_class: "btn btn-primary",
						buttonup_class: "btn btn-primary",
					});

					$(".demical_picker").TouchSpin({
						min: 0,
						max: 10000,
						decimals: 2,
						forcestepdivisibility: 'none',
						buttondown_class: "btn btn-primary",
						buttonup_class: "btn btn-primary",
					});

					$(".numeric_price").TouchSpin({
						min: -1000000000,
						max: 1000000000,
						stepinterval: 50,
						forcestepdivisibility: 'none',
						buttondown_class: "btn btn-primary",
						buttonup_class: "btn btn-primary",
						prefix: '฿'
					});
				});
			</script>

			<?php
    //FILE JS
    if (!empty($js)) {
        foreach ($js as $datajs) {
            $filejs = base_url().$datajs;
            echo "<script src=\"$filejs\"></script>\n";
        }
    }
    ?>

	<script>
		$(".percent").TouchSpin({
			min: 0,
			max: 100,
			step: 1,
			boostat: 5,
			maxboostedstep: 10,
			buttondown_class: "btn btn-primary",
			buttonup_class: "btn btn-primary",
			postfix: '%'
		});

		$(".integernum").TouchSpin({
			min: 0,
			max: 1000000000,
			step: 50,
			boostat: 50,
			stepinterval: 50,
			decimals: 2,
			buttondown_class: "btn btn-primary",
			buttonup_class: "btn btn-primary",
			maxboostedstep: 10000000,
			prefix: '฿'
		});
	</script>


	<script>
	$(document).ready(function(){
		$('#datatable-responsive').DataTable({
			responsive: true,
			lengthMenu: [[20, 35, 60, -1], [20, 35, 60, "All"]],
      columnDefs: [ {
        targets: 0,
        orderable: false
      }],
		});
	});

  function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }
	</script>

				<!-- App js -->
				<script src="<?php echo base_url()?>assets/js/jquery.core.js"></script>
				<script src="<?php echo base_url()?>assets/js/jquery.app.js"></script>
