<?php defined('BASEPATH') OR exit();
if(ENVIRONMENT!=='production'){} ?>

	<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js lt-ie10"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
		<meta name="author" content="Coderthemes">

		<link rel="shortcut icon" type="image/jpg" ref="<?php echo base_url()?>assets/images/favicon.jpg">

		<title>HARRISON : Condo Compare</title>


		<?php
		//FILE JS
		if( isset($css) )
		{
			foreach($css as $datacss)
			{
				$file_css = base_url().$datacss;
				echo "<link href=\"$file_css\" rel=\"stylesheet\" type=\"text/css\" />\n";
			}
		}
		?>
    <style media="screen">
      .table-hover > tbody > tr:hover{
        cursor: move;
      }
    </style>

			<!-- form Uploads -->
			<link href="<?php echo base_url()?>assets/plugins/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />

			<!--Morris Chart CSS -->

			<!-- Datepicker css-->
			<link href="<?php echo base_url()?>assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">

			<!-- Touchspin css-->
			<link href="<?php echo base_url()?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />

			<!--  Sweet Alerts -->
			<link href="<?php echo base_url()?>assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />

			<!--  Data Tables -->
			<link href="<?php echo base_url()?>assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url()?>assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url()?>assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url()?>assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url()?>assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url()?>assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

			<!-- App css -->
			<link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url()?>assets/css/core.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url()?>assets/css/components.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url()?>assets/css/icons.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url()?>assets/css/pages.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url()?>assets/css/menu.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url()?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url()?>assets/css/backoffice.css" rel="stylesheet" type="text/css" />



			<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
			<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
			<!--[if lt IE 9]>
					<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
					<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
					<![endif]-->
			<script src="<?php echo base_url()?>assets/js/modernizr.min.js"></script>
	<?php
	if(!empty($googlemap))
	{
		?>
		<style>
			#map-canvas {
				height: 100%;
				margin: 0;
				padding: 0;
			}

			#map-canvas {
				width: 100%;
				height: 400px;
			}
		</style>
		<?php
	}
	?>





	</head>

	<body class="<?php echo $BodyClass ?>">

		<!-- Begin page -->
		<div id="wrapper">

		<?php if ($header){ ?>
			<!-- Top Bar Start -->
			<div class="topbar">

				<!-- LOGO -->
				<div class="topbar-left">
					<a href="#" class="logo"><span>Backoffice<span> 's</span></span><i class="zmdi zmdi-layers"></i></a>
				</div>

				<!-- Button mobile view to collapse sidebar menu -->
				<div class="navbar navbar-default" role="navigation">
					<div class="container">

						<!-- Page title -->
						<ul class="nav navbar-nav navbar-left">
							<li>
								<button class="button-menu-mobile open-left">
									<i class="zmdi zmdi-menu"></i>
								</button>
							</li>
							<li>
								<h4 class="page-title"><?php echo $title; ?></h4>
							</li>
						</ul>

						<!-- Right(Notification and Searchbox -->
						<!-- <ul class="nav navbar-nav navbar-right"> -->
							<!-- <li> -->
								<!-- Notification -->
								<!-- <div class="notification-box">
									<ul class="list-inline m-b-0">
										<li>
											<a href="javascript:void(0);" class="right-bar-toggle">
												<i class="zmdi zmdi-notifications-none"></i>
											</a>
											<div class="noti-dot">
												<span class="dot"></span>
												<span class="pulse"></span>
											</div>
										</li>
									</ul>
								</div> -->
								<!-- End Notification bar -->
							<!-- </li> -->
						<!-- </ul> -->

					</div>
					<!-- end container -->
				</div>
				<!-- end navbar -->
			</div>
			<!-- Top Bar End -->

			<?php } ?>
