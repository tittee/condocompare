<?php defined('BASEPATH') OR exit(); ?>
	<!-- ========== Left Sidebar Start ========== -->
	<div class="left side-menu">
		<div class="sidebar-inner slimscrollleft">

			<!-- User -->
			<div class="user-box">
				<div class="user-img">
					<img src="http://placekitten.com/140/140" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
					<div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>
				</div>
				<h5><a href="#">Administator</a> </h5>
				<ul class="list-inline">
					<li>
						<a href="#">
							<i class="zmdi zmdi-settings"></i>
						</a>
					</li>

					<li>
						<a href="<?php echo site_url('backoffice/logout'); ?>" class="text-custom">
							<i class="zmdi zmdi-power"></i>
						</a>
					</li>
				</ul>
			</div>
			<!-- End User -->

			<!--- Sidemenu -->
			<div id="sidebar-menu">
				<ul>
					<li class="text-muted menu-title">Main Menu</li>

					<!-- <li>
						<a href="<?php echo site_url('backoffice/dashboard'); ?>" class="waves-effect active"><i class="zmdi zmdi-view-dashboard"></i> <span> หน้าแรก </span> </a>
					</li> -->

					<?php
					if( !empty($this->input->cookie('zGROUPID')) )
					{




            $category = $this->Staff_model->get_permissions_category();
            foreach ($category as $key_cate => $value_cate)
            {
							if( $value_cate->permissions_category_id === '1' )
							{
								echo "<li>
									<a href='".site_url('backoffice/dashboard')."' class=\"waves-effect active\"><i class=\"zmdi zmdi-view-dashboard\"></i> <span> หน้าแรก </span> </a>
								</li>";
							}
							else
							{
              $permissions_category_id = $value_cate->permissions_category_id;
              $permissions_category_title = $value_cate->permissions_category_title;
              $permissions_category_icon = $value_cate->permissions_category_icon;
              $permissions_category_controller = $value_cate->permissions_category_controller;
					?>
          <li class="has_sub">
            <a href="<?php echo $permissions_category_controller; ?>" class="waves-effect"><i class="<?php echo $permissions_category_icon; ?>"></i> <span> <?php echo $permissions_category_title; ?> </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
              <?php
              $permissions = $this->Staff_model->get_permissions_by_category_id($permissions_category_id);
              foreach ($permissions as $key_per => $value_per)
              {
                // $row_permission = $this->Staff_model->get_permissions_relation_by_type_staff_id( $this->input->cookie('zGROUPID') );
                // print_r($row_permission);
                $permissions_id = $value_per->permissions_id;
                $permissions_title = $value_per->permissions_title;
                $permissions_controller = $value_per->permissions_controller;
                $selected_op =   '';
              ?>
							<li <?php echo $selected_op; ?> ><a href="<?php echo site_url($permissions_controller); ?>"><?php echo $permissions_title; ?></a></li>
              <?php } ?>
						</ul>
					</li>
					<?php
				}//END IF
        }//END FOREACH CATEGORY
      }//END IF COOKIES
				?>



				</ul>
				<div class="clearfix"></div>
			</div>
			<!-- Sidebar -->
			<div class="clearfix"></div>

		</div>

	</div>
	<!-- Left Sidebar End -->
