<?php
defined('BASEPATH') or exit();
/*HEADER*/
$this->load->view('backoffice/partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'BodyClass' => 'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');
?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">
              <form class="" action="" name="form_admin" method="post">

              <div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
									 <i class="zmdi zmdi-menu"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="?action=delete_all" class="delete_all">ลบทั้งหมด</a></li>
                  <li class="divider"></li>
									<li><a href="?action=approved_all" class="approved_all">อนุมัติทั้งหมด</a></li>
									<li><a href="?action=unapproved_all" class="unapproved_all">ไม่อนุมัติทั้งหมด</a></li>
								</ul>
							</div>

							<a href="<?php echo $url_href ?>" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">
								<i class="zmdi zmdi-account-add"></i> ADD</a>

										<table id="datatable-land" class="table table-striped table-bordered display responsive nowrap" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th class="not-mobile">รหัส <input type="checkbox"  onchange="checkAll(this)" name="row_id[]"></th>
													<th class="not-mobile">รูป</th>
													<th class="">ชื่อโครงการ</th>
													<th class="">รหัสทรัพย์สิน</th>
													<th class="not-mobile">วันที่ / แสดงผล</th>
													<th>จัดการ</th>
												</tr>

											</thead>

										</table>
                    </form>
						</div>
					</div>
				</div>
				<!-- End row -->

			</div>
			<!-- container -->

		</div>
		<!-- content -->


	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>


	<script>
	$(document).ready(function() {

			$('#datatable-land').DataTable( {
				"order": [], //Initial no order.
				"processing": true,
				"serverSide": true,
				// "pageLength": 50,
  			"pagingType": "full_numbers",
				"responsive": true,
				"lengthMenu": [[ 50, 100, 150, 200, -1], [ 50, 100, 150, 200, "All"]],
				"ajax": {
						"url": "<?php echo site_url('land/json_land') ?>",
						"type": "POST",
						// "dataSrc": "records"
				},
				// "columns": [
				// 		{ "data": "land_id" },
				// 		{ "data": "pic_thumb" },
				// 		{ "data": "land_title" },
				// 		{ "data": "land_property_id" },
				// 		{ "data": "createdate" },
				// 		{ "data": "approved" },
				// ],

			"columnDefs": [
				{
					"targets": 0,
	        "orderable": false
				},
				// {
				// 	"targets": 1,
				// 	"render": function ( data, type, row ) {
				// 		var path = "<?php echo base_url().'uploads/land/'; ?>";
				// 		return '<img src='+ path + row["pic_thumb"] + ' alt="" style="width: 100px;" class="img-thumbnail" />';
				// 	},
				// },
				// {
				// 	"targets": 2,
				// 	"render": function ( data, type, row ) {
				// 		var output = '';
				// 		output += row["land_title"] + '<br />';
				// 		output += 'จำนวนคนดู : ' + row["visited"];
				// 		return output;
				// 	},
				// },
				// {
				// 	"targets": 4,
				// 	"render": function ( data, type, row ) {
				// 		var output = '';
				// 		moment.locale('th');
				// 		// var createdate = moment(row["createdate"]).format('MMMM Do YYYY');
				// 		// var modifydate = moment(row["modifydate"]).format('MMMM Do YYYY');
				// 		var approved = row['approved'] == 2 ? 'อนุมัติ': 'ไม่อนุมัติ';
				// 		// output += 'วันที่สร้าง : ' + createdate + '<br />';
				// 		// output += 'วันที่แก้ไข : ' + modifydate + '<br />';
				// 		output += 'การแสดงผล : ' + approved;
				// 		return output;
				// 	},
				// },
				// {
				// 	"targets": 5,
				// 		"render": function ( data, type, row ) {
				// 			var id = row['land_id'];
				// 			var path_edit = "<?php echo site_url('land/edit/'); ?>" + id;
				// 			var path_images = "<?php echo site_url('land/images/'); ?>" + id;
				// 			var path_share = "<?php echo site_url('land/share/'); ?>" + id;
				// 			var output = '';
				// 			output += '<a href='+ path_edit +' class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5"><i class="fa fa-pencil"></i></a>';
				// 			//output += '<a href='+ path_images +' class="btn btn-icon waves-effect waves-light btn-purple m-b-5 m-r-5"><i class="zmdi zmdi-collection-image-o"></i></a>';
				// 			//output += '<a href="#" class="btn btn-approved btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id='+ id +'><i class="zmdi zmdi-lock-open"></i></a>';
				// 			//output += '<a href="#" class="btn btn-unapproved btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id='+ id +'><i class="zmdi zmdi-lock-outline"></i></a>';
				// 			// output += '<a href="#" id='+ id +' class="btn btn-delete btn-icon waves-effect waves-light btn-danger m-b-5 m-r-5"><i class="ti-trash"></i></a>';
				// 			// output += '<a href='+path_share+' class="btn btn-icon waves-effect waves-light btn-pink m-b-5 m-r-5"><i class="zmdi zmdi-share"></i></a>';
				// 			return output;
				// 	}
				// },
					]
			});
	});


		$(document).on('click', '.btn-delete', function(event){
			event.preventDefault();
			var id = $(this).attr("id");
		 	swal({
				title: "Warning Data!",
				text: "Confirm you delete",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
				 $.ajax({
					 url: "<?php echo base_url('land/delete_land/'); ?>"+id,
					 type: "POST",
					 data: id,
					 cache:false,
					 success: function (data)
					 {
						 //console.log(data);
						 window.location.reload();
					 },
					 error: function (xhr, desc, err)
					 {
						 console.log( err );
					 }
				 });
				 return false;
			}
			else
			{
			 swal.close();
			 return false;
			}

			return true;
		 });

		});



		$(document).on('click', '.btn-approved', function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you Approved",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, approved it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('land/approved_land/'); ?>",
						type: "POST",
						data: {
							action : 'approved',
							land_id : id,
							approved : '2',
						},
						cache:false,
						success: function (data)
						{
//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				return false;
			});
		});

		$(document).on('click', '.btn-unapproved', function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you unapproved",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Unapproved it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('land/approved_land/'); ?>",
						type: "POST",
						data: {
							action : 'unapproved',
							land_id : id,
							approved : '1',
						},
						cache:false,
						success: function (data)
						{
//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}

				return false;
			});
		});



		$(document).on('click', '.delete_all', function(event){
			event.preventDefault();
      var land_id = $( "form" ).serialize();
      // var land_id = $( "input[name='land_id[]']" ).val();
			swal({
				title: "Warning Data!",
				text: "Confirm you unapproved",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Unapproved it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
          // console.log(land_id);
					$.ajax({
						url: "<?php echo base_url('land/delete_multi_land/'); ?>",
						type: "POST",
						data: land_id,
						cache:false,
						success: function (data)
						{
							// console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}

				return false;
			});
		});


		$(document).on('click', '.approved_all', function(event){
			event.preventDefault();
      var land_id = $( "form" ).serialize();
      // var land_id = $( "input[name='land_id[]']" ).val();
			swal({
				title: "Warning Data!",
				text: "Confirm you approved",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Approved all it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
          // console.log(land_id);
					$.ajax({
						url: "<?php echo base_url('land/approved_multi_land/'); ?>",
						type: "POST",
						data: land_id,
						cache:false,
						success: function (data)
						{
							// console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}

				return false;
			});
		});
    
		$(document).on('click', '.unapproved_all', function(event){
			event.preventDefault();
      var land_id = $( "form" ).serialize();
      // var land_id = $( "input[name='land_id[]']" ).val();
			swal({
				title: "Warning Data!",
				text: "Confirm you approved",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Approved all it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
          // console.log(land_id);
					$.ajax({
						url: "<?php echo base_url('land/unapproved_multi_land/'); ?>",
						type: "POST",
						data: land_id,
						cache:false,
						success: function (data)
						{
							// console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}

				return false;
			});
		});

	</script>
