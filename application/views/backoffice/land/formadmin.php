<?php
defined('BASEPATH') OR exit();
$this->load->view('backoffice/partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'BodyClass' => 'fixed-left'));
$this->load->view('backoffice/partials/sidebar');
if (isset($data)) {
		$action = 'edit';
		$land_id = $data->land_id;
		$land_property_id = $data->land_property_id;
		$staff_id = $data->staff_id;
		$type_zone_id = $data->type_zone_id;
		$type_offering_id = $data->type_offering_id;
		$zone_id = $data->zone_id;
		$member_id = $data->member_id;
		$land_title = $data->land_title;
		$land_owner_name = $data->land_owner_name;
		$land_size = $data->land_size;
		$land_size_unit = $data->land_size_unit;
		$provinces_id = $data->provinces_id;
		$districts_id = $data->districts_id;
		$sub_districts_id = $data->sub_districts_id;
		$land_alley = $data->land_alley;
		$land_road = $data->land_road;
		$land_address = $data->land_address;
		$land_zipcode = $data->land_zipcode;
		$land_googlemaps = $data->land_googlemaps;
		$latitude = (!empty($data->latitude)) ? $data->latitude : '13.7563';
		$longitude = (!empty($data->longitude)) ? $data->longitude : '100.5018';
		$land_total_price = $data->land_total_price;
		$land_price_per_square_meter = $data->land_price_per_square_meter;
		$land_price_per_square_mortgage = $data->land_price_per_square_mortgage;
		$land_tag = $data->land_tag;
		$land_expert = trim($data->land_expert);
		$land_description = trim($data->land_description);
		$pic_thumb = $data->pic_thumb;
		$pic_large = $data->pic_large;
		$approved = $data->approved;
		$newsarrival = $data->newsarrival;
		$highlight = $data->highlight;
		$land_holder_name = $data->land_holder_name;
		$property_type_id = $data->property_type_id;
		$type_status_id = $data->type_status_id;
		$land_size_square_rai = $data->land_size_square_rai;
		$land_size_square_ngan = $data->land_size_square_ngan;
		$land_size_square_yard = $data->land_size_square_yard;
} else {
		$action = 'add';
		$land_id = "";
		$land_property_id = "";
		$staff_id = "";
		$type_zone_id = "";
		$type_offering_id = "";
		$zone_id = "";
		$member_id = "";
		$land_title = "";
		$land_owner_name = "";
		$land_size = "";
		$land_size_unit = "";
		$provinces_id = "";
		$districts_id = "";
		$sub_districts_id = "";
		$land_alley = "";
		$land_road = "";
		$land_zipcode = "";
		$land_googlemaps = "";
		$longitude = "";
		$latitude = "";
		$land_total_price = "";
		$land_price_per_square_meter = "";
		$land_price_per_square_mortgage = "";
		$land_tag = "";
		$land_expert = "";
		$land_description = "";
		$pic_thumb = "";
		$pic_large = "";
		$approved = "";
		$newsarrival = "";
		$highlight = "";
		$transportation_category_id = "";
		$land_holder_name = "";
		$publish = "";
		// $land_no="";
		//$land_order="";
		//$land_address="";
		$type_status_id = "";
		$property_type_id = "";
		$visited = "";
		$land_size_square_rai = "";
		$land_size_square_ngan = "";
		$land_size_square_yard = "";

} ?>
	<!--content-->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<form action="" id="frmdata" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
							<input type="hidden" name="land_id" value="<?php echo  $land_id; ?>">
							<input type="hidden" name="old_pic_thumb" value="<?php echo $pic_thumb; ?>">
							<input type="hidden" name="old_pic_large" value="<?php echo $pic_large; ?>">
							<div class="card-box">
								<h4 class="header-title m-t-0 m-b-30">รายการทรัพย์สิน</h4>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label class="col-md-2 control-label">รหัสประเภททรัพย์สิน</label>
											<div class="col-md-7">
												<input class="form-control" type="text" value="<?php echo $land_property_id; ?>" name="land_property_id">
											</div>
										</div>

										<div class="form-group">
											<label class="col-md-2 control-label">ประเภททรัพย์</label>
											<div class="col-md-7">
												<select id="property_type_id" class="form-control" name="property_type_id" onchange="(this)">
													<option value="0">----เลือก----</option>
													<?php
																foreach ($type_property as $row) {//START FOREACH
																	$sel = ($row->type_property_id == $property_type_id)? 'selected': '';
															?>
														<option value="<?php echo $row->type_property_id; ?>" <?php echo $sel; ?>>
															<?php echo $row->type_property_title; ?>
														</option>
														<?php
																}//END FOREACH
															?>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-md-2 control-label">พนักงานขาย</label>
											<div class="col-md-7">
												<select id="staff_id" class="form-control" name="staff_id" onchange="(this)">
													<option value="0">----เลือก----</option>
													<?php foreach ($staff_list as $row) {
																										$sel = ($row->staff_id == $staff_id) ? 'selected' : ''; ?>
														<option value="<?php echo $row->staff_id; ?>" <?php echo $sel; ?>>
															<?php echo $row->fullname; ?>
														</option>
														<?php } ?>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label">โซนของที่ดิน</label>
											<div class="col-md-7">
												<select id="type_zone_id" class="form-control" name="type_zone_id" onchange="(this)">
													<option value="0">----เลือก----</option>
													<?php foreach ($type_zone as $row) {
																										$sel = ($row->type_zone_id == $type_zone_id) ? 'selected' : ''; ?>
														<option value="<?php echo $row->type_zone_id; ?>" <?php echo $sel; ?>>
															<?php echo $row->type_zone_title; ?>
														</option>
														<?php } ?>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label">ประเภทการเสนอขาย</label>
											<div class="col-md-7">
												<select id="type_offering_id" class="form-control" name="type_offering_id" onchange="(this)">
													<option value="0">----เลือก----</option>
													<?php foreach ($type_offering as $row) {
																										$sel = ($row->type_offering_id == $type_offering_id) ? 'selected' : ''; ?>
														<option value="<?php echo $row->type_offering_id; ?>" <?php echo $sel; ?>>
															<?php echo $row->type_offering_title; ?>
														</option>
														<?php }//END FOREACH?>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label">โซน</label>
											<div class="col-md-7">
												<select id="zone_id" class="form-control" name="zone_id" onchange="(this)">
													<option value="0">----เลือก----</option>
													<?php foreach ($type_zone as $row) {
																										$sel = ($row->type_zone_id == $zone_id) ? 'selected' : ''; ?>
														<option value="<?php echo $row->type_zone_id; ?>" <?php echo $sel; ?>>
															<?php echo $row->type_zone_title; ?>
														</option>
														<?php } ?>
												</select>
											</div>
										</div>
									</div>
									<!-- end col -->
								</div>
								<!-- end row -->
							</div>
							<div class="card-box">
								<h4 class="header-title m-t-0 m-b-30">รายละเอียดที่ดิน</h4>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label class="col-md-2 control-label">ชื่อสมาชิก เจ้าของโครงการ</label>
											<div class="col-md-7">
												<select id="member_id" class="form-control" name="member_id" onchange="(this)">
													<option value="0">----เลือก----</option>
													<?php foreach ($member as $row) {
																										$sel = ($row->member_id == $member_id) ? 'selected' : ''; ?>
														<option value="<?php echo $row->member_id; ?>" <?php echo $sel; ?>>
															<?php echo $row->member_fname . ' ' . $row->member_lname; ?>
														</option>
														<?php } ?>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label">ชื่อที่ดิน</label>
											<div class="col-md-7">
												<input class="form-control" type="text" value="<?php echo $land_title; ?>" name="land_title">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label">เจ้าของที่ดิน</label>
											<div class="col-md-7">
												<input type="text" name="land_owner_name" class="form-control" value="<?php echo $land_owner_name; ?>" placeholder="">
											</div>
										</div>
										<!-- <div class="form-group">
											<label class="col-md-2 control-label">ขนาดที่ดิน</label>
											<div class="col-md-5">
												<input class="form-control" type="text" value="<?php echo $land_size; ?>" name="land_size">
											</div>
											<div class="col-md-2">
												<select class="form-control and_size_unit" name="land_size_unit">
													<?php
													$sel_sq_wah = ($land_size_unit == 'SQUARE WAH' || $land_size_unit == '') ? 'selected' : '';
													$sel_acre = ($land_size_unit == 'ACRE') ? 'selected' : '';
													?>
													<option value="SQUARE WAH" <?php echo $sel_sq_wah; ?>>ตารางวา</option>
													<option value="ACRE" <?php echo $sel_acre; ?>>ไร่</option>
												</select>
											</div>
										</div> -->
                    <div class="row">
    									<div class="col-lg-12">
    										<div class="form-group">
    											<label class="col-md-2 control-label">ที่ดิน (ไร่)</label>
    											<div class="col-md-2">
    												<input class="form-control" type="text" value="<?php echo $land_size_square_rai; ?>" name="land_size_square_rai">
    											</div>
    											<label class="col-md-1 control-label">ที่ดิน (งาน)</label>
    											<div class="col-md-3">
    												<input class="form-control" type="text" value="<?php echo $land_size_square_ngan; ?>" name="land_size_square_ngan">
    											</div>
    											<label class="col-md-1 control-label">ที่ดิน (วา)</label>
    											<div class="col-md-3">
    												<input type="text" name="land_size_square_yard" class="form-control" value="<?php echo $land_size_square_yard; ?>" placeholder="">
    											</div>
    										</div>
    									</div>
    									<!-- end col -->
    								</div>
    								<!-- end row -->
										<div class="form-group">
											<label class="col-md-2 control-label">สิทธิ์ถือครอง</label>
											<div class="col-md-7">
												<input type="text" name="land_holder_name" class="form-control" value="<?php echo $land_holder_name; ?>" placeholder="">
											</div>

										</div>
									</div>
								</div>
							</div>
							<!-- end box -->
							<div class="card-box">
								<h4 class="header-title m-t-0 m-b-30">รายละเอียดที่ตั้ง</h4>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label class="col-sm-2 control-label">จังหวัด</label>
											<div class="col-sm-2">
												<select id="provinces_id" class="form-control" name="provinces_id" onchange="(this)">
													<option value="0">----เลือก----</option>
													<?php foreach ($provinces as $row) {
																										$sel = ($row->provinces_id == $provinces_id) ? 'selected' : ''; ?>
														<option value="<?php echo $row->provinces_id; ?>" <?php echo $sel; ?>>
															<?php echo $row->provinces_name; ?>
														</option>
														<?php }//END FOREACH?>
												</select>
											</div>
											<label class="col-sm-1 control-label">อำเภอ/เขต</label>
											<div class="col-sm-3">
												<select id="districts_id" class="form-control" name="districts_id" onchange="(this)">
													<option value="0">----เลือก----</option>
												</select>
											</div>
											<label class="col-sm-1 control-label">ตำบล/แขวง</label>
											<div class="col-sm-3">
												<select id="sub_districts_id" class="form-control" name="sub_districts_id" onchange="(this)">
													<option value="0">----เลือก----</option>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label">ตรอก / ซอย</label>
											<div class="col-md-2">
												<input class="form-control" type="text" value="<?php echo $land_alley; ?>" name="land_alley">
											</div>
											<label class="col-md-1 control-label">โซนพังเมือง</label>
											<div class="col-md-3">
												<input class="form-control" type="text" value="<?php echo $land_road; ?>" name="land_road">
											</div>
											<label class="col-md-1 control-label">รหัสไปรษณีย์</label>
											<div class="col-md-3">
												<input type="text" name="land_zipcode" maxlength="5" class="form-control zipcode" value="<?php echo $land_zipcode; ?>" placeholder="">
											</div>
										</div>
									</div>
									<!-- end col -->
								</div>
								<!-- end row -->
							</div>
							<!-- end box -->
							<div class="card-box">
								<h4 class="header-title m-t-0 m-b-30">แผนที่</h4>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label class="col-md-2 control-label">Google Maps</label>
											<div class="col-md-10">
												<div id="map-canvas"></div>
											</div>
										</div>
										<div class="form-group m-to-30">
											<label class="col-md-2 control-label">Latitude</label>
											<div class="col-md-4">
												<input class="form-control" type="text" id="latitude" name="latitude" value="<?php echo $latitude; ?>">
											</div>
											<label class="col-md-1 control-label">Longitude</label>
											<div class="col-md-4">
												<input class="form-control" type="text" id="longitude" name="longitude" value="<?php echo $longitude; ?>">
											</div>
										</div>
									</div>
									<!-- end col -->
								</div>
								<!-- end row -->
							</div>
							<!-- end box -->
							<div class="card-box">
								<h4 class="header-title m-t-0 m-b-30">ราคาโครงการ</h4>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label class="control-label col-sm-2">ราคา</label>
											<div class="col-sm-7">
												<input type="text" name="land_total_price" class="float_price form-control" value="<?php echo $land_total_price; ?>">
												<span class="font-13 text-muted">กรอกทศนยิม 2 ตำแหน่ง : 999999999.99</span>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-2">ราคา/ตารางวา</label>
											<div class="col-sm-7">
												<input type="text" name="land_price_per_square_meter" class="float_price form-control" value="<?php echo $land_price_per_square_meter; ?>">
												<span class="font-13 text-muted">กรอกทศนยิม 2 ตำแหน่ง : 999999999.99</span>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-2">ราคาจำนอง/เดือน</label>
											<div class="col-sm-7">
												<input type="text" name="land_price_per_square_mortgage" class="float_price form-control" value="<?php echo $land_price_per_square_mortgage; ?>">
												<span class="font-13 text-muted">กรอกทศนยิม 2 ตำแหน่ง : 999999999.99</span>
											</div>
										</div>
									</div>
									<!-- end col -->
								</div>
								<!-- end row -->
							</div>
							<!-- end box -->
							<div class="card-box">
								<h4 class="header-title m-t-0 m-b-30">ข้อมูลรายละเอียด</h4>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label class="col-md-2 control-label">คำค้นหา</label>
											<div class="col-md-7">
												<input type="text" name="land_tag" maxlength="255" class="inputmaxlength form-control" value="<?php echo $land_tag; ?>" placeholder="">
												<span class="font-13 text-muted">ระบุคำที่ต้องการแล้วตามด้วย (,)</span>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label">รายละเอียดที่ดิน</label>
											<div class="col-md-7">
												<input class="form-control" type="text" value="<?php echo $land_expert; ?>" name="land_expert">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label">รายละเอียดทั้งหมด</label>
											<div class="col-md-10">
												<textarea class="wysiwigeditor" name="land_description" id="land_description">
													<?php echo $land_description; ?>
												</textarea>
											</div>
										</div>
									</div>
									<!-- end col -->
								</div>
								<!-- end row -->
							</div>

							<!-- end box -->
							<div class="card-box">
								<h4 class="header-title m-t-0 m-b-30">รูปภาพ / แกเลอรี่</h4>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label class="col-md-2 control-label" for="pic_thumb">รูปภาพขนาดเล็ก</label>
											<div class="col-md-4">
												<input type="file" name="pic_thumb"
													id="pic_thumb"
													class="dropify"
													data-max-file-size="1M"
													data-default-file="<?php echo (!empty($pic_thumb))? base_url().'uploads/land/'.$pic_thumb : '';?>" />
													<span class="help-block"><small>ขนาดรูปภาพไม่เกิน 2 MB และ <span class="label label-warning"> กว้าง : 656px X ยาว : 464px </span></small></span>
											</div>
										</div>

										<div class="form-group">
											<label class="col-md-2 control-label" for="pic_large">รูปภาพขนาดใหญ่</label>
											<div class="col-md-4">
												<input type="file" name="pic_large"
													id="pic_large"
													class="dropify"
													data-max-file-size="1M"
													data-default-file="<?php echo (!empty($pic_large))? base_url().'uploads/land/'.$pic_large : '';?>" />
													<span class="help-block"><small>ขนาดรูปภาพไม่เกิน 2 MB และ <span class="label label-warning"> กว้าง : 900px X ยาว : 500px </span></small></span>
											</div>
										</div>

										<?php
								if ( $action == 'add')
								{
									?>
										<div class="form-group">
											<label class="col-md-2 control-label" for="pic_gallery">รูปภาพแกเลอรี่</label>
											<div class="col-md-10">

												<div id="dropzone" class="dropzone">
													<div class="fallback">
														<input name="file[]" type="file" multiple />
														<span class="help-block"><small>ขนาดรูปภาพไม่เกิน 2 MB และ <span class="label label-warning"> กว้าง : 1200px X ยาว : 800px </span></small></span>
													</div>
												</div>
											</div>
										</div>

										<div class="form-group">
											<label class="col-md-2 control-label" for="pic_large">&nbsp;</label>
											<div class="col-md-10">
												<div id="preview-gallery" style=""></div>
												<div id="file_name"></div>
											</div>
										</div>
									<?php
								}
									?>
									</div>
									<!-- end col -->
								</div>
								<!-- end row -->
							</div>
							<!-- end box -->

							<div class="card-box">
								<h4 class="header-title m-t-0 m-b-30">สถานที่ใกล้เคียง</h4>
								<span class="font12">จำกัด 10 สถานที่ใกล้เคียง</span>
								<div class="row">
									<div class="col-lg-12">
									<?php
										if( !empty($land_relation_transportation) )
										{
										?>
											<?php
											//$this->primaryclass->pre_var_dump($land_relation_transportation);
											#1. หาที่มีใน DB ก่อน
//														foreach ($land_relation_transportation as $row_relation)
//														{
//															echo $transportation_category_id = $row_relation->transportation_category_id;
//															echo '<br>';
//															echo $transportation_id = $row_relation->transportation_id;
//															echo '<br>';
//															echo $transportation_id = $row_relation->transportation_id;
//															echo '<br>';
//															echo $transportation_distance = $row_relation->transportation_distance;
//															echo '<br>';
//														}


														foreach ($land_relation_transportation as $row_relation)
														{
															$land_relation_transportation_id = $row_relation->land_relation_transportation_id;
															$transportation_category_id = $row_relation->transportation_category_id;
															$transportation_id = $row_relation->transportation_id;
															$transportation_distance = $row_relation->transportation_distance;

												?>
										<div class="form-group transportation<?php echo $land_relation_transportation_id; ?>">
											<label class="col-md-2 control-label m-t-10">สถานที่ใกล้เคียง</label>
											<div class="col-sm-12 col-md-4">
												<select id="transportation_category_id" class="form-control" name="transportation_category_id[]" onchange="(this)">
													<option value="0">----เลือก----</option>
													<?php
													foreach ($transportation_category as $row_tc)
													{//START FOREACH GROUP


														if ($transportation_category != $row_tc->transportation_category_id)
														{
															if ($transportation_category != '')
															{
																echo '</optgroup>';
															}
															echo '<optgroup label="'.ucfirst($row_tc->transportation_category_title).'">';
														}
														//$transportation = $this->Condominium_model->get_transportation_row_by_land($land_id, $row->transportation_category_id);
														$transportation = $this->Condominium_model->get_transportation_option($row_tc->transportation_category_id);
														foreach ($transportation as $row_t)
														{//START FOREACH
															$sel = ($row_t->transportation_id == $transportation_id)? 'selected': '';
															echo '<option value="'.$row_t->transportation_id.'" '.$sel.' >'.$row_t->transportation_title.'</option>';
														}//END FOREACH
													?>
													<?php
													}//END FOREACH GROUP
													if ($transportation_category != '') {
														echo '</optgroup>';
													}
													?>
												</select>
											</div>
											<div class="col-sm-10 col-md-4">
												<input type="text" name="transportation_distance[]" class="inputmaxlength form-control" value="<?php echo $transportation_distance; ?>" maxlength="50" placeholder="">
												<span class="font-13 text-muted">ตัวอย่าง : 7 นาที &#40;1.46 กิโลเมตร&#41;</span>
											</div>
											<div class="col-sm-2 col-md-2">
												<a href="javascript:void(0);" class="delete_transportation" id="<?php echo $land_relation_transportation_id; ?>" title="delete item"><i class="zmdi zmdi-delete"></i></a>
											</div>
										</div>
													<?php
														}//END FOREACH
											?>
													<?php
										}//END IF
											?>

										<div class="form-clone">
											<div class="form-group">
												<label class="col-md-2 control-label m-t-10">สถานที่ใกล้เคียง</label>
												<?php
												if ( $action == 'add')
												{
													$transportation_distance = '';
												?>

												<div class="col-sm-12 col-md-4">
													<select id="transportation_category_id" class="form-control" name="transportation_category_id[]" onchange="(this)">
														<option value="0">----เลือก----</option>
														<?php
														foreach ($transportation_category as $row)
														{//START FOREACH GROUP
															$sel = ($row->transportation_category_id == $transportation_category_id)? 'selected': '';

															if ($transportation_category != $row->transportation_category_id)
															{
																if ($transportation_category != '')
																{
																	echo '</optgroup>';
																}
																echo '<optgroup label="'.ucfirst($row->transportation_category_title).'">';
															}
															$transportation = $this->Condominium_model->get_transportation_option($row->transportation_category_id);
															//$transportation = $this->Condominium_model->get_transportation_row_by_land($land_id, $row->transportation_category_id);
															foreach ($transportation as $row_in)
															{//START FOREACH
																echo '<option value="'.$row_in->transportation_id.'">'.$row_in->transportation_title.'</option>';
															}//END FOREACH
														?>

															<?php
														}//END FOREACH GROUP
														if ($transportation_category != '') {
															echo '</optgroup>';
														}
														?>
													</select>
												</div>
												<div class="col-sm-10 col-md-4">
													<input type="text" name="transportation_distance[]" class="inputmaxlength form-control" value="<?php echo $transportation_distance; ?>" maxlength="50" placeholder="">
													<span class="font-13 text-muted">ตัวอย่าง : 7 นาที &#40;1.46 กิโลเมตร&#41;</span>
												</div>
												<?php
												}
												else
												{
												?>




												<div class="col-sm-12 col-md-4">
													<select id="transportation_category_id" class="form-control" name="transportation_category_id[]" onchange="(this)">
														<option value="0">----เลือก----</option>
														<?php
														foreach ($transportation_category as $row)
														{//START FOREACH GROUP
															$transportation_category_id = 0;
															$sel = ($row->transportation_category_id == $transportation_category_id)? 'selected': '';

															if ($transportation_category != $row->transportation_category_id)
															{
																if ($transportation_category != '')
																{
																	echo '</optgroup>';
																}
																echo '<optgroup label="'.ucfirst($row->transportation_category_title).'">';
															}
															//$transportation = $this->Condominium_model->get_transportation_row_by_land($land_id, $row->transportation_category_id);
															$transportation = $this->Condominium_model->get_transportation_option($row->transportation_category_id);
															foreach ($transportation as $row_in)
															{//START FOREACH
																echo '<option value="'.$row_in->transportation_id.'">'.$row_in->transportation_title.'</option>';
															}//END FOREACH
														?>

															<?php
														}//END FOREACH GROUP
														if ($transportation_category != '') {
															echo '</optgroup>';
														}
														?>
													</select>
												</div>
												<div class="col-sm-10 col-md-4">
													<input type="text" name="transportation_distance[]" class="inputmaxlength form-control" value="<?php //echo $row_in->transportation_distance; ?>" maxlength="50" placeholder="">
													<span class="font-13 text-muted">ตัวอย่าง : 7 นาที &#40;1.46 กิโลเมตร&#41;</span>
												</div>
												<?php
												}//END IF ACTION
												?>
												<div class="col-sm-2 col-md-2">
													<a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa fa-plus"></i></a>
												</div>
											</div>
										</div>
										<!-- end clone -->
									</div>
									<!-- end col -->
								</div>
								<!-- end row -->
							</div>
							<!-- end box -->

							<div class="card-box">
								<h4 class="header-title m-t-0 m-b-30">ส่วนการแสดงผล</h4>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label class="col-sm-2 control-label"> อนุมัติ </label>
											<div class="col-sm-10">
												<div class="radio radio-pink">
													<input id="radio1" value="2" name="approved" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" <?php echo ($approved==2 ) ? 'checked' : ''; ?>>
													<label for="radio1"> อนุมัติ </label>
												</div>
												<div class="radio radio-pink">
													<input id="radio2" value="1" name="approved" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" required="" <?php echo ($approved==1 || $approved=='' ) ? 'checked' : ''; ?>>
													<label for="radio2"> ไม่อนุมัติ </label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label"> ใหม่ล่าสุด </label>
											<div class="col-sm-10">
												<div class="radio radio-pink">
													<input id="newsarrival1" value="2" name="newsarrival" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" <?php echo ($newsarrival==2 ) ? 'checked' : ''; ?>>
													<label for="newsarrival1"> ใหม่ล่าสุด </label>
												</div>
												<div class="radio radio-pink">
													<input id="newsarrival2" value="1" name="newsarrival" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" required="" <?php echo ($newsarrival==1 || $newsarrival=='' ) ? 'checked' : ''; ?>>
													<label for="newsarrival2"> ไม่ใหม่ล่าสุด </label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">แนะนำ</label>
											<div class="col-sm-10">
												<div class="radio radio-pink">
													<input id="highlight1" value="2" name="highlight" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" <?php echo ($highlight==2 ) ? 'checked' : ''; ?>>
													<label for="highlight1"> แนะนำ </label>
												</div>
												<div class="radio radio-pink">
													<input id="highlight2" value="1" name="highlight" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" required="" <?php echo ($highlight==1 || $highlight=='' ) ? 'checked' : ''; ?>>
													<label for="highlight2"> ไม่แนะนำ </label>
												</div>
											</div>
										</div>
									</div>
									<!-- end col -->
								</div>
								<!-- end row -->
							</div>
							<!-- end box -->
							<div class="card-box">
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10 m-t-15">
												<button id="submit" type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light" onclick="tinyMCE.triggerSave(true, true);">บันทึก
												</button>
												<a href="<?php echo site_url('land/view'); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">ยกเลิก</a>
												<input type="hidden" name="action" value="<?php echo $action ?>">
											</div>
										</div>
									</div>
									<!-- end col -->
								</div>
								<!-- end row -->
							</div>
							<!-- end box -->
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--content-->
		<?php if (!empty($googlemap)) { ?>
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtK77y4LHtYZ5ZoVeVVwpkMUoy-Eq9JRA&sensor=false"></script>
			<script>
				// In the following example, markers appear when the user clicks on the map.
				// Each marker is labeled with a single alphabetical character.
				var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
				var labelIndex = 0;
				var map;

				function initialize() {
					<?php if ($action == 'edit') {  ?>
					var bangkok = {
						lat: <?php  echo $latitude; ?>,
						lng: <?php  echo $longitude; ?>
					};
					<?php  } else {  ?>
					var bangkok = {
						lat: 13.7563,
						lng: 100.5018
					};
					<?php  } ?>
					map = new google.maps.Map(document.getElementById('map-canvas'), {
						zoom: 11,
						center: bangkok
					});
					var marker = new google.maps.Marker({
						draggable: true,
						position: bangkok,
						map: map,
						title: "Your location"
					});

					google.maps.event.addListener(marker, 'dragend', function (event) {
						$('#latitude').val(event.latLng.lat());
						$('#longitude').val(event.latLng.lng());
						//console.log( 'latitude : ' + event.latLng.lat()  );
						//console.log( 'longtitude : ' + event.latLng.lng()  );
					});
				}
				// Adds a marker to the map.
				var i = 0;

				function addMarker(location, map) {
					// Add the marker at the clicked location, and add the next-available label
					// from the array of alphabetical characters.
					var iMax = 1;
					if (i < iMax) {
						var marker = new google.maps.Marker({
							draggable: true,
							position: location,
							label: labels[labelIndex++ % labels.length],
							map: map,
							title: "Your location"
						});
						i++;
					} else {
						console.log('you can only post', i - 1, 'markers');
					}
					//map.panTo(location);
				}
				google.maps.event.addDomListener(window, 'load', initialize);
			</script>
			<?php }
		$this->load->view('backoffice/partials/footer'); ?>


				<script>
					$(document).ready(function () {
						$("form#frmdata").submit(function (event) {
							var formData = new FormData($(this)[0]);
							var land_id = $('#land_id').val();
							<?php
								$ajax_url = ($action === 'add') ? "land/add" : "land/edit";
								$msg = ($action === 'add') ? "Insert" : "Update"; ?>
							var url = "<?php echo base_url() . $ajax_url; ?>";
							var formData = new FormData($(this)[0]);
							$.ajax({
								url: url,
								type: "POST",
								data: formData,
								cache: false,
								processData: false,
								contentType: false,
								context: this,
								success: function (data, status) {
									// console.log(data);
									swal({
										title: "Success Data!",
										text: "Confirm you save",
										type: "warning",
										showCancelButton: true,
										confirmButtonColor: "#DD6B55",
										confirmButtonText: "Yes, <?php echo $msg; ?> it!",
										cancelButtonText: "No, cancel plx!",
										closeOnConfirm: true,
										closeOnCancel: true
									}, function (isConfirm) {
										if (isConfirm) {
											window.location.replace('<?php echo base_url(); ?>land/view');
										} else {
											window.location.reload();
										}
									});
								},
								error: function (xhr, desc, err) {
									console.log(err);
								},
							});
							event.preventDefault();
							return false;
						});
						var fileList = new Array;
						var i = 0;
						var file_up_names = [];
						$("div#dropzone").dropzone({
							url: "<?php echo site_url('land/add_upload'); ?>",
							addRemoveLinks: true,
							//previewsContainer: "#preview-gallery",
							dictRemoveFile: 'REMOVE',
							success: function (file, responseText) {
								$.ajax({
									url: "<?php echo site_url('land/add_upload'); ?>",
									type: "POST",
									data: {
										'file_name': file.name
									},
									success: function (data) {
										//console.log( $(this).length );
										var count_img = $(this).length;
										$("<input type='hidden' name='file_name_[]'>").val(file.name).appendTo("div#file_name");
										//console.log( count_img );
									}
								});

							},
							removedfile: function (file) {
								x = confirm('Do you want to delete?');
								if (!x)
									return false;
								$.ajax({
									url: "<?php echo site_url('land/canceled_upload'); ?>",
									type: "POST",
									data: {
										'file_name': file.name
									},
									success: function (data) {
										$("input[value='" + file.name + "']").remove();
										//remove preview//
										$(document).find(file.previewElement).remove();
									}
								});
							}
						});
						//	Dropzone.options.myDropzone = false;
						Dropzone.autoDiscover = false;
						var unique = "file[]";
						var acceptedFileTypes = "image/*"; //dropzone requires this param be a comma separated list
						Dropzone.options.myAwesomeDropzone = {
							// your other settings as listed above
							maxFiles: 10, // allowing any more than this will stress a basic php/mysql stack
							uploadMultiple: true,
							paramName: unique,
							headers: {
								"MyAppname-Service-Type": "Dropzone"
							},
							acceptedFiles: acceptedFileTypes,
						}
					});
					$(document).ready(function () {
						// jQuery
						//	event.preventDefault();
						var provinces_id = $("#provinces_id").val();
						//var districts_id = $("#districts_id").val();
						if (provinces_id != 0) {
							$.ajax({
								url: "<?php echo base_url(); ?>page/provinces_districts_relation",
								data: {
									id: provinces_id
								},
								type: "POST",
								success: function (data) {
									$("#districts_id").html(data);
									$("#districts_id").val(<?php echo $districts_id; ?>);
									var districts_id = $("#districts_id").val();
									$.ajax({
										url: "<?php echo base_url(); ?>page/districts_subdistricts_relation",
										data: {
											id: districts_id
										},
										type: "POST",
										success: function (data) {
											$("#sub_districts_id").html(data);
											$("#sub_districts_id").val(<?php echo $sub_districts_id; ?>);
										}
									});
								}
							});
						}
					});
					$("#provinces_id").change(function () {
						/*dropdown post */ //
						$.ajax({
							url: "<?php echo base_url(); ?>page/provinces_districts_relation",
							data: {
								id: $(this).val()
							},
							type: "POST",
							success: function (data) {
								$("#districts_id").html(data);
							}
						});
					});
					$("#districts_id").change(function () {
						/*dropdown post */ //
						$.ajax({
							url: "<?php echo base_url(); ?>page/districts_subdistricts_relation",
							data: {
								id: $(this).val()
							},
							type: "POST",
							success: function (data) {
								$("#sub_districts_id").html(data);
							}
						});
					});
					$("#transportation_category_id").change(function () {
						/*dropdown post */ //
						$.ajax({
							url: "<?php echo base_url(); ?>land/transportation_category_relation",
							data: {
								id: $(this).val()
							},
							type: "POST",
							success: function (data) {
								$("#transportation_id").html(data);
							}
						});
					});
				</script>

				<script type="text/javascript">
				$(document).ready(function () {
					var maxField = 10; //Input fields increment limitation
					var addButton = $('.add_button'); //Add button selector
					var wrapper = $('.form-clone'); //Input field wrapper
					//var fieldHTML = '<div><input type="text" name="field_name[]" value=""/><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="remove-icon.png"/></a></div>'; //New input field html
					var fieldHTML = ''; //New input field html


					$.ajax({
						url	: "<?php echo base_url(); ?>land/transportation_category_option",
						data: {
							id: $(this).val()
						},
						type: "POST",
						success: function (data) {
							fieldHTML = data;
							//console.log(fieldHTML);
							var x = 1; //Initial field counter is 1
							$(addButton).click(function () { //Once add button is clicked
								if (x < maxField) { //Check maximum number of input fields
									x++; //Increment field counter
									$(wrapper).append(fieldHTML); // Add field html
								}
							});
							$(wrapper).on('click', '.remove_button', function (e) { //Once remove button is clicked
								//alert('sssss');
								e.preventDefault();
								$(this).closest('.form-group').remove(); //Remove field html
								x--; //Decrement field counter
							});
						}
					});
					//var fieldHTML = '<div><input type="text" name="field_name[]" value=""/><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="remove-icon.png"/></a></div>'; //New input field html




				});
			</script>


<script type="text/javascript">

	var land_id = $("#land_id").val();
	$('.dropify').dropify({
		messages: {
				'default': 'Drag and drop a file here or click',
				'replace': 'Drag and drop or click to replace',
				'remove': 'Remove',
				'error': 'Ooops, something wrong appended.'
		},
		error: {
				'fileSize': 'The file size is too big (1M max).'
		}
	});

	var drEvent = $('.dropify').dropify();
	drEvent.on('dropify.beforeClear', function(event, element){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('land/remove_image_update'); ?>",
			data: {
				"land_id": land_id,
				"imagefile": element.file.name,
				"field_name": $(this).attr("name"),
			},
			success: function(response) {
				console.log('ok success!');
			}
		}); //END AJX

		var r = confirm("Do you really want to delete \"" + element.file.name + "\" ?");
		if (r == true) {
			$( "#old_" + $('.dropify').attr('id') ).val( "" );
		} else {
			console.log('Cancel');
		}
		//return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
	});

	var drEvent = $('.dropify').dropify();
	drEvent.on('dropify.afterClear', function(event, element){
		console.log('File delete success!');
	});
</script>
