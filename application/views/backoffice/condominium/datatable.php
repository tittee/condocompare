<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');
?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">
              <form class="" action="" name="form_admin" method="post">

              <div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
									 <i class="zmdi zmdi-menu"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="?action=delete_all" class="delete_all">ลบทั้งหมด</a></li>
                  <li class="divider"></li>
									<li><a href="?action=approved_all" class="approved_all">อนุมัติทั้งหมด</a></li>
									<li><a href="?action=unapproved_all" class="unapproved_all">ไม่อนุมัติทั้งหมด</a></li>
								</ul>
							</div>

							<a href="<?php echo $url_href ?>" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">
								<i class="zmdi zmdi-account-add"></i> ADD</a>

										<table id="datatable-responsive" class="table table-striped table-bordered display responsive nowrap" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th class="not-mobile">ลำดับ <input type="checkbox"  onchange="checkAll(this)" name="row_id[]"></th>
													<th class="not-mobile">รูป</th>
													<th class="">ชื่อโครงการ</th>
													<th class="">รหัสทรัพย์สิน</th>
													<th class="not-mobile">วันที่ / แสดงผล</th>
													<th>จัดการ</th>
												</tr>
											</thead>
											<tbody>
												<?php
	$i = 1;
									foreach($data as $row => $value)
									{//START FOREACH#1
										$id = $value->condo_id;

										$sfilepath = base_url().'uploads/condominium';
										$pic_thumb = $sfilepath.'/'.$value->pic_thumb;

										$staff_fullname = ( !empty($value->fullname) )? $value->fullname : 'ยังไม่ทำการระบุ';
										$condo_property_id = ( !empty($value->condo_property_id) )? $value->condo_property_id : 'ยังไม่ทำการระบุ';

										$createdate = $value->createdate;
										$modifydate = $value->modifydate;
										$createdate_t = ($createdate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
										$modifydate_t = ($modifydate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($modifydate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
										$approved = $value->approved;
									?>
													<tr>
														<th>
                              <input type="checkbox" name="condo_id[]" class="condo_id" value="<?php echo $id; ?>">
															<?php echo $i; ?>
														</th>
														<td><img src="<?php echo $pic_thumb;?>" style="width: 150px;" class="img-thumbnail" alt=""></td>
														<td>
															<?php echo $value->condo_title; ?><br>
															รหัส : <?php echo $id; ?><br>
															จำนวนคนดู : <?php echo $value->visited; ?>
														</td>
														<td>
															<?php echo $condo_property_id; ?>
														</td>
														<td>
															วันที่สร้าง : <?php echo $createdate_t; ?><br>
															วันที่แก้ไข : <?php echo $modifydate_t; ?><br>
															การแสดงผล : <?php echo $this->primaryclass->get_approved($approved); ?>
														</td>
														<td>
															<!-- <input type="hidden" name="id" value="<?php echo $id; ?>"> -->
															<a href="<?php echo site_url('condominium/edit/'.$id); ?>" class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5"><i class="fa fa-pencil"></i></a>
															<a href="<?php echo site_url('condominium/images/'.$id); ?>" class="btn btn-icon waves-effect waves-light btn-purple m-b-5 m-r-5"><i class="zmdi zmdi-collection-image-o"></i></a>


															<?php if( $approved == 1){ //ไม่แสดงผล ต้องเปลี่ยนเป็นแสดงผล ?>
															<a href="#" class="btn btn-approved btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="<?php echo $id; ?>"><i class="zmdi zmdi-lock-open"></i></a>
															<?php } else {//แสดงผล ต้องเปลี่ยนเป็นไม่แสดงผล ?>
															<a href="#" class="btn btn-unapproved btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="<?php echo $id; ?>"><i class="zmdi zmdi-lock-outline"></i></a>
															<?php }//END IF APPROVED ?>

															<a href="#" id="<?php echo $id; ?>" class="btn btn-delete btn-icon waves-effect waves-light btn-danger m-b-5 m-r-5"><i class="ti-trash"></i></a>

															<a href="<?php echo site_url('condominium/share/'.$id); ?>" class="btn btn-icon waves-effect waves-light btn-pink m-b-5 m-r-5"><i class="zmdi zmdi-share"></i></a>
														</td>
													</tr>
													<?php $i++;}//END FOREACH#1 ?>
											</tbody>
										</table>
                    </form>
						</div>
					</div>
				</div>
				<!-- End row -->

			</div>
			<!-- container -->

		</div>
		<!-- content -->


	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>


	<script>
		$(".btn-delete").click(function(event){
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you delete",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('condominium/delete_condo/'); ?>"+id,
						type: "POST",
						data: id,
						cache:false,
						success: function (data)
						{
							//console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				event.preventDefault();
				return false;
			});

		});

		$(".btn-approved").click(function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you Approved",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, approved it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('condominium/approved_condo/'); ?>",
						type: "POST",
						data: {
							action : 'approved',
							condo_id : id,
							approved : '2',
						},
						cache:false,
						success: function (data)
						{
//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				return false;
			});
		});

		$(".btn-unapproved").click(function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you unapproved",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Unapproved it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('condominium/approved_condo/'); ?>",
						type: "POST",
						data: {
							action : 'unapproved',
							condo_id : id,
							approved : '1',
						},
						cache:false,
						success: function (data)
						{
//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}

				return false;
			});
		});

    $(".delete_all").click(function(event){
			event.preventDefault();
      var condo_id = $( "form" ).serialize();
      // var condo_id = $( "input[name='condo_id[]']" ).val();
			swal({
				title: "Warning Data!",
				text: "Confirm you unapproved",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Unapproved it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
          // console.log(condo_id);
					$.ajax({
						url: "<?php echo base_url('condominium/delete_multi_condo/'); ?>",
						type: "POST",
						data: condo_id,
						cache:false,
						success: function (data)
						{
							// console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}

				return false;
			});
		});

    $(".delete_all").click(function(event){
			event.preventDefault();
      var condo_id = $( "form" ).serialize();
      // var condo_id = $( "input[name='condo_id[]']" ).val();
			swal({
				title: "Warning Data!",
				text: "Confirm you delete",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete All it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
          // console.log(condo_id);
					$.ajax({
						url: "<?php echo base_url('condominium/delete_multi_condo/'); ?>",
						type: "POST",
						data: condo_id,
						cache:false,
						success: function (data)
						{
							// console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}

				return false;
			});
		});

    $(".approved_all").click(function(event){
			event.preventDefault();
      var condo_id = $( "form" ).serialize();
      // var condo_id = $( "input[name='condo_id[]']" ).val();
			swal({
				title: "Warning Data!",
				text: "Confirm you approved",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Approved all it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
          // console.log(condo_id);
					$.ajax({
						url: "<?php echo base_url('condominium/approved_multi_condo/'); ?>",
						type: "POST",
						data: condo_id,
						cache:false,
						success: function (data)
						{
							// console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}

				return false;
			});
		});

    $(".unapproved_all").click(function(event){
			event.preventDefault();
      var condo_id = $( "form" ).serialize();
      // var condo_id = $( "input[name='condo_id[]']" ).val();
			swal({
				title: "Warning Data!",
				text: "Confirm you approved",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Approved all it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
          // console.log(condo_id);
					$.ajax({
						url: "<?php echo base_url('condominium/unapproved_multi_condo/'); ?>",
						type: "POST",
						data: condo_id,
						cache:false,
						success: function (data)
						{
							// console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}

				return false;
			});
		});

	</script>
