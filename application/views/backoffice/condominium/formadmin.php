<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

//var_dump($data['row']);
if(isset( $data['row'] ))
{
	$action 									= 'edit';
	$condo_id 								= $data['row']->condo_id;
	$condo_property_id 				= $data['row']->condo_property_id;
	$member_id 								= $data['row']->member_id;
	$staff_id 								= $data['row']->staff_id;
	$condo_title 							= $data['row']->condo_title;
	$condo_owner_name 				= $data['row']->condo_owner_name;
	$condo_holder_name	 			= $data['row']->condo_holder_name;
	$condo_builing_finish	 		= $data['row']->condo_builing_finish;
	$type_zone_id	 						= $data['row']->type_zone_id;
	$zone_id	 								= $data['row']->zone_id;
	$type_offering_id					= $data['row']->type_offering_id;
	$type_suites_id 					= $data['row']->type_suites_id;
	$type_decoration_id 			= $data['row']->type_decoration_id;
	$condo_all_room 					= $data['row']->condo_all_room;
	$condo_all_toilet 				= $data['row']->condo_all_toilet;
	$condo_interior_room 			= $data['row']->condo_interior_room;
	$condo_direction_room 		= $data['row']->condo_direction_room;
	$condo_scenery_room	 			= $data['row']->condo_scenery_room;
	$condo_living_area 				= $data['row']->condo_living_area;
	$condo_electricity 				= $data['row']->condo_electricity;
	$condo_expert 						= trim($data['row']->condo_expert);
	$condo_description 				= trim($data['row']->condo_description);
  // $condo_description        = $this->primaryclass->decode_htmlspecialchars($condo_description);
	$pic_thumb 								= $data['row']->pic_thumb;
	$pic_large 								= $data['row']->pic_large;
	$condo_no 								= $data['row']->condo_no;
	$condo_floor 							= $data['row']->condo_floor;
	$condo_area_apartment			= $data['row']->condo_area_apartment;
	$condo_alley 							= $data['row']->condo_alley;
	$condo_road 							= $data['row']->condo_road;
	$condo_address 						= $data['row']->condo_address;
	$provinces_id 						= $data['row']->provinces_id;
	$districts_id 						= $data['row']->districts_id;
	$sub_districts_id 				= $data['row']->sub_districts_id;
	$condo_zipcode 						= $data['row']->condo_zipcode;
	$condo_total_price 				= $data['row']->condo_total_price;
	$condo_price_per_square_meter		= $data['row']->condo_price_per_square_meter;
	$condo_googlemaps 				= $data['row']->condo_googlemaps;
	$latitude 								= (!empty($data['row']->latitude))? $data['row']->latitude : '13.7563';
	$longitude 								= (!empty($data['row']->longitude))? $data['row']->longitude : '100.5018';
	$condo_tag 								= $data['row']->condo_tag;
	//CreateDate
	$createdate								= (!empty($data['row']->createdate))? $data['row']->createdate : date("Y-m-d H:i:s");
	$createdate_t = explode(" ", $createdate);
	$createdate_date = $createdate_t[0];
	$createdate_time = $createdate_t[1];

	//Modify Date
	$modifydate								= (!empty($data['row']->modifydate))? $data['row']->modifydate : date("Y-m-d H:i:s");
	$modifydate_t = explode(" ", $modifydate);
	$modifydate_date = $modifydate_t[0];
	$modifydate_time = $modifydate_t[1];


	$type_status_id 					= $data['row']->type_status_id;
	$newsarrival 							= $data['row']->newsarrival;
	$highlight 								= $data['row']->highlight;
	$approved 								= $data['row']->approved;

//	$facilities_id 						= $data['row']->facilities_id;
//	$featured_id 							= $data['row']->featured_id;
//	$transportation_category_id = $data['row']->transportation_category_id;
//	$transportation_id 				= $data['row']->transportation_id;
//	$transportation_distance 	= $data['row']->transportation_distance;
}
else
{
	$action 									= 'add';
	$condo_property_id 				= "";
	$member_id 								= "";
	$staff_id 								= "";
	$condo_title 							= "";
	$condo_owner_name 				= "";
	$condo_holder_name	 			= "";
	$condo_builing_finish	 		= "";
	$type_zone_id	 						= "";
	$zone_id	 								= "";
	$type_offering_id 				= "";
	$type_suites_id 					= "";
	$type_decoration_id 			= "";
	$condo_all_room 					= "";
	$condo_all_toilet 				= "";
	$condo_interior_room 			= "";
	$condo_direction_room 		= "";
	$condo_scenery_room	 			= "";
	$condo_living_area 				= "";
	$condo_electricity 				= "";
	$condo_expert 						= "";
	$condo_description 				= "";
	$pic_thumb 								= "";
	$pic_large 								= "";
	$condo_no 								= "";
	$condo_floor 							= "";
	$condo_area_apartment			= "";
	$condo_alley 							= "";
	$condo_road 							= "";
	$condo_address 						= "";
	$provinces_id 						= "";
	$districts_id 						= "";
	$sub_districts_id 				= "";
	$condo_zipcode 						= "";
	$condo_total_price 				= "";
	$condo_price_per_square_meter		= "";
	$condo_transportation 		= "";
	$condo_googlemaps					= "";
	$latitude 								= "";
	$longitude 								= "";
	$condo_tag 								= "";
	//CreateDate
	$createdate								=  date("Y-m-d H:i:s");
	$createdate_t = explode(" ", $createdate);
	$createdate_date = $createdate_t[0];
	$createdate_time = $createdate_t[1];


	//Modify Date
	$modifydate								= date("Y-m-d H:i:s");
	$modifydate_t = explode(" ", $modifydate);
	$modifydate_date = $modifydate_t[0];
	$modifydate_time = $modifydate_t[1];


	$type_status_id 					= "";
	$newsarrival 							= "";
	$highlight 								= "";
//	$publish 									= "1";
	$approved 								= "1";

//	$facilities_id 						= "";
//	$featured_id 							= "";
//	$transportation_category_id = "";
//	$transportation_id 				= "";
//	$transportation_distance 	= "";
}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->

	<link href="<?php echo base_url()?>assets/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url()?>assets/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url()?>assets/css/dropzone.min.css" rel="stylesheet" type="text/css">

	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">

						<?php if(isset( $data['row'] ))
									{
								?>
							<form action="" id="frmdata" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
								<input type="hidden" name="condo_id" id="condo_id" value="<?php echo $condo_id; ?>">
								<input type="hidden" id="old_pic_thumb" name="old_pic_thumb" value="<?php echo $pic_thumb; ?>">
								<input type="hidden" id="old_pic_large" name="old_pic_large" value="<?php echo $pic_large; ?>">
								<?php }
									else
									{
								?>
							<form action="" id="frmdata" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
										<?php } ?>


								<div class="card-box">
									<h4 class="header-title m-t-0 m-b-30">รายการทรัพย์สิน</h4>
									<div class="row">
										<div class="col-lg-12">

											<div class="form-group">
												<label class="col-md-2 control-label">รหัสประเภททรัพย์สิน</label>
												<div class="col-md-10">
													<input class="form-control" type="text" value="<?php echo $condo_property_id; ?>" name="condo_property_id">
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-2 control-label">พนักงานขาย</label>
												<div class="col-md-10">
													<select id="staff_id" class="form-control" name="staff_id" onchange="(this)">
														<option value="0">----เลือก----</option>
														<?php
																foreach ($staff_list as $row) {//START FOREACH
																	$sel = ($row->staff_id == $staff_id)? 'selected': '';
															?>
															<option value="<?php echo $row->staff_id; ?>" <?php echo $sel; ?>>
																<?php echo $row->fullname; ?>
															</option>
															<?php
																}//END FOREACH
															?>
													</select>
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-2 control-label">โซนของทรัพย์</label>
												<div class="col-md-10">
													<select id="type_zone_id" class="form-control" name="type_zone_id" onchange="(this)">
														<option value="0">----เลือก----</option>
														<?php
																foreach ($type_zone as $row) {//START FOREACH
																	$sel = ($row->type_zone_id == $type_zone_id)? 'selected': '';
															?>
															<option value="<?php echo $row->type_zone_id; ?>" <?php echo $sel; ?>>
																<?php echo $row->type_zone_title; ?>
															</option>
															<?php
																}//END FOREACH
															?>
													</select>
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-2 control-label">ประเภทการเสนอขาย</label>
												<div class="col-md-10">
													<select id="type_offering_id" class="form-control" name="type_offering_id" onchange="(this)">
														<option value="0">----เลือก----</option>
														<?php
																foreach ($type_offering as $row) {//START FOREACH
																	$sel = ($row->type_offering_id == $type_offering_id)? 'selected': '';
															?>
															<option value="<?php echo $row->type_offering_id; ?>" <?php echo $sel; ?>>
																<?php echo $row->type_offering_title; ?>
															</option>
															<?php
																}//END FOREACH
															?>
													</select>
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-2 control-label">โซน</label>
												<div class="col-md-10">
													<select id="zone_id" class="form-control" name="zone_id" onchange="(this)">
														<option value="0">----เลือก----</option>
														<?php
																foreach ($type_zone as $row) {//START FOREACH
																	$sel = ($row->type_zone_id == $zone_id)? 'selected': '';
															?>
															<option value="<?php echo $row->type_zone_id; ?>" <?php echo $sel; ?>>
																<?php echo $row->type_zone_title; ?>
															</option>
															<?php
																}//END FOREACH
															?>
													</select>
												</div>
											</div>
										</div>
										<!-- end col -->
									</div>
									<!-- end row -->
								</div>
								<!-- end box -->

								<?php
								/*if ( $action == 'edit')
								//{
									?>
								<div class="card-box">
									<h4 class="header-title m-t-0 m-b-30">สถานะ</h4>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group">
												<label class="col-md-2 control-label">สถานะของโครงการ</label>
												<div class="col-md-10">
													<div class="row">
														<select id="type_status_id" class="form-control" name="type_status_id" onchange="(this)">
															<option value="0">----เลือก----</option>
															<?php
																foreach ($type_status as $row)
																{
																	$sel = ($row->type_status_id == $type_status_id)? 'selected': '';
																?>
																<option value="<?php echo $row->type_status_id; ?>" <?php echo $sel; ?>>
																	<?php echo $row->type_status_title; ?>
																</option>
																<?php
																}//END FOREACH
																?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<!-- end col -->
									</div>
									<!-- end row -->
								</div>
								<!-- end box -->
								<?php
										//} *///END IF ACTION
								?>

								<div class="card-box">
									<h4 class="header-title m-t-0 m-b-30">รายละเอียดโครงการ</h4>
									<div class="row">
										<div class="col-lg-12">

											<div class="form-group">
												<label class="col-md-2 control-label">ชื่อโครงการ</label>
												<div class="col-md-10">
													<input type="text" name="condo_title" class="form-control" value="<?php echo $condo_title; ?>" placeholder="">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-2 control-label">เจ้าของโครงการ</label>
												<div class="col-md-10">
													<input type="text" name="condo_owner_name" class="form-control" value="<?php echo $condo_owner_name; ?>" placeholder="">
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-2 control-label">สิทธิ์ถือครอง</label>
												<div class="col-md-10">
													<input type="text" name="condo_holder_name" class="form-control" value="<?php echo $condo_holder_name; ?>" placeholder="">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-sm-2">ปีที่สร้างเสร็จ</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" maxlength="4" name="condo_builing_finish" id="yearsoption" value="<?php echo $condo_builing_finish;?>">
													<!-- input-group -->
												</div>
											</div>
											<!-- end col -->
										</div>
										<!-- end col -->
									</div>
									<!-- end row -->
								</div>
								<!-- end box -->

								<div class="card-box">
									<h4 class="header-title m-t-0 m-b-30">ที่อยู่</h4>
									<div class="row">
										<div class="col-lg-12">

											<div class="form-group">
												<label class="control-label col-sm-2">ห้องเลขที่ / บ้านเลขที่</label>
												<div class="col-sm-2">
													<input class="form-control" type="text" value="<?php echo $condo_no; ?>" name="condo_no">
												</div>

												<label class="control-label col-sm-1">ชั้นที่ (คอนโด)</label>
												<div class="col-sm-3">
													<input class="form-control" type="text" value="<?php echo $condo_floor; ?>" name="condo_floor">
												</div>

												<label class="control-label col-sm-2">พื้นที่ใช้สอย / ตร.ม.</label>
												<div class="col-sm-2">
													<input class="demical_picker" type="text" value="<?php echo $condo_area_apartment; ?>" name="condo_area_apartment">
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-2 control-label">ตรอก / ซอย</label>
												<div class="col-md-4">
													<input class="form-control" type="text" value="<?php echo $condo_alley; ?>" name="condo_alley">
												</div>

												<label class="col-md-1 control-label">ถนน</label>
												<div class="col-md-4">
													<input class="form-control" type="text" value="<?php echo $condo_road; ?>" name="condo_road">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">จังหวัด</label>
												<div class="col-sm-2">
													<select id="provinces_id" class="form-control" name="provinces_id" onchange="(this)">
														<option value="0">----เลือก----</option>
														<?php
																foreach ($provinces as $row) {//START FOREACH
																	$sel = ($row->provinces_id == $provinces_id)? 'selected': '';
															?>
															<option value="<?php echo $row->provinces_id; ?>" <?php echo $sel; ?>>
																<?php echo $row->provinces_name; ?>
															</option>
															<?php
																}//END FOREACH
															?>
													</select>
												</div>

												<label class="col-sm-1 control-label">อำเภอ/เขต</label>
												<div class="col-sm-3">
													<select id="districts_id" class="form-control" name="districts_id" onchange="(this)">
														<option value="0">----เลือก----</option>
													</select>
												</div>

												<label class="col-sm-1 control-label">ตำบล/แขวง</label>
												<div class="col-sm-3">
													<select id="sub_districts_id" class="form-control" name="sub_districts_id" onchange="(this)">
														<option value="0">----เลือก----</option>

													</select>
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-2 control-label">รหัสไปรษณีย์</label>
												<div class="col-md-10">
													<input type="text" name="condo_zipcode" maxlength="5" class="form-control zipcode" value="<?php echo $condo_zipcode; ?>" placeholder="">
												</div>
											</div>
										</div>
										<!-- end col -->
									</div>
									<!-- end row -->
								</div>
								<!-- end box -->

								<div class="card-box">
									<h4 class="header-title m-t-0 m-b-30">แบบห้อง</h4>
									<div class="row">
										<div class="col-lg-12">



											<div class="form-group">

												<label class="col-md-2 control-label">แบบห้องชุด</label>
												<div class="col-md-4">
													<select id="type_suites_id" class="form-control" name="type_suites_id" onchange="(this)">
														<option value="0">----เลือก----</option>
														<?php
																foreach ($type_suites as $row) {//START FOREACH
																	$sel = ($row->type_suites_id == $type_suites_id)? 'selected': '';
															?>
															<option value="<?php echo $row->type_suites_id; ?>" <?php echo $sel; ?>>
																<?php echo $row->type_suites_title; ?>
															</option>
															<?php
																}//END FOREACH
															?>
													</select>
												</div>

												<label class="col-md-2 control-label">การตกแต่ง</label>
												<div class="col-md-4">
													<select id="type_decoration_id" class="form-control" name="type_decoration_id" onchange="(this)">
														<option value="0">----เลือก----</option>
														<?php
																foreach ($type_decoration as $row) {//START FOREACH
																	$sel = ($row->type_decoration_id == $type_decoration_id)? 'selected': '';
															?>
															<option value="<?php echo $row->type_decoration_id; ?>" <?php echo $sel; ?>>
																<?php echo $row->type_decoration_title; ?>
															</option>
															<?php
																}//END FOREACH
															?>
													</select>
												</div>


											</div>

											<div class="form-group">
												<label class="control-label col-sm-2">จำนวนห้องนอน</label>
												<div class="col-sm-4">
													<input class="numeric_picker" type="text" value="<?php echo $condo_all_room; ?>" name="condo_all_room">
												</div>

												<label class="control-label col-sm-2">จำนวนห้องน้ำ</label>
												<div class="col-sm-4">
													<input class="numeric_picker" type="text" value="<?php echo $condo_all_toilet; ?>" name="condo_all_toilet">
												</div>
											</div>

											<div class="form-group">
												<label class="control-label col-sm-2">พื้นที่ใช้สอย / ตร.ม.</label>
												<div class="col-sm-4">
													<input class="demical_picker" type="text" value="<?php echo $condo_living_area; ?>" name="condo_living_area">
												</div>

												<label class="control-label col-sm-2">อุปกรณ์ตกแต่งภายในห้อง</label>
												<div class="col-sm-4">
													<input class="form-control" type="text" value="<?php echo $condo_interior_room; ?>" name="condo_interior_room">
												</div>
											</div>

											<div class="form-group">
												<label class="control-label col-sm-2">ทิศของห้อง</label>
												<div class="col-sm-4">
													<input class="form-control" type="text" value="<?php echo $condo_direction_room; ?>" name="condo_direction_room">
												</div>

												<label class="control-label col-sm-2">วิวของห้อง</label>
												<div class="col-sm-4">
													<input class="form-control" type="text" value="<?php echo $condo_scenery_room; ?>" name="condo_scenery_room">
												</div>

											</div>




										</div>
										<!-- end col -->
									</div>
									<!-- end row -->
								</div>
								<!-- end box -->

								<div class="card-box">
									<h4 class="header-title m-t-0 m-b-30">ราคาโครงการ</h4>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group">

												<label class="control-label col-sm-2">ราคา</label>
												<div class="col-sm-4">
													<input type="text" name="condo_total_price" class="float_price form-control" value="<?php echo $condo_total_price; ?>">
													<span class="font-13 text-muted">กรอกทศนยิม 2 ตำแหน่ง : 999999999.99</span>
												</div>

												<label class="control-label col-sm-2">ราคา/ตร.ม.</label>
												<div class="col-sm-4">
													<input type="text" name="condo_price_per_square_meter" class="float_price form-control" value="<?php echo $condo_price_per_square_meter; ?>">
													<span class="font-13 text-muted">กรอกทศนยิม 2 ตำแหน่ง : 999999999.99</span>
												</div>
											</div>

										</div>
										<!-- end col -->
									</div>
									<!-- end row -->
								</div>
								<!-- end box -->

							<div class="card-box">
								<h4 class="header-title m-t-0 m-b-30">ข้อมูลรายละเอียด</h4>
								<div class="row">
									<div class="col-lg-12">

										<div class="form-group">
											<label class="col-md-2 control-label">คำค้นหา</label>
											<div class="col-md-10">
												<input type="text" name="condo_tag" maxlength="255" class="inputmaxlength form-control" value="<?php echo $condo_tag; ?>" placeholder="">
												<span class="font-13 text-muted">ระบุคำที่ต้องการแล้วตามด้วย (,)</span>
											</div>
										</div>

										<div class="form-group">
											<label class="col-md-2 control-label">รายละเอียดย่อย</label>
											<div class="col-md-10">
												<textarea name="condo_expert" class="form-control" id="condo_expert" cols="30" rows="10"><?php echo $condo_expert; ?></textarea>
											</div>
										</div>

										<div class="form-group">
											<label class="col-md-2 control-label">รายละเอียดทั้งหมด</label>
											<div class="col-md-10">
												<textarea class="wysiwigeditor"  name="condo_description" id="condo_description"><?php echo $condo_description;?></textarea>
												<!-- <input name="image" type="file" id="upload_wysiwigeditor" class="hidden" onchange=""> -->
											</div>
										</div>
										</div>
									<!-- end col -->
								</div>
								<!-- end row -->
							</div>
							<!-- end box -->

							<div class="card-box">
								<h4 class="header-title m-t-0 m-b-30">สิ่งอำนวยความสะดวก / รายการที่น่าสนใจ</h4>
								<div class="row">
									<div class="col-lg-12">


										<div class="form-group">
											<label class="col-md-2 control-label">สิ่งอำนวยความสะดวก</label>
											<div class="col-md-10">
												<div class="row">
													<?php
													if ( $action == 'add' )
													{
														foreach ($facilities as $row => $value)
														{
													?>
														<div class="col-sm-6 col-md-4">
															<div class="checkbox checkbox-inline checkbox-primary" >
																<input type="checkbox" name="facilities_id[]" value="<?php echo $value->facilities_id; ?>">
																<label for="">
																	<?php echo $value->facilities_title; ?>
																</label>
															</div>
														</div>

													<?php
														}//END FOREACH
													}//END IF ACTION
													?>
													<?php
													if ( $action == 'edit')
													{
														$query = $this->db
															->select('facilities_id, facilities_title')
															->order_by('facilities_id', 'ASC')
															->get('facilities');
														while($row = $query->unbuffered_row())
														{
															$facilities_id = $row->facilities_id;

															$query2 = $this->db
																->select('*')
																->where('condo_id', $condo_id)
																->where('facilities_id', $facilities_id)
																->get('condo_relation_facilities');
															$i = 1;
															if ($query2->num_rows() > 0)
															{
																$row2 = $query2->row();
																//var_dump($row2->condo_relation_facilities_id);
																//echo $row2->featured_id;
														?>
														<div class="col-sm-6 col-md-4">
															<div class="checkbox checkbox-inline checkbox-primary" >
																<input type="checkbox" name="facilities_id[]" value="<?php echo $row->facilities_id; ?>" checked>

																<label for="">
																	<?php echo $row->facilities_title; ?>
																</label>
																<input type="hidden" name="condo_relation_facilities_id[]" value="<?php echo $row2->condo_relation_facilities_id; ?>">
															</div>
														</div>
															<?php
															}
															else
															{
															?>
														<div class="col-sm-6 col-md-4">
															<div class="checkbox checkbox-inline checkbox-primary" >
																<input type="checkbox" name="facilities_id[]" value="<?php echo $row->facilities_id; ?>">
																<label for="">
																	<?php echo $row->facilities_title; ?>
																</label>
															</div>
														</div>
														<?php
															}//END IF
														}
													}//END IF ACTION
														?>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label">รายการที่น่าสนใจ</label>
											<div class="col-md-10">
												<div class="row">
													<?php
													if ( $action == 'add' )
													{
														foreach ($featured as $row_feat)
														{
													?>
														<div class="col-sm-6 col-md-4">
															<div class="checkbox checkbox-inline checkbox-primary" >
																<input type="checkbox" name="featured_id[]" value="<?php echo $row_feat->featured_id; ?>">
																<label for="">
																	<?php echo $row_feat->featured_title; ?>
																</label>
															</div>
														</div>
														<?php
														}//END FOREACH
													}//END IF ACTION

													if ( $action == 'edit')
													{


													$i = 0;
													$query = $this->db
														->select('featured_id, featured_title')
														->order_by('featured_id', 'ASC')
														->get('featured');
													while($row = $query->unbuffered_row())
													{
														$featured_id = $row->featured_id;

														$query2 = $this->db
															->select('*')
															->where('condo_id', $condo_id)
															->where('featured_id', $featured_id)
															->get('condo_relation_featured');
														if ($query2->num_rows() > 0)
														{
															$row2 = $query2->row();// ไม่ได้ใช้อะไรนิ
															//echo $row2->featured_id;

														?>
														<div class="col-sm-6 col-md-4">
															<div class="checkbox checkbox-inline checkbox-primary">
																<input type="checkbox" name="featured_id[]" value="<?php echo $row->featured_id; ?>" checked>
																<label for="">
																	<?php echo $row->featured_title; ?>
																</label>
																<input type="hidden" name="condo_relation_featured_id[]" value="<?php echo $row2->condo_relation_featured_id; ?>">
															</div>
														</div>
														<?php
														}
														else
														{
														?>
														<div class="col-sm-6 col-md-4">
															<div class="checkbox checkbox-inline checkbox-primary">
																<input type="checkbox" name="featured_id[]" value="<?php echo $row->featured_id; ?>">
																<label for="">
																	<?php echo $row->featured_title; ?>
																</label>
															</div>
														</div>
														<?php
														}
//																		$this->primaryclass->pre_var_dump($row2);
													}
															}//END IF ACTION
														?>
												</div>
											</div>
										</div>

									</div>
									<!-- end col -->
								</div>
								<!-- end row -->
							</div>
							<!-- end box -->

							<div class="card-box">
								<h4 class="header-title m-t-0 m-b-30">สถานที่ใกล้เคียง</h4>
								<span class="font12">จำกัด 10 สถานที่ใกล้เคียง</span>
								<div class="row">
									<div class="col-lg-12">
									<?php
										if( !empty($condo_relation_transportation) )
										{
										?>
											<?php
														foreach ($condo_relation_transportation as $row_relation)
														{
															$condo_relation_transportation_id = $row_relation->condo_relation_transportation_id;
															$transportation_category_id = $row_relation->transportation_category_id;
															$transportation_id = $row_relation->transportation_id;
															$transportation_distance = $row_relation->transportation_distance;

												?>
										<div class="form-group transportation<?php echo $condo_relation_transportation_id; ?>">
											<label class="col-md-2 control-label m-t-10">สถานที่ใกล้เคียง</label>
											<div class="col-sm-12 col-md-4">
												<select id="transportation_category_id" class="form-control" name="transportation_category_id[]" onchange="(this)">
													<option value="0">----เลือก----</option>
													<?php
													foreach ($transportation_category as $row_tc)
													{//START FOREACH GROUP


														if ($transportation_category != $row_tc->transportation_category_id)
														{
															if ($transportation_category != '')
															{
																echo '</optgroup>';
															}
															echo '<optgroup label="'.ucfirst($row_tc->transportation_category_title).'">';
														}
														//$transportation = $this->Condominium_model->get_transportation_row_by_condo($condo_id, $row->transportation_category_id);
														$transportation = $this->Condominium_model->get_transportation_option($row_tc->transportation_category_id);
														foreach ($transportation as $row_t)
														{//START FOREACH
															$sel = ($row_t->transportation_id == $transportation_id)? 'selected': '';
															echo '<option value="'.$row_t->transportation_id.'" '.$sel.' >'.$row_t->transportation_title.'</option>';
														}//END FOREACH
													?>
													<?php
													}//END FOREACH GROUP
													if ($transportation_category != '') {
														echo '</optgroup>';
													}
													?>
												</select>
											</div>
											<div class="col-sm-10 col-md-4">
												<input type="text" name="transportation_distance[]" class="inputmaxlength form-control" value="<?php echo $transportation_distance; ?>" maxlength="50" placeholder="">
												<span class="font-13 text-muted">ตัวอย่าง : 7 นาที &#40;1.46 กิโลเมตร&#41;</span>
											</div>
											<div class="col-sm-2 col-md-2">
												<a href="javascript:void(0);" class="delete_transportation" id="<?php echo $condo_relation_transportation_id; ?>" title="delete item"><i class="zmdi zmdi-delete"></i></a>
											</div>
										</div>
													<?php
														}//END FOREACH
											?>
													<?php
										}//END IF
											?>

										<div class="form-clone">
											<div class="form-group">
												<label class="col-md-2 control-label m-t-10">สถานที่ใกล้เคียง</label>
												<?php
												if ( $action == 'add')
												{
													$transportation_distance = '';
												?>

												<div class="col-sm-12 col-md-4">
													<select id="transportation_category_id" class="form-control" name="transportation_category_id[]" onchange="(this)">
														<option value="0">----เลือก----</option>
														<?php
														foreach ($transportation_category as $row)
														{//START FOREACH GROUP
															$sel = ($row->transportation_category_id == $transportation_category_id)? 'selected': '';

															if ($transportation_category != $row->transportation_category_id)
															{
																if ($transportation_category != '')
																{
																	echo '</optgroup>';
																}
																echo '<optgroup label="'.ucfirst($row->transportation_category_title).'">';
															}
															$transportation = $this->Condominium_model->get_transportation_option($row->transportation_category_id);
															//$transportation = $this->Condominium_model->get_transportation_row_by_condo($condo_id, $row->transportation_category_id);
															foreach ($transportation as $row_in)
															{//START FOREACH
																echo '<option value="'.$row_in->transportation_id.'">'.$row_in->transportation_title.'</option>';
															}//END FOREACH
														?>

															<?php
														}//END FOREACH GROUP
														if ($transportation_category != '') {
															echo '</optgroup>';
														}
														?>
													</select>
												</div>
												<div class="col-sm-10 col-md-4">
													<input type="text" name="transportation_distance[]" class="inputmaxlength form-control" value="<?php echo $transportation_distance; ?>" maxlength="50" placeholder="">
													<span class="font-13 text-muted">ตัวอย่าง : 7 นาที &#40;1.46 กิโลเมตร&#41;</span>
												</div>
												<?php
												}
												else
												{
												?>




												<div class="col-sm-12 col-md-4">
													<select id="transportation_category_id" class="form-control" name="transportation_category_id[]" onchange="(this)">
														<option value="0">----เลือก----</option>
														<?php
														foreach ($transportation_category as $row)
														{//START FOREACH GROUP
															$transportation_category_id = 0;
															$sel = ($row->transportation_category_id == $transportation_category_id)? 'selected': '';

															if ($transportation_category != $row->transportation_category_id)
															{
																if ($transportation_category != '')
																{
																	echo '</optgroup>';
																}
																echo '<optgroup label="'.ucfirst($row->transportation_category_title).'">';
															}
															//$transportation = $this->Condominium_model->get_transportation_row_by_condo($condo_id, $row->transportation_category_id);
															$transportation = $this->Condominium_model->get_transportation_option($row->transportation_category_id);
															foreach ($transportation as $row_in)
															{//START FOREACH
																echo '<option value="'.$row_in->transportation_id.'">'.$row_in->transportation_title.'</option>';
															}//END FOREACH
														?>

															<?php
														}//END FOREACH GROUP
														if ($transportation_category != '') {
															echo '</optgroup>';
														}
														?>
													</select>
												</div>
												<div class="col-sm-10 col-md-4">
													<input type="text" name="transportation_distance[]" class="inputmaxlength form-control" value="<?php //echo $row_in->transportation_distance; ?>" maxlength="50" placeholder="">
													<span class="font-13 text-muted">ตัวอย่าง : 7 นาที &#40;1.46 กิโลเมตร&#41;</span>
												</div>
												<?php
												}//END IF ACTION
												?>
												<div class="col-sm-2 col-md-2">
													<a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa fa-plus"></i></a>
												</div>
											</div>
										</div>
										<!-- end clone -->
									</div>
									<!-- end col -->
								</div>
								<!-- end row -->
							</div>
							<!-- end box -->



							<div class="card-box">
								<h4 class="header-title m-t-0 m-b-30">เจ้าของคอนโด</h4>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label class="col-md-2 control-label">ชื่อสมาชิก เจ้าของโครงการ</label>
											<div class="col-md-10">
												<select id="member_id" class="form-control" name="member_id" onchange="(this)">
													<option value="0">----เลือก----</option>
													<?php
															foreach ($member as $row) {//START FOREACH
																$sel = ($row->member_id == $member_id)? 'selected': '';
														?>
														<option value="<?php echo $row->member_id; ?>" <?php echo $sel; ?>>
															<?php echo $row->member_fname. ' '.$row->member_lname; ?>
														</option>
														<?php
															}//END FOREACH
														?>
												</select>
											</div>
										</div>
									</div>
									<!-- end col -->
								</div>
								<!-- end row -->
							</div>
							<!-- end box -->

							<div class="card-box">
								<h4 class="header-title m-t-0 m-b-30">แผนที่</h4>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label class="col-md-2 control-label">Google Maps</label>
											<div class="col-md-10">
												<div id="map-canvas"></div>
											</div>
											<input class="form-control" type="hidden" id="latitude" name="latitude" value="<?php echo $latitude; ?>">
											<input class="form-control" type="hidden" id="longitude" name="longitude" value="<?php echo $longitude; ?>">
										</div>

										<!-- <div class="form-group m-to-30">
											<label class="col-md-2 control-label">Latitude</label>
											<div class="col-md-4">
											</div>

											<label class="col-md-1 control-label">Longitude</label>
											<div class="col-md-4">
											</div>
										</div> -->

									</div>
									<!-- end col -->
								</div>
								<!-- end row -->
							</div>
							<!-- end box -->

							<div class="card-box">
								<h4 class="header-title m-t-0 m-b-30">รูปภาพ / แกเลอรี่</h4>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label class="col-md-2 control-label" for="pic_thumb">รูปภาพขนาดเล็ก</label>
											<div class="col-md-4">
												<input type="file" name="pic_thumb"
													id="pic_thumb"
													class="dropify"
													data-max-file-size="1M"
													data-default-file="<?php echo (!empty($pic_thumb))? base_url().'uploads/condominium/'.$pic_thumb : '';?>" />
													<span class="help-block"><small>ขนาดรูปภาพไม่เกิน 2 MB และ <span class="label label-warning"> กว้าง : 656px X ยาว : 464px </span></small></span>
											</div>
										</div>

										<div class="form-group">
											<label class="col-md-2 control-label" for="pic_large">รูปภาพขนาดใหญ่</label>
											<div class="col-md-4">
												<input type="file" name="pic_large"
													id="pic_large"
													class="dropify"
													data-max-file-size="1M"
													data-default-file="<?php echo (!empty($pic_large))? base_url().'uploads/condominium/'.$pic_large : '';?>" />
													<span class="help-block"><small>ขนาดรูปภาพไม่เกิน 2 MB และ <span class="label label-warning"> กว้าง : 900px X ยาว : 500px </span></small></span>
											</div>
										</div>

										<?php
								if ( $action == 'add')
								{
									?>
										<div class="form-group">
											<label class="col-md-2 control-label" for="pic_gallery">รูปภาพแกเลอรี่</label>
											<div class="col-md-10">

												<div id="dropzone" class="dropzone">
													<div class="fallback">
														<input name="file[]" type="file" multiple />
														<span class="help-block"><small>ขนาดรูปภาพไม่เกิน 2 MB และ <span class="label label-warning"> กว้าง : 1200px X ยาว : 800px </span></small></span>
													</div>
												</div>
											</div>
										</div>

										<div class="form-group">
											<label class="col-md-2 control-label" for="pic_large">&nbsp;</label>
											<div class="col-md-10">
												<div id="preview-gallery" style=""></div>
												<div id="file_name"></div>
											</div>
										</div>
									<?php
								}
									?>
									</div>
									<!-- end col -->
								</div>
								<!-- end row -->
							</div>
							<!-- end box -->

							<div class="card-box">
								<h4 class="header-title m-t-0 m-b-30">ส่วนการแสดงผล</h4>
								<div class="row">
									<div class="col-lg-12">

										<div class="form-group">
											<label class="control-label col-sm-2">วันที่สร้าง</label>
											<div class="col-sm-2">
												<div class="input-group">
													<input type="text" name="createdate_date" class="form-control datepicker-autoclose" placeholder="" value="<?php echo $createdate_date; ?>" id="">
													<span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
												</div><!-- input-group -->
											</div>
											<div class="col-sm-2">
												<div class="input-group">
													<div class="bootstrap-timepicker">
	                          <input id="" type="text" name="createdate_time" class="timepicker form-control" value="<?php echo $createdate_time; ?>">
	                        </div>
	                        <span class="input-group-addon bg-primary b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>
												</div>
											</div>

											<label class="control-label col-sm-2">วันที่แก้ไข</label>
											<div class="col-sm-2">
												<div class="input-group">
													<input type="text" name="modifydate_date" class="form-control datepicker-autoclose" placeholder="" value="<?php echo $modifydate_date; ?>" id="">
													<span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
												</div><!-- input-group -->
											</div>
											<div class="col-sm-2">
												<div class="input-group">
													<div class="bootstrap-timepicker">
	                          <input id="" name="modifydate_time" type="text" class="timepicker form-control" value="<?php echo $modifydate_time; ?>">
	                        </div>
	                        <span class="input-group-addon bg-primary b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>
												</div>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label"> อนุมัติ </label>
											<div class="col-sm-10">
												<div class="radio radio-pink">
													<input id="radio1" value="2" name="approved" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" <?php echo ($approved==2)? 'checked' : ''; ?>>
													<label for="radio1"> อนุมัติ </label>
												</div>

												<div class="radio radio-pink">
													<input id="radio2" value="1" name="approved" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" required="" <?php echo ($approved==1 || $approved=='' )? 'checked' : ''; ?>>
													<label for="radio2"> ไม่อนุมัติ </label>
												</div>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label"> ใหม่ล่าสุด </label>
											<div class="col-sm-10">
												<div class="radio radio-pink">
													<input id="newsarrival1" value="2" name="newsarrival" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" <?php echo ($newsarrival==2)? 'checked' : ''; ?>>
													<label for="newsarrival1"> ใหม่ล่าสุด </label>
												</div>

												<div class="radio radio-pink">
													<input id="newsarrival2" value="1" name="newsarrival" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" required="" <?php echo ($newsarrival==1 || $newsarrival=='' )? 'checked' : ''; ?>>
													<label for="newsarrival2"> ไม่ใหม่ล่าสุด </label>
												</div>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">แนะนำ</label>
											<div class="col-sm-10">
												<div class="radio radio-pink">
													<input id="highlight1" value="2" name="highlight" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" <?php echo ($highlight==2)? 'checked' : ''; ?>>
													<label for="highlight1"> แนะนำ </label>
												</div>

												<div class="radio radio-pink">
													<input id="highlight2" value="1" name="highlight" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" required="" <?php echo ($highlight==1 || $highlight=='' )? 'checked' : ''; ?>>
													<label for="highlight2"> ไม่แนะนำ </label>
												</div>
											</div>
										</div>

<!--
										<div class="form-group">
											<label class="col-sm-2 control-label">แสดงผล</label>
											<div class="col-sm-10">
												<div class="radio radio-pink">
													<input id="publish1" value="2" name="publish" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" <?php echo ($publish==2)? 'checked' : ''; ?>>
													<label for="publish1"> เผยแพร่ </label>
												</div>

												<div class="radio radio-pink">
													<input id="publish2" value="1" name="publish" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" required="" <?php echo ($publish==1 || $publish=='' )? 'checked' : ''; ?>>
													<label for="publish2"> ไม่เผยแพร่ </label>
												</div>
											</div>
										</div>
-->

									</div>
									<!-- end col -->
								</div>
								<!-- end row -->
							</div>
							<!-- end box -->





							<div class="card-box">
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10 m-t-15">
												<button id="submit" type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light"  onclick="tinyMCE.triggerSave(true,true);">Submit</button>
												<a href="<?php echo site_url('condominium/view'); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">
Cancel
								</a>
								<input type="hidden" name="action" value="action">
											</div>
										</div>
									</div>
									<!-- end col -->
								</div>
								<!-- end row -->
							</div>
							<!-- end box -->
						<?php
							if(isset( $data['row'] ))
							{
						?>
						</form>
						<?php
							}
							else
							{
						?>
						</form>
				<?php } ?>
						<!-- end form -->
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->
			</div>
			<!-- container -->

		</div>
		<!-- content -->


		<!-- ============================================================== -->
		<!-- End Right content here -->
		<!-- ============================================================== -->

		<?php
	if(!empty($googlemap))
	{
		?>
				<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtK77y4LHtYZ5ZoVeVVwpkMUoy-Eq9JRA&sensor=false"></script>

				<script>
					// In the following example, markers appear when the user clicks on the map.
					// Each marker is labeled with a single alphabetical character.
					var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
					var labelIndex = 0;
					var map;

					function initialize() {

						<?php
		if($action == 'edit' )
		{
						?>
						var bangkok = {
							lat: <?php echo $latitude; ?>,
							lng: <?php echo $longitude; ?>
						};
	<?php
		}
		else
		{
	?>
						var bangkok = {
							lat: 13.7563,
							lng: 100.5018
						};
	<?php
		}
	?>
						map = new google.maps.Map(document.getElementById('map-canvas'), {
							zoom: 11,
							center: bangkok
						});

//						var marker = new google.maps.Marker({
//							position: bangkok,
//							map: map,
//							title: 'Set lat/lon values for this property',
//							draggable: true
//						});

						var marker = new google.maps.Marker({
							draggable: true,
							position: bangkok,
							map: map,
							title: "Your location"
						});

						// This event listener calls addMarker() when the map is clicked.
//						google.maps.event.addListener(map, 'click', function(event) {
//							addMarker(event.latLng, map);
//						});

						// This event listener calls addMarker() when the map is clicked.
						//addMarker(bangkok, map);

						google.maps.event.addListener(marker, 'dragend', function (event) {
							$('#latitude').val(event.latLng.lat());
							$('#longitude').val(event.latLng.lng());
							//console.log( 'latitude : ' + event.latLng.lat()  );
							//console.log( 'longtitude : ' + event.latLng.lng()  );
						});
					}

					// Adds a marker to the map.
					var i = 0;

					function addMarker(location, map) {
						// Add the marker at the clicked location, and add the next-available label
						// from the array of alphabetical characters.
						var iMax = 1;
						if (i < iMax) {
							var marker = new google.maps.Marker({
								draggable: true,
								position: location,
								label: labels[labelIndex++ % labels.length],
								map: map,
								title: "Your location"
							});
							i++;
						} else {
							console.log('you can only post', i - 1, 'markers');
						}
						//map.panTo(location);
					}

					google.maps.event.addDomListener(window, 'load', initialize);


				</script>
				<?php
	}
	?>

		<?php $this->load->view('backoffice/partials/footer')?>
			<script src="<?php echo base_url()?>assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
			<script src="<?php echo base_url()?>assets/js/dropzone.min.js" type="text/javascript"></script>
			<script>

$(document).ready(function () {

	// Select2
	$(".select2").select2();

	//Datepicker
	jQuery('.datepicker-autoclose').datepicker({
			autoclose: true,
			defaultDate: new Date(),
			format: 'yyyy-mm-dd',
			todayHighlight: true,
	});
	// Time Picker
	jQuery('.timepicker').timepicker({
		maxHours		: 12,
		defaultTIme : 'current',
		showSeconds : true,
		showMeridian: false,
	});



	$("form#frmdata").submit(function(event){
		var formData = new FormData($(this)[0]);

		<?php
		$ajax_url = ($action === 'add')? "condominium/add_condominium" :  "condominium/edit_condominium";
		$msg = ($action === 'add')? "Insert" :  "Update";
		?>
		var url = "<?php echo base_url().$ajax_url; ?>";

		var formData = new FormData($(this)[0]);

		$.ajax({
			url: url,
			type: "POST",
			data: formData,
			cache: false,
			processData: false,
			contentType: false,
			context: this,
			success: function (data, status)
			{
				console.log( data );
				swal({
					title: "Success Data!",
					text: "Confirm you save",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, <?php echo $msg; ?> it!",
					cancelButtonText: "No, cancel plx!",
					closeOnConfirm: true,
					closeOnCancel: true }, function(isConfirm){
					if (isConfirm)
					{
						//window.location.replace('<?php echo base_url(); ?>condominium/view');
					}
					else
					{
						window.location.reload();
					}
				});

			},
			error: function (xhr, desc, err)
			{
				console.log( err );
			},

		});
		event.preventDefault();
		return false;
	});

	$(".delete_transportation").click(function(event){

		<?php
		$ajax_url = "condominium/delete_transportation_condominium";
		?>
		var url = "<?php echo base_url().$ajax_url; ?>";
		var id = $(this).attr('id');

		$.ajax({
			url: url,
			type: "POST",
			data: {id : id},
			success: function (data, status)
			{
				//console.log( data );
				swal({
					title: "Success Data!",
					text: "Confirm you save",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, Delete it!",
					cancelButtonText: "No, cancel plx!",
					closeOnConfirm: true,
					closeOnCancel: true }, function(isConfirm){
					if (isConfirm)
					{
						$('.transportation'+id).fadeOut( "slow" );
						$('.transportation'+id).empty();
					}
					else
					{
						window.location.reload();
					}
				});

			},
			error: function (xhr, desc, err)
			{
				console.log( err );
			},

		});
		event.preventDefault();
		return false;
	});


	var fileList = new Array;
	var i = 0;
	var file_up_names=[];
	$("div#dropzone").dropzone({
		url: "<?php echo site_url('condominium/add_upload'); ?>",
		addRemoveLinks : true,
		//previewsContainer: "#preview-gallery",
		dictRemoveFile : 'REMOVE',
		success : function(file, responseText) {
			// Handle the responseText here. For example, add the text to the preview element:
			//console.log(responseText);
			//console.log(file);

			//file_up_names.push(file.name);
			//fileList[serverFileName] = {"serverFileName" : serverFileName, "fileName" : file.name };'./uploads/condominium/.'
			//$(file.previewTemplate).append('<span class="server_file">'+'<?php //echo site_url('/uploads/condominium/'); ?>'+file.name+'</span>');

			$.ajax({
				url: "<?php echo site_url('condominium/add_upload'); ?>",
				type: "POST",
				data: { 'file_name': file.name},
				success: function(data)
				{
					//console.log( $(this).length );

					var count_img = $(this).length;
					$( "<input type='hidden' name='file_name_[]'>" ).val(file.name).appendTo( "div#file_name" );
					//console.log( count_img );
				}
			});

		},
		removedfile: function(file)
		{
			x = confirm('Do you want to delete?');
			if(!x)  return false;

			$.ajax({
				url: "<?php echo site_url('condominium/canceled_upload'); ?>",
				type: "POST",
				data: { 'file_name': file.name},
				success: function(data)
				{

//					if( $( "input[name^='file_name_[]']" ).val() == file.name )
//					{
//						alert(file.name);
//					}
					$( "input[value='" + file.name+ "']" ).remove();
					//remove preview//
					$(document).find(file.previewElement).remove();
				}
			});
		}
	});
//	Dropzone.options.myDropzone = false;

	Dropzone.autoDiscover = false;
	var unique = "file[]";
	var acceptedFileTypes = "image/*"; //dropzone requires this param be a comma separated list

	Dropzone.options.myAwesomeDropzone = {
		// your other settings as listed above
		maxFiles: 10, // allowing any more than this will stress a basic php/mysql stack
		uploadMultiple	: true,
		paramName: unique,
		headers: {"MyAppname-Service-Type": "Dropzone"},
		acceptedFiles: acceptedFileTypes,
	}
});


$(document).ready(function () {


					// jQuery
//	event.preventDefault();


	var provinces_id = $("#provinces_id").val();
	//var districts_id = $("#districts_id").val();


	if( provinces_id != 0 )
	{
		$.ajax({
			url: "<?php echo base_url(); ?>page/provinces_districts_relation",
			data: {
				id: provinces_id
			},
			type: "POST",
			success: function (data) {
				$("#districts_id").html(data);
				$("#districts_id").val(<?php echo $districts_id; ?>);

				var districts_id = $("#districts_id").val();
				//console.log(districts_id);

				$.ajax({
					url: "<?php echo base_url(); ?>page/districts_subdistricts_relation",
					data: {
						id: districts_id
					},
					type: "POST",
					success: function (data) {
						$("#sub_districts_id").html(data);
						$("#sub_districts_id").val(<?php echo $sub_districts_id; ?>);
					}
				});

			}
		});


	}

});
				$("#provinces_id").change(function () {
					/*dropdown post */ //
					$.ajax({
						url: "<?php echo base_url(); ?>page/provinces_districts_relation",
						data: {
							id: $(this).val()
						},
						type: "POST",
						success: function (data) {
							$("#districts_id").html(data);
						}
					});
				});

				$("#districts_id").change(function () {
					/*dropdown post */ //
					$.ajax({
						url: "<?php echo base_url(); ?>page/districts_subdistricts_relation",
						data: {
							id: $(this).val()
						},
						type: "POST",
						success: function (data) {
							$("#sub_districts_id").html(data);
						}
					});
				});

				$("#transportation_category_id").change(function () {
					/*dropdown post */ //
					$.ajax({
						url: "<?php echo base_url(); ?>condominium/transportation_category_relation",
						data: {
							id: $(this).val()
						},
						type: "POST",
						success: function (data) {
							$("#transportation_id").html(data);
						}
					});
				});
			</script>
			<script type="text/javascript">
				$(document).ready(function () {
					var maxField = 10; //Input fields increment limitation
					var addButton = $('.add_button'); //Add button selector
					var wrapper = $('.form-clone'); //Input field wrapper
					//var fieldHTML = '<div><input type="text" name="field_name[]" value=""/><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="remove-icon.png"/></a></div>'; //New input field html
					var fieldHTML = ''; //New input field html
					$.ajax({
						url	: "<?php echo base_url(); ?>condominium/transportation_category_option",
						data: {
							id: $(this).val()
						},
						type: "POST",
						success: function (data) {
							fieldHTML = data;
							//console.log(fieldHTML);
							var x = 1; //Initial field counter is 1
							$(addButton).click(function () { //Once add button is clicked
								if (x < maxField) { //Check maximum number of input fields
									x++; //Increment field counter
									$(wrapper).append(fieldHTML); // Add field html
								}
							});
							$(wrapper).on('click', '.remove_button', function (e) { //Once remove button is clicked
								//alert('sssss');
								e.preventDefault();
								$(this).closest('.form-group').remove(); //Remove field html
								x--; //Decrement field counter
							});
						}
					});
					//var fieldHTML = '<div><input type="text" name="field_name[]" value=""/><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="remove-icon.png"/></a></div>'; //New input field html




				});
			</script>


<script type="text/javascript">

	var condo_id = $("#condo_id").val();
	$('.dropify').dropify({
		messages: {
				'default': 'Drag and drop a file here or click',
				'replace': 'Drag and drop or click to replace',
				'remove': 'Remove',
				'error': 'Ooops, something wrong appended.'
		},
		error: {
				'fileSize': 'The file size is too big (1M max).'
		}
	});

	var drEvent = $('.dropify').dropify();
	drEvent.on('dropify.beforeClear', function(event, element){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('condominium/remove_image_update'); ?>",
			data: {
				"condo_id": condo_id,
				"imagefile": element.file.name,
				"field_name": $(this).attr("name"),
			},
			success: function(response) {
				console.log('ok success!');
			}
		}); //END AJX

		var r = confirm("Do you really want to delete \"" + element.file.name + "\" ?");
		if (r == true) {
			$( "#old_" + $('.dropify').attr('id') ).val( "" );
		} else {
			console.log('Cancel');
		}
		//return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
	});

	var drEvent = $('.dropify').dropify();
	drEvent.on('dropify.afterClear', function(event, element){
		console.log('File delete success!');
	});
</script>
