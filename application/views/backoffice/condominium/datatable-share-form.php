<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');
?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">


										<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>ลำดับ</th>
													<th>ชื่อผู้ติดต่อ</th>
													<th>เบอร์</th>
													<th>อีเมล์</th>
													<th>ข้อความ</th>
													<th>สถานะ</th>
													<th>จัดการ</th>
												</tr>
											</thead>
											<tbody>
												<?php
												$i = 1;

												if( !empty($data) )
												{

									foreach($data['data'] as $row => $value)
									{//START FOREACH#1

										$condo_share_form_id = $value->condo_share_form_id;
										$fk_member_id = $value->fk_member_id;
										$fk_condo_id = $value->fk_condo_id;
										$type_status_id = $value->fk_type_status_id;
									?>
													<tr>
														<td><?php echo $i; ?></td>
														<td><?php echo $value->condo_share_form_name; ?></td>
														<td><?php echo $value->condo_share_form_phone; ?></td>
														<td><?php echo $value->condo_share_form_email; ?></td>
														<td><?php echo $value->condo_share_form_message; ?></td>
														<td>
															<span class="label label-default"><?php echo $this->Configsystem->get_type_status_title($type_status_id); ?></span>
														</td>
														<td>

															<a href="#" class="btn btn-success waves-effect waves-light fk-type-status-id-modal" data-toggle="modal" data-target="#fk-type-status-id-modal<?php echo $i; ?>"><i class="zmdi zmdi-assignment-o"></i></a>

					<div id="fk-type-status-id-modal<?php echo $i; ?>" class="modal fade fk-type-status-id-modal<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<a href="#" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</a>
									<h4 class="modal-title">แก้ไขสถานะการแชร์</h4>
								</div>
								<div class="modal-body">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label for="field-1" class="control-label">สถานะ</label>
												<select id="fk_type_status_id" class="form-control fk_type_status_id<?php echo $condo_share_form_id; ?>" name="fk_type_status_id" >
													<option value="0">----เลือก----</option>
													<?php
														foreach ($data['type_status'] as $row)
														{
															$sel = ($row->type_status_id == $type_status_id)? 'selected': '';
														?>
														<option value="<?php echo $row->type_status_id; ?>" <?php echo $sel; ?>>
															<?php echo $row->type_status_title; ?>
														</option>
														<?php
														}//END FOREACH
														?>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<a href="#" type="button" class="btn btn-default waves-effect" data-dismiss="modal">ปิด</a>
									<a href="#" type="button" id="<?php echo $condo_share_form_id; ?>" class="btn btn-modal btn-info waves-effect waves-light">บันทึก</a>
									<input type="hidden" name="fk_condo_id" id="fk_condo_id" value="<?php echo $fk_condo_id; ?>">
									<input type="hidden" name="fk_member_id" id="fk_member_id" value="<?php echo $fk_member_id; ?>">
								</div>
							</div>
						</div>
					</div>
					<!-- /.modal -->

														</td>
													</tr>
													<?php ++$i; }//END FOREACH#1 ?>
											 <?php }
												else
												{
												?>
												<tr>
												<td colspan="6">ไม่พบข้อมูล</td>
												</tr>
											 <?php } ?>
											</tbody>
											<!--
									<tfoot>
										<tr>
											<th data-priority="2">ID</th>
											<th>Name</th>
											<th data-priority="1">Username</th>
											<th data-priority="2">Group</th>
											<th data-priority="3">Last Login</th>
											<th data-priority="2">Manage</th>
										</tr>
									</tfoot>
-->
										</table>


						</div>
					</div>
				</div>
				<!-- End row -->

			</div>
			<!-- container -->

		</div>
		<!-- content -->


	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>


	<script>
		$(document).ready(function () {


			$('.btn-modal').click( function(event) {
				var id = $(this).attr("id");
				//var fk_type_status_id = $('.fk_type_status_id').val();
				var fk_member_id = $("#fk_member_id").val();
				var fk_type_status_id = $(".fk_type_status_id"+id).val();
				var fk_condo_id = $("#fk_condo_id").val();
				var action = 'form-share';
							// console.log(fk_type_status_id);
				swal({
					title: "Warning Data!",
					text: "Confirm you update",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, Update it!",
					cancelButtonText: "No, Cancel plx!",
					closeOnConfirm: false,
					closeOnCancel: false,
				}, function (isConfirm) {
					if (isConfirm) {
						$.ajax({
							url: "<?php echo base_url('condominium/edit_modal_status_share_form/'); ?>",
							type: "POST",
							data: {
								"condo_share_form_id" : id,
								"fk_member_id" : fk_member_id,
								"fk_type_status_id" : fk_type_status_id,
								"fk_condo_id" : fk_condo_id,
								"action" : action
							},
							cache: false,
							success: function (data) {
								console.log(data);
	//										table.ajax.reload();
								swal.close();
								//Custombox.close();
								window.location.reload();
							},
							error: function (xhr, desc, err) {
								console.log(err);
							}
						});
						return false;
					} else {
						swal.close();
						return false;
					}
				});
				event.preventDefault();
				return false;
			});
		});
	</script>
