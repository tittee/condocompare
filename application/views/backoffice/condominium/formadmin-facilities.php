<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

//var_dump($data['row']);
if(isset( $data['row'] ))
{
	$facilities_id 								= $data['row']->facilities_id;
	$facilities_title 						= $data['row']->facilities_title;
	$facilities_title_en 					= $data['row']->facilities_title_en;
}
else
{
	$facilities_title 				= "";
	$facilities_title_en			= "";
}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">

							<div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
									<i class="zmdi zmdi-more-vert"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</div>

							<h4 class="header-title m-t-0 m-b-30">Form details</h4>


							<div class="row">
								<div class="col-lg-12">
									<?php if(isset( $data['row'] ))
									{
								?>
										<form action="<?php echo site_url('condominium/edit_data_facilities'); ?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
											<input type="hidden" name="facilities_id" value="<?php echo $facilities_id; ?>">
											<?php }
									else
									{
								?>
												<form action="<?php echo site_url('condominium/add_data_facilities'); ?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
													<?php } ?>
														<div class="form-group">
															<label class="col-md-2 control-label">Facilities Title</label>
															<div class="col-md-10">
																<input type="text" name="facilities_title" class="form-control" value="<?php echo $facilities_title; ?>" placeholder="Facilities Title">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">Facilities Title &#40;EN&#41;</label>
															<div class="col-md-10">
																<input type="text" name="facilities_title_en" class="form-control" value="<?php echo $facilities_title_en; ?>" placeholder="Facilities Title En">
															</div>
														</div>
														<!-- end col -->


														<div class="form-group">
															<div class="col-sm-offset-2 col-sm-10 m-t-15">
																<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light">
																	Submit
																</button>
																<a href="<?php echo site_url('condominium/view'); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">
													Cancel
												</a>
															</div>
														</div>

												</form>
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->


	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
