<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

//var_dump($data['row']);
if(isset( $data['row'] ))
{
	$action = 'edit';
	$mass_transportation_category_id 								= $data['row']->mass_transportation_category_id;
	$mass_transportation_category_title 						= $data['row']->mass_transportation_category_title;
	$publish 													= $data['row']->publish;
}
else
{
	$action = 'add';
	$mass_transportation_category_title 				= "";
	$publish											= 2;
}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">

							<div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
									<i class="zmdi zmdi-more-vert"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</div>

							<h4 class="header-title m-t-0 m-b-30">Form details</h4>


							<div class="row">
								<div class="col-lg-12">
										<form id="frmdata" action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
											<?php if($action==='edit'){ 	?>
											<input type="hidden" name="mass_transportation_category_id" value="<?php echo $mass_transportation_category_id; ?>">
											<?php } ?>

														<div class="form-group">
															<label class="col-md-2 control-label">ชื่อสถานที่ใกล้เคียง</label>
															<div class="col-md-10">
																<input type="text" name="mass_transportation_category_title" class="form-control" value="<?php echo $mass_transportation_category_title; ?>" placeholder="" required="">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">การเผยแพร่</label>
															<div class="col-md-10">
																<div class="radio-inline radio radio-success radio-single">
																	<input type="radio" name="publish" id="publish1" value="1" <?php echo ($publish == 1)? 'checked' : ''; ?>>
																	<label for="">
																		ปิดการเผยแพร่
																	</label>
																</div>
																<div class="radio radio-success radio-single radio-inline">
																	<input type="radio" name="publish" id="publish2" value="2" <?php echo ($publish == 2)? 'checked' : ''; ?>>
																	<label class="">
																		เผยแพร่
																	</label>
																</div>


															</div>
														</div>
														<!-- end col -->


														<div class="form-group">
															<div class="col-sm-offset-2 col-sm-10 m-t-15">
																<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light" onclick="tinyMCE.triggerSave(true,true);">
																	Submit
																</button>
																<a href="<?php echo site_url('condominium/view_mass_transportation_category'); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">Cancel</a>
																<input type="hidden" name="action" value="action">
															</div>
														</div>

												</form>
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->

	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
<script type="text/javascript">
$("#frmdata").submit(function(event){

	var formData = new FormData($(this)[0]);

	<?php
	$ajax_url = ($action === 'add')? "condominium/add_mass_transportation_category" :  "condominium/edit_mass_transportation_category";
	$msg = ($action === 'add')? "Insert" :  "Update";
	?>
	var url = "<?php echo base_url().$ajax_url; ?>";

	$.ajax({
		url: url,
		type: "POST",
		data: formData,
		cache: false,
		processData: false,
		contentType: false,
		context: this,
		success: function (data, status)
		{
			console.log( data );
			swal({
				title: "Success Data!",
				text: "Confirm you save",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, <?php echo $msg; ?> it!",
				cancelButtonText: "No, cancel plx!",
				closeOnConfirm: true,
				closeOnCancel: true }, function(isConfirm){
				if (isConfirm)
				{
					window.location.replace('<?php echo base_url(); ?>condominium/view_mass_transportation_category');
				}
				else
				{
					window.location.reload();
				}
			});

		},
		error: function (xhr, desc, err)
		{
			console.log( err );
		},

	});
event.preventDefault();
	return false;
});
</script>
