<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');
?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">


							<a href="<?php echo $url_href ?>" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">
								<i class="zmdi zmdi-account-add"></i> ADD</a>

										<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>ลำดับ</th>
													<th>สถานที่ใกล้เคียง</th>
													<th>หมวดหมู่</th>
													<th>วันที่</th>
													<th>แสดงผล</th>
													<th>จัดการ</th>
												</tr>
											</thead>
											<tbody>
												<?php
									foreach($data as $row => $value)
									{//START FOREACH#1
										$id = $value->mass_transportation_id;
										$mass_transportation_title = $value->mass_transportation_title;
										$createdate = $value->createdate;
										$updatedate = $value->updatedate;
										$createdate_t = ($createdate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
										$updatedate_t = ($updatedate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($updatedate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
										$publish = $value->publish;

										$mass_transportation_category_id = (!empty($value->mass_transportation_category_id))? $value->mass_transportation_category_id : '';
										$mass_transportation_category_title = (!empty($mass_transportation_category_id) || $mass_transportation_category_id !== 0)? $this->M->get_name_by_id('mass_transportation_category_id', 'mass_transportation_category_title', $mass_transportation_category_id, 'mass_transportation_category') : '-';
										$mass_transportation_category_title_t = ( isset($mass_transportation_category_title[0]) )? $mass_transportation_category_title[0]['mass_transportation_category_title'] : '-';

									?>
													<tr>
														<th>
															<?php echo $value->mass_transportation_id; ?>
														</th>
														<td>
															<?php echo $value->mass_transportation_title; ?>
														</td>
														<td>
															<?php echo $mass_transportation_category_title_t; ?>
														</td>
														<td>
															สร้าง : <?php echo $createdate_t; ?><br>
															แก้ไข : <?php echo $updatedate_t; ?>
														</td>
														<td>
															<?php echo $this->primaryclass->get_publish($publish); ?>
														</td>
														<td>
															<input type="hidden" name="id" value="<?php echo $id; ?>">

															<?php if( $publish == 1){ //ไม่แสดงผล ต้องเปลี่ยนเป็นแสดงผล ?>
															<a href="#" class="btn btn-publish btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="<?php echo $id; ?>"><i class="zmdi zmdi-lock-open"></i></a>
															<?php } else {//แสดงผล ต้องเปลี่ยนเป็นไม่แสดงผล ?>
															<a href="#" class="btn btn-unpublish btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="<?php echo $id; ?>"><i class="zmdi zmdi-lock-outline"></i></a>
															<?php }//END IF publish ?>

															<a href="<?php echo site_url('condominium/edit_mass_transportation/'.$id); ?>" class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5"><i class="fa fa-pencil"></i></a>
															<a href="void:javascript(0);" id="<?php echo $id; ?>" class="btn-delete btn btn-icon waves-effect waves-light btn-danger m-b-5 m-r-5"><i class="fa fa-trash-o"></i></a>
														</td>
													</tr>
													<?php }//END FOREACH#1 ?>
											</tbody>

										</table>


						</div>
					</div>
				</div>
				<!-- End row -->

			</div>
			<!-- container -->

		</div>
		<!-- content -->


	</div>
	<!-- End content-page -->


	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
	<script>
		$(".btn-delete").click(function(event){
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you delete",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('condominium/delete_mass_transportation/'); ?>"+id,
						type: "POST",
						data: id,
						cache:false,
						success: function (data)
						{
							//console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				event.preventDefault();
				return false;
			});

		});

		$(".btn-publish").click(function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you publish",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, publish it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('condominium/mass_transportation_publish/'); ?>",
						type: "POST",
						data: {
							action : 'publish',
							mass_transportation_id : id,
							publish : '2',
						},
						cache:false,
						success: function (data)
						{
								// console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				return false;
			});
		});

		$(".btn-unpublish").click(function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you unpublish",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Unpublish it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('condominium/mass_transportation_publish/'); ?>",
						type: "POST",
						data: {
							action : 'unpublish',
							mass_transportation_id : id,
							publish : '1',
						},
						cache:false,
						success: function (data)
						{
	//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}

				return false;
			});
		});

	</script>
