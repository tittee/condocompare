<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

//var_dump($data['row']);
if(isset( $data['row'] ))
{
	$featured_id 								= $data['row']->featured_id;
	$featured_title 						= $data['row']->featured_title;
	$featured_title_en 					= $data['row']->featured_title_en;
}
else
{
	$featured_title 				= "";
	$featured_title_en			= "";
}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">

							<div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
									<i class="zmdi zmdi-more-vert"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</div>

							<h4 class="header-title m-t-0 m-b-30">Form details</h4>


							<div class="row">
								<div class="col-lg-12">
									<?php if(isset( $data['row'] ))
									{
								?>
										<form action="<?php echo site_url('condominium/edit_data_featured'); ?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
											<input type="hidden" name="featured_id" value="<?php echo $featured_id; ?>">
											<?php }
									else
									{
								?>
												<form action="<?php echo site_url('condominium/add_data_featured'); ?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
													<?php } ?>
														<div class="form-group">
															<label class="col-md-2 control-label">หัวข้อจุดเด่น</label>
															<div class="col-md-10">
																<input type="text" name="featured_title" class="form-control" value="<?php echo $featured_title; ?>" placeholder="Featured Title">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">หัวข้อจุดเด่น &#40;EN&#41;</label>
															<div class="col-md-10">
																<input type="text" name="featured_title_en" class="form-control" value="<?php echo $featured_title_en; ?>" placeholder="Featured Title En">
															</div>
														</div>
														<!-- end col -->


														<div class="form-group">
															<div class="col-sm-offset-2 col-sm-10 m-t-15">
																<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light">
																	Submit
																</button>
																<a href="<?php echo site_url('condominium/view'); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">
													Cancel
												</a>
															</div>
														</div>

												</form>
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->

	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
