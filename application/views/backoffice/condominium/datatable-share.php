<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');
?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">

							<div class="dropdown pull-right">
								<a href="#" class="btn btn-icon waves-effect waves-light btn-inverse m-b-5 m-r-5 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<i class="zmdi zmdi-more"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li class="divider"></li>
									<li><a href="#">เผยแพร่</a></li>
									<li><a href="#">ซ่อนเผยแพร่</a></li>
									<li><a href="#">ลบทั้งหมด</a></li>
								</ul>
							</div>

							<h4 class="header-title m-t-0 m-b-30 waves-effect w-md waves-light"><?php echo $title; ?></h4>

							<form action="" method="post">
							<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>ลำดับ</th>
										<th>ชื่อโครงการ</th>
										<!-- <th>สถานะ</th> -->
										<th>ผู้แชร์</th>
										<th>วันที่แชร์</th>
										<!-- <th>จัดการ</th> -->
									</tr>
								</thead>
								<tbody>
									<?php
									$j = 1;
									foreach($data['data'] as $row => $value)
									{//START FOREACH#1
										$i = 0;

										$member_share_condo_id = $value->member_share_condo_id;
										$condo_id = $value->condo_id;
										$type_status_id = $value->fk_type_status_id;

										$createdate = (!empty($value->createdate))? $value->createdate : '0000-00-00 00:00:00';
										//$sharedate = $value->sharedate;
										$createdate_t = ($createdate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
										//$sharedate_t = ($sharedate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($sharedate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';

										$member_share_fname = $this->M->get_name('member_id, member_fname', 'member_id', 'member_fname', $value->member_id, 'member');
									?>
										<tr>
											<th>
												<?php echo $j; ?>
											</th>
											<td>
												<?php echo $value->condo_title; ?>
											</td>
											<!-- <td>
												<span class="label label-default"><?php echo $this->Configsystem->get_type_status_title($type_status_id); ?></span>
											</td> -->
											<td>
												<?php
										if( !empty($member_share_fname[0]->member_fname) )
										{
										?>
											<a href="<?php echo site_url('condominium/share_with_contact/'.$value->condo_id.'/'.$value->member_id); ?>"><?php echo $member_share_fname[0]->member_fname;  ?></a>
													<?php
										}
										else
										{
										?>
														ไม่มีข้อมูล
														<?php
									}
												?>
											</td>
											<td>
												<?php echo $createdate_t; ?>
											</td>
											<!-- <td>
												<a href="#" class="btn btn-success waves-effect waves-light fk-type-status-id-modal" data-toggle="modal" data-target="#fk-type-status-id-modal<?php echo $j; ?>"><i class="zmdi zmdi-assignment-o"></i></a>

		<div id="fk-type-status-id-modal<?php echo $j; ?>" class="modal fade fk-type-status-id-modal<?php echo $j; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<a href="#" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</a>
						<h4 class="modal-title">แก้ไขสถานะการแชร์</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="field-1" class="control-label">สถานะ</label>
									<select id="fk_type_status_id" class="form-control fk_type_status_id<?php echo $member_share_condo_id; ?>" name="fk_type_status_id" >
										<option value="0">----เลือก----</option>
										<?php
											foreach ($data['type_status'] as $row)
											{
												$sel = ($row->type_status_id == $type_status_id)? 'selected': '';
											?>
											<option value="<?php echo $row->type_status_id; ?>" <?php echo $sel; ?>>
												<?php echo $row->type_status_title; ?>
											</option>
											<?php
											}//END FOREACH
											?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<a href="#" type="button" class="btn btn-default waves-effect" data-dismiss="modal">ปิด</a>
						<a href="#" type="button" id="<?php echo $member_share_condo_id; ?>" class="btn btn-modal btn-info waves-effect waves-light">บันทึก</a>
						<input type="hidden" name="condo_id" id="condo_id" value="<?php echo $condo_id; ?>">
					</div>
				</div>
			</div>
		</div>

											</td> -->
										</tr>
										<?php ++$j;}//END FOREACH#1 ?>
								</tbody>
								<tfoot>
									<tr>
										<th colspan="6"><p class="text-default m-b-15 font-13">แก้ไขรายการสถานะให้ทำการเลือกสถานะแล้ว กดปุ่ม <code>ยืนยันแก้ไขสถานะ</code> </p></th>
									</tr>
								</tfoot>

							</table>
							</form>


						</div>
					</div>
				</div>
				<!-- End row -->

			</div>
			<!-- container -->

		</div>
		<!-- content -->


		<!-- ============================================================== -->
		<!-- End Right content here -->
		<!-- ============================================================== -->
		<?php $this->load->view('backoffice/partials/footer')?>
			<script>
				$(document).ready(function () {

					$('.btn-modal').click( function(event) {
						var id = $(this).attr("id");
						//var fk_type_status_id = $('.fk_type_status_id').val();
						var fk_type_status_id = $(".fk_type_status_id"+id).val();
						var condo_id = $("#condo_id").val();
						var action = 'form-share';
	//						console.log(fk_type_status_id);
						swal({
							title: "Warning Data!",
							text: "Confirm you delete",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Yes, Delete it!",
							cancelButtonText: "No, Cancel plx!",
							closeOnConfirm: false,
							closeOnCancel: false,
						}, function (isConfirm) {
							if (isConfirm) {
								$.ajax({
									url: "<?php echo base_url('condominium/edit_modal_status_share/'); ?>",
									type: "POST",
									data: {
										"member_share_condo_id" : id,
										"fk_type_status_id" : fk_type_status_id,
										"condo_id" : condo_id,
										"action" : action
									},
									cache: false,
									success: function (data) {
										console.log(data);
//										table.ajax.reload();
										swal.close();
										//Custombox.close();
										window.location.reload();
									},
									error: function (xhr, desc, err) {
										console.log(err);
									}
								});
								return false;
							} else {
								swal.close();
								return false;
							}
						});
						event.preventDefault();
						return false;
					});
				});
			</script>

			<script>
				$(".btn-delete").click(function (event) {
					var id = $(this).attr("id");
					swal({
						title: "Warning Data!",
						text: "Confirm you delete",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Yes, Delete it!",
						cancelButtonText: "No, Cancel plx!",
						closeOnConfirm: false,
						closeOnCancel: false
					}, function (isConfirm) {
						if (isConfirm) {
							$.ajax({
								url: "<?php echo base_url('condominium/delete_condo/'); ?>" + id,
								type: "POST",
								data: id,
								cache: false,
								success: function (data) {
									//console.log(data);
									window.location.reload();
								},
								error: function (xhr, desc, err) {
									console.log(err);
								}
							});
							return false;
						} else {
							swal.close();
							return false;
						}
						event.preventDefault();
						return false;
					});
				});

				$(".btn-approved").click(function (event) {
					event.preventDefault();
					var id = $(this).attr("id");
					swal({
						title: "Warning Data!",
						text: "Confirm you Approved",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Yes, approved it!",
						cancelButtonText: "No, Cancel plx!",
						closeOnConfirm: false,
						closeOnCancel: false
					}, function (isConfirm) {
						if (isConfirm) {
							$.ajax({
								url: "<?php echo base_url('condominium/approved_condo/'); ?>",
								type: "POST",
								data: {
									action: 'approved',
									condo_id: id,
									approved: '2',
								},
								cache: false,
								success: function (data) {
									//							console.log(data);
									window.location.reload();
								},
								error: function (xhr, desc, err) {
									console.log(err);
								}
							});
							return false;
						} else {
							swal.close();
							return false;
						}
						return false;
					});
				});

				$(".btn-unapproved").click(function (event) {
					event.preventDefault();
					var id = $(this).attr("id");
					swal({
						title: "Warning Data!",
						text: "Confirm you unapproved",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Yes, Unapproved it!",
						cancelButtonText: "No, Cancel plx!",
						closeOnConfirm: false,
						closeOnCancel: false
					}, function (isConfirm) {
						if (isConfirm) {
							$.ajax({
								url: "<?php echo base_url('condominium/approved_condo/'); ?>",
								type: "POST",
								data: {
									action: 'unapproved',
									condo_id: id,
									approved: '1',
								},
								cache: false,
								success: function (data) {
									//							console.log(data);
									window.location.reload();
								},
								error: function (xhr, desc, err) {
									console.log(err);
								}
							});
							return false;
						} else {
							swal.close();
							return false;
						}

						return false;
					});
				});
			</script>
