<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');
?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">



				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">

							<form class="" action="" name="form_admin" method="post">
							<!-- <div class="row">
							<div class="form-group">
								<label class="control-label col-sm-2">ช่วงเวลา</label>
								<div class="col-sm-10">
									<div class="input-daterange input-group" id="date-range">
										<input type="text" class="form-control" name="start">
										<span class="input-group-addon bg-primary b-0 text-white">to</span>
										<input type="text" class="form-control" name="end">
									</div>
								</div>
							</div>
							</div> -->
							<!-- <hr class="clearfix"> -->

										<table id="datatable-notification" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">

											<thead>
												<tr>
													<th>ลำดับ</th>
													<th>โครงการ</th>
													<th>ชื่อผู้ติดต่อ</th>
													<th>เบอร์</th>
													<th>อีเมล์</th>
													<th>ข้อความ</th>
													<th>สถานะ</th>
													<th>จัดการ</th>
												</tr>
											</thead>
											<tbody>

											</tbody>
											<!--
									<tfoot>
										<tr>
											<th data-priority="2">ID</th>
											<th>Name</th>
											<th data-priority="1">Username</th>
											<th data-priority="2">Group</th>
											<th data-priority="3">Last Login</th>
											<th data-priority="2">Manage</th>
										</tr>
									</tfoot>
-->
										</table>
							</form>

						</div>
					</div>
				</div>
				<!-- End row -->

			</div>
			<!-- container -->

		</div>
		<!-- content -->


	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>


	<script>
		$(document).ready(function () {


			var oTable = $('#datatable-notification').DataTable( {
				"order": [], //Initial no order.
				"processing": true,
				"serverSide": true,
				// "pageLength": 50,
  			"pagingType": "full_numbers",
				"responsive": true,
				"lengthMenu": [[ 50, 100, 150, 200, -1], [ 50, 100, 150, 200, "All"]],
				"ajax": {
						"url": "<?php echo site_url('condominium/json_notification_condo') ?>",
						"type": "POST",
				},


			"columnDefs": [
				{
					"targets": 0,
	        "orderable": false
				},

					]
			});


			jQuery('#date-range').datepicker({
					toggleActive: true,
					"onSelect": function(date) {
			      minDateFilter = new Date(date).getTime();
			      oTable.fnDraw();
			    }
	  	}).keyup(function() {
	    	minDateFilter = new Date(this.value).getTime();
	    	oTable.fnDraw();
			});




  // $("#datepicker_from").datepicker({
  //   showOn: "button",
  //   buttonImage: "images/calendar.gif",
  //   buttonImageOnly: false,
	//     "onSelect": function(date) {
	//       minDateFilter = new Date(date).getTime();
	//       oTable.fnDraw();
	//     }
	//   }).keyup(function() {
	//     minDateFilter = new Date(this.value).getTime();
	//     oTable.fnDraw();
	//   });





		});
	</script>
