<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');
?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">


							<a href="<?php echo $url_href ?>" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">
								<i class="zmdi zmdi-account-add"></i> ADD</a>

										<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th class="not-mobile">ลำดับ</th>
													<th>รูป</th>
													<th class="not-mobile">ชื่อ</th>
													<th class="not-mobile">วันที่สร้าง</th>
													<th>จัดการ</th>
												</tr>
											</thead>
											<tbody>
												<?php
	$i = 1;
								 $condo_id = $this->uri->segment(3);
									foreach($data as $row => $value)
									{//START FOREACH#1
										$id = $value->condo_images_id;
										$publish = $value->publish;
										$sfilepath = base_url().'uploads/condominium/';
										$pic_large = $sfilepath.$value->pic_large;
										$createdate = $value->createdate;
										$updatedate = $value->updatedate;
										$createdate_t = ($createdate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
										$updatedate_t = ($updatedate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($updatedate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
									?>
													<tr>
														<th>
															<?php echo $i; ?>
														</th>
														<td><img src="<?php echo $pic_large;?>" style="width: 150px;" class="img-thumbnail" alt=""></td>
														<td>
															<?php echo $value->condo_images_title; ?>
														</td>
														<td>
															วันที่สร้าง : <?php echo $createdate_t; ?><br>
															วันที่แก้ไข : <?php echo $updatedate_t; ?><br>
															แสดงผล : <?php echo $this->primaryclass->get_publish($publish); ?>
														</td>
														<td>
															<input type="hidden" name="id" value="<?php echo $value->condo_images_id; ?>">
															<a href="<?php echo site_url('condominium/edit_image/'.$condo_id.'/'.$value->condo_images_id); ?>" class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5"><i class="fa fa-pencil"></i></a>

															<?php if( $publish == 1){ //ไม่แสดงผล ต้องเปลี่ยนเป็นแสดงผล ?>
															<a href="#" class="btn btn-publish btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="<?php echo $id; ?>"><i class="zmdi zmdi-globe-alt"></i></a>
															<?php } else {//แสดงผล ต้องเปลี่ยนเป็นไม่แสดงผล ?>
															<a href="#" class="btn btn-unpublish btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="<?php echo $id; ?>"><i class="zmdi zmdi-globe-lock"></i></a>
															<?php }//END IF PUBLISH ?>

															<a href="#" id="<?php echo $id; ?>" class="btn btn-delete btn-icon waves-effect waves-light btn-danger m-b-5 m-r-5"><i class="ti-trash"></i></a>
														</td>
													</tr>
													<?php $i++; }//END FOREACH#1 ?>
											</tbody>
										</table>


						</div>
					</div>
				</div>
				<!-- End row -->

			</div>
			<!-- container -->

		</div>
		<!-- content -->

	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
<script>
		$(".btn-delete").click(function(event){
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you delete",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('condominium/delete_image/'); ?>"+id,
						type: "POST",
						data: id,
						cache:false,
						success: function (data)
						{
							//console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				event.preventDefault();
				return false;
			});

		});

		$(".btn-publish").click(function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you publish",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Publish it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('condominium/display_condo_images/'); ?>"+id,
						type: "POST",
						data: {
							condo_images_id : id,
							publish : '1',
						},
						cache:false,
						success: function (data)
						{
//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				return false;
			});
		});

		$(".btn-unpublish").click(function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you unpublish",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Unpublish it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('condominium/undisplay_condo_images/'); ?>"+id,
						type: "POST",
						data: {
							condo_images_id : id,
							publish : '1',
						},
						cache:false,
						success: function (data)
						{
//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}

				return false;
			});
		});

	</script>
