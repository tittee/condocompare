<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');
$condo_id =  $this->uri->segment(3);
$id =  $this->uri->segment(4);
if(isset( $data['row'] ))
{
	$action 									= 'edit';
//	$condo_id 								= $data['row'][0]->condo_id;
	$condo_id 								= $data['row'][0]->condo_id;
	$condo_images_id 					= $data['row'][0]->condo_images_id;
	$condo_images_title 			= $data['row'][0]->condo_images_title;
	$condo_images_description = trim($data['row'][0]->condo_images_description);
	$pic_thumb 								= $data['row'][0]->pic_thumb;
	$pic_large 								= $data['row'][0]->pic_large;
	$publish 									= $data['row'][0]->publish;
}
else
{
	$action 									= 'add';
	$condo_images_title 			= "";
	$condo_images_description = "";
	$pic_thumb 								= "";
	$pic_large 								= "";
	$publish 									= "2";
}
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">

						<?php if( $action === 'edit')
									{
								?>
							<form action=""  id="frmdata" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
								<input type="hidden" name="condo_id" id="condo_id" value="<?php echo $condo_id; ?>">
								<input type="hidden" name="condo_images_id" id="condo_images_id" value="<?php echo $condo_images_id; ?>">
								<input type="hidden" id="old_pic_thumb" name="old_pic_thumb" value="<?php echo $pic_thumb; ?>">
								<input type="hidden" id="old_pic_large" name="old_pic_large" value="<?php echo $pic_large; ?>">
								<div class="card-box">
									<h4 class="header-title m-t-0 m-b-30">รายละเอียดย่อย / รูปภาพ</h4>
									<div class="row">
										<div class="col-lg-12">

											<div class="form-group">
												<label class="col-md-2 control-label" for="pic_large">รูปภาพขนาดใหญ่</label>
												<div class="col-md-4">
													<input type="file" name="pic_large"
														id="pic_large"
														class="dropify"
														data-max-file-size="1M"
														data-default-file="<?php echo base_url()?>uploads/condominium/<?php echo $pic_large; ?>" />
														<span class="help-block"><small>ขนาดรูปภาพไม่เกิน 2 MB และ <span class="label label-warning"> กว้าง : 900px X ยาว : 500px </span></small></span>
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-2 control-label">ชื่อรูป </label>
												<div class="col-md-10">
													<input type="text" name="condo_images_title" class="form-control" value="<?php echo $condo_images_title; ?>" placeholder="">
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-2 control-label">คำอธิบายรูป </label>
												<div class="col-md-10">
													<textarea name="condo_images_description" class="form-control" id="condo_images_description" cols="30" rows="2"><?php echo $condo_images_description; ?></textarea>
												</div>
											</div>

										</div>
										<!-- end col -->
									</div>
									<!-- end row -->
								</div>
								<!-- end box -->

								<div class="card-box">
									<h4 class="header-title m-t-0 m-b-30">ส่วนการแสดงผล</h4>
									<div class="row">
										<div class="col-lg-12">

											<div class="form-group">
												<label class="col-sm-2 control-label">แสดงผล</label>
												<div class="col-sm-10">
													<div class="radio radio-pink">
														<input id="publish1" value="2" name="publish" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" <?php echo ($publish==2)? 'checked' : ''; ?>>
														<label for="publish1"> เผยแพร่ </label>
													</div>

													<div class="radio radio-pink">
														<input id="publish2" value="1" name="publish" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" required="" <?php echo ($publish==1 )? 'checked' : ''; ?>>
														<label for="publish2"> ไม่เผยแพร่ </label>
													</div>
												</div>
											</div>

										</div>
										<!-- end col -->
									</div>
									<!-- end row -->
								</div>
								<!-- end box -->

								<div class="card-box">
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group">
												<div class="col-sm-offset-2 col-sm-10 m-t-15">
													<input type="hidden" name="action" value="action">
													<input type="hidden" name="condo_id" id="condo_id" value="<?php echo $condo_id; ?>">
													<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light">
														Submit
													</button>
													<a href="<?php echo site_url('condominium/images/'.$condo_id); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">Cancel</a>
												</div>
											</div>
										</div>
										<!-- end col -->
									</div>
									<!-- end row -->
								</div>
								<!-- end box -->

							</form>
							<!-- end form -->

								<?php
									}
									else
									{
								?>
							<form action="" id="frmdata" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">

							<?php
								for($i=1; $i<=5; $i++)
								{
								?>
								<div class="card-box">
									<h4 class="header-title m-t-0 m-b-30">รายละเอียดย่อย / รูปภาพ</h4>
									<div class="row">
										<div class="col-lg-12">

											<div class="form-group">
												<label class="col-md-2 control-label" for="pic_large_<?php echo $i;?>">รูปภาพขนาดใหญ่</label>
												<div class="col-md-4">
													<input type="file" name="pic_large_<?php echo $i;?>"
														id="pic_large<?php echo $i;?>"
														class="dropify"
														data-max-file-size="1M"
														data-default-file="<?php echo base_url()?>uploads/condominium/<?php echo $pic_large; ?>" />
														<span class="help-block"><small>ขนาดรูปภาพไม่เกิน 2 MB และ <span class="label label-warning"> กว้าง : 900px X ยาว : 500px </span></small></span>
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-2 control-label">ชื่อรูป <?php echo $i;?></label>
												<div class="col-md-10">
													<input type="text" name="condo_images_title<?php echo $i;?>" class="form-control" value="<?php echo $condo_images_title; ?>" placeholder="">
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-2 control-label">คำอธิบายรูป <?php echo $i;?></label>
												<div class="col-md-10">
													<textarea name="condo_images_description<?php echo $i;?>" class="form-control" id="condo_images_description" cols="30" rows="2"><?php echo $condo_images_description; ?></textarea>
												</div>
											</div>

										</div>
										<!-- end col -->
									</div>
									<!-- end row -->
								</div>
								<!-- end box -->
								<?php } ?>

								<div class="card-box">
									<h4 class="header-title m-t-0 m-b-30">ส่วนการแสดงผล</h4>
									<div class="row">
										<div class="col-lg-12">

											<div class="form-group">
												<label class="col-sm-2 control-label">แสดงผล</label>
												<div class="col-sm-10">
													<div class="radio radio-pink">
														<input id="publish1" value="2" name="publish" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" <?php echo ($publish==2)? 'checked' : ''; ?>>
														<label for="publish1"> เผยแพร่ </label>
													</div>

													<div class="radio radio-pink">
														<input id="publish2" value="1" name="publish" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" required="" <?php echo ($publish==1 || $publish=='' )? 'checked' : ''; ?>>
														<label for="publish2"> ไม่เผยแพร่ </label>
													</div>
												</div>
											</div>

										</div>
										<!-- end col -->
									</div>
									<!-- end row -->
								</div>
								<!-- end box -->

								<div class="card-box">
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group">
												<div class="col-sm-offset-2 col-sm-10 m-t-15">
													<input type="hidden" name="action" value="action">
													<input type="hidden" name="condo_id" value="<?php echo $condo_id; ?>">
													<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light">
														Submit
													</button>
													<a href="<?php echo site_url('condominium/images/'.$condo_id); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">Cancel</a>
												</div>
											</div>
										</div>
										<!-- end col -->
									</div>
									<!-- end row -->
								</div>
								<!-- end box -->
							<?php } ?>


							<!-- end form -->
							</form>
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->



	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>


<script type="text/javascript">

	$("form#frmdata").submit(function(event){
		var formData = new FormData($(this)[0]);

		<?php
		$ajax_url = ($action === 'add')? "condominium/add_image" :  "condominium/edit_image/".$condo_id."/".$id;
		$msg = ($action === 'add')? "Insert" :  "Update";
		?>
		var url = "<?php echo base_url().$ajax_url; ?>";

		var formData = new FormData($(this)[0]);
		$.ajax({
			url: url,
			type: "POST",
			data: formData,
			cache: false,
			processData: false,
			contentType: false,
			context: this,
			success: function (data, status)
			{
				console.log( data );
				swal({
					title: "Success Data!",
					text: "Confirm you save",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, <?php echo $msg; ?> it!",
					cancelButtonText: "No, cancel plx!",
					closeOnConfirm: true,
					closeOnCancel: true }, function(isConfirm){
					if (isConfirm)
					{
						window.location.replace('<?php echo base_url("condominium/images/$condo_id"); ?>');
					}
					else
					{
						window.location.reload();
					}
				});

			},
			error: function (xhr, desc, err)
			{
				console.log( err );
			},

		});
		event.preventDefault();
		return false;
	});

	$(".delete_transportation").click(function(event){

		<?php
		$ajax_url = "condominium/delete_transportation_condominium";
		?>
		var url = "<?php echo base_url().$ajax_url; ?>";
		var id = $(this).attr('id');

		$.ajax({
			url: url,
			type: "POST",
			data: {id : id},
			success: function (data, status)
			{

				swal({
					title: "Success Data!",
					text: "Confirm you save",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, Delete it!",
					cancelButtonText: "No, cancel plx!",
					closeOnConfirm: true,
					closeOnCancel: true }, function(isConfirm){
					if (isConfirm)
					{
						$('.transportation'+id).fadeOut( "slow" );
					}
					else
					{
						window.location.reload();
					}
				});

			},
			error: function (xhr, desc, err)
			{
				console.log( err );
			},

		});
		event.preventDefault();
		return false;
	});


	var condo_images_id = $("#condo_images_id").val();
	$('.dropify').dropify({
		messages: {
				'default': 'Drag and drop a file here or click',
				'replace': 'Drag and drop or click to replace',
				'remove': 'Remove',
				'error': 'Ooops, something wrong appended.'
		},
		error: {
				'fileSize': 'The file size is too big (1M max).'
		}
	});

	var drEvent = $('.dropify').dropify();
	drEvent.on('dropify.beforeClear', function(event, element){
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('condominium/remove_condo_images_update'); ?>",
			data: {
				"condo_images_id": condo_images_id,
				"imagefile": element.file.name,
				"field_name": $(this).attr("name"),
			},
			success: function(response) {
				//$('#old_'+element.file.name).val(' ');
				console.log(response);
			}
		}); //END AJX
			//return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
		var r = confirm("Do you really want to delete \"" + element.file.name + "\" ?");
		if (r == true) {
			$( "#old_" + $('.dropify').attr('id') ).val( "" );
		} else {
			console.log('Cancel');
		}
	});

	var drEvent = $('.dropify').dropify();
	drEvent.on('dropify.afterClear', function(event, element){
			alert('File deleted');
	});
</script>
