<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');
?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">


							<a href="<?php echo $url_href ?>" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">
								<i class="zmdi zmdi-account-add"></i> ADD</a>

										<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>ลำดับ</th>
													<th>สถานที่ใกล้เคียง</th>
													<th>หมวดหมู่</th>
													<th>วันที่</th>
													<th>แสดงผล</th>
													<th>จัดการ</th>
												</tr>
											</thead>
											<tbody>
												<?php
									foreach($data as $row => $value)
									{//START FOREACH#1
									?>
													<tr>
														<th>
															<?php echo $value->transportation_id; ?>
														</th>
														<td>
															<?php echo $value->transportation_title; ?>
														</td>
														<td>
															<?php echo $this->Condominium_model->get_transportation_category_name($value->transportation_category_id); ?>
														</td>
														<td>
															<?php echo $this->dateclass->DateTimeShortFormat($value->createdate, 0, 1, "Th"); ?>
															<br>
															<?php echo $this->dateclass->DateTimeShortFormat($value->updatedate, 0, 1, "Th"); ?>
														</td>
														<td>
															<?php echo $this->primaryclass->get_publish($value->publish); ?>
														</td>
														<td>
															<input type="hidden" name="id" value="<?php echo $value->transportation_id; ?>">
															<a href="<?php echo site_url('condominium/edit_transportation/'.$value->transportation_id); ?>" class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5"><i class="fa fa-pencil"></i></a>
															<a href="<?php echo site_url('condominium/delete_transportation/'.$value->transportation_id); ?>" class="btn btn-icon waves-effect waves-light btn-danger m-b-5 m-r-5"><i class="fa fa-trash-o"></i></a>
														</td>
													</tr>
													<?php }//END FOREACH#1 ?>
											</tbody>

										</table>


						</div>
					</div>
				</div>
				<!-- End row -->

			</div>
			<!-- container -->

		</div>
		<!-- content -->


	</div>
	<!-- End content-page -->


	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
