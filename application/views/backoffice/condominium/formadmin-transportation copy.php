<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

//var_dump($data['row']);
if(isset( $data['row'] ))
{
	$transportation_id 								= $data['row']->transportation_id;
	$transportation_title 						= $data['row']->transportation_title;
	$transportation_category_id 					= $data['row']->transportation_category_id;
	$publish 													= $data['row']->publish;
}
else
{
	$transportation_title 				= "";
	$transportation_category_id			= "";
	$publish											= 0;
}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">

							<div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
									<i class="zmdi zmdi-more-vert"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</div>

							<h4 class="header-title m-t-0 m-b-30">Form details</h4>


							<div class="row">
								<div class="col-lg-12">
									<?php if(isset( $data['row'] ))
									{
								?>
										<form action="<?php echo site_url('condominium/edit_data_transportation'); ?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
											<input type="hidden" name="transportation_id" value="<?php echo $transportation_id; ?>">
											<?php }
									else
									{
								?>
												<form action="<?php echo site_url('condominium/add_data_transportation'); ?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
													<?php } ?>
														<div class="form-group">
															<label class="col-md-2 control-label">ชื่อสถานที่ใกล้เคียง</label>
															<div class="col-md-10">
																<input type="text" name="transportation_title" class="form-control" value="<?php echo $transportation_title; ?>" placeholder="">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">หมวดหมู่สถานที่ใกล้เคียง</label>
															<div class="col-md-10">
															<select id="transportation_category_id" class="form-control" name="transportation_category_id" onchange="(this)">
																	<option value="0">----เลือก----</option>
																	<?php
																			foreach ($transportation_category as $row) {//START FOREACH
																				$sel = ($row->transportation_category_id == $transportation_category_id)? 'selected': '';
																		?>
																		<option value="<?php echo $row->transportation_category_id; ?>" <?php echo $sel; ?>>
																			<?php echo $row->transportation_category_title; ?>
																		</option>
																		<?php
																			}//END FOREACH
																		?>
																</select>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">การเผยแพร่</label>
															<div class="col-md-10">
																<div class="radio-inline radio radio-success radio-single">
																	<input type="radio" name="publish" id="publish1" value="0" <?php echo ($publish == 0)? 'checked' : ''; ?>>
																	<label for="">
																		ปิดการเผยแพร่
																	</label>
																</div>
																<div class="radio radio-success radio-single radio-inline">
																	<input type="radio" name="publish" id="publish2" value="1" <?php echo ($publish == 1)? 'checked' : ''; ?>>
																	<label class="">
																		เผยแพร่
																	</label>
																</div>


															</div>
														</div>
														<!-- end col -->


														<div class="form-group">
															<div class="col-sm-offset-2 col-sm-10 m-t-15">
																<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light">
																	Submit
																</button>
																<a href="<?php echo site_url('condominium/view_transportation'); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">
													Cancel
												</a>
															</div>
														</div>

												</form>
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->

	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
