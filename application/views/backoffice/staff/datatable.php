<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'js'=>isset($js)?$js:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');
?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">


							<a href="<?php echo $url_href ?>" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">
								<i class="zmdi zmdi-account-add"></i> ADD</a>

							<div class="table-rep-plugin">
								<div class="table-responsive" data-pattern="priority-columns">
									<table id="datatable-responsive" class="table table-striped table-bordered display responsive nowrap" cellspacing="0" width="100%">
										<!--<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">-->
										<thead>
											<tr>
												<th data-priority="1">ลำดับ</th>
												<th data-priority="3">ชื่อ</th>
												<th data-priority="1">อีเมล์*</th>
												<th data-priority="3">กลุ่มผู้ใช้</th>
												<th data-priority="3">สถานะ</th>
												<th data-priority="3">วันที่เข้าสู่ระบบ</th>
												<th data-priority="6">จัดการ</th>
											</tr>
										</thead>
										<tbody>
											<?php
									$numrows = 1;
									foreach($data as $row => $value)
									{//START FOREACH#1
										$id = $value->staff_id;
                    $staff_group_id = $value->staff_group_id;
                    $type_staff_title = (!empty($staff_group_id))? $this->Staff_model->group_staff( $staff_group_id ) : '';
                    $type_staff_title_t = (!empty($type_staff_title))? $type_staff_title->type_staff_title : 'ไม่ระบุ';
										$approved = $value->approved;
									?>
												<tr>
													<th>
														<?php echo $numrows; ?>
													</th>
													<td>
														<?php echo $value->fullname; ?>
													</td>
													<td>
														<?php echo $value->username; ?>
													</td>
													<td>
														<?php echo $type_staff_title_t; ?>
													</td>
													<td>
														<?php echo $this->primaryclass->get_approved($approved); ?>
													</td>
													<td>
														<?php
											echo ($value->lastlogin != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($value->lastlogin, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
												?>
													</td>
													<td>
														<input type="hidden" name="id" value="<?php echo $id; ?>">

														<a href="<?php echo site_url('staff/edit_staff/'.$id); ?>" class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5"><i class="zmdi zmdi-edit"></i></a>

														<?php if( $approved == 1){ //ไม่แสดงผล ต้องเปลี่ยนเป็นแสดงผล ?>
														<a href="#" class="btn btn-approved btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="<?php echo $id; ?>"><i class="zmdi zmdi-lock-open"></i></a>
														<?php } else {//แสดงผล ต้องเปลี่ยนเป็นไม่แสดงผล ?>
														<a href="#" class="btn btn-unapproved btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="<?php echo $id; ?>"><i class="zmdi zmdi-lock-outline"></i></a>
														<?php }//END IF APPROVED ?>

														<a href="#" id="<?php echo $id; ?>" class="btn btn-delete btn-icon waves-effect waves-light btn-danger m-b-5 m-r-5"><i class="ti-trash"></i></a>
													</td>
												</tr>
												<?php $numrows++;}//END FOREACH#1 ?>
										</tbody>

									</table>
								</div>
							</div>


						</div>
					</div>
				</div>
				<!-- End row -->

			</div>
			<!-- container -->

		</div>
		<!-- content -->

	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
	<script>
		$(".btn-delete").click(function(event){
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you delete",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('staff/delete_staff/'); ?>"+id,
						type: "POST",
						data: id,
						cache:false,
						success: function (data)
						{
							//console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				event.preventDefault();
				return false;
			});

		});

		$(".btn-approved").click(function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you Approved",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, approved it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('staff/approved_staff/'); ?>"+id,
						type: "POST",
						data: {
							staff_id : id,
							approved : '1',
						},
						cache:false,
						success: function (data)
						{
//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				return false;
			});
		});

		$(".btn-unapproved").click(function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you unapproved",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Unapproved it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('staff/unapproved_staff/'); ?>"+id,
						type: "POST",
						data: {
							staff_id : id,
							approved : '1',
						},
						cache:false,
						success: function (data)
						{
//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}

				return false;
			});
		});

	</script>
