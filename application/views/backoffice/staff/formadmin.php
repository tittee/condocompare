<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'js'=>isset($js)?$js:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

if(isset( $data['row'] ))
{
	$action = 'edit';
	$staff_id = $data['row']->staff_id;
	$fullname = $data['row']->fullname;
	$username = $data['row']->username;
	$password = $data['row']->password;
	$pwd_disabled = 'disabled';
	$email 		= $data['row']->email;
	$phone 		= $data['row']->phone;
	$avatar 	= $data['row']->avatar;
	$approved 			= $data['row']->approved;
	$staff_group_id = $data['row']->staff_group_id;
}
else
{
	$action = 'add';
	$fullname = '';
	$username = '';
	$password = '';
	$email 		= '';
	$phone 		= '';
	$avatar 	= '';
	$pwd_disabled 	= '';
	$approved = '1';
	$staff_group_id = '';
}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">

							<h4 class="header-title m-t-0 m-b-30">Form details</h4>
							<div class="row">
								<div class="col-lg-12">
								<?php if(isset( $data['row'] ))
									{
								?>
									<form id="frmdata" action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
									<input type="hidden" name="staff_id" value="<?php echo $staff_id; ?>">
									<input type="hidden" name="old_avatar" value="<?php echo $avatar; ?>">
								<?php }
									else
									{
								?>
									<form id="frmdata" action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">

									<?php //echo form_open_multipart('staff/add_staff');?>
								<?php } ?>
										<div class="form-group">
											<label class="col-md-2 control-label">ชื่อ - นามสกุล</label>
											<div class="col-md-10">
												<input type="text" name="fullname" class="form-control" value="<?php echo $fullname; ?>" placeholder="Fullname">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label">ชื่อผู้ใช้งาน</label>
											<div class="col-md-10">
												<input type="email" id="example-email" name="username" class="form-control" value="<?php echo $username; ?>" placeholder="Username@harrison.co.th">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label">เบอร์โทรศัพท์</label>
											<div class="col-md-10">
												<input type="text" name="phone" class="form-control" value="<?php echo $phone; ?>" placeholder="">
											</div>
										</div>

										<?php
										if($action==='edit')
										{
											?>
										<div class="form-group">
											<label class="col-md-2 control-label">แก้ไขรหัสผ่าน</label>
											<div class="col-md-10">
												<div class="checkbox">
														<input id="chk_pwd" name="chk_pwd" type="checkbox" value="1" data-parsley-multiple="chk_pwd" data-parsley-id="13">
														<label for="chk_pwd"> เปลี่ยนรหัสผ่าน </label>
												</div>
											</div>
										</div>
										<?php
										}
										?>

										<div class="form-group">
											<label class="col-md-2 control-label">รหัสผ่าน</label>
											<div class="col-md-10">
												<input type="password" id="password" name="password" class="form-control" value="<?php echo $password; ?>" <?php echo $pwd_disabled; ?> placeholder="Password">
											</div>
										</div>

										<div class="form-group">
											<label class="col-md-2 control-label" for="example-email">อีเมล์</label>
											<div class="col-md-10">
												<input type="email" id="example-email" name="email" class="form-control" placeholder="Email" value="<?php echo $email; ?>">
											</div>
										</div>


										<div class="form-group">
											<label class="col-md-2 control-label" for="avatar">รูป</label>
											<div class="col-md-10">
												<input type="file" name="avatar" />
												<span class="help-block"><small>ขนาดรูปภาพไม่เกิน 2 MB และ <span class="label label-warning"> กว้าง : 200px X ยาว : 200px </span></small></span>
												<?php if(!empty($avatar))
														{
												?>
												<img src="<?php echo base_url()?>uploads/staff/<?php echo $avatar; ?>" class="img-responsive" alt="">
												<?php } ?>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">ระดับ</label>
											<div class="col-sm-10">
												<select class="form-control" name="staff_group_id">
												 	<option value="0"> ----เลือก----</option>
														<!--<option value="1" <?php echo ($staff_group_id==1)? 'selected' : ''; ?>>ผู้ดูแลระบบ</option>
													<option value="2" <?php echo ($staff_group_id==2)? 'selected' : ''; ?>>พนักงานขาย</option>
													<option value="3" <?php echo ($staff_group_id==3)? 'selected' : ''; ?>>เจ้าของเว็บไซต์</option> -->
													<?php
															foreach ($type_staff as $row) {//START FOREACH
																$sel = ($row->type_staff_id == $staff_group_id)? 'selected': '';
														?>
														<option value="<?php echo $row->type_staff_id; ?>" <?php echo $sel; ?>>
															<?php echo $row->type_staff_title; ?>
														</option>
														<?php
															}//END FOREACH
														?>

												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">อนุมัติ</label>
											<div class="col-sm-10">
												<div class="radio radio-pink">
													<input id="radio1" value="2" name="approved" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" <?php echo ($approved==2)? 'checked' : ''; ?>>
													<label for="radio1"> Approved </label>
												</div>

												<div class="radio radio-pink">
													<input id="radio2" value="1"  name="approved" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" required="" <?php echo ($approved==1 || $approved=='')? 'checked' : ''; ?>>
													<label for="radio2"> Unapproved </label>
												</div>
											</div>
										</div>

										<!-- end col -->

										<!--<div class="form-group">
											<label class="col-md-2 control-label">Text area</label>
											<div class="col-md-10">
												<textarea class="form-control" rows="5"></textarea>
											</div>
										</div>-->

										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10 m-t-15">
												<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light">
													Submit
												</button>
												<a href="<?php echo site_url('staff/staff'); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">
													Cancel
												</a>
											</div>
										</div>

									</form>
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->

	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>


<script>

	$(function() {
		$("#chk_pwd").on( "click", enable_pwd );
	});

	function enable_pwd() {
		if (this.checked) {
			$("#password").removeAttr("disabled");
		} else {
			$("#password").attr("disabled", true);
		}
	}

		$("form#frmdata").submit(function(event){
			var formData = new FormData($(this)[0]);
			<?php
			$ajax_url = ($action === 'add')? "staff/add_data_staff" :  "staff/edit_data_staff";
			$msg = ($action === 'add')? "Insert" :  "Update";
			?>
			var url = "<?php echo base_url().$ajax_url; ?>";
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				cache: false,
				processData: false,
				contentType: false,
				context: this,
				success: function (data, status)
				{
					console.log( data );
					swal({
						title: "Success Data!",
						text: "Confirm you save",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Yes, <?php echo $msg; ?> it!",
						cancelButtonText: "No, cancel plx!",
						closeOnConfirm: true,
						closeOnCancel: true }, function(isConfirm){
						if (isConfirm)
						{
							window.location.replace('<?php echo base_url(); ?>staff/staff');
						}
						else
						{
							window.location.reload();
						}
					});

				},
				error: function (xhr, desc, err)
				{
					console.log( err );
				},

			});
			event.preventDefault();
			return false;
		});
	</script>
