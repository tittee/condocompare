<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'js'=>isset($js)?$js:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

if(isset( $data['row'] ))
{
	$action = 'edit';
	$type_staff_id 								= $data['row']->type_staff_id;
	$type_staff_title 						= $data['row']->type_staff_title;

	$type_staff_detail 					= $data['row']->type_staff_detail;

	$publish 					= $data['row']->publish;
}
else
{
	$action = 'add';
	$type_staff_id 								= "";
	$type_staff_title 							= "";

	$type_staff_detail 						= "";

	$publish 					= "1";
}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">

						<?php if(isset( $data['row'] ))
									{
								?>
						<form id="frmdata" action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
							<input type="hidden" name="type_staff_id" value="<?php echo $type_staff_id; ?>">
							<?php }
									else
									{
								?>
						<form id="frmdata" action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
							<?php } ?>
						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">รายละเอียด</h4>
							<div class="row">
								<div class="col-lg-12">

									<div class="form-group">
										<label class="col-md-2 control-label">ชื่อ</label>
										<div class="col-md-10">
											<input type="text" name="type_staff_title" maxlength="150" class="inputmaxlength form-control" value="<?php echo $type_staff_title; ?>" maxlength="100" placeholder="">
										</div>
									</div>
									<!-- end from-group -->

									<div class="form-group">
										<label class="col-md-2 control-label">การแสดงผล</label>
										<div class="col-md-2">
											<div class="radio-inline radio radio-success radio-single">
												<input type="radio" name="publish" value="2" required <?php echo ($publish == 2)? 'checked': ''; ?>>
												<label for="">
													แสดงผล
												</label>
											</div>
										</div>

										<div class="col-md-2">
											<div class="radio-inline radio radio-success radio-single">
												<input type="radio" name="publish" value="1" required <?php echo ($publish == 1)? 'checked': ''; ?>>
												<label for="">
													ไม่แสดงผล
												</label>
											</div>
										</div>
									</div>
									<!-- end from-group -->

								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->

						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">รายละเอียด</h4>
							<div class="row">
								<div class="col-lg-12">



									<div class="form-group">
										<label class="col-md-2 control-label">รายละเอียด</label>
										<div class="col-md-10">
											<textarea class="wysiwigeditor" name="type_staff_detail"><?php echo $type_staff_detail; ?></textarea>
										</div>
									</div>
									<!-- end from-group -->

                  <div class="form-group">
                    <label class="col-md-2 control-label">หมวดหมู่สิทธิ์</label>
                    <div class="col-md-10">
                      <select multiple="multiple" class="multi-select" id="permissions_id" name="permissions_id[]" data-plugin="multiselect" data-selectable-optgroup="true">
                          <?php echo $data['display_permission_rel']; ?>
                      </select>
                    </div>
                  </div>
                  <!-- end from-group -->



								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->

						<div class="card-box">

							<div class="row">
								<div class="col-lg-12">

									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10 m-t-15">
											<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light" onclick="tinyMCE.triggerSave(true,true);">
												Submit
											</button>
											<a href="<?php echo site_url('staff/type_staff'); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">
													Cancel
												</a>
											<input type="hidden" name="action" value="action">
										</div>
									</div>

								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->

						<?php if(isset( $data['row'] ))
									{
								?>
						</form>
						<?php }
									else
									{
								?>
						</form>
						<?php } ?>
						<!-- end form -->
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->


	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
	<script>
		$("form#frmdata").submit(function(event){
			var formData = new FormData($(this)[0]);

			<?php
			$ajax_url = ($action === 'add')? "staff/add_type_staff" :  "staff/edit_type_staff/".$type_staff_id;
			$msg = ($action === 'add')? "Insert" :  "Update";
			?>
			var url = "<?php echo base_url().$ajax_url; ?>";

			var formData = new FormData($(this)[0]);

			swal({
				title: "Success Data!",
				text: "Confirm you save",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, <?php echo $msg; ?> it!",
				cancelButtonText: "No, cancel plx!",
				closeOnConfirm: true,
				closeOnCancel: true }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: url,
						type: "POST",
						data: formData,
						cache: false,
						processData: false,
						contentType: false,
						context: this,
						success: function (data, status)
						{
							window.location.replace('<?php echo base_url(); ?>staff/type_staff');
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						},

					});


				}
				else
				{
					window.location.reload();
				}
			});


			event.preventDefault();
			return false;
		});
	</script>
