<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'js'=>isset($js)?$js:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');
?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">


							<a href="<?php echo $url_href ?>" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">
								<i class="zmdi zmdi-account-add"></i> ADD</a>

									<table id="datatable-responsive" class="table table-striped table-bordered display responsive nowrap" cellspacing="0" width="100%">
										<!--<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">-->
										<thead>
											<tr>
												<th>ลำดับ</th>
												<th>ชื่อรายการ</th>
												<th>จัดการ</th>
											</tr>
										</thead>
										<tbody>
											<?php
									$numrows = 1;
									foreach($data['data'] as $row => $value)
									{//START FOREACH#1
										$id = $value->permissions_category_id;
										$permissions_category_title = $value->permissions_category_title;
									?>
												<tr>
													<th>
														<?php echo $numrows; ?>
													</th>
													<td>
														<?php echo $permissions_category_title; ?>
													</td>
                          <td>
                            <input type="hidden" name="id" value="<?php echo $id; ?>">
														<a href="<?php echo site_url('staff/update_permissions_category/'.$id); ?>" class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5"><i class="zmdi zmdi-edit"></i></a>
                          </td>
												</tr>
												<?php $numrows++;}//END FOREACH#1 ?>
										</tbody>

									</table>


						</div>
					</div>
				</div>
				<!-- End row -->

			</div>
			<!-- container -->

		</div>
		<!-- content -->

	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
	<script>
		$(".btn-delete").click(function(event){
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you delete",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('staff/delete_staff/'); ?>"+id,
						type: "POST",
						data: id,
						cache:false,
						success: function (data)
						{
							//console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				event.preventDefault();
				return false;
			});

		});

		$(".btn-approved").click(function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you Approved",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, approved it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('staff/approved_staff/'); ?>"+id,
						type: "POST",
						data: {
							permissions_id : id,
							approved : '1',
						},
						cache:false,
						success: function (data)
						{
//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				return false;
			});
		});

		$(".btn-unapproved").click(function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you unapproved",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Unapproved it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('staff/unapproved_staff/'); ?>"+id,
						type: "POST",
						data: {
							permissions_id : id,
							approved : '1',
						},
						cache:false,
						success: function (data)
						{
//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}

				return false;
			});
		});

	</script>
