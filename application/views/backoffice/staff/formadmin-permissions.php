<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'js'=>isset($js)?$js:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

if(isset( $data['row'] ))
{
	$action = 'edit';
	$permissions_id 								= $data['row']->permissions_id;
	$permissions_category_id 								= $data['row']->permissions_category_id;
	$permissions_title 						= $data['row']->permissions_title;
}
else
{
	$action = 'add';
  $permissions_id = "";
	$permissions_category_id 								= "";
	$permissions_title 							= "";

}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">

						<form id="frmdata" action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
              <?php if($action === 'edit') { ?>
                <input type="hidden" name="permissions_id" value="<?php echo $permissions_id; ?>">
							<?php } ?>
						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">รายละเอียด</h4>
							<div class="row">
								<div class="col-lg-12">

									<div class="form-group">
										<label class="col-md-2 control-label">ชื่อ</label>
										<div class="col-md-10">
											<input type="text" name="permissions_title" maxlength="150" class="inputmaxlength form-control" value="<?php echo $permissions_title; ?>" maxlength="100" placeholder="">
										</div>
									</div>
									<!-- end from-group -->

                  <div class="form-group">
                    <label class="col-md-2 control-label">หมวดหมู่สิทธิ์</label>
                    <div class="col-md-10">
                      <select id="permissions_category_id" class="form-control" name="permissions_category_id" onchange="(this)">
                        <option value="0">----เลือก----</option>
                        <?php
                            foreach ($data['permissions_category'] as $row) {//START FOREACH
                              $sel = ($row->permissions_category_id == $permissions_category_id)? 'selected': '';
                          ?>
                          <option value="<?php echo $row->permissions_category_id; ?>" <?php echo $sel; ?>>
                            <?php echo $row->permissions_category_title; ?>
                          </option>
                          <?php
                            }//END FOREACH
                          ?>
                      </select>
                    </div>
                  </div>
                  <!-- end from-group -->

								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->



						<div class="card-box">

							<div class="row">
								<div class="col-lg-12">

									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10 m-t-15">
											<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light" onclick="tinyMCE.triggerSave(true,true);">
												Submit
											</button>
											<a href="<?php echo site_url('staff/permissions'); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">
													Cancel
												</a>
											<input type="hidden" name="action" value="action">
										</div>
									</div>

								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->


						</form>

						<!-- end form -->
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->


	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
	<script>
		$("form#frmdata").submit(function(event){
			var formData = new FormData($(this)[0]);

			<?php
			$ajax_url = ($action === 'add')? "staff/create_permissions" :  "staff/update_permissions/".$permissions_id;
			$msg = ($action === 'add')? "Insert" :  "Update";
			?>
			var url = "<?php echo base_url().$ajax_url; ?>";

			var formData = new FormData($(this)[0]);

			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				cache: false,
				processData: false,
				contentType: false,
				context: this,
				success: function (data, status)
				{
					console.log( data );
					swal({
						title: "Success Data!",
						text: "Confirm you save",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Yes, <?php echo $msg; ?> it!",
						cancelButtonText: "No, cancel plx!",
						closeOnConfirm: true,
						closeOnCancel: true }, function(isConfirm){
						if (isConfirm)
						{
							window.location.replace('<?php echo base_url(); ?>staff/permissions');
						}
						else
						{
							window.location.reload();
						}
					});

				},
				error: function (xhr, desc, err)
				{
					console.log( err );
				},

			});
			event.preventDefault();
			return false;
		});
	</script>
