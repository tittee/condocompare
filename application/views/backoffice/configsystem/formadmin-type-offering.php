<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

//var_dump($data['row']);
if(isset( $data['row'] ))
{
	$type_offering_id 								= $data['row']->type_offering_id;
	$type_offering_title 						= $data['row']->type_offering_title;
	$type_offering_images 					= $data['row']->type_offering_images;
	$type_offering_detail 					= $data['row']->type_offering_detail;
	$publish 													= $data['row']->publish;
}
else
{
	$type_offering_title 				= "";
	$type_offering_images			= "";
	$type_offering_detail			= "";
	$publish											= 1;
}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">

							<div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
									<i class="zmdi zmdi-more-vert"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</div>

							<h4 class="header-title m-t-0 m-b-30">Form details</h4>


							<div class="row">
								<div class="col-lg-12">
									<?php if(isset( $data['row'] ))
									{
								?>
										<form action="<?php echo site_url('configsystem/edit_data_type_offering'); ?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
											<input type="hidden" name="type_offering_id" value="<?php echo $type_offering_id; ?>">
											<input type="hidden" id="old_type_offering_images" name="old_type_offering_images" value="<?php echo $type_offering_images; ?>">
											<?php }
									else
									{
								?>
												<form action="<?php echo site_url('configsystem/add_data_type_offering'); ?>" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
													<?php } ?>
														<div class="form-group">
															<label class="col-md-2 control-label">ชื่อประเภทการเสนอขาย</label>
															<div class="col-md-10">
																<input type="text" name="type_offering_title" class="form-control" value="<?php echo $type_offering_title; ?>" placeholder="">
															</div>
														</div>

														<div class="form-group">
															<label class="col-md-2 control-label" for="type_offering_images">รูปภาพ</label>
															<div class="col-md-4">
																<input type="file" name="type_offering_images"
																	id="type_offering_images"
																	class="dropify"
																	data-max-file-size="1M"
																	data-default-file="<?php echo (!empty($type_offering_images))? base_url().'uploads/config/'.$type_offering_images : '';?>" />
																	<span class="help-block"><small>ขนาดรูปภาพไม่เกิน 1 MB และ <span class="label label-warning"> กว้าง : 60px X ยาว : 60px </span></small></span>
															</div>
														</div>

														<div class="form-group">
															<label class="col-md-2 control-label">เงื่อนไขประเภทการเสนอขาย</label>
															<div class="col-md-10">
																<textarea id="type_offering_detail" name="type_offering_detail" class="form-control" maxlength="150" rows="2" placeholder=""><?php echo $type_offering_detail; ?></textarea>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">การเผยแพร่</label>
															<div class="col-md-10">
																<div class="radio-inline radio radio-success radio-single">
																	<input type="radio" name="publish" id="publish1" value="1" <?php echo ($publish == 1)? 'checked' : ''; ?>>
																	<label for="">
																		ปิดการเผยแพร่
																	</label>
																</div>
																<div class="radio radio-success radio-single radio-inline">
																	<input type="radio" name="publish" id="publish2" value="2" <?php echo ($publish == 2)? 'checked' : ''; ?>>
																	<label class="">
																		เผยแพร่
																	</label>
																</div>


															</div>
														</div>
														<!-- end col -->


														<div class="form-group">
															<div class="col-sm-offset-2 col-sm-10 m-t-15">
																<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light">
																	Submit
																</button>
																<a href="<?php echo site_url('configsystem/view_type_offering'); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">
													Cancel
												</a>
															</div>
														</div>

												</form>
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->

	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
