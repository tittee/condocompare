<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');
?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">


							<a href="<?php echo $url_href ?>" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">
								<i class="zmdi zmdi-account-add"></i> ADD</a>

										<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>ลำดับ</th>
													<th>การตกแต่ง</th>
													<th>วันที่</th>
													<th>แสดงผล</th>
													<th>จัดการ</th>
												</tr>
											</thead>
											<tbody>
												<?php
									foreach($data as $row => $value)
									{//START FOREACH#1
									?>
													<tr>
														<th>
															<?php echo $value->type_decoration_id; ?>
														</th>
														<td>
															<?php echo $value->type_decoration_title; ?>
														</td>
														<td>
															<?php echo $value->createdate; ?>
																<br>
																<?php echo $value->updatedate; ?>
														</td>
														<td>
															<?php echo $this->primaryclass->get_publish($value->publish); ?>
														</td>
														<td>
															<input type="hidden" name="id" value="<?php echo $value->type_decoration_id; ?>">
															<a href="<?php echo site_url('configsystem/edit_type_decoration/'.$value->type_decoration_id); ?>" class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5"><i class="fa fa-pencil"></i></a>
															<a href="<?php echo site_url('configsystem/delete_type_decoration/'.$value->type_decoration_id); ?>" class="btn btn-icon waves-effect waves-light btn-danger m-b-5 m-r-5"><i class="fa fa-trash-o"></i></a>
														</td>
													</tr>
													<?php }//END FOREACH#1 ?>
											</tbody>

										</table>


						</div>
					</div>
				</div>
				<!-- End row -->

			</div>
			<!-- container -->

		</div>
		<!-- content -->


	</div>
	<!-- End content-page -->


	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
