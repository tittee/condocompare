<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

//var_dump($data['row']);
if(isset( $data['row'] ))
{
	$action = 'edit';
	$type_property_id 								= $data['row']['type_property_id'];
	$type_property_title 						= $data['row']['type_property_title'];
	$type_property_detail 					= $data['row']['type_property_detail'];
	$publish 													= $data['row']['publish'];
}
else
{
	$action = 'add';
	$type_property_title 				= "";
	$type_property_detail			= "";
	$publish											= 1;
}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">

							<div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
									<i class="zmdi zmdi-more-vert"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</div>

							<h4 class="header-title m-t-0 m-b-30"><?php echo $title; ?></h4>


							<div class="row">
								<div class="col-lg-12">
									<form id="frmdata" name="frmdata" action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
										<?php if($action === 'edit'){ ?>
											<input type="hidden" name="type_property_id" value="<?php echo $type_property_id; ?>">
											<?php } ?>
												<div class="form-group">
													<label class="col-md-2 control-label">ชื่อประเภททรัพย์</label>
													<div class="col-md-10">
														<input type="text" name="type_property_title" class="form-control" value="<?php echo $type_property_title; ?>" placeholder="">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">เงื่อนไขประเภททรัพย์</label>
													<div class="col-md-10">
														<textarea id="type_property_detail" name="type_property_detail" class="form-control" maxlength="150" rows="2" placeholder=""><?php echo $type_property_detail; ?></textarea>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">การเผยแพร่</label>
													<div class="col-md-10">
														<div class="radio-inline radio radio-success radio-single">
															<input type="radio" name="publish" id="publish1" value="1" <?php echo ($publish==1 || $publish==='' )? 'checked' : ''; ?>>
															<label for="">
																ปิดการเผยแพร่
															</label>
														</div>
														<div class="radio radio-success radio-single radio-inline">
															<input type="radio" name="publish" id="publish2" value="2" <?php echo ($publish==2 )? 'checked' : ''; ?>>
															<label class="">
																เผยแพร่
															</label>
														</div>


													</div>
												</div>
												<!-- end col -->


												<div class="form-group">
													<div class="col-sm-offset-2 col-sm-10 m-t-15">
														<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light">
															ยืนยัน
														</button>
														<a href="<?php echo site_url('configsystem/view_type_property'); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">
													ยกเลิก
												</a>
												<input type="hidden" name="action" value="<?php echo $action ?>">
													</div>
												</div>

									</form>
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->

		<!-- ============================================================== -->
		<!-- End Right content here -->
		<!-- ============================================================== -->
		<?php $this->load->view('backoffice/partials/footer')?>
<script>

$(document).ready(function () {

	// Select2


	$("form#frmdata").submit(function(event){
		var formData = new FormData($(this)[0]);

		<?php
		$ajax_url = ($action === 'add')? "configsystem/create_type_property" :  "configsystem/edit_type_property";
		$msg = ($action === 'add')? "Insert" :  "Update";
		?>
		var url = "<?php echo base_url().$ajax_url; ?>";

		var formData = new FormData($(this)[0]);

		$.ajax({
			url: url,
			type: "POST",
			data: formData,
			cache: false,
			processData: false,
			contentType: false,
			context: this,
			success: function (data, status)
			{
//				console.log( data );
				swal({
					title: "Success Data!",
					text: "Confirm you save",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, <?php echo $msg; ?> it!",
					cancelButtonText: "No, cancel plx!",
					closeOnConfirm: true,
					closeOnCancel: true }, function(isConfirm){
					if (isConfirm)
					{
						window.location.replace('<?php echo base_url(); ?>configsystem/view_type_property');
					}
					else
					{
						window.location.reload();
					}
				});

			},
			error: function (xhr, desc, err)
			{
				console.log( err );
			},

		});
		event.preventDefault();
		return false;
	});
});

		</script>
