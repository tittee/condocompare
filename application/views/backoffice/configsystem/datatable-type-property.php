<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');
?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">

							<div class="dropdown pull-right">
								<a href="#" class="btn btn-icon waves-effect waves-light btn-inverse m-b-5 m-r-5 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<i class="zmdi zmdi-more"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="<?php echo site_url('configsystem/create_type_property') ?>"><i class="zmdi zmdi-plus"></i> เพิ่มรายการใหม่</a></li>
									<li class="divider"></li>
									<li><a href="#">เผยแพร่</a></li>
									<li><a href="#">ซ่อนเผยแพร่</a></li>
									<li><a href="#">ลบทั้งหมด</a></li>
								</ul>
							</div>

							<h4 class="header-title m-t-0 m-b-30 waves-effect w-md waves-light"><?php echo $title; ?></h4>


							<table id="datatable-responsive" class="table table-striped table-bordered display responsive nowrap" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th class="not-mobile">ลำดับ</th>
										<th>หัวข้อทรัพย์</th>
										<th class="not-mobile">วันที่สร้าง &#92; แก้ไข</th>
										<th class="not-mobile">แสดงผล</th>
										<th>จัดการ</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i = 1;
									foreach($data as $row => $value)
									{//START FOREACH#1
										$id = $value['type_property_id'];
										$pubilsh = $value['publish'];
										$pubilsh_t = ($value['publish'] == 2)? 'success' : 'danger';
										$createdate = $value['createdate'];
										$updatedate = $value['updatedate'];
										$createdate_t = ($createdate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
										$updatedate_t = ($updatedate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($updatedate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
									?>
										<tr>
											<th>
												<?php echo $i; ?>
											</th>
											<td>
												<?php echo $value['type_property_title']; ?><br />
												ID : <?php echo $id; ?>
											</td>
											<td>
												<?php echo $createdate_t; ?>
													<br>
													<?php echo $updatedate_t; ?>
											</td>
											<td>
												<span class="label label-<?php echo $pubilsh_t; ?>"><?php echo $this->primaryclass->get_publish($pubilsh); ?></span>
											</td>
											<td>
												<input type="hidden" name="id" value="<?php echo $id; ?>">
												<a href="<?php echo site_url('configsystem/edit_type_property/'.$id); ?>" class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5"><i class="zmdi zmdi-edit"></i></a>


												<?php if( $pubilsh == 1 ){ //ไม่แสดงผล ต้องเปลี่ยนเป็นแสดงผล ?>
													<a href="#" class="btn btn-publish btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="<?php echo $id; ?>"><i class="zmdi zmdi-globe-alt"></i></a>
												<?php } elseif( $pubilsh == 2){//แสดงผล ต้องเปลี่ยนเป็นไม่แสดงผล ?>
													<a href="#" class="btn btn-unpublish btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="<?php echo $id; ?>"><i class="zmdi zmdi-globe-lock"></i></a>
												<?php }//END IF PUBLISH ?>

												<a href="#" id="<?php echo $id; ?>" class="btn btn-delete btn-icon waves-effect waves-light btn-danger m-b-5 m-r-5"><i class="zmdi zmdi-delete"></i></a>
											</td>
										</tr>
										<?php $i++;}//END FOREACH#1 ?>
								</tbody>

							</table>


						</div>
					</div>
				</div>
				<!-- End row -->

			</div>
			<!-- container -->

		</div>
		<!-- content -->


	</div>
	<!-- End content-page -->


	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
		<script>
			$(".btn-delete").click(function (event) {
				var id = $(this).attr("id");
				swal({
					title: "Warning Data!",
					text: "Confirm you delete",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, Delete it!",
					cancelButtonText: "No, Cancel plx!",
					closeOnConfirm: true,
					closeOnCancel: true
				}, function (isConfirm) {
					if (isConfirm) {
						$.ajax({
							url: "<?php echo base_url('main/delete/type_property/type_property_id/'); ?>" + id,
							type: "POST",
							data: {
							},
							cache: false,
							success: function (data) {
//															console.log(data);
								window.location.reload();
							},
							error: function (xhr, desc, err) {
								console.log(err);
							}
						});
						return false;
					} else {
						swal.close();
						return false;
					}
					event.preventDefault();
					return false;
				});

			});

			$(".btn-publish").click(function (event) {
				event.preventDefault();
				var id = $(this).attr("id");
				swal({
					title: "Warning Data!",
					text: "Confirm you publish",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, Publish it!",
					cancelButtonText: "No, Cancel plx!",
					closeOnConfirm: true,
					closeOnCancel: true
				}, function (isConfirm) {
					if (isConfirm) {
						$.ajax({
							url: "<?php echo base_url('main/edit/type_property/'); ?>" + id,
							type: "POST",
							data: {
								type_property_id: id,
								publish: 2,
							},
							cache: false,
							success: function (data) {
//															console.log(data);
								window.location.reload();
							},
							error: function (xhr, desc, err) {
								console.log(err);
							}
						});
						return false;
					} else {
						swal.close();
						return false;
					}
					return false;
				});
			});

			$(".btn-unpublish").click(function (event) {
				event.preventDefault();
				var id = $(this).attr("id");
				swal({
					title: "Warning Data!",
					text: "Confirm you unpublish",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, Unpublish it!",
					cancelButtonText: "No, Cancel plx!",
					closeOnConfirm: true,
					closeOnCancel: true
				}, function (isConfirm) {
					if (isConfirm) {
						$.ajax({
							url: "<?php echo base_url('main/edit/type_property/'); ?>" + id,
							type: "POST",
							data: {
								type_property_id: id,
								publish: 1,
							},
							cache: false,
							success: function (data) {
//															console.log(data);
								window.location.reload();
							},
							error: function (xhr, desc, err) {
								console.log(err);
							}
						});
						return false;
					} else {
						swal.close();
						return false;
					}

					return false;
				});
			});
		</script>
