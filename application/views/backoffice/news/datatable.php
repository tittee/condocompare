<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');
?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">


							<a href="<?php echo $url_href ?>" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">
								<i class="zmdi zmdi-account-add"></i> ADD</a>

							<form action="">
										<table id="datatable-customize" class="table table-striped table-bordered table-hover display responsive nowrap" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>ลำดับ</th>
													<th>รหัส</th>
													<th>รูป</th>
													<th>ชื่อ</th>
													<th>หมวดข่าวสาร</th>
													<th>สถานะ</th>
													<th>วันที่สร้าง / แก้ไข </th>
													<th>จัดการ</th>
												</tr>
											</thead>
											<tbody>
												<?php

										$numrows = 1;
									foreach($data as $row => $value)
									{//START FOREACH#1
										$i = 0;
										$sfilepath = base_url().'/uploads/news';
										$pic_thumb = $value->news_pic_thumb;
										$pic_thumb_t = ( !empty($pic_thumb) )? $sfilepath.'/'.$pic_thumb : 'http://placekitten.com/200/200';

										$news_category_name = $this->M->get_name('news_category_id, news_category_name', 'news_category_id', 'news_category_name', $value->fk_news_category_id,'news_category');


										$id = $value->news_id;
										$pubilsh = $value->publish;
                    $visited = $value->visited;
										$createdate = $value->createdate;
										$updatedate = $value->updatedate;
										$createdate_t = ($createdate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
										$updatedate_t = ($updatedate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($updatedate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';

										$news_category_name = $this->M->get_name('news_category_id, news_category_name', 'news_category_id', 'news_category_name', $value->fk_news_category_id,'news_category');
									?>
													<tr data-position='<?php echo $numrows; ?>' id='news_id_<?php echo $id; ?>'>
														<td><?php echo $numrows; ?></td>
                            <td><?php echo $id; ?></td>
														<td><img src="<?php echo $pic_thumb_t; ?>" style="width: 150px;" class="img-thumbnail" alt=""></td>
														<td><?php echo $value->news_title; ?><br>
                            จำนวนคนดู : <?php echo $value->visited; ?></td>
														<td>
															<?php echo $news_category_name[$i]->news_category_name; ?>
														</td>

														<td>
															<?php echo $this->primaryclass->get_publish($pubilsh); ?>
														</td>
														<td>
																สร้าง : <?php echo $createdate_t; ?>
																<br>
																แก้ไข : <?php echo $updatedate_t; ?>
														</td>
														<td>
															<input type="hidden" id="news_id" name="news_id" value="<?php echo $id; ?>">
															<a href="<?php echo site_url('news/edit_news/'.$id); ?>" class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5"><i class="zmdi zmdi-edit"></i></a>

															<?php if( $pubilsh == 1){ //ไม่แสดงผล ต้องเปลี่ยนเป็นแสดงผล ?>
															<a href="#" class="btn btn-publish btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="<?php echo $id; ?>"><i class="zmdi zmdi-globe-alt"></i></a>
															<?php } else {//แสดงผล ต้องเปลี่ยนเป็นไม่แสดงผล ?>
															<a href="#" class="btn btn-unpublish btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="<?php echo $id; ?>"><i class="zmdi zmdi-globe-lock"></i></a>
															<?php }//END IF PUBLISH ?>

															<a href="#" id="<?php echo $id; ?>" class="btn btn-delete btn-icon waves-effect waves-light btn-danger m-b-5 m-r-5"><i class="ti-trash"></i></a>
														</td>
													</tr>
													<?php ++$numrows;}//END FOREACH#1 ?>
											</tbody>

										</table>
							</form>
              <div id="result"></div>


						</div>
					</div>
				</div>
				<!-- End row -->

			</div>
			<!-- container -->

		</div>
		<!-- content -->


	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>

  <script type="text/javascript">
  $(document).ready(function() {
    var table =  $('#datatable-customize').DataTable({
      rowReorder: true,
      ordering: true,
      responsive: true,
			lengthMenu: [[20, 35, 60, -1], [20, 35, 60, "All"]],
      columnDefs: [
        {targets: [0], visible: false, orderable: false },
    ],
    });

    table.on( 'row-reorder', function ( e, diff, edit ) {
        var result = 'Reorder started on row: '+edit.triggerRow.data()[0]+'<br>';

        // var id = table.row( 'tr' ).id();

        for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
            var rowData = table.row( diff[i].node ).data();
            var rowDatasss = table.row().data();

              console.log( rowData[0] );

            result += rowData[0]+' updated to be in position '+
                diff[i].newData+' (was '+diff[i].oldData+')<br>';

            $.ajax({
  						url: "<?php echo site_url('news/set_news_order')?>",
  						type: "POST",
  						data: {
                count_order : i,
                id : rowData[1],
                news_order :  diff[i].newData,
              },
  						cache:false,
  						success: function (data)
  						{
  							// console.log(data);
  							window.location.reload();
  						},
  						error: function (xhr, desc, err)
  						{
  							//console.log( err );
  						}
  					});
          // console.log(diff[i].newData);
          // console.log(diff[i]);
          //console.log(  table.row( this ).id() );

        }
        $('#result').html( 'Event result:<br>'+result );
    } );



  });
  </script>
<script>
		$(".btn-delete").click(function(event){
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you delete",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('news/delete_news/'); ?>"+id,
						type: "POST",
						data: id,
						cache:false,
						success: function (data)
						{
							//console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				event.preventDefault();
				return false;
			});

		});

		$(".btn-publish").click(function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you publish",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Publish it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('news/display_news/'); ?>"+id,
						type: "POST",
						data: {
							news_id : id,
							publish : '1',
						},
						cache:false,
						success: function (data)
						{
//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				return false;
			});
		});

		$(".btn-unpublish").click(function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you unpublish",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Unpublish it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('news/undisplay_news/'); ?>"+id,
						type: "POST",
						data: {
							news_id : id,
							publish : '1',
						},
						cache:false,
						success: function (data)
						{
//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}

				return false;
			});
		});

	</script>
