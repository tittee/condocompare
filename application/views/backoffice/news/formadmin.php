<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'js'=>isset($js)?$js:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

if(isset( $data['row'] ))
{
	$action = 'edit';
	$news_id 								= $data['row']->news_id;
	$fk_news_category_id 								= $data['row']->fk_news_category_id;

	$news_title 						= $data['row']->news_title;
	$news_caption 					= $data['row']->news_caption;
	$news_description 					= $data['row']->news_description;

	$news_pic_thumb 								= $data['row']->news_pic_thumb;
	$news_pic_large 								= $data['row']->news_pic_large;

	$meta_title 					= $data['row']->meta_title;
	$meta_keyword 					= $data['row']->meta_keyword;
	$meta_description 					= $data['row']->meta_description;

	$createby 					= $data['row']->createby;
	$updateby 					= $data['row']->updateby;

	$highlight 					= $data['row']->highlight;
	$recommended 					= $data['row']->recommended;
	$publish 					= $data['row']->publish;
}
else
{
	$action = 'add';
	$fk_news_category_id 								= "";

	$news_title 							= "";
	$news_caption 					= "";
	$news_description 			= "";

	$news_pic_thumb 								= "";
	$news_pic_large 								= "";

	$meta_title 					= "";
	$meta_keyword 				= "";
	$meta_description 		= "";

	$createby 					= "";
	$updateby 					= "";

	$highlight 				= 1;
	$recommended 			= 1;
	$publish 					= 1;

}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">

						<?php if(isset( $data['row'] ))
									{
								?>
						<form id="frmdata" action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
							<input type="hidden" name="news_id" value="<?php echo $news_id; ?>">
							<input type="hidden" name="old_news_pic_thumb" value="<?php echo $news_pic_thumb; ?>">
							<input type="hidden" name="old_news_pic_large" value="<?php echo $news_pic_large; ?>">
							<?php }
									else
									{
								?>
						<form id="frmdata" action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
							<?php } ?>
						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">รายละเอียด</h4>
							<div class="row">
								<div class="col-lg-12">

									<div class="form-group">
										<label class="col-md-2 control-label">ชื่อ</label>
										<div class="col-md-10">
											<input type="text" name="news_title" maxlength="150" class="inputmaxlength form-control" value="<?php echo $news_title; ?>" placeholder="">
										</div>
									</div>
									<!-- end from-group -->

									<div class="form-group">
										<label class="col-sm-2 control-label">หมวดหมู่ข่าวสาร *</label>
										<div class="col-sm-10">
											<select id="fk_news_category_id" class="form-control" name="fk_news_category_id" onchange="(this)" required="">
												<option value="0">----เลือก----</option>
												<?php
														foreach ($news_category as $row) {//START FOREACH
															$sel = ($row->news_category_id == $fk_news_category_id)? 'selected': '';
													?>
													<option value="<?php echo $row->news_category_id; ?>" <?php echo $sel; ?>>
														<?php echo $row->news_category_name; ?>
													</option>
													<?php
														}//END FOREACH
													?>
											</select>
										</div>
									</div>
									<!-- end from-group -->


									<div class="form-group">
										<label class="col-md-2 control-label">การแสดงผล</label>
										<div class="col-md-2">
											<div class="radio-inline radio radio-success radio-single">
												<input type="radio" name="publish" value="2" required <?php echo ($publish == 2)? 'checked': ''; ?>>
												<label for="">
													แสดงผล
												</label>
											</div>
										</div>

										<div class="col-md-2">
											<div class="radio-inline radio radio-success radio-single">
												<input type="radio" name="publish" value="1" required <?php echo ($publish == 1)? 'checked': ''; ?>>
												<label for="">
													ไม่แสดงผล
												</label>
											</div>
										</div>
									</div>
									<!-- end from-group -->

								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->


						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">คุณลักษณะข่าวสาร</h4>
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<label class="col-md-2 control-label">ข่าวสารแนะนำ</label>
										<div class="col-md-2">
											<div class="radio-inline radio radio-success radio-single">
												<input type="radio" name="recommended" value="2" required <?php echo ($recommended == 2)? 'checked': ''; ?>>
												<label for="">
													ข่าวสารแนะนำ
												</label>
											</div>
										</div>

										<div class="col-md-2">
											<div class="radio-inline radio radio-success radio-single">
												<input type="radio" name="recommended" value="1" required <?php echo ($recommended == 1)? 'checked': ''; ?>>
												<label for="">
													ไม่ตั้งค่าข่าวสารแนะนำ
												</label>
											</div>
										</div>
									</div>
									<!-- end from-group -->

									<div class="form-group">
										<label class="col-md-2 control-label">ข่าวสารพิเศษ</label>
										<div class="col-md-2">
											<div class="radio-inline radio radio-success radio-single">
												<input type="radio" name="highlight" value="2" required <?php echo ($highlight == 2)? 'checked': ''; ?>>
												<label for="">
													ข่าวสารพิเศษ
												</label>
											</div>
										</div>

										<div class="col-md-2">
											<div class="radio-inline radio radio-success radio-single">
												<input type="radio" name="highlight" value="1" required <?php echo ($highlight == 1)? 'checked': ''; ?>>
												<label for="">
													ไม่ตั้งค่าข่าวสารพิเศษ
												</label>
											</div>
										</div>
									</div>
									<!-- end from-group -->

								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->

						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">รูปภาพ</h4>
							<div class="row">
								<div class="col-lg-12">


									<div class="form-group">
										<label class="col-md-2 control-label" for="news_pic_thumb">รูปภาพขนาดเล็ก</label>
										<div class="col-md-10">
											<input type="file" name="news_pic_thumb" />
											<?php if(!empty($news_pic_thumb))
									{
							?>
												<img src="<?php echo base_url()?>uploads/news/<?php echo $news_pic_thumb; ?>" class="img-responsive" alt="">
												<?php } ?>
										</div>
									</div>
									<!-- end from-group -->

									<div class="form-group">
										<label class="col-md-2 control-label" for="news_pic_large">รูปภาพขนาดใหญ่</label>
										<div class="col-md-10">
											<input type="file" name="news_pic_large" />
											<?php if(!empty($news_pic_large))
									{
							?>
												<img src="<?php echo base_url()?>uploads/news/<?php echo $news_pic_large; ?>" class="img-responsive" alt="">
												<?php } ?>
										</div>
									</div>
									<!-- end from-group -->
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->

						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">ข้อมูลข่าวสาร</h4>
							<div class="row">
								<div class="col-lg-12">

									<div class="form-group">
										<label class="col-md-2 control-label">คำอธิบายข่าวสาร</label>
										<div class="col-md-10">
											<textarea name="news_caption" id="news_caption" maxlength="160" class="inputmaxlength form-control" cols="30" rows="10"><?php echo $news_caption; ?></textarea>
										</div>
									</div>
									<!-- end from-group -->

									<div class="form-group">
										<label class="col-md-2 control-label">รายละเอียดทั้งหมด</label>
										<div class="col-md-10">
											<textarea class="wysiwigeditor" name="news_description"><?php echo $news_description;?></textarea>
										</div>
									</div>
									<!-- end from-group -->

								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->

						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">คำค้นหา</h4>
							<div class="row">
								<div class="col-lg-12">

									<div class="form-group">
										<label class="col-md-2 control-label">META TITLE</label>
										<div class="col-md-10">
											<textarea name="meta_title" id="meta_title" maxlength="100" class="inputmaxlength form-control" cols="30" rows="3"><?php echo $meta_title; ?></textarea>
											<span class="text-warning help-block"><small>กรอกไม่เกิน 100 ตัวอักษรและคั่นด้วย , เสมอ</small></span>
										</div>
									</div>
									<!-- end from-group -->

									<div class="form-group">
										<label class="col-md-2 control-label">META KEYWORD</label>
										<div class="col-md-10">
											<textarea name="meta_keyword" id="meta_keyword" maxlength="255" class="inputmaxlength form-control" cols="30" rows="3"><?php echo $meta_keyword; ?></textarea>
											<span class="text-warning help-block"><small>กรอกไม่เกิน 255 ตัวอักษรและคั่นด้วย , เสมอ</small></span>
										</div>
									</div>
									<!-- end from-group -->

									<div class="form-group">
										<label class="col-md-2 control-label">META TITLE</label>
										<div class="col-md-10">
											<textarea name="meta_description" id="meta_description" maxlength="255" class="inputmaxlength form-control" cols="30" rows="3"><?php echo $meta_description; ?></textarea>
											<span class="text-warning help-block"><small>กรอกไม่เกิน 255 ตัวอักษร</small></span>
										</div>
									</div>
									<!-- end from-group -->

								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->

						<div class="card-box">
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10 m-t-15">
											<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light" onclick="tinyMCE.triggerSave(true,true);">
												Submit
											</button>
											<a href="<?php echo site_url('news/news'); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">
													Cancel
												</a>
											<input type="hidden" name="action" value="action">
										</div>
									</div>
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->


						<?php if(isset( $data['row'] ))
									{
								?>
						</form>
						<?php }
									else
									{
								?>
						</form>
						<?php } ?>
						<!-- end form -->
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->

	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
	<script>
		$("form#frmdata").submit(function(event){
			var formData = new FormData($(this)[0]);

			<?php
			$ajax_url = ($action === 'add')? "news/add_news" :  "news/edit_news/".$news_id;
			$msg = ($action === 'add')? "Insert" :  "Update";
			?>
			var url = "<?php echo base_url().$ajax_url; ?>";

			var formData = new FormData($(this)[0]);

			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				cache: false,
				processData: false,
				contentType: false,
				context: this,
				success: function (data, status)
				{
					//console.log( data );
					swal({
						title: "Success Data!",
						text: "Confirm you save",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Yes, <?php echo $msg; ?> it!",
						cancelButtonText: "No, cancel plx!",
						closeOnConfirm: true,
						closeOnCancel: true }, function(isConfirm){
						if (isConfirm)
						{
							window.location.replace('<?php echo base_url(); ?>news/news');
						}
						else
						{
							window.location.reload();
						}
					});

				},
				error: function (xhr, desc, err)
				{
					console.log( err );
				},

			});
			event.preventDefault();
			return false;
		});
	</script>
