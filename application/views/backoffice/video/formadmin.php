<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'js'=>isset($js)?$js:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

if(isset( $data['row'] ))
{
	$action = 'edit';
	$video_id 								= $data['row']->video_id;

	$video_title 						= $data['row']->video_title;
	$video_url 						= $data['row']->video_url;
	$video_caption 					= $data['row']->video_caption;
	$video_description 					= $data['row']->video_description;

	$video_pic_thumb 								= $data['row']->video_pic_thumb;
	$video_pic_large 								= $data['row']->video_pic_large;

	$meta_title 					= $data['row']->meta_title;
	$meta_keyword 					= $data['row']->meta_keyword;
	$meta_description 					= $data['row']->meta_description;

	$createby 					= $data['row']->createby;
	$updateby 					= $data['row']->updateby;

	$highlight 					= $data['row']->highlight;
	$recommended 					= $data['row']->recommended;
	$publish 					= $data['row']->publish;
}
else
{
	$action = 'add';

	$video_title 							= "";
	$video_url 							= "";
	$video_caption 					= "";
	$video_description 			= "";

	$video_pic_thumb 								= "";
	$video_pic_large 								= "";

	$meta_title 					= "";
	$meta_keyword 				= "";
	$meta_description 		= "";

	$createby 					= "";
	$updateby 					= "";

	$highlight 				= 1;
	$recommended 			= 1;
	$publish 					= 1;

}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">

						<?php if(isset( $data['row'] ))
									{
								?>
						<form id="frmdata" action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
							<input type="hidden" name="video_id" value="<?php echo $video_id; ?>">
							<input type="hidden" name="old_video_pic_thumb" value="<?php echo $video_pic_thumb; ?>">
							<input type="hidden" name="old_video_pic_large" value="<?php echo $video_pic_large; ?>">
							<?php }
									else
									{
								?>
						<form id="frmdata" action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
							<?php } ?>
						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">รายละเอียด</h4>
							<div class="row">
								<div class="col-lg-12">

									<div class="form-group">
										<label class="col-md-2 control-label">ชื่อ</label>
										<div class="col-md-10">
											<input type="text" name="video_title" maxlength="150" class="inputmaxlength form-control" value="<?php echo $video_title; ?>" maxlength="100" placeholder="">
										</div>
									</div>
									<!-- end from-group -->

                  <div class="form-group">
										<label class="col-md-2 control-label">ลิ้งค์</label>
										<div class="col-md-10">
											<input type="text" name="video_url" maxlength="150" class="inputmaxlength form-control" value="<?php echo $video_url; ?>" maxlength="100" placeholder="">
                      <span class="font-13 text-muted">นำ URL ของ Youtube มาใส่ดังนี้ : https://youtu.be/QyhrOruvT1c</span>
										</div>
									</div>
									<!-- end from-group -->




									<div class="form-group">
										<label class="col-md-2 control-label">การแสดงผล</label>
										<div class="col-md-2">
											<div class="radio-inline radio radio-success radio-single">
												<input type="radio" name="publish" value="2" required <?php echo ($publish == 2)? 'checked': ''; ?>>
												<label for="">
													แสดงผล
												</label>
											</div>
										</div>

										<div class="col-md-2">
											<div class="radio-inline radio radio-success radio-single">
												<input type="radio" name="publish" value="1" required <?php echo ($publish == 1)? 'checked': ''; ?>>
												<label for="">
													ไม่แสดงผล
												</label>
											</div>
										</div>
									</div>
									<!-- end from-group -->

								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->


						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">คุณลักษณะวีดีโอ</h4>
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<label class="col-md-2 control-label">วีดีโอแนะนำ</label>
										<div class="col-md-2">
											<div class="radio-inline radio radio-success radio-single">
												<input type="radio" name="recommended" value="2" required <?php echo ($recommended == 2)? 'checked': ''; ?>>
												<label for="">
													วีดีโอแนะนำ
												</label>
											</div>
										</div>

										<div class="col-md-2">
											<div class="radio-inline radio radio-success radio-single">
												<input type="radio" name="recommended" value="1" required <?php echo ($recommended == 1)? 'checked': ''; ?>>
												<label for="">
													ไม่ตั้งค่าวีดีโอแนะนำ
												</label>
											</div>
										</div>
									</div>
									<!-- end from-group -->



								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->

						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">รูปภาพ</h4>
							<div class="row">
								<div class="col-lg-12">


									<div class="form-group">
										<label class="col-md-2 control-label" for="video_pic_thumb">รูปภาพขนาดเล็ก</label>
										<div class="col-md-10">
											<input type="file" name="video_pic_thumb" />
											<?php if(!empty($video_pic_thumb))
									{
							?>
												<img src="<?php echo base_url()?>uploads/video/<?php echo $video_pic_thumb; ?>" class="img-responsive" alt="">
												<?php } ?>
										</div>
									</div>
									<!-- end from-group -->

									<div class="form-group">
										<label class="col-md-2 control-label" for="video_pic_large">รูปภาพขนาดใหญ่</label>
										<div class="col-md-10">
											<input type="file" name="video_pic_large" />
											<?php if(!empty($video_pic_large))
									{
							?>
												<img src="<?php echo base_url()?>uploads/video/<?php echo $video_pic_large; ?>" class="img-responsive" alt="">
												<?php } ?>
										</div>
									</div>
									<!-- end from-group -->
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->

						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">ข้อมูลวีดีโอ</h4>
							<div class="row">
								<div class="col-lg-12">

									<div class="form-group">
										<label class="col-md-2 control-label">คำอธิบายวีดีโอ</label>
										<div class="col-md-10">
											<textarea name="video_caption" id="video_caption" maxlength="255" class="inputmaxlength form-control" cols="30" rows="10"><?php echo $video_caption; ?></textarea>
										</div>
									</div>
									<!-- end from-group -->

									<div class="form-group">
										<label class="col-md-2 control-label">รายละเอียดทั้งหมด</label>
										<div class="col-md-10">
											<textarea class="wysiwigeditor" name="video_description"><?php echo $video_description;?></textarea>
										</div>
									</div>
									<!-- end from-group -->

								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->

						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">คำค้นหา</h4>
							<div class="row">
								<div class="col-lg-12">

									<div class="form-group">
										<label class="col-md-2 control-label">META TITLE</label>
										<div class="col-md-10">
											<textarea name="meta_title" id="meta_title" maxlength="100" class="inputmaxlength form-control" cols="30" rows="3"><?php echo $meta_title; ?></textarea>
											<span class="text-warning help-block"><small>กรอกไม่เกิน 100 ตัวอักษรและคั่นด้วย , เสมอ</small></span>
										</div>
									</div>
									<!-- end from-group -->

									<div class="form-group">
										<label class="col-md-2 control-label">META KEYWORD</label>
										<div class="col-md-10">
											<textarea name="meta_keyword" id="meta_keyword" maxlength="255" class="inputmaxlength form-control" cols="30" rows="3"><?php echo $meta_keyword; ?></textarea>
											<span class="text-warning help-block"><small>กรอกไม่เกิน 255 ตัวอักษรและคั่นด้วย , เสมอ</small></span>
										</div>
									</div>
									<!-- end from-group -->

									<div class="form-group">
										<label class="col-md-2 control-label">META TITLE</label>
										<div class="col-md-10">
											<textarea name="meta_description" id="meta_description" maxlength="255" class="inputmaxlength form-control" cols="30" rows="3"><?php echo $meta_description; ?></textarea>
											<span class="text-warning help-block"><small>กรอกไม่เกิน 255 ตัวอักษร</small></span>
										</div>
									</div>
									<!-- end from-group -->

								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->

						<div class="card-box">
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10 m-t-15">
											<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light" onclick="tinyMCE.triggerSave(true,true);">
												Submit
											</button>
											<a href="<?php echo site_url('video/video'); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">
													Cancel
												</a>
											<input type="hidden" name="action" value="action">
										</div>
									</div>
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->


						<?php if(isset( $data['row'] ))
									{
								?>
						</form>
						<?php }
									else
									{
								?>
						</form>
						<?php } ?>
						<!-- end form -->
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->

	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
	<script>
		$("form#frmdata").submit(function(event){
			var formData = new FormData($(this)[0]);

			<?php
			$ajax_url = ($action === 'add')? "video/add_video" :  "video/edit_video/".$video_id;
			$msg = ($action === 'add')? "Insert" :  "Update";
			?>
			var url = "<?php echo base_url().$ajax_url; ?>";

			var formData = new FormData($(this)[0]);

			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				cache: false,
				processData: false,
				contentType: false,
				context: this,
				success: function (data, status)
				{
					//console.log( data );
					swal({
						title: "Success Data!",
						text: "Confirm you save",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Yes, <?php echo $msg; ?> it!",
						cancelButtonText: "No, cancel plx!",
						closeOnConfirm: true,
						closeOnCancel: true }, function(isConfirm){
						if (isConfirm)
						{
							window.location.replace('<?php echo base_url(); ?>video/video');
						}
						else
						{
							window.location.reload();
						}
					});

				},
				error: function (xhr, desc, err)
				{
					console.log( err );
				},

			});
			event.preventDefault();
			return false;
		});
	</script>
