<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));
?>

	<div class="account-pages"></div>
	<div class="clearfix"></div>
	<div class="wrapper-page">
		<div class="text-center">
			<a href="index.html" class="logo"><span>CONDO <span>COMPARE</span></span></a>
			<h5 class="text-muted m-t-0 font-600">by THC</h5>
		</div>
		<div class="m-t-40 card-box">
			<div class="text-center">
				<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>
			</div>
			<div class="panel-body">
				<?php
				$attributes = array(
					'class' => 'form-horizontal m-t-20',
					'id' => 'form_login',
					'autocomplete'=> 'off',
				);
				//echo form_open('staff/check_login', $attributes);
				echo form_open('staff/check_login', $attributes);
				?>
<!--				<form class="form-horizontal m-t-20" action="<?php echo site_url('staff/check_login'); ?>" method="post" autocomplete="off">-->

					<div class="form-group ">
						<div class="col-xs-12">
							<?php $frm_email = array(
								"name" => "email",
								"id" => "email",
									"type" => "email",
									"class" => "form-control",
									"required"=>"required",
									"placeholder" => "Email / Username"
								);
							?>
							<?php echo form_input($frm_email) ?>
						</div>
					</div>

					<div class="form-group">
						<div class="col-xs-12">
							<?php $frm_password = array("name" => "password",
																				"id" => "password",
																			 "class" => "form-control",
																			 "required"=>"required",
																			"placeholder" => "Password");
							?>
							<?php echo form_password($frm_password) ?>
						</div>
					</div>

					<div class="form-group text-center m-t-30">
						<div class="col-xs-12">
							<?php
//								$frm_button = array(
//									'name'          => 'submit',
//									'id'          	=> 'submit',
//									'class' 				=> 'btn btn-custom btn-bordred btn-block waves-effect waves-light',
//									'value'         => '',
//									'type'          => 'submit',
//									'content'       => 'Log In'
//								);
//								echo form_button($frm_button);
							?>
							<button type="submit" class="btn btn-custom btn-bordred btn-block waves-effect waves-light" >Log In</button>
						</div>
					</div>

				<?php
					$string = '';
					echo form_close($string);
				?>
<!--				</form>-->

			</div>
		</div>
		<!-- end card-box-->



	</div>
	<?php $this->load->view('backoffice/partials/footer', array('js'=>isset($js)?$js:array(),true) )?>
<script type="text/javascript">
		$("#form_login").submit(function(event){
			event.preventDefault();

			var email = $('#email').val();
			var password = $('#password').val();

			$.ajax({
				url: "<?php echo base_url('staff/check_login'); ?>",
				type: "POST",
				data: {
					email : email,
					password : password
				},
				cache:false,
				success: function (data)
				{
					console.log(data);
					if( data == "0")
					{
						swal("Invalid your Username or Password");
						return false
					}
					else if( data == "1")
					{
						swal({
							title: "Success Login!",
							text: "Welcome to backoffice",
							type: "info",
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "OK",
							closeOnConfirm: false,
							animation: "slide-from-top",
						},
						function(isConfirm){
							if (isConfirm)
							{
								window.location.replace('<?php echo base_url(); ?>backoffice/dashboard');
							}
							else
							{
								swal.close();
								return false;
							}

						});

					}
				},
				error: function (xhr, desc, err)
				{
					console.log( err );
				}
			});

			// return false;



		});

</script>
