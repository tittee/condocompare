<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

if(isset( $data['row'] ))
{
	$action						= 'edit';
	$member_id 				= $data['row']->member_id;
	$member_fname 		= $data['row']->member_fname;
	$member_lname 		= $data['row']->member_lname;
	$member_type_id	 	= $data['row']->member_type_id;
	$member_email	 		= $data['row']->member_email;
	$member_mobileno	 		= $data['row']->member_mobileno;
	$member_profile 	= $data['row']->member_profile;
	$member_password 	= $data['row']->member_password;
	$pwd_disabled = 'disabled';
	$member_address 	= trim($data['row']->member_address);
	$provinces_id 		= $data['row']->provinces_id;
	$districts_id 		= $data['row']->districts_id;
	$sub_districts_id = $data['row']->sub_districts_id;
	$zipcode 					= $data['row']->zipcode;
	$approved 				= $data['row']->approved;
	$facebook_id 			= $data['row']->facebook_id;
}
else
{
	$action						= 'add';
	$member_fname 		= '';
	$member_lname 		= '';
	$member_type_id 	= '';
	$member_email 		= '';
	$member_mobileno 		= '';
	$member_profile 	= '';
	$member_password 	= '';
	$pwd_disabled			= '';
	$member_address 	= '';
	$provinces_id 		= '';
	$districts_id 		= '';
	$sub_districts_id = '';
	$zipcode 					= '';
	$approved 				= '1';
}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">

							<div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
									<i class="zmdi zmdi-more-vert"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</div>

							<h4 class="header-title m-t-0 m-b-30">Form details</h4>


							<div class="row">
								<div class="col-lg-12">
									<?php if(isset( $data['row'] ))
									{
								?>
										<form id="frmdata" action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
											<input type="hidden" name="member_id" value="<?php echo $member_id; ?>">
											<input type="hidden" name="old_member_profile" value="<?php echo $member_profile; ?>">
											<?php }
									else
									{
								?>
												<form id="frmdata" action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
													<?php } ?>
														<div class="form-group">
															<label class="col-md-2 control-label">First name</label>
															<div class="col-md-10">
																<input type="text" name="member_fname" class="form-control" value="<?php echo $member_fname; ?>" placeholder="ชื่อ">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">Last Name</label>
															<div class="col-md-10">
																<input type="text" name="member_lname" class="form-control" value="<?php echo $member_lname; ?>" placeholder="Last name">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">Mobile NO</label>
															<div class="col-md-10">
																<input type="text" name="member_mobileno" class="form-control" value="<?php echo $member_mobileno; ?>" placeholder="Mobile No">
															</div>
														</div>

														<div class="form-group">
															<label class="col-md-2 control-label" for="member_email">Email</label>
															<div class="col-md-10">
																<input type="email" id="example-email" name="member_email" class="form-control" placeholder="Email@harrison.co.th" value="<?php echo $member_email; ?>">
															</div>
														</div>

														<?php
														if($action==='edit')
														{
															?>
														<div class="form-group">
															<label class="col-md-2 control-label">แก้ไขรหัสผ่าน</label>
															<div class="col-md-10">
																<div class="checkbox">
																		<input id="chk_pwd" name="chk_pwd" type="checkbox" value="1" data-parsley-multiple="chk_pwd" data-parsley-id="13">
																		<label for="chk_pwd"> เปลี่ยนรหัสผ่าน </label>
																</div>
															</div>
														</div>
														<?php
														}
														?>

														<div class="form-group">
															<label class="col-md-2 control-label">Password</label>
															<div class="col-md-10">
																<input type="password" id="password" name="member_password" class="form-control" value="<?php echo $member_password; ?>" placeholder="Password" <?php echo $pwd_disabled; ?>>
															</div>
														</div>

														<div class="form-group">
															<label class="col-sm-2 control-label">Member Type</label>
															<div class="col-sm-10">
																<select class="form-control" name="member_type_id">
																	<option value="0" <?php echo ($member_type_id==0)? 'selected' : ''; ?>> ----เลือก----</option>
																	<option value="1" <?php echo ($member_type_id==1)? 'selected' : ''; ?>>แชร์ไม่ได้</option>
																	<option value="2" <?php echo ($member_type_id==2)? 'selected' : ''; ?>>แชร์ได้</option>
																</select>
															</div>
														</div>

														<div class="form-group">
															<label class="col-md-2 control-label">Address</label>
															<div class="col-md-10">
																<textarea name="member_address" class="form-control" id="member_address" cols="30" rows="10"><?php echo $member_address; ?></textarea>
															</div>
														</div>


														<div class="form-group">
															<label class="col-md-2 control-label" for="member_profile">Picture Profile</label>
															<div class="col-md-10">
																<input type="file" name="member_profile" />
																<?php if(!empty($member_profile))
														{

	$member_profile_t = ( !empty($facebook_id) )? $member_profile : base_url().'uploads/'.$member_profile;
												?>
																	<img src="<?php echo $member_profile_t; ?>" class="img-responsive" alt="">
																	<?php } ?>
															</div>
														</div>


														<div class="form-group">
															<label class="col-sm-2 control-label">Province</label>
															<div class="col-sm-10">
																<select id="provinces_id" class="form-control" name="provinces_id" onchange="(this)">
																	<option value="0">----เลือก----</option>
																	<?php
																			foreach ($provinces as $row) {//START FOREACH
																				$sel = ($row->provinces_id == $provinces_id)? 'selected' : '';
																		?>
																		<option value="<?php echo $row->provinces_id; ?>" <?php echo $sel; ?> >
																			<?php echo $row->provinces_name; ?>
																		</option>
																		<?php
																			}//END FOREACH
																		?>
																</select>
															</div>
														</div>

														<div class="form-group">
															<label class="col-sm-2 control-label">Districts</label>
															<div class="col-sm-10">
																<select id="districts_id" class="form-control" name="districts_id" onchange="(this)">
																	<option value="0">----เลือก----</option>
																</select>
															</div>
														</div>

														<div class="form-group">
															<label class="col-sm-2 control-label">Sub Districts</label>
															<div class="col-sm-10">
																<select id="sub_districts_id" class="form-control" name="sub_districts_id" onchange="(this)">
																	<option value="0">----เลือก----</option>
																</select>
															</div>
														</div>

														<div class="form-group">
															<label class="col-md-2 control-label" for="zipcode">Zipcode</label>
															<div class="col-md-10">
																<input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="Zipcode" value="<?php echo $zipcode; ?>">
															</div>
														</div>

														<div class="form-group">
															<label class="col-sm-2 control-label">Approved</label>
															<div class="col-sm-10">
																<div class="radio radio-pink">
																	<input id="radio1" value="2" name="approved" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" data-parsley-id="69" <?php echo ($approved==2)? 'checked' : ''; ?>>
																	<label for="radio1"> Approved </label>
																</div>

																<div class="radio radio-pink">
																	<input id="radio2" value="1" name="approved" type="radio" data-parsley-multiple="groups" data-parsley-mincheck="2" required="" <?php echo ($approved==1 || $approved=='' )? 'checked' : ''; ?>>
																	<label for="radio2"> Unapproved </label>
																</div>
															</div>
														</div>

														<!-- end col -->

														<!--<div class="form-group">
											<label class="col-md-2 control-label">Text area</label>
											<div class="col-md-10">
												<textarea class="form-control" rows="5"></textarea>
											</div>
										</div>-->

														<div class="form-group">
															<div class="col-sm-offset-2 col-sm-10 m-t-15">
																<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light">
																	Submit
																</button>
																<a href="<?php echo site_url('member/member'); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">
													Cancel
												</a>
															</div>
														</div>

												<?php if(isset( $data['row'] ))
									{
								?>
										</form>
											<?php }
									else
									{
								?>
												</form>
													<?php } ?>
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->

	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
		<script>
			$(document).ready(function () {
				var provinces_id = $("#provinces_id").val();
				if( provinces_id != 0 )
				{
					$.ajax({
						url: "<?php echo base_url(); ?>page/provinces_districts_relation",
						data: {
							id: provinces_id
						},
						type: "POST",
						success: function (data) {
							$("#districts_id").html(data);
							$("#districts_id").val(<?php echo $districts_id; ?>);

							var districts_id = $("#districts_id").val();
							//console.log(districts_id);

							$.ajax({
								url: "<?php echo base_url(); ?>page/districts_subdistricts_relation",
								data: {
									id: districts_id
								},
								type: "POST",
								success: function (data) {
									$("#sub_districts_id").html(data);
									$("#sub_districts_id").val(<?php echo $sub_districts_id; ?>);
								}
							});

						}
					});
				}
			});

			$("#provinces_id").change(function () {
				/*dropdown post */ //
				$.ajax({
					url: "<?php echo base_url(); ?>page/provinces_districts_relation",
					data: {
						id: $(this).val()
					},
					type: "POST",
					success: function (data) {
						$("#districts_id").html(data);
					}
				});
			});

			$("#districts_id").change(function () {
				/*dropdown post */ //
				$.ajax({
					url: "<?php echo base_url(); ?>page/districts_subdistricts_relation",
					data: {
						id: $(this).val()
					},
					type: "POST",
					success: function (data) {
						$("#sub_districts_id").html(data);
					}
				});
			});
		</script>



	<script>
		$(function() {
			$("#chk_pwd").on( "click", enable_pwd );
		});

		function enable_pwd() {
			if (this.checked) {
				$("#password").removeAttr("disabled");
			} else {
				$("#password").attr("disabled", true);
			}
		}

		$("form#frmdata").submit(function(event){
			var formData = new FormData($(this)[0]);

			swal({
				title: "Success Data!",
				text: "Confirm you save",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
<?php if( isset($formup) ) { ?>
<?php if( $formup === 'add') { ?>
				confirmButtonText: "Yes, insert it!",
	<?php } else { ?>
				confirmButtonText: "Yes, update it!",
	<?php }//END IF FORM ?>
<?php }//END IF ISSET ?>
				cancelButtonText: "No, cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
			<?php if( isset($formup) ) { ?>
				<?php if( $formup === 'add') { ?>
						url: "<?php echo base_url(); ?>member/add_data_member",
				<?php } else { ?>
						url: "<?php echo base_url(); ?>member/edit_data_member",
				<?php }//END IF FORM ?>
			<?php }//END IF ISSET ?>
							type: "POST",
							data: formData,
							async: false,
							success: function (data, status)
							{

								//console.log(data)
								window.location.replace('<?php echo base_url(); ?>member/member');
							},
							error: function (xhr, desc, err)
							{
								console.log( err );
							},
							cache: false,
							contentType: false,
							processData: false,
					});
					return false;
				}
				else
				{
					//window.location.reload();
					swal.close();
					return false;
				}
			});

			event.preventDefault();
			return false;
		});
	</script>
