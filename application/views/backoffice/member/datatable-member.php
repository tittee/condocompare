<?php
defined('BASEPATH') or exit();
/*HEADER*/
$this->load->view('backoffice/partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'BodyClass' => 'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');
?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">
              <form class="" action="" name="form_admin" method="post">

              <div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
									 <i class="zmdi zmdi-menu"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="?action=delete_all" class="delete_all">ลบทั้งหมด</a></li>
                  <li class="divider"></li>
									<li><a href="?action=approved_all" class="approved_all">อนุมัติทั้งหมด</a></li>
									<li><a href="?action=unapproved_all" class="unapproved_all">ไม่อนุมัติทั้งหมด</a></li>
								</ul>
							</div>

							<a href="<?php echo $url_href ?>" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">
								<i class="zmdi zmdi-account-add"></i> ADD</a>

										<table id="datatable-member" class="table table-striped table-bordered display responsive nowrap" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th class="not-mobile">รหัส <input type="checkbox"  onchange="checkAll(this)" name="row_id[]"></th>
													<th class="not-mobile">ID</th>
													<th class="">ชื่อ-นามสกุล</th>
													<th class="">อีเมล</th>
													<th class="not-mobile">เบอร์ติดต่อ</th>
													<th class="">ประเภท</th>
													<th class="not-mobile">สถานะ</th>
													<th class="">จัดการ</th>
												</tr>

											</thead>

										</table>
                    </form>
						</div>
					</div>
				</div>
				<!-- End row -->

			</div>
			<!-- container -->

		</div>
		<!-- content -->


	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>

	<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
	<script>
	$(document).ready(function() {

			$('#datatable-member').DataTable( {
				"dom": 'Bfrtip',
				"buttons": [
					{
						extend: 'csvHtml5',
						exportOptions: {
							// columns: [ 0, 2, 3, 4, 5, 6 ]
							columns: ':visible'
						}
					},
					{
						extend: 'excelHtml5',
						exportOptions: {
							// columns: [ 0, 2, 3, 4, 5, 6 ]
							columns: ':visible'
						}
					},
					'colvis'
				],
				"order": [], //Initial no order.
				"processing": true,
				"serverSide": true,
				// "pageLength": 50,
  			"pagingType": "full_numbers",
				"responsive": true,
				"lengthMenu": [[ 50, 100, 150, 200, -1], [ 50, 100, 150, 200, "All"]],
				"ajax": {
						"url": "<?php echo site_url('member/json_member') ?>",
						"type": "POST",
						// "dataSrc": "records"
				},

			"columnDefs": [
				{
					"targets": 0,
	        "orderable": false
				},

					]
			});
	});


		$(document).on('click', '.btn-delete', function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you delete",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('member/delete_member/'); ?>"+id,
						type: "POST",
						data: id,
						cache:false,
						success: function (data)
						{
							//console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				event.preventDefault();
				return false;
			});
		});



		$(document).on('click', '.btn-approved', function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you Approved",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, approved it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('member/approved_member/'); ?>"+id,
						type: "POST",
						data: {
							member_id : id,
							approved : '2',
						},
						cache:false,
						success: function (data)
						{
//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				return false;
			});
		});

		$(document).on('click', '.btn-unapproved', function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you unapproved",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Unapproved it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('member/unapproved_member/'); ?>"+id,
						type: "POST",
						data: {
							member_id : id,
							approved : '1',
						},
						cache:false,
						success: function (data)
						{
//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}

				return false;
			});
		});



		$(document).on('click', '.delete_all', function(event){
			event.preventDefault();
      var member_id = $( "form" ).serialize();
      // var member_id = $( "input[name='member_id[]']" ).val();
      swal({
        title: "Warning Data!",
        text: "Confirm you delete",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Delete All it!",
        cancelButtonText: "No, Cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false }, function(isConfirm){
        if (isConfirm)
        {
          // console.log(member_id);
          $.ajax({
            url: "<?php echo base_url('member/delete_multi_member/'); ?>",
            type: "POST",
            data: member_id,
            cache:false,
            success: function (data)
            {
              // console.log(data);
              window.location.reload();
            },
            error: function (xhr, desc, err)
            {
              console.log( err );
            }
          });
          return false;
        }
        else
        {
          swal.close();
          return false;
        }

        return false;
      });
		});


		$(document).on('click', '.approved_all', function(event){
			event.preventDefault();
      var member_id = $( "form" ).serialize();
      // var member_id = $( "input[name='member_id[]']" ).val();
      swal({
        title: "Warning Data!",
        text: "Confirm you approved",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Approved all it!",
        cancelButtonText: "No, Cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false }, function(isConfirm){
        if (isConfirm)
        {
          // console.log(member_id);
          $.ajax({
            url: "<?php echo base_url('member/approved_multi_member/'); ?>",
            type: "POST",
            data: member_id,
            cache:false,
            success: function (data)
            {
              // console.log(data);
              window.location.reload();
            },
            error: function (xhr, desc, err)
            {
              console.log( err );
            }
          });
          return false;
        }
        else
        {
          swal.close();
          return false;
        }

        return false;
      });
		});

		$(document).on('click', '.unapproved_all', function(event){
			event.preventDefault();
      var member_id = $( "form" ).serialize();
      // var member_id = $( "input[name='member_id[]']" ).val();
      swal({
        title: "Warning Data!",
        text: "Confirm you approved",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Approved all it!",
        cancelButtonText: "No, Cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false }, function(isConfirm){
        if (isConfirm)
        {
          // console.log(member_id);
          $.ajax({
            url: "<?php echo base_url('member/unapproved_multi_member/'); ?>",
            type: "POST",
            data: member_id,
            cache:false,
            success: function (data)
            {
              // console.log(data);
              window.location.reload();
            },
            error: function (xhr, desc, err)
            {
              console.log( err );
            }
          });
          return false;
        }
        else
        {
          swal.close();
          return false;
        }

        return false;
      });
		});

	</script>
