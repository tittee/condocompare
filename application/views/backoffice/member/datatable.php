<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');
?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">

              <div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
									 <i class="zmdi zmdi-menu"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="?action=delete_all" class="delete_all">ลบทั้งหมด</a></li>
                  <li class="divider"></li>
									<li><a href="?action=approved_all" class="approved_all">อนุมัติทั้งหมด</a></li>
									<li><a href="?action=unapproved_all" class="unapproved_all">ไม่อนุมัติทั้งหมด</a></li>
								</ul>
							</div>

              <form action="" method="post">

							<a href="<?php echo $url_href ?>" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">
								<i class="zmdi zmdi-account-add"></i> ADD</a>


							<table id="datatable-condo" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th data-priority="3">ลำดับ <input type="checkbox"  onchange="checkAll(this)" name="row_id[]"></th>
													<th data-priority="1">ID</th>
													<th data-priority="1">ชื่อ-นามสกุล</th>
													<th data-priority="1">อีเมล</th>
													<th data-priority="3">เบอร์ติดต่อ</th>
													<th data-priority="3">ประเภท</th>
													<th data-priority="3">สถานะ</th>
													<th data-priority="3">จัดการ</th>
												</tr>
											</thead>
											<tbody>
												<?php
									$numrow = 1;
									foreach($data as $row => $value)
									{//START FOREACH#1
										$id = $value->member_id;
										$approved = $value->approved;
										$member_mobileno = (!empty($value->member_mobileno))? $value->member_mobileno : '';
									?>
													<tr>
														<th>
                              <input type="checkbox" name="member_id[]" class="member_id" value="<?php echo $id; ?>">
															<?php echo $numrow; ?>
														</th>
														<td><?php echo $id; ?></td>
														<td>
															<?php echo $value->member_fname . ' ' .$value->member_lname; ?>
														</td>
														<td>
															<?php echo $value->member_email; ?>
														</td>
														<td>
															<?php echo $member_mobileno; ?>
														</td>
														<td>
															<?php echo $this->primaryclass->get_member_type($value->member_type_id); ?>
														</td>
														<td>
															<?php echo $this->primaryclass->get_approved($value->approved); ?>
														</td>
														<td>
															<input type="hidden" name="id" value="<?php echo $id; ?>">
															<a href="<?php echo site_url('member/edit_member/'.$id); ?>" class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit this Item"><i class="fa fa-pencil"></i></a>


															<?php if( $approved == 1){ //ไม่แสดงผล ต้องเปลี่ยนเป็นแสดงผล ?>
															<a href="#" class="btn btn-approved btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="<?php echo $id; ?>"><i class="zmdi zmdi-lock-open"></i></a>
															<?php } else {//แสดงผล ต้องเปลี่ยนเป็นไม่แสดงผล ?>
															<a href="#" class="btn btn-unapproved btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="<?php echo $id; ?>"><i class="zmdi zmdi-lock-outline"></i></a>
															<?php }//END IF APPROVED ?>

															<a href="#" id="<?php echo $id; ?>" class="btn btn-delete btn-icon waves-effect waves-light btn-danger m-b-5 m-r-5"><i class="ti-trash"></i></a>
														</td>

													</tr>
													<?php $numrow++; }//END FOREACH#1 ?>
											</tbody>
										</table>
                  </form>
							</div>
						</div>


				</div>
				<!-- End row -->

			</div>
			<!-- container -->

		</div>
		<!-- content -->


		<!-- ============================================================== -->
		<!-- End Right content here -->
		<!-- ============================================================== -->
		<?php $this->load->view('backoffice/partials/footer')?>
		<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
		<script>
	$(document).ready(function(){
		$('#datatable-condo').DataTable({
			dom: 'Bfrtip',
			buttons: [
				{
					extend: 'csvHtml5',
					exportOptions: {
						// columns: [ 0, 2, 3, 4, 5, 6 ]
						columns: ':visible'
					}
				},
				{
					extend: 'excelHtml5',
					exportOptions: {
						// columns: [ 0, 2, 3, 4, 5, 6 ]
						columns: ':visible'
					}
				},
				'colvis'
			],
			responsive: true,
			lengthMenu: [[20, 35, 60, -1], [20, 35, 60, "All"]],
			columnDefs: [
				{
				targets: 0,
				orderable: false
				},

			],
		});
	});
	</script>

			<script>
		$(".btn-delete").click(function(event){
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you delete",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('member/delete_member/'); ?>"+id,
						type: "POST",
						data: id,
						cache:false,
						success: function (data)
						{
							//console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				event.preventDefault();
				return false;
			});

		});

		$(".btn-approved").click(function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you Approved",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, approved it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('member/approved_member/'); ?>"+id,
						type: "POST",
						data: {
							member_id : id,
							approved : '2',
						},
						cache:false,
						success: function (data)
						{
//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				return false;
			});
		});

		$(".btn-unapproved").click(function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you unapproved",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Unapproved it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('member/unapproved_member/'); ?>"+id,
						type: "POST",
						data: {
							member_id : id,
							approved : '1',
						},
						cache:false,
						success: function (data)
						{
//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}

				return false;
			});
		});

    $(".delete_all").click(function(event){
      event.preventDefault();
      var member_id = $( "form" ).serialize();
      // var member_id = $( "input[name='member_id[]']" ).val();
      swal({
        title: "Warning Data!",
        text: "Confirm you delete",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Delete All it!",
        cancelButtonText: "No, Cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false }, function(isConfirm){
        if (isConfirm)
        {
          // console.log(member_id);
          $.ajax({
            url: "<?php echo base_url('member/delete_multi_member/'); ?>",
            type: "POST",
            data: member_id,
            cache:false,
            success: function (data)
            {
              // console.log(data);
              window.location.reload();
            },
            error: function (xhr, desc, err)
            {
              console.log( err );
            }
          });
          return false;
        }
        else
        {
          swal.close();
          return false;
        }

        return false;
      });
    });

    $(".approved_all").click(function(event){
      event.preventDefault();
      var member_id = $( "form" ).serialize();
      // var member_id = $( "input[name='member_id[]']" ).val();
      swal({
        title: "Warning Data!",
        text: "Confirm you approved",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Approved all it!",
        cancelButtonText: "No, Cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false }, function(isConfirm){
        if (isConfirm)
        {
          // console.log(member_id);
          $.ajax({
            url: "<?php echo base_url('member/approved_multi_member/'); ?>",
            type: "POST",
            data: member_id,
            cache:false,
            success: function (data)
            {
              // console.log(data);
              window.location.reload();
            },
            error: function (xhr, desc, err)
            {
              console.log( err );
            }
          });
          return false;
        }
        else
        {
          swal.close();
          return false;
        }

        return false;
      });
    });

    $(".unapproved_all").click(function(event){
      event.preventDefault();
      var member_id = $( "form" ).serialize();
      // var member_id = $( "input[name='member_id[]']" ).val();
      swal({
        title: "Warning Data!",
        text: "Confirm you approved",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Approved all it!",
        cancelButtonText: "No, Cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false }, function(isConfirm){
        if (isConfirm)
        {
          // console.log(member_id);
          $.ajax({
            url: "<?php echo base_url('member/unapproved_multi_member/'); ?>",
            type: "POST",
            data: member_id,
            cache:false,
            success: function (data)
            {
              // console.log(data);
              window.location.reload();
            },
            error: function (xhr, desc, err)
            {
              console.log( err );
            }
          });
          return false;
        }
        else
        {
          swal.close();
          return false;
        }

        return false;
      });
    });


	</script>
