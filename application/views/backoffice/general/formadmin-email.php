<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

//var_dump($data['row']);
if(isset( $data['row'] ))
{
	$action 					= "edit";

	$site_option_id 					= $data['row']['site_option_id'];
	$site_email 	= explode(",",  $data['row']['site_email']);
}
else
{
	$action 					= "add";
	$site_option_id 				= "";
	$site_email 				= "";
}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">

							<div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
									<i class="zmdi zmdi-more-vert"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</div>

							<h4 class="header-title m-t-0 m-b-30">แบบฟอร์ม</h4>


							<div class="row">
								<div class="col-lg-12">

									<form action="" id="frmdata" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
										<input type="hidden" name="site_option_id" id="site_option_id" value="<?php echo $site_option_id; ?>">
										<div class="form-group">
											<label class="col-md-2 control-label" for="site_email_1">อีเมล์ : 1</label>
											<div class="col-md-4">
												<div class="input-group">
													<input type="email" id="site_email_1" name="site_email[]" value="<?php echo (!empty($site_email[0]))? $site_email[0] : ''; ?>" class="form-control" placeholder="Email">
													<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
												</div>
											</div>

											<label class="col-md-2 control-label" for="site_email_2">อีเมล์ : 2</label>
											<div class="col-md-4">
												<div class="input-group">
													<input type="email" id="site_email_2" name="site_email[]" value="<?php echo (!empty($site_email[1]))? $site_email[1] : ''; ?>" class="form-control" placeholder="Email">
													<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
												</div>
											</div>
										</div>
										<!-- end col -->

										<div class="form-group">
											<label class="col-md-2 control-label" for="site_email_3">อีเมล์ : 3</label>
											<div class="col-md-4">
												<div class="input-group">
													<input type="email" id="site_email_3" name="site_email[]" value="<?php echo (!empty($site_email[2]))? $site_email[2] : ''; ?>" class="form-control" placeholder="Email">
													<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
												</div>
											</div>

											<label class="col-md-2 control-label" for="site_email_4">อีเมล์ : 4</label>
											<div class="col-md-4">
												<div class="input-group">
													<input type="email" id="site_email_4" name="site_email[]" value="<?php echo (!empty($site_email[3]))? $site_email[3] : ''; ?>" class="form-control" placeholder="Email">
													<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
												</div>
											</div>
										</div>
										<!-- end col -->

										<div class="form-group">
											<label class="col-md-2 control-label" for="site_email_5">อีเมล์ : 5</label>
											<div class="col-md-4">
												<div class="input-group">
													<input type="email" id="site_email_5" name="site_email[]" value="<?php echo (!empty($site_email[4]))? $site_email[4] : ''; ?>" class="form-control" placeholder="Email">
													<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
												</div>
											</div>

											<label class="col-md-2 control-label" for="site_email_6">อีเมล์ : 6</label>
											<div class="col-md-4">
												<div class="input-group">
													<input type="email" id="site_email_6" name="site_email[]" value="<?php echo (!empty($site_email[5]))? $site_email[5] : ''; ?>" class="form-control" placeholder="Email">
													<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
												</div>
											</div>
										</div>
										<!-- end col -->


										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10 m-t-15">
												<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light">
													ยืนยัน
												</button>
												<a href="#" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">
													ยกเลิก
												</a>
												<input type="hidden" name="action" id="action" value="action">
											</div>
										</div>

									</form>
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->

		<!-- ============================================================== -->
		<!-- End Right content here -->
		<!-- ============================================================== -->
		<?php $this->load->view('backoffice/partials/footer')?>

	<script>

$(document).ready(function () {



	$("form#frmdata").submit(function(event){
		var formData = new FormData($(this)[0]);

		<?php
		$ajax_url = "backoffice/site_email";
		$msg = ($action === 'add')? "Insert" :  "Update";
		?>
		var url = "<?php echo base_url().$ajax_url; ?>";

		var formData = new FormData($(this)[0]);

		$.ajax({
			url: url,
			type: "POST",
			data: formData,
			cache: false,
			processData: false,
			contentType: false,
			context: this,
			success: function (data, status)
			{
				//console.log( data );
				swal({
					title: "Success Data!",
					text: "Confirm you save",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, <?php echo $msg; ?> it!",
					cancelButtonText: "No, cancel plx!",
					closeOnConfirm: true,
					closeOnCancel: true }, function(isConfirm){
					if (isConfirm)
					{
						window.location.reload();
					}
					else
					{
						window.location.reload();
					}
				});

			},
			error: function (xhr, desc, err)
			{
				console.log( err );
			},

		});
		event.preventDefault();
		return false;
	});



});




			</script>

