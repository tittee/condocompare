<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

//var_dump($data['row']);
if(isset( $data['row'] ))
{
	$action = 'edit';
	$site_option_id 								= $data['row']['site_option_id'];
	$site_facebook 								= $data['row']['site_facebook'];
	$site_twitter 								= $data['row']['site_twitter'];
	$site_youtube 								= $data['row']['site_youtube'];
}
else
{
	$action = 'add';
	$site_option_id 				= "";
	$site_facebook 					= "";
	$site_twitter 					= "";
	$site_youtube 					= "";
}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">

							<div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
									<i class="zmdi zmdi-more-vert"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</div>

							<h4 class="header-title m-t-0 m-b-30">แบบฟอร์ม</h4>


							<div class="row">
								<div class="col-lg-12">

									<form action="" id="frmdata" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
										<input type="hidden" name="site_option_id" id="site_option_id" value="<?php echo $site_option_id; ?>">
										<div class="form-group">
											<label class="col-md-2 control-label" for="site_facebook">Facebook</label>
											<div class="col-md-10">
												<div class="input-group">
													<input type="text" id="site_facebook" name="site_facebook" value="<?php echo $site_facebook; ?>" class="form-control" placeholder="">
													<span class="input-group-addon"><i class="zmdi zmdi-facebook"></i></span>
												</div>
											</div>
										</div>
										<!-- end col -->

										<div class="form-group">
											<label class="col-md-2 control-label" for="site_logo">Youtube</label>
											<div class="col-md-10">
												<div class="input-group">
													<input type="text" id="site_youtube" name="site_youtube" value="<?php echo $site_youtube; ?>" class="form-control" placeholder="">
													<span class="input-group-addon"><i class="zmdi zmdi-youtube"></i></span>
												</div>
											</div>
										</div>
										<!-- end col -->

										<div class="form-group">
											<label class="col-md-2 control-label" for="site_logo">Twitter</label>
											<div class="col-md-10">
												<div class="input-group">
													<input type="text" id="site_twitter" name="site_twitter" value="<?php echo $site_twitter; ?>" class="form-control" placeholder="">
													<span class="input-group-addon"><i class="zmdi zmdi-twitter"></i></span>
												</div>
											</div>
										</div>
										<!-- end col -->

										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10 m-t-15">
												<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light">ยืนยัน</button>
												<a href="#" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">ยกเลิก</a>
												<input type="hidden" name="action" id="action" value="action">
											</div>
										</div>

									</form>
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->

		<!-- ============================================================== -->
		<!-- End Right content here -->
		<!-- ============================================================== -->
		<?php $this->load->view('backoffice/partials/footer')?>

	<script>

$(document).ready(function () {



	$("form#frmdata").submit(function(event){
		var formData = new FormData($(this)[0]);

		<?php
		$ajax_url = "backoffice/site_social";
		$msg = ($action === 'add')? "Insert" :  "Update";
		?>
		var url = "<?php echo base_url().$ajax_url; ?>";

		var formData = new FormData($(this)[0]);

		$.ajax({
			url: url,
			type: "POST",
			data: formData,
			cache: false,
			processData: false,
			contentType: false,
			context: this,
			success: function (data, status)
			{
				console.log( data );
				swal({
					title: "Success Data!",
					text: "Confirm you save",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, <?php echo $msg; ?> it!",
					cancelButtonText: "No, cancel plx!",
					closeOnConfirm: true,
					closeOnCancel: true }, function(isConfirm){
					if (isConfirm)
					{
						//window.location.reload();
					}
					else
					{
						window.location.reload();
					}
				});

			},
			error: function (xhr, desc, err)
			{
				console.log( err );
			},

		});
		event.preventDefault();
		return false;
	});


});
			</script>
