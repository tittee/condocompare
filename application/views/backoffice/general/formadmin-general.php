<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

//var_dump($data['row']);
if(isset( $data['row'] ))
{
	$action 					= "edit";
	$site_option_id 					= $data['row']['site_option_id'];
	$site_logo 								= $data['row']['site_logo'];
	$site_favicon 						= $data['row']['site_favicon'];
}
else
{
	$action 					= "add";
	$site_option_id 	= "";
	$site_logo 				= "";
	$site_favicon			= "";
}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">

							<div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
									<i class="zmdi zmdi-more-vert"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</div>

							<h4 class="header-title m-t-0 m-b-30">แบบฟอร์ม</h4>


							<div class="row">
								<div class="col-lg-12">

									<form action="" id="frmdata" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
										<input type="hidden" name="site_option_id" id="site_option_id" value="<?php echo $site_option_id; ?>">
										<input type="hidden" name="old_site_logo" id="old_site_logo" value="<?php echo $site_logo; ?>">
										<input type="hidden" name="old_site_favicon" id="old_site_favicon" value="<?php echo $site_favicon; ?>">

										<div class="form-group">
											<label class="col-md-2 control-label" for="site_logo">รูปโลโก้</label>
											<div class="col-md-4">
												<input type="file" name="site_logo"
													id="site_logo"
													class="dropify"
													data-max-file-size="1M"
													data-default-file="<?php echo (!empty($site_logo))? base_url().'uploads/site_setting/'.$site_logo : '';?>" />
												<span class="help-block"><small>ขนาดรูปภาพไม่เกิน 1 MB และ <span class="label label-warning"> กว้าง : 220px X ยาว : 100px </span></small></span>
											</div>
										</div>

										<div class="form-group">
											<label class="col-md-2 control-label" for="site_favicon">รูป favicon</label>
											<div class="col-md-4">
												<input type="file" name="site_favicon"
													id="site_favicon"
													class="dropify"
													data-max-file-size="1M"
													data-default-file="<?php echo (!empty($site_favicon))? base_url().'uploads/site_setting/'.$site_favicon : '';?>" />
													<span class="help-block"><small>ขนาดรูปภาพไม่เกิน 1 MB และ <span class="label label-warning"> กว้าง : 16px X ยาว : 16px </span></small></span>
											</div>
										</div>
										<!-- end col -->


										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10 m-t-15">
												<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light">
													ยืนยัน
												</button>
												<a href="#" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">
													ยกเลิก
												</a>
											 <input type="hidden" name="action" value="action">
											</div>
										</div>

									</form>
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->

		<!-- ============================================================== -->
		<!-- End Right content here -->
		<!-- ============================================================== -->
		<?php $this->load->view('backoffice/partials/footer')?>

<script>
$(document).ready(function () {
	$("form#frmdata").submit(function(event){
		var formData = new FormData($(this)[0]);

		<?php
		$ajax_url = "backoffice/site_general";
		$msg = ($action === 'add')? "Insert" :  "Update";
		?>
		var url = "<?php echo base_url().$ajax_url; ?>";

		var formData = new FormData($(this)[0]);
		//console.log(formData);
		$.ajax({
			url: url,
			type: "POST",
			data: formData,
			cache: false,
			processData: false,
			contentType: false,
			context: this,
			success: function (data, status)
			{
				console.log( data );
				swal({
					title: "Success Data!",
					text: "Confirm you save",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, <?php echo $msg; ?> it!",
					cancelButtonText: "No, cancel plx!",
					closeOnConfirm: true,
					closeOnCancel: true }, function(isConfirm){
					if (isConfirm)
					{
						window.location.reload();
					}
					else
					{
						window.location.reload();
					}
				});

			},
			error: function (xhr, desc, err)
			{
				console.log( err );
			},

		});
		event.preventDefault();
		return false;
	});

});


	var site_option_id = $("#site_option_id").val();
	$('.dropify').dropify({
		messages: {
				'default': 'Drag and drop a file here or click',
				'replace': 'Drag and drop or click to replace',
				'remove': 'Remove',
				'error': 'Ooops, something wrong appended.'
		},
		error: {
				'fileSize': 'The file size is too big (1M max).'
		}
	});

	var drEvent = $('.dropify').dropify();
	drEvent.on('dropify.beforeClear', function(event, element){

		//console.log(element.file.name);
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('backoffice/remove_image_update'); ?>",
			data: {
				"site_option_id": site_option_id,
				"imagefile": element.file.name,
				"field_name": $(this).attr("name"),
			},
			success: function(response) {
				console.log('ok success!');
			}
		}); //END AJX

		var r = confirm("Do you really want to delete \"" + element.file.name + "\" ?");
		if (r == true) {
			$( "#old_" + $('.dropify').attr('id') ).val( "" );
		} else {
			console.log('Cancel');
		}
		//return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
	});

	var drEvent = $('.dropify').dropify();
	drEvent.on('dropify.afterClear', function(event, element){
		window.location.reload();
		console.log('File delete success!');
	});
</script>
