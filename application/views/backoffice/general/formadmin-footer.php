<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

//var_dump($data['row']);
if(isset( $data['row'] ))
{
	$action = 'edit';
	$site_option_id 								= $data['row']['site_option_id'];
	$site_footer 								= $data['row']['site_footer'];
	$site_copyright 								= $data['row']['site_copyright'];
}
else
{
	$action = 'add';
	$site_option_id			= "";
	$site_footer 				= "";
	$site_copyright			= "";
}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">

							<div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
									<i class="zmdi zmdi-more-vert"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</div>

							<h4 class="header-title m-t-0 m-b-30">แบบฟอร์ม</h4>


							<div class="row">
								<div class="col-lg-12">

									<form action="" id="frmdata" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
										<input type="hidden" name="site_option_id" id="site_option_id" value="<?php echo $site_option_id; ?>">
										<div class="form-group">
											<label class="col-md-2 control-label">รายละเอียด footer</label>
											<div class="col-md-10">
												<textarea class="wysiwigeditor"  name="site_footer" id="site_footer"><?php echo $site_footer;?></textarea>
												<input name="image" type="file" id="upload_wysiwigeditor" class="hidden" onchange="">
											</div>
										</div>

										<div class="form-group">
											<label class="col-md-2 control-label">Copyright</label>
											<div class="col-md-10">
												<textarea class="wysiwigeditor"  name="site_copyright" id="site_copyright"><?php echo $site_copyright;?></textarea>
												<input name="image" type="file" id="upload_wysiwigeditor2" class="hidden" onchange="">
											</div>
										</div>


										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10 m-t-15">
												<button id="submit" type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light" onclick="tinyMCE.triggerSave(true,true);">ยืนยัน</button>
												<a href="#" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">ยกเลิก</a>
												<input type="hidden" name="action" id="action" value="action">
											</div>
										</div>

									</form>
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->

		<!-- ============================================================== -->
		<!-- End Right content here -->
		<!-- ============================================================== -->
		<?php $this->load->view('backoffice/partials/footer')?>

	<script>

$(document).ready(function () {

	$("form#frmdata").submit(function(event){
		var formData = new FormData($(this)[0]);

		<?php
		$ajax_url =  "backoffice/site_footer";
		$msg = ($action === 'add')? "Insert" :  "Update";
		?>
		var url = "<?php echo base_url().$ajax_url; ?>";

		var formData = new FormData($(this)[0]);

		$.ajax({
			url: url,
			type: "POST",
			data: formData,
			cache: false,
			processData: false,
			contentType: false,
			context: this,
			success: function (data, status)
			{
//				console.log( data );
				swal({
					title: "Success Data!",
					text: "Confirm you save",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, <?php echo $msg; ?> it!",
					cancelButtonText: "No, cancel plx!",
					closeOnConfirm: true,
					closeOnCancel: true }, function(isConfirm){
					if (isConfirm)
					{
						window.location.reload();
					}
					else
					{
						window.location.reload();
					}
				});

			},
			error: function (xhr, desc, err)
			{
				console.log( err );
			},

		});
		event.preventDefault();
		return false;
	});
});
</script>


