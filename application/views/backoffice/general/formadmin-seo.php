<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

//var_dump($data['row']);
if(isset( $data['row'] ))
{
	$action = 'edit';
	$site_option_id 								= $data['row']['site_option_id'];
	$site_seo_title 								= $data['row']['site_seo_title'];
	$site_seo_meta 								= $data['row']['site_seo_meta'];
	$site_seo_description 								= $data['row']['site_seo_description'];
}
else
{
	$action = 'add';
	$site_option_id = "";
	$site_seo_title = "";
	$site_seo_meta 	= "";
	$site_seo_description	= "";
}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">

							<div class="dropdown pull-right">
								<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
									<i class="zmdi zmdi-more-vert"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</div>

							<h4 class="header-title m-t-0 m-b-30">แบบฟอร์ม</h4>


							<div class="row">
								<div class="col-lg-12">

									<form action="" id="frmdata" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
										<input type="hidden" name="site_option_id" id="site_option_id" value="<?php echo $site_option_id; ?>">
										<div class="form-group">
											<label class="col-md-2 control-label" for="site_logo">Meta Title</label>ุ
											<div class="col-md-10">
												<input type="text" name="site_seo_title" id="site_seo_title" class="alloptions" value="<?php echo $site_seo_title; ?>" maxlength="100" data-role="tagsinput" placeholder="กรอกข้อมูล" />
												<span class="help-block"><small>กรอกรายละเอียดข้อความ ตามด้วย &#40;,&#41; <span class="label label-warning"> ไม่เกิน 100 ตัวอักษร</span></small>
												</span>
											</div>
										</div>

										<div class="form-group">
											<label class="col-md-2 control-label" for="site_logo">Meta Keywords</label>
											<div class="col-md-10">
												<input type="text" class="alloptions" name="site_seo_meta" id="site_seo_meta" value="<?php echo $site_seo_meta; ?>" maxlength="225" data-role="tagsinput" placeholder="กรอกข้อมูล" />
												<span class="help-block"><small>กรอกรายละเอียดข้อความ ตามด้วย &#40;,&#41; <span class="label label-warning"> ไม่เกิน 255 ตัวอักษร</span></small>
												</span>
											</div>
										</div>
										<!-- end col -->

										<div class="form-group">
											<label class="col-md-2 control-label" for="site_logo">Meta Description</label>
											<div class="col-md-10">
												<textarea id="textarea site_seo_description" name="site_seo_description" class="form-control alloptions" maxlength="225" rows="2" placeholder="กรอกข้อมูล ไม่เกิน 255 ตัวอักษร"><?php echo $site_seo_description; ?></textarea>
												<span class="help-block"><small>กรอกรายละเอียดข้อความ <span class="label label-warning"> ไม่เกิน 255 ตัวอักษร</span></small>
												</span>
											</div>
										</div>
										<!-- end col -->


										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10 m-t-15">
												<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light">
													ยืนยัน
												</button>
												<a href="#" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">
													ยกเลิก
												</a>
												<input type="hidden" name="action" id="action" value="action">
											</div>
										</div>

									</form>
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->

		<!-- ============================================================== -->
		<!-- End Right content here -->
		<!-- ============================================================== -->
		<?php $this->load->view('backoffice/partials/footer')?>

			<script>
				$(document).ready(function () {

					// Select2
					$('.alloptions').maxlength({
						alwaysShow: true,
						warningClass: "label label-success",
						limitReachedClass: "label label-danger",
						separator: ' out of ',
						preText: 'You typed ',
						postText: ' chars available.',
						validate: true
					});


					$("form#frmdata").submit(function (event) {
						var formData = new FormData($(this)[0]);

						<?php
		$ajax_url = "backoffice/site_seo";
		$msg = ($action === 'add')? "Insert" :  "Update";
		?>
						var url = "<?php echo base_url().$ajax_url; ?>";

						var formData = new FormData($(this)[0]);

						$.ajax({
							url: url,
							type: "POST",
							data: formData,
							cache: false,
							processData: false,
							contentType: false,
							context: this,
							success: function (data, status) {
//												console.log( data );
								swal({
									title: "Success Data!",
									text: "Confirm you save",
									type: "warning",
									showCancelButton: true,
									confirmButtonColor: "#DD6B55",
									confirmButtonText: "Yes, <?php echo $msg; ?> it!",
									cancelButtonText: "No, cancel plx!",
									closeOnConfirm: true,
									closeOnCancel: true
								}, function (isConfirm) {
									if (isConfirm) {
										window.location.reload();
									} else {
										window.location.reload();
									}
								});

							},
							error: function (xhr, desc, err) {
								console.log(err);
							},

						});
						event.preventDefault();
						return false;
					});
				});
			</script>
