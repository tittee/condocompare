<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'js'=>isset($js)?$js:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');
?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">


									<form action="">
							<a href="<?php echo $url_href ?>" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">
								<i class="zmdi zmdi-account-add"></i> ADD</a>

										<div class="table-rep-plugin">
								<div class="table-responsive" data-pattern="priority-columns">
									<table id="datatable-responsive" class="table table-striped table-bordered display responsive nowrap" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th data-priority="1">ลำดับ</th>
													<th data-priority="3">รูป</th>
													<th data-priority="1">ชื่อหมวดข่าวสาร</th>
													<th data-priority="3">สถานะ</th>
													<th data-priority="3">วันที่สร้าง / แก้ไข </th>
													<th data-priority="6">จัดการ</th>
												</tr>
											</thead>
											<tbody>
												<?php

									$numrows = 1;
									foreach($data as $row => $value)
									{//START FOREACH#1
										$sfilepath = base_url().'/uploads/news';
										$pic_thumb =  $value->news_category_pic_thumb;
										$pic_thumb_t = ( !empty($pic_thumb) )? $sfilepath.'/'.$value->news_category_pic_thumb : 'http://placekitten.com/200/200';
										$pubilsh = $value->publish;
										$createdate = $value->createdate;
										$updatedate = $value->updatedate;
										$createdate_t = ($createdate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
										$updatedate_t = ($updatedate != '0000-00-00 00:00:00')? $this->dateclass->DateTimeShortFormat($updatedate, 0, 0, "Th") : 'ยังไม่มีข้อมูล';
									?>
													<tr>
														<td>
															<?php echo $numrows; ?>
														</td>
														<td><img src="<?php echo $pic_thumb_t;?>" style="width: 150px;" class="img-thumbnail" alt=""></td>
														<td>
															<?php echo $value->news_category_name; ?>
														</td>
														<td>
															<?php echo $this->primaryclass->get_publish($pubilsh); ?>
														</td>
														<td>
																สร้าง : <?php echo $createdate_t; ?>
																<br>
																แก้ไข : <?php echo $updatedate_t; ?>
														</td>
														<td>
															<input type="hidden" name="id" value="<?php echo $value->news_category_id; ?>">
															<a href="<?php echo site_url('news/edit_news_category/'.$value->news_category_id); ?>" class="btn btn-icon waves-effect waves-light btn-warning m-b-5 m-r-5"><i class="ti-pencil"></i></a>
															<a href="#" id="<?php echo $value->news_category_id; ?>" class="btn btn-delete btn-icon waves-effect waves-light btn-danger m-b-5 m-r-5"><i class="ti-trash"></i></a>

															<?php if( $pubilsh == 1){ //ไม่แสดงผล ต้องเปลี่ยนเป็นแสดงผล ?>
															<a href="<?php echo site_url('news/display_news_category/'.$value->news_category_id); ?>" class="btn btn-publish btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="<?php echo $value->news_category_id; ?>"><i class="zmdi zmdi-globe-alt"></i></a>
															<?php } else {//แสดงผล ต้องเปลี่ยนเป็นไม่แสดงผล ?>
															<a href="<?php echo site_url('news/undisplay_news_category/'.$value->news_category_id); ?>" class="btn btn-unpublish btn-icon waves-effect waves-light btn-custom m-b-5 m-r-5" id="<?php echo $value->news_category_id; ?>"><i class="zmdi zmdi-globe-lock"></i></a>
															<?php }//END IF PUBLISH ?>
														</td>
													</tr>
													<?php ++$numrows; }//END FOREACH#1 ?>
											</tbody>

										</table>
											</div>
										</div>

										</form>
						</div>
					</div>
				</div>
				<!-- End row -->

			</div>
			<!-- container -->

		</div>
		<!-- content -->


	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
	<script>
		$(".btn-delete").click(function(event){
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you delete",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('news/delete_news_category/'); ?>"+id,
						type: "POST",
						data: id,
						cache:false,
						success: function (data)
						{
							//console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				event.preventDefault();
				return false;
			});

		});

		$(".btn-publish").click(function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you publish",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Publish it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('news/display_news_category/'); ?>"+id,
						type: "POST",
						data: {
							news_category_id : id,
							publish : '1',
						},
						cache:false,
						success: function (data)
						{
//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}
				return false;
			});
		});

		$(".btn-unpublish").click(function(event){
			event.preventDefault();
			var id = $(this).attr("id");
			swal({
				title: "Warning Data!",
				text: "Confirm you unpublish",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, UnPublish it!",
				cancelButtonText: "No, Cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false }, function(isConfirm){
				if (isConfirm)
				{
					$.ajax({
						url: "<?php echo base_url('news/undisplay_news_category/'); ?>"+id,
						type: "POST",
						data: {
							news_category_id : id,
							publish : '1',
						},
						cache:false,
						success: function (data)
						{
//							console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
					return false;
				}
				else
				{
					swal.close();
					return false;
				}

				return false;
			});
		});

	</script>
