<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'js'=>isset($js)?$js:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');

if(isset( $data['row'] ))
{
	$action = 'edit';
	$activities_id 								= $data['row']->activities_id;
	$fk_activities_category_id 								= $data['row']->fk_activities_category_id;

	$activities_title 						= $data['row']->activities_title;
	$activities_days 						= $data['row']->activities_days;
	$activities_caption 					= $data['row']->activities_caption;
	$activities_description 					= $data['row']->activities_description;

	$activities_pic_thumb 								= $data['row']->activities_pic_thumb;
	$activities_pic_large 								= $data['row']->activities_pic_large;

	$meta_title 					= $data['row']->meta_title;
	$meta_keyword 					= $data['row']->meta_keyword;
	$meta_description 					= $data['row']->meta_description;


	//Activities Date
	$activities								= (!empty($data['row']->activities))? $data['row']->activities : date("Y-m-d H:i:s");
	$activities_t = explode(" ", $activities);
	$activities_date = $activities_t[0];
	$activities_time = $activities_t[1];

	//CreateDate
	$createdate								= (!empty($data['row']->createdate))? $data['row']->createdate : date("Y-m-d H:i:s");
	$createdate_t = explode(" ", $createdate);
	$createdate_date = $createdate_t[0];
	$createdate_time = $createdate_t[1];

	//UpdateDate
	$updatedate								= (!empty($data['row']->updatedate))? $data['row']->updatedate : date("Y-m-d H:i:s");
	$updatedate_t = explode(" ", $updatedate);
	$updatedate_date = $updatedate_t[0];
	$updatedate_time = $updatedate_t[1];

	$createby 					= $data['row']->createby;
	$updateby 					= $data['row']->updateby;

	$highlight 					= $data['row']->highlight;
	$recommended 					= $data['row']->recommended;
	$publish 					= $data['row']->publish;
}
else
{
	$action = 'add';
	$fk_activities_category_id 								= "";

	$activities_title 							= "";
	$activities_days 							= "";
	$activities_caption 					= "";
	$activities_description 			= "";

	$activities_pic_thumb 								= "";
	$activities_pic_large 								= "";

	$meta_title 					= "";
	$meta_keyword 				= "";
	$meta_description 		= "";

	$createby 					= "";
	$updateby 					= "";

	//Activities
	$activities								=  date("Y-m-d H:i:s");
	$activities_t = explode(" ", $activities);
	$activities_date = $activities_t[0];
	$activities_time = $activities_t[1];

	//CreateDate
	$createdate								=  date("Y-m-d H:i:s");
	$createdate_t = explode(" ", $createdate);
	$createdate_date = $createdate_t[0];
	$createdate_time = $createdate_t[1];


	//Modify Date
	$updatedate								= date("Y-m-d H:i:s");
	$updatedate_t = explode(" ", $updatedate);
	$updatedate_date = $updatedate_t[0];
	$updatedate_time = $updatedate_t[1];


	$highlight 				= 1;
	$recommended 			= 1;
	$publish 					= 1;

}
//var_dump( $data['row'] );
?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<div class="row">
					<div class="col-sm-12">

						<?php if(isset( $data['row'] ))
									{
								?>
						<form id="frmdata" action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
							<input type="hidden" name="activities_id" value="<?php echo $activities_id; ?>">
							<input type="hidden" name="old_activities_pic_thumb" value="<?php echo $activities_pic_thumb; ?>">
							<input type="hidden" name="old_activities_pic_large" value="<?php echo $activities_pic_large; ?>">
							<?php }
									else
									{
								?>
						<form id="frmdata" action="" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
							<?php } ?>
						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">รายละเอียด</h4>
							<div class="row">
								<div class="col-lg-12">

									<div class="form-group">
										<label class="col-md-2 control-label">ชื่อ</label>
										<div class="col-md-10">
											<input type="text" name="activities_title" maxlength="150" class="inputmaxlength form-control" value="<?php echo $activities_title; ?>" maxlength="100" placeholder="" required="">
										</div>
									</div>
									<!-- end from-group -->

									<div class="form-group">
										<label class="control-label col-sm-2">วันที่เริ่มกิจกรรม</label>
										<div class="col-sm-2">
											<div class="input-group">
												<input type="text" name="activities_date" class="form-control datepicker-autoclose" placeholder="" value="<?php echo $activities_date; ?>" id="">
												<span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
											</div><!-- input-group -->
										</div>
										<div class="col-sm-2">
											<div class="input-group">
												<div class="bootstrap-timepicker">
													<input id="" type="text" name="activities_time" class="timepicker form-control" value="<?php echo $activities_time; ?>">
												</div>
												<span class="input-group-addon bg-primary b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>
											</div>
										</div>

										<label class="control-label col-sm-2">ระยะเวลากิจกรรม</label>
										<div class="col-sm-4">
												<input class="numeric_picker" type="text" value="<?php echo $activities_days; ?>" name="activities_days">
										</div>
									</div>


									<div class="form-group">
										<label class="control-label col-sm-2">วันที่สร้าง</label>
										<div class="col-sm-2">
											<div class="input-group">
												<input type="text" name="createdate_date" class="form-control datepicker-autoclose" placeholder="" value="<?php echo $createdate_date; ?>" id="">
												<span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
											</div><!-- input-group -->
										</div>
										<div class="col-sm-2">
											<div class="input-group">
												<div class="bootstrap-timepicker">
													<input id="" type="text" name="createdate_time" class="timepicker form-control" value="<?php echo $createdate_time; ?>">
												</div>
												<span class="input-group-addon bg-primary b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>
											</div>
										</div>

										<label class="control-label col-sm-2">วันที่แก้ไข</label>
										<div class="col-sm-2">
											<div class="input-group">
												<input type="text" name="updatedate_date" class="form-control datepicker-autoclose" placeholder="" value="<?php echo $updatedate_date; ?>" id="">
												<span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
											</div><!-- input-group -->
										</div>
										<div class="col-sm-2">
											<div class="input-group">
												<div class="bootstrap-timepicker">
													<input id="" name="updatedate_time" type="text" class="timepicker form-control" value="<?php echo $updatedate_time; ?>">
												</div>
												<span class="input-group-addon bg-primary b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>
											</div>
										</div>
									</div>


									<div class="form-group">
										<label class="col-md-2 control-label">การแสดงผล</label>
										<div class="col-md-2">
											<div class="radio-inline radio radio-success radio-single">
												<input type="radio" name="publish" value="2" required <?php echo ($publish == 2)? 'checked': ''; ?>>
												<label for="">
													แสดงผล
												</label>
											</div>
										</div>

										<div class="col-md-2">
											<div class="radio-inline radio radio-success radio-single">
												<input type="radio" name="publish" value="1" required <?php echo ($publish == 1)? 'checked': ''; ?>>
												<label for="">
													ไม่แสดงผล
												</label>
											</div>
										</div>
									</div>
									<!-- end from-group -->

								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->


						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">คุณลักษณะกิจกรรม</h4>
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<label class="col-md-2 control-label">กิจกรรมแนะนำ</label>
										<div class="col-md-2">
											<div class="radio-inline radio radio-success radio-single">
												<input type="radio" name="recommended" value="2" required <?php echo ($recommended == 2)? 'checked': ''; ?>>
												<label for="">
													กิจกรรมแนะนำ
												</label>
											</div>
										</div>

										<div class="col-md-2">
											<div class="radio-inline radio radio-success radio-single">
												<input type="radio" name="recommended" value="1" required <?php echo ($recommended == 1)? 'checked': ''; ?>>
												<label for="">
													ไม่ตั้งค่ากิจกรรมแนะนำ
												</label>
											</div>
										</div>
									</div>
									<!-- end from-group -->

									<!-- <div class="form-group">
										<label class="col-md-2 control-label">กิจกรรมพิเศษ</label>
										<div class="col-md-2">
											<div class="radio-inline radio radio-success radio-single">
												<input type="radio" name="highlight" value="2" required <?php echo ($highlight == 2)? 'checked': ''; ?>>
												<label for="">
													กิจกรรมพิเศษ
												</label>
											</div>
										</div>

										<div class="col-md-2">
											<div class="radio-inline radio radio-success radio-single">
												<input type="radio" name="highlight" value="1" required <?php echo ($highlight == 1)? 'checked': ''; ?>>
												<label for="">
													ไม่ตั้งค่ากิจกรรมพิเศษ
												</label>
											</div>
										</div>
									</div> -->
									<!-- end from-group -->

								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->

						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">รูปภาพ</h4>
							<div class="row">
								<div class="col-lg-12">


									<div class="form-group">
										<label class="col-md-2 control-label" for="activities_pic_thumb">รูปภาพขนาดเล็ก</label>
										<div class="col-md-10">
											<input type="file" name="activities_pic_thumb" />
											<?php if(!empty($activities_pic_thumb))
									{
							?>
												<img src="<?php echo base_url()?>uploads/activities/<?php echo $activities_pic_thumb; ?>" class="img-responsive" alt="">
												<?php } ?>
										</div>
									</div>
									<!-- end from-group -->

									<div class="form-group">
										<label class="col-md-2 control-label" for="activities_pic_large">รูปภาพขนาดใหญ่</label>
										<div class="col-md-10">
											<input type="file" name="activities_pic_large" />
											<?php if(!empty($activities_pic_large))
									{
							?>
												<img src="<?php echo base_url()?>uploads/activities/<?php echo $activities_pic_large; ?>" class="img-responsive" alt="">
												<?php } ?>
										</div>
									</div>
									<!-- end from-group -->
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->

						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">ข้อมูลกิจกรรม</h4>
							<div class="row">
								<div class="col-lg-12">

									<div class="form-group">
										<label class="col-md-2 control-label">คำอธิบายกิจกรรม</label>
										<div class="col-md-10">
											<textarea name="activities_caption" id="activities_caption" maxlength="255" class="inputmaxlength form-control" cols="30" rows="10"><?php echo $activities_caption; ?></textarea>
										</div>
									</div>
									<!-- end from-group -->

									<div class="form-group">
										<label class="col-md-2 control-label">รายละเอียดทั้งหมด</label>
										<div class="col-md-10">
											<textarea class="wysiwigeditor" name="activities_description"><?php echo $activities_description;?></textarea>
										</div>
									</div>
									<!-- end from-group -->

								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->

						<div class="card-box">
							<h4 class="header-title m-t-0 m-b-30">คำค้นหา</h4>
							<div class="row">
								<div class="col-lg-12">

									<div class="form-group">
										<label class="col-md-2 control-label">META TITLE</label>
										<div class="col-md-10">
											<textarea name="meta_title" id="meta_title" maxlength="100" class="inputmaxlength form-control" cols="30" rows="3"><?php echo $meta_title; ?></textarea>
											<span class="text-warning help-block"><small>กรอกไม่เกิน 100 ตัวอักษรและคั่นด้วย , เสมอ</small></span>
										</div>
									</div>
									<!-- end from-group -->

									<div class="form-group">
										<label class="col-md-2 control-label">META KEYWORD</label>
										<div class="col-md-10">
											<textarea name="meta_keyword" id="meta_keyword" maxlength="255" class="inputmaxlength form-control" cols="30" rows="3"><?php echo $meta_keyword; ?></textarea>
											<span class="text-warning help-block"><small>กรอกไม่เกิน 255 ตัวอักษรและคั่นด้วย , เสมอ</small></span>
										</div>
									</div>
									<!-- end from-group -->

									<div class="form-group">
										<label class="col-md-2 control-label">META TITLE</label>
										<div class="col-md-10">
											<textarea name="meta_description" id="meta_description" maxlength="255" class="inputmaxlength form-control" cols="30" rows="3"><?php echo $meta_description; ?></textarea>
											<span class="text-warning help-block"><small>กรอกไม่เกิน 255 ตัวอักษร</small></span>
										</div>
									</div>
									<!-- end from-group -->

								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->

						<div class="card-box">
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10 m-t-15">
											<button type="submit" class="btn btn-primary btn-lg btn-bordred waves-effect w-md waves-light" onclick="tinyMCE.triggerSave(true,true);">
												Submit
											</button>
											<a href="<?php echo site_url('activities/activities'); ?>" type="reset" class="btn btn-default btn-lg btn-bordred waves-effect w-md m-l-5">
													Cancel
												</a>
											<input type="hidden" name="action" value="action">
										</div>
									</div>
								</div>
								<!-- end col -->
							</div>
							<!-- end row -->
						</div>
						<!-- end card-box -->


						<?php if(isset( $data['row'] ))
									{
								?>
						</form>
						<?php }
									else
									{
								?>
						</form>
						<?php } ?>
						<!-- end form -->
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->


			</div>
			<!-- container -->

		</div>
		<!-- content -->

	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>
	<script>

		$(document).ready(function(){
			//Datepicker
			jQuery('.datepicker-autoclose').datepicker({
					autoclose: true,
					defaultDate: new Date(),
					format: 'yyyy-mm-dd',
					todayHighlight: true,
			});
			// Time Picker
			jQuery('.timepicker').timepicker({
				maxHours		: 12,
				defaultTIme : 'current',
				showSeconds : true,
				showMeridian: false,
			});
		});

		$("form#frmdata").submit(function(event){
			var formData = new FormData($(this)[0]);

			<?php
			$ajax_url = ($action === 'add')? "activities/add_activities" :  "activities/edit_activities/".$activities_id;
			$msg = ($action === 'add')? "Insert" :  "Update";
			?>
			var url = "<?php echo base_url().$ajax_url; ?>";

			var formData = new FormData($(this)[0]);

			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				cache: false,
				processData: false,
				contentType: false,
				context: this,
				success: function (data, status)
				{
					//console.log( data );
					swal({
						title: "Success Data!",
						text: "Confirm you save",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Yes, <?php echo $msg; ?> it!",
						cancelButtonText: "No, cancel plx!",
						closeOnConfirm: true,
						closeOnCancel: true }, function(isConfirm){
						if (isConfirm)
						{
							window.location.replace('<?php echo base_url(); ?>activities/activities');
						}
						else
						{
							window.location.reload();
						}
					});

				},
				error: function (xhr, desc, err)
				{
					console.log( err );
				},

			});
			event.preventDefault();
			return false;
		});
	</script>
