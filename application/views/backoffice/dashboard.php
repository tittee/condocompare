<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('backoffice/partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>'fixed-left'));

/*SIDEBAR*/
$this->load->view('backoffice/partials/sidebar');


?>

	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->

	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">


				<div class="row">

						<div class="col-lg-6">
							<div class="card-box">

								<h4 class="header-title m-t-0 m-b-30">จำนวนสมาชิก HOB</h4>

								<div class="widget-chart-1">
									<div class="widget-chart-box-1">
										<input class="hob-knob" value="<?php echo $data['all_hob_member']; ?>"  />
									</div>
									<div class="widget-detail-1">
										<h2 class="p-t-10 m-b-0"> <?php echo $data['all_hob_member_today']; ?></h2>
										<p class="text-muted">สมัครสมาชิกวันนี้</p>
									</div>
								</div>
	              <p class="text-warning">จำนวนเต็ม 1000 จากสมาชิกอนุมัติ</span>
							</div>
						</div>
						<!-- end col -->

	          <div class="col-lg-6">
							<div class="card-box">
								<h4 class="header-title m-t-0 m-b-30">จำนวนสมาชิก</h4>

								<div class="widget-chart-1">
									<div class="widget-chart-box-1">
	                  <input class="general-knob" value="<?php echo $data['all_general_member']; ?>"  />
									</div>
									<div class="widget-detail-1">
										<h2 class="p-t-10 m-b-0"> <?php echo $data['all_general_member_today']; ?> </h2>
										<p class="text-muted">สมัครสมาชิกวันนี้</p>
									</div>
								</div>
	              <p class="text-warning">จำนวนเต็ม 1000 จากสมาชิกอนุมัติ</span>
							</div>
						</div>
						<!-- end col -->
					</div>
					<!-- end row -->

					<div class="row">
						<div class="col-lg-6">
							<div class="card-box">

								<h4 class="header-title m-t-0 m-b-30">พนักงานขายที่มีคอนโดมากที่สุด</h4>

								<div class="widget-box-2">
									<div class="widget-detail-2">
	                  <?php
	                  $most_staff = $data['staff_most_condo']['most_staff'];
	                  $all_condo = $data['all_condo'];
	                  $most_staff_cal = ($most_staff * 100 ) / $all_condo;
	                  $most_staff_percent_complete =  100 - $most_staff_cal;
	                  ?>
										<span class="badge badge-success pull-left m-t-20"><?php echo number_format($most_staff_cal, 2); ?>% <i class="zmdi zmdi-trending-up"></i> </span>
										<h2 class="m-b-0"> <?php echo $most_staff; ?> </h2>
										<p class="text-muted m-b-25"> <?php echo $data['staff_most_condo']['fullname']; ?></p>
									</div>
									<div class="progress progress-bar-success-alt progress-sm m-b-0">
										<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo number_format($most_staff_percent_complete, 2); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo number_format($most_staff_percent_complete, 2); ?>%;">
											<span class="sr-only"><?php echo number_format($most_staff_percent_complete, 2); ?>% Complete</span>
										</div>
									</div>
								</div>ุ

							</div>
						</div>
						<!-- end col -->



						<div class="col-lg-6">
							<div class="card-box">

								<h4 class="header-title m-t-0 m-b-30">พนักงานขายที่มีทรัพย์มากที่สุด</h4>
	              <?php
	              $land_most_staff = $data['staff_most_land']['most_staff'];
	              $all_land = $data['all_land'];
	              $fullname = $data['staff_most_land']['fullname'];
	              $land_most_staff_cal = ($land_most_staff * 100 ) / $all_land;
	              $land_most_staff_percent_complete =  100 - $land_most_staff_cal;
	              ?>

								<div class="widget-box-2">
									<div class="widget-detail-2">
										<span class="badge badge-pink pull-left m-t-20"><?php echo number_format($land_most_staff_cal, 2); ?>% <i class="zmdi zmdi-trending-up"></i> </span>
										<h2 class="m-b-0"> <?php echo $land_most_staff; ?> </h2>
										<p class="text-muted m-b-25"><?php echo $fullname ?>;</p>
									</div>
									<div class="progress progress-bar-pink-alt progress-sm m-b-0">
										<div class="progress-bar progress-bar-pink" role="progressbar" aria-valuenow="<?php echo number_format($land_most_staff_percent_complete, 2); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo number_format($land_most_staff_percent_complete, 2); ?>%;">
											<span class="sr-only"><?php echo number_format($land_most_staff_percent_complete, 2); ?>% Complete</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- end col -->

				</div>
				<!-- end row -->


				<div class="row">

					<!-- end col -->

					<!-- end col -->

					<!-- <div class="col-lg-12">
						<div class="card-box">
							<h4 class="header-title m-t-0">คอนโดที่คนดูมากที่สุด</h4>
							<div id="morris-line-example" style="height: 280px;"></div>
						</div>
					</div> -->
					<!-- end col -->

				</div>
				<!-- end row -->
			</div>
			<!-- container -->

		</div>

	</div>
	<!-- content -->



	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	<?php $this->load->view('backoffice/partials/footer')?>

  <script type="text/javascript">
  jQuery(document).ready(function($){
    $('.hob-knob').knob({
        'min':0,
        'max': 1000,
        'width' : 80,
        'height' : 80,
        'fgColor' : '#f05050',
        'bgColor' : '#F9B9B9',
        'skin'  : "tron",
        'angleOffset '  : 180,
        'step': 1,
        'readOnly': true,
        'thickness': .15,
    });

    $('.general-knob').knob({
        'min':0,
        'max': 1000,
        'width' : 80,
        'height' : 80,
        'fgColor' : '#4A148C',
        'bgColor' : '#E1BEE7',
        'skin'  : "tron",
        'angleOffset '  : 180,
        'step': 1,
        'readOnly': true,
        'thickness': .15,
    });
  });

  </script>
