<!DOCTYPE html>
<html>
		<head>
				<meta charset="utf-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
				<meta name="author" content="Coderthemes">
				<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
				<title><?php echo $title ?></title>
				<link href="<?php echo base_url(); ?>assets/plugins/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />
				<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
				<link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
				<link href="<?php echo base_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css" />
				<link href="<?php echo base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
				<link href="<?php echo base_url(); ?>assets/css/pages.css" rel="stylesheet" type="text/css" />
				<link href="<?php echo base_url(); ?>assets/css/menu.css" rel="stylesheet" type="text/css" />
				<link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
				<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
				<link href="<?php echo base_url(); ?>assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
				<link href="<?php echo base_url(); ?>assets/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
				<link href="<?php echo base_url(); ?>assets/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
				<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
				<link href="<?php echo base_url(); ?>assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
				<link href="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
				<link href="<?php echo base_url(); ?>assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
				<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
				<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
				<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
				<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
				<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
				<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
				<![endif]-->
				<script src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>
				<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
				<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
				<script src="<?php echo base_url(); ?>assets/js/detect.js"></script>
				<script src="<?php echo base_url(); ?>assets/js/fastclick.js"></script>
				<script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
				<script src="<?php echo base_url(); ?>assets/js/jquery.blockUI.js"></script>
				<script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
				<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
				<script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
				<script src="<?php echo base_url(); ?>assets/plugins/parsleyjs/dist/parsley.min.js"></script>
				<script src="<?php echo base_url(); ?>assets/js/jquery.core.js"></script>
				<script src="<?php echo base_url(); ?>assets/js/jquery.app.js"></script>
				<script src="<?php echo base_url(); ?>assets/plugins/fileuploads/js/dropify.min.js"></script>
				<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
				<script src="<?php echo base_url(); ?>assets/plugins/moment/moment.js"></script>
				<script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
				<script src="<?php echo base_url(); ?>assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
				<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
				<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
				<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
		</head>
