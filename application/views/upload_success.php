<!DOCTYPE html>

<!--[if lt IE 7 ]> <html lang="en" class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js lt-ie10"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
	<meta name="author" content="Coderthemes">

	<link rel="shortcut icon" href="http://localhost:8080/condocompare/assets/images/favicon.ico">

	<title>HARRISON : Condo Compare</title>

	<!-- form Uploads -->
	<link href="http://localhost:8080/condocompare/assets/plugins/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />

	<!--Morris Chart CSS -->
	<link rel="stylesheet" href="http://localhost:8080/condocompare/assets/plugins/morris/morris.css">

	<!-- Datepicker css-->
	<link href="http://localhost:8080/condocompare/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">

	<!-- Touchspin css-->
	<link href="http://localhost:8080/condocompare/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />

	<!-- App css -->
	<link href="http://localhost:8080/condocompare/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="http://localhost:8080/condocompare/assets/css/core.css" rel="stylesheet" type="text/css" />
	<link href="http://localhost:8080/condocompare/assets/css/components.css" rel="stylesheet" type="text/css" />
	<link href="http://localhost:8080/condocompare/assets/css/icons.css" rel="stylesheet" type="text/css" />
	<link href="http://localhost:8080/condocompare/assets/css/pages.css" rel="stylesheet" type="text/css" />
	<link href="http://localhost:8080/condocompare/assets/css/menu.css" rel="stylesheet" type="text/css" />
	<link href="http://localhost:8080/condocompare/assets/css/responsive.css" rel="stylesheet" type="text/css" />

	<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
				<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
				<![endif]-->
	<script src="http://localhost:8080/condocompare/assets/js/modernizr.min.js"></script>

</head>

<body class="fixed-left">

	<!-- Begin page -->
	<div id="wrapper">

		<!-- Top Bar Start -->
		<div class="topbar">

			<!-- LOGO -->
			<div class="topbar-left">
				<a href="index.html" class="logo"><span>Admin<span>to</span></span><i class="zmdi zmdi-layers"></i></a>
			</div>

			<!-- Button mobile view to collapse sidebar menu -->
			<div class="navbar navbar-default" role="navigation">
				<div class="container">

					<!-- Page title -->
					<ul class="nav navbar-nav navbar-left">
						<li>
							<button class="button-menu-mobile open-left">
								<i class="zmdi zmdi-menu"></i>
							</button>
						</li>
						<li>
							<h4 class="page-title">ตารางข้อมูลสิ่งอำนวยความสะดวก</h4>
						</li>
					</ul>

					<!-- Right(Notification and Searchbox -->
					<ul class="nav navbar-nav navbar-right">
						<li>
							<!-- Notification -->
							<div class="notification-box">
								<ul class="list-inline m-b-0">
									<li>
										<a href="javascript:void(0);" class="right-bar-toggle">
											<i class="zmdi zmdi-notifications-none"></i>
										</a>
										<div class="noti-dot">
											<span class="dot"></span>
											<span class="pulse"></span>
										</div>
									</li>
								</ul>
							</div>
							<!-- End Notification bar -->
						</li>
						<li class="hidden-xs">
							<form role="search" class="app-search">
								<input type="text" placeholder="Search..." class="form-control">
								<a href=""><i class="fa fa-search"></i></a>
							</form>
						</li>
					</ul>

				</div>
				<!-- end container -->
			</div>
			<!-- end navbar -->
		</div>
		<!-- Top Bar End -->

		<!-- ========== Left Sidebar Start ========== -->
		<div class="left side-menu">
			<div class="sidebar-inner slimscrollleft">

				<!-- User -->
				<div class="user-box">
					<div class="user-img">
						<img src="http://localhost:8080/condocompare/assets/images/wittawat2.jpg" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
						<div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>
					</div>
					<h5><a href="#">Administator</a> </h5>
					<ul class="list-inline">
						<li>
							<a href="#">
								<i class="zmdi zmdi-settings"></i>
							</a>
						</li>

						<li>
							<a href="#" class="text-custom">
								<i class="zmdi zmdi-power"></i>
							</a>
						</li>
					</ul>
				</div>
				<!-- End User -->

				<!--- Sidemenu -->
				<div id="sidebar-menu">
					<ul>
						<li class="text-muted menu-title">Main Menu</li>

						<li>
							<a href="dashboard" class="waves-effect active"><i class="zmdi zmdi-view-dashboard"></i> <span> Dashboard </span> </a>
						</li>

						<li class="has_sub">
							<a href="javascript:void(0);" class="waves-effect"><i class="fa fa-building-o"></i> <span> คอนโด </span> <span class="menu-arrow"></span></a>
							<ul class="list-unstyled">
								<li><a href="http://localhost:8080/condocompare/condominium/view">รายการคอนโด</a></li>
								<li><a href="http://localhost:8080/condocompare/condominium/view_facilities">รายการสิ่งอำนวยความสะดวก</a></li>
								<li><a href="http://localhost:8080/condocompare/condominium/view_featured">รายการสิ่ดเด่น</a></li>
							</ul>
						</li>

						<li>
							<a href="http://localhost:8080/condocompare/land/view" class="waves-effect"><i class="zmdi zmdi-landscape"></i> <span> Land for sales </span> </a>
						</li>

						<li class="has_sub">
							<a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-account"></i> <span> Member </span> <span class="menu-arrow"></span></a>
							<ul class="list-unstyled">
								<li><a href="http://localhost:8080/condocompare/member/view">List Member</a></li>
								<li><a href="http://localhost:8080/condocompare/member/groupview">List Member Group</a></li>
							</ul>
						</li>

						<li>
							<a href="http://localhost:8080/condocompare/user/view" class="waves-effect"><i class="zmdi zmdi-key"></i> <span> Staff </span> </a>
						</li>

					</ul>
					<div class="clearfix"></div>
				</div>
				<!-- Sidebar -->
				<div class="clearfix"></div>

			</div>

		</div>
		<!-- Left Sidebar End -->

		<!-- ============================================================== -->
		<!-- Start right Content here -->
		<!-- ============================================================== -->
		<div class="content-page">
			<!-- Start content -->
			<div class="content">
				<div class="container">

					<div class="row">
						<div class="col-sm-12">
							<div class="card-box">




								<div class="table-rep-plugin">
										<div class="table-responsive" data-pattern="priority-columns">
											<table id="tech-companies-1" class="table  table-striped">
												<thead>
													<tr>
														<th>Company</th>
														<th data-priority="1">Last Trade</th>
														<th data-priority="3">Trade Time</th>
														<th data-priority="1">Change</th>
														<th data-priority="3">Prev Close</th>
														<th data-priority="3">Open</th>
														<th data-priority="6">Bid</th>
														<th data-priority="6">Ask</th>
														<th data-priority="6">1y Target Est</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<th>GOOG <span class="co-name">Google Inc.</span></th>
														<td>597.74</td>
														<td>12:12PM</td>
														<td>14.81 (2.54%)</td>
														<td>582.93</td>
														<td>597.95</td>
														<td>597.73 x 100</td>
														<td>597.91 x 300</td>
														<td>731.10</td>
													</tr>
													<tr>
														<th>AAPL <span class="co-name">Apple Inc.</span></th>
														<td>378.94</td>
														<td>12:22PM</td>
														<td>5.74 (1.54%)</td>
														<td>373.20</td>
														<td>381.02</td>
														<td>378.92 x 300</td>
														<td>378.99 x 100</td>
														<td>505.94</td>
													</tr>
													<tr>
														<th>AMZN <span class="co-name">Amazon.com Inc.</span></th>
														<td>191.55</td>
														<td>12:23PM</td>
														<td>3.16 (1.68%)</td>
														<td>188.39</td>
														<td>194.99</td>
														<td>191.52 x 300</td>
														<td>191.58 x 100</td>
														<td>240.32</td>
													</tr>
													<tr>
														<th>ORCL <span class="co-name">Oracle Corporation</span></th>
														<td>31.15</td>
														<td>12:44PM</td>
														<td>1.41 (4.72%)</td>
														<td>29.74</td>
														<td>30.67</td>
														<td>31.14 x 6500</td>
														<td>31.15 x 3200</td>
														<td>36.11</td>
													</tr>
													<tr>
														<th>MSFT <span class="co-name">Microsoft Corporation</span></th>
														<td>25.50</td>
														<td>12:27PM</td>
														<td>0.66 (2.67%)</td>
														<td>24.84</td>
														<td>25.37</td>
														<td>25.50 x 71100</td>
														<td>25.51 x 17800</td>
														<td>31.50</td>
													</tr>
													<tr>
														<th>CSCO <span class="co-name">Cisco Systems, Inc.</span></th>
														<td>18.65</td>
														<td>12:45PM</td>
														<td>0.97 (5.49%)</td>
														<td>17.68</td>
														<td>18.23</td>
														<td>18.65 x 10300</td>
														<td>18.66 x 24000</td>
														<td>21.12</td>
													</tr>
													<tr>
														<th>YHOO <span class="co-name">Yahoo! Inc.</span></th>
														<td>15.81</td>
														<td>12:25PM</td>
														<td>0.11 (0.67%)</td>
														<td>15.70</td>
														<td>15.94</td>
														<td>15.79 x 6100</td>
														<td>15.80 x 17000</td>
														<td>18.16</td>
													</tr>
													<!-- Repeat -->
													<tr>
														<th>GOOG <span class="co-name">Google Inc.</span></th>
														<td>597.74</td>
														<td>12:12PM</td>
														<td>14.81 (2.54%)</td>
														<td>582.93</td>
														<td>597.95</td>
														<td>597.73 x 100</td>
														<td>597.91 x 300</td>
														<td>731.10</td>
													</tr>
													<tr>
														<th>AAPL <span class="co-name">Apple Inc.</span></th>
														<td>378.94</td>
														<td>12:22PM</td>
														<td>5.74 (1.54%)</td>
														<td>373.20</td>
														<td>381.02</td>
														<td>378.92 x 300</td>
														<td>378.99 x 100</td>
														<td>505.94</td>
													</tr>
													<tr>
														<th>AMZN <span class="co-name">Amazon.com Inc.</span></th>
														<td>191.55</td>
														<td>12:23PM</td>
														<td>3.16 (1.68%)</td>
														<td>188.39</td>
														<td>194.99</td>
														<td>191.52 x 300</td>
														<td>191.58 x 100</td>
														<td>240.32</td>
													</tr>
													<tr>
														<th>ORCL <span class="co-name">Oracle Corporation</span></th>
														<td>31.15</td>
														<td>12:44PM</td>
														<td>1.41 (4.72%)</td>
														<td>29.74</td>
														<td>30.67</td>
														<td>31.14 x 6500</td>
														<td>31.15 x 3200</td>
														<td>36.11</td>
													</tr>
													<tr>
														<th>MSFT <span class="co-name">Microsoft Corporation</span></th>
														<td>25.50</td>
														<td>12:27PM</td>
														<td>0.66 (2.67%)</td>
														<td>24.84</td>
														<td>25.37</td>
														<td>25.50 x 71100</td>
														<td>25.51 x 17800</td>
														<td>31.50</td>
													</tr>
													<tr>
														<th>CSCO <span class="co-name">Cisco Systems, Inc.</span></th>
														<td>18.65</td>
														<td>12:45PM</td>
														<td>0.97 (5.49%)</td>
														<td>17.68</td>
														<td>18.23</td>
														<td>18.65 x 10300</td>
														<td>18.66 x 24000</td>
														<td>21.12</td>
													</tr>
													<tr>
														<th>YHOO <span class="co-name">Yahoo! Inc.</span></th>
														<td>15.81</td>
														<td>12:25PM</td>
														<td>0.11 (0.67%)</td>
														<td>15.70</td>
														<td>15.94</td>
														<td>15.79 x 6100</td>
														<td>15.80 x 17000</td>
														<td>18.16</td>
													</tr>
													<!-- Repeat -->
													<tr>
														<th>GOOG <span class="co-name">Google Inc.</span></th>
														<td>597.74</td>
														<td>12:12PM</td>
														<td>14.81 (2.54%)</td>
														<td>582.93</td>
														<td>597.95</td>
														<td>597.73 x 100</td>
														<td>597.91 x 300</td>
														<td>731.10</td>
													</tr>
													<tr>
														<th>AAPL <span class="co-name">Apple Inc.</span></th>
														<td>378.94</td>
														<td>12:22PM</td>
														<td>5.74 (1.54%)</td>
														<td>373.20</td>
														<td>381.02</td>
														<td>378.92 x 300</td>
														<td>378.99 x 100</td>
														<td>505.94</td>
													</tr>
													<tr>
														<th>AMZN <span class="co-name">Amazon.com Inc.</span></th>
														<td>191.55</td>
														<td>12:23PM</td>
														<td>3.16 (1.68%)</td>
														<td>188.39</td>
														<td>194.99</td>
														<td>191.52 x 300</td>
														<td>191.58 x 100</td>
														<td>240.32</td>
													</tr>
													<tr>
														<th>ORCL <span class="co-name">Oracle Corporation</span></th>
														<td>31.15</td>
														<td>12:44PM</td>
														<td>1.41 (4.72%)</td>
														<td>29.74</td>
														<td>30.67</td>
														<td>31.14 x 6500</td>
														<td>31.15 x 3200</td>
														<td>36.11</td>
													</tr>
													<tr>
														<th>MSFT <span class="co-name">Microsoft Corporation</span></th>
														<td>25.50</td>
														<td>12:27PM</td>
														<td>0.66 (2.67%)</td>
														<td>24.84</td>
														<td>25.37</td>
														<td>25.50 x 71100</td>
														<td>25.51 x 17800</td>
														<td>31.50</td>
													</tr>
													<tr>
														<th>CSCO <span class="co-name">Cisco Systems, Inc.</span></th>
														<td>18.65</td>
														<td>12:45PM</td>
														<td>0.97 (5.49%)</td>
														<td>17.68</td>
														<td>18.23</td>
														<td>18.65 x 10300</td>
														<td>18.66 x 24000</td>
														<td>21.12</td>
													</tr>
													<tr>
														<th>YHOO <span class="co-name">Yahoo! Inc.</span></th>
														<td>15.81</td>
														<td>12:25PM</td>
														<td>0.11 (0.67%)</td>
														<td>15.70</td>
														<td>15.94</td>
														<td>15.79 x 6100</td>
														<td>15.80 x 17000</td>
														<td>18.16</td>
													</tr>
													<!-- Repeat -->
													<tr>
														<th>GOOG <span class="co-name">Google Inc.</span></th>
														<td>597.74</td>
														<td>12:12PM</td>
														<td>14.81 (2.54%)</td>
														<td>582.93</td>
														<td>597.95</td>
														<td>597.73 x 100</td>
														<td>597.91 x 300</td>
														<td>731.10</td>
													</tr>
													<tr>
														<th>AAPL <span class="co-name">Apple Inc.</span></th>
														<td>378.94</td>
														<td>12:22PM</td>
														<td>5.74 (1.54%)</td>
														<td>373.20</td>
														<td>381.02</td>
														<td>378.92 x 300</td>
														<td>378.99 x 100</td>
														<td>505.94</td>
													</tr>
													<tr>
														<th>AMZN <span class="co-name">Amazon.com Inc.</span></th>
														<td>191.55</td>
														<td>12:23PM</td>
														<td>3.16 (1.68%)</td>
														<td>188.39</td>
														<td>194.99</td>
														<td>191.52 x 300</td>
														<td>191.58 x 100</td>
														<td>240.32</td>
													</tr>
													<tr>
														<th>ORCL <span class="co-name">Oracle Corporation</span></th>
														<td>31.15</td>
														<td>12:44PM</td>
														<td>1.41 (4.72%)</td>
														<td>29.74</td>
														<td>30.67</td>
														<td>31.14 x 6500</td>
														<td>31.15 x 3200</td>
														<td>36.11</td>
													</tr>
													<tr>
														<th>MSFT <span class="co-name">Microsoft Corporation</span></th>
														<td>25.50</td>
														<td>12:27PM</td>
														<td>0.66 (2.67%)</td>
														<td>24.84</td>
														<td>25.37</td>
														<td>25.50 x 71100</td>
														<td>25.51 x 17800</td>
														<td>31.50</td>
													</tr>
													<tr>
														<th>CSCO <span class="co-name">Cisco Systems, Inc.</span></th>
														<td>18.65</td>
														<td>12:45PM</td>
														<td>0.97 (5.49%)</td>
														<td>17.68</td>
														<td>18.23</td>
														<td>18.65 x 10300</td>
														<td>18.66 x 24000</td>
														<td>21.12</td>
													</tr>
													<tr>
														<th>YHOO <span class="co-name">Yahoo! Inc.</span></th>
														<td>15.81</td>
														<td>12:25PM</td>
														<td>0.11 (0.67%)</td>
														<td>15.70</td>
														<td>15.94</td>
														<td>15.79 x 6100</td>
														<td>15.80 x 17000</td>
														<td>18.16</td>
													</tr>
													<!-- Repeat -->
													<tr>
														<th>GOOG <span class="co-name">Google Inc.</span></th>
														<td>597.74</td>
														<td>12:12PM</td>
														<td>14.81 (2.54%)</td>
														<td>582.93</td>
														<td>597.95</td>
														<td>597.73 x 100</td>
														<td>597.91 x 300</td>
														<td>731.10</td>
													</tr>
													<tr>
														<th>AAPL <span class="co-name">Apple Inc.</span></th>
														<td>378.94</td>
														<td>12:22PM</td>
														<td>5.74 (1.54%)</td>
														<td>373.20</td>
														<td>381.02</td>
														<td>378.92 x 300</td>
														<td>378.99 x 100</td>
														<td>505.94</td>
													</tr>
													<tr>
														<th>AMZN <span class="co-name">Amazon.com Inc.</span></th>
														<td>191.55</td>
														<td>12:23PM</td>
														<td>3.16 (1.68%)</td>
														<td>188.39</td>
														<td>194.99</td>
														<td>191.52 x 300</td>
														<td>191.58 x 100</td>
														<td>240.32</td>
													</tr>
													<tr>
														<th>ORCL <span class="co-name">Oracle Corporation</span></th>
														<td>31.15</td>
														<td>12:44PM</td>
														<td>1.41 (4.72%)</td>
														<td>29.74</td>
														<td>30.67</td>
														<td>31.14 x 6500</td>
														<td>31.15 x 3200</td>
														<td>36.11</td>
													</tr>
													<tr>
														<th>MSFT <span class="co-name">Microsoft Corporation</span></th>
														<td>25.50</td>
														<td>12:27PM</td>
														<td>0.66 (2.67%)</td>
														<td>24.84</td>
														<td>25.37</td>
														<td>25.50 x 71100</td>
														<td>25.51 x 17800</td>
														<td>31.50</td>
													</tr>
													<tr>
														<th>CSCO <span class="co-name">Cisco Systems, Inc.</span></th>
														<td>18.65</td>
														<td>12:45PM</td>
														<td>0.97 (5.49%)</td>
														<td>17.68</td>
														<td>18.23</td>
														<td>18.65 x 10300</td>
														<td>18.66 x 24000</td>
														<td>21.12</td>
													</tr>
													<tr>
														<th>YHOO <span class="co-name">Yahoo! Inc.</span></th>
														<td>15.81</td>
														<td>12:25PM</td>
														<td>0.11 (0.67%)</td>
														<td>15.70</td>
														<td>15.94</td>
														<td>15.79 x 6100</td>
														<td>15.80 x 17000</td>
														<td>18.16</td>
													</tr>
												</tbody>
											</table>
										</div>

									</div>

							</div>
						</div>
					</div>
					<!-- End row -->

				</div>
				<!-- container -->

			</div>
			<!-- content -->


		</div>
		<!-- End content-page -->


		<!-- ============================================================== -->
		<!-- End Right content here -->
		<!-- ============================================================== -->

		<footer class="footer">
			2016 © Harisson Co., Ltd
		</footer>

	</div>



	<!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->



	<script>
		var resizefunc = [];
	</script>

	<!-- jQuery  -->
	<script src="http://localhost:8080/condocompare/assets/js/jquery.min.js"></script>
	<script src="http://localhost:8080/condocompare/assets/js/bootstrap.min.js"></script>
	<script src="http://localhost:8080/condocompare/assets/js/detect.js"></script>
	<script src="http://localhost:8080/condocompare/assets/js/fastclick.js"></script>
	<script src="http://localhost:8080/condocompare/assets/js/jquery.slimscroll.js"></script>
	<script src="http://localhost:8080/condocompare/assets/js/jquery.blockUI.js"></script>
	<script src="http://localhost:8080/condocompare/assets/js/waves.js"></script>
	<script src="http://localhost:8080/condocompare/assets/js/jquery.nicescroll.js"></script>
	<script src="http://localhost:8080/condocompare/assets/js/jquery.scrollTo.min.js"></script>

	<!--Morris Chart-->
	<script src="http://localhost:8080/condocompare/assets/plugins/morris/morris.min.js"></script>
	<script src="http://localhost:8080/condocompare/assets/plugins/raphael/raphael-min.js"></script>

	<!-- Dashboard init -->
	<script src="http://localhost:8080/condocompare/assets/pages/jquery.dashboard.js"></script>
	<!-- KNOB JS -->
	<!--[if IE]>
		<script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
		<![endif]-->
	<script src="http://localhost:8080/condocompare/assets/plugins/jquery-knob/jquery.knob.js"></script>

	<!-- file uploads js -->
	<script src="http://localhost:8080/condocompare/assets/plugins/fileuploads/js/dropify.min.js"></script>

	<!-- Datepicker js -->
	<script src="http://localhost:8080/condocompare/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

	<!-- Inputmask js -->
	<script src="http://localhost:8080/condocompare/assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>

	<!-- Touchspin js-->
	<script src="http://localhost:8080/condocompare/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>

	<!--form wysiwig js-->
	<script src="http://localhost:8080/condocompare/assets/plugins/tinymce/tinymce.min.js"></script>


	<!-- Responsive-table -->
	<script src="http://localhost:8080/condocompare/assets/plugins/RWD-Table-Patterns/dist/js/rwd-table.min.js"></script>


	<!-- App js -->
	<script src="http://localhost:8080/condocompare/assets/js/jquery.core.js"></script>
	<script src="http://localhost:8080/condocompare/assets/js/jquery.app.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {

			/*** RWD-Table-Patterns ***/
			//			$('.table-responsive').responsiveTable({
			//				addDisplayAllBtn	: false,
			//				addFocusBtn				: false
			//			});

			if ($(".wysiwigeditor").length > 0) {
				tinymce.init({
					selector: "textarea.wysiwigeditor",
					theme: "modern",
					height: 300,
					plugins: [
											"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
											"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
											"save table contextmenu directionality emoticons template paste textcolor"
									],
					toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
					style_formats: [
						{
							title: 'Bold text',
							inline: 'b'
						},
						{
							title: 'Red text',
							inline: 'span',
							styles: {
								color: '#ff0000'
							}
						},
						{
							title: 'Red header',
							block: 'h1',
							styles: {
								color: '#ff0000'
							}
						},
						{
							title: 'Example 1',
							inline: 'span',
							classes: 'example1'
						},
						{
							title: 'Example 2',
							inline: 'span',
							classes: 'example2'
						},
						{
							title: 'Table styles'
						},
						{
							title: 'Table row 1',
							selector: 'tr',
							classes: 'tablerow1'
						}
									]
				});
			}
		});

		$('.dropify').dropify({
			messages: {
				'default': 'Drag and drop a file here or click',
				'replace': 'Drag and drop or click to replace',
				'remove': 'Remove',
				'error': 'Ooops, something wrong appended.'
			},
			error: {
				'fileSize': 'The file size is too big (1M max).'
			}
		});

		jQuery('.datepicker-autoclose').datepicker({
			format: "dd/mm/yyyy",
			autoclose: true,
			todayHighlight: true
		});

		$(".numeric_picker").TouchSpin({
			buttondown_class: "btn btn-primary",
			buttonup_class: "btn btn-primary"
		});
	</script>

</body>

</html>
