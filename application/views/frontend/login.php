<?php
defined('BASEPATH') or exit();
/*HEADER*/
$this->load->view('partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'BodyClass' => ''));

?>

	<div id="container">
	<div class="bg-cs-gray-second">
		<div class="row align-middle">
			<div class="columns">
				<nav aria-label="You are here:" role="navigation">
					<?php echo $breadcrumbs; ?>
				</nav>
			</div>
		</div>
	</div>
	<div class="bg-cs-gray clearfix">
		<h3 class="text-center color-blue page-title"><?php echo $title; ?></h3>
	</div>
	<div class="row content-inside">
		<!--LEFT-->
		<div class="small-12 medium-12 columns">
				<?php if ($facebook === true) { ?>
			<form action="" method="post" id="form_fblogin" autocomplete="off">

				<div class="well-gray">
					<h4 class="color-red text-center"><?php echo $title; ?></h4>
					<p class="text-red">สมัครสมาชิกประเภท HOB ต้องรอการอนุมัติการเข้าสู่ระบบจาก แอดมินก่อนนะค่ะ...</p>
					<div class="login_facebook">
						<!-- <button class="button button-facebook expanded">Log in with Facebook</button> -->
						<button id="fb_login" type="submit"  class="button button-facebook expanded">Log in with Facebook</button>
						<!-- <button id="fb_logout" type="button" class="button button-facebook expanded" style="display: none;">Log out</button> -->
					</div>
					<input type="hidden" name="url" id="url" value="<?php echo site_url('page/login_facebook_member'); ?>">
					<!-- <input type="hidden" name="facebook_id" id="facebook_id">
					<input type="hidden" name="email" id="email">
					<input type="hidden" name="first_name" id="first_name">
					<input type="hidden" name="last_name" id="last_name">
					<input type="hidden" name="picture" id="picture"> -->
				</div>
			</form>
			<?php } ?>

			<div id="status"></div>


			<form action="" method="post" id="form_login"  data-abide novalidate autocomplete="off">


				<div class="well-gray">
					<!-- <div data-abide-error data-toggler data-animate="fade-in fade-out" class="alert callout fade-in mui-enter" style="display: none;"> -->
					<div data-abide-error data-toggler="" data-animate="fade-in fade-out" class="alert callout callout secondary ease" id="motion-example-1" aria-expanded="true" style="display: none;">
				    <p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> กรุณากรอกข้อมูลเพื่อทำการเข้าสู่ระบบ..</p>
				  </div>

					<div data-toggler="" data-animate="fade-in fade-out" class="alert callout callout secondary ease" id="fb-error" aria-expanded="true" style="display: none;">
				    <p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ไม่สามารถทำการ Login ได้เนื่องจากการเข้าถึงข้อมูลถูกปิดกั้น..</p>
				  </div>

					<div data-toggler="" data-animate="fade-in fade-out" class="alert callout callout secondary ease" id="login-success" aria-expanded="true" style="display: none;">
				    <p><i class="fa fa-check-circle" aria-hidden="true"></i> <span id="message"></span></p>
				  </div>

				<?php if ($facebook === false) {?>
				<h4 class="color-red text-center"><?php echo $title; ?></h4>
				<p class="text-red">สมัครสมาชิกประเภท HOB ต้องรอการอนุมัติการเข้าสู่ระบบจาก แอดมินก่อนนะค่ะ...</p>
				<?php } ?>
					<hr class="clearfix">
					<label for="email">อีเมล์</label>
					<div class="input-group">
						<input type="email" name="member_email" id="member_email" placeholder="Email"  aria-describedby="email-error"  required>
						<span class="form-error">
		          กรุณากรอกชื่อผู้ใช้ หรืออีเมล์ให้ถูกต้อง
		        </span>
						<!-- <p class="help-text" id="email-error">กรุณากรอก อีเมล์ ให้ถูกต้อง</span> -->
					</div>
					<label for="password">รหัสผ่าน</label>
					<div class="input-group">
						<input type="password" name="member_password" minlength='2' placeholder="Password" required>
						<span class="form-error">
		          กรุณากรอกรหัสผ่าน
		        </span>
					</div>
				<div class="bg-cs-blue row">
					<div class="small-12 medium-4 column mar-top-small">
						<input type="hidden" name="url_login" id="url_login" value="<?php echo site_url('page/m_login_success'); ?>">
						<button type="submit" name="action_login" id="action_login" class="button button-red-white expanded">เข้าสู่ระบบ</button>
					</div>
					<div class="small-12 medium-8 column bg-cs-blue">
						<ul class="color-red">
							<li><a href="<?php echo base_url('forgot-password'); ?>" class="color-white">ลืมรหัสผ่าน!</a></li>
							<li><a href="<?php echo base_url().$url_register; ?>" class="color-white">สมัครสมาชิก</a></li>
						</ul>
					</div>
				</div>
				</div>
			</form>
		</div>

		<!--RIGHT-->
	</div>
</div>
	<?php $this->load->view('partials/footer');?>

<script>
		// Initiate Facebook JS SDK
		// window.fbAsyncInit = function () {
		// 	FB.init({
		// 		appId		: '176921789401784', // Your app id
		// 		cookie	: true, // enable cookies to allow the server to access the session
		// 		xfbml		: true, // disable xfbml improves the page load time
		// 		version	: 'v2.7', // use version 2.4
		// 		//status  : true // Check for user login status right away
		// 	});
		//
		// 	// FB.getLoginStatus(function (response) {
		// 	// 	//console.log('getLoginStatus', response);
		// 	// 	loginCheck(response);
		// 	// });
		// };

		// Check login status
		// function statusCheck(response) {
		// 	//console.log('statusCheck', response.status);
		// 	//console.log(response);
		//
		// 	if (response.status === 'connected') {
		// 		getUser();
		// 		//$('.login_facebook').hide();
		// 		//$('.form').fadeIn();
		//
		// 	} else if (response.status === 'not_authorized') {
		// 		// User logged into facebook, but not to our app.
		// 		//  document.getElementById('status').innerHTML = 'Please log ' +
		// 		// 'into this app.';
		// 	} else {
		// 		// User not logged into Facebook.
		// 		// document.getElementById('status').innerHTML = 'Please log ' +
		// 			// 'into Facebook.';
		// 	}
		// }

		// Get login status
		// function loginCheck() {
		// 	FB.getLoginStatus(function (response) {
		// 		//console.log('loginCheck', response);
		// 		statusCheck(response);
		// 	});
		// }

		// Here we run a very simple test of the Graph API after login is
		// successful.  See statusChangeCallback() for when this call is made.
		// function getUser() {
		// 	FB.api('/me?fields=first_name,last_name,email,picture', function (response) {
		// 		console.log('getUser', response);
		// 		if( !response.email ){
		// 			console.log('LOGIN ไม่ได้');
		// 		}
		// 		$('#facebook_id').val(response.id);
		// 		$('#first_name').val(response.first_name);
		// 		$('#last_name').val(response.last_name);
		// 		$('#email').val(response.email);
		// 		$('#picture').val(response.picture['data']['url']);
		// 	});
		// }

		// $(function () {
		// 	// Trigger login
		// 	$('.login_facebook').on('click', 'button', function () {
		// 		FB.login(function () {
		// 			loginCheck();
		// 		}, {
		// 			scope: '<?php echo implode(',', $this->config->item('facebook_permissions')); ?>'
		// 		});
		// 	});
		//
		// 	$('.content-inside').on('submit', '#form_fblogin', function (e) {
		// 		e.preventDefault();
		// 		//alert('ssss');
		// 		var formdata = $(this).serialize();
		// 		//console.log(formdata);
		// 		$.ajax({
		// 			url: '<?php echo site_url('page/login_facebook_member'); ?>',
		// 			data: formdata,
		// 			type: 'POST',
		// 			success: function (data) {
		// 				// console.log(data);
		// 				if( data.success === false )
		// 				{
		// 					// $('.animation-wrapper').addClass('is-animating');
		// 					$('#motion-example-1').show();
		// 				}
		// 				// if( data == 'duplicate' )//Repeat Email Acc.
		// 				// {
		// 				// 	swal({
		// 				// 		title: 	"เกิดข้อผิดพลาด!",
		// 				// 		text: 	"ท่านได้ทำการสมัครด้วย facebook นี้แล้ว..",
		// 				// 		type: 	"warning",
		// 				// 	});
		// 				// 	return false;
		// 				// }
		// 				// else if ( data == 'success' )
		// 				// {
		// 				// 	swal({
		// 				// 		title: 	"สำเร็จ",
		// 				// 		text: 	"ท่านได้ล็อกอินด้วย facebook เรียบร้อยแล้ว..",
		// 				// 		type: 	"info",
		// 				// 	},
		// 				// 	function(isConfirm){
		// 				// 		window.location.replace('<?php echo base_url(); ?>condominium');
		// 				// 	});
		// 				// 	return true;
		// 				// }
		// 			}
		// 		})
		// 	});
		// });
		//
		// (function (d, s, id) {
		// 	var js, fjs = d.getElementsByTagName(s)[0];
		// 	if (d.getElementById(id)) {
		// 		return;
		// 	}
		// 	js = d.createElement(s);
		// 	js.id = id;
		// 	js.src = "//connect.facebook.net/en_US/sdk.js";
		// 	fjs.parentNode.insertBefore(js, fjs);
		// }(document, 'script', 'facebook-jssdk'));
	</script>
