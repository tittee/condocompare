<?php
defined('BASEPATH') or exit();
/*HEADER*/
$this->load->view('partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'BodyClass' => ''));

$sfilepath = base_url().'uploads/land';

// SEARCH FILTER
$provinces_id = (!empty($this->input->get('provinces_id'))) ? $this->input->get('provinces_id') : '';
$min_price = (!empty($this->input->get('min_price'))) ? $this->input->get('min_price') : '';
$max_price = (!empty($this->input->get('max_price'))) ? $this->input->get('max_price') : '';
$type_offering = (!empty($this->input->get('type_offering'))) ? $this->input->get('type_offering') : '';

// SEARCH ORDER
$order_by_list = (!empty($this->input->get('order_by_list'))) ? $this->input->get('order_by_list') : '';
$order_by_price_visited = (!empty($this->input->get('order_by_price_visited'))) ? $this->input->get('order_by_price_visited') : '';
?>
	<style type="text/css">
		.sb-icon-search, .sb-search-submit {
			right: auto;
			top: auto;
		}
		.content-1{ padding: 1rem; }
	</style>

	<div class="container">
		<div class="bg-cs-gray-second">
			<div class="row align-middle">
				<div class="columns">
					<nav aria-label="You are here:" role="navigation">
						<?php echo $breadcrumbs; ?>
					</nav>
				</div>
			</div>
		</div>
		<div class="bg-cs-gray clearfix">
			<h3 class="text-center color-blue page-title"><?php echo $title; ?></h3>
		</div>
		<div class="row mar-top-large align-middle">
			<div class="title small-12 medium-12 medium-centered column">
				<!--  ID : 6 = banner ด้านบน header หน้ารายการที่ดิน  -->
				<div id="" class="banner-carousel owl-carousel owl-theme">
				<?php
          $banner_6 = $this->Page_model->get_banner_location(6);
          $sfilepath_banner = base_url().'uploads/banner/';
          $banner_count = count($banner_6);

          // $banner_6_arr = array();
          for ($i = 0; $i < $banner_count;  ++$i) {
              $banner_6_id = (!empty($banner_6[$i]->banner_id)) ? $banner_6[$i]->banner_id : '';

              //UPDATE DISPLAY
              $displayed = (!empty($banner_6[$i]->displayed)) ? $banner_6[$i]->displayed : '';
              $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_6_id, 'banner');

              $banner_6_title = (!empty($banner_6[$i]->banner_title)) ? $banner_6[$i]->banner_title : '';
              $banner_6_url = (!empty($banner_6[$i]->banner_url)) ? $banner_6[$i]->banner_url : '#';
              $banner_6_pic_thumb = (!empty($banner_6[$i]->pic_thumb)) ? $sfilepath_banner.$banner_6[$i]->pic_thumb : 'http://placehold.it/1400x175/333?text=1400x175';
              // $banner_6_arr[] = '<a href="'.$banner_6_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_6_id.'"><img src="'.$banner_6_pic_thumb.'" alt="'.$banner_6_title.'" title=""></a>';
              echo $banner_6_arr = '<div class="item"><a href="'.$banner_6_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_6_id.'"><img src="'.$banner_6_pic_thumb.'" alt="'.$banner_6_title.'" title=""></a></div>';
          }
          // shuffle($banner_6_arr);
          // echo (!empty($banner_6_arr[0])) ? $banner_6_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/1400x175/333?text=1400x175" alt="1400x175" title="1400x175"></a>';
        ?>
				</div>

			</div>
		</div>
	</div>

	<form class="container" name="from_search" method="get" action="<?php echo site_url('house-and-land'); ?>">
	<header id="header" class="header">
		<div class="container">
			<div class="row">
				<div class="columns">
					<section class="tabs">
							<div class="content-1">
									<!-- SEARCH -->
									<p class="small-12 large-3 columns"><strong>จังหวัด</strong>
										<select id="provinces_id" class="" name="provinces_id" onchange="(this)">
											<option value="0">----เลือก----</option>
											<?php
                        foreach ($data['provinces'] as $row) {
                            //START FOREACH
                            $sel = ($row->provinces_id === $provinces_id) ? 'selected' : '';
                            ?>
                      <option value="<?php echo $row->provinces_id;?>" <?php echo $sel; ?>><?php echo $row->provinces_name; ?></option>
												<?php
                            }//END FOREACH
                        ?>
										</select>
									</p>
									<p class="small-12 large-3 columns"><strong>ประเภทการขาย</strong>
										<select name="type_offering" value="type_offering">
											<option value="">----เลือก----</option>
											<?php
	                      foreach ($data['type_offering'] as $row) {
	                          //START FOREACH
	                        $sel = ($row->type_offering_id == $type_offering) ? 'selected' : ''; ?>
													<option value="<?php echo $row->type_offering_id; ?>" <?php echo $sel; ?>>
													<?php echo $row->type_offering_title; ?>
													</option>
												<?php
												}//END FOREACH
												?>
										</select>
									</p>
									<p class="small-12 medium-6 large-2 columns"><strong>ราคาต่ำสุด</strong>
										<select name="min_price" value="min_price">
											<option value="">----เลือก----</option>
											<option value="1000000" <?php echo ($min_price === '1000000') ? 'selected' : ''; ?>>ไม่เกิน1,000,000</option>
											<option value="2000000" <?php echo ($min_price === '2000000') ? 'selected' : ''; ?>>ไม่เกิน2,000,000</option>
											<option value="3000000" <?php echo ($min_price === '3000000') ? 'selected' : ''; ?>>ไม่เกิน3,000,000</option>
											<option value="4000000" <?php echo ($min_price === '4000000') ? 'selected' : ''; ?>>ไม่เกิน4,000,000</option>
											<option value="5000000" <?php echo ($min_price === '5000000') ? 'selected' : ''; ?>>ไม่เกิน5,000,000</option>
										</select>
									</p>
									<p class="small-12 medium-6 large-2 columns"><strong>ราคาสูงสุด</strong>
										<select name="max_price" value="max_price">
											<option value="">----เลือก----</option>
											<option value="6000000" <?php echo ($max_price === '6000000') ? 'selected' : ''; ?>>6,000,000</option>
											<option value="7000000" <?php echo ($max_price === '7000000') ? 'selected' : ''; ?>>7,000,000</option>
											<option value="8000000" <?php echo ($max_price === '8000000') ? 'selected' : ''; ?>>8,000,000</option>
											<option value="9000000" <?php echo ($max_price === '9000000') ? 'selected' : ''; ?>>9,000,000</option>
											<option value="10000000" <?php echo ($max_price === '10000000') ? 'selected' : ''; ?>>10,000,000</option>
											<option value="11000000" <?php echo ($max_price === '11000000') ? 'selected' : ''; ?>>11,000,000</option>
											<option value="12000000" <?php echo ($max_price === '12000000') ? 'selected' : ''; ?>>12,000,000</option>
											<option value="13000000" <?php echo ($max_price === '13000000') ? 'selected' : ''; ?>>13,000,000</option>
											<option value="14000000" <?php echo ($max_price === '14000000') ? 'selected' : ''; ?>>14,000,000</option>
											<option value="15000000" <?php echo ($max_price === '15000000') ? 'selected' : ''; ?>>15,000,000</option>
											<option value="16000000" <?php echo ($max_price === '16000000') ? 'selected' : ''; ?>>16,000,000</option>
											<option value="17000000" <?php echo ($max_price === '17000000') ? 'selected' : ''; ?>>17,000,000</option>
											<option value="18000000" <?php echo ($max_price === '18000000') ? 'selected' : ''; ?>>18,000,000</option>
											<option value="19000000" <?php echo ($max_price === '19000000') ? 'selected' : ''; ?>>19,000,000</option>
											<option value="20000000" <?php echo ($max_price === '20000000') ? 'selected' : ''; ?>>20,000,000</option>
											<option value="20000001" <?php echo ($max_price === '20000001') ? 'selected' : ''; ?>>20,000,001 ขึ้นไป</option>
										</select>
									</p>
									<p class="small-12 medium-4 large-2 columns hide-for-small show-for-large">
										<input type="submit" name="submit" value="action" id="submit" class=" sb-search-submit"><span class="button sb-icon-search"></span></p>
									<p class="small-12 medium-12 large-2 columns show-for-small hide-for-large">
										<input type="submit" name="submit" class="button expanded button-red-white" value="ค้นหา" >
									</p>
							</div>
					</section>
				</div>
			</div>
		</div>
	</header>


	<div id="container">
		<div id="highlight">
			<div class="sub-container row">
				<div class="title mar-bott-medium column">


							<h2 class="title-red-black"><?php echo (!empty($data['land_total'])) ? $data['land_total'] : '0'; ?><span>	รายการที่ดิน</span></h2>


				</div>
				<div class="small-12 large-9 column hover">
					<div class="small-12 column well bg-cs-gray">
						<!-- <form class="from_sort" action="<?php echo site_url('house-and-land'); ?>" method="get"> -->

						<div class="row">
							<div class="small-12 large-8 columns">
								<!-- <div class="small-12 large-6 columns">
									<select name="order_by_list">
										<option value="5" <?php echo ($order_by_list === '') ? 'selected' : ''; ?>>5 ประกาศก่อนหน้า</option>
										<option value="10" <?php echo ($order_by_list === '10') ? 'selected' : ''; ?>>10 ประกาศก่อนหน้า</option>
										<option value="20" <?php echo ($order_by_list === '20') ? 'selected' : ''; ?>>20 ประกาศก่อนหน้า</option>
										<option value="30" <?php echo ($order_by_list === '30') ? 'selected' : ''; ?>>30 ประกาศก่อนหน้า</option>
									</select>
								</div> -->

								<div class="small-12 large-6 columns">
									<select name="order_by_price_visited" class="">
										<option value="1" <?php echo ($order_by_price_visited === '1') ? 'selected' : ''; ?>>ราคามากไปน้อย</option>
										<option value="2" <?php echo ($order_by_price_visited === '2') ? 'selected' : ''; ?>>ราคาน้อยไปมาก</option>
										<option value="3" <?php echo ($order_by_price_visited === '3') ? 'selected' : ''; ?>>เข้าชมเยอะที่สุด</option>
									</select>
								</div>

							</div>
							<div class="small-12 large-4 columns">
								<div class="small-12 large-6 columns">
									<!-- <a href="#" class="alert hollow button no-bg expanded"><i class="fa fa-bars">	รายการ</i></a> -->
									<button type="submit" name="button_submit" class="alert hollow button no-bg expanded"><i class="fa fa-search"></i> ค้นหา</button>
								</div>

							</div>
						</div>
						<!-- </form> -->
					</div>
					<!-- END FILTER -->

					<div class="title small-12 large-12 column hover">
						<div class="row">

							<?php
					    //LIST CONDO
					    $i = 0;

					    //$sel = '';
					//							var_dump($data);
					    if (!empty($data['land'])) {
					        while ($row = $data['land']->unbuffered_row()) {
					            $land_id = (!empty($row->land_id)) ? $row->land_id : 0;
					            $land_title = (!empty($row->land_title)) ? $row->land_title : 'ไม่ระบุ';
					            $visited = (!empty($row->visited)) ? $row->visited : 0;

					            $land_size = ($row->land_size !== '0.00') ? $row->land_size : '';
											$land_size_unit = (!empty($row->land_size_unit)) ? $this->primaryclass->get_square($row->land_size_unit) : '';
					            $land_size_square_rai = ($row->land_size_square_rai !== '0.00') ? $row->land_size_square_rai : '';
					            $land_size_square_ngan = ($row->land_size_square_ngan !== '0.00') ? $row->land_size_square_ngan : '';
					            $land_size_square_yard = ($row->land_size_square_yard !== '0.00') ? $row->land_size_square_yard : '';
					            // LAND SIZE & LAND SIZE UNIT
					            $msg_size_unit = $this->primaryclass->get_size_square($land_size,$land_size_square_rai, $land_size_square_ngan, $land_size_square_yard, $land_size_unit);

					            $land_total_price = ($row->land_total_price !== '0.00') ? number_format($row->land_total_price, 0, '.', ',').' บาท' : 'ไม่ระบุ';

					            //$provinces_id = ( !empty($row->provinces_id))? $row->provinces_id : 0;

											if( !empty($row->provinces_id) &&  $row->provinces_id !== 0)
											{
												$provinces = $this->M->get_data_by_id('provinces_id, provinces_name', 'provinces_id', $row->provinces_id, 'provinces');
												$land_road_provinces = $this->primaryclass->land_road_provinces($row->land_road, $provinces['provinces_name']);
											}else {
												$land_road_provinces = '';
											}


					            $createdate = ($row->createdate !== '0000-00-00 00:00:00') ? $this->dateclass->DateTimeShortFormat($row->createdate, 0, 0, 'Th') : 'ไม่ระบุ';

					            $type_offering_id = (!empty($row->type_offering_id)) ? $row->type_offering_id : '';

											// echo $type_offering_id;
					            if ($type_offering_id !== '' || $type_offering_id !== 0) {
					                $type_offering = $this->Page_model->get_offering($type_offering_id);
					                $type_offering_img = base_url().'uploads/config/'.$type_offering->type_offering_images;
					            } elseif( $type_offering_id === 0) {
					                $type_offering_img = base_url().'assets/images/sell1.png';

					            } else {
					                $type_offering_img = base_url().'assets/images/sell1.png';
					            }
					            $pic_thumb = $sfilepath.'/'.$row->pic_thumb;

					            ?>
								<div class=" display condo-list small-12 large-12 column">
									<figure class="small-12 medium-12 large-4 column">
										<span><img src="<?php echo $type_offering_img;?>" alt="" /></span>
										<a href="<?php echo site_url('land-details/'.$land_id);?>"><img src="<?php echo $pic_thumb;?>" alt=""></a></figure>
									<div class="small-12 medium-6 large-5 column">
										<h2><a href="<?php echo site_url('land-details/'.$land_id);?>"><?php echo $land_title;?></a></h2>
										<p class="location_address">
											<?php echo $land_road_provinces;?>
										</p>
										<table class="details">
											<thead>
												<th>ขนาดที่ดิน</th>
											</thead>
											<tbody>
												<td>
													<?php echo $msg_size_unit; ?></td>
											</tbody>
										</table>
										<p class="contact_name">
											ประกาศเมื่อ <?php echo $createdate;?>
										</p>
										<p class="contact_name"><i class="fa fa-eye" style="color: #E9E9E9;"></i> จำนวนผู้ชม <?php echo $visited;?> </p>
									</div>
									<div class="small-12 medium-6 large-3 column">
										<p class="price">
											<?php echo $land_total_price;?></p>
										<!-- <p><?php //echo $msg_size_unit;?> </p> -->
										<ul class="inline-list">

											<?php if (isset($_SESSION['mTID'])) {
    ?>
												<li>
													<span data-tooltip aria-haspopup="true" class="has-tip right" data-disable-hover="false" tabindex="3" title="เก็บไว้ดูภายหลัง">
														<a href="#" onclick="javascript:(0);" class="hob-wishlist" id="<?php echo $row->land_id;
    ?>"><i class="fa fa-heart"></i></a>
													</span>
													</li>
												<?php

// } else {
    ?>
												<!-- <li>
													<span data-tooltip aria-haspopup="true" class="has-tip right" data-disable-hover="false" tabindex="3" title="กรุณาล็อกอิน">
														<a href="#" onclick="javascript:(0);" class="" id="<?php echo $row->land_id;
    ?>"><i class="fa fa-warning "></i></a>
													</span>
													</li> -->
											<?php }
                      //END IF
                       ?>
										</ul>
									</div>
								</div>
								<?php

                                        }
                                        ++$i;
                                    } else {
                                        ?>
							<div class="location display condo-list small-12 large-12 column">
							<p class="text-center">ยังไม่มีข้อมูลที่ดิน</p>
							</div>
								<?php

                                    } ?>
						</div>
					</div>
					<div class="column text-center">
						<?php echo $links; ?>
					</div>
					<div class="clearfix"></div>
					<!--  ID : 9 = banner ด้านล่าง หน้ารายการที่ดิน -->
					<div class="column banner">
						<div id="" class="banner-carousel owl-carousel owl-theme">
					<?php
            $banner_9 = $this->Page_model->get_banner_location(9);
            $sfilepath_banner = base_url().'uploads/banner/';
            $banner_count = count($banner_9);

            // $banner_9_arr = array();
            for ($i = 0; $i < $banner_count;  ++$i) {
                $banner_9_id = (!empty($banner_9[$i]->banner_id)) ? $banner_9[$i]->banner_id : '';

                //UPDATE DISPLAY
                $displayed = (!empty($banner_9[$i]->displayed)) ? $banner_9[$i]->displayed : '';
                $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_9_id, 'banner');

                $banner_9_title = (!empty($banner_9[$i]->banner_title)) ? $banner_9[$i]->banner_title : '';
                $banner_9_url = (!empty($banner_9[$i]->banner_url)) ? $banner_9[$i]->banner_url : '#';
                $banner_9_pic_thumb = (!empty($banner_9[$i]->pic_thumb)) ? $sfilepath_banner.$banner_9[$i]->pic_thumb : 'http://placehold.it/1400x175/333?text=1400x175';
                // $banner_9_arr[] = '<a href="'.$banner_9_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_9_id.'"><img src="'.$banner_9_pic_thumb.'" alt="'.$banner_9_title.'" title=""></a>';
                  echo $banner_9_arr = '<div class="item"><a href="'.$banner_9_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_9_id.'"><img src="'.$banner_9_pic_thumb.'" alt="'.$banner_9_title.'" title=""></a></div>';
            }
            // shuffle($banner_9_arr);
            // echo (!empty($banner_9_arr[0])) ? $banner_9_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/1400x175/333?text=1400x175" alt="1400x175" title="1400x175"></a>';
            ?>
						</div>
					</div>
				</div>
				<div class="title small-12 large-3 column">

					<!--  ID : 8 = banner ขวามือด้านบน หน้ารายการที่ดิน  -->
					<div id="" class="banner-carousel owl-carousel owl-theme">
					<?php
              $banner_8 = $this->Page_model->get_banner_location(8);
              $sfilepath_banner = base_url().'uploads/banner/';
              $banner_count = count($banner_8);

              // $banner_8_arr = array();
              for ($i = 0; $i < $banner_count;  ++$i) {
                  $banner_8_id = (!empty($banner_8[$i]->banner_id)) ? $banner_8[$i]->banner_id : '';

                  //UPDATE DISPLAY
                  $displayed = (!empty($banner_8[$i]->displayed)) ? $banner_8[$i]->displayed : '';
                  $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_8_id, 'banner');

                  $banner_8_title = (!empty($banner_8[$i]->banner_title)) ? $banner_8[$i]->banner_title : '';
                  $banner_8_url = (!empty($banner_8[$i]->banner_url)) ? $banner_8[$i]->banner_url : '#';
                  $banner_8_pic_thumb = (!empty($banner_8[$i]->pic_thumb)) ? $sfilepath_banner.$banner_8[$i]->pic_thumb : 'http://placehold.it/370x540/333?text=370x540';
                  // $banner_8_arr[] = '<a href="'.$banner_8_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_8_id.'"><img src="'.$banner_8_pic_thumb.'" alt="'.$banner_8_title.'" title=""></a>';
                  echo $banner_8_arr = '<div class="item"><a href="'.$banner_8_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_8_id.'"><img src="'.$banner_8_pic_thumb.'" alt="'.$banner_8_title.'" title=""></a></div>';
              }
              // shuffle($banner_8_arr);
              // echo (!empty($banner_8_arr[0])) ? $banner_8_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/370x540/333?text=370x540" alt="370x540" title="370x540"></a>';
          ?>
					</div>

					<h2 class="title-blue-bg right">ทรัพย์ยอดนิยม<span>ประจำสัปดาห์</span></h2>
					<div class="inside">
						<article>
							<div class="highlight-sidebar">
								<?php
                                        #SIDEBAR land
                                        if (!empty($condo_sidebar)) {
                                            foreach ($land_sidebar as $row) {
                                                $land_id = (!empty($row->land_id)) ? $row->land_id : 0;
                                                $land_title = (!empty($row->land_title)) ? $row->land_title : 'ไม่ระบุ';
                                                $pic_thumb = (!empty($row->pic_thumb)) ? $sfilepath.'/'.$row->pic_thumb : 'http://placehold.it/150x150';
                                                $condo_road_alley = $this->primaryclass->condo_road_soi($row->land_road, $row->land_alley);

                                                ?>


									<div class="content-sidebar hover-eff bg-cs-gray">
										<figure>
											<div class="bg-cs-gray">
												<div class="small-12 large-6 columns">
													<p><a href="<?php echo site_url('land-details/'.$land_id);
                                                ?>"><img src="<?php echo $pic_thumb;
                                                ?>" alt="<?php echo $land_title;
                                                ?>"></a></p>
												</div>
												<div class="small-12 large-6 columns highlight-caption">
													<h3><a href="<?php echo site_url('land-details/'.$land_id);
                                                ?>"><?php echo $land_title;
                                                ?></a></h3>
													<p>
														<?php echo $condo_road_alley;
                                                ?>
													</p>
												</div>
											</div>
											<div class="clearfix"></div>
										</figure>
									</div>
									<?php

                                            }//END FOREACH
                                        }//END IF LAND SIDEBAR
                                        ?>

							</div>
							<!-- <div class="columns"><a href="<?php echo site_url('news'); ?>" class="button small-12">ดูข่าวและบทความทั้งหมด</a></div> -->
						</article>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>


	<?php $this->load->view('partials/footer');?>

		<script>
			$(document).ready(function () {

				$(".hob-wishlist").click(function (event) {
					var land_id = $(this).attr("id");
					$.ajax({
						url: "<?php echo base_url(); ?>page/wishlist_land",
						type: "POST",
						data: {
							land_id: land_id,
						},
						//async: false,
						success: function (data, status) {
							//console.log(data);
							if (data == 'insert') {
								$("#" + land_id + ".hob-wishlist").children().css('color', 'green');
							} else if (data == 'delete') {
								$("#" + land_id + ".hob-wishlist").children().css('color', '');
							}

						},
						error: function (xhr, desc, err) {
							console.log(err);
						},
					});
					event.preventDefault();
					return false;
				});

				$(".hob-compare").click(function (event) {
					//alert('ssss');
					var land_id = $(this).attr("id");
					$.ajax({
						url: "<?php echo base_url(); ?>page/compare_land",
						type: "POST",
						data: {
							land_id: $(this).attr("id"),
						},
						success: function (data, status) {
							//console.log(data);
							if (data == 'insert') {
								$("#" + land_id + ".hob-compare").children().css('color', 'green');
							} else if (data == 'delete') {
								$("#" + land_id + ".hob-compare").children().css('color', '');
							}
						},
						error: function (xhr, desc, err) {
							console.log(err);
						},
					});
					event.preventDefault();
					return false;
				});


			});
		</script>
