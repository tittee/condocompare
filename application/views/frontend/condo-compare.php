<?php
defined('BASEPATH') or exit();
/*HEADER*/
$this->load->view('partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'BodyClass' => ''));

$sfilepath = base_url().'uploads/condominium';
?>
<style>
#owl-demo .item{
	margin: 1rem;
}
#owl-demo .item img{
	display: block;
	width: 100%;
	height: auto;
}
</style>
	<div id="container">
		<div id="compare" class="">
			<div class="row">
				<div class="column title small-12 hover">
					<h2 class="title-blue-red">รายการเปรียบเทียบ<span>คอนโดมิเนียม</span></h2>

					<div class="row">
					<div id="owl-demo" class="owl-carousel">
					<?php
                        //LIST CONDO
                        $i = 0;

                        //COMPARE
                        if (!empty($data)) {
                            foreach ($data as $rowcompare => $vcompare) {

                            // $this->primaryclass->pre_var_dump($vcompare);

                            $arr_condo_total_price[$rowcompare] = $vcompare['condo_total_price'];

                                $condo_total_price = (!empty($vcompare['condo_total_price'])) ? $vcompare['condo_total_price'] : '0.00';
                                $condo_area_apartment = ($vcompare['condo_area_apartment'] !== '0.00') ? $vcompare['condo_area_apartment'] : '1';

                                $arr_condo_all_room[$rowcompare] = (!empty($vcompare['condo_all_room'])) ? $vcompare['condo_all_room'] : '0';
                                $arr_condo_all_toilet[$rowcompare] = (!empty($vcompare['condo_all_toilet'])) ? $vcompare['condo_all_toilet'] : '0';
                                $arr_condo_living_area[$rowcompare] = (!empty($vcompare['condo_living_area']) || $vcompare['condo_living_area'] !== 0) ? $vcompare['condo_living_area'] : '0';

                                $arr_condo_price_per_square_meter[$rowcompare] = ($condo_total_price / $condo_area_apartment);
                            }
                        // echo "<br />";

                        min($arr_condo_total_price);
                            max($arr_condo_total_price);
                            min($arr_condo_all_room);
                            max($arr_condo_all_room);
                            min($arr_condo_all_toilet);
                            max($arr_condo_all_toilet);
                            min($arr_condo_living_area);
                            max($arr_condo_living_area);
                            min($arr_condo_price_per_square_meter);
                            max($arr_condo_price_per_square_meter);

                            foreach ($data as $row => $v) {
                                $condo_id = (!empty($v['condo_id'])) ? $v['condo_id'] : '0';

                                $condo_no = (!empty($v['condo_no'])) ? $v['condo_no'] : '';
                                $condo_title = (!empty($v['condo_title'])) ? $v['condo_title'] : 'ไม่ระบบุ';
                                $zone_id = (!empty($v['zone_id'])) ? $v['zone_id'] : '';

                                $type_offering_id = (!empty($v['type_offering_id'])) ? $v['type_offering_id'] : '';
                                $type_offering_title = (!empty($type_offering_id) || $type_offering_id !== 0 || isset($type_offering_id)) ? $this->M->get_name_by_id('type_offering_id', 'type_offering_title', $type_offering_id, 'type_offering') : '';
                                $type_offering_title_t = (isset($type_offering_title[0])) ? $type_offering_title[0]['type_offering_title'] : '';

                                $type_suites_id = (!empty($v['type_suites_id'])) ? $v['type_suites_id'] : '';
                                $type_suites_title = (!empty($type_suites_id) || $type_suites_id !== 0 || isset($type_suites_id)) ? $this->M->get_name_by_id('type_suites_id', 'type_suites_title', $type_suites_id, 'type_suites') : '';
                                $type_suites_title_t = (isset($type_suites_title[0])) ? $type_suites_title[0]['type_suites_title'] : '';

                                $type_decoration_id = (!empty($v['type_decoration_id'])) ? $v['type_decoration_id'] : '';
                                $type_decoration_title = (!empty($type_decoration_id) || $type_decoration_id !== 0  || isset($type_decoration_id)) ? $this->M->get_name_by_id('type_decoration_id', 'type_decoration_title', $type_decoration_id, 'type_decoration') : '';
                                $type_decoration_title_t = (isset($type_decoration_title[0])) ? $type_decoration_title[0]['type_decoration_title'] : '-';

                                $condo_all_room = (!empty($v['condo_all_room'])) ? $v['condo_all_room'] : 'ไม่ระบบุ';
                                $condo_all_toilet = (!empty($v['condo_all_toilet'])) ? $v['condo_all_toilet'] : 'ไม่ระบบุ';
                                $condo_interior_room = (!empty($v['condo_interior_room'])) ? $v['condo_interior_room'] : 'ไม่ระบบุ';
                                $condo_direction_room = (!empty($v['condo_direction_room'])) ? $v['condo_direction_room'] : 'ไม่ระบบุ';
                                $condo_scenery_room = (!empty($v['condo_scenery_room'])) ? $v['condo_scenery_room'] : 'ไม่ระบบุ';
                                $condo_living_area = (!empty($v['condo_living_area'])) ? $v['condo_living_area'] : 'ไม่ระบบุ';
                                $condo_electricity = (!empty($v['condo_electricity'])) ? $v['condo_electricity'] : 'ไม่ระบบุ';

                                $condo_floor = (!empty($v['condo_floor'])) ? $v['condo_floor'] : '';

                                $condo_area_apartment = ($v['condo_area_apartment'] !== '0.00' || !empty($v['condo_area_apartment'])) ? $v['condo_area_apartment'] : '0.00';
                                $condo_area_apartment_meter = ($condo_area_apartment !== '0.00') ? $v['condo_area_apartment'].' ตร.ม.' : '-';

                                $condo_alley = (!empty($v['condo_alley'])) ? $v['condo_alley'] : '';
                                $condo_road = (!empty($v['condo_road'])) ? $v['condo_road'] : '';

                                $condo_road_alley = $this->primaryclass->condo_road_soi($condo_road, $condo_alley);

                                $condo_address = (!empty($v['condo_address'])) ? $v['condo_address'] : '';

                                $condo_total_price = ($v['condo_total_price'] !== '0.00') ? $v['condo_total_price'] : '0.00';
                                $condo_total_price_bath = ($condo_total_price !== '0.00') ? number_format($v['condo_total_price'], 0, '.', ',') : 'ไม่ระบุ';
                                $condo_price_per_square_meter = ($v['condo_price_per_square_meter'] !== '0.00') ? $v['condo_price_per_square_meter'] : '0.00';

                                $msg_price_per_meter = $this->primaryclass->get_price_per_square(0, $condo_total_price, $condo_area_apartment, $condo_price_per_square_meter);

                                $provinces_id = (!empty($v['provinces_id'])) ? $v['provinces_id'] : '';
                                $provinces_name = (!empty($provinces_id) || $provinces_id !== 0) ? $this->M->get_name_by_id('provinces_id', 'provinces_name', $provinces_id, 'provinces') : '';
                                $provinces_name_t = (isset($provinces_name[0])) ? $provinces_name[0]['provinces_name'] : '';

                                $districts_id = (!empty($v['districts_id'])) ? $v['districts_id'] : '';
                                $districts_name = (!empty($districts_id) || $districts_id !== 0) ? $this->M->get_name_by_id('districts_id', 'districts_name', $districts_id, 'districts') : 'ไม่ระบบุ';
                                $districts_name_t = (isset($districts_name[0])) ? $districts_name[0]['districts_name'] : '';

                                $sub_districts_id = (!empty($v['sub_districts_id'])) ? $v['sub_districts_id'] : '';
                                $sub_districts_name = (!empty($sub_districts_id) || $sub_districts_id !== 0) ? $this->M->get_name_by_id('sub_districts_id', 'sub_districts_name', $sub_districts_id, 'sub_districts') : '';
                                $sub_districts_name_t = (isset($sub_districts_name[0])) ? $sub_districts_name[0]['sub_districts_name'] : '';

                                $condo_zipcode = (!empty($v['condo_zipcode'])) ? $v['condo_zipcode'] : '';

                                $compare_condo_id = (!empty($v['compare_condo_id'])) ? $v['compare_condo_id'] : 'ไม่ระบบุ';
                                $pic_thumb = $sfilepath.'/'.$v['pic_thumb'];

                                ?>

					<div class="item hover">
								<div class="po_relative"><a href="javascript:void(0);" id="<?php echo $compare_condo_id;
                                ?>" class="btn-delete alert button po_absolute right-0">Remove</a></div>
								<figure><a href="<?php echo site_url('condominium-details/'.$condo_id);
                                ?>"><img src="<?php echo $pic_thumb;
                                ?>" alt="เอ สเปซ มี บางนา"></a></figure>
								<div class="caption">
									<h2><a href="<?php echo site_url('condominium-details/'.$condo_id);
                                ?>"><?php echo $condo_title;
                                ?></a></h2>
									<p class="mar-bott-large"><?php echo $condo_road_alley;
                                ?></p>
									<div class="row bg-cs-blue ">
										<?php

                                        if (min($arr_condo_total_price) === $condo_total_price) {
                                            $css_color = 'red bolder';
                                        } else {
                                            $css_color = 'white';
                                        }
                                ?>
										<div class="column price color-<?php echo $css_color;
                                ?>">
											<?php echo $condo_total_price_bath ?> </div>
									</div>

									<div class="table mar-top-large">
										<table>
											<tr>
												<td>ห้องนอน</td>
												<td>ห้องน้ำ</td>
											</tr>
											<tr>
												<td>
													<?php
                                                    if (min($arr_condo_all_room) === $condo_all_room) {
                                                        $condo_all_room_t = '<strong class="color-green bolder">'.$condo_all_room.'</strong>';
                                                    } elseif (max($arr_condo_all_room) === $condo_all_room) {
                                                        $condo_all_room_t = '<strong class="color-red red">'.$condo_all_room.'</strong>';
                                                    } else {
                                                        $condo_all_room_t = $condo_all_room;
                                                    }
                                ?>
													<?php echo $condo_all_room_t;
                                ?>
												</td>
												<td>
													<?php
                                                    // if( min($arr_condo_all_toilet) === $condo_all_toilet )
                                                    // {
                                                    // 	$condo_all_toilet_t = "<strong class=\"color-green bolder\">".$condo_all_toilet."</strong>";
                                                    // }
                                                    // elseif (max($arr_condo_all_toilet) === $condo_all_toilet)
                                                    // {
                                                    // 	$condo_all_toilet_t = "<strong class=\"color-red red\">".$condo_all_toilet."</strong>";
                                                    // }

                                                    if (max($arr_condo_all_toilet) === $condo_all_toilet) {
                                                        $condo_all_toilet_t = '<strong class="color-red red">'.$condo_all_toilet.'</strong>';
                                                    } else {
                                                        $condo_all_toilet_t = $condo_all_toilet;
                                                    }
                                ?>
													<?php echo $condo_all_toilet_t;
                                ?>
												</td>
											</tr>
											<tr>
												<td colspan="2">พื้นที่ใช้สอย</td>
											</tr>
											<tr>
												<td colspan="2">
													<?php

                                                    if (max($arr_condo_living_area) === $condo_area_apartment) {
                                                        $condo_area_apartment_max = '<strong class="color-red red">'.$condo_area_apartment_meter.'</strong>';
                                                    } else {
                                                        $condo_area_apartment_meter = $condo_area_apartment_meter;
                                                    }
                                ?>

												<?php echo $condo_area_apartment_meter;
                                ?></td>
											</tr>
										</table>
									</div>

									<div class="mar-bott-medium">
										<label for="">ชื่อโครงการ</label>
										<strong><?php echo $condo_title;
                                ?></strong>
									</div>

									<div class="mar-bott-medium">
										<label for="">ที่อยู่</label>
										<?php
                                            if (!empty($condo_no)) {
                                                ?>
										<strong><?php echo $condo_no;
                                                ?>  </strong>
										<?php

                                            }
                                ?>

										<?php
                                            if (!empty($condo_floor)) {
                                                ?>
										<strong><?php echo $condo_floor;
                                                ?></strong>
										<?php

                                            }
                                ?>
										<?php
                                            if (!empty($condo_alley)) {
                                                ?>
										<strong><?php echo $condo_alley;
                                                ?></strong>
										<?php

                                            }
                                ?>

										<?php
                                            if (!empty($condo_road)) {
                                                ?>
										<strong><?php echo $condo_road;
                                                ?></strong>
										<?php

                                            }
                                ?>

										<?php
                                            if (!empty($condo_address)) {
                                                ?>
										<strong><?php echo $condo_address;
                                                ?></strong>
										<?php

                                            }
                                ?>

										<?php
                                            if (!empty($sub_districts_name_t)) {
                                                ?>
										<strong>
											<?php
                                            echo $sub_districts_name_t;
                                                ?>
										</strong>
										<?php

                                            }
                                ?>

										<?php
                                            if (!empty($districts_name_t)) {
                                                ?>
										<strong>
											<?php
                                            echo $districts_name_t;
                                                ?>
										</strong>
										<?php

                                            }
                                ?>

										<?php
                                            if (!empty($provinces_name_t)) {
                                                ?>
										<strong>
											<?php
                                            echo $provinces_name_t;
                                                ?>
										</strong>
										<?php

                                            }
                                ?>
									</div>
									<!-- End Col -->

									<div class="mar-bott-medium">
										<label for="">ประเภททรัพย์</label>
										<strong>คอนโด</strong>
									</div>
									<!-- End Col -->

									<div class="mar-bott-medium">
										<label for="">ราคาต่อตร.ม.</label>
										<?php
                                        // if( min($arr_condo_price_per_square_meter) === $condo_price_per_square_meter )
                                        // {
                                        // 	$css_color = 'green bolder';
                                        // }
                                        // elseif (max($arr_condo_price_per_square_meter) === $condo_price_per_square_meter)
                                        // {
                                        // 	$css_color = 'red bolder';
                                        // }
                                        if (max($arr_condo_price_per_square_meter) === $condo_price_per_square_meter) {
                                            $css_color = 'red bolder';
                                        } else {
                                            $css_color = 'black';
                                        }
                                ?>
										<strong class="price color-<?php echo $css_color;
                                ?>"><?php echo $msg_price_per_meter;
                                ?></strong>
									</div>
									<!-- End Col -->

									<div class="mar-bott-medium">
										<label for="">การตกแต่ง</label>
										<strong>
											<?php echo $type_decoration_title_t;
                                ?>
										</strong>
									</div>
									<!-- End Col -->

									<div class="mar-bott-medium">
										<a class="links" href="<?php echo site_url('condominium-details/'.$condo_id);
                                ?>">รายละเอียดเพิ่มเติ่ม</a>
									</div>
									<!-- End Col -->

								</div>
					</div>

						<?php

                            } //END FOREACH
                        } //END IF

                        ?>
				</div>
					</div>



					<div class="column text-center"><a href="<?php echo site_url('condocompare'); ?>" class="button">ดูโครงการใหม่ทั้งหมด</a></div>
					<div class="clearfix"></div>

					<!--  ID : 3 = banner หน้าแรก เหนือ footer -->
					<div class="row">
					<div class="column banner">
            <div id="" class="banner-carousel owl-carousel owl-theme">
					<?php
              $banner_3 = $this->Page_model->get_banner_location(3);
              $sfilepath_banner = base_url().'uploads/banner/';
              $banner_count = count($banner_3);

              // $banner_3_arr = array();
              for ($i = 0; $i < $banner_count;  ++$i) {
                  $banner_3_id = (!empty($banner_3[$i]->banner_id)) ? $banner_3[$i]->banner_id : '';

                  //UPDATE DISPLAY
                  $displayed = (!empty($banner_3[$i]->displayed)) ? $banner_3[$i]->displayed : '';
                  $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_3_id, 'banner');

                  $banner_3_title = (!empty($banner_3[$i]->banner_title)) ? $banner_3[$i]->banner_title : '';
                  $banner_3_url = (!empty($banner_3[$i]->banner_url)) ? $banner_3[$i]->banner_url : '#';
                  $banner_3_pic_thumb = (!empty($banner_3[$i]->pic_thumb)) ? $sfilepath_banner.$banner_3[$i]->pic_thumb : 'http://placehold.it/1400x175/333?text=1400x175';
                  // $banner_3_arr[] = '<a href="'.$banner_3_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_3_id.'"><img src="'.$banner_3_pic_thumb.'" alt="'.$banner_3_title.'" title=""></a>';
                  echo $banner_3_arr = '<div class="item"><a href="'.$banner_3_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_3_id.'"><img src="'.$banner_3_pic_thumb.'" alt="'.$banner_3_title.'" title=""></a></div>';
              }
              // shuffle($banner_3_arr);
              // echo (!empty($banner_3_arr[0])) ? $banner_3_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/1400x175/333?text=1400x175" alt="1400x175" title="1400x175"></a>';
                    ?>
					   </div>
					</div>
					</div>

				</div>

			</div>
		</div>
	</div>
	<?php $this->load->view('partials/footer', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'BodyClass' => ''));?>
<script>
		$(".btn-delete").click(function(event){
			var id = $(this).attr("id");
					$.ajax({
						url: "<?php echo base_url('page/remove_compare'); ?>",
						type: "POST",
						data: {
							compare_condo_id : id,
						},
						cache:false,
						success: function (data)
						{
							//console.log(data);
							window.location.reload();
						},
						error: function (xhr, desc, err)
						{
							console.log( err );
						}
					});
				event.preventDefault();
				return false;

		});

$(document).ready(function() {

	$("#owl-demo").owlCarousel({
		items : 4,
		lazyLoad : true,
		navigation : true,
		autoPlay		: false
	});

});

</script>
