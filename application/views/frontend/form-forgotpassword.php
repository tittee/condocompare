<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>''));
$member_id = (!empty($data['member_id']))? $data['member_id'] : 0;
// $this->primaryclass->pre_var_dump($member_id);

?>

	<div id="container">
	<div class="bg-cs-gray-second">
		<div class="row align-middle">
			<div class="columns">
				<nav aria-label="You are here:" role="navigation">
					<ul class="breadcrumbs">
						<li><a href="#">หน้าแรก</a></li>
						<li><span class="show-for-sr">Current:</span> <?php echo $title; ?></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
	<div class="bg-cs-gray clearfix">
		<h3 class="text-center color-blue page-title"><?php echo $title; ?></h3>
	</div>
	<div class="row content-inside">
		<!--LEFT-->
		<div class="columns">

			<form action="" method="post" id="form_reset" autocomplete="off">
				<div class="well-gray">


					<hr class="clearfix">

					<div class="row">
						<p class="text-center warning">
							ระบุรหัสผ่านใหม่
						</p>

						<div class="medium-6 medium-centered  mar-top-small">
							<label for="email">รหัสผ่านใหม่</label>
							<div class="input-group">
								<input type="password" name="new_password" id="new_password" placeholder="Password" required>
							</div>

						</div>
						<div class="medium-6 medium-centered ">
							<label for="email">ยืนยันรหัสผ่านใหม่</label>
							<div class="input-group">
								<input type="password" name="cf_new_password" id="cf_new_password" placeholder="Password" required>
							</div>

						</div>

						<div class="medium-6 medium-centered ">
							<div id="result"></div>
							<button name="btn-forgot" id="btn-forgot" type="button" class="button button-red-white">ยืนยัน</button>
							<input type="hidden" name="action" value="reset_password">
							<input type="hidden" id="member_id" name="member_id" value="<?php echo $member_id ?>">
						</div>

					</div>


				</div>
			</form>
		</div>

		<!--RIGHT-->
	</div>
</div>
	<?php $this->load->view('partials/footer');?>
	<script>

	$(document).ready(function(){
		$("#new_password").keyup(validate);
		$("#cf_new_password").keyup(validate);
	})

	function validate() {
		var pwd1 = $('#new_password').val();
		var pwd2 = $('#cf_new_password').val();
    if(pwd1 === pwd2) {
       $("#result").text("รหัสผ่านตรงกัน");
         return true;
    }
    else {
        $("#result").text("รหัสผ่านไม่ตรงกัน");
        return false;
    }
	}

	// $("#form_reset").submit(function (event) {
	$("#btn-forgot").click(function (event) {
		event.preventDefault();
    var data = $("#form_reset").serialize();
		var member_id = $('#member_id').val();
		var password = $('#new_password').val();
		var cf_password = $('#cf_new_password').val();
		// console.log(data);
		if( password === '' )
		{
			swal({
				title: 	"เกิดข้อผิดพลาด!",
				text: 	"กรุณากรอกข้อมูลให้ถูกต้อง.....",
				type: 	"warning",
			});
			return false;
		}
		else if( cf_password === '' )
		{
			swal({
				title: 	"เกิดข้อผิดพลาด!",
				text: 	"กรุณากรอกข้อมูลให้ถูกต้อง....",
				type: 	"warning",
			});
			return false;
		}
		else if( password !== cf_password || cf_password !== password )
		{
			swal({
				title: 	"เกิดข้อผิดพลาด!",
				text: 	"กรุณากรอกข้อมูลให้ถูกต้อง....",
				type: 	"warning",
			});
			return false;
		}
		else
		{
			var url = '<?php echo site_url('page/m_form_forgotpassword/'.$member_id); ?>';
			$.ajax({
				method: 'POST',
				url: url,
				data: data,
				success: function (data) {
					// console.log(data);
					if( data.success === true ){
						$('#message').html(data.message);
						$('#panel-message').show();

						$('.fa').addClass('fa-check-circle');
						setInterval(function(){
							window.location.replace('<?php echo base_url(); ?>member-login');
						}, 3000);
						return true;
					}else{
						$('#message').html(data.message);
						$('#panel-message').show();
						$('.fa').addClass('fa-exclamation-triangle');
						setInterval(function(){
							window.location.reload();
						}, 3000);
						return false;
					}
				},
				error: function () {
					$('#message').html(data.message);
					$('#panel-message').show();
					$('.fa').addClass('fa-exclamation-triangle');
					setInterval(function(){
						window.location.reload();
					}, 3000);
					return false;
				}
			});
		}
	});

	</script>
