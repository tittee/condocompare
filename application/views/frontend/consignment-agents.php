<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>''));
?>

	<div class="container">
		<div class="bg-cs-gray-second">
			<div class="row align-middle">
				<div class="columns">
					<nav aria-label="You are here:" role="navigation">
						<?php echo $breadcrumbs; ?>
					</nav>
				</div>
			</div>
		</div>
		<div class="bg-cs-gray clearfix">
			<h3 class="text-center color-blue page-title"><?php echo $title; ?></h3>
		</div>
	</div>



	<div id="container">
		<div id="highlight" class="row">
			<div class="sub-container row">

				<div class="small-12 column ">
					<form action="" method="post" role="form" name="form_agents" id="form_agents" class="form-contact" enctype="multipart/form-data" data-abide novalidate>

						<div id="foo"></div>
						<div id="bar"></div>
						<div data-abide-error class="alert callout" style="display: none;">
					    <p><i class="fi-alert"></i> กรุณากรอกข้อมุลให้ครบถ้วน..</p>
					  </div>

						<div id="success" class=" callout" data-closable="slide-out-right" style="display: none;">
						  <p id="message" style="margin-bottom: 0;"></p>
						  <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>

						<div class="row">
							<div class="medium-8 columns">
								<h3><?php echo $title; ?></h3>
								<div class="row clearfix">
									<div class="medium-3 large-2 columns">
										<label for="left-label" class="f_size1">หัวข้อ *</label>
									</div>
									<div class="medium-9 large-10 columns">
										<div class="input-group">
											<label>
												<input type="text" name="subject" id="subject" class="input-group-field validation" aria-describedby="help_title" required>
												<span class="form-error" id="help_title">
													กรุณาระบุ ชื่อหัวข้อที่ต้องติดต่อด้วยค่ะ...
												</span>
											</label>
										</div>
									</div>
									<!--  END COL -->
								</div>
								<!-- END ROW -->

								<div class="row clearfix">
									<div class="medium-3 large-2 columns">
										<label for="left-label" class="f_size1">ชื่อ - นามสกุล *</label>
									</div>
									<div class="medium-9 large-10 columns">

										<div class="input-group">
											<label>
												<input type="text" name="fullname" id="fullname" class="input-group-field validation" aria-describedby="help_name" required>
												<span class="form-error" id="help_name">
													กรุณาระบุ ชื่อ-นามสกุลด้วยค่ะ...
												</span>
											</label>
										</div>
									</div>
									<!--  END COL -->
								</div>
								<!-- END ROW -->

								<div class="row clearfix">
									<div class="medium-3 large-2 columns">
										<label for="left-label" class="f_size1">อีเมล์ *</label>
									</div>
									<div class="medium-9 large-10 columns">
										<div class="input-group">
											<label>
												<input type="email" name="email" id="email" class="input-group-field validation"  aria-describedby="help_email" required>
												<span class="form-error" id="help_email">
								          กรุณาระบุ อีเมล์ให้ถูกต้องด้วยค่ะ...
								        </span>
											</label>
										</div>
									</div>
									<!--  END COL -->
								</div>
								<!-- END ROW -->

								<div class="row clearfix">
									<div class="medium-3 large-2 columns">
										<label for="left-label" class="f_size1">เบอร์โทร *</label>
									</div>
									<div class="medium-9 large-10 columns">
										<div class="input-group">
											<label>
												<input type="text" name="telephone" id="telephone" class="input-group-field validation" aria-describedby="help_telephone" required>
												<span class="form-error" id="help_telephone">
													กรุณาระบุ เบอร์ติดต่อด้วยค่ะ...
												</span>
											</label>
										</div>
									</div>
									<!--  END COL -->
								</div>
								<!-- END ROW -->

								<div class="row clearfix">
									<div class="medium-3 large-2 columns">
										<label for="left-label" class="f_size1">ไฟล์รายละเอียด </label>
									</div>
									<div class="medium-9 large-10 columns">
										<div class="input-group">
											<label>
	                      <input type="file" name="file_agents" class="input-group-field validation"  value="" required="">
												<span class="form-error" id="help_telephone">
													กรุณาใส่ ไฟล์เพื่อการฝากทรัพย์ด้วยค่ะ...
												</span>
											</label>
										</div>
                    <p class="text-red">อนุญาติให้อัฟโหลด ไฟล์ได้ไม่เกิน 4 MB และ ไฟล์ประเภท .pdf, .doc, .docx, .xls, .xlsx, .csv เท่านั้น</p>
									</div>
								</div>
								<!-- END ROW -->

                <div class="row clearfix">
									<div class="medium-3 large-2 columns">
										<label for="left-label" class="f_size1">ข้อความ</label>
									</div>
									<div class="medium-9 large-10 columns">
										<div class="input-group">
											<textarea id="message" name="message" cols="10" rows="3"></textarea>
										</div>
									</div>
								</div>
								<!-- END ROW -->

								<div class="row clearfix">
									<div class="medium-3 large-2 columns">
										<label for="left-label" class="f_size1">ตรวจสอบตัวตน</label>
									</div>
									<div class="small-6 medium-4 large-4 columns">
										<div class="input-group">
											<?php echo $data['captcha']['image']; ?>
										</div>
									</div>
									<div class="small-6 medium-5 large-6 end columns">
										<div class="input-group">
											<input type="text" name="captcha" id="captcha" class="input-group-field validation" aria-describedby="help_captcha" required>
											<span class="form-error" id="help_captcha">
												กรุณาระบุ การตรวจสอบตัวตนให้ถูกต้อง...
											</span>
										</div>
									</div>
								</div>
								<!-- END ROW -->

								<div class="row clearfix">
									<div class="small-12 large-push-2 large-10 columns">
										<div class="input-group">
											<button type="submit" value="submit" class="button button-send button-red-white expanded">ส่งข้อความ</button>
											<input type="hidden" name="action" value="action">
										</div>
									</div>
								</div>
								<!-- END ROW -->
							</div>

							<!-- !! CONTACT INPUT -->
							<div class="medium-4 columns">
								<!-- <div class="small-12 medium-5 large-3 column "> -->
									<p>
										<strong>Harrison Public Company Limited</strong>
									</p>
									<p>
										999/9 The Offices at Centralworld <br>
										Unit 2408-2410 , 24 th Floor <br>
										Rama 1 Rd. Pathumwan,	<br>
										Bangkok 10330
									</p>
									<ul>
										<li>Tel : + (66) 2 264 5666</li>
										<li>Fax : + (66) 2 251 5397</li>
										<li>Email : webmaster@harrison.co.th</li>
									</ul>
								<!-- </div> -->
							</div>
							<!-- !! CONTACT INFO -->



							</div>
						</div>
					</form>
				</div>

			</div>
		</div>
	</div>



	<?php $this->load->view('partials/footer');?>
		<script>
		$(document)
		  // field element is invalid
		  .on("invalid.zf.abide", function(ev,elem) {
		    // console.log("Field id "+ev.target.id+" is invalid");
		  })
		  // field element is valid
		  .on("valid.zf.abide", function(ev,elem) {
		    // console.log("Field name "+elem.attr('name')+" is valid");
		  })
		  // form validation failed
		  .on("forminvalid.zf.abide", function(ev,frm) {
		    // console.log("Form id "+ev.target.id+" is invalid");
		  })
		  // form validation passed, form will submit if submit event not returned false
		  .on("formvalid.zf.abide", function(ev,frm) {
		    // console.log("Form id "+frm.attr('id')+" is valid");
		    // ajax post form
		  })
		  // to prevent form from submitting upon successful validation
		  .on("submit", function(ev) {
		    ev.preventDefault();
		    // console.log("Submit for form id "+ev.target.id+" intercepted");

				$.isLoading({
					 'text'  : "Loading....",
					 'tpl'   : '<span class="isloading-wrapper %wrapper%">%text%<i class="fa fa-spinner glyphicon-refresh-animate"></i></span>',
				 });

				var formData = new FormData($('#form_agents')[0]);
				<?php
					$ajax_url = site_url('consignment-agents');
				?>

				var url = "<?php echo $ajax_url; ?>";
				$.ajax({
					url: url,
					type: "POST",
					data: formData,
	        async: false,
	        cache: false,
	        contentType: false,
	        processData: false,
					success: function (data, status) {
						// console.log(data);

						var type = ( data.success == true ? "success" : "warning");
						$('#message').html(data.message);
						$('#success').addClass(type);
						$('#success').fadeIn('fast');
						$('html, body').animate({
				        scrollTop: $("#success").offset().top
				    }, 2000);


						// Re-enabling event
              setTimeout( function(){
                  $.isLoading( "hide" );
                  window.location.reload();
                  // console.log( "Content Loaded" );
              }, 5000 );

					},


					error: function (xhr, desc, err) {
						console.log(err);
					},
				});


		  });
		// You can bind field or form event selectively
		$("#foo").on("invalid.zf.abide", function(ev,el) {
		  alert("Input field foo is invalid");
		});
		$("#bar").on("formvalid.zf.abide", function(ev,frm) {
		  alert("Form is valid, finally!");
		  // do something perhaps
		});



		</script>
