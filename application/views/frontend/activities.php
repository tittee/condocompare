<?php
defined('BASEPATH') or exit();
/*HEADER*/
$this->load->view('partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'js' => isset($js) ? $js : array(), 'BodyClass' => ''));

$sfilepath = base_url().'uploads/activities';
$keyword = (!empty($this->input->get('keyword'))) ? $this->input->get('keyword') : '';
?>
		<div class="container">
			<div class="bg-cs-gray-second">
				<div class="row align-middle">
					<div class="columns">
						<nav aria-label="You are here:" role="navigation">
							<?php echo $breadcrumbs; ?>
						</nav>
					</div>
				</div>
			</div>
			<div class="bg-cs-gray clearfix">
				<h3 class="text-center color-blue page-title"><?php echo $title;?></h3>
			</div>
			<div class="row mar-top-large align-middle">
				<div class="title small-12 medium-12 medium-centered column">
					<!--  ID : 11 = banner หน้ากิจกรรม  ด้านบน header  -->
					<div id="" class="banner-carousel owl-carousel owl-theme">
					<?php
            $banner_11 = $this->Page_model->get_banner_location(11);
            $sfilepath_banner = base_url().'uploads/banner/';
            $banner_count = count($banner_11);

            // $banner_11_arr = array();
            for ($i = 0; $i < $banner_count;  ++$i) {
                $banner_11_id = (!empty($banner_11[$i]->banner_id)) ? $banner_11[$i]->banner_id : '';

                //UPDATE DISPLAY
                $displayed = (!empty($banner_11[$i]->displayed)) ? $banner_11[$i]->displayed : '';
                $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_11_id, 'banner');

                $banner_11_title = (!empty($banner_11[$i]->banner_title)) ? $banner_11[$i]->banner_title : '';
                $banner_11_url = (!empty($banner_11[$i]->banner_url)) ? $banner_11[$i]->banner_url : '#';
                $banner_11_pic_thumb = (!empty($banner_11[$i]->pic_thumb)) ? $sfilepath_banner.$banner_11[$i]->pic_thumb : 'http://placehold.it/1400x175/333?text=1400x175';
                // $banner_11_arr[] = '<a href="'.$banner_11_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_11_id.'"><img src="'.$banner_11_pic_thumb.'" alt="'.$banner_11_title.'" title=""></a>';
                echo $banner_11_arr = '<div class="item"><a href="'.$banner_11_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_11_id.'"><img src="'.$banner_11_pic_thumb.'" alt="'.$banner_11_title.'" title=""></a></div>';
            }
            // shuffle($banner_11_arr);
            // echo (!empty($banner_11_arr[0])) ? $banner_11_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/1400x175/333?text=1400x175" alt="1400x175" title="1400x175"></a>';
        	?>
					</div>

				</div>
			</div>

			<!-- ACTIVITIES SLIDER-->
			<div class="row clearfix">
				<!--LEFT-->
				<div class="small-12 large-9 column">
          <section>
						<h2 class="title-red-black"><?php echo $data['activities_total']; ?><span> กิจกรรม</span></h2>
					</section>

					<?php
					// $rows_data = count($data_banner);
					if( count($data_banner) > 0 ){
					?>

					<div class="row clearfix" id="news-demo">
						<?php
            //LIST CONDO CATEGORY
            $i = 0;

            foreach ($data_banner as $row) {
                $activities_id = $row->activities_id;
                $activities_title = (!empty($row->activities_title)) ? $row->activities_title : 'ไม่ระบุหัวข้อ';
                $activities_title_limit = character_limiter($activities_title, 40);

                $activities_caption = (!empty($row->activities_caption)) ? $row->activities_caption : '-';
                $activities_caption_limit = character_limiter($activities_caption, 80);

                $fk_activities_category_id = (!empty($row->fk_activities_category_id)) ? $row->fk_activities_category_id : '0';
                $activities_category_name = (!empty($fk_activities_category_id) || $fk_activities_category_id !== 0) ? $this->M->get_name('activities_category_id, activities_category_name', 'activities_category_id', 'activities_category_name', $fk_activities_category_id, 'activities_category') : '-';
                $activities_category_name_t = (!empty($activities_category_name[0])) ? $activities_category_name[0]->activities_category_name : '-';

                $pic_large = $sfilepath.'/'.$row->activities_pic_large;
                ?>
						<article>
							<div class="text-content">
								<h2><?php echo $activities_title_limit; ?></h2>
								<p><?php echo $activities_caption_limit; ?></p>
								<a href="<?php echo site_url('activities-details/'.$activities_id); ?>" class="button-link read-more">read more</a>
							</div>
							<div class="image-content"><img src="<?php echo $pic_large; ?>" alt="<?php echo $activities_title; ?>"></div>
						</article>
						<?php }//END FOREACH ?>
					</div>
					<?php }//END IF ?>
					<!--TITLE ACTIVITIES-->
					<div class="row clearfix">
						<div class="medium-12 columns">
							<?php
              if (!empty($keyword)) {
                  echo '<h2 class="title-blue-bg mar-top-large right">คำค้นหา : '.$keyword.'</h2>';
              }
              ?>
							<input type="hidden" name="keyword" value="<?php echo $keyword; ?>">



							<div class="tabs-content tabs-category-content">
							<?php
                //LIST CONDO CATEGORY
                $j = 0;
								?>
								<div id="" class="">
									<?php
									foreach ($data['activities'] as $row) {
	                  //echo $j;
	                  $activities_id = $row->activities_id;
	                  $activities_title = (!empty($row->activities_title)) ? $row->activities_title : 'ไม่ระบุหัวข้อ';
	                  $activities_caption = (!empty($row->activities_caption)) ? $row->activities_caption : '-';

	                  // $fk_activities_category_id = (!empty($row->fk_activities_category_id)) ? $row->fk_activities_category_id : '0';
	                  // $activities_category_name = (!empty($fk_activities_category_id) || $fk_activities_category_id !== 0) ? $this->M->get_name('activities_category_id, activities_category_name', 'activities_category_id', 'activities_category_name', $fk_activities_category_id, 'activities_category') : '-';
	                  // $activities_category_name_t = (!empty($activities_category_name[0])) ? $activities_category_name[0]->activities_category_name : '-';

	                  $visited = (!empty($row->visited)) ? $row->visited : '-';
	                  $pic_thumb = $sfilepath.'/'.$row->activities_pic_thumb;
									?>

									<div class="row border-bottom mar-top-medium">
										<div class="small-12 large-4 columns hover"><a href="<?php echo site_url('activities-details/'.$activities_id); ?>"><img src="<?php echo $pic_thumb; ?>" alt=""></a></div>
										<div class="small-12 large-8 columns">
											<h4><a href="<?php echo site_url('activities-details/'.$activities_id); ?>" class="color-red grow"><?php echo $activities_title; ?></a></h4>
											<p class="date-news"><?php echo $this->dateclass->DateTimeFullFormat($row->createdate, 0, 1, 'Th'); ?> </p>
											<p class="caption-content"><a href="<?php echo site_url('activities-details/'.$activities_id); ?>" class="grow"><?php echo $activities_caption; ?></a></p>
											<p class="mar-top-medium"><a href="<?php echo site_url('activities-details/'.$activities_id); ?>" class="readmore-red">รายละเอียดเพิ่มเติม</a></p>
										</div>
									</div>
									<?php
								} // !! END FOREACH ?>
								</div>
							</div>

						</div>

					</div>
					<div class=" clearfix mar-top-large">
						<div class="column text-center">
							<?php echo $data['links']; ?>
						</div>
						<div class="clearfix"></div>
					</div>

				</div>
				<!--RIGTH-->
				<div class="small-12 large-3 column">
					<div class="bg-cs-blue well columns mar-bott-large">
						<form id="form_search" action="<?php echo site_url('activities'); ?>" method="get">
						<div style="margin-bottom: 0;" class="input-group">
								<input type="text" name="keyword" placeholder="ค้นหากิจกรรม" class="input-group-field" required>
								<div class="input-group-button">
									<button name="submit" style="font-size: .85rem;" class="button button-red-white"><i class="fa fa-search"></i></button>
								</div>
								<input type="hidden" name="action" value="action">
						</div>
					</form>
					</div>

					<!--  ID : 13 = banner หน้ากิจกรรม ด้านบน ขวามือ -->

						<div class="small-12 columns">
					<div id="" class="banner-carousel owl-carousel owl-theme">
					<?php
              $banner_13 = $this->Page_model->get_banner_location(13);
              $sfilepath_banner = base_url().'uploads/banner/';
              $banner_count = count($banner_13);

              // $banner_13_arr = array();
              for ($i = 0; $i < $banner_count;  ++$i) {
                  $banner_13_id = (!empty($banner_13[$i]->banner_id)) ? $banner_13[$i]->banner_id : '';

                  //UPDATE DISPLAY
                  $displayed = (!empty($banner_13[$i]->displayed)) ? $banner_13[$i]->displayed : '';
                  $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_13_id, 'banner');

                  $banner_13_title = (!empty($banner_13[$i]->banner_title)) ? $banner_13[$i]->banner_title : '';
                  $banner_13_url = (!empty($banner_13[$i]->banner_url)) ? $banner_13[$i]->banner_url : '#';
                  $banner_13_pic_thumb = (!empty($banner_13[$i]->pic_thumb)) ? $sfilepath_banner.$banner_13[$i]->pic_thumb : 'http://placehold.it/380x400/333?text=380x400';
                  // $banner_13_arr[] = '<a href="'.$banner_13_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_13_id.'"><img src="'.$banner_13_pic_thumb.'" alt="'.$banner_13_title.'" title=""></a>';
                  echo $banner_13_arr = '<div class="item"><a href="'.$banner_13_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_13_id.'"><img src="'.$banner_13_pic_thumb.'" alt="'.$banner_13_title.'" title=""></a></div>';
              }
              // shuffle($banner_13_arr);
              // echo (!empty($banner_13_arr[0])) ? $banner_13_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/380x400/333?text=380x400" alt="380x400" title="380x400"></a>';
          ?>
					</div>
					</div>

					<h2 class="title-blue-bg right">Most Read<span>	This Month</span></h2>
					<div class="inside">
						<article>

							<?php
                                //LIST CONDO CATEGORY
                                foreach ($data_mostread as $row) {
                                    $activities_id = $row->activities_id;
                                    $activities_title = (!empty($row->activities_title)) ? $row->activities_title : '-';
                                    $activities_title_limit = character_limiter($activities_title, 20);

                                    $createdate = $row->createdate;
                                    $createdate_t = (isset($createdate) || !empty($createdate)) ? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, 'Th') : 'ยังไม่มีข้อมูล';
                                    ?>

							<div class="row clearfix news-sidebar">
								<div class="columns">
									<div class="well-gray">
										<h5><a href="<?php echo site_url('activities-details/'.$activities_id);?>" class="color-red"><?php echo $activities_title_limit;?></a></h5>
										<p class="mar-bott-zero date_news"><?php echo $createdate_t;
                                    ?></p>
									</div>
								</div>
							</div>

							<?php

                                }//END FOREACH ?>

						</article>
					</div>


				</div>
			</div>
			<div class="row clearfix">
				<div class="title small-12 medium-8 medium-centered column">
					<!--  ID : 12 = banner หน้ากิจกรรม ด้านบน footer -->
					<div id="" class="banner-carousel owl-carousel owl-theme">
					<?php
                        $banner_12 = $this->Page_model->get_banner_location(12);
                        $sfilepath_banner = base_url().'uploads/banner/';
                        $banner_count = count($banner_12);

                        // $banner_12_arr = array();
                        for ($i = 0; $i < $banner_count;  ++$i) {
                            $banner_12_id = (!empty($banner_12[$i]->banner_id)) ? $banner_12[$i]->banner_id : '';

                            //UPDATE DISPLAY
                            $displayed = (!empty($banner_12[$i]->displayed)) ? $banner_12[$i]->displayed : '';
                            $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_12_id, 'banner');

                            $banner_12_title = (!empty($banner_12[$i]->banner_title)) ? $banner_12[$i]->banner_title : '';
                            $banner_12_url = (!empty($banner_12[$i]->banner_url)) ? $banner_12[$i]->banner_url : '#';
                            $banner_12_pic_thumb = (!empty($banner_12[$i]->pic_thumb)) ? $sfilepath_banner.$banner_12[$i]->pic_thumb : 'http://placehold.it/1400x175/333?text=1400x175';
                            // $banner_12_arr[] = '<a href="'.$banner_12_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_12_id.'"><img src="'.$banner_12_pic_thumb.'" alt="'.$banner_12_title.'" title=""></a>';
                                                        echo $banner_12_arr = '<div class="item"><a href="'.$banner_12_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_12_id.'"><img src="'.$banner_12_pic_thumb.'" alt="'.$banner_12_title.'" title=""></a></div>';
                        }
                        // shuffle($banner_12_arr);
                        // echo (!empty($banner_12_arr[0])) ? $banner_12_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/1400x175/333?text=1400x175" alt="1400x175" title="1400x175"></a>';
                    ?>
					</div>

				</div>
			</div>
			<div class="row clearfix"></div>
		</div>
<?php $this->load->view('partials/footer');?>
<?php
if (isset($slippry)) {
    ?>
<!--activities SLIDER : SLIPPRY-->
<script type="text/javascript">
	$(document).ready(function() {
		jQuery('#activities-demo').slippry({
			// general elements & wrapper
			slippryWrapper: '<div class="sy-box activities-slider" />', // wrapper to wrap everything, including pager
			elements: 'article', // elments cointaining slide content

			// options
			adaptiveHeight: false, // height of the sliders adapts to current
			captions: false,


			// transitions
			transition: 'horizontal', // fade, horizontal, kenburns, false
			speed: 1200,
			pause: 8000,

			// slideshow
			autoDirection: 'prev'
		});
	});
</script>
<?php

}//END IF SLIPPY ?>
