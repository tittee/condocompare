<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>''));

$sfilepath = base_url().'uploads/land';
?>


	<style type="text/css">
		#map-canvas {
  height: 20rem;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 0;
}

#containermaps {
  z-index: 100;
  position: relative;
}
	</style>

	<div class="container">
		<div class="bg-cs-gray-second">
			<div class="row align-middle">
				<div class="columns">
					<nav aria-label="You are here:" role="navigation">
						<?php echo $breadcrumbs; ?>
					</nav>
				</div>
			</div>
		</div>
		<div class="bg-cs-gray clearfix">
			<h3 class="text-center color-blue page-title"><?php echo $title; ?></h3>
		</div>
	</div>

	<header id="" class="">
		<div class="container">
			<div id="containermaps">
			</div>
    	<div id='map-canvas'></div>
		</div>
	</header>


	<div id="container">
		<div id="highlight" class="row">
			<div class="sub-container row">
				<!-- <div class="small-12 medium-5 large-3 column ">
					<p>
						<strong>Harrison Public Company Limited</strong>
					</p>
					<p>
						999/9 The Offices at Centralworld <br>
						Unit 2408-2410 , 24 th Floor <br>
						Rama 1 Rd. Pathumwan,	<br>
						Bangkok 10330
					</p>
					<ul>
						<li>Tel : + (66) 2 264 5666</li>
						<li>Fax : + (66) 2 251 5397</li>
						<li>Email : webmaster@harrison.co.th</li>
					</ul>
				</div> -->
				<!-- <div class="small-12 medium-7 large-9 column "> -->
				<div class="small-12 column ">
					<form action="" method="post" role="form" name="form_contactus" id="form_contactus" class="form-contact"  data-abide novalidate>
						<div data-abide-error class="alert callout" style="display: none;">
					    <p><i class="fi-alert"></i> กรุณากรอกข้อมุลให้ครบถ้วน..</p>
					  </div>
						<div class="row">

							<div id="foo"></div>
							<div id="bar"></div>
							<div data-abide-error class="alert callout" style="display: none;">
						    <p><i class="fi-alert"></i> กรุณากรอกข้อมุลให้ครบถ้วน..</p>
						  </div>

							<div id="success" class=" callout" data-closable="slide-out-right" style="display: none;">
							  <p id="message" style="margin-bottom: 0;"></p>
							  <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							    <span aria-hidden="true">&times;</span>
							  </button>
							</div>

							<div class="medium-8 columns">
								<h3><?php echo $title; ?></h3>
								<div class="row clearfix">
									<div class="medium-3 large-2 columns">
										<label for="left-label" class="f_size1">หัวข้อ *</label>
									</div>
									<div class="medium-9 large-10 columns">
										<div class="input-group">
											<label>
												<input type="text" name="subject" id="subject" class="input-group-field validation" aria-describedby="help_title" required>
												<span class="form-error" id="help_title">
													กรุณาระบุ ชื่อหัวข้อที่ต้องติดต่อด้วยค่ะ...
												</span>
											</label>
										</div>
									</div>
									<!--  END COL -->
								</div>
								<!-- END ROW -->
								<div class="row clearfix">
									<div class="medium-3 large-2 columns">
										<label for="left-label" class="f_size1">ชื่อ - นามสกุล *</label>
									</div>
									<div class="medium-9 large-10 columns">
										<div class="input-group">
											<label>
												<input type="text" name="fullname" id="fullname" class="input-group-field validation" aria-describedby="help_name" required>
												<span class="form-error" id="help_name">
													กรุณาระบุ ชื่อ-นามสกุลด้วยค่ะ...
												</span>
											</label>
										</div>
									</div>
									<!--  END COL -->
								</div>
								<!-- END ROW -->
								<div class="row clearfix">
									<div class=" medium-3 large-2 columns">
										<label for="left-label" class="f_size1">อีเมล์</label>
									</div>
									<div class="medium-9 large-10 columns">
										<div class="input-group">
											<input type="email" name="email" id="email" class="input-group-field" required>
										</div>
									</div>
									<!--  END COL -->
								</div>
								<!-- END ROW -->
								<div class="row clearfix">
									<div class="medium-3 large-2 columns">
										<label for="left-label" class="f_size1">เบอร์โทร</label>
									</div>
									<div class="medium-9 large-10 columns">
										<div class="input-group">
											<input type="text" name="telephone" id="telephone" class="input-group-field" required>
										</div>
									</div>
									<!--  END COL -->
								</div>
								<!-- END ROW -->
								<div class="row clearfix">
									<div class="medium-3 large-2 columns">
										<label for="left-label" class="f_size1">ข้อความ</label>
									</div>
									<div class="medium-9 large-10 columns">
										<div class="input-group">
											<textarea id="message" name="message" cols="10" rows="3"></textarea>
										</div>
									</div>
								</div>
								<!-- END ROW -->
                <div class="row clearfix">
									<div class="medium-3 large-2 columns">
										<label for="left-label" class="f_size1">ตรวจสอบตัวตน</label>
									</div>
									<div class="small-6 medium-4 large-4 columns">
										<div class="input-group">
											<?php echo $data['captcha']['image']; ?>
										</div>
									</div>
									<div class="small-6 medium-5 large-6 columns">
										<div class="input-group">
											<input type="text" name="captcha" id="captcha" class="input-group-field validation" aria-describedby="help_captcha" required>
											<span class="form-error" id="help_captcha">
												กรุณาระบุ การตรวจสอบตัวตนให้ถูกต้อง...
											</span>
										</div>
									</div>
								</div>
								<!-- END ROW -->
								<div class="row clearfix">
									<div class="small-12 large-push-2 large-10 columns">
										<div class="input-group">
											<button name="submit_contact" value="action" id="submit_contact" class="button button-red-white expanded">ส่งข้อความ</button>
											<input type="hidden" name="action" value="action">
										</div>
									</div>
								</div>
							</div>
							<!-- !! CONTACT INPUT -->

							<div class="medium-4 columns">
								<!-- <div class="small-12 medium-5 large-3 column "> -->
									<p>
										<strong>Harrison Public Company Limited</strong>
									</p>
									<p>
										999/9 The Offices at Centralworld <br>
										Unit 2408-2410 , 24 th Floor <br>
										Rama 1 Rd. Pathumwan,	<br>
										Bangkok 10330
									</p>
									<ul>
										<li>Tel : + (66) 2 264 5666</li>
										<li>Fax : + (66) 2 251 5397</li>
										<li>Email : webmaster@harrison.co.th</li>
									</ul>
								<!-- </div> -->
							</div>
							<!-- !! CONTACT INFO -->
						</div>
					</form>
				</div>

			</div>
		</div>
	</div>



	<?php $this->load->view('partials/footer');?>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtK77y4LHtYZ5ZoVeVVwpkMUoy-Eq9JRA&callback=initMap"></script>
		<script>

		function initMap() {
        var myLatLng = {lat: 13.7460969, lng: 100.538434};

        var map = new google.maps.Map(document.getElementById('map-canvas'), {
          zoom: 12,
          center: myLatLng
        });

				var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h1 id="firstHeading" class="firstHeading">Harrison </h1>'+
            '<div id="bodyContent">'+
            '<p><b>the Offices At Central World</b>' +
            'ชั้น L24 ถนน พระรามที่ 1 แขวง ปทุมวัน เขต ปทุมวัน กรุงเทพมหานคร 10330</p>'+
            '</div>'+
            '</div>';

				var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Harrison (Offices)'
        });

				marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
				infowindow.open(map,marker);

      }
		// $("#form_contactus").submit(function (event) {
		// 	var formData = new FormData($(this)[0]);
		// 	<?php
		// 		$ajax_url = "page/contactus";
		// 	?>
		// 	//console.log(formData);
		// 	var url = "<?php //echo base_url() . $ajax_url; ?>";
		// 	$.ajax({
		// 		url: url,
		// 		type: "POST",
		// 		data: formData,
		// 		cache: false,
		// 		processData: false,
		// 		contentType: false,
		// 		context: this,
		// 		success: function (data, status) {
		// 			console.log(data);
		//
		// 			// swal({
		// 			// 	title: "Success Data!",
		// 			// 	text: "ระบบได้ทำการส่งอีเมล์ การติดต่อของท่านเรียบร้อยแล้ว..",
		// 			// 	type: "info",
    //       //
		// 			// }, function (isConfirm) {
		// 			// 	if (isConfirm) {
		// 			// 		window.location.reload();
		// 			// 	} else {
		// 			// 		window.location.reload();
		// 			// 	}
		// 			// });
		// 		},
		// 		error: function (xhr, desc, err) {
		// 			console.log(err);
		// 		},
		// 	});
		// 	event.preventDefault();
		// 	return false;
		// });

		$(document)
		  // field element is invalid
		  .on("invalid.zf.abide", function(ev,elem) {
		    // console.log("Field id "+ev.target.id+" is invalid");
		  })
		  // field element is valid
		  .on("valid.zf.abide", function(ev,elem) {
		    // console.log("Field name "+elem.attr('name')+" is valid");
		  })
		  // form validation failed
		  .on("forminvalid.zf.abide", function(ev,frm) {
		    // console.log("Form id "+ev.target.id+" is invalid");
		  })
		  // form validation passed, form will submit if submit event not returned false
		  .on("formvalid.zf.abide", function(ev,frm) {
		    // console.log("Form id "+frm.attr('id')+" is valid");
		    // ajax post form
		  })
		  // to prevent form from submitting upon successful validation
		  .on("submit", function(ev) {
		    ev.preventDefault();
				var formData = new FormData($('#form_contactus')[0]);
		    // console.log("Submit for form id "+ev.target.id+" intercepted");

				$.isLoading({
					'text'  : "Loading....",
					'tpl'   : '<span class="isloading-wrapper %wrapper%">%text%<i class="fa fa-spinner glyphicon-refresh-animate"></i></span>',
				});

 				url = "<?php echo site_url('contact-us'); ?>";
 				//console.log(formData);
 				$.ajax({
					url: url,
					type: "POST",
					data: formData,
					cache: false,
					processData: false,
					contentType: false,
					context: this,
 					success: function (data, status) {

						// console.log(data);

						var type = ( data.success == true ? "success" : "warning");
						$('#message').html(data.message);
						$('#success').addClass(type);
						$('#success').fadeIn('fast');
						$('html, body').animate({
				        scrollTop: $("#success").offset().top
				    }, 2000);


						// Re-enabling event
              setTimeout( function(){
                  $.isLoading( "hide" );
                  window.location.reload();
                  // console.log( "Content Loaded" );
              }, 5000 );

					},


					error: function (xhr, desc, err) {
						console.log(err);
					},

 				});

 				return true;

		});

		</script>
