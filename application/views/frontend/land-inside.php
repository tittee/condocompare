<?php
defined('BASEPATH') or exit();
/*HEADER*/
$this->load->view('partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'BodyClass' => ''));

$sfilepath = base_url().'uploads/land';

$land_id = $data->land_id;

$land_property_id = (!empty($data->land_property_id)) ? $data->land_property_id : '-';

$staff_id = (!empty($data->staff_id)) ? $data->staff_id : '0';

$staff = $this->M->get_staff('staff_id, fullname, email, phone', 'staff_id', $data->staff_id, 'staff');
//$this->primaryclass->pre_var_dump($data);
$staff_ids = (!empty($staff->staff_id)) ? $staff->staff_id : '0';
$staff_fullname = (!empty($staff->fullname)) ? $staff->fullname : '-';
$staff_email = (!empty($staff->email)) ? $staff->email : '-';
$staff_phone = (!empty($staff->phone)) ? $staff->phone : '-';

$show_sale = $this->input->get('show', true);

$land_title = (!empty($data->land_title)) ? $data->land_title : '-';
$land_holder_name = (!empty($data->land_holder_name)) ? $data->land_holder_name : '-';

$property_type_id = (!empty($data->property_type_id)) ? $data->property_type_id : '-';
$type_property_title = (!empty($property_type_id) || $property_type_id !== 0  || isset($property_type_id)) ? $this->M->get_name_by_id('type_property_id', 'type_property_title', $property_type_id, 'type_property') : '-';
$type_property_title_t = (isset($type_property_title[0])) ? $type_property_title[0]['type_property_title'] : '-';

$type_decoration_id = (!empty($data->type_decoration_id)) ? $data->type_decoration_id : '-';
$type_decoration_title = (!empty($type_decoration_id) || $type_decoration_id !== 0  || isset($type_decoration_id)) ? $this->M->get_name_by_id('type_decoration_id', 'type_decoration_title', $type_decoration_id, 'type_decoration') : '-';
$type_decoration_title_t = (isset($type_decoration_title[0])) ? $type_decoration_title[0]['type_decoration_title'] : '-';

$land_expert = (!empty($data->land_expert)) ? $data->land_expert : '-';
$land_description = (!empty($data->land_description)) ? $this->primaryclass->decode_htmlspecialchars($data->land_description) : '-';

$land_alley = (!empty($data->land_alley)) ? $data->land_alley : '';
$land_road = (!empty($data->land_road)) ? $data->land_road : '';
$land_address = (!empty($data->land_address)) ? $data->land_address : '-';
$provinces_id = (!empty($data->provinces_id)) ? $data->provinces_id : '';
$provinces_name = (!empty($provinces_id) || $provinces_id !== 0) ? $this->M->get_name_by_id('provinces_id', 'provinces_name', $provinces_id, 'provinces') : '-';
$provinces_name_t = (isset($provinces_name[0])) ? $provinces_name[0]['provinces_name'] : '-';


$land_road_provinces = $this->primaryclass->land_road_provinces($land_road, $provinces_name_t);

$districts_id = (!empty($data->districts_id)) ? $data->districts_id : '';
$districts_name = (!empty($districts_id) || $districts_id !== 0) ? $this->M->get_name_by_id('districts_id', 'districts_name', $districts_id, 'districts') : '-';
$districts_name_t = (isset($districts_name[0])) ? $districts_name[0]['districts_name'] : '-';

$sub_districts_id = (!empty($data->sub_districts_id)) ? $data->sub_districts_id : '';
$sub_districts_name = (!empty($sub_districts_id) || $sub_districts_id !== 0) ? $this->M->get_name_by_id('sub_districts_id', 'sub_districts_name', $sub_districts_id, 'sub_districts') : '-';
$sub_districts_name_t = (isset($sub_districts_name[0])) ? $sub_districts_name[0]['sub_districts_name'] : '-';

$land_zipcode = (!empty($data->land_zipcode)) ? $data->land_zipcode : '-';

$land_total_price = ($data->land_total_price !== '0.00') ? $data->land_total_price : '0.00';
$land_total_price_bath = ($land_total_price !== '0.00') ? number_format($land_total_price, 0, '.', ',').' บาท' : 'ไม่ระบุ';

$land_size = ($data->land_size !== '0.00') ? $data->land_size : '0.00';
$land_size_unit = ($data->land_size !== '0.00') ? $this->primaryclass->get_square($data->land_size_unit) : 'ไม่ระบุ';
$land_size_square_rai = ($data->land_size_square_rai !== '0.00') ? $data->land_size_square_rai : '';
$land_size_square_ngan = ($data->land_size_square_ngan !== '0.00') ? $data->land_size_square_ngan : '';
$land_size_square_yard = ($data->land_size_square_yard !== '0.00') ? $data->land_size_square_yard : '';

$msg_size_unit = $this->primaryclass->get_size_square($land_size, $land_size_square_rai, $land_size_square_ngan, $land_size_square_yard,  $land_size_unit);

$land_price_per_square_meter = ($data->land_price_per_square_meter !== '0.00') ? $data->land_price_per_square_meter : '0.00';
$land_price_per_square_meter_bath = ($land_price_per_square_meter !== '0.00') ? number_format($land_price_per_square_meter, 0, '.', ',').' บาท' : 'ไม่ระบุ';

$msg_price_per_meter = $this->primaryclass->get_land_price_per_square($display_rakha = 0, $land_total_price, $land_size, $land_size_unit, $land_price_per_square_meter);

// $land_price_per_square_meter = (!empty($data->land_price_per_square_meter))? $data->land_price_per_square_meter : '-';
$land_price_per_square_mortgage = ($data->land_price_per_square_mortgage !== '0.00') ?
    'จำนอง '.$data->land_price_per_square_mortgage.' บาท / เดือน' : '';

// $land_price_per_square_mortgage = ( $land_total_price === '0.00' || $land_size === '0.00' )? "0.00" : $land_total_price / $land_size;

$createdate = $data->createdate;

$createdate_t = ($createdate !== '0000-00-00 00:00:00') ? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, 'Th') : 'ไม่ระบุ';

// $createdate_t = (isset($createdate) || !empty($createdate)) ? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, 'Th') : 'ยังไม่มีข้อมูล';

$visited = (!empty($data->visited)) ? $data->visited : '-';

$site_id = 1;
$site_loan = $this->M->get_loan_setting($site_id);
$site_mrl = (!empty($site_loan['site_mrl'])) ? $site_loan['site_mrl'] : '';
$site_mor = (!empty($site_loan['site_mor'])) ? $site_loan['site_mor'] : '';
$site_mrr = (!empty($site_loan['site_mrr'])) ? $site_loan['site_mrr'] : '';
$site_cpr = (!empty($site_loan['site_cpr'])) ? $site_loan['site_cpr'] : '';
?>
	<div id="container">
		<div class="bg-cs-gray-second">
			<div class="row align-middle">
				<div class="columns">
					<nav aria-label="You are here:" role="navigation">

            <?php echo $breadcrumbs; ?>
					</nav>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row content-inside">

			<div data-toggler="" data-animate="fade-in fade-out" class="alert callout callout secondary ease" id="panel-message" aria-expanded="true" style="display: none;">
        <p><i class="fa fa-check-circle" aria-hidden="true"></i> <span id="message"></span></p>
      </div>

			<!--LEFT-->
			<div class="small-12 large-9 columns">
				<h1><?php echo $data->land_title; ?></h1>
				<!-- <p class="location_address"><?php echo $land_road_provinces;?></p> -->
				<div class="row">
					<div class="small-6 medium-6 large-9 columns">
						<p class="location_address">
							<?php echo $data->type_zone_title . $land_road_provinces; ?>
						</p>

						<div class="row">
							<div class="medium-4 columns">
								<p class="contact_name"><i class="fa fa-eye" style="color: #E9E9E9;"></i> จำนวนผู้ชม <?php echo $visited; ?></p>
							</div>
							<div class="medium-8 columns">
								<p class="contact_name">ประกาศเมื่อ <?php echo $createdate_t;?></p>
							</div>
						</div>

						<table class="details small-12">
							<thead>
								<th>ขนาดที่ดิน</th>
							</thead>
							<tbody>
								<td>
									<?php echo $msg_size_unit; ?></td>
							</tbody>
						</table>
					</div>
					<div class="small-6 medium-6 large-3 columns">
						<div class="price"><span><?php echo $land_total_price_bath; ?> </span></div>
						<!-- <p>
							<?php echo $msg_size_unit; ?></p> -->
						<p>
							<?php echo $land_price_per_square_mortgage; ?></p>
					</div>
				</div>
				<!--.clearfix-->
				<div class="slide">
					<div id="sync1" class="owl-carousel">
						<div class="item"><img src="<?php echo $sfilepath.'/'.$data->pic_large; ?>" alt=""></div>
						<!-- First Images -->
						<?php
                            foreach ($land_image as $value_images) {
                                ?>
							<div class="item"><img src="<?php echo $sfilepath.'/'.$value_images->pic_large;
                                ?>" alt=""></div>
							<?php

                            }
                            ?>
					</div>
					<div id="sync2" class="owl-carousel">
						<div class="item"><img src="<?php echo $sfilepath.'/'.$data->pic_large; ?>" alt=""></div>
						<!-- First Images -->
						<?php
                            foreach ($land_image as $value_images) {
                                ?>
							<div class="item"><img src="<?php echo $sfilepath.'/'.$value_images->pic_large;
                                ?>" alt=""></div>
							<?php

                            }
                            ?>
					</div>
				</div>
				<div class="content-detail">
					<div class="well columns">
						<h3>รายละเอียดโครงการ</h3>

						<div class="small-12 columns">

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ชื่อโครงการ</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $land_title; ?>
									</p>
								</div>
								<!-- #End col-->

								<div class="small-6 medium-6 large-3 columns">
									<p><strong>รหัสทรัพย์สิน</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p><?php echo $land_property_id; ?></p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ประเภททรัพย์</strong></p>
								</div>
								<div class="small-6 medium-6 large-9 columns">
									<p><?php echo $type_property_title_t; ?></p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ราคา</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p><?php echo $land_total_price_bath; ?></p>
								</div>
								<!-- #End col-->
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ราคาต่อตารางวา</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $msg_price_per_meter; ?></p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>สิทธิ์ถือครอง</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $land_holder_name; ?>
									</p>
								</div>
								<!-- #End col-->

								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ขนาดที่ดิน</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $land_size; ?> / <?php echo $this->primaryclass->get_square($land_size_unit); ?>
									</p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->





							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ลงประกาศเมื่อ</strong></p>
								</div>
								<div class="small-6 medium-6 large-9 columns">
									<p>
										<?php echo $createdate_t; ?>
									</p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->
						</div>
						<div class="clearfix"></div>

						<div class="small-12 columns mar-top-medium">
							<div class="row">
								<div class="small-12 columns">
									<p><strong><u>ที่อยู่โครงการ</u></strong></p>
								</div>
							</div>
							<!-- #End col-->
						</div>
						<div class="clearfix"></div>

						<div class="small-12 columns">

							<div class="row">


								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ตรอก / ซอย</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $land_alley; ?>
									</p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ถนน</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $land_road; ?>
									</p>
								</div>
								<!-- #End col-->

								<div class="small-6 medium-6 large-3 columns">
									<p><strong>จังหวัด</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $provinces_name_t; ?>
									</p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>อำเภอ/เขต</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $districts_name_t; ?>
									</p>
								</div>
								<!-- #End col-->

								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ตำบล/แขวง</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $sub_districts_name_t; ?>
									</p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>รหัสไปรษณีย์</strong></p>
								</div>
								<div class="small-6 medium-6 large-9 columns">
									<p>
										<?php echo $land_zipcode; ?>
									</p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->
						</div>
						<div class="clearfix mar-bott-large"></div>

						<div class="small-12 columns mar-top-medium">
							<div class="row">
								<div class="small-12 medium-6 large-3 columns">
									<p><strong>รายละเอียดเพิ่มเติม</strong></p>
								</div>
								<div class="small-12 medium-6 large-9 columns">
									<p>
										<?php echo $land_description; ?>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="well columns clearfix">
						<h3>คำนวณยอดผ่อนชำระบ้านต่อเดือน</h3>
						<p>ปรับรายละเอียดข้างล่างเพื่อคำนวณยอดเงินผ่อนต่อเดือน</p>
						<p>เครื่องคำนวณ โดย Harrison</p>
						<form action="" method="post" name="form_cal_loan" id="form_cal_loan" enctype="multipart/form-data" autocomplete="off">
							<div class="row">
							<div class="small-12 large-6 columns">
								<div class="row clearfix">
									<div class="small-12 large-5 columns">
										<label for="left-label" class="f_size1">ราคา</label>
									</div>
									<div class="small-12 large-7 columns">
										<p>
											<?php echo $land_total_price_bath; ?> ฿</p>
									</div>
								</div>
								<div class="row clearfix">
									<div class="small-12 large-5 columns">
										<label for="left-label" class="f_size1">วงเงินกู้</label>
									</div>
									<div class="small-12 large-7 columns">
										<div class="input-group"><span class="input-group-label"> ฿</span>
											<input type="number" name="loan_price" id="loan_price" required class="input-group-field">
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="small-12 large-5 columns">
										<label for="left-label" class="f_size1">ระยะเวลากู้</label>
									</div>
									<div class="small-10 large-5 columns">
										<div class="input-group">
											<input type="text" name="loan_years" id="loan_years" class="input-group-field">
										</div>
									</div>
                  <div class="small-2 large-2 columns">
                    <p>ปี</p>
                  </div>
								</div>
								<div class="row clearfix">
									<div class="small-12 large-5 columns">
										<label for="left-label" class="f_size1">อัตราดอกเบี้ย</label>
									</div>
									<div class="small-10 large-5 columns">
										<div class="input-group">
											<input type="text" name="loan_interest" id="loan_interest" class="input-group-field">
										</div>
									</div>
                  <div class="small-2 large-2 columns">
                  <p>%</p>
                  </div>
								</div>
							</div>
							<div class="small-12 large-6 columns">
								<div class="row">
									<div class="small-12 medium-12 columns">
										<div id="mySmallDoughnut"></div>
										<p id="pay_per_month" style="display: none;">฿ <span></span> ต่องวด</p>
										<!-- <p>฿ 32,850 ชำระดอกเบี้ย</p> -->
									</div>
									<div class="small-12 columns">
										<h3>ประเภทอัตราดอกเบี้ยเงินกู้</h3>
										<table class="stack details">
											<thead>
												<th>ประเภท</th>
												<th>ดอกเบี้ย</th>
											</thead>
											<tbody>
												<tr>
													<td>MLR</td>
													<td><?php echo $site_mrl ?>% ต่อปี</td>
												</tr>
												<tr>
													<td>MOR</td>
													<td><?php echo $site_mor ?>% ต่อปี</td>
												</tr>
												<tr>
													<td>MRR</td>
													<td><?php echo $site_mrr ?>% ต่อปี</td>
												</tr>
												<tr>
													<td>CPR</td>
													<td><?php echo $site_cpr ?>% ต่อปี</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						</form>
						<hr>
						<div class="row">
							<!-- <div class="small-12 medium-6 large-6 columns">
								<p><a href="#" class="button button-red-white text-center"><i class="fa fa-calculator"></i></a> ฉันสามารถกู้เงินได้เท่าไหร่?</p>
							</div> -->
							<div class="small-12 medium-6 columns">
								<button name="submit_calculator" id="submit_calculator" class="button button-red-white expanded"><i class="fa fa-calculator"></i> คำนวณการชำระเงิน</button>
							</div>
						</div>
						<hr>

						<div id="share-facebook">
						<?php
                            if (isset($this->session->mID)) {
                                $share_repeat = $this->Land_model->check_member_share($land_id, $this->session->mID);

                                if (isset($_SESSION['mTID']) && ($_SESSION['mTID'] == '2')) {
                                    //3 = Facebook Login

                            ?>
							<?php if ($share_repeat > 0) {
    ?>
								<div class=" share_link">
									<div class="colomns-align-center text-center">
										<p class="color-white">คุณได้แชร์ลิ้งค์นี้ไปแล้ว สามารถต่ออายุได้ที่หน้าโปรไฟล์ </p>
									</div>
								</div>
								<?php

} else {
    ?>

									<div class=" share_link">
										<div class="colomns-align-center text-center">
											<p class="color-white">ท่านสามารถเปิดหรือนำลิงค์ URL นี้ไปฝากลิงค์หรือแปะที่เว็บไซต์อื่นๆ ที่ท่านได้ลงประกาศได้</p>
											<p class="color-white">เพื่อความสะดวกและง่ายในการโปรโมทอสังหาริมทรัพย์ของท่านให้เป็นที่รู้จักมากขึ้น</p>

											<!-- Load Facebook SDK for JavaScript -->
	                    <input type="hidden" value="<?php echo $land_id; ?>" name="land_id" id="land_id">
	                    <input type="hidden" value="<?php echo $land_title; ?>" name="land_title" id="land_title">
	                    <input type="hidden" value="<?php echo $sfilepath.'/'.$data->pic_thumb; ?>" name="pic_thumb" id="pic_thumb">

	                    <?php if (isset($this->session->mID)) {
	                       $share_url = site_url('land-share/'.$land_id.'/'.$fk_member_id);
	                       $url_member_share = site_url('page/land_member_share_facebook');
	                    ?>
	                    <input type="hidden" value="<?php echo $fk_member_id; ?>" name="member_id" id="member_id">
	                    <input type="hidden" name="url_share" id="url_share" value="<?php echo $share_url; ?>">
	                    <input type="hidden" name="url_member_share" id="url_member_share" value="<?php echo $url_member_share; ?>">
	                   <?php } else {
	                     $share_url = site_url('page/landinside_share/'.$land_id);
	                     $url_member_share = base_url();
	                     ?>
	                     <input type="hidden" value="1" name="member_id" id="member_id">
	                    <input type="hidden" name="url_share" id="url_share" value="<?php echo $share_url; ?>">
	                    <input type="hidden" name="url_member_share" id="url_member_share" value="<?php echo $url_member_share; ?>">
	                   <?php }// END IF ?>

	                    <!-- Your share button code -->
	                    <div class="jssocials-shares">
	                      <div class="jssocials-share jssocials-share-facebook">
	                        <a href="#" class="fb-share-button jssocials-share-link" id="fb-share-button" data-layout="button_count" data-href="<?php echo $share_url; ?>">
	                          <i class="fa fa-facebook jssocials-share-logo"></i> แชร์กดตรงนี้
	                        </a>
	                      </div>
	                    </div>
	                    <!-- ้ -->
	                    <!--   -->

										</div>

									</div>
									<?php

}//END IF  ?>
										<?php

                                } else { //END IF  ?>
											<div class=" share_link">
												<div class="colomns-align-center text-center">
													<p class="color-white">ท่านสามารถเปิดหรือนำลิงค์ URL นี้ไปฝากลิงค์หรือแปะที่เว็บไซต์อื่นๆ ที่ท่านได้ลงประกาศได้</p>
													<p class="color-white">เพื่อความสะดวกและง่ายในการโปรโมทอสังหาริมทรัพย์ของท่านให้เป็นที่รู้จักมากขึ้น</p>
													<p><a class="color-red" href="<?php echo site_url('page/m_login');
                                    ?>">กรุณา สมัครสมาชิก Condo Compare หรือ HOB เพื่อทำการแชร์</a></p>
												</div>
											</div>
											<?php

                                }//END IF  ?>

												<?php

                            } else {
                                ?>
													<?php
                                    if (isset($_SESSION['mTID']) && ($_SESSION['mTID'] == '2')) {
                                        //3 = Facebook Login

                                ?>
														<div class=" share_link">
															<div class="colomns-align-center text-center">
																<p class="color-white">ท่านสามารถเปิดหรือนำลิงค์ URL นี้ไปฝากลิงค์หรือแปะที่เว็บไซต์อื่นๆ ที่ท่านได้ลงประกาศได้</p>
																<p class="color-white">เพื่อความสะดวกและง่ายในการโปรโมทอสังหาริมทรัพย์ของท่านให้เป็นที่รู้จักมากขึ้น</p>
																<p><a class="color-red" href="<?php echo site_url('page/m_login');
                                        ?>">กรุณา Login เข้าสู่ระบบ เพื่อทำการแชร์</a></p>
															</div>
														</div>
														<?php

                                    } else { //END IF  ?>
															<div class=" share_link">
																<div class="colomns-align-center text-center">
																	<p class="color-white">ท่านสามารถเปิดหรือนำลิงค์ URL นี้ไปฝากลิงค์หรือแปะที่เว็บไซต์อื่นๆ ที่ท่านได้ลงประกาศได้</p>
																	<p class="color-white">เพื่อความสะดวกและง่ายในการโปรโมทอสังหาริมทรัพย์ของท่านให้เป็นที่รู้จักมากขึ้น</p>
																	<p><a class="color-red" href="<?php echo site_url('page/m_login');
                                        ?>">กรุณา สมัครสมาชิก Condo Compare หรือ HOB เพื่อทำการแชร์</a></p>
																</div>
															</div>
															<?php

                                    }//END IF  ?>
																<?php

                            }//END IF  ?>
																	<div class="clearfix"></div>
														</div>
					</div>
				</div>
			</div>
			<!--RIGHT-->
			<div class="small-12 large-3 columns">
				<div id="share"></div>
				<div class="clearfix row mar-top-medium">
					<?php
                        if (isset($_SESSION['mTID'])) {
                            ?>
						<div class="small-12 large-7 columns"><a href="#" onclick="javascript:(0);" class="black hob-wishlist" id="<?php echo $land_id;
                            ?>"><i class="fa fa-heart sidebar"></i>	เก็บไว้ดูภายหลัง</a></div>
						<?php

                        // } else {
                            ?>
							<!-- <div class="small-12 large-6 columns">เข้าสู่ระบบเพื่อทำการเปรียบเทียบ และจัดลำดับรายการโปรด</div> -->
							<?php

                        } //END IF ?>
						<input type="hidden" name="url_facebook_share" id="url_facebook_share" value="<?php echo current_url(); ?>">
						 <div class="small-12 large-5 end columns">
							 <a href="#" id="fb-share" class="black" data-layout="button_count"
							 data-href="<?php echo current_url(); ?>"><i class="fa fa-share-alt sidebar"></i>	แชร์</a>
						 </div>
								<!--<div><a href="#" class="black"><i class="fa fa-file sidebar"></i>	พิมพ์</a></div>-->
				</div>

				<form action="" method="post" role="form" name="form_contact_sidebar" id="form_contact_sidebar">
					<input type="hidden" name="staff_id" id="staff_id" value="<?php echo $staff_id; ?>">


					<div class="bg-gray mar-top-medium clearfix">

						<?php //if (isset($show_sale)) { ?>
							<!-- <div class="row align-middle">
								<div class="small-12 medium-5 column">
									<a href="#"><img src="http://placehold.it/100x100&amp;text=Who?" alt=""></a>
								</div>
								<div class="small-12 medium-7 column align-self-middle">
									<p>
										<?php echo $staff_fullname; ?><?php echo $staff_email;?>
									</p>
								</div>
							</div>
							<div class="column align-middle title-blue-bg">
								<div class="small-4 column"><i class="fa fa-phone sidebar"></i></div>
								<div class="small-8 column">
									<p class="color-white">
										<?php //echo $staff_phone; ?>
									</p>
								</div>
							</div> -->
							<?php //} else { ?>
								<!-- <div class="column align-middle title-blue-bg">
									<div class="small-12 column">
										<p class="color-white">กรอกฟอร์มเพื่อรับข้อมูลการติดต่อตัวแทน</p>
									</div>
								</div> -->
								<?php //}//END IF SHOW ?>
									<div class="row bg-gray">
										<!-- <form action="" method="post"> -->
											<div class="mar-top-small mar-bott-small">
												<!-- <div class="small-12 columns">
													<label>
														<input type="text" name="land_share_form_name" id="land_share_form_name" placeholder="ชื่อ-นามสกุล" required>
													</label>
												</div>
												<div class="small-12 columns">
													<label>
														<input type="text" name="land_share_form_phone" id="land_share_form_phone" placeholder="เบอร์โทร" required>
													</label>
												</div>
												<div class="small-12 columns">
													<label>
														<input type="email" name="land_share_form_email" id="land_share_form_email" required placeholder="อีเมล์">
													</label>
												</div>
												<div class="small-12 columns">
													<label>
														<textarea id="land_share_form_message" name="land_share_form_message" cols="10" rows="3"></textarea>
													</label>
												</div> -->
                        <div class="small-12 columns" style="margin-bottom: 10px;">
                          <a href="#share-facebook" class="button bg-cs-blue expanded">Share HOB</a>
                        </div>

												<div class="small-12 columns">
													<!-- <button name="submit_contact" value="action" class="button button-red-white expanded">ติดต่อตัวแทน</button> -->
													<a href="#contact-bottom" class="button button-red-white expanded">ติดต่อตัวแทน</a>
													<!-- <input type="hidden" name="fk_land_id" value="<?php echo $fk_land_id; ?>"> -->
													<!-- <input type="hidden" name="fk_staff_id" value="<?php echo $staff_ids; ?>"> -->
													<!-- <input type="hidden" name="fk_member_id" value="<?php echo $fk_member_id; ?>"> -->
												</div>
											</div>
										<!-- </form> -->
									</div>
					</div>
				</form>

				<div class="column well clearfix mar-top-medium">
					<h3>สถานที่ใกล้เคียง</h3>

					<?php
                    if (!empty($transportation)) {
                        foreach ($transportation as $val_transportation) {
                            $transportation_id = (!empty($val_transportation->transportation_id)) ? $val_transportation->transportation_id : '-';
                            if ($transportation_id !== '0') {
                                $transportation_title = (!empty($val_transportation->transportation_title)) ? $val_transportation->transportation_title : '-';
                                $transportation_category_title = (!empty($val_transportation->transportation_category_title)) ? $val_transportation->transportation_category_title : '-';
                                $transportation_distance = (!empty($val_transportation->transportation_distance)) ? $val_transportation->transportation_distance : '-';
                                ?>
					<div class="row clearfix mar-top-medium">
						<div class="small-12 columns">
							<p class="color-red"><?php echo $transportation_category_title.' : '.$transportation_title;
                                ?></p><i class="fa fa-male"></i> <?php echo $transportation_distance;
                                ?>
						</div>
					</div>
					<?php

                            }//END IF
                        }//END FOREACH
                    ?>
					<?php

                    } else {
                        ?>
					<div class="row clearfix mar-top-medium">
						<div class="small-12 columns">
							<p class="color-red">ไม่มีข้อมูล</p><i class="fa fa-male"></i> -
						</div>
					</div>
					<?php

                    }//END IF
                    ?>


				</div>
				<div class="row clearfix">
					<div class="columns text-center mar-top-large">
            <div id="" class="banner-carousel owl-carousel owl-theme">
						<!--  ID : 10 = banner หน้ารายละเอียดที่ดิน  -->
						<?php
              $banner_10 = $this->Page_model->get_banner_location(10);
              $sfilepath_banner = base_url().'uploads/banner/';
              $banner_count = count($banner_10);

              // $banner_10_arr = array();
              for ($i = 0; $i < $banner_count;  ++$i) {
                  $banner_10_id = (!empty($banner_10[$i]->banner_id)) ? $banner_10[$i]->banner_id : '';

                  //UPDATE DISPLAY
                  $displayed = (!empty($banner_10[$i]->displayed)) ? $banner_10[$i]->displayed : '';
                  $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_10_id, 'banner');

                  $banner_10_title = (!empty($banner_10[$i]->banner_title)) ? $banner_10[$i]->banner_title : '';
                  $banner_10_url = (!empty($banner_10[$i]->banner_url)) ? $banner_10[$i]->banner_url : '#';
                  $banner_10_pic_thumb = (!empty($banner_10[$i]->pic_thumb)) ? $sfilepath_banner.$banner_10[$i]->pic_thumb : 'http://placehold.it/370x540/333?text=370x540';
                  // $banner_10_arr[] = '<a href="'.$banner_10_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_10_id.'"><img src="'.$banner_10_pic_thumb.'" alt="'.$banner_10_title.'" title=""></a>';
                echo $banner_10_arr = '<div class="item"><a href="'.$banner_10_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_10_id.'"><img src="'.$banner_10_pic_thumb.'" alt="'.$banner_10_title.'" title=""></a></div>';
              }
              // shuffle($banner_10_arr);
              // echo (!empty($banner_10_arr[0])) ? $banner_10_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/370x540/333?text=370x540" alt="370x540" title="370x540"></a>';
          ?>
					</div>
					</div>
				</div>
			</div>
			<!--CONTACTS-->
			<div id="contact-bottom" class="columns mar-top-medium clearfix">
				<div class="google-maps">
					<?php //echo $land_googlemaps; ?>
				</div>
				<form action="" method="post" role="form" name="form_contact_bottom" id="form_contact_bottom" class="form-contact">
					<input type="hidden" name="staff_id" id="staff_id" value="<?php echo $staff_id; ?>">
					<div class="row">
						<h3>ติดต่อตัวแทนเกี่ยวกับทรัพย์นี้</h3>
						<div class="small-12 large-6 columns">
							<div class="row clearfix">
								<div class="small-12 large-5 columns">
									<label for="left-label" class="f_size1">ชื่อ <span class="color-red">*</span></label>
								</div>
								<div class="small-12 large-7 columns">
									<div class="input-group">
										<input type="text" name="land_share_form_name" id="land_share_form_name" class="input-group-field" required>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="small-12 large-5 columns">
									<label for="left-label" class="f_size1">เบอร์โทร <span class="color-red">*</span></label>
								</div>
								<div class="small-12 large-7 columns">
									<div class="input-group">
										<input type="text" name="land_share_form_phone" id="land_share_form_phone" class="input-group-field" required>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="small-12 large-5 columns">
									<label for="left-label" class="f_size1">อีเมล์ <span class="color-red">*</span></label>
								</div>
								<div class="small-12 large-7 columns">
									<div class="input-group">
										<input type="email" name="land_share_form_email" id="land_share_form_email" class="input-group-field" required>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="small-12 large-5 columns">
									<label for="left-label" class="f_size1">ข้อความ <span class="color-red">*</span></label>
								</div>
								<div class="small-12 large-7 columns">
									<div class="input-group">
										<textarea id="land_share_form_message" name="land_share_form_message" cols="10" rows="3"></textarea>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="small-12 large-5 hide hide-for-small columns">&nbsp;</div>
								<div class="small-12 large-7 columns">
									<div class="input-group">
										<input id="enewsletter" type="checkbox" name="enewsletter" value="1">
										<label for="enewsletter">รับจดหมายข่าวสารอสังหาริมทรัพย์ จากเรา</label>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="small-12 large-5 hide hide-for-small columns">&nbsp;</div>
								<div class="small-12 large-7 columns">
									<div class="input-group">
										<button name="submit_contact" value="action" class="button button-red-white expanded">ติดต่อตัวแทน</button>
										<input type="hidden" name="fk_land_id" value="<?php echo $fk_land_id; ?>">
										<input type="hidden" name="fk_staff_id" value="<?php echo $staff_ids; ?>">
										<input type="hidden" name="fk_member_id" value="<?php echo $fk_member_id; ?>">
									</div>
								</div>
							</div>
						</div>
						<div class="small-12 large-6 columns align-middle">
							<?php
                                if (isset($show_sale)) {
                                    ?>
								<div class="row align-middle">
									<div class="small-4 columns">
										<a href="#"><img src="http://placehold.it/200x100&amp;text=Who?" alt=""></a>
									</div>
									<div class="small-8 column align-self-middle">
										<p class="text-center">
											<?php echo $staff_fullname;
                                    ?>
										</p>
										<p class="text-center">
											<?php echo $staff_email;
                                    ?>
										</p>
									</div>
								</div>
								<div class="column align-middle title-blue-bg">
									<div class="small-4 columns"><i class="fa fa-phone sidebar"></i></div>
									<div class="small-8 column">
										<p class="color-white">
											<?php echo $staff_phone;
                                    ?>
										</p>
									</div>
								</div>
								<?php

                                } else {
                                    ?>
									<div class="row align-middle">
										<div class="small-12 column align-self-middle">
											<p class="text-center">กรอกฟอร์มเพื่อรับข้อมูลการติดต่อตัวแทน</p>
										</div>
									</div>
									<div class="column align-middle title-blue-bg">
										<div class="small-12 column">
											<p class="color-white">กรอกฟอร์มเพื่อรับข้อมูลการติดต่อตัวแทน</p>
										</div>
									</div>
									<?php

                                }//END IF SHOW
                                ?>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php $this->load->view('partials/footer');?>

  <!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
  <script>
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '176921789401784',
        xfbml      : true,
        version    : 'v2.7'
      });
      FB.AppEvents.logPageView();
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
  </script>
<script>

$('#test-share').click(function(){
	var land_id = $('#land_id').val();
	var land_title = $('#land_title').val();
	var pic_thumb = $('#pic_thumb').val();
	var url_share = $('#url_share').val();
	var url_member_share = $('#url_member_share').val();
	var member_id = $('#member_id').val();
	FB.ui({
		 app_id: '176921789401784',
		 redirect_uri: url_member_share,
		 display: 'popup',
		 method: 'share',
		 href: url_share,
		 hashtag: '#condocompare, #แชร์แล้วรวย',
		 quote: 'CONDO-COMPARE.COM แชร์แล้วรวย...',
		 mobile_iframe: true,
	 }, function(response){

	 });

});

$('#fb-share').click(function(){
  var land_id = $('#land_id').val();
  var land_title = $('#land_title').val();
  var pic_thumb = $('#pic_thumb').val();
  var url_facebook_share = $('#url_facebook_share').val();
  FB.ui({
    method: 'feed',
    display: 'popup',
    link : url_facebook_share,
    picture : pic_thumb,
    name   : land_title,
    caption: 'WWW.CONDO-COMPARE.COM', //งงเด้ งงเด้ บางอันได้บางอันกูก็งงเด้
   //  mobile_iframe: true,
   //  href: url_share,
  }, function(response){

  });

});

 $('#fb-share-button').click(function(){
   var land_id = $('#land_id').val();
   var land_title = $('#land_title').val();
   var pic_thumb = $('#pic_thumb').val();
   var url_share = $('#url_share').val();
   var url_member_share = $('#url_member_share').val();
   var member_id = $('#member_id').val();

  //  FB.ui({
  //    method: 'feed',
  //    display: 'popup',
  //    link : url_share,
  //    picture : pic_thumb,
  //    name   : land_title,
  //    caption: 'WWW.CONDO-COMPARE.COM', //งงเด้ งงเด้ บางอันได้บางอันกูก็งงเด้
  //   //  mobile_iframe: true,
  //   //  href: url_share,


	//  hashtag: '#condocompare, #แชร์แล้วรวย',
	FB.ui({
		method: 'share',
		display: 'popup',
		app_id		: '176921789401784',
		redirect_uri: url_member_share,
		href		: url_share,
		quote		: land_title,
		picture : pic_thumb,
		title   : land_title,
		name   	: land_title,
		caption	: 'WWW.CONDO-COMPARE.COM',
		mobile_iframe: true,
  }, function(response){
     if( response && !response.error_message ){
      //  console.log(response.post_id);
       $.ajax({
          url: url_member_share,
					type: "POST",
					data: {
						id : land_id,
						member_id : member_id
					},
          success: function (data) {
						console.log(data);
            if( data.success === true ){
              $('#message').html(data.message);
              $('#panel-message').show();
							$('.fa').addClass('fa-check-circle');
		          setInterval(function(){
		            window.location.reload();
		          }, 3000);
              return true;
            }else{
              $('#message').html(data.message);
              $('#panel-message').show();
              $('.fa').addClass('fa-exclamation-triangle');
		          setInterval(function(){
		            window.location.reload();
		          }, 3000);
              return false;
            }
          },
          error: function () {
						$('#message').html('เกิดข้อผิดพลาดในการแชร์จากหัวข้อนี้...');
						$('.fa').addClass('fa-exclamation-triangle');
						$('#panel-message').show();
          }
       });

     }else{

				 $('#message').html('เกิดข้อผิดพลาดในการแชร์...');
				 $('.fa').addClass('fa-exclamation-triangle');
				 $('#panel-message').show();

     }
   });

 });


</script>

		<script src="<?php echo base_url()?>assets/js/doughnutit.js"></script>
		<script>
			$("#form_contact_bottom, #form_contact_sidebar").submit(function (event) {
				event.preventDefault();
				//var data = $('#form_register').serialize();
				var data = new FormData($(this)[0]);

				var url = '<?php echo site_url('page/m_land_email_success'); ?>';
				$.ajax({
					method: 'POST',
					url: url,
					data: data,
					cache: false,
					contentType: false,
					processData: false,
					success: function (data) {
						// console.log(data);
						if (data == 'email_error') //Password Not Match!
						{
							swal({
								title: "เกิดข้อผิดพลาด!",
								text: "กรุณาติดต่อเจ้าหน้าที่หรือตัวแทนขายอสังหสริมทรัพย์",
								type: "warning",
							});
						} else if (data == 'success') {
							swal({
									title: "สำเร็จ",
									text: "ระบบได้ทำการส่งอีเมล์ การติดต่อของท่านเรียบร้อยแล้ว..",
									type: "info",
								},
								function (isConfirm) {
									window.location.replace('<?php echo base_url(uri_string()); ?>/?show=true');
								});
						}

					},
					error: function () {
						console.log("failure");
					}

				});
				return false;
			}); //END FORM SUBMIT


		</script>
		<script>
			$(document).ready(function () {

				$(".hob-wishlist").click(function (event) {
					var land_id = $(this).attr("id");
					$.ajax({
						url: "<?php echo base_url(); ?>page/wishlist_land",
						type: "POST",
						data: {
							land_id: land_id,
						},
						//async: false,
						success: function (data, status) {
							console.log(data);
							if (data == 'insert') {
								$("#" + land_id + ".hob-wishlist").children().css('color', 'green');
							} else if (data == 'delete') {
								$("#" + land_id + ".hob-wishlist").children().css('color', '');
							}

						},
						error: function (xhr, desc, err) {
							console.log(err);
						},
					});
					event.preventDefault();
					return false;
				});

				$(".hob-compare").click(function (event) {
					//alert('ssss');
					var land_id = $(this).attr("id");
					$.ajax({
						url: "<?php echo base_url(); ?>page/compare_land",
						type: "POST",
						data: {
							land_id: $(this).attr("id"),
						},
						success: function (data, status) {
							//console.log(data);
							if (data == 'insert') {
								$("#" + land_id + ".hob-compare").children().css('color', 'green');
							} else if (data == 'delete') {
								$("#" + land_id + ".hob-compare").children().css('color', '');
							}
						},
						error: function (xhr, desc, err) {
							console.log(err);
						},
					});
					event.preventDefault();
					return false;
				});


				$("#submit_calculator").click(function (event) {

          var data_loan = $( "form#form_cal_loan" ).serialize();

          var loan_price = $('#loan_price').val();
          var loan_years = $('#loan_years').val();
          var loan_interest = $('#loan_interest').val();

          if( loan_price === '' || loan_price === '0' )
          {
            alert('กรณีระบุข้อมูลการคำนวณให้ครบถ้วน');
            return false;
          }else if( loan_years === '' || loan_years === '0' ){
            alert('กรณีระบุข้อมูลการคำนวณให้ครบถ้วน');
            return false;
          }else if( loan_interest === ''  || loan_interest === '0'  ){
            alert('กรณีระบุข้อมูลการคำนวณให้ครบถ้วน');
            return false;
          }else {
            $.ajax({
  						url: "<?php echo base_url(); ?>page/calculate_condo_loan",
  						type: "POST",
  						data: data_loan,
  						success: function (data, status) {
  							// console.log(data);
                $("#pay_per_month").css('display', 'block');
                $("#pay_per_month").html("฿ "+ data +" ต่องวด");
  						},
  						error: function (xhr, desc, err) {
  							console.log(err);
  						},
  					});
  					event.preventDefault();
  					return true;
          }

				});

			});
		</script>
