<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>''));
?>

	<div id="container">
		<div class="bg-cs-gray-second">
			<div class="row align-middle">
				<div class="columns">
					<nav aria-label="You are here:" role="navigation">
						<?php echo $breadcrumbs; ?>
					</nav>
				</div>
			</div>
		</div>
		<div class="bg-cs-gray clearfix">
			<h3 class="text-center color-blue"><?php echo $title; ?></h3>
		</div>
		<div class="row content-inside">
			<!-- -------------- -->
			<!-- LEFT : SIDEBAR -->
			<!-- -------------- -->

			<div class="small-12 medium-4 large-3 columns">
				<?php $this->load->view('partials/acc-sidebar');?>
			</div>

			<!-- RIGHT : CONTENT -->
			<form action="">
			<div class="small-12 medium-8 large-9 columns">
				<div class="small-12 medium-12 columns">
					<table class="hover">
						<thead>
							<tr>
								<th width="100">รูป</th>
								<th width="200">ชื่อโครงการ</th>
								<th width="250">วันที่</th>
								<?php
								if( isset($page) )
								{ //IF #1
								?>
								<?php
									if( $page=='wishlist' )
									{ //IF #2
								?>
								<th width="100">จัดการ</th>
								<?php
									} //END IF#2 wishlist
								?>
								<?php
									if( $page=='share_condo' )
									{ //IF #2
								?>
								<th width="100">ต่ออายุลิ้งค์</th>
								<?php
									} //END IF#2 wishlist
								?>
								<?php
								} //END IF#1
								?>
							</tr>
						</thead>
						<tbody>
							<?php
								foreach($condo_list as $row => $value)
								{//START FOREACH#1
									$sfilepath = base_url().'uploads/condominium';
									$pic_thumb = $sfilepath.'/'.$value->pic_thumb;
									//echo $value->type_status_id;
							?>
							<input type="hidden" name="condo_id[]" value="<?php echo $value->condo_id; ?>">
							<tr>
								<td><a href="<?php echo site_url('condominium-details/'.$value->condo_id); ?>"><img src="<?php echo $pic_thumb;?>" style="width: 150px;" class="img-thumbnail" alt=""></a></td>
								<td><a href="<?php echo site_url('condominium-details/'.$value->condo_id); ?>"><?php echo $value->condo_title; ?></a></td>
								<td>
									<?php

									if( $page=='share_condo' )
									{
										$sharedate = date($value->sharedate);
										$date_share_expire = date("Y-m-d hh:mm:ii", strtotime( date( $sharedate, strtotime( $sharedate ) ) . "+1 month" ) );
									?>
									วันที่แชร์ : <?php echo $this->dateclass->DateTimeShortFormat($value->sharedate, 0, 1, "Th"); ?><br>
									หมดอายุ : <?php echo $this->dateclass->DateTimeShortFormat($date_share_expire, 0, 1, "Th"); ?>
									<?php
									}
									else
									{
									?>
									วันที่เลือกเป็นรายการโปรด : <?php echo $this->dateclass->DateTimeShortFormat($value->createdate, 0, 1, "Th"); ?><br>
									<?php
									}
									?>
								</td>
								<?php
									if( isset($page) )
									{ //IF #1
								?>
								<?php
										if( $page=='wishlist' )
										{ //IF #2
								?>
								<td><a href="#" class="hob-delete" id="<?php echo $value->wishlist_condo_id; ?>"><i class="fa fa-trash-o"></i></a></td>
								<?php
										} //END IF#2 WISHLIST
								?>
									<?php
										if( $page=='share_condo' )
										{ //IF #2
								?>
								<td><a href="#" class="hob-renew-share" id="<?php echo $value->member_share_condo_id; ?>"><i class="fa fa-clock-o"></i></a></td>
								<?php
										} //END IF#2 WISHLIST
								?>
								<?php
									} //END IF#1
								?>
							</tr>
							<?php }//END FOREACH#1 ?>
						</tbody>
					</table>
				</div>
				<!--RIGHT-->
			</div>
			</form>

			<!-- ------------------- -->
			<!-- END RIGHT : CONTENT -->
			<!-- ------------------- -->



		</div>
	</div>


	<?php $this->load->view('partials/footer', array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>''));?>


<script>
$(document).ready(function () {

	$(".hob-delete").click(function (event) {
		var select = $(this);
		var id = select.attr('id');

		$.ajax({
			<?php
			if( isset($page) )
			{ //IF #1
			?>
			<?php
			 if( $page=='wishlist' )
			 { //IF #2
			?>
			url: "<?php echo base_url(); ?>page/m_delete_wishlist_condo",
			<?php
			 } //END IF#2
			?>
			<?php
			} //END IF#1
			?>
			type: "POST",
			data: {
				id : id,
			},
			async: false,
			success: function (data, status)
			{
				//console.log(data);
					window.location.reload();

			},
			error: function (xhr, desc, err)
			{
				console.log( err );
			},
		});
		event.preventDefault();
		return false;
	});

	$(".hob-renew-share").click( function (event) {

		var select = $(this);
		var id = select.attr('id');
		console.log(id);
		var url = '<?php echo base_url(); ?>page/m_share_condo_renew_date';
		$.ajax({
			method: 'POST',
			url: "<?php echo base_url(); ?>page/m_share_condo_renew_date",
			data: {
				id : id,
			},
			async: false,
			success: function (data) {
				console.log(data);
				if ( data == 'success' )
				{
					swal({
						title: 	"สำเร็จ",
						text: 	"ท่านได้ทำการต่ออายุประกาศเรียบร้อยแล้วค่ะ...",
						type: 	"info",
					},
						function(isConfirm){
							window.location.reload();
					});
				}

			},
			error: function () {
				console.log("failure");
			}

		});
		event.preventDefault();
		return false;
	});
	/* END CLICK */
});
</script>
