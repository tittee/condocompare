<?php
defined('BASEPATH') or exit();
/*HEADER*/
$this->load->view('partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'BodyClass' => ''));

?>
<style>



</style>
	<div id="container">
		<div class="bg-cs-gray-second">
			<div class="row align-middle">
				<div class="columns">
					<nav aria-label="You are here:" role="navigation">
						<?php echo $breadcrumbs; ?>
					</nav>
				</div>
			</div>
		</div>
		<div class="bg-cs-gray clearfix">
			<h3 class="text-center color-blue page-title"><?php echo $title; ?></h3>
		</div>
		<div class="row content-inside">
			<!-- -------------- -->
			<!-- LEFT : SIDEBAR -->
			<!-- -------------- -->

			<div class="small-12 medium-4 large-3 columns">
				<?php $this->load->view('partials/acc-sidebar');?>
			</div>

			<!-- RIGHT : CONTENT -->
			<div class="small-12 medium-8 large-9 columns">
				<div class="small-12 medium-12 columns">
						<form action="" method="post" id="form_password" enctype="multipart/form-data" data-abide novalidate>
							<div class="well">

								<div id="foo"></div>
								<div id="bar"></div>
								<div data-abide-error class="alert callout" style="display: none;">
							    <p><i class="fi-alert"></i> กรุณากรอกข้อมุลให้ครบถ้วน..</p>
							  </div>

								<div id="success" class="success callout" data-closable="slide-out-right" style="display: none;">
								  <p id="message" style="margin-bottom: 0;"></p>
								  <button class="close-button" aria-label="" type="button" data-close>
								    <span aria-hidden="true">&times;</span>
								  </button>
								</div>

								<div class="row ">
									<div class="medium-6 columns">
										<label for="email">รหัสผ่านเดิม</label>
										<div class="input-group">
											<input type="password" name="password" id="password" placeholder="Password" required aria-describedby="help_password">
											<span class="form-error" id="help_password">
												กรุณาระบุ รหัสผ่านเดิม...
											</span>
										</div>
									</div>
								</div>

								<div class="row ">
									<div class="medium-6 columns">
										<label for="email">รหัสผ่านใหม่</label>
										<div class="input-group">
											<input type="password" name="new_password" id="new_password" placeholder="Password" aria-describedby="help_new_password" required>
											<span class="form-error" id="help_new_password">
												กรุณาระบุ รหัสผ่านใหม่...
											</span>
										</div>
									</div>

									<div class="medium-6 columns end">
										<label for="email">ยืนยันรหัสผ่านใหม่</label>
										<div class="input-group">
											<input type="password" name="cf_new_password" id="cf_new_password" placeholder="Password" aria-describedby="help_cf_new_password" data-equalto="new_password" required>
											<span class="form-error" id="help_cf_new_password">
												กรุณาระบุ รหัสผ่านใหม่ตรงกัน...
											</span>
										</div>
									</div>
									<div class="medium-12 columns">
										<span id="result"></span>
									</div>
								</div>

								<div class="row">
									<div class="small-12 medium-4 align-center columns">
										<div class="input-group">
											<button type="submit" name="submit" id="btn-password" class="button button-red-white expanded">แก้ไขรหัสผ่าน</button>
											<input type="hidden" name="member_id" value="<?php echo $this->session->mID; ?>">
											<input type="hidden" name="action" value="change-password">
										</div>
									</div>
								</div>
							</div>
						</form>

				</div>
			</div>
			<!--RIGHT-->

			<!-- ------------------- -->
			<!-- END RIGHT : CONTENT -->
			<!-- ------------------- -->
		</div>
	</div>
	<?php $this->load->view('partials/footer'); ?>
	<script>
	$(document).ready(function(){
		$("#new_password").keyup(validate);
		$("#cf_new_password").keyup(validate);
	})

	function validate() {
		var pwd1 = $('#new_password').val();
		var pwd2 = $('#cf_new_password').val();
    if(pwd1 === pwd2) {
       $("#result").text("รหัสผ่านตรงกัน");
         return true;
    }
    else {
        $("#result").text("รหัสผ่านไม่ตรงกัน");
        return false;
    }
	}

	$(document)
		// field element is invalid
		.on("invalid.zf.abide", function(ev,elem) {
			console.log("Field id "+ev.target.id+" is invalid");
		})
		// field element is valid
		.on("valid.zf.abide", function(ev,elem) {
			console.log("Field name "+elem.attr('name')+" is valid");
		})
		// form validation failed
		.on("forminvalid.zf.abide", function(ev,frm) {
			console.log("Form id "+ev.target.id+" is invalid");
		})
		// form validation passed, form will submit if submit event not returned false
		.on("formvalid.zf.abide", function(ev,frm) {
			console.log("Form id "+frm.attr('id')+" is valid");
			// ajax post form
		})
		// to prevent form from submitting upon successful validation
		.on("submit", function(ev) {
			ev.preventDefault();
			console.log("Submit for form id "+ev.target.id+" intercepted");

			$.isLoading({
				 'text'  : "Loading....",
				 'tpl'   : '<span class="isloading-wrapper %wrapper%">%text%<i class="fa fa-spinner glyphicon-refresh-animate"></i></span>',
			 });

			var formData = new FormData($('form')[0]);
 			var url = '<?php echo site_url('change-password'); ?>';
			$.ajax({
				url: url,
				type: "POST",
				data: formData,
				async: false,
				cache: false,
				contentType: false,
				processData: false,
				success: function (data, status) {
					// console.log(data);

					var type = ( data.success == true ? "success" : "warning");
					$('#message').html(data.message);
					$('#success').fadeIn('fast');
					$('html, body').animate({
							scrollTop: $("#success").offset().top
					}, 2000);


					// Re-enabling event
						setTimeout( function(){
								$.isLoading( "hide" );
								window.location.reload();
						}, 5000 );

				},
				error: function (xhr, desc, err) {
					console.log(err);
				},
			});
		});
		</script>
