<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>''));
?>

	<div id="container">
		<div class="bg-cs-gray-second">
			<div class="row align-middle">
				<div class="columns">
					<nav aria-label="You are here:" role="navigation">
						<ul class="breadcrumbs">
							<li><a href="<?php echo site_url('home'); ?>">หน้าแรก</a></li>
							<li><span class="show-for-sr">Current:</span>
								<?php echo $_SESSION['mUSER']; ?>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
		<div class="bg-cs-gray clearfix">
			<h3 class="text-center color-blue page-title"><?php echo $title; ?></h3>
		</div>
		<div class="row content-inside">
			<!-- -------------- -->
			<!-- LEFT : SIDEBAR -->
			<!-- -------------- -->

			<div class="small-12 medium-4 large-3 columns">
				<?php $this->load->view('partials/acc-sidebar');?>
			</div>

			<!-- RIGHT : CONTENT -->
			<form action="">
			<div class="small-12 medium-8 large-9 columns">
				<div class="small-12 medium-12 columns">
					<table class="hover">
						<thead>
							<tr>
								<th width="100">ลำดับ</th>
								<th width="200">ชื่อผู้ติดต่อ</th>
								<!--<th width="100">เบอร์</th>-->
								<!--<th width="150">อีเมล์</th>-->
								<!-- <th width="300">ข้อความ</th> -->
								<th width="150">สถานะ</th>
								<!-- <th width="100">จัดการ</th> -->
							</tr>
						</thead>
						<tbody>
							<?php
							$i = 1;
								foreach($contact_list as $row => $value)
								{//START FOREACH#1
							?>
							<input type="hidden" name="condo_share_form_id[]" value="<?php echo $value->condo_share_form_id; ?>">
							<tr>
								<td><?php echo $i; ?></td>
								<td><?php echo $value->condo_share_form_name; ?></td>
								<!--<td><?php //echo $value->condo_share_form_phone; ?></td>-->
								<!--<td><?php //echo $value->condo_share_form_email; ?></td>-->
								<!-- <td><?php echo $value->condo_share_form_message; ?></td> -->
<!--								<td><?php //echo $this->primaryclass->get_condo_share_form_status($value->condo_share_form_status); ?><br></td>-->
								<td><?php echo $this->Configsystem_model->get_type_status_title($value->fk_type_status_id); ?></td>
								<!-- <td><a href="#" class="btn-contact" id="<?php echo $value->condo_share_form_id; ?>"><i class="fa fa-check"></i></a> | <a href="#" class="btn-uncontact" id="<?php echo $value->condo_share_form_id; ?>"><i class="fa fa-remove"></i></a></td> -->
							</tr>
							<?php $i++; }//END FOREACH#1 ?>
						</tbody>
					</table>
				</div>
				<!--RIGHT-->
			</div>
			</form>

			<!-- ------------------- -->
			<!-- END RIGHT : CONTENT -->
			<!-- ------------------- -->



		</div>
	</div>


	<?php $this->load->view('partials/footer', array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>''));?>


<script>
$(document).ready(function () {
	$(".btn-contact").click( function (event) {

		var select = $(this);
		var id = select.attr('id');
		var status = 1;
		$.ajax({
			method: 'POST',
			url: "<?php echo base_url(); ?>page/m_contact_list_update_status",
			data: {
				id : id,
				status : status,
			},
			async: false,
			success: function (data) {
				console.log(data);
				if ( data == 'success' )
				{
					swal({
						title: 	"สำเร็จ",
						text: 	"ท่านได้ทำการแก้ไขสถานะการติดต่อไปยังผู้ติดต่อเรียบร้อยแล้วค่ะ",
						type: 	"info",
					},
						function(isConfirm){
							window.location.reload();
					});
				}

			},
			error: function () {
				console.log("failure");
			}

		});
		event.preventDefault();
		return false;
	});
	/* END CLICK */

	$(".btn-uncontact").click( function (event) {

		var select = $(this);
		var id = select.attr('id');
		var status = 0;
		$.ajax({
			method: 'POST',
			url: "<?php echo base_url(); ?>page/m_contact_list_update_status",
			data: {
				id : id,
				status : status,
			},
			async: false,
			success: function (data) {
				console.log(data);
				if ( data == 'success' )
				{
					swal({
						title: 	"สำเร็จ",
						text: 	"ท่านได้ทำการยกเลิกสถานะการติดต่อไปยังผู้ติดต่อเรียบร้อยแล้วค่ะ",
						type: 	"info",
					},
						function(isConfirm){
							window.location.reload();
					});
				}

			},
			error: function () {
				console.log("failure");
			}

		});
		event.preventDefault();
		return false;
	});
	/* END CLICK */
});
</script>
