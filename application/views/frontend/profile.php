<?php
defined('BASEPATH') or exit();
/*HEADER*/
$this->load->view('partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'BodyClass' => ''));
$sfilepath = 'uploads/member/';
if (isset($data)) {
    $member_id = $data->member_id;
    $member_fname = $data->member_fname;
    $member_lname = $data->member_lname;
    $member_type_id = $data->member_type_id;
    $member_email = $data->member_email;
    $member_mobileno = $data->member_mobileno;
    $member_profile = $data->member_profile;
    $member_address = trim($data->member_address);
    $provinces_id = $data->provinces_id;
    $districts_id = $data->districts_id;
    $sub_districts_id = $data->sub_districts_id;
    $zipcode = $data->zipcode;
    $facebook_id = $data->facebook_id;
} else {
    $member_fname = '';
    $member_lname = '';
    $member_type_id = '';
    $member_email = '';
    $member_mobileno = '';
    $member_profile = '';
    $member_address = '';
    $provinces_id = '';
    $districts_id = '';
    $sub_districts_id = '';
    $zipcode = '';
}

?>
<style>



</style>
	<div id="container">
		<div class="bg-cs-gray-second">
			<div class="row align-middle">
				<div class="columns">
					<nav aria-label="You are here:" role="navigation">
						<?php echo $breadcrumbs; ?>
					</nav>
				</div>
			</div>
		</div>
		<div class="bg-cs-gray clearfix">
			<h3 class="text-center color-blue page-title"><?php echo $title; ?></h3>
		</div>
		<div class="row content-inside">
			<!-- -------------- -->
			<!-- LEFT : SIDEBAR -->
			<!-- -------------- -->

			<div class="small-12 medium-4 large-3 columns">
				<?php $this->load->view('partials/acc-sidebar');?>
			</div>

			<!-- RIGHT : CONTENT -->
			<div class="small-12 medium-8 large-9 columns">
				<div class="small-12 medium-12 columns">
						<form action="" method="post" id="form_profile" enctype="multipart/form-data">
							<input type="hidden" name="member_id" value="<?php echo $member_id; ?>">
							<input type="hidden" name="old_member_profile" value="<?php echo $member_profile; ?>">

							<div>
								<div class="row">
									<div class="small-12 columns">
										<label for="email" class="clearfix">อีเมล์ / ชื่อผู้ใช้งาน</label>
										<div class="input-group">
											 <?php echo $member_email; ?>
										</div>
									</div>
								</div>
								<!-- end col -->

								<div class="row">
									<div class="small-12 medium-6 columns">
										<label for="member_fname" class="clearfix">ชื่อ<span class="color-red">*</span></label>
										<div class="input-group">
											<input type="text" name="member_fname" id="member_fname" value="<?php echo $member_fname; ?>" placeholder="ชื่อ" required>
										</div>
									</div>
									<div class="small-12 medium-6 columns">
										<label for="member_lname" class="clearfix">นามสกุล</label>
										<div class="input-group">
											<input type="text" name="member_lname" placeholder="นามสกุล" value="<?php echo $member_lname; ?>">
										</div>
									</div>
								</div>
								<!-- end col -->

								<div class="row">
									<div class="small-12 medium-6 columns">
										<label for="member_mobileno" class="clearfix">เบอร์ติดต่อ</label>
										<div class="input-group">
											<input type="text" name="member_mobileno" placeholder="เบอร์ติดต่อ" value="<?php echo $member_mobileno; ?>">
										</div>
									</div>
								</div>
								<!-- end col -->

										<?php if (!empty($member_profile)) {
    $member_profile_t = (!empty($facebook_id)) ? $member_profile : base_url().$sfilepath.$member_profile;
    ?>
								<div class="row">
									<div class="small-12 medium-8 columns mar-top-medium mar-bott-medium">
											<label for="member_lname" class="clearfix">รูปประจำตัว</label>
												<div class="po_relative">
													<a href="javascript:void(0);" id="<?php echo $member_id;
    ?>" class="btn-delete alert button po_absolute right-0">Remove</a>
													<input type="file" id="member_profile" name="member_profile" value="<?php echo $member_profile;
    ?>" >
												</div>
												<img src="<?php echo $member_profile_t;
    ?>" class="img-responsive thumbnail" alt="">

									</div>
								</div>
										<?php
} ?>
								<!-- end col -->

								<?php
if (empty($member_profile)) {
    ?>
								<div class="row  mar-top-medium mar-bott-medium">
									<div class="small-12 medium-8 columns">
									<label for="" class="clearfix">รูปประจำตัว</label>
										<input type="file" id="member_profile" name="member_profile" >
									</div>
								</div>
								<!-- end col -->




								<?php
} ?>

								<div class="row">
									<div class="small-12 medium-12 columns">
										<label for="email" class="clearfix">ที่อยู่</label>
										<div class="input-group">
											<textarea id="member_address" name="member_address" cols="20" rows="3"><?php echo $member_address; ?></textarea>
										</div>
									</div>
								</div>
								<!-- end col -->

								<div class="row">
									<div class="small-12 large-3 columns">
										<label for="provinces_id" class="clearfix">จังหวัด</label>
										<div class="input-group">
											<select id="provinces_id" class="" name="provinces_id" onchange="(this)">
												<option value="0">----เลือก----</option>
												<?php
                                                        foreach ($provinces as $row) {
                                                            //START FOREACH
                                                            $sel = ($row->provinces_id == $provinces_id) ? 'selected' : '';
                                                            ?>
													<option value="<?php echo $row->provinces_id;
                                                            ?>" <?php echo $sel;
                                                            ?>>
														<?php echo $row->provinces_name;
                                                            ?>
													</option>
													<?php

                                                        }//END FOREACH
                                                    ?>
											</select>
										</div>
									</div>
									<div class="small-12 large-3 columns">
										<label for="districts_id" class="clearfix">อำเภอ / เขต</label>
										<div class="input-group">
											<select id="districts_id" class="" name="districts_id" onchange="(this)">
												<option value="0">----เลือก----</option>
											</select>
										</div>
									</div>
									<div class="small-12 large-3 columns">
										<label for="sub_districts_id" class="clearfix">ตำบล / แขวง</label>
										<div class="input-group">
											<select id="sub_districts_id" class="form-control" name="sub_districts_id" onchange="(this)">
												<option value="0">----เลือก----</option>
											</select>
										</div>
									</div>

									<div class="small-12 large-3 columns">
										<label for="zipcode" class="clearfix">รหัสไปรษณีย์</label>
										<div class="input-group">
											<input type="text" maxlength="5" name="zipcode" placeholder="รหัสไปรษณีย์" value="<?php echo $zipcode; ?>">
										</div>
									</div>

								</div>
								<!-- end col -->

								<div class="row">
									<div class="small-12 medium-4 align-center columns">
										<div class="input-group">
											<button type="submit" name="action_condo_register" id="" class="button button-red-white expanded">แก้ไขข้อมูลส่วนตัว</button>
										</div>
									</div>
								</div>

								<div data-toggler="" data-animate="fade-in fade-out" class="alert callout callout secondary ease" id="panels-success" aria-expanded="true" style="display: none;">
							    <p><i class="fa " aria-hidden="true"></i> <span id="message"></span></p>
							  </div>

							</div>
						</form>

				</div>
			</div>
			<!--RIGHT-->

			<!-- ------------------- -->
			<!-- END RIGHT : CONTENT -->
			<!-- ------------------- -->



		</div>
	</div>
	<?php $this->load->view('partials/footer'); ?>
	<script>
			$("#form_profile").submit(function(event){

				//var data = $(this).serialize();
				var data = new FormData($(this)[0]);
				var url = '<?php echo site_url('page/m_edit_profile'); ?>';
				$.ajax({
					method: 'POST',
					url: url,
					data: data,
					cache: false,
					contentType: false,
					processData: false,
					success: function (data) {

						if( data.success === true ){
							$('#panels-success').show();
		          $('#message').html(data.message);
							$('.fa').addClass('fa-check-circle');
		          setInterval(function(){
		            window.location.reload();
		          }, 3000);

		          return true;
		        }else{
		          $('#panels-success').show();
							$('#message').html(data.message);
							$('.fa').addClass('fa-exclamation-triangle');
		          setInterval(function(){
		            window.location.reload();
		          }, 5000);

		          return false;
		        }

					},
					error: function () {
						console.log("failure");
					}
				});
				event.preventDefault();
				return false;

			});//END FORM SUBMIT


$(document).ready( function() {

		var provinces_id = $("#provinces_id").val();


	if( provinces_id != 0 )
	{
		$.ajax({
			url: "<?php echo base_url(); ?>page/provinces_districts_relation",
			data: {
				id: provinces_id
			},
			type: "POST",
			success: function (data) {
				$("#districts_id").html(data);
				$("#districts_id").val(<?php echo $districts_id; ?>);

				var districts_id = $("#districts_id").val();
				//console.log(districts_id);

				$.ajax({
					url: "<?php echo base_url(); ?>page/districts_subdistricts_relation",
					data: {
						id: districts_id
					},
					type: "POST",
					success: function (data) {
						$("#sub_districts_id").html(data);
						$("#sub_districts_id").val(<?php echo $sub_districts_id; ?>);
					}
				});

			}
		});


	}


			//$("#provinces_id").change(function () {
			$("#provinces_id").bind("change", function () {
				/*dropdown post */ //
				$.ajax({
					url: "<?php echo base_url(); ?>page/provinces_districts_relation",
					data: {
						id: $(this).val()
					},
					type: "POST",
					success: function (data) {
						$("#districts_id").html(data);
					}
				});
			});

			$("#districts_id").bind("change",function () {
				/*dropdown post */ //
				$.ajax({
					url: "<?php echo base_url(); ?>page/districts_subdistricts_relation",
					data: {
						id: $(this).val()
					},
					type: "POST",
					success: function (data) {
						$("#sub_districts_id").html(data);
					}
				});
			});
});
		</script>

<script>
	$(".btn-delete").click(function(event){
		var id = $(this).attr("id");
		var member_profile = $('#member_profile_name').val();
		$.ajax({
			url: "<?php echo base_url('page/remove_member_profile'); ?>",
			type: "POST",
			data: {
				member_id : id,
				member_profile : member_profile,
			},
			cache:false,
			success: function (data)
			{
//				console.log(data);
				window.location.reload();
			},
			error: function (xhr, desc, err)
			{
				console.log( err );
			}
		});
	event.preventDefault();
	return false;
	});
</script>
