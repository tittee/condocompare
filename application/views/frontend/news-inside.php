<?php
defined('BASEPATH') or exit();
/*HEADER*/
$this->load->view('partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'js' => isset($js) ? $js : array(), 'BodyClass' => ''));

$sfilepath = base_url().'uploads/news';
$news_title = (!empty($data->news_title)) ? $data->news_title : '-';
$news_caption = (!empty($data->news_caption)) ? $data->news_caption : '-';
$news_description = (!empty($data->news_description)) ? $this->primaryclass->decode_htmlspecialchars($data->news_description) : '-';

$fk_news_category_id = (!empty($data->fk_news_category_id)) ? $data->fk_news_category_id : '0';
$news_category_name = (!empty($fk_news_category_id) || $fk_news_category_id !== 0) ? $this->M->get_name('news_category_id, news_category_name', 'news_category_id', 'news_category_name', $fk_news_category_id, 'news_category') : '-';
$news_category_name_t = (!empty($news_category_name[0])) ? $news_category_name[0]->news_category_name : '-';

$visited = (!empty($data->visited)) ? $data->visited : '-';
$pic_thumb = $sfilepath.'/'.$data->news_pic_thumb;

$createdate = $this->dateclass->DateTimeFullFormat($data->createdate, 0, 1, 'Th');

?>
		<div class="container">
			<div class="bg-cs-gray-second">
				<div class="row align-middle">
					<div class="columns">
						<nav aria-label="You are here:" role="navigation">
							<?php echo $breadcrumbs; ?>
						</nav>
					</div>
				</div>
			</div>
			<div class="bg-cs-gray clearfix">
				<h3 class="text-center color-blue page-title"><?php echo $title; ?></h3>
			</div>
			<div class="row mar-top-large mar-bott-large align-middle">
				<div class="title small-12 medium-12 medium-centered column">
					<!--  ID : 14 = banner หน้ารายละเอียดข่าวสาร ด้านบน header -->
					<div id="" class="banner-carousel owl-carousel owl-theme">
					<?php
            $banner_14 = $this->Page_model->get_banner_location(14);
            $sfilepath_banner = base_url().'uploads/banner/';
            $banner_count = count($banner_14);

            // $banner_14_arr = array();
            for ($i = 0; $i < $banner_count;  ++$i) {
                $banner_14_id = (!empty($banner_14[$i]->banner_id)) ? $banner_14[$i]->banner_id : '';

                //UPDATE DISPLAY
                $displayed = (!empty($banner_14[$i]->displayed)) ? $banner_14[$i]->displayed : '';
                $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_14_id, 'banner');

                $banner_14_title = (!empty($banner_14[$i]->banner_title)) ? $banner_14[$i]->banner_title : '';
                $banner_14_url = (!empty($banner_14[$i]->banner_url)) ? $banner_14[$i]->banner_url : '#';
                $banner_14_pic_thumb = (!empty($banner_14[$i]->pic_thumb)) ? $sfilepath_banner.$banner_14[$i]->pic_thumb : 'http://placehold.it/1400x175/333?text=1400x175';
                // $banner_14_arr[] = '<a href="'.$banner_14_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_14_id.'"><img src="'.$banner_14_pic_thumb.'" alt="'.$banner_14_title.'" title=""></a>';
                            echo $banner_14_arr = '<div class="item"><a href="'.$banner_14_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_14_id.'"><img src="'.$banner_14_pic_thumb.'" alt="'.$banner_14_title.'" title=""></a></div>';
            }
            // shuffle($banner_14_arr);
            // echo (!empty($banner_14_arr[0])) ? $banner_14_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/1400x175/333?text=1400x175" alt="1400x175" title="1400x175"></a>';
          ?>
        </div>

				</div>
			</div>
			<!--NEWS SLIDER-->
			<div class="row clearfix">
				<!--LEFT-->
				<div class="small-12 large-9 column">
					<h2 class="title-red-black"><?php echo $news_title; ?></h2>
					<p><?php echo $createdate; ?></p>
          <!-- <p class="contact_name"><i class="fa fa-eye" style="color: #E9E9E9;"></i> จำนวนผู้ชม <?php echo $visited; ?> </p> -->
					<!--TITLE NEWS-->
					<div class="row clearfix">
						<div class="small-12 column">

<?php echo $news_description; ?>

						</div>
					</div>
				</div>
				<!--RIGTH-->
				<div class="small-12 large-3 column">
					<div class="bg-cs-blue well columns mar-bott-large">
						<form id="form_search" action="<?php echo site_url('news'); ?>" method="get">
						<div style="margin-bottom: 0;" class="input-group">
								<input type="text" name="keyword" placeholder="ค้นหาข่าวสาร" class="input-group-field" required>
								<div class="input-group-button">
									<button name="submit" style="font-size: .85rem;" class="button button-red-white"><i class="fa fa-search"></i></button>
								</div>
								<input type="hidden" name="action" value="action">
						</div>
					</form>
					</div>

					<!--  ID : 16 = banner หน้ารายละเอียดข่าวสาร  ด้านบน ขวามือ -->
					<div class="row">
						<div class="small-12 columns">
					<div id="" class="banner-carousel owl-carousel owl-theme">
					<?php
            $banner_16 = $this->Page_model->get_banner_location(16);
            $sfilepath_banner = base_url().'uploads/banner/';
            $banner_count = count($banner_16);

            // $banner_16_arr = array();
            for ($i = 0; $i < $banner_count;  ++$i) {
                $banner_16_id = (!empty($banner_16[$i]->banner_id)) ? $banner_16[$i]->banner_id : '';

                //UPDATE DISPLAY
                $displayed = (!empty($banner_16[$i]->displayed)) ? $banner_16[$i]->displayed : '';
                $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_16_id, 'banner');

                $banner_16_title = (!empty($banner_16[$i]->banner_title)) ? $banner_16[$i]->banner_title : '';
                $banner_16_url = (!empty($banner_16[$i]->banner_url)) ? $banner_16[$i]->banner_url : '#';
                $banner_16_pic_thumb = (!empty($banner_16[$i]->pic_thumb)) ? $sfilepath_banner.$banner_16[$i]->pic_thumb : 'http://placehold.it/380x400/333?text=380x400';
                // $banner_16_arr[] = '<a href="'.$banner_16_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_16_id.'"><img src="'.$banner_16_pic_thumb.'" alt="'.$banner_16_title.'" title=""></a>';
                echo $banner_16_arr = '<div class="item"><a href="'.$banner_16_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_16_id.'"><img src="'.$banner_16_pic_thumb.'" alt="'.$banner_16_title.'" title=""></a></div>';
            }
            // shuffle($banner_16_arr);
            // echo (!empty($banner_16_arr[0])) ? $banner_16_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/380x400/333?text=380x400" alt="380x400" title="380x400"></a>';
            ?>
					</div>
					</div>
					</div>

					<h2 class="title-blue-bg right">Most Read<span>	This Month</span></h2>
					<div class="inside">
						<article>

							<?php
                                //LIST CONDO CATEGORY
                                foreach ($data_mostread as $row) {
                                    $news_id = $row->news_id;
                                    $news_title = (!empty($row->news_title)) ? $row->news_title : '-';
                                    $news_slug = (!empty($row->news_slug)) ? $row->news_slug : $row->news_id;
                                    $news_title_limit = character_limiter($news_title, 20);

                                    $createdate = $row->createdate;
                                    $createdate_t = (isset($createdate) || !empty($createdate)) ? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, 'Th') : 'ยังไม่มีข้อมูล';
                                    ?>

							<div class="row clearfix news-sidebar">
								<div class="columns">
									<div class="well-gray">
										<h5><a href="<?php echo site_url('news-details/'.$news_slug);
                                    ?>" class="color-red"><?php echo $news_title_limit;?></a></h5>
										<p class="mar-bott-zero date_news"><?php echo $createdate_t;
                                    ?></p>
									</div>
								</div>
							</div>

							<?php

                                }//END FOREACH ?>

						</article>
					</div>
					<!-- <h2 class="title-blue-bg right">Lastest<span>	Comments</span></h2> -->
					<!-- <div class="inside">
						<article>
							<div class="well-list border-all clearfix">
								<div class="row">
									<div class="small-12 medium-5 columns">
										<p><a href="#"><img src="http://placehold.it/400x400&amp;text=Who/333" alt=""></a></p>
									</div>
									<div class="small-12 medium-7 columns highlight-caption">
										<h4 class="f_size1"><a href="#" class="color-red">หัวข้อคอมเม้นท์จาก บทความ</a></h4>
										<p class="mar-bott-zero">เนื้อหาคอมเม้นท์จาก บทความบทความ</p>
									</div>
								</div>
							</div>
							<div class="well-list border-all clearfix">
								<div class="row">
									<div class="small-12 medium-5 columns">
										<p><a href="#"><img src="http://placehold.it/400x400&amp;text=Who/333" alt=""></a></p>
									</div>
									<div class="small-12 medium-7 columns highlight-caption">
										<h4 class="f_size1"><a href="#" class="color-red">หัวข้อคอมเม้นท์จาก บทความ</a></h4>
										<p class="mar-bott-zero">เนื้อหาคอมเม้นท์จาก บทความบทความ</p>
									</div>
								</div>
							</div>
							<div class="well-list border-all clearfix">
								<div class="row">
									<div class="small-12 medium-5 columns">
										<p><a href="#"><img src="http://placehold.it/400x400&amp;text=Who/333" alt=""></a></p>
									</div>
									<div class="small-12 medium-7 columns highlight-caption">
										<h4 class="f_size1"><a href="#" class="color-red">หัวข้อคอมเม้นท์จาก บทความ</a></h4>
										<p class="mar-bott-zero">เนื้อหาคอมเม้นท์จาก บทความบทความ</p>
									</div>
								</div>
							</div>
						</article>
					</div> -->
					<!-- <h2 class="title-blue-red right clearfix mar-top-large">Popular<span>	Tags</span></h2>
					<div class="row">
						<div class="columns"><a href="#" class="button tag-gray-bg">ข่าวอสังหาริมทริพย์</a><a href="#" class="button tag-gray-bg">ข่าวดวง</a><a href="#" class="button tag-gray-bg">ข่าวท่ีดิน</a><a href="#" class="button tag-gray-bg">ข่าวเศรษฐกิจ</a><a href="#" class="button tag-gray-bg">ข่าวการเงิน</a><a href="#" class="button tag-gray-bg">ผ่อนบ้าน</a><a href="#" class="button tag-gray-bg">ภาษีหัก ณ ที่จ่าย</a></div>
					</div> -->
				</div>
			</div>
			<div class="row clearfix">
				<div class="title small-12 medium-8 medium-centered column">
					<!--  ID : 15 = banner หน้ารายละเอียดข่าวสาร  ด้านบน footer -->
					<div id="" class="banner-carousel owl-carousel owl-theme">
					<?php
            $banner_15 = $this->Page_model->get_banner_location(15);
            $sfilepath_banner = base_url().'uploads/banner/';
            $banner_count = count($banner_15);

            // $banner_15_arr = array();
            for ($i = 0; $i < $banner_count;  ++$i) {
                $banner_15_id = (!empty($banner_15[$i]->banner_id)) ? $banner_15[$i]->banner_id : '';

                //UPDATE DISPLAY
                $displayed = (!empty($banner_15[$i]->displayed)) ? $banner_15[$i]->displayed : '';
                $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_15_id, 'banner');

                $banner_15_title = (!empty($banner_15[$i]->banner_title)) ? $banner_15[$i]->banner_title : '';
                $banner_15_url = (!empty($banner_15[$i]->banner_url)) ? $banner_15[$i]->banner_url : '#';
                $banner_15_pic_thumb = (!empty($banner_15[$i]->pic_thumb)) ? $sfilepath_banner.$banner_15[$i]->pic_thumb : 'http://placehold.it/1400x175/333?text=1400x175';
                // $banner_15_arr[] = '<a href="'.$banner_15_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_15_id.'"><img src="'.$banner_15_pic_thumb.'" alt="'.$banner_15_title.'" title=""></a>';
                            echo $banner_15_arr = '<div class="item"><a href="'.$banner_15_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_15_id.'"><img src="'.$banner_15_pic_thumb.'" alt="'.$banner_15_title.'" title=""></a></div>';
            }
            // shuffle($banner_15_arr);
            // echo (!empty($banner_15_arr[0])) ? $banner_15_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/1400x175/333?text=1400x175" alt="1400x175" title="1400x175"></a>';
          ?>
					</div>

				</div>
			</div>
			<div class="row clearfix"></div>
		</div>
<?php $this->load->view('partials/footer');?>
<?php
if (isset($slippry)) {
    ?>
<!--NEWS SLIDER : SLIPPRY-->
<script type="text/javascript">
	$(document).ready(function() {
		jQuery('#news-demo').slippry({
			// general elements & wrapper
			slippryWrapper: '<div class="sy-box news-slider" />', // wrapper to wrap everything, including pager
			elements: 'article', // elments cointaining slide content

			// options
			adaptiveHeight: false, // height of the sliders adapts to current
			captions: false,


			// transitions
			transition: 'horizontal', // fade, horizontal, kenburns, false
			speed: 1200,
			pause: 8000,

			// slideshow
			autoDirection: 'prev'
		});
	});
</script>
<?php

}//END IF SLIPPY ?>
