<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'js'=>isset($js)?$js:array(),'BodyClass'=>''));
if(isset( $data['row'] ))
{
	$action 									= 'edit';
	$condo_id 								= $data['row']->condo_id;
	$condo_property_id 				= $data['row']->condo_property_id;
	$member_id 								= $data['row']->member_id;
	$staff_id 								= $data['row']->staff_id;
	$type_offering_id					= $data['row']->type_offering_id;
	$type_zone_id	 						= $data['row']->type_zone_id;
	$condo_title 							= $data['row']->condo_title;
	$condo_owner_name 				= $data['row']->condo_owner_name;
	$condo_holder_name	 			= $data['row']->condo_holder_name;
	$condo_builing_finish	 		= $data['row']->condo_builing_finish;
	$zone_id	 								= $data['row']->zone_id;
	$type_suites_id 					= $data['row']->type_suites_id;
	$type_decoration_id 			= $data['row']->type_decoration_id;
	$facilities_id 						= $data['row']->facilities_id;
	$featured_id 							= $data['row']->featured_id;
	$transportation_category_id 	= $data['row']->transportation_category_id;
	$transportation_distance  = $data['row']->transportation_distance;
	$transportation_id				= $data['row']->transportation_id;
	$condo_all_room 					= $data['row']->condo_all_room;
	$condo_all_toilet 				= $data['row']->condo_all_toilet;

	$condo_interior_room 				= $data['row']->condo_interior_room;
	$condo_direction_room 				= $data['row']->condo_direction_room;
	$condo_scenery_room 				= $data['row']->condo_scenery_room;
	$condo_living_area 				= $data['row']->condo_living_area;
	$condo_electricity 				= $data['row']->condo_electricity;
	$condo_tag 						= trim($data['row']->condo_tag);
	$condo_expert 						= trim($data['row']->condo_expert);
	$condo_description 				= trim($data['row']->condo_description);
	$pic_thumb 								= $data['row']->pic_thumb;
	$pic_large 								= $data['row']->pic_large;
	$condo_no 								= $data['row']->condo_no;
	$condo_floor 							= $data['row']->condo_floor;
	$condo_area_apartment			= $data['row']->condo_area_apartment;
	$condo_alley 							= $data['row']->condo_alley;
	$condo_road 							= $data['row']->condo_road;
	$condo_address 						= $data['row']->condo_address;
	$provinces_id 						= $data['row']->provinces_id;
	$districts_id 						= $data['row']->districts_id;
	$sub_districts_id 				= $data['row']->sub_districts_id;
	$condo_zipcode 						= $data['row']->condo_zipcode;
	$condo_total_price 					= $data['row']->condo_total_price;
	$condo_price_per_square_meter		= $data['row']->condo_price_per_square_meter;
	$condo_transportation 					= $data['row']->condo_transportation;
	$condo_googlemaps 				= $data['row']->condo_googlemaps;
	$latitude 								= $data['row']->latitude;
	$longitude 								= $data['row']->longitude;
	$type_status_id 					= $data['row']->type_status_id;
	$newsarrival 							= $data['row']->newsarrival;
	$highlight 								= $data['row']->highlight;
	$publish 									= $data['row']->publish;
	$approved 								= $data['row']->approved;
}
else
{
	$action 									= 'add';
	$condo_property_id 				= "";
	$member_id 								= "";
	$staff_id 								= "";
	$type_zone_id	 						= "";
	$type_offering_id 				= "";
	$condo_title 							= "";
	$condo_owner_name 				= "";
	$condo_holder_name	 			= "";
	$condo_builing_finish	 		= "";
	$zone_id	 								= "";
	$type_suites_id 					= "";
	$type_decoration_id 			= "";
	$facilities_id 						= "";
	$featured_id 							= "";
	$transportation_category_id = "";
	$transportation_id				= "";
	$condo_all_room 					= "";
	$condo_all_toilet 				= "";
	$condo_interior_room 			= "";
	$condo_direction_room 		= "";
	$condo_scenery_room 			= "";
	$condo_living_area 				= "";
	$condo_electricity 				= "";
	$condo_tag								= "";
	$condo_expert 						= "";
	$condo_description 				= "";
	$pic_thumb 								= "";
	$pic_large 								= "";
	$condo_no 								= "";
	$condo_floor 							= "";
	$condo_area_apartment			= "";
	$condo_alley 							= "";
	$condo_road 							= "";
	$condo_address 						= "";
	$provinces_id 						= "";
	$districts_id 						= "";
	$sub_districts_id 				= "";
	$condo_zipcode 						= "";
	$condo_total_price 				= "";
	$condo_price_per_square_meter		= "";
	$condo_transportation 		= "";
	$condo_googlemaps					= "";
	$latitude 								= "";
	$longitude 								= "";
	$type_status_id 					= "";
	$newsarrival 							= "";
	$highlight 								= "";
	$publish 									= "";
	$approved 								= "";
	$transportation_distance 	= "";
}
?>
	<link href="<?php echo base_url()?>assets/css/dropzone.min.css" rel="stylesheet" type="text/css">

	<div id="container">
		<div class="bg-cs-gray-second">
			<div class="row align-middle">
				<div class="columns">
					<nav aria-label="You are here:" role="navigation">
						<?php echo $breadcrumbs; ?>
					</nav>
				</div>
			</div>
		</div>
		<div class="bg-cs-gray clearfix">
			<h3 class="text-center color-blue page-title"><?php echo $title; ?></h3>
		</div>
		<div class="row content-inside">
			<!-- -------------- -->
			<!-- LEFT : SIDEBAR -->
			<!-- -------------- -->

			<div class="small-12 medium-4 large-3 columns">
				<?php $this->load->view('partials/acc-sidebar');?>
			</div>

			<!-- RIGHT : CONTENT -->
			<div class="small-12 medium-8 large-9 columns">
				<div class="small-12 medium-12 columns">
					<!-- CONTENT -->

					<!-- <div class="progress" role="progressbar" tabindex="0" aria-valuenow="20" aria-valuemin="0" aria-valuetext="25 percent" aria-valuemax="100">
						<span class="progress-meter" style="width: 25%">
						<p class="progress-meter-text">25%</p>
						</span>
					</div> -->

					<form action="" id="form_condo" name="form_condo"  method="post" enctype="multipart/form-data">


						<ul class="tabs" data-tabs id="example-tabs">
							<li class="tabs-title is-active"><a href="#panel1" aria-selected="true">1. รายละเอียดโครงการ</a></li>
							<li class="tabs-title"><a href="#panel2" onclick="loadScript();">2. รายการทรัพย์สิน</a></li>
							<li class="tabs-title"><a href="#panel3"  onclick="javascript:void(0);">3. รูปภาพ/แกเลอรี่</a></li>
							<li class="tabs-title"><a href="#panel4"  onclick="javascript:void(0);">4. รายละเอียด</a></li>
						</ul>
						<div class="tabs-content" data-tabs-content="example-tabs">
							<div class="tabs-panel is-active" id="panel1">

								<div class="row">
									<div class="medium-6 columns">
										<label>โซนของทรัพย์
											<select id="zone_id" name="zone_id" onchange="(this)">
											<option value="0">----เลือก----</option>
											<?php
												foreach ($type_zone as $row) {//START FOREACH
													$sel = ($row->type_zone_id == $zone_id)? 'selected': '';
											?>
											<option value="<?php echo $row->type_zone_id; ?>" <?php echo $sel; ?>>
												<?php echo $row->type_zone_title; ?>
											</option>
											<?php
												}//END FOREACH
											?>
											</select>
										</label>
									</div>
									<div class="medium-6 columns">
										<label>แบบห้องชุด
											<select id="type_suites_id" name="type_suites_id" onchange="(this)">
												<option value="0">----เลือก----</option>
												<?php
													foreach ($type_suites as $row) {//START FOREACH
														$sel = ($row->type_suites_id == $type_suites_id)? 'selected': '';
												?>
												<option value="<?php echo $row->type_suites_id; ?>" <?php echo $sel; ?>>
													<?php echo $row->type_suites_title; ?>
												</option>
												<?php
													}//END FOREACH
												?>
											</select>
										</label>
									</div>
								</div>
								<!-- END COL -->

								<div class="row">
									<div class="medium-6 columns">
										<label>การตกแต่ง
											<select id="type_decoration_id" name="type_decoration_id" onchange="(this)">
												<option value="0">----เลือก----</option>
												<?php
													foreach ($type_decoration as $row) {//START FOREACH
														$sel = ($row->type_decoration_id == $type_decoration_id)? 'selected': '';
												?>
												<option value="<?php echo $row->type_decoration_id; ?>" <?php echo $sel; ?>>
													<?php echo $row->type_decoration_title; ?>
												</option>
												<?php
													}//END FOREACH
												?>
											</select>
										</label>
									</div>
									<div class="medium-6 columns">
										<label>เฟอร์นิเจอร์
											<select id="condo_electricity" name="condo_electricity" onchange="(this)">
												<option value="0">----เลือก----</option>
												<?php
													for($i=1; $i<=10; $i++) {//START FOREACH
														$sel = ($i == $condo_electricity)? 'selected': '';
												?>
												<option value="<?php echo $i; ?>" <?php echo $sel; ?>>
													<?php echo $i; ?> รายการ
												</option>
												<?php
													}//END FOREACH
												?>
											</select>
										</label>
									</div>
								</div>
								<!-- END COL -->

								<div class="row">
									<div class="medium-6 columns">
										<label>อยู่ชั้น
											<div class="input-group">
												<span class="input-group-label"><i class="fa fa-building-o"></i></span>
												<input class="input-group-field" type="number" name="condo_floor" value="<?php echo $condo_floor;?>">
											</div>
										</label>
									</div>
									<div class="medium-6 columns">
										<label>ขนาดพื้นที่ใช้สอย
											<div class="input-group">
												<span class="input-group-label"><i class="fa fa-hotel"></i></span>
												<input class="input-group-field" type="number" value="<?php echo $condo_living_area; ?>" name="condo_living_area">
											</div>
										</label>
									</div>
								</div>
								<!-- END COL -->

								<div class="row">
									<div class="medium-6 columns">
										<label>ห้องนอน
											<div class="input-group">
												<span class="input-group-label"><i class="fa fa-hotel"></i></span>
												<input class="input-group-field" type="number" value="<?php echo $condo_all_room; ?>" name="condo_all_room">
											</div>
										</label>
									</div>
									<div class="medium-6 columns">
										<label>ห้องน้ำ
											<div class="input-group">
												<span class="input-group-label"><i class="fa fa-male"></i></span>
												<input class="input-group-field" type="number" value="<?php echo $condo_all_toilet; ?>" name="condo_all_toilet">
											</div>
										</label>
									</div>
								</div>
								<!-- END COL -->

								<div class="row">
									<div class="medium-12 columns">
										<label>อุปกรณ์ตกแต่งภายในห้อง
												<input class="input" type="text" value="<?php echo $condo_interior_room; ?>" name="condo_interior_room">
										</label>
									</div>
								</div>
								<!-- END COL -->

								<div class="row">
									<div class="medium-12 columns">
										<label>ทิศของห้อง
												<input class="input" type="text" value="<?php echo $condo_direction_room; ?>" name="condo_direction_room">
										</label>
									</div>
								</div>
								<!-- END COL -->

								<div class="row">
									<div class="medium-12 columns">
										<label>วิวของห้อง
												<input class="input" type="text" value="<?php echo $condo_scenery_room; ?>" name="condo_scenery_room">
										</label>
									</div>
								</div>
								<!-- END COL -->

								<div class="row">
									<section class="columns mar-top-medium">
										<h6>สิ่งอำนวยความสะดวก</h6>
									</section>
									<hr>
								</div>
								<!-- END TITLE -->
								<div class="row">
									<?php
									if ( $action == 'add' )
									{
										foreach ($facilities as $row)
										{
									?>
										<div class="small-6 medium-4 columns">
											<input type="checkbox" name="facilities_id[]" value="<?php echo $row->facilities_id; ?>">
											<label for="checkbox1"><?php echo $row->facilities_title; ?></label>
										</div>

									<?php
										}//END FOREACH
									}//END IF ACTION
									?>
									<?php
									if ( $action == 'edit')
									{
										$query = $this->db
											->select('facilities_id, facilities_title')
											->order_by('facilities_id', 'ASC')
											->get('facilities');
										while($row = $query->unbuffered_row())
										{
											$facilities_id = $row->facilities_id;

											$query2 = $this->db
												->select('*')
												->where('condo_id', $condo_id)
												->where('facilities_id', $facilities_id)
												->get('condo_relation_facilities');
											$i = 1;
											if ($query2->num_rows() > 0)
											{
												$row2 = $query2->row();
												//var_dump($row2->condo_relation_facilities_id);
												//echo $row2->featured_id;
										?>
										<div class="small-6 medium-4 columns">
											<input type="checkbox" name="facilities_id[]" value="<?php echo $row->facilities_id; ?>" checked>
											<label for="checkbox1"><?php echo $row->facilities_title; ?></label>
											<input type="hidden" name="condo_relation_facilities_id[]" value="<?php echo $row2->condo_relation_facilities_id; ?>">
										</div>
											<?php
											}
											else
											{
											?>
										<div class="small-6 medium-4 columns">
											<input type="checkbox" name="facilities_id[]" value="<?php echo $row->facilities_id; ?>">
											<label for="checkbox1"><?php echo $row->facilities_title; ?></label>
										</div>
										<?php
											}//END IF
										}
									}//END IF ACTION
										?>
								</div>
								<!-- END COL -->

								<div class="row">
									<section class="columns mar-top-medium">
										<h6>รายการที่น่าสนใจ</h6>
									</section>
									<hr>
								</div>
								<!-- END TITLE -->
								<div class="row">
								<?php
										if ( $action == 'add' )
										{
											foreach ($featured as $row_feat)
											{
										?>
											<div class="small-6 medium-4 columns">
												<input type="checkbox" name="featured_id[]" value="<?php echo $row_feat->featured_id; ?>">
												<label for=""><?php echo $row_feat->featured_title; ?></label>
											</div>
											<?php
											}//END FOREACH
										}//END IF ACTION

										if ( $action == 'edit')
										{


										$i = 0;
										$query = $this->db
											->select('featured_id, featured_title')
											->order_by('featured_id', 'ASC')
											->get('featured');
										while($row = $query->unbuffered_row())
										{
											$featured_id = $row->featured_id;

											$query2 = $this->db
												->select('*')
												->where('condo_id', $condo_id)
												->where('featured_id', $featured_id)
												->get('condo_relation_featured');
											if ($query2->num_rows() > 0)
											{
												$row2 = $query2->row();// ไม่ได้ใช้อะไรนิ
												//echo $row2->featured_id;

											?>
											<div class="small-6 medium-4 columns">
												<input type="checkbox" name="featured_id[]" value="<?php echo $row->featured_id; ?>" checked>
												<label for=""><?php echo $row->featured_title; ?></label>
												<input type="hidden" name="condo_relation_featured_id[]" value="<?php echo $row2->condo_relation_featured_id; ?>">
											</div>
											<?php
											}
											else
											{
											?>
											<div class="small-6 medium-4 columns">
												<input type="checkbox" name="featured_id[]" value="<?php echo $row->featured_id; ?>">
												<label for=""><?php echo $row->featured_title; ?></label>
											</div>
											<?php
											}
	//																		$this->primaryclass->pre_var_dump($row2);
										}
									}//END IF ACTION
								?>
								</div>
								<!-- END COL -->

								<div class="row">
									<section class="columns mar-top-medium">
										<h6>สถานที่ใกล้เคียง &#40;<span class="subheader">เลือกได้ ไม่เกิิน 5 สถานที่</span>&#41;</h6>

									</section>
									<hr>
								</div>
								<!-- END TITLE -->
								<!--<div class="row">
									<div class="small-6 medium-4 columns">
										<input id="checkbox1" type="checkbox">
										<label for="checkbox1">Checkbox 1</label>
									</div>
									<div class="small-6 medium-8 columns">
										<span class="warning label">ตัวอย่าง : 7 นาที (1.46 กิโลเมตร)</span>
										<input type="text">
									</div>
								</div>-->
								<!-- END COL -->
								<div class="form-clone">
									<div class="row">
										<div class="small-5 medium-4 columns">
											<span class="warning label">เลือกสถานที่ใกล้เคียง</span>
											<select id="transportation_category_id" name="transportation_category_id[]" onchange="(this)">
												<option value="0">----เลือก----</option>
												<?php
												foreach ($transportation_category as $row)
												{//START FOREACH GROUP
													$sel = ($row->transportation_category_id == $transportation_category_id)? 'selected': '';

													if ($transportation_category != $row->transportation_category_id)
													{
														if ($transportation_category != '')
														{
															echo '</optgroup>';
														}
														echo '<optgroup label="'.ucfirst($row->transportation_category_title).'">';
													}
													$transportation = $this->Condominium_model->get_transportation_option($row->transportation_category_id);
													foreach ($transportation as $row_in)
													{//START FOREACH
														echo '<option value="'.$row_in->transportation_id.'">'.$row_in->transportation_title.'</option>';
													}//END FOREACH
												?>

													<?php
												}//END FOREACH GROUP
												if ($transportation_category != '') {
													echo '</optgroup>';
												}
												?>
											</select>
										</div>
										<div class="small-5 medium-4 columns">
											<span class="warning label">ตัวอย่าง : 7 นาที (1.46 กิโลเมตร)</span>
											<input type="text" name="transportation_distance[]" value="<?php echo $transportation_distance; ?>" maxlength="50" placeholder="">
										</div>
										<div class="small-2 medium-2 columns">
											<a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa fa-plus"></i></a>
										</div>
									</div>
								</div>
								<!-- END COL -->

								<div class="row">
									<div class="columns">
										<a href="#panel2" class="float-right button tabclick1">ถัดไป <i class="fa fa-chevron-right"></i></a>
									</div>
								</div>
								<!-- END COL -->

							</div>
							<!-------------------------------------- END TAB #1 --------------------------------------->

							<div class="tabs-panel" id="panel2">
								<div class="row">
									<section class="columns mar-top-medium">
										<h6 class="float-left">ข้อมูลเบื้องต้น </h6>
										<small class="text-right color-red float-right">&#40;เลือกได้ไม่เกิน 5 สถานที่&#41;</small>
									</section>
									<hr>
								</div>
								<!-- END TITLE -->
								<div class="row">
									<div class="medium-6 columns">
										<label>ประเภททรัพย์สิน
											<select name="property_type_id">
												<option value="C">คอนโดมิเนี่ยม</option>
											</select>
										</label>
									</div>
									<div class="medium-6 columns">
										<label>ประเภทการเสนอขาย
											<select id="type_offering_id"  name="type_offering_id" onchange="(this)">
												<option value="0">----เลือก----</option>
												<?php
														foreach ($type_offering as $row) {//START FOREACH
															$sel = ($row->type_offering_id == $type_offering_id)? 'selected': '';
													?>
													<option value="<?php echo $row->type_offering_id; ?>" <?php echo $sel; ?>>
														<?php echo $row->type_offering_title; ?>
													</option>
													<?php
														}//END FOREACH
													?>
											</select>
										</label>
									</div>
								</div>
								<!-- END COL -->

								<div class="row">
									<div class="medium-12 columns">
										<label>ชื่อโครงการ
											<input type="text" name="condo_title" value="<?php echo $condo_title; ?>" placeholder="">
										</label>
									</div>
								</div>
								<!-- END COL -->

								<div class="row">
									<div class="medium-8 columns">
										<label>เจ้าของโครงการ
											<input type="text" name="condo_owner_name" value="<?php echo $condo_owner_name; ?>" placeholder="">
										</label>
									</div>
									<div class="medium-4 columns">
										<label>ปีที่สร้างเสร็จ
											<input type="text" maxlength="4" name="condo_builing_finish"  value="<?php echo $condo_builing_finish;?>">
										</label>
									</div>
								</div>
								<!-- END COL -->

								<div class="row">
									<section class="columns mar-top-medium">
										<h6 class="float-left">รายละเอียดที่ตั้ง</h6>
									</section>
									<hr>
								</div>
								<!-- END TITLE -->

								<div class="row">
									<div class="medium-4 columns">
										<label>ห้องเลขที่ / บ้านเลขที่
											<div class="input-group">
												<span class="input-group-label"><i class="fa fa-home"></i></span>
												<input class="input-group-field" type="text" value="<?php echo $condo_no; ?>" name="condo_no">
											</div>
										</label>
									</div>
									<div class="medium-4 columns">
										<label>หมู่บ้าน / ซอย
											<div class="input-group">
												<span class="input-group-label"><i class="fa fa-location-arrow"></i></span>
												<input class="input-group-field" type="text" value="<?php echo $condo_alley; ?>" name="condo_alley">
											</div>
										</label>
									</div>
									<div class="medium-4 columns">
										<label>ถนน
											<div class="input-group">
												<span class="input-group-label"><i class="fa fa-bus"></i></span>
												<input class="input-group-field" type="text" value="<?php echo $condo_road; ?>" name="condo_road">
											</div>
										</label>
									</div>
								</div>
								<!-- END COL -->

								<div class="row">
									<div class="medium-4 columns">
										<label>จังหวัด
											<select id="provinces_id" name="provinces_id" onchange="(this)">
												<option value="0">----เลือก----</option>
												<?php
														foreach ($provinces as $row) {//START FOREACH
															$sel = ($row->provinces_id == $provinces_id)? 'selected': '';
													?>
													<option value="<?php echo $row->provinces_id; ?>" <?php echo $sel; ?>>
														<?php echo $row->provinces_name; ?>
													</option>
													<?php
														}//END FOREACH
													?>
											</select>
										</label>
									</div>
									<div class="medium-4 columns">
										<label>อำเภอ/เขต
											<select id="districts_id" name="districts_id" onchange="(this)">
												<option value="0">----เลือก----</option>
											</select>
										</label>
									</div>
									<div class="medium-4 columns">
										<label>ตำบล/แขวง
											<select id="sub_districts_id" name="sub_districts_id" onchange="(this)">
												<option value="0">----เลือก----</option>
											</select>
										</label>
									</div>
								</div>
								<!-- END COL -->

								<div class="row">
									<div class="medium-12 columns">
										<label>รหัสไปรษณีย์
											<div class="input-group">
												<span class="input-group-label"><i class="fa fa-envelope-o"></i></span>
												<input class="input-group-field" type="text" name="condo_zipcode" maxlength="5" value="<?php echo $condo_zipcode; ?>">
											</div>
										</label>
									</div>
								</div>
								<!-- END COL -->

								<div class="row">
									<section class="columns mar-top-medium">
										<h6>แผนที่</h6>
									</section>
									<hr>
								</div>
								<!-- END TITLE -->
								<div class="row">
									<div class="medium-12 columns">
									<?php //echo $data['map']['html']; ?>
									<div id="map-canvas"></div>
									</div>
								</div>
								<!-- END COL -->

								<div class="row  mar-top-medium">
									<section class="columns">
										<h6>ข้อมูลด้านราคา</h6>
									</section>
									<hr>
								</div>
								<!-- END TITLE -->
								<div class="row">
									<div class="small-6 medium-6 columns">
										<label>ราคา
											<span class="warning label">กรอกทศนยิม 2 ตำแหน่ง : 999999999.99</span>
											<div class="input-group">
												<span class="input-group-label"><i class="fa fa-usd"></i></span>
												<input class="input-group-field" type="text" name="condo_total_price" value="<?php echo $condo_total_price; ?>">
											</div>
										</label>
									</div>
									<div class="small-6 medium-6 columns">
										<label>ราคา/ตร.ม.
											<span class="warning label">กรอกทศนยิม 2 ตำแหน่ง : 999999999.99</span>
											<div class="input-group">
												<span class="input-group-label"><i class="fa fa-usd"></i></span>
												<input class="input-group-field" type="text" name="condo_price_per_square_meter" value="<?php echo $condo_price_per_square_meter; ?>">
											</div>
										</label>
									</div>

								</div>
								<!-- END COL -->
								<div class="row">
									<div class="columns">
										<a href="#panel2" class="float-right button tabclick2">ถัดไป <i class="fa fa-chevron-right"></i></a>
									</div>
								</div>
								<!-- END COL -->

							</div>
							<!----------------------------------------- END TAB #2 ------------------------------------>

							<div class="tabs-panel" id="panel3">
								<div class="row">
									<section class="columns mar-top-medium">
										<h6 class="float-left">รูปภาพปก</h6>
									</section>
									<hr>
								</div>
								<!-- END TITLE -->
								<div class="row">
									<div class="small-5 medium-3 columns">
										<label>รูปภาพขนาดเล็ก</label>
									</div>
									<div class="small-7 medium-9 columns">
										<!-- <label for="pic_thumb" class="button">Upload File</label> -->
										<p>
											<span class="label warning">ขนาดรูปภาพไม่เกิน 4 MB และ กว้าง : 656px X ยาว : 464px</span>
										</p>
										<input type="file" id="pic_thumb" name="pic_thumb" class="">
										<?php if(!empty($pic_thumb))
										{
										?>
											<img src="<?php echo base_url()?>uploads/condominium/<?php echo $pic_thumb; ?>" class="img-responsive" alt="">
										<?php } ?>
									</div>
								</div>
								<!-- END COL -->

								<div class="row">
									<div class="small-5 medium-3 columns">
										<label>รูปภาพขนาดใหญ่</label>
									</div>
									<div class="small-7 medium-9 columns">
										<!-- <label for="pic_large" class="button">Upload File</label> -->
										<p>
											<span class="label warning">ขนาดรูปภาพไม่เกิน 4 MB และ กว้าง : 900px X ยาว : 500px</span>
										</p>
										<input type="file" id="pic_large" name="pic_large" class="">
										<?php if(!empty($pic_large))
										{
										?>
											<img src="<?php echo base_url()?>uploads/condominium/<?php echo $pic_large; ?>" class="img-responsive" alt="">
										<?php } ?>
									</div>
								</div>
								<!-- END COL -->

								<div class="row">
									<div class="small-12 columns">
										<label>รูปภาพแกเลอรี่
										</label>
									</div>
									<div class="small-12 columns">
										<div id="dropzone" class="dropzone">
											<div class="fallback">
												<input name="file[]" type="file" multiple />
											</div>
										</div>
									</div>
								</div>
								<!-- END COL -->
								<div class="row">
									<div class="columns">
										<a href="#panel3" class="float-right button tabclick3">ถัดไป <i class="fa fa-chevron-right"></i></a>
									</div>
								</div>
								<!-- END COL -->
							</div>
							<!----------------------------------------- END TAB #4 ------------------------------------>

							<div class="tabs-panel" id="panel4">
								<div class="row">
									<section class="columns mar-top-medium">
										<h6 class="float-left">รายละเอียด</h6>
									</section>
									<hr>
								</div>
								<!-- END TITLE -->
								<div class="row">
									<div class="small-12 medium-12 columns">
										<label>คำค้นหา</label>
										<textarea  name="condo_tag" id="condo_tag"><?php echo $condo_tag;?></textarea>
									</div>
								</div>
								<!-- END COL -->

								<div class="row">
									<div class="small-12 medium-12 columns">
										<label>รายละเอียดย่อย</label>
										<textarea  name="condo_expert" id="condo_expert"><?php echo $condo_expert;?></textarea>
									</div>
								</div>
								<!-- END COL -->

								<div class="row">
									<div class="small-12 medium-12 columns">
										<label>รายละเอียดประกาศ</label>
										<textarea class="wysiwigeditor"  name="condo_description" id="condo_description"><?php echo $condo_description;?></textarea>
									</div>
								</div>
								<!-- END COL -->

								<!-- END COL -->
								<div class="row mar-top-large">
									<div class="columns">
										<button id="submit" type="submit" class="float-right success button" onclick="tinyMCE.triggerSave(true,true);">SUBMIT</button>
										<?php if( isset($_SESSION['mID']) ){ ?>
										<input type="hidden" name="member_id" value="<?php echo $_SESSION['mID']; ?>">
										<?php }else{ ?>
										<input type="hidden" name="member_id" value="0">
										<?php } ?>
										<input type="hidden" name="approved" value="0">
										<input type="hidden" name="publish" value="0">
										<input type="hidden" name="newsarrival" value="0">
										<input type="hidden" name="highlight" value="0">
										<input type="hidden" name="latitude" id="latitude" value="">
										<input type="hidden" name="longitude" id="longitude" value="">
									</div>
								</div>
								<!-- END COL -->
							</div>
							<!----------------------------------------- END TAB #4 ------------------------------------>
						</div>
					</form>
					<!-- END FORM -->
				</div>
				<!--RIGHT-->
			</div>

			<!-- ------------------- -->
			<!-- END RIGHT : CONTENT -->
			<!-- ------------------- -->



		</div>
	</div>

	<?php $this->load->view('partials/footer', array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>''));?>



	<script src="<?php echo base_url()?>assets/js/dropzone.min.js" type="text/javascript"></script>
	<script>
$(document).ready(function(){
		$('.tabclick1').on('click',function(e){
			e.preventDefault();
			$('.tabs-title > a[href="#panel2"]').trigger('click');
			//return false;
		});

		$('.tabclick2').on('click',function(e){
			e.preventDefault();
			$('.tabs-title > a[href="#panel3"]').trigger('click');
			//return false;
		});

		$('.tabclick3').on('click',function(e){
			e.preventDefault();
			$('.tabs-title > a[href="#panel4"]').trigger('click');
			//return false;
		});
});

	$(document).ready(function () {
		$("#form_condo").submit(function (event) {
			var formData = new FormData($(this)[0]);


			$.ajax({
					url: "<?php echo base_url(); ?>condominium/add_condominium",
					type: "POST",
					data: formData,
					async: false,
					success: function (data, status)
					{
						console.log(data);
						//
						swal({
							title: 	"สำเร็จ",
							text: 	"ท่านได้ทำการฝากทรัพย์ เรียบร้อยแล้ว..",
							type: 	"info",
						},
						function(isConfirm){
							window.location.replace('<?php echo base_url(); ?>condominium');
						});
						return true;


					},
					error: function (xhr, desc, err)
					{
						console.log( err );
					},
					cache: false,
					contentType: false,
					processData: false,
			});
			event.preventDefault();
			return false;
		});

		var fileList = new Array;
		var i = 0;
		var file_up_names=[];
		$("div#dropzone").dropzone({
			url: "<?php echo site_url('condominium/add_upload'); ?>",
			addRemoveLinks : true,
			//previewsContainer: "#preview-gallery",
			dictRemoveFile : 'REMOVE',
			success : function(file, responseText) {
				$.ajax({
					url: "<?php echo site_url('condominium/add_upload'); ?>",
					type: "POST",
					data: { 'file_name': file.name},
					success: function(data)
					{
						//console.log( $(this).length );

						var count_img = $(this).length;
						$( "<input type='hidden' name='file_name_[]'>" ).val(file.name).appendTo( "div#file_name" );
						//console.log( $("input[name~='file_name[]']" ).val(file.name) );
					}
				});
			},
			removedfile: function(file)
			{
				x = confirm('Do you want to delete?');
				if(!x)  return false;

				$.ajax({
					url: "<?php echo site_url('condominium/canceled_upload'); ?>",
					type: "POST",
					data: { 'file_name': file.name},
					success: function(data)
					{

	//					if( $( "input[name^='file_name_[]']" ).val() == file.name )
	//					{
	//						alert(file.name);
	//					}
						$( "input[value='" + file.name+ "']" ).remove();
						//remove preview//
						$(document).find(file.previewElement).remove();
					}
				});
			}
		});
	//	Dropzone.options.myDropzone = false;

		Dropzone.autoDiscover = false;
		var unique = "file[]";
		var acceptedFileTypes = "image/*"; //dropzone requires this param be a comma separated list

		Dropzone.options.myAwesomeDropzone = {
			// your other settings as listed above
			maxFiles: 10, // allowing any more than this will stress a basic php/mysql stack
			uploadMultiple	: true,
			paramName: unique,
			headers: {"MyAppname-Service-Type": "Dropzone"},
			acceptedFiles: acceptedFileTypes,
		}
	});


		$(document).ready(function () {

			var provinces_id = $("#provinces_id").val();
			//var districts_id = $("#districts_id").val();
			if( provinces_id != 0 )
			{
				$.ajax({
					url: "<?php echo base_url(); ?>page/provinces_districts_relation",
					data: {
						id: provinces_id
					},
					type: "POST",
					success: function (data) {
						$("#districts_id").html(data);
						$("#districts_id").val(<?php echo $districts_id; ?>);

						var districts_id = $("#districts_id").val();
						//console.log(districts_id);

						$.ajax({
							url: "<?php echo base_url(); ?>page/districts_subdistricts_relation",
							data: {
								id: districts_id
							},
							type: "POST",
							success: function (data) {
								$("#sub_districts_id").html(data);
								$("#sub_districts_id").val(<?php echo $sub_districts_id; ?>);
							}
						});

					}
				});
			}
		});

		$("#provinces_id").change(function () {
			/*dropdown post */ //
			$.ajax({
				url: "<?php echo base_url(); ?>page/provinces_districts_relation",
				data: {
					id: $(this).val()
				},
				type: "POST",
				success: function (data) {
					$("#districts_id").html(data);
				}
			});
		});

		$("#districts_id").change(function () {
			/*dropdown post */ //
			$.ajax({
				url: "<?php echo base_url(); ?>page/districts_subdistricts_relation",
				data: {
					id: $(this).val()
				},
				type: "POST",
				success: function (data) {
					$("#sub_districts_id").html(data);
				}
			});
		});

		$("#transportation_category_id").change(function () {
			/*dropdown post */ //
			$.ajax({
				url: "<?php echo base_url(); ?>condominium/transportation_category_relation",
				data: {
					id: $(this).val()
				},
				type: "POST",
				success: function (data) {
					$("#transportation_id").html(data);
				}
			});
		});


	</script>
		<!--form wysiwig js-->
	<script src="<?php echo base_url()?>assets/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
		$(document).ready(function () {

			if ($(".wysiwigeditor").length > 0) {
				tinymce.init({
					selector: "textarea.wysiwigeditor",
					theme: "modern",
					entity_encoding:"raw",
					forced_root_block : " ",
					valid_elements :"a[href|target=_blank],strong,u,p,iframe[src|frameborder|style|scrolling|class|width|height|name|align]",
					extended_valid_elements : "iframe[src|frameborder|style|scrolling|class|width|height|name|align]",
					media_strict: false,
					paste_data_images: true,
					height: 300,
					plugins: [
										"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
										"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
										"save table contextmenu directionality emoticons template paste textcolor jbimages youtube"
								],
					toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages | print preview fullpage | forecolor backcolor emoticons | youtube",
					image_advtab: true,
					relative_urls: false,
					extended_valid_elements: "+iframe[src|width|height|name|align|class]",
					file_picker_callback: function(callback, value, meta) {
						if (meta.filetype == 'image') {
							$('#upload_wysiwigeditor, #upload_wysiwigeditor2').trigger('click');
							$('#upload_wysiwigeditor, #upload_wysiwigeditor2').on('change', function() {
								var file = this.files[0];
								var reader = new FileReader();
								reader.onload = function(e) {
									callback(e.target.result, {
										alt: ''
									});
								};
								reader.readAsDataURL(file);
							});
						}
					},
					style_formats: [
						{
							title: 'Bold text',
							inline: 'b'
					},
						{
							title: 'Red text',
							inline: 'span',
							styles: {
								color: '#ff0000'
							}
					},
						{
							title: 'Red header',
							block: 'h1',
							styles: {
								color: '#ff0000'
							}
					},
						{
							title: 'Example 1',
							inline: 'span',
							classes: 'example1'
					},
						{
							title: 'Example 2',
							inline: 'span',
							classes: 'example2'
					},
						{
							title: 'Table styles'
					},
						{
							title: 'Table row 1',
							selector: 'tr',
							classes: 'tablerow1'
					}
								]
				});
			}
		});

		$('.dropify').dropify({
			messages: {
				'default': 'Drag and drop a file here or click',
				'replace': 'Drag and drop or click to replace',
				'remove': 'Remove',
				'error': 'Ooops, something wrong appended.'
			},
			error: {
				'fileSize': 'The file size is too big (1M max).'
			}
		});

		jQuery('.datepicker-autoclose').datepicker({
			format: "dd/mm/yyyy",
			autoclose: true,
			todayHighlight: true
		});

		$('input#yearsoption').maxlength({
			alwaysShow: true,
			warningClass: "label label-success",
			limitReachedClass: "label label-danger",
			separator: ' out of ',
			preText: 'You typed ',
			postText: ' chars available.',
			validate: true
		});

		$('.zipcode, .inputmaxlength').maxlength({
			alwaysShow: true,
			warningClass: "label label-success",
			limitReachedClass: "label label-danger",
			separator: ' out of ',
			preText: 'You typed ',
			postText: ' chars available.',
			validate: true
		});


		$(".numeric_picker").TouchSpin({
			buttondown_class: "btn btn-primary",
			buttonup_class: "btn btn-primary"
		});
		$(".float_price").TouchSpin({
			min: 0,
			max: 1000000000,
			step: 50000,
			decimals: 2,
			boostat: 50000,
			maxboostedstep: 100000,
			buttondown_class: "btn btn-primary",
			buttonup_class: "btn btn-primary",
		});

		$(".numeric_price").TouchSpin({
			min: -1000000000,
			max: 1000000000,
			stepinterval: 50,
			buttondown_class: "btn btn-primary",
			buttonup_class: "btn btn-primary",
			maxboostedstep: 10000000,
			prefix: '฿'
		});

	</script>

	<script type="text/javascript">
		$(document).ready(function () {
			var maxField = 10; //Input fields increment limitation
			var addButton = $('.add_button'); //Add button selector
			var wrapper = $('.form-clone'); //Input field wrapper
			//var fieldHTML = '<div><input type="text" name="field_name[]" value=""/><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="remove-icon.png"/></a></div>'; //New input field html
			var fieldHTML = ''; //New input field html
			$.ajax({
				url	: "<?php echo base_url(); ?>page/transportation_category_option",
				data: {
					id: $(this).val()
				},
				type: "POST",
				success: function (data) {
					fieldHTML = data;
					//console.log(fieldHTML);
					var x = 1; //Initial field counter is 1
					$(addButton).click(function () { //Once add button is clicked
						if (x < maxField) { //Check maximum number of input fields
							x++; //Increment field counter
							$(wrapper).append(fieldHTML); // Add field html
						}
					});
					$(wrapper).on('click', '.remove_button', function (e) { //Once remove button is clicked
						//alert('sssss');
						e.preventDefault();
						$(this).closest('.row').remove(); //Remove field html
						x--; //Decrement field counter
					});
				}
			});
			//var fieldHTML = '<div><input type="text" name="field_name[]" value=""/><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="remove-icon.png"/></a></div>'; //New input field html




		});
	</script>
