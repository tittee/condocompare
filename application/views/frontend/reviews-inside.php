<?php
defined('BASEPATH') or exit();
/*HEADER*/
$this->load->view('partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'js' => isset($js) ? $js : array(), 'BodyClass' => ''));

$sfilepath = base_url().'uploads/reviews';

$reviews_id = $data->reviews_id;
$reviews_title = (!empty($data->reviews_title)) ? $data->reviews_title : '-';
$reviews_description = (!empty($data->reviews_description)) ? $this->primaryclass->decode_htmlspecialchars($data->reviews_description) : '-';

$fk_reviews_category_id = (!empty($data->fk_reviews_category_id)) ? $data->fk_reviews_category_id : '0';
$reviews_category_name = (!empty($fk_reviews_category_id) || $fk_reviews_category_id !== 0) ? $this->M->get_name('reviews_category_id, reviews_category_name', 'reviews_category_id', 'reviews_category_name', $fk_reviews_category_id, 'reviews_category') : '-';
$reviews_category_name_t = (!empty($reviews_category_name[0])) ? $reviews_category_name[0]->reviews_category_name : '-';

$visited = (!empty($data->visited)) ? $data->visited : '-';
$createdate = $this->dateclass->DateTimeFullFormat($data->createdate, 0, 1, 'Th');

?>
		<div class="container">
			<div class="bg-cs-gray-second">
				<div class="row align-middle">
					<div class="columns">
						<nav aria-label="You are here:" role="navigation">
							<?php echo $breadcrumbs; ?>
						</nav>
					</div>
				</div>
			</div>
			<div class="bg-cs-gray clearfix">
				<h3 class="text-center color-blue page-title"><?php echo $title; ?></h3>
			</div>
			<div class="row mar-top-large mar-bott-large align-middle">
				<div class="title small-12 medium-12 medium-centered column">

					<!--  ID : 20 = banner หน้ารายละเอียดรีวิว ด้านบน header -->
					<div id="" class="banner-carousel owl-carousel owl-theme">
					<?php
              $banner_20 = $this->Page_model->get_banner_location(20);
              $sfilepath_banner = base_url().'uploads/banner/';
              $banner_count = count($banner_20);

              // $banner_20_arr = array();
              for ($i = 0; $i < $banner_count;  ++$i) {
                  $banner_20_id = (!empty($banner_20[$i]->banner_id)) ? $banner_20[$i]->banner_id : '';

                  //UPDATE DISPLAY
                  $displayed = (!empty($banner_20[$i]->displayed)) ? $banner_20[$i]->displayed : '';
                  $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_20_id, 'banner');

                  $banner_20_title = (!empty($banner_20[$i]->banner_title)) ? $banner_20[$i]->banner_title : '';
                  $banner_20_url = (!empty($banner_20[$i]->banner_url)) ? $banner_20[$i]->banner_url : '#';
                  $banner_20_pic_thumb = (!empty($banner_20[$i]->pic_thumb)) ? $sfilepath_banner.$banner_20[$i]->pic_thumb : 'http://placehold.it/1400x175/333?text=1400x175';
                  // $banner_20_arr[] = '<a href="'.$banner_20_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_20_id.'"><img src="'.$banner_20_pic_thumb.'" alt="'.$banner_20_title.'" title=""></a>';
                  echo $banner_20_arr = '<div class="item"><a href="'.$banner_20_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_20_id.'"><img src="'.$banner_20_pic_thumb.'" alt="'.$banner_20_title.'" title=""></a></div>';
              }
              // shuffle($banner_20_arr);
              // echo (!empty($banner_20_arr[0])) ? $banner_20_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/1400x175/333?text=1400x175" alt="1400x175" title="1400x175"></a>';
          ?>
				</div>

				</div>
			</div>
			<!--NEWS SLIDER-->
			<div class="row clearfix">
				<!--LEFT-->
				<div class="small-12 large-9 column">
					<h2 class="title-red-black"><?php echo $reviews_title; ?></h2>
					<p><?php echo $createdate; ?></p>
          <!-- <p class="contact_name"><i class="fa fa-eye" style="color: #E9E9E9;"></i> จำนวนผู้ชม <?php echo $visited; ?> </p> -->
					<!--TITLE NEWS-->
					<div class="row clearfix">
						<div class="small-12 column">

<?php echo $reviews_description; ?>

						</div>
					</div>
				</div>
				<!--RIGTH-->
				<div class="small-12 large-3 column">
					<div class="bg-cs-blue well columns mar-bott-large">
						<form id="form_search" action="<?php echo site_url('reviews'); ?>" method="get">
						<div style="margin-bottom: 0;" class="input-group">
								<input type="text" name="keyword" placeholder="ค้นหารีวิว" class="input-group-field" required>
								<div class="input-group-button">
									<button name="submit" style="font-size: .85rem;" class="button button-red-white"><i class="fa fa-search"></i></button>
								</div>
								<input type="hidden" name="action" value="action">
						</div>
					</form>
					</div>

					<!--  ID : 22 = banner หน้ารายละเอียดรีวิว  ด้านบน ขวามือ -->
					<div id="" class="banner-carousel owl-carousel owl-theme">
					<?php
              $banner_22 = $this->Page_model->get_banner_location(22);
              $sfilepath_banner = base_url().'uploads/banner/';
              $banner_count = count($banner_22);

              // $banner_22_arr = array();
              for ($i = 0; $i < $banner_count;  ++$i) {
                  $banner_22_id = (!empty($banner_22[$i]->banner_id)) ? $banner_22[$i]->banner_id : '';

                  //UPDATE DISPLAY
                  $displayed = (!empty($banner_22[$i]->displayed)) ? $banner_22[$i]->displayed : '';
                  $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_22_id, 'banner');

                  $banner_22_title = (!empty($banner_22[$i]->banner_title)) ? $banner_22[$i]->banner_title : '';
                  $banner_22_url = (!empty($banner_22[$i]->banner_url)) ? $banner_22[$i]->banner_url : '#';
                  $banner_22_pic_thumb = (!empty($banner_22[$i]->pic_thumb)) ? $sfilepath_banner.$banner_22[$i]->pic_thumb : 'http://placehold.it/380x400/333?text=380x400';
                  // $banner_22_arr[] = '<a href="'.$banner_22_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_22_id.'"><img src="'.$banner_22_pic_thumb.'" alt="'.$banner_22_title.'" title=""></a>';
									echo $banner_22_arr = '<div class="item"><a href="'.$banner_22_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_22_id.'"><img src="'.$banner_22_pic_thumb.'" alt="'.$banner_22_title.'" title=""></a></div>';
              }
          ?>
				</div>

					<h2 class="title-blue-bg right">Most Read<span>	This Month</span></h2>
					<div class="inside">
						<article>
							<?php
                //LIST CONDO CATEGORY
                foreach ($data_mostread as $row) {
                    $reviews_id = $row->reviews_id;
                    $reviews_title = (!empty($row->reviews_title)) ? $row->reviews_title : '-';
                    $reviews_title_limit = character_limiter($reviews_title, 20);

                    $createdate = $row->createdate;
                    $createdate_t = (isset($createdate) || !empty($createdate)) ? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, 'Th') : 'ยังไม่มีข้อมูล';
                    ?>
							<div class="row clearfix news-sidebar">
								<div class="columns">
									<div class="well-gray">
										<h5><a href="<?php echo site_url('reviews-details/'.$reviews_id);
                                    ?>" class="color-red"><?php echo $reviews_title_limit;?></a></h5>
										<p class="mar-bott-zero date_news"><?php echo $createdate_t;
                                    ?></p>
									</div>
								</div>
							</div>

							<?php

                                }//END FOREACH ?>


						</article>
					</div>

				</div>
			</div>
			<div class="row clearfix">
				<div class="title small-12 medium-8 medium-centered column">

					<!--  ID : 21 = banner หน้ารายละเอียดรีวิว  ด้านบน footer -->
					<?php
						$banner_21 = $this->Page_model->get_banner_location(21);
						if( count($banner_21) > 0 ){
						?>
						<div class="row">
							<div class="small-12 columns">
					<div id="" class="banner-carousel owl-carousel owl-theme">
					<?php

                $sfilepath_banner = base_url().'uploads/banner/';
                $banner_count = count($banner_21);

                // $banner_21_arr = array();
                for ($i = 0; $i < $banner_count;  ++$i) {
                    $banner_21_id = (!empty($banner_21[$i]->banner_id)) ? $banner_21[$i]->banner_id : '';

                    //UPDATE DISPLAY
                    $displayed = (!empty($banner_21[$i]->displayed)) ? $banner_21[$i]->displayed : '';
                    $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_21_id, 'banner');

                    $banner_21_title = (!empty($banner_21[$i]->banner_title)) ? $banner_21[$i]->banner_title : '';
                    $banner_21_url = (!empty($banner_21[$i]->banner_url)) ? $banner_21[$i]->banner_url : '#';
                    $banner_21_pic_thumb = (!empty($banner_21[$i]->pic_thumb)) ? $sfilepath_banner.$banner_21[$i]->pic_thumb : 'http://placehold.it/1400x175/333?text=1400x175';
                    // $banner_21_arr[] = '<a href="'.$banner_21_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_21_id.'"><img src="'.$banner_21_pic_thumb.'" alt="'.$banner_21_title.'" title=""></a>';
                    echo $banner_21_arr = '<div class="item"><a href="'.$banner_21_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_21_id.'"><img src="'.$banner_21_pic_thumb.'" alt="'.$banner_21_title.'" title=""></a></div>';
                }
                // shuffle($banner_21_arr);
                // echo (!empty($banner_21_arr[0])) ? $banner_21_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/1400x175/333?text=1400x175" alt="1400x175" title="1400x175"></a>';
            ?>
					</div>
					</div>
					</div>
					<?php }//END IF ?>
				</div>
			</div>
			<div class="row clearfix"></div>
		</div>
<?php $this->load->view('partials/footer');?>
<?php
if (isset($slippry)) {
    ?>
<!--NEWS SLIDER : SLIPPRY-->
<script type="text/javascript">
	$(document).ready(function() {
		jQuery('#news-demo').slippry({
			// general elements & wrapper
			slippryWrapper: '<div class="sy-box news-slider" />', // wrapper to wrap everything, including pager
			elements: 'article', // elments cointaining slide content

			// options
			adaptiveHeight: false, // height of the sliders adapts to current
			captions: false,


			// transitions
			transition: 'horizontal', // fade, horizontal, kenburns, false
			speed: 1200,
			pause: 8000,

			// slideshow
			autoDirection: 'prev'
		});
	});
</script>
<?php

}//END IF SLIPPY ?>
