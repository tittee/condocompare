<?php
defined('BASEPATH') or exit();
/*HEADER*/
$this->load->view('partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'BodyClass' => ''));

$sfilepath = base_url().'uploads/condominium';
//---------------------------------------------
// SEARCH TABS : 1
$keyword = (!empty($this->input->get('search'))) ? $this->input->get('search') : '';
$min_price = (!empty($this->input->get('min_price'))) ? $this->input->get('min_price') : '';
$max_price = (!empty($this->input->get('max_price'))) ? $this->input->get('max_price') : '';
$type_offering = (!empty($this->input->get('type_offering'))) ? $this->input->get('type_offering') : '';
$type_suites = (!empty($this->input->get('type_suites'))) ? $this->input->get('type_suites') : '';
$condo_room = (!empty($this->input->get('condo_room'))) ? $this->input->get('condo_room') : '';
$condo_area_apartment = (!empty($this->input->get('condo_area_apartment'))) ? $this->input->get('condo_area_apartment') : '';
//---------------------------------------------
// SEARCH TABS : 2
$mass_transportation_category_id = (!empty($this->input->get('mass_transportation_category_id'))) ? $this->input->get('mass_transportation_category_id') : '';
$mass_transportation_id = (!empty($this->input->get('mass_transportation_id'))) ? $this->input->get('mass_transportation_id') : '';
$transportation_category = (!empty($this->input->get('transportation_category'))) ? $this->input->get('transportation_category') : '';
$bts = (!empty($this->input->get('bts'))) ? $this->input->get('bts') : '';
$mrt = (!empty($this->input->get('mrt'))) ? $this->input->get('mrt') : '';
$airportlink = (!empty($this->input->get('airportlink'))) ? $this->input->get('airportlink') : '';
//---------------------------------------------
// SEARCH TABS : 3
$type_zone_id = (!empty($this->input->get('type_zone_id'))) ? $this->input->get('type_zone_id') : '';
//---------------------------------------------
// SEARCH TABS :  4
$transportation_id = (!empty($this->input->get('transportation_id'))) ? $this->input->get('transportation_id') : '';
//---------------------------------------------
// SEARCH FOOTER :  1
$condo_owner_name = (!empty($this->input->get('condo_owner_name'))) ? $this->input->get('condo_owner_name') : '';

// SEARCH ORDER
$order_by_list = (!empty($this->input->get('order_by_list'))) ? $this->input->get('order_by_list') : '';
$order_by_price_visited = (!empty($this->input->get('order_by_price_visited'))) ? $this->input->get('order_by_price_visited') : '';
?>
<div class="container">
  <div class="bg-cs-gray-second">
    <div class="row align-middle">
      <div class="columns">
        <nav aria-label="You are here:" role="navigation">
          <?php echo $breadcrumbs; ?>
        </nav>
      </div>
    </div>
  </div>

  <div class="bg-cs-gray clearfix">
    <h3 class="text-center color-blue page-title"><?php echo $title; ?></h3>
  </div>
  <div class="row mar-top-large align-middle">
    <div class="title small-12 medium-12 medium-centered column">
      <!--  ID : 23 = banner ด้านบน header หน้ารายการคอนโด -->
      <div id="" class="banner-carousel owl-carousel owl-theme">
      <?php
        $banner_23 = $this->Page_model->get_banner_location(23);
        $sfilepath_banner = base_url().'uploads/banner/';
        $banner_count = count($banner_23);

        // $banner_23_arr = array();
        for ($i = 0; $i < $banner_count;  ++$i) {
            $banner_23_id = (!empty($banner_23[$i]->banner_id)) ? $banner_23[$i]->banner_id : '';

          //UPDATE DISPLAY
          $displayed = (!empty($banner_23[$i]->displayed)) ? $banner_23[$i]->displayed : '';
            $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_23_id, 'banner');

            $banner_23_title = (!empty($banner_23[$i]->banner_title)) ? $banner_23[$i]->banner_title : '';
            $banner_23_url = (!empty($banner_23[$i]->banner_url)) ? $banner_23[$i]->banner_url : '#';
            $banner_23_pic_thumb = (!empty($banner_23[$i]->pic_thumb)) ? $sfilepath_banner.$banner_23[$i]->pic_thumb : 'http://placehold.it/1400x175/333?text=1400x175';
            // $banner_23_arr[] = '<a href="'.$banner_23_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_23_id.'"><img src="'.$banner_23_pic_thumb.'" alt="'.$banner_23_title.'" title=""></a>';
            echo $banner_23_arr = '<div class="item"><a href="'.$banner_23_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_23_id.'"><img src="'.$banner_23_pic_thumb.'" alt="'.$banner_23_title.'" title=""></a></div>';
        }
        // shuffle($banner_23_arr);
        // echo (!empty($banner_23_arr[0])) ? $banner_23_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/1400x175/333?text=1400x175" alt="1400x175" title="1400x175"></a>';
      ?>
    </div>

    </div>
  </div>
</div>

		<div id="container">
			<div id="highlight">
				<div class="sub-container row">
					<div class="title mar-bott-medium column">
						<div class="row">
							<div class="small-6 medium-6 large-9 column">

								<h2 class="title-red-black"><?php echo (!empty($data['condo_total'])) ? $data['condo_total'] : '0'; ?><span>	รายการคอนโดมิเนียม</span></h2>
							</div>
							<div class="small-6 medium-6 large-3 column"><a href="<?php echo site_url('condocompare'); ?>" class="button"><i class="fa fa-file"></i>	เปรียบเทียบราคาคอนโด</a></div>
						</div>


					</div>
					<div class="small-12 medium-12 large-9 column hover">
						<div class="small-12 column well bg-cs-gray">
							<form class="from_sort" action="" method="get">
                <!-- SEARCH TAB -->
                <input type="hidden" name="search" value="<?php echo $keyword; ?>">
                <input type="hidden" name="min_price" value="<?php echo $min_price; ?>">
                <input type="hidden" name="max_price" value="<?php echo $max_price; ?>">
                <input type="hidden" name="type_offering" value="<?php echo $type_offering; ?>">
                <input type="hidden" name="type_suites" value="<?php echo $type_suites; ?>">
                <input type="hidden" name="condo_room" value="<?php echo $condo_room; ?>">
                <input type="hidden" name="condo_area_apartment" value="<?php echo $condo_area_apartment; ?>">
                <input type="hidden" name="mass_transportation_category_id" value="<?php echo $mass_transportation_category_id; ?>">
                <input type="hidden" name="mass_transportation_id" value="<?php echo $mass_transportation_id; ?>">
                <input type="hidden" name="transportation_category" value="<?php echo $transportation_category; ?>">
                <input type="hidden" name="bts" value="<?php echo $bts; ?>">
                <input type="hidden" name="mrt" value="<?php echo $mrt; ?>">
                <input type="hidden" name="airportlink" value="<?php echo $airportlink; ?>">
                <input type="hidden" name="type_zone_id" value="<?php echo $type_zone_id; ?>">
                <input type="hidden" name="transportation_id" value="<?php echo $transportation_id; ?>">
                <input type="hidden" name="condo_owner_name" value="<?php echo $condo_owner_name; ?>">
                <!-- SORT & ORDER -->
                <input type="hidden" name="order_by_list" value="<?php echo $order_by_list; ?>">
                <input type="hidden" name="order_by_price_visited" value="<?php echo $order_by_price_visited; ?>">
							<div class="row">
								<div class="small-12 medium-12 large-8 columns">
									<!-- <div class="small-12 medium-6 columns">
										<select name="order_by_list">
											<option value="5" <?php echo ($order_by_list === '') ? 'selected' : ''; ?>>5 ประกาศก่อนหน้า</option>
											<option value="10" <?php echo ($order_by_list === '10') ? 'selected' : ''; ?>>10 ประกาศก่อนหน้า</option>
											<option value="20" <?php echo ($order_by_list === '20') ? 'selected' : ''; ?>>20 ประกาศก่อนหน้า</option>
											<option value="30" <?php echo ($order_by_list === '30') ? 'selected' : ''; ?>>30 ประกาศก่อนหน้า</option>
										</select>
									</div> -->

									<div class="small-12 medium-6 columns">
										<select name="order_by_price_visited" class="">
											<option value="">จัดเรียงตาม</option>
											<option value="1" <?php echo ($order_by_price_visited === '1') ? 'selected' : ''; ?>>ราคามากไปน้อย</option>
											<option value="2" <?php echo ($order_by_price_visited === '2') ? 'selected' : ''; ?>>ราคาน้อยไปมาก</option>
											<option value="3" <?php echo ($order_by_price_visited === '3') ? 'selected' : ''; ?>>เข้าชมเยอะที่สุด</option>
										</select>
									</div>

								</div>
								<div class="small-12 medium-12 large-4 columns">
									<div class="small-12 medium-12 large-6 columns">
										<!-- <a href="#" class="alert hollow button no-bg expanded"><i class="fa fa-bars">	รายการ</i></a> -->
										<button type="submit" name="button_submit" class="alert hollow button no-bg expanded"><i class="fa fa-search"></i> ค้นหา</button>
									</div>
									<!-- <div class="small-12 medium-6 columns"><a href="#" class="secondary hollow button no-bg expanded"><i class="fa fa-map-marker">	รายการ</i></a></div> -->
									<input type="hidden" name="action_sort" value="action_sort">
								</div>
							</div>
							</form>
						</div>
						<!-- END FILTER -->

						<div class="title small-12 column hover">
							<div class="row">

								<?php
                                    //LIST CONDO
                                    $i = 0;
                                    //$sel = '';
                                    if (!empty($data['condo'])) {
                                        while ($row = $data['condo']->unbuffered_row()) {
                                            $condo_id = (!empty($row->condo_id)) ? $row->condo_id : 0;
                                            $condo_title = (!empty($row->condo_title)) ? $row->condo_title : 'ไม่ระบุ';
                                            $visited = (!empty($row->visited)) ? $row->visited : 0;

                                            $condo_road_alley = $this->primaryclass->condo_road_soi($row->condo_road, $row->condo_alley);
                                            // $this->Configsystem_model->get_type_zone_title($row->zone_id);

                                            $createdate = $row->createdate;
                                            $createdate_t = (isset($createdate) || !empty($createdate)) ? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, 'Th') : 'ยังไม่มีข้อมูล';

                                            $condo_total_price = ($row->condo_total_price !== '0.00') ? $row->condo_total_price : '0.00';
                                            $condo_total_price_bath = ($condo_total_price !== '0.00') ? number_format($condo_total_price, 0, '.', ',').' บาท' : 'ไม่ระบุ';


                                            $condo_area_apartment = ($row->condo_area_apartment !== '0.00') ? $row->condo_area_apartment : '0.00';
                                            $condo_area_apartment_meter = ($row->condo_area_apartment !== '0.00') ? $row->condo_area_apartment.' ตร.ม.' : 'ไม่ระบุ';


                                            $condo_price_per_square_meter = ($row->condo_price_per_square_meter !== '0.00') ? $row->condo_price_per_square_meter : '0.00';
                                            $condo_price_per_square_meter_bath = ($condo_price_per_square_meter !== '0.00') ? number_format($condo_price_per_square_meter, 0, '.', ',').' บาท' : 'ไม่ระบุ';


                                            $msg_price_per_meter = $this->primaryclass->get_price_per_square(1, $condo_total_price, $condo_area_apartment, $condo_price_per_square_meter);

                                            $condo_all_room = (!empty($row->condo_all_room)) ? $row->condo_all_room : '-';
                                            $condo_all_toilet = (!empty($row->condo_all_toilet)) ? $row->condo_all_toilet : '-';

                                            $type_offering_id = (!empty($row->type_offering_id)) ? $row->type_offering_id : '';
                                            if ($type_offering_id !== '' || $type_offering_id !== 0) {
                                                $type_offering = $this->Page_model->get_offering($type_offering_id);
                                                $type_offering_img = base_url().'uploads/config/'.$type_offering->type_offering_images;
                                            } else {
                                                $type_offering_img = base_url().'assets/images/sell1.png';
                                            }

                                            $pic_thumb = $sfilepath.'/'.$row->pic_thumb;

                                            $query2 = $this->db
                                                ->select('condo_id, sess_id')
                                                ->where('condo_id', $condo_id)
                                                ->where('sess_id', $_SESSION['sess_id'])
                                                ->get('compare_condo');

                                            $row_relation = $query2->row_array();
                                            $fk_condo_id = $row_relation['condo_id'];
                                            $sel = ($condo_id ==  $fk_condo_id) ? "style='color: green;'" : '';

                                            ?>
								<div class=" display condo-list small-12 large-12 column">
									<figure class="small-12 medium-12 large-4 column">
										<span><img src="<?php echo $type_offering_img;?>" alt="" /></span>
										<a href="<?php echo site_url('condominium-details/'.$condo_id); ?>"><img src="<?php echo $pic_thumb;?>" alt=""></a>
									</figure>
									<div class="small-12 medium-6 large-5 column">
										<h2><a href="<?php echo site_url('condominium-details/'.$condo_id);?>"><?php echo $condo_title;
                                            ?></a></h2>
										<p class="location_address"><?php echo $condo_road_alley;
                                            ?></p>
										<table class="details">
											<thead>
												<th>ห้องนอน</th>
												<th>ห้องน้ำ</th>
												<th>พื้นที่ใช้สอย</th>
											</thead>
											<tbody>
												<td><?php echo $condo_all_room;
                                            ?></td>
												<td><?php echo $condo_all_toilet;
                                            ?></td>
												<td><?php echo $condo_area_apartment_meter;
                                            ?></td>
											</tbody>
										</table>
										<p class="contact_name"><?php //echo $this->User_model->get_staff_fullname($row->staff_id); ?> ประกาศเมื่อ
										<?php echo $createdate_t;
                                            ?> </p>
										<p class="contact_name"><i class="fa fa-eye" style="color: #E9E9E9;"></i> จำนวนผู้ชม <?php echo $visited;?> </p>
									</div>
									<div class="small-12 medium-6 large-3 column">
										<p class="price"><?php echo $condo_total_price_bath;?></p>
										<p><?php echo $msg_price_per_meter;?> </p>
										<!-- <p>จำนวน <?php //echo number_format($row->condo_total_price, 0, '.', ','); ?> บาท </p> -->
										<ul class="inline-list">
											<!-- <li>
												<span data-tooltip aria-haspopup="true" class="has-tip right" data-disable-hover="false" tabindex="3" title="ติดต่อตัวแทนเกี่ยวกับอสังหาริมทรัพย์นี้">
													<a href="<?php echo site_url('condominium-details/'.$condo_id);
                                            ?>"><i class="fa fa-phone"></i></a>
												</span>
											</li> -->
											<!--<li><a href="#"><i class="fa fa-comment"></i></a></li>-->
											<?php if (isset($_SESSION['mTID'])) {
    ?>
												<?php
                                                $query3 = $this->db
                                                    ->select('condo_id, member_id')
                                                    ->where('condo_id', $condo_id)
                                                    ->where('member_id', $_SESSION['mID'])
                                                    ->get('wishlist_condo');
                                                // if ($data->num_rows() > 0)
                                                // {
                                                    $row_wishlist = $query3->row_array();
    $fk_condo_id_wishlist = $row_wishlist['condo_id'];
    $sel_wishlist = ($condo_id ==  $fk_condo_id_wishlist) ? "style='color: green;'" : '';

    ?>

											<li>
												<span data-tooltip aria-haspopup="true" class="has-tip right" data-disable-hover="false" tabindex="3" title="เก็บไว้ดูภายหลัง">
													<a href="#" onclick="javascript:(0);" class="hob-wishlist" id="<?php echo $condo_id;
    ?>"><i class="fa fa-heart" <?php echo $sel_wishlist;
    ?> ></i></a>
												</span>
											</li>

											<?php
                    // } else {
                      ?>
											<!-- <li>
												<span data-tooltip aria-haspopup="true" class="has-tip right" data-disable-hover="false" tabindex="3" title="กรุณาล็อกอิน">
													<a href="#" onclick="javascript:(0);" class="" id="<?php echo $condo_id;
    ?>"><i class="fa fa-warning "></i></a>
												</span>
												</li> -->
											<?php }
                      // ENDIF
                      ?>

											<?php if (isset($_SESSION['mTID'])) {
    ?>
												<li>
													<span data-tooltip aria-haspopup="true" class="has-tip right" data-disable-hover="false" tabindex="3" title="เปรียบเทียบ">
														<a href="#" onclick="javascript:(0);" class="hob-compare" id="<?php echo $condo_id;
    ?>"><i class="fa fa-file"<?php echo $sel;
    ?> ></i></a>
													</span>
												</li>
											<?php
                    // } else {
                      ?>
											<!-- <li>
												<span data-tooltip aria-haspopup="true" class="has-tip right" data-disable-hover="false" tabindex="3" title="กรุณาล็อกอิน">
													<a href="#" onclick="javascript:(0);" class="" id="<?php echo $condo_id;
    ?>"><i class="fa fa-warning "></i></a>
												</span>
												</li> -->

                        <?php }
                        // ENDIF
                        ?>
										</ul>
									</div>
								</div>
								<?php

                                        }
                                        ++$i;
                                    }
                                ?>
							</div>
						</div>
						<div class="column text-center">
							<?php echo $links; ?>
							<?php //echo $links; ?>
						</div>
						<div class="clearfix"></div>

						<!--  ID : 5 = banner ด้านล่าง หน้ารายการคอนโด -->
            <div class="column banner">
              <div id="" class="banner-carousel owl-carousel owl-theme">
            <?php
              $banner_5 = $this->Page_model->get_banner_location(5);
              $sfilepath_banner = base_url().'uploads/banner/';
              $banner_count = count($banner_5);

              // $banner_5_arr = array();
              for ($i = 0; $i < $banner_count;  ++$i) {
                  $banner_5_id = (!empty($banner_5[$i]->banner_id)) ? $banner_5[$i]->banner_id : '';

                //UPDATE DISPLAY
                $displayed = (!empty($banner_5[$i]->displayed)) ? $banner_5[$i]->displayed : '';
                  $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_5_id, 'banner');

                  $banner_5_title = (!empty($banner_5[$i]->banner_title)) ? $banner_5[$i]->banner_title : '';
                  $banner_5_url = (!empty($banner_5[$i]->banner_url)) ? $banner_5[$i]->banner_url : '#';
                  $banner_5_pic_thumb = (!empty($banner_5[$i]->pic_thumb)) ? $sfilepath_banner.$banner_5[$i]->pic_thumb : 'http://placehold.it/1400x175/333?text=1400x175';
                  // $banner_5_arr[] = '<a href="'.$banner_5_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_5_id.'"><img src="'.$banner_5_pic_thumb.'" alt="'.$banner_5_title.'" title=""></a>';
                  echo $banner_5_arr = '<div class="item"><a href="'.$banner_5_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_5_id.'"><img src="'.$banner_5_pic_thumb.'" alt="'.$banner_5_title.'" title=""></a></div>';
              }
              // shuffle($banner_5_arr);
              // echo (!empty($banner_5_arr[0])) ? $banner_5_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/1400x175/333?text=1400x175" alt="1400x175" title="1400x175"></a>';
            ?>
						</div>
						</div>
					</div>
					<div class="title small-12 medium-12 large-3 column">

						<!--  ID : 4 = banner ขวามือด้านบน หน้ารายการคอนโด  -->
            <div id="" class="banner-carousel owl-carousel owl-theme">
            <?php
              $banner_4 = $this->Page_model->get_banner_location(4);
              $sfilepath_banner = base_url().'uploads/banner/';
              $banner_count = count($banner_4);

              // $banner_4_arr = array();
              for ($i = 0; $i < $banner_count;  ++$i) {
                  $banner_4_id = (!empty($banner_4[$i]->banner_id)) ? $banner_4[$i]->banner_id : '';

                //UPDATE DISPLAY
                $displayed = (!empty($banner_4[$i]->displayed)) ? $banner_4[$i]->displayed : '';
                  $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_4_id, 'banner');

                  $banner_4_title = (!empty($banner_4[$i]->banner_title)) ? $banner_4[$i]->banner_title : '';
                  $banner_4_url = (!empty($banner_4[$i]->banner_url)) ? $banner_4[$i]->banner_url : '#';
                  $banner_4_pic_thumb = (!empty($banner_4[$i]->pic_thumb)) ? $sfilepath_banner.$banner_4[$i]->pic_thumb : 'http://placehold.it/370x540/333?text=370x540';
                  // $banner_4_arr[] = '<a href="'.$banner_4_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_4_id.'"><img src="'.$banner_4_pic_thumb.'" alt="'.$banner_4_title.'" title=""></a>';
                  echo $banner_4_arr = '<div class="item"><a href="'.$banner_4_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_4_id.'"><img src="'.$banner_4_pic_thumb.'" alt="'.$banner_4_title.'" title=""></a></div>';
              }
              // shuffle($banner_4_arr);
              // echo (!empty($banner_4_arr[0])) ? $banner_4_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/370x540/333?text=370x540" alt="370x540" title="370x540"></a>';
            ?>
            </div>

						<h2 class="title-blue-bg right">คอนโดยอดนิยม<span>ประจำสัปดาห์</span></h2>
						<div class="inside">
							<article>
									<div class="highlight-sidebar">
									<?php
                                        #SIDEBAR CONDO
                    if (!empty($condo_sidebar)) {
                        foreach ($condo_sidebar as $row) {
                            $condo_id = (!empty($row->condo_id)) ? $row->condo_id : 0;
                            $condo_title = (!empty($row->condo_title)) ? $row->condo_title : 'ไม่ระบุ';
                            $pic_thumb = (!empty($row->pic_thumb)) ? $sfilepath.'/'.$row->pic_thumb : 'http://placehold.it/150x150';
                            $condo_road_alley = $this->primaryclass->condo_road_soi($row->condo_road, $row->condo_alley);
                                            // $type_zone_title = $row->type_zone_title;
                                        ?>


										<div class="content-sidebar hover-eff bg-cs-gray">
											<figure>
												<div class="bg-cs-gray">
													<div class="small-12 large-6 columns">
														<p><a href="<?php echo site_url('condominium-details/'.$condo_id);
                            ?>"><img src="<?php echo $pic_thumb;
                            ?>" alt="<?php echo $condo_title;
                            ?>"></a></p>
													</div>
													<div class="small-12 large-6 columns highlight-caption">
														<h3><a href="<?php echo site_url('condominium-details/'.$condo_id);
                            ?>"><?php echo $condo_title;
                            ?></a></h3>
														<p><?php echo $condo_road_alley;
                            ?></p>
													</div>
												</div>
												<div class="clearfix"></div>
											</figure>
										</div>
										<?php

                        }//END FOREACH
                    }//END IF CONDO SIDEBAR
                                        ?>

									</div>
									<!-- <div class="columns"><a href="<?php echo site_url('news'); ?>" class="button small-12">ดูข่าวและบทความทั้งหมด</a></div> -->
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>



<?php $this->load->view('partials/footer');?>

<script>
$(document).ready(function () {

	$(".hob-wishlist").click(function (event) {
		var condo_id = $(this).attr("id");
		$.ajax({
			url: "<?php echo base_url(); ?>page/wishlist_condo",
			type: "POST",
			data: {
				condo_id : condo_id,
			},
			//async: false,
			success: function (data, status)
			{
				//console.log(data);
				if(data == 'insert'){
					$( "#"+condo_id+".hob-wishlist" ).children().css('color', 'green');
				}else if(data == 'delete'){
					$( "#"+condo_id+".hob-wishlist" ).children().css('color', '');
				}

			},
			error: function (xhr, desc, err)
			{
				console.log( err );
			},
		});
		event.preventDefault();
		return false;
	});

	$(".hob-compare").click(function (event) {
		//alert('ssss');
		var condo_id = $(this).attr("id");
		$.ajax({
			url: "<?php echo base_url(); ?>page/compare_condo",
			type: "POST",
			data: {
				condo_id : $(this).attr("id"),
			},
			success: function (data, status)
			{
				//console.log(data);
				if(data == 'insert'){
					$( "#"+condo_id+".hob-compare" ).children().css('color', 'green');
				}else if(data == 'delete'){
					$( "#"+condo_id+".hob-compare" ).children().css('color', '');
				}
			},
			error: function (xhr, desc, err)
			{
				console.log( err );
			},
		});
		event.preventDefault();
		return false;
	});


});

</script>
