<?php
defined('BASEPATH') or exit();
/*HEADER*/
$this->load->view('partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'js' => isset($js) ? $js : array(), 'BodyClass' => ''));

$sfilepath = base_url().'uploads/reviews';
$keyword = (!empty($this->input->get('keyword'))) ? $this->input->get('keyword') : '';
?>
		<div class="container">
			<div class="bg-cs-gray-second">
				<div class="row align-middle">
					<div class="columns">
						<nav aria-label="You are here:" role="navigation">
							<?php echo $breadcrumbs; ?>
						</nav>
					</div>
				</div>
			</div>
			<div class="bg-cs-gray clearfix">
				<h3 class="text-center color-blue page-title"><?php echo $title; ?></h3>
			</div>
			<div class="row mar-top-large mar-bott-large align-middle">
				<div class="title small-12 medium-12 medium-centered column">
					<!--  ID : 17 = banner หน้ารีวิว  ด้านบน header  -->
					<div id="" class="banner-carousel owl-carousel owl-theme">
					<?php
              $banner_17 = $this->Page_model->get_banner_location(17);
              $sfilepath_banner = base_url().'uploads/banner/';
              $banner_count = count($banner_17);

              // $banner_17_arr = array();
              for ($i = 0; $i < $banner_count;  ++$i) {
                  $banner_17_id = (!empty($banner_17[$i]->banner_id)) ? $banner_17[$i]->banner_id : '';

                  //UPDATE DISPLAY
                  $displayed = (!empty($banner_17[$i]->displayed)) ? $banner_17[$i]->displayed : '';
                  $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_17_id, 'banner');

                  $banner_17_title = (!empty($banner_17[$i]->banner_title)) ? $banner_17[$i]->banner_title : '';
                  $banner_17_url = (!empty($banner_17[$i]->banner_url)) ? $banner_17[$i]->banner_url : '#';
                  $banner_17_pic_thumb = (!empty($banner_17[$i]->pic_thumb)) ? $sfilepath_banner.$banner_17[$i]->pic_thumb : 'http://placehold.it/1400x175/333?text=1400x175';
                  // $banner_17_arr[] = '<a href="'.$banner_17_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_17_id.'"><img src="'.$banner_17_pic_thumb.'" alt="'.$banner_17_title.'" title=""></a>';
                  echo $banner_17_arr = '<div class="item"><a href="'.$banner_17_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_17_id.'"><img src="'.$banner_17_pic_thumb.'" alt="'.$banner_17_title.'" title=""></a></div>';
              }

          ?>

          </div>

				</div>
			</div>
			<!--reviews SLIDER-->
			<div class="row clearfix">
				<!--LEFT-->
				<div class="small-12 large-9 column">
					<section>
						<h2 class="title-red-black"><?php echo $data['reviews_total']; ?><span> รีวิวโครงการ</span></h2>
					</section>
					<!--TITLE reviews-->
          <div class="row clearfix">
						<div class="medium-12 columns">
							<?php
              if (!empty($keyword)) {
                  echo '<h2 class="title-blue-bg mar-top-large right">คำค้นหา : '.$keyword.'</h2>';
              }
              ?>
							<input type="hidden" name="keyword" value="<?php echo $keyword; ?>">

              <ul id=""  class="tabs bg-cs-gray tabs-category">
								<?php
                //LIST CONDO CATEGORY
                $i = 0;

                $rows_data = count($data_category);
                foreach ($data_category as $row_category) {
									$category = (!empty($this->uri->segment(2))) ? $this->uri->segment(2) : '' ;
                    $reviews_category_id = $row_category->reviews_category_id;
                    $reviews_category_name = $row_category->reviews_category_name;
										$reviews_url = site_url('reviews');
										$reviews_category_url = site_url('reviews-category/'.$reviews_category_id);
										$is_active = ($category === $reviews_category_id && $category !== '') ? 'is-active' : '';

                    ?>
										<?php if ( $i === 0 ): ?>
											<li class="tabs-title news-tabs ">
												<h4><a href="<?php echo $reviews_url; ?>" aria-selected="true">ทั้งหมด</a></h4>
											</li>
										<?php endif; ?>
								<li class="tabs-title news-tabs <?php echo $is_active;?>">
									<h4><a href="<?php echo $reviews_category_url; ?>" aria-selected="true"><?php echo $reviews_category_name; ?></a></h4>
								</li>
								<?php
								++$i;
							} //END FOREACH CATEGORY
							?>
							</ul>

							<div class="tabs-content tabs-category-content">
							<?php
                //LIST CONDO CATEGORY
                $j = 0;
								?>
								<div id="" class="">
									<?php
									foreach ($data['reviews'] as $row) {
	                  //echo $j;
	                  $reviews_id = $row->reviews_id;
	                  $reviews_title = (!empty($row->reviews_title)) ? $row->reviews_title : 'ไม่ระบุหัวข้อ';
	                  $reviews_caption = (!empty($row->reviews_caption)) ? $row->reviews_caption : '-';

	                  $fk_reviews_category_id = (!empty($row->fk_reviews_category_id)) ? $row->fk_reviews_category_id : '0';
	                  $reviews_category_name = (!empty($fk_reviews_category_id) || $fk_reviews_category_id !== 0) ? $this->M->get_name('reviews_category_id, reviews_category_name', 'reviews_category_id', 'reviews_category_name', $fk_reviews_category_id, 'reviews_category') : '-';
	                  $reviews_category_name_t = (!empty($reviews_category_name[0])) ? $reviews_category_name[0]->reviews_category_name : '-';

	                  $visited = (!empty($row->visited)) ? $row->visited : '-';
	                  $pic_thumb = $sfilepath.'/'.$row->reviews_pic_thumb;
									?>

									<div class="row border-bottom mar-top-medium">
										<div class="small-12 large-4 columns hover"><a href="<?php echo site_url('reviews-details/'.$reviews_id); ?>"><img src="<?php echo $pic_thumb; ?>" alt=""></a></div>
										<div class="small-12 large-8 columns">
											<h4><a href="<?php echo site_url('reviews-details/'.$reviews_id); ?>" class="color-red grow"><?php echo $reviews_title; ?></a></h4>
											<p class="date-reviews"><?php echo $reviews_category_name[0]->reviews_category_name; ?> - <?php echo $this->dateclass->DateTimeFullFormat($row->createdate, 0, 1, 'Th'); ?> </p>
											<p class="caption-content"><a href="<?php echo site_url('reviews-details/'.$reviews_id); ?>" class="grow"><?php echo $reviews_caption; ?></a></p>
											<p class="mar-top-medium"><a href="<?php echo site_url('reviews-details/'.$reviews_id); ?>" class="readmore-red">รายละเอียดเพิ่มเติม</a></p>
										</div>
									</div>
									<?php
								} // !! END FOREACH ?>
								</div>
							</div>

						</div>

					</div>
          <div class=" clearfix mar-top-large">
						<div class="column text-center">
							<?php echo $data['links']; ?>
						</div>
						<div class="clearfix"></div>
					</div>

				</div>
				<!--RIGTH-->
				<div class="small-12 large-3 column">
					<div class="bg-cs-blue well columns mar-bott-large">
						<form id="form_search" action="<?php echo site_url('reviews'); ?>" method="get">
						<div style="margin-bottom: 0;" class="input-group">
								<input type="text" name="keyword" placeholder="ค้นหารีวิว" class="input-group-field" required>
								<div class="input-group-button">
									<button name="submit" style="font-size: .85rem;" class="button button-red-white"><i class="fa fa-search"></i></button>
								</div>
								<input type="hidden" name="action" value="action">
						</div>
					</form>
					</div>

					<!--  ID : 19 = banner หน้ารีวิว ด้านบน ขวามือ -->
					<?php
						$banner_19 = $this->Page_model->get_banner_location(19);
						if( count($banner_19) > 0 ){
						?>
						<div class="row">
							<div class="small-12 columns">
								<div id="" class="banner-carousel owl-carousel owl-theme">
					<?php

              $sfilepath_banner = base_url().'uploads/banner/';
              $banner_count = count($banner_19);

              // $banner_19_arr = array();
              for ($i = 0; $i < $banner_count;  ++$i) {
                  $banner_19_id = (!empty($banner_19[$i]->banner_id)) ? $banner_19[$i]->banner_id : '';

                  //UPDATE DISPLAY
                  $displayed = (!empty($banner_19[$i]->displayed)) ? $banner_19[$i]->displayed : '';
                  $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_19_id, 'banner');

                  $banner_19_title = (!empty($banner_19[$i]->banner_title)) ? $banner_19[$i]->banner_title : '';
                  $banner_19_url = (!empty($banner_19[$i]->banner_url)) ? $banner_19[$i]->banner_url : '#';
                  $banner_19_pic_thumb = (!empty($banner_19[$i]->pic_thumb)) ? $sfilepath_banner.$banner_19[$i]->pic_thumb : 'http://placehold.it/380x400/333?text=380x400';
                  // $banner_19_arr[] = '<a href="'.$banner_19_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_19_id.'"><img src="'.$banner_19_pic_thumb.'" alt="'.$banner_19_title.'" title=""></a>';
                  echo $banner_19_arr = '<div class="item"><a href="'.$banner_19_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_19_id.'"><img src="'.$banner_19_pic_thumb.'" alt="'.$banner_19_title.'" title=""></a></div>';
              }
              // shuffle($banner_19_arr);
              // echo (!empty($banner_19_arr[0])) ? $banner_19_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/380x400/333?text=380x400" alt="380x400" title="380x400"></a>';
          ?>
							</div>
						</div>
					</div>
					<?php }//END IF ?>

					<h2 class="title-blue-bg right">Most Read<span>	This Month</span></h2>
					<div class="inside">
						<article>
							<?php
                                //LIST CONDO CATEGORY
                                foreach ($data_mostread as $row) {
                                    $reviews_id = $row->reviews_id;
                                    $reviews_title = (!empty($row->reviews_title)) ? $row->reviews_title : '-';
                                    $reviews_title_limit = character_limiter($reviews_title, 20);

                                    $createdate = $row->createdate;
                                    $createdate_t = (isset($createdate) || !empty($createdate)) ? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, 'Th') : 'ยังไม่มีข้อมูล';
                                    ?>
							<div class="row clearfix reviews-sidebar">
								<div class="columns">
									<div class="well-gray">
										<h5><a href="<?php echo site_url('reviews-details/'.$reviews_id);
                                    ?>" class="color-red"><?php echo $reviews_title_limit;?></a></h5>
										<p class="mar-bott-zero date_reviews"><?php echo $createdate_t;
                                    ?></p>
									</div>
								</div>
							</div>

							<?php

                                }//END FOREACH ?>


						</article>
					</div>
					<!-- <h2 class="title-blue-bg right">Lastest<span>	Comments</span></h2> -->
					<!-- <div class="inside">
						<article>
							<div class="well-list border-all clearfix">
								<div class="row">
									<div class="small-12 medium-5 columns">
										<p><a href="#"><img src="http://placehold.it/400x400&amp;text=Who/333" alt=""></a></p>
									</div>
									<div class="small-12 medium-7 columns highlight-caption">
										<h4 class="f_size1"><a href="#" class="color-red">หัวข้อคอมเม้นท์จาก บทความ</a></h4>
										<p class="mar-bott-zero">เนื้อหาคอมเม้นท์จาก บทความบทความ</p>
									</div>
								</div>
							</div>
							<div class="well-list border-all clearfix">
								<div class="row">
									<div class="small-12 medium-5 columns">
										<p><a href="#"><img src="http://placehold.it/400x400&amp;text=Who/333" alt=""></a></p>
									</div>
									<div class="small-12 medium-7 columns highlight-caption">
										<h4 class="f_size1"><a href="#" class="color-red">หัวข้อคอมเม้นท์จาก บทความ</a></h4>
										<p class="mar-bott-zero">เนื้อหาคอมเม้นท์จาก บทความบทความ</p>
									</div>
								</div>
							</div>
							<div class="well-list border-all clearfix">
								<div class="row">
									<div class="small-12 medium-5 columns">
										<p><a href="#"><img src="http://placehold.it/400x400&amp;text=Who/333" alt=""></a></p>
									</div>
									<div class="small-12 medium-7 columns highlight-caption">
										<h4 class="f_size1"><a href="#" class="color-red">หัวข้อคอมเม้นท์จาก บทความ</a></h4>
										<p class="mar-bott-zero">เนื้อหาคอมเม้นท์จาก บทความบทความ</p>
									</div>
								</div>
							</div>
						</article>
					</div> -->
					<!-- <h2 class="title-blue-red right clearfix mar-top-large">Popular<span>	Tags</span></h2>
					<div class="row">
						<div class="columns"><a href="#" class="button tag-gray-bg">ข่าวอสังหาริมทริพย์</a><a href="#" class="button tag-gray-bg">ข่าวดวง</a><a href="#" class="button tag-gray-bg">ข่าวท่ีดิน</a><a href="#" class="button tag-gray-bg">ข่าวเศรษฐกิจ</a><a href="#" class="button tag-gray-bg">ข่าวการเงิน</a><a href="#" class="button tag-gray-bg">ผ่อนบ้าน</a><a href="#" class="button tag-gray-bg">ภาษีหัก ณ ที่จ่าย</a></div>
					</div> -->
				</div>
			</div>
			<div class="row clearfix">
				<div class="title small-12 medium-8 medium-centered column">
					<!--  ID : 18 = banner หน้ารีวิว ด้านบน footer -->
					<?php
						$banner_18 = $this->Page_model->get_banner_location(18);
						if( count($banner_18) > 0 ){
						?>
					<div id="" class="banner-carousel owl-carousel owl-theme">
					<?php
              $sfilepath_banner = base_url().'uploads/banner/';
              $banner_count = count($banner_18);

              // $banner_18_arr = array();
              for ($i = 0; $i < $banner_count;  ++$i) {
                  $banner_18_id = (!empty($banner_18[$i]->banner_id)) ? $banner_18[$i]->banner_id : '';

                  //UPDATE DISPLAY
                  $displayed = (!empty($banner_18[$i]->displayed)) ? $banner_18[$i]->displayed : '';
                  $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_18_id, 'banner');

                  $banner_18_title = (!empty($banner_18[$i]->banner_title)) ? $banner_18[$i]->banner_title : '';
                  $banner_18_url = (!empty($banner_18[$i]->banner_url)) ? $banner_18[$i]->banner_url : '#';
                  $banner_18_pic_thumb = (!empty($banner_18[$i]->pic_thumb)) ? $sfilepath_banner.$banner_18[$i]->pic_thumb : 'http://placehold.it/1400x175/333?text=1400x175';
                  // $banner_18_arr[] = '<a href="'.$banner_18_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_18_id.'"><img src="'.$banner_18_pic_thumb.'" alt="'.$banner_18_title.'" title=""></a>';
                  echo $banner_18_arr = '<div class="item"><a href="'.$banner_18_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_18_id.'"><img src="'.$banner_18_pic_thumb.'" alt="'.$banner_18_title.'" title=""></a></div>';
              }
              // shuffle($banner_18_arr);
              // echo (!empty($banner_18_arr[0])) ? $banner_18_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/1400x175/333?text=1400x175" alt="1400x175" title="1400x175"></a>';
          ?>
					</div>
					<?php }//END IF ?>
				</div>
			</div>
			<div class="row clearfix"></div>
		</div>
<?php $this->load->view('partials/footer');?>
<?php
if (isset($slippry)) {
    ?>
<!--reviews SLIDER : SLIPPRY-->
<script type="text/javascript">
	$(document).ready(function() {
		jQuery('#reviews-demo').slippry({
			// general elements & wrapper
			slippryWrapper: '<div class="sy-box reviews-slider" />', // wrapper to wrap everything, including pager
			elements: 'article', // elments cointaining slide content

			// options
			adaptiveHeight: false, // height of the sliders adapts to current
			captions: false,


			// transitions
			transition: 'horizontal', // fade, horizontal, kenburns, false
			speed: 1200,
			pause: 8000,

			// slideshow
			autoDirection: 'prev'
		});
	});
</script>
<?php

}//END IF SLIPPY ?>
