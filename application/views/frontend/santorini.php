<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'js'=>isset($js)?$js:array(),'BodyClass'=>''));

$sfilepath = base_url().'uploads/news';
?>
		<div class="container">
			<div class="bg-cs-gray-second">
				<div class="row align-middle">
					<div class="columns">
						<nav aria-label="You are here:" role="navigation">
							<ul class="breadcrumbs">
								<li><a href="<?php echo base_url(); ?>">หน้าแรก</a></li>
								<li><span class="show-for-sr">Current:</span> <?php echo $title; ?></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
			<div class="bg-cs-gray clearfix">
				<h3 class="text-center color-blue page-title"><?php echo $title; ?></h3>
			</div>

			<!--NEWS SLIDER-->
			<div class="row mar-top-large clearfix">
				<!--LEFT-->
				<div class="small-12 medium-12 column">

					<ul id="pictures-demo">
					  <li title="This is caption 1 <a href='#link'>Even with links!</a>">
					    <img src="<?php echo base_url(); ?>assets/images/santorini1.jpg" alt="demo1_1">
					  </li>
					  <li title="This is caption 2">
					    <img src="<?php echo base_url(); ?>assets/images/santorini2.jpg"  alt="demo1_2">
					  </li>
					  <li title="And this is some very long caption for slide 3. Yes, really long.">
					    <img src="<?php echo base_url(); ?>assets/images/santorini3.jpg" alt="demo1_3">
					  </li>
					</ul>

					<!--TITLE NEWS-->
					<div class="mar-top-large mar-bott-large clearfix">
						<div class="">

							<h1 class="color-blue f_size1_25 text-center ">Santorini Hua Hin - Khao Tao</h1>
							<h3 class="color-skyblue f_size2 text-center">The Heaven Privacy of the Beach Town</h3>
							<div class="well border-none bg-cs-skyblue text-center">
The Blue sky over the serene sea of the gulf of Thailand evokes memories of a seaside. The vita Hua Hin – Pranburi resort offers pleasant and calm surroundings, fresh air, clear water, and special white fine sandy beach. The stunning beachfront scenery here has the reputation of being Thailand’s top class seaside resort. The rooms were designed to enjoy the finest panorama sunset view with few steps from the beach and the glamorous privacy environment from the mountain view on private balcony for each unit. Indulging in breathtaking oceanfront views, elegant accommodations, green space, and golf course added to this masterpiece condominium.
							</div>
						</div>
					</div>
					<div class="row">
						<div class="small-6 medium-4 columns">
							<span class="color-blue">Contact</span> <span class="color-skyblue"> Santorini Hua Hin</span>
						</div>
						<div class="small-6 medium-2 columns" style="border-right: 1px solid #666;">
							<span class="color-skyblue"> Telephone </span><br>
							(099) 262-1222 <br>
							(099) 262-1333 <br>
							(080) 451-1546
						</div>
						<div class="small-6 medium-3 columns" style="border-right: 1px solid #666;">
							<span class="color-skyblue"> E-mail </span> <br>
							Sales: santorinicondo1@gmail.com
						</div>
						<div class="small-6 medium-3 columns">
							<span class="color-skyblue"> Line ID  </span> <br>
							santorini1,santorini2
						</div>
					</div>

					<div class="row mar-top-large align-middle">
						<div class="title small-12 medium-2 medium-centered column">
							<a href="#" class="hollow button" href="#">santorini-huahin.com</a>
						</div>
					</div>

				</div>
			</div>

			<div class="row clearfix"></div>
		</div>
<?php $this->load->view('partials/footer');?>
<?php
if(isset($slippry))
{
?>
<!--NEWS SLIDER : SLIPPRY-->
<script type="text/javascript">
	$(document).ready(function() {
		jQuery('#out-of-the-box-demo').slippry({
			// general elements & wrapper
			slippryWrapper: '<div class="sy-box out-of-the-box-demo" />', // wrapper to wrap everything, including pager
			elements: 'article', // elments cointaining slide content

			// options
			adaptiveHeight: false, // height of the sliders adapts to current
			captions: false,


			// transitions
			transition: 'horizontal', // fade, horizontal, kenburns, false
			speed: 1200,
			pause: 8000,

			// slideshow
			autoDirection: 'prev'
		});

		jQuery('#pictures-demo').slippry({
		  // general elements & wrapper
		  slippryWrapper: '<div class="sy-box pictures-slider" />', // wrapper to wrap everything, including pager

		  // options
		  adaptiveHeight: false, // height of the sliders adapts to current slide
		  captions: false, // Position: overlay, below, custom, false

		  // pager
		  pager: true,

		  // controls
		  controls: true,
		  autoHover: false,

		  // transitions
		  transition: 'fade', // fade, horizontal, kenburns, false
		  kenZoom: 140,
		  speed: 2000 // time the transition takes (ms)
		});

	});

</script>
<?php }//END IF SLIPPY ?>
