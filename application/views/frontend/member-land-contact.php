<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>''));
?>

	<div id="container">
		<div class="bg-cs-gray-second">
			<div class="row align-middle">
				<div class="columns">
					<nav aria-label="You are here:" role="navigation">
						<?php echo $breadcrumbs; ?>
					</nav>
				</div>
			</div>
		</div>
		<div class="bg-cs-gray clearfix">
			<h3 class="text-center color-blue"><?php echo $title; ?></h3>
		</div>
		<div class="row content-inside">
			<!-- -------------- -->
			<!-- LEFT : SIDEBAR -->
			<!-- -------------- -->

			<div class="small-12 medium-4 large-3 columns">
				<?php $this->load->view('partials/acc-sidebar');?>
			</div>

			<!-- RIGHT : CONTENT -->
			<form action="">
			<div class="small-12 medium-8 large-9 columns">
				<div class="small-12 medium-12 columns">
					<table class="hover">
						<thead>
							<tr>
								<th width="100">รูป</th>
								<th width="200">ชื่อโครงการ</th>
								<!-- <th width="150">สถานะ</th> -->
								<th width="250">วันที่ิติดต่อ</th>
								<th width="100">จัดการ</th>
							</tr>
						</thead>
						<tbody>
							<?php
								foreach($land_list as $row => $value)
								{//START FOREACH#1
									$sfilepath = base_url().'uploads/land';
									$pic_thumb = $sfilepath.'/'.$value->pic_thumb;
									//echo $value->type_status_id;
							?>
							<input type="hidden" name="land_id[]" value="<?php echo $value->land_id; ?>">
							<tr>
								<td><a href="<?php echo site_url('page/landinside/'.$value->land_id); ?>"><img src="<?php echo $pic_thumb;?>" style="width: 150px;" class="img-thumbnail" alt=""></a></td>
								<td><a href="<?php echo site_url('page/landinside/'.$value->land_id); ?>"><?php echo $value->land_title; ?></a></td>
								<!-- <td><?php echo $this->Configsystem_model->get_type_status_title($value->fk_type_status_id); ?></td> -->
								<td>
									<?php echo $this->dateclass->DateTimeShortFormat($value->sharedate, 0, 1, "Th"); ?><br>
								</td>
								<td><a href="<?php echo base_url(); ?>land-share-contact-list/<?php echo $value->land_id ?>/<?php echo $_SESSION['mID']; ?>" class=""><i class="fa fa-envelope"></i></a></td>
							</tr>
							<?php }//END FOREACH#1 ?>
						</tbody>
					</table>
				</div>
				<!--RIGHT-->
			</div>
			</form>

			<!-- ------------------- -->
			<!-- END RIGHT : CONTENT -->
			<!-- ------------------- -->



		</div>
	</div>


	<?php $this->load->view('partials/footer', array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>''));?>


<script>
$(document).ready(function () {



	$(".hob-renew-share").click( function (event) {

		var select = $(this);
		var id = select.attr('id');
		console.log(id);
		var url = '<?php echo base_url(); ?>page/m_share_land_renew_date';
		$.ajax({
			method: 'POST',
			url: "<?php echo base_url(); ?>page/m_share_land_renew_date",
			data: {
				id : id,
			},
			async: false,
			success: function (data) {
				console.log(data);
				if ( data == 'success' )
				{
					swal({
						title: 	"สำเร็จ",
						text: 	"ท่านได้ทำการต่ออายุประกาศเรียบร้อยแล้วค่ะ...",
						type: 	"info",
					},
						function(isConfirm){
							window.location.reload();
					});
				}

			},
			error: function () {
				console.log("failure");
			}

		});
		event.preventDefault();
		return false;
	});
	/* END CLICK */
});
</script>
