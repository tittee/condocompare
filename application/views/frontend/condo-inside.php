<?php
defined('BASEPATH') or exit();
/*HEADER*/
$this->load->view('partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'BodyClass' => ''));

$sfilepath = base_url().'uploads/condominium';

$condo_id = $data->condo_id;

$condo_property_id = (!empty($data->condo_property_id)) ? $data->condo_property_id : '-';
$condo_owner_name = (!empty($data->condo_owner_name)) ? $data->condo_owner_name : '-';

$staff_id = (!empty($data->staff_id)) ? $data->staff_id : '0';

$staff = $this->M->get_staff('staff_id, fullname, email, phone', 'staff_id', $data->staff_id, 'staff');
//$this->primaryclass->pre_var_dump($data);
$staff_ids = (!empty($staff->staff_id)) ? $staff->staff_id : '0';
$staff_fullname = (!empty($staff->fullname)) ? $staff->fullname : '-';
$staff_email = (!empty($staff->email)) ? $staff->email : '-';
$staff_phone = (!empty($staff->phone)) ? $staff->phone : '-';

$show_sale = $this->input->get('show', true);

$condo_title = (!empty($data->condo_title)) ? $data->condo_title : '-';
$condo_holder_name = (!empty($data->condo_holder_name)) ? $data->condo_holder_name : '-';
$condo_builing_finish = (!empty($data->condo_builing_finish)) ? $data->condo_builing_finish : '-';

$type_offering_id = (!empty($data->type_offering_id)) ? $data->type_offering_id : '-';
$type_offering_title = (!empty($type_offering_id) || $type_offering_id !== 0 || isset($type_offering_id)) ? $this->M->get_name_by_id('type_offering_id', 'type_offering_title', $type_offering_id, 'type_offering') : '-';
$type_offering_title_t = (isset($type_offering_title[0])) ? $type_offering_title[0]['type_offering_title'] : '-';

$type_suites_id = (!empty($data->type_suites_id)) ? $data->type_suites_id : '-';
$type_suites_title = (!empty($type_suites_id) || $type_suites_id !== 0 || isset($type_suites_id)) ? $this->M->get_name_by_id('type_suites_id', 'type_suites_title', $type_suites_id, 'type_suites') : '-';
$type_suites_title_t = (isset($type_suites_title[0])) ? $type_suites_title[0]['type_suites_title'] : '-';

$type_decoration_id = (!empty($data->type_decoration_id)) ? $data->type_decoration_id : '-';
$type_decoration_title = (!empty($type_decoration_id) || $type_decoration_id !== 0  || isset($type_decoration_id)) ? $this->M->get_name_by_id('type_decoration_id', 'type_decoration_title', $type_decoration_id, 'type_decoration') : '-';
$type_decoration_title_t = (isset($type_decoration_title[0])) ? $type_decoration_title[0]['type_decoration_title'] : '-';

$condo_all_room = (!empty($data->condo_all_room)) ? $data->condo_all_room : '-';
$condo_all_toilet = (!empty($data->condo_all_toilet)) ? $data->condo_all_toilet : '-';
$condo_interior_room = (!empty($data->condo_interior_room)) ? $data->condo_interior_room : '-';
$condo_direction_room = (!empty($data->condo_direction_room)) ? $data->condo_direction_room : '-';
$condo_scenery_room = (!empty($data->condo_scenery_room)) ? $data->condo_scenery_room : '-';
$condo_living_area = (!empty($data->condo_living_area)) ? $data->condo_living_area : '-';
$condo_electricity = (!empty($data->condo_electricity)) ? $data->condo_electricity : '-';

$condo_expert = (!empty($data->condo_expert)) ? $data->condo_expert : '-';
$condo_description = (!empty($data->condo_description)) ? $this->primaryclass->decode_htmlspecialchars($data->condo_description) : '-';

$type_zone_title = (!empty($data->type_zone_title)) ? $data->type_zone_title : '-';

$condo_no = (!empty($data->condo_no)) ? $data->condo_no : '-';
$condo_floor = (!empty($data->condo_floor)) ? $data->condo_floor : '-';
// $condo_area_apartment = (!empty($data->condo_area_apartment))? $data->condo_area_apartment : '0';
$condo_alley = (!empty($data->condo_alley)) ? $data->condo_alley : '-';
$condo_road = (!empty($data->condo_road)) ? $data->condo_road : '-';
$condo_address = (!empty($data->condo_address)) ? $data->condo_address : '-';
$provinces_id = (!empty($data->provinces_id)) ? $data->provinces_id : '';
$provinces_name = (!empty($provinces_id) || $provinces_id !== 0) ? $this->M->get_name_by_id('provinces_id', 'provinces_name', $provinces_id, 'provinces') : '-';
$provinces_name_t = (isset($provinces_name[0])) ? $provinces_name[0]['provinces_name'] : '-';

$districts_id = (!empty($data->districts_id)) ? $data->districts_id : '';
$districts_name = (!empty($districts_id) || $districts_id !== 0) ? $this->M->get_name_by_id('districts_id', 'districts_name', $districts_id, 'districts') : '-';
$districts_name_t = (isset($districts_name[0])) ? $districts_name[0]['districts_name'] : '-';

$sub_districts_id = (!empty($data->sub_districts_id)) ? $data->sub_districts_id : '';
$sub_districts_name = (!empty($sub_districts_id) || $sub_districts_id !== 0) ? $this->M->get_name_by_id('sub_districts_id', 'sub_districts_name', $sub_districts_id, 'sub_districts') : '-';
$sub_districts_name_t = (isset($sub_districts_name[0])) ? $sub_districts_name[0]['sub_districts_name'] : '-';

$condo_zipcode = (!empty($data->condo_zipcode)) ? $data->condo_zipcode : '-';

//PRICE
$condo_total_price = ($data->condo_total_price !== '0.00') ? $data->condo_total_price : '0.00';
$condo_total_price_bath = ($condo_total_price !== '0.00') ? number_format($condo_total_price, 0, '.', ',').' บาท' : 'ไม่ระบุ';

$condo_area_apartment = ($data->condo_area_apartment !== '0.00') ? $data->condo_area_apartment : '0.00';
$condo_area_apartment_meter = ($data->condo_area_apartment !== '0.00') ? $data->condo_area_apartment.' ตร.ม.' : 'ไม่ระบุ';

$condo_price_per_square_meter = ($data->condo_price_per_square_meter !== '0.00') ? $data->condo_price_per_square_meter : '0.00';
$condo_price_per_square_meter_bath = ($condo_price_per_square_meter !== '0.00') ? number_format($condo_price_per_square_meter, 0, '.', ',').' บาท' : 'ไม่ระบุ';

//ราคา XXXX บาท / ตร.ม.
$msg_price_per_meter = $this->primaryclass->get_price_per_square(0, $condo_total_price, $condo_area_apartment, $condo_price_per_square_meter);

$createdate = $data->createdate;
$createdate_t = (isset($createdate) || !empty($createdate)) ? $this->dateclass->DateTimeShortFormat($createdate, 0, 0, 'Th') : 'ยังไม่มีข้อมูล';

$latitude = (!empty($data->latitude)) ? $data->latitude : '13744425';
$longitude = (!empty($data->longitude)) ? $data->longitude : '100545679';
$visited = (!empty($data->visited)) ? $data->visited : '-';

$site_id = 1;
$site_loan = $this->M->get_loan_setting($site_id);
$site_mrl = (!empty($site_loan['site_mrl'])) ? $site_loan['site_mrl'] : '';
$site_mor = (!empty($site_loan['site_mor'])) ? $site_loan['site_mor'] : '';
$site_mrr = (!empty($site_loan['site_mrr'])) ? $site_loan['site_mrr'] : '';
$site_cpr = (!empty($site_loan['site_cpr'])) ? $site_loan['site_cpr'] : '';

$share_url = '';
?>
<style>
    html,
    body,
    #map-canvas {
        height: 100%;
        margin: 0;
        padding: 0;
    }

    #map-canvas {
        width: 100%;
        height: 400px;
    }
</style>

	<div id="container">
		<div class="bg-cs-gray-second">
			<div class="row align-middle">
				<div class="columns">
					<nav aria-label="You are here:" role="navigation">
            <?php echo $breadcrumbs; ?>
					</nav>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row content-inside">
			<!--LEFT-->

      <div data-toggler="" data-animate="fade-in fade-out" class="alert callout callout secondary ease" id="panel-message" aria-expanded="true" style="display: none;">
        <p><i class="fa fa-check-circle" aria-hidden="true"></i> <span id="message"></span></p>
      </div>

			<div class="small-12 large-9 columns">
				<h1><?php echo $condo_title; ?></h1>
				<div class="row">
					<div class="small-12 medium-6 large-9 columns">
						<p class="location_address">
							<?php echo $type_zone_title; ?>
						</p>
                        <p class="contact_name"><i class="fa fa-eye" style="color: #E9E9E9;"></i> จำนวนผู้ชม <?php echo $visited; ?> </p>
						<table class="details small-12 large-12">
							<thead>
								<th>ห้องนอน</th>
								<th>ห้องน้ำ</th>
								<th>พื้นที่ใช้สอย</th>
							</thead>
							<tbody>
								<td>
									<?php echo $condo_all_room; ?>
								</td>
								<td>
									<?php echo $condo_all_toilet; ?>
								</td>
								<td>
									<?php echo $condo_area_apartment_meter; ?></td>
							</tbody>
						</table>
					</div>
					<div class="small-12 medium-6 large-3 columns">
						<div class="price"><span><?php echo $condo_total_price_bath; ?></span></div>
						<p>
							<?php echo $msg_price_per_meter; ?></p>
						<!--<p>จำนวน 39,400 บาท / เดือน</p>-->
					</div>
				</div>
				<!--.clearfix-->
				<div class="slide">
					<div id="sync1" class="owl-carousel owl-theme">
						<div class="item"><img src="<?php echo $sfilepath.'/'.$data->pic_large; ?>" alt=""></div>
						<!-- First Images -->
						<?php
                            foreach ($condo_image as $value_images) {
                                ?>
							<div class="item"><img src="<?php echo $sfilepath.'/'.$value_images->pic_large;
                                ?>" alt=""></div>
							<?php

                            }
                            ?>
					</div>
					<div id="sync2" class="owl-carousel owl-theme">
						<div class="item"><img src="<?php echo $sfilepath.'/'.$data->pic_large; ?>" alt=""></div>
						<!-- First Images -->
						<?php
                            foreach ($condo_image as $value_images) {
                                ?>
							<div class="item"><img src="<?php echo $sfilepath.'/'.$value_images->pic_large;
                                ?>" alt=""></div>
							<?php

                            }
                            ?>
					</div>
				</div>
				<div class="content-detail">
					<div class="well columns">
						<h3>รายละเอียดโครงการ</h3>

						<div class="small-12 columns mar-bott-medium">
							<div class="row">
								<div class="small-12 columns">
									<p><strong><u>ที่อยู่โครงการ</u></strong></p>
								</div>
							</div>
							<!-- #End col-->
						</div>
						<div class="clearfix"></div>

						<div class="small-12 columns">
							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ห้องเลขที่ / บ้านเลขที่</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $condo_no; ?>
									</p>
								</div>
								<!-- #End col-->

								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ชั้นที่ (คอนโด)</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $condo_floor; ?>
									</p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ขนาดพื้นที่ / ตร.ม.</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
                                        <?php echo $condo_area_apartment_meter; ?>
									</p>
								</div>
								<!-- #End col-->

								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ตรอก / ซอย</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $condo_alley; ?>
									</p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ถนน</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $condo_road; ?>
									</p>
								</div>
								<!-- #End col-->

								<div class="small-6 medium-6 large-3 columns">
									<p><strong>จังหวัด</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $provinces_name_t; ?>
									</p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>อำเภอ/เขต</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $districts_name_t; ?>
									</p>
								</div>
								<!-- #End col-->

								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ตำบล/แขวง</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $sub_districts_name_t; ?>
									</p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>รหัสไปรษณีย์</strong></p>
								</div>
								<div class="small-6 medium-6 large-9 columns">
									<p>
										<?php echo $condo_zipcode; ?>
									</p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->
						</div>
						<div class="clearfix mar-bott-large"></div>

						<div class="small-12 columns">

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ชื่อโครงการ</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $condo_title; ?>
									</p>
								</div>
								<!-- #End col-->

								<div class="small-6 medium-6 large-3 columns">
									<p><strong>รหัสทรัพย์สิน</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p><?php echo $condo_property_id; ?></p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ประเภททรัพย์</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>คอนโดมิเนี่ยม</p>
								</div>
								<!-- #End col-->

                <div class="small-6 medium-6 large-3 columns">
									<p><strong>เจ้าของโครงการ</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p><?php echo $condo_owner_name; ?></p>
								</div>
								<!-- #End col-->

							</div>
							<!-- #End Row-->

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ราคา</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p><?php echo $condo_total_price_bath; ?></p>
								</div>
								<!-- #End col-->

								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ราคาต่อตร.ม.</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $msg_price_per_meter; ?></p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>สิทธิ์ถือครอง</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $condo_holder_name; ?>
									</p>
								</div>
								<!-- #End col-->

								<div class="small-6 medium-6 large-3 columns">
									<p><strong>พื้นที่ใช้สอย / ตร.ม.</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $condo_area_apartment; ?> ตร.ม.</p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ประเภทการเสนอขาย</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $type_offering_title_t; ?></p>
								</div>
								<!-- #End col-->

								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ปีที่สร้างเสร็จ</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $condo_builing_finish; ?>
									</p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->


							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>แบบห้องชุด</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $type_suites_title_t; ?>
									</p>
								</div>
								<!-- #End col-->

								<div class="small-6 medium-6 large-3 columns">
									<p><strong>การตกแต่ง</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $type_decoration_title_t; ?></p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ห้องนอน</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $condo_all_room; ?>
									</p>
								</div>
								<!-- #End col-->

								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ห้องน้ำ</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $condo_all_toilet; ?>
									</p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>อุปกรณ์ตกแต่งภายในห้อง</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $condo_interior_room; ?>
									</p>
								</div>
								<!-- #End col-->

								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ทิศของห้อง</strong></p>
								</div>
								<div class="small-6 medium-6 large-3 columns">
									<p>
										<?php echo $condo_direction_room; ?>
									</p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>วิวของห้อง</strong></p>
								</div>
								<div class="small-6 medium-6 large-9 columns">
									<p>
										<?php echo $condo_scenery_room; ?>
									</p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p><strong>ลงประกาศเมื่อ</strong></p>
								</div>
								<div class="small-6 medium-6 large-9 columns">
									<p>
										<?php echo $createdate_t; ?>
									</p>
								</div>
								<!-- #End col-->
							</div>
							<!-- #End Row-->
						</div>
						<div class="clearfix"></div>

						<div class="small-12 columns mar-top-medium">
							<div class="row">
								<div class="small-12 medium-6 large-3 columns">
									<p><strong>รายละเอียดเพิ่มเติม</strong></p>
								</div>
								<div class="small-12 medium-6 large-9 columns">
									<?php echo $condo_description; ?>
								</div>
							</div>
						</div>
					</div>
					<div class="well columns">
						<h3>สิ่งอำนวยความสะดวก</h3>
						<div class="small-12 columns">
							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p>จุดเด่น</p>
								</div>
								<div class="small-6 medium-6 large-9 columns">
                  <div class="row">
									<?php
                  $numFeatured = count($featured);
                  if (!empty($featured)) {
                      $i = 0;
                      $cssEnd = '';
                      foreach ($featured as $key_featured => $val_featured) {
                        // echo $key_featured;
                      $featured_title = (!empty($val_featured->featured_title)) ? $val_featured->featured_title : '-';
                      if(++$i === $numFeatured) {
                        $cssEnd =  "end";
                      }
                  ?>
									<div class="small-12 large-4 <?php echo $cssEnd; ?> columns"><i aria-hidden="true" class="fa fa-check"></i> <?php echo $featured_title;?></div>
									<?php }//END FOREACH ?>
									<?php } else { ?>
									<div class="small-12 large-4 columns"><i aria-hidden="true" class="fa fa-check"></i> ไม่ระบุ </div>
									<?php }//END IF ?>
                  </div>
								</div>
							</div>

							<div class="row">
								<div class="small-6 medium-6 large-3 columns">
									<p>สิ่งอำนวยความสะดวก</p>
								</div>
								<div class="small-6 medium-6 large-9 columns">
                  <div class="row">

									<?php
                  $numFacilities = count($facilities);
                  if (!empty($facilities)) {
                    $i = 0;
                    $cssEnd = '';
                      foreach ($facilities as $key_facilities => $val_facilities) {
                        // echo $val_facilities;
                        $facilities_title = (!empty($val_facilities->facilities_title)) ? $val_facilities->facilities_title : '-';
                        if(++$i === $numFacilities) {
                          $cssEnd =  "end";
                        }
                  ?>
                  <div class="small-12 large-4 <?php echo $cssEnd; ?> columns"><i aria-hidden="true" class="fa fa-check"></i> <?php echo $facilities_title;?></div>
                  <?php

                      }//END FOREACH
                  ?>
									<?php } else { ?>
									<div class="small-12 large-4 columns"><p> ไม่ระบุ </div>
									<?php }//END IF ?>
                  </div>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>




					<div class="well columns clearfix">
						<h3>คำนวณยอดผ่อนชำระบ้านต่อเดือน</h3>
						<p>ปรับรายละเอียดข้างล่างเพื่อคำนวณยอดเงินผ่อนต่อเดือน</p>
						<p>เครื่องคำนวณ โดย Harrison</p>
            <form action="" method="post" name="form_cal_loan" id="form_cal_loan" enctype="multipart/form-data" autocomplete="off">
            <div class="row">
							<div class="small-12 medium-6 columns">
								<div class="row clearfix">
									<div class="small-5 columns">
										<label for="left-label" class="f_size1">ราคา</label>
									</div>
									<div class="small-7 columns">
										<p>
											<?php echo $condo_total_price_bath; ?></p>
									</div>
								</div>
								<div class="row clearfix">
									<div class="small-12 large-5 columns">
										<label for="left-label" class="f_size1">วงเงินกู้</label>
									</div>
									<div class="small-12 large-7 columns">
										<div class="input-group"><span class="input-group-label"> ฿</span>
											<input type="number" name="loan_price" id="loan_price" required class="input-group-field">
										</div>
									</div>
								</div>
								<div class="row clearfix">
									<div class="small-12 large-5 columns">
										<label for="left-label" class="f_size1">ระยะเวลากู้</label>
									</div>
									<div class="small-10 large-5 columns">
										<div class="input-group">
											<input type="text" name="loan_years" id="loan_years" class="input-group-field">
										</div>
									</div>
                  <div class="small-2 large-2 columns">
                    <p>ปี</p>
                  </div>
								</div>
								<div class="row clearfix">
									<div class="small-12 large-5 columns">
										<label for="left-label" class="f_size1">อัตราดอกเบี้ย</label>
									</div>
									<div class="small-10 large-5 columns">
										<div class="input-group">
											<input type="text" name="loan_interest" id="loan_interest" class="input-group-field">
										</div>
									</div>
                  <div class="small-2 large-2 columns">
                  <p>%</p>
                  </div>
								</div>
							</div>
							<div class="small-12 medium-6 columns">
								<div class="row">
									<div class="small-12 medium-12 columns">
										<div id="mySmallDoughnut"></div>
										<p id="pay_per_month" style="display: none;">฿ <span></span> ต่องวด</p>
										<!-- <p>฿ 32,850 ชำระดอกเบี้ย</p> -->
									</div>
									<div class="small-12 columns">
										<h3>ประเภทอัตราดอกเบี้ยเงินกู้</h3>
										<table class="stack details">
											<thead>
												<th>ประเภท</th>
												<th>ดอกเบี้ย</th>
											</thead>
											<tbody>
												<tr>
													<td>MLR</td>
													<td><?php echo $site_mrl ?>% ต่อปี</td>
												</tr>
												<tr>
													<td>MOR</td>
													<td><?php echo $site_mor ?>% ต่อปี</td>
												</tr>
												<tr>
													<td>MRR</td>
													<td><?php echo $site_mrr ?>% ต่อปี</td>
												</tr>
												<tr>
													<td>CPR</td>
													<td><?php echo $site_cpr ?>% ต่อปี</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
            </form>

                        <hr>
						<div class="row">
							<!-- <div class="small-12 medium-6 columns">
								<p><a href="#" class="button button-red-white text-center"><i class="fa fa-calculator"></i></a> ฉันสามารถกู้เงินได้เท่าไหร่?</p>
							</div> -->
							<div class="small-12 medium-6 columns">
								<button name="submit_calculator" id="submit_calculator" class="button button-red-white expanded"><i class="fa fa-calculator"></i> คำนวณการชำระเงิน</button>
							</div>
						</div>
                        <hr>
                        <!-- GOOGLE MAPS -->

            				<div class="row">
            					<div class="columns">
            						<div id="map-canvas"></div>
            					</div>
            				</div>

                    <div id="share-facebook">

						<?php
                            if (isset($this->session->mID)) {
                                $share_repeat = $this->Condominium_model->check_member_share($condo_id, $this->session->mID);

                                if (isset($this->session->mTID) && ($this->session->mTID == '2')) {
                                    //3 = Facebook Login

                            ?>
							<?php if ($share_repeat > 0) {
    ?>
								<div class="share_link">
									<div class="colomns-align-center text-center">
										<p class="color-white">คุณได้แชร์ลิ้งค์นี้ไปแล้ว สามารถต่ออายุได้ที่หน้าโปรไฟล์ </p>
									</div>
								</div>
								<?php

} else {
    ?>

									<div class=" share_link">
										<div class="colomns-align-center text-center">
											<p class="color-white">ท่านสามารถเปิดหรือนำลิงค์ URL นี้ไปฝากลิงค์หรือแปะที่เว็บไซต์อื่นๆ ที่ท่านได้ลงประกาศได้</p>
											<p class="color-white">เพื่อความสะดวกและง่ายในการโปรโมทอสังหาริมทรัพย์ของท่านให้เป็นที่รู้จักมากขึ้น</p>

										<!-- <div id="shareIconsCount"></div> -->

                      <!-- Load Facebook SDK for JavaScript -->
                      <input type="hidden" value="<?php echo $condo_id; ?>" name="condo_id" id="condo_id">
                      <input type="hidden" value="<?php echo $condo_title; ?>" name="condo_title" id="condo_title">
                      <input type="hidden" value="<?php echo $sfilepath.'/'.$data->pic_thumb; ?>" name="pic_thumb" id="pic_thumb">

                      <?php if (isset($this->session->mID)) {
                         $share_url = site_url('condominium-share/'.$condo_id.'/'.$fk_member_id);
                         $url_member_share = site_url('page/member_share_facebook');
                      ?>
                      <input type="hidden" value="<?php echo $fk_member_id; ?>" name="member_id" id="member_id">
                      <input type="hidden" name="url_share" id="url_share" value="<?php echo $share_url; ?>">
                      <input type="hidden" name="url_member_share" id="url_member_share" value="<?php echo $url_member_share; ?>">
                     <?php } else {
                       $share_url = site_url('condominium-details/'.$condo_id);
                       $url_member_share = site_url('condominium-details/'.$condo_id);
                       ?>
                      <input type="hidden" name="url_share" id="url_share" value="<?php echo $share_url; ?>">
                      <input type="hidden" name="url_member_share" id="url_member_share" value="<?php echo $url_member_share; ?>">
                     <?php }// END IF ?>

                      <!-- Your share button code -->
                      <div class="jssocials-shares">
                        <div class="jssocials-share jssocials-share-facebook">
                          <a href="#" class="fb-share-button jssocials-share-link" id="fb-share-button" data-layout="button_count" data-href="<?php echo $share_url; ?>">
                            <i class="fa fa-facebook jssocials-share-logo"></i> แชร์กดตรงนี้
                          </a>
                        </div>
                      </div>
                    <!-- ้ -->
                    <!--   -->
                    </div>




									</div>
									<?php

}//END IF  ?>
										<?php

                                } else { //END IF  ?>
											<div class=" share_link">
												<div class="colomns-align-center text-center">
													<p class="color-white">ท่านสามารถเปิดหรือนำลิงค์ URL นี้ไปฝากลิงค์หรือแปะที่เว็บไซต์อื่นๆ ที่ท่านได้ลงประกาศได้</p>
													<p class="color-white">เพื่อความสะดวกและง่ายในการโปรโมทอสังหาริมทรัพย์ของท่านให้เป็นที่รู้จักมากขึ้น</p>
													<p><a class="color-red" href="<?php echo site_url('page/m_login');
                                    ?>">กรุณา สมัครสมาชิก Condo Compare หรือ HOB เพื่อทำการแชร์</a></p>
												</div>
											</div>
											<?php

                                }//END IF  ?>

												<?php

                            } else {
                                ?>
													<?php
                                    if (isset($this->session->mTID) && ($this->session->mTID == '2')) {
                                        //3 = Facebook Login

                                ?>
														<div class=" share_link">
															<div class="colomns-align-center text-center">
																<p class="color-white">ท่านสามารถเปิดหรือนำลิงค์ URL นี้ไปฝากลิงค์หรือแปะที่เว็บไซต์อื่นๆ ที่ท่านได้ลงประกาศได้</p>
																<p class="color-white">เพื่อความสะดวกและง่ายในการโปรโมทอสังหาริมทรัพย์ของท่านให้เป็นที่รู้จักมากขึ้น</p>
																<p><a class="color-red" href="<?php echo site_url('page/m_login');
                                        ?>">กรุณา Login เข้าสู่ระบบ เพื่อทำการแชร์</a></p>
															</div>
														</div>
														<?php

                                    } else { //END IF  ?>
															<div class=" share_link">
																<div class="colomns-align-center text-center">
																	<p class="color-white">ท่านสามารถเปิดหรือนำลิงค์ URL นี้ไปฝากลิงค์หรือแปะที่เว็บไซต์อื่นๆ ที่ท่านได้ลงประกาศได้</p>
																	<p class="color-white">เพื่อความสะดวกและง่ายในการโปรโมทอสังหาริมทรัพย์ของท่านให้เป็นที่รู้จักมากขึ้น</p>
																	<p><a class="color-red" href="<?php echo site_url('page/m_login');
                                        ?>">กรุณา สมัครสมาชิก Condo Compare หรือ HOB เพื่อทำการแชร์</a></p>
																</div>
															</div>
															<?php

                                    }//END IF  ?>
																<?php

                            }//END IF  ?>
																	<div class="clearfix"></div>
                   </div>
					</div>
				</div>
			</div>
			<!--RIGHT-->
			<div class="small-12 large-3 columns">
				<div id="share"></div>
				<div class="clearfix"></div>
				<div class="mar-top-large"><a href="<?php echo site_url('condocompare'); ?>" class="button button-red-white expanded"><i class="fa fa-file"></i>	เปรียบเทียบราคาคอนโด</a></div>
				<div class="clearfix row mar-top-medium">
					<?php
            if (isset($this->session->mTID)) {
                ?>
                <?php
                $query1 = $this->db
                ->select('condo_id, sess_id')
                ->where('condo_id', $condo_id)
                ->where('sess_id', $_SESSION['sess_id'])
                ->get('wishlist_condo');
                if ($query1->num_rows() > 0) {
                    $row_relation = $query1->row_array();
                    $fk_condo_id = $row_relation['condo_id'];
                    $sel = ($condo_id ==  $fk_condo_id) ? "style='color: green;'" : '';
                    ?>
                     <div class="small-12 large-7 columns"><a href="#" onclick="javascript:(0);" class="black hob-wishlist" id="<?php echo $condo_id; ?>"><i class="fa fa-heart sidebar" <?php echo $sel; ?>></i>	เก็บไว้ดูภายหลัง</a></div>
            <?php

                } else {
                    ?>
                <div class="small-12 large-7 columns"><a href="#" onclick="javascript:(0);" class="black hob-wishlist" id="<?php echo $condo_id; ?>"><i class="fa fa-heart sidebar"></i>	เก็บไว้ดูภายหลัง</a></div>
            <?php } //END IF ?>
            <?php
            $query2 = $this->db
                ->select('condo_id, sess_id')
                ->where('condo_id', $condo_id)
                ->where('sess_id', $_SESSION['sess_id'])
                ->get('compare_condo');
                if ($query2->num_rows() > 0) {
                    $row_relation = $query2->row_array();
                    $fk_condo_id = $row_relation['condo_id'];
                    $sel = ($condo_id ==  $fk_condo_id) ? "style='color: green;'" : '';
                    ?>
                    <div class="small-12 large-5 columns"><a href="#" onclick="javascript:(0);" class="black hob-compare" id="<?php echo $condo_id; ?>"><i class="fa fa fa-file sidebar" <?php echo $sel; ?>></i>	เปรียบเทียบ</a></div>
            <?php } else {  ?>
                    <div class="small-12 large-5 columns"><a href="#" onclick="javascript:(0);" class="black hob-compare" id="<?php echo $condo_id; ?>"><i class="fa fa fa-file sidebar"></i>	เปรียบเทียบ</a></div>
                <?php

                } //END IF ?>
						<?php

                        // } else {
                            ?>
							<!-- <div class="small-12 large-7 columns">เข้าสู่ระบบเพื่อทำการเปรียบเทียบ และจัดลำดับรายการโปรด</div> -->
							<?php

                        } //END IF ?>

                <input type="hidden" name="url_facebook_share" id="url_facebook_share" value="<?php echo current_url(); ?>">
								<div class="small-12 large-5 end columns">
                  <a href="#" id="fb-share" class="black" data-layout="button_count"
                  data-href="<?php echo current_url(); ?>"><i class="fa fa-share-alt sidebar"></i>	แชร์</a>
                </div>
								<!--<li><a href="#" class="black"><i class="fa fa-file sidebar"></i>	พิมพ์</a></li>-->
				</div>

				<form action="" method="post" role="form" name="form_contact_sidebar" id="form_contact_sidebar">
					<input type="hidden" name="staff_id" id="staff_id" value="<?php echo $staff_id; ?>">


					<div class="bg-gray mar-top-medium clearfix">

						<?php // if (isset($show_sale)) {    ?>
							<!-- <div class="row align-middle">
								<div class="small-12 medium-5 column">
									<a href="#"><img src="http://placehold.it/100x100&amp;text=Who?" alt=""></a>
								</div>
								<div class="small-12 medium-7 column align-self-middle">
									<p>
										<?php //echo $staff_fullname; ?>
										<?php //echo $staff_email; ?>
									</p>
								</div>
							</div> -->
							<!-- <div class="column align-middle title-blue-bg">
								<div class="small-4 column"><i class="fa fa-phone sidebar"></i></div>
								<div class="small-8 column">
									<p class="color-white">
										<?php //echo $staff_phone;?>
									</p>
								</div>
							</div> -->
							<?php  //} //else { ?>
								<!-- <div class="column align-middle title-blue-bg">
									<div class="small-12 column">
										<p class="color-white">กรอกฟอร์มเพื่อรับข้อมูลการติดต่อตัวแทน</p>
									</div>
								</div> -->
								<?php //}//END IF SHOW ?>
									<div class="row bg-gray">
										<!-- <form action="" method="post"> -->
											<div class=" mar-top-small mar-bott-small">
												<!-- <div class="small-12 columns">
													<label>
														<input type="text" name="condo_share_form_name" id="condo_share_form_name" placeholder="ชื่อ-นามสกุล" required>
													</label>
												</div>
												<div class="small-12 columns">
													<label>
														<input type="text" name="condo_share_form_phone" id="condo_share_form_phone" placeholder="เบอร์โทร" required>
													</label>
												</div>
												<div class="small-12 columns">
													<label>
														<input type="email" name="condo_share_form_email" id="condo_share_form_email" required placeholder="อีเมล์">
													</label>
												</div>
												<div class="small-12 columns">
													<label>
														<textarea id="condo_share_form_message" name="condo_share_form_message" cols="10" rows="3"></textarea>
													</label>
												</div> -->
												<div class="small-12 columns" style="margin-bottom: 10px;">
                          <a href="#share-facebook" class="button bg-cs-blue expanded">Share HOB</a>
                        </div>

												<div class="small-12 columns">
													<!-- <button name="submit_contact" value="action" class="button button-red-white expanded">ติดต่อตัวแทน</button> -->

                          <a href="#contact-bottom" class="button button-red-white expanded">ติดต่อตัวแทน</a>
													<!-- <input type="hidden" name="fk_condo_id" value="<?php echo $fk_condo_id; ?>"> -->
													<!-- <input type="hidden" name="fk_staff_id" value="<?php echo $staff_ids; ?>"> -->
													<!-- <input type="hidden" name="fk_member_id" value="<?php echo $fk_member_id; ?>"> -->
												</div>
											</div>
										<!-- </form> -->
									</div>
					</div>
				</form>

				<div class="column well clearfix mar-top-medium">
					<h3>สถานที่ใกล้เคียง</h3>

					<?php
                    if (!empty($transportation) && count($transportation) > 1) {
                        // echo count( $transportation );
            // $this->primaryclass->pre_var_dump( $transportation );
                        foreach ($transportation as $val_transportation) {
                            // echo $val_transportation->transportation_distance;
              if ($val_transportation->transportation_title !== null && $val_transportation->transportation_distance !== null) {
                  $transportation_id = (!empty($val_transportation->transportation_id)) ? $val_transportation->transportation_id : '-';
                  if ($transportation_id !== '0') {
                      $transportation_title = (!empty($val_transportation->transportation_title)) ? $val_transportation->transportation_title : '-';
                      $transportation_category_title = (!empty($val_transportation->transportation_category_title)) ? $val_transportation->transportation_category_title : '-';
                      $transportation_distance = (!empty($val_transportation->transportation_distance)) ? $val_transportation->transportation_distance : '-';
                      ?>
					<div class="row clearfix mar-top-medium">
						<div class="small-12 columns">
							<p class="color-red"><?php echo $transportation_category_title.' : '.$transportation_title;
                      ?></p><i class="fa fa-male"></i> <?php echo $transportation_distance;
                      ?>
						</div>
					</div>
					<?php

                  }//END IF
              }//END IF
                        }//END FOREACH
                    ?>
					<?php

                    } else {
                        ?>
					<div class="row clearfix mar-top-medium">
						<div class="small-12 columns">
							<p class="color-red">ไม่มีข้อมูลสถานที่ใกล้เคียง</p>
						</div>
					</div>
					<?php

                    }//END IF
                    ?>


				</div>
				<div class="row clearfix">
                    <div class="columns mar-top-large">
					<?php
                        $banner_7 = $this->Page_model->get_banner_location(7);
                        $sfilepath_banner = base_url().'uploads/banner/';
                        $banner_count = count($banner_7);

                        $banner_7_arr = array();
                        for ($i = 0; $i < $banner_count;  ++$i) {
                            $banner_7_id = (!empty($banner_7[$i]->banner_id)) ? $banner_7[$i]->banner_id : '';

                            //UPDATE DISPLAY
                            $displayed = (!empty($banner_7[$i]->displayed)) ? $banner_7[$i]->displayed : '';
                            $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_7_id, 'banner');

                            $banner_7_title = (!empty($banner_7[$i]->banner_title)) ? $banner_7[$i]->banner_title : '';
                            $banner_7_url = (!empty($banner_7[$i]->banner_url)) ? $banner_7[$i]->banner_url : '#';
                            $banner_7_pic_thumb = (!empty($banner_7[$i]->pic_thumb)) ? $sfilepath_banner.$banner_7[$i]->pic_thumb : 'http://placehold.it//370x540&amp;text=/370x540/333';
                            $banner_7_arr[] = '<a href="'.$banner_7_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_7_id.'"><img src="'.$banner_7_pic_thumb.'" alt="'.$banner_7_title.'" title=""></a>';
                        }
                        shuffle($banner_7_arr);
                        echo (!empty($banner_7_arr[0])) ? $banner_7_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/370x540/333?text=370x540" alt="370x540" title="1400x175"></a>';
                    ?>
					</div>

				</div>
			</div>



			<!--CONTACTS-->
			<div id="contact-bottom" class="columns mar-top-medium clearfix">
				<!-- <div class="google-maps">
				</div> -->



				<form action="" method="post" role="form" name="form_contact_bottom" id="form_contact_bottom" class="form-contact">
					<input type="hidden" name="staff_id" id="staff_id" value="<?php echo $staff_id; ?>">
					<div class="row">
						<h3>ติดต่อตัวแทนเกี่ยวกับทรัพย์นี้</h3>
						<div class="small-12 large-6 columns">
							<div class="row clearfix">
								<div class="small-12 large-5 columns">
									<label for="left-label" class="f_size1">ชื่อ <span class="color-red">*</span></label>
								</div>
								<div class="small-12 large-7 columns">
									<div class="input-group">
										<input type="text" name="condo_share_form_name" id="condo_share_form_name" class="input-group-field" required>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="small-12 large-5 columns">
									<label for="left-label" class="f_size1">เบอร์โทร <span class="color-red">*</span></label>
								</div>
								<div class="small-12 large-7 columns">
									<div class="input-group">
										<input type="text" name="condo_share_form_phone" id="condo_share_form_phone" class="input-group-field" required>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="small-12 large-5 columns">
									<label for="left-label" class="f_size1">อีเมล์ <span class="color-red">*</span></label>
								</div>
								<div class="small-12 large-7 columns">
									<div class="input-group">
										<input type="email" name="condo_share_form_email" id="condo_share_form_email" class="input-group-field" required>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="small-12 large-5 columns">
									<label for="left-label" class="f_size1">ข้อความ <span class="color-red">*</span></label>
								</div>
								<div class="small-12 large-7 columns">
									<div class="input-group">
										<textarea id="condo_share_form_message" name="condo_share_form_message" cols="10" rows="3"></textarea>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="small-12 large-5 hide hide-for-small columns">&nbsp;</div>
								<div class="small-12 large-7 columns">
									<div class="input-group">
										<input id="enewsletter" type="checkbox" name="enewsletter" value="1">
										<label for="enewsletter">รับจดหมายข่าวสารอสังหาริมทรัพย์ จากเรา</label>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="small-12 large-5 hide hide-for-small  columns">&nbsp;</div>
								<div class="small-12 large-7 columns">
									<div class="input-group">
										<button name="submit_contact" value="action" class="button button-red-white expanded">ติดต่อตัวแทน</button>
										<input type="hidden" name="fk_condo_id" value="<?php echo $fk_condo_id; ?>">
										<input type="hidden" name="fk_member_id" value="<?php echo $fk_member_id; ?>">
										<input type="hidden" name="fk_staff_id" value="<?php echo $staff_ids; ?>">

									</div>
								</div>
							</div>
						</div>
						<div class="small-12 large-6 columns align-middle">
							<?php
                                if (isset($show_sale)) {
                                    ?>
								<div class="row align-middle">
									<div class="small-4 columns">
										<a href="#"><img src="http://placehold.it/200x100&amp;text=Who?" alt=""></a>
									</div>
									<div class="small-8 column align-self-middle">
										<p class="text-center">
											<?php echo $staff_fullname; ?>
										</p>
										<p class="text-center">
											<?php echo $staff_email; ?>
										</p>
									</div>
								</div>
								<div class="column align-middle title-blue-bg">
									<div class="small-4 columns"><i class="fa fa-phone sidebar"></i></div>
									<div class="small-8 column">
										<p class="color-white">
											<?php echo $staff_phone;?>
										</p>
									</div>
								</div>
								<?php

                                } else {
                                    ?>
									<div class="row align-middle">
										<div class="small-12 column align-self-middle">
											<p class="text-center">กรอกฟอร์มเพื่อรับข้อมูลการติดต่อตัวแทน</p>
										</div>
									</div>
									<div class="column align-middle title-blue-bg">
										<div class="small-12 column">
											<p class="color-white">กรอกฟอร์มเพื่อรับข้อมูลการติดต่อตัวแทน</p>
										</div>
									</div>
									<?php }//END IF SHOW ?>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

  <script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyDtK77y4LHtYZ5ZoVeVVwpkMUoy-Eq9JRA"></script>
  <!-- <script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyDtK77y4LHtYZ5ZoVeVVwpkMUoy-Eq9JRA&sensor=false"></script> -->
  <script>
      // In the following example, markers appear when the user clicks on the map.
      // Each marker is labeled with a single alphabetical character.
      var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      var labelIndex = 0;
      var map;

      function initialize() {
          var bangkok = {
              lat: <?php echo $latitude; ?>,
              lng: <?php echo $longitude; ?>
          };
          map = new google.maps.Map(document.getElementById('map-canvas'), {
              zoom: 11,
              center: bangkok
          });


          var marker = new google.maps.Marker({
              draggable: true,
              position: bangkok,
              map: map,
              title: "Your location"
          });



          google.maps.event.addListener(marker, 'dragend', function (event) {
              //console.log( 'latitude : ' + event.latLng.lat()  );
              //console.log( 'longtitude : ' + event.latLng.lng()  );
              $('#latitude').val(event.latLng.lat());
              $('#longitude').val(event.latLng.lng());
          });
      }

      // Adds a marker to the map.
      var i = 0;

      function addMarker(location, map) {
          // Add the marker at the clicked location, and add the next-available label
          // from the array of alphabetical characters.
          var iMax = 1;
          if (i < iMax) {
              var marker = new google.maps.Marker({
                  draggable: true,
                  position: location,
                  label: labels[labelIndex++ % labels.length],
                  map: map,
                  title: "Your location"
              });
              i++;
          } else {
              console.log('you can only post', i - 1, 'markers');
          }
          //map.panTo(location);
      }

      google.maps.event.addDomListener(window, 'load', initialize);

      // function loadScript() {
      //     var script = document.createElement('script');
      //     script.type = 'text/javascript';
      //     script.src = 'https://maps.googleapis.com/maps/api/js?AIzaSyDtK77y4LHtYZ5ZoVeVVwpkMUoy-Eq9JRA&v=3.exp&sensor=false&callback=initialize';
      //     document.body.appendChild(script);
      // }

      // window.onload = loadScript;
  </script>


	<?php $this->load->view('partials/footer');?>



  <!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
  <script>
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '176921789401784',
        xfbml      : true,
        version    : 'v2.7'
      });
      FB.AppEvents.logPageView();
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
  </script>
<script>
$('#fb-share').click(function(){
  var condo_id = $('#condo_id').val();
  var condo_title = $('#condo_title').val();
  var pic_thumb = $('#pic_thumb').val();
  var url_facebook_share = $('#url_facebook_share').val();
  FB.ui({
    method: 'feed',
    display: 'popup',
    link : url_facebook_share,
    picture : pic_thumb,
    name   : condo_title,
    caption: 'WWW.CONDO-COMPARE.COM', //งงเด้ งงเด้ บางอันได้บางอันกูก็งงเด้
   //  mobile_iframe: true,
   //  href: url_share,
  }, function(response){

  });

});

 $('#fb-share-button').click(function(){
   var condo_id = $('#condo_id').val();
   var condo_title = $('#condo_title').val();
   var pic_thumb = $('#pic_thumb').val();
   var url_share = $('#url_share').val();
   var url_member_share = $('#url_member_share').val();
   var member_id = $('#member_id').val();

  FB.ui({
      method: 'share',
      display: 'popup',
    app_id		: '176921789401784',
    redirect_uri: url_member_share,
    href		: url_share,
    quote		: condo_title,
    picture : pic_thumb,
    title   : condo_title,
    name   	: condo_title,
    caption	: 'WWW.CONDO-COMPARE.COM',
    mobile_iframe: true,
   }, function(response){
     if( response && !response.error_message ){

       $.ajax({
          url: url_member_share,
          type: "POST",
          data: {
             id: condo_id,
             member_id: member_id
          },
          success: function (data) {
            // console.log(data);
            if( data.success === true ){


              $('#message').html(data.message);
              $('#panel-message').show();

							$('.fa').addClass('fa-check-circle');
		          setInterval(function(){
		            window.location.reload();
		          }, 3000);
              return true;
            }else{
              $('#message').html(data.message);
              $('#panel-message').show();
              $('.fa').addClass('fa-exclamation-triangle');
		          setInterval(function(){
		            window.location.reload();
		          }, 3000);
              return false;
            }
          },
          error: function () {
            console.log("failure");
          }
       });

     }else{

         $('#message').html('เกิดข้อผิดพลาดในการแชร์...');
         $('.fa').addClass('fa-exclamation-triangle');
         $('#panel-message').show();

     }
   });

 });


</script>

	<!-- <script src="https://maps.googleapis.com/maps/api/js?AIzaSyDtK77y4LHtYZ5ZoVeVVwpkMUoy-Eq9JRA&sensor=false"></script> -->


		<script src="<?php echo base_url()?>assets/js/doughnutit.js"></script>
		<script>

			$("#form_contact_bottom, #form_contact_sidebar").submit(function (event) {
				event.preventDefault();
				//var data = $('#form_register').serialize();
				var data = new FormData($(this)[0]);
				var url = '<?php echo site_url('page/m_condo_email_success'); ?>';
				$.ajax({
					method: 'POST',
					url: url,
					data: data,
					cache: false,
					contentType: false,
					processData: false,
					success: function (data) {
						// console.log(data);
						if (data == 'email_error') //Password Not Match!
						{
							swal({
								title: "เกิดข้อผิดพลาด!",
								text: "กรุณาติดต่อเจ้าหน้าที่หรือตัวแทนขายอสังหสริมทรัพย์",
								type: "warning",
							});
						} else if (data == 'success') {
							swal({
									title: "สำเร็จ",
									text: "ระบบได้ทำการส่งอีเมล์ การติดต่อของท่านเรียบร้อยแล้ว..",
									type: "info",
								},
								function (isConfirm) {
									window.location.replace('<?php echo base_url(uri_string()); ?>/?show=true');
								});
						}
					},
					error: function () {
						console.log("failure");
					}

				});
				return false;
			}); //END FORM SUBMIT



		</script>
		<script>
			$(document).ready(function () {

				$(".hob-wishlist").click(function (event) {
					var condo_id = $(this).attr("id");
					$.ajax({
						url: "<?php echo base_url(); ?>page/wishlist_condo",
						type: "POST",
						data: {
							condo_id: condo_id,
						},
						//async: false,
						success: function (data, status) {
							console.log(data);
							if (data == 'insert') {
								$("#" + condo_id + ".hob-wishlist").children().css('color', 'green');
							} else if (data == 'delete') {
								$("#" + condo_id + ".hob-wishlist").children().css('color', '');
							}

						},
						error: function (xhr, desc, err) {
							console.log(err);
						},
					});
					event.preventDefault();
					return false;
				});

				$(".hob-compare").click(function (event) {
					//alert('ssss');
					var condo_id = $(this).attr("id");
					$.ajax({
						url: "<?php echo base_url(); ?>page/compare_condo",
						type: "POST",
						data: {
							condo_id: $(this).attr("id"),
						},
						success: function (data, status) {
							//console.log(data);
							if (data == 'insert') {
								$("#" + condo_id + ".hob-compare").children().css('color', 'green');
							} else if (data == 'delete') {
								$("#" + condo_id + ".hob-compare").children().css('color', '');
							}
						},
						error: function (xhr, desc, err) {
							console.log(err);
						},
					});
					event.preventDefault();
					return false;
				});


        $("#submit_calculator").click(function (event) {

          var data_loan = $( "form#form_cal_loan" ).serialize();

          var loan_price = $('#loan_price').val();
          var loan_years = $('#loan_years').val();
          var loan_interest = $('#loan_interest').val();

          if( loan_price === '' || loan_price === '0' )
          {
            alert('กรณีระบุข้อมูลการคำนวณให้ครบถ้วน');
            return false;
          }else if( loan_years === '' || loan_years === '0' ){
            alert('กรณีระบุข้อมูลการคำนวณให้ครบถ้วน');
            return false;
          }else if( loan_interest === ''  || loan_interest === '0'  ){
            alert('กรณีระบุข้อมูลการคำนวณให้ครบถ้วน');
            return false;
          }else {
            $.ajax({
  						url: "<?php echo base_url(); ?>page/calculate_condo_loan",
  						type: "POST",
  						data: data_loan,
  						success: function (data, status) {
  							// console.log(data);
                $("#pay_per_month").css('display', 'block');
                $("#pay_per_month").html("฿ "+ data +" ต่องวด");
  						},
  						error: function (xhr, desc, err) {
  							console.log(err);
  						},
  					});
  					event.preventDefault();
  					return true;
          }

				});


			});
		</script>
