<?php
defined('BASEPATH') or exit();
/*HEADER*/
$this->load->view('partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'BodyClass' => ''));
?>

	<div id="container">
	<div class="bg-cs-gray-second">
		<div class="row align-middle">
			<div class="columns">
				<nav aria-label="You are here:" role="navigation">
					<?php echo $breadcrumbs; ?>
				</nav>
			</div>
		</div>
	</div>
	<div class="bg-cs-gray clearfix">
		<h3 class="text-center color-blue page-title"><?php echo $title; ?></h3>
	</div>
	<div class="row content-inside">
		<!--LEFT-->
		<div class="columns">

			<form action="" method="post" id="form_login" autocomplete="off">
				<div class="well-gray">


					<hr class="clearfix">

					<div class="row">
					<div class="small-12 medium-12 column mar-top-small">
						<p class="text-center warning">
							กรอกอีเมล์ เพื่อทำการรับลิ้งค์ ในการแก้ไขรหัสผ่าน
						</p>
						<label for="email">อีเมล์</label>
						<div class="input-group">
							<input type="email" name="member_email" id="member_email" placeholder="Email" required>
						</div>
						<button name="action" class="button button-red-white">ส่งอีเมล์</button>
						<input type="hidden" name="action" value="action_forgot">
					</div>

					</div>


				</div>
			</form>
		</div>

		<!--RIGHT-->
	</div>
</div>
	<?php $this->load->view('partials/footer', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'BodyClass' => ''));?>
	<script>
	$("#form_login").submit(function (event) {
		event.preventDefault();

		//var inst = $('[data-remodal-id=modal2]').remodal();
		var data = $(this).serialize();
		console.log(data);

		var url = '<?php echo site_url('page/m_forgotpassword'); ?>';
		$.ajax({
			method: 'POST',
			url: url,
			data: data,
			success: function (data) {
				console.log(data);
				// if( data == 'failed_1' )//Repeat Email Acc.
				// {
				// 	swal({
				// 		title: 	"เกิดข้อผิดพลาด!",
				// 		text: 	"ท่านกรอกอีเมล์ ไม่ถูกต้อง หรือไม่พบอีเมล์ในระบบ",
				// 		type: 	"warning",
				// 	});
				// 	return false;
				// }
				// else if ( data == 'success' )
				// {
					swal({
						title: 	"สำเร็จ",
						text: 	"ระบบได้ทำการส่งอีเมล์เพื่อทำการเปลี่ยนรหัสผ่านให้ท่านแล้วค่ะ ..",
						type: 	"info",
					},
						function(isConfirm){
							window.location.replace('<?php echo base_url(); ?>condominium');
					});
					return true;
				// }


			},
			error: function () {
				console.log("failure");
			}
			//window.location.replace = 'home';
		});
		return false;
	});
	</script>
