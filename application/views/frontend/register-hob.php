<?php
defined('BASEPATH') OR exit();
/*HEADER*/
$this->load->view('partials/header',array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>''));
?>
<style>
	#cropContaineroutput {
		width: 100%;
		height: 300px;
		position:relative; /* or fixed or absolute */
		margin: 0 auto;
		border: 1px solid #ccc;
	}
</style>
	<div id="container">
		<div class="bg-cs-gray-second">
			<div class="row align-middle">
				<div class="columns">
					<nav aria-label="You are here:" role="navigation">
						<?php echo $breadcrumbs; ?>
					</nav>
				</div>
			</div>
		</div>
		<div class="bg-cs-gray clearfix">
			<h3 class="text-center color-blue page-title"><?php echo $title; ?></h3>
		</div>
		<div class="row content-inside">
			<!--Tabs-->
			<div class="columns">
				<div class="well-gray">
					<ul id="example-tabs" data-tabs="" class="tabs">
						<li class="tabs-title no-float is-active small-12 medium-12 columns">
							<h4><a href="#condo" aria-selected="true"><?php echo $title; ?></a></h4>
						</li>
						<!--
						<li class="tabs-title no-float small-12 medium-6 columns">
							<h4><a href="#landforsale">Land for sale</a></h4>
						</li>
-->
					</ul>
					<div data-tabs-content="example-tabs" class="tabs-content bg-cs-gray">
						<form action="" method="post" id="form_register" enctype="multipart/form-data">
							<div id="condo" class="tabs-panel is-active">
								<div class="row">
									<div class="small-12 columns">
										<label for="email" class="clearfix">อีเมล์ / ชื่อผู้ใช้งาน</label>
										<div class="input-group">
											<input type="email" name="member_email" id="member_email" placeholder="Email" required>
										</div>
									</div>
								</div>
								<!-- end col -->

								<div class="row">
									<div class="small-12 medium-6 columns">
										<label for="member_fname" class="clearfix">ชื่อ</label>
										<div class="input-group">
											<input type="text" name="member_fname" id="member_fname" placeholder="ชื่อ" required>
										</div>
									</div>
									<div class="small-12 medium-6 columns">
										<label for="member_lname" class="clearfix">นามสกุล</label>
										<div class="input-group">
											<input type="text" name="member_lname" placeholder="นามสกุล">
										</div>
									</div>
								</div>
								<!-- end col -->

								<div class="row">
									<div class="small-12 medium-6 columns">
										<label for="member_mobileno" class="clearfix">เบอร์มือถือ / เบอร์ติดต่อ</label>
										<div class="input-group">
											<input type="text" name="member_mobileno"  placeholder="Mobile">
										</div>
									</div>
									<!--<div class="small-12 medium-6 columns">
										<label for="member_lname" class="clearfix">รูปภาพประจำตัว</label>
										<div class="input-group">

											<label for="member_profile" class="button">อัฟโหลดรูปภาพ</label>
											<input type="file" name="member_profile" id="member_profile" class="show-for-sr">

										</div>

									</div>-->
								</div>
								<!-- end col -->

								<div class="row">
									<div class="small-12 medium-6 columns">
										<span class="warning label"><i class="fa fa-exclamation"></i> อัฟโหลดภาพได้เฉพาะ .jpg | .jpeg | .png | .gif ความกว้าง ยาวของภาพไม่เกิน 600X300 และขนาดไม่เกิน 1MB.</span>
									</div>
								</div>
								<!-- end col -->

								<div class="row  mar-top-medium">
									<div class="small-12 medium-6 columns">
										<input type="file" id="member_profile" name="member_profile" class="">
									</div>
								</div>
								<!-- end col -->

								<div class="row">
									<div class="small-12 medium-12 columns">
										<label for="email" class="clearfix">ที่อยู่</label>
										<div class="input-group">
											<textarea id="member_address" name="member_address" cols="20" rows="3"></textarea>
										</div>
									</div>
								</div>
								<!-- end col -->


								<div class="row">
									<div class="small-12 medium-4 columns">
										<label for="provinces_id" class="clearfix">จังหวัด</label>
										<div class="input-group">
											<select id="provinces_id" class="" name="provinces_id" onchange="(this)">
												<option value="0">----เลือก----</option>
												<?php
														foreach ($provinces as $row) {//START FOREACH
													?>
													<option value="<?php echo $row->provinces_id; ?>">
														<?php echo $row->provinces_name; ?>
													</option>
													<?php
														}//END FOREACH
													?>
											</select>
										</div>
									</div>
									<div class="small-12 medium-4 columns">
										<label for="districts_id" class="clearfix">อำเภอ / เขต</label>
										<div class="input-group">
											<select id="districts_id" class="" name="districts_id" onchange="(this)">
												<option value="0">----เลือก----</option>
											</select>
										</div>
									</div>
									<div class="small-12 medium-4 columns">
										<label for="sub_districts_id" class="clearfix">ตำบล / แขวง</label>
										<div class="input-group">

											<select id="sub_districts_id" class="form-control" name="sub_districts_id" onchange="(this)">
												<option value="0">----เลือก----</option>
											</select>
										</div>
									</div>
								</div>
								<!-- end col -->

								<div class="row">
									<div class="small-12 medium-12 columns">
										<label for="zipcode" class="clearfix">รหัสไปรษณีย์</label>
										<div class="input-group">
											<input type="text" maxlength="5" name="zipcode" placeholder="Zipcode">
										</div>
									</div>
								</div>
								<!-- end col -->



								<div class="row">
									<div class="small-12 medium-6 columns">
										<label for="password" class="clearfix">รหัสผ่าน <span class="color-red">*</span></label>
										<div class="input-group">
											<input type="password" name="member_password" id="member_password" placeholder="Password" required>
										</div>
									</div>
									<div class="small-12 medium-6 columns">
										<label for="password" class="clearfix">ยืนยันรหัสผ่าน</label>
										<div class="input-group">
											<input type="password" name="cf_member_password" id="cf_member_password" placeholder="Confirm Password" required>
										</div>
									</div>
								</div>
                <div class="row">
                  <div class="medium-12 align-center columns">
                    <div class="well overflow-x-scroll">
                      <h4>เงื่อนไขและข้อตกลงในการจ่ายผลตอบแทน(คอมมิชชั่น)</h4>
                      <p>ยินดีต้อนรับทุกท่าน โปรดอ่านข้อตกลงในการทำงานร่วมกันและเงื่อนไขการจ่ายผลตอบแทน(คอมมิชชั่น)</p>
                      <ul class="no-bullet">
                        <li>1. บริษัท ฮาริสัน จำกัด (มหาชน) เป็นบริษัทที่ให้บริการในด้านอสังหาริมทรัพย์ อาทิ ตัวแทนบริหารงานขายโครงการคอนโดมิเนียม นายหน้าซื้อขายอสังหาริมทรัพย์ ที่ปรึกษาด้านการตลาดด้านอสังหาริมทรัพย์ เป็นต้นรวมทั้งเป็นเจ้าของระบบการทำงาน “HOB”ในข้อตกลงนี้เรียกว่า “บริษัท”</li>
                        <li>2. สิทธิ หน้าที่ และความรับผิดชอบของ HOB
                          <ul>
                            <li>2.1 HOB หรือ HARRISON ONLINE BROKERหมายถึง บุคคลทั่วไปที่เข้ามาเป็นสมาชิกนายหน้าอิสระกับ “บริษัท” ในระบบการทำงานที่เรียกว่า “HOB” โดยสมาชิกแต่ละท่าน ไม่ถือว่าเป็นพนักงานของ “บริษัท” แต่อย่างไรการลงทะเบียนขอสมัครเป็นสมาชิกนายหน้าอิสระ ผู้สมัครจะต้องให้ข้อมูลส่วนบุคคลของตนเองอย่างถูกต้อง สมบูรณ์ และทันสมัยที่สุด รวมถึงอัพเดทข้อมูลส่วนบุคคลเมื่อมีการเปลี่ยนแปลงด้วย</li>
                            <li>2.2 ในกรณีที่สมาชิกนายหน้าอิสระก่อให้เกิดความเสียหายต่อ ”บริษัท” หรือบุคคลใด ๆ ก็ตาม สมาชิกนายหน้าอิสระตกลงที่จะรับผิดชอบต่อความเสียหายดังกล่าวที่เกิดขึ้นเต็มจำนวน</li>
                            <li>2.3 “บริษัท” สงวนสิทธิที่จะเพิกถอนการเป็นสมาชิกนายหน้าอิสระได้ทันทีโดยไม่ต้องแจ้งให้ทราบล่วงหน้าหากพบว่ามีการกระทำดังต่อไปนี้
                              <ul>
                                <li>- สมาชิกนายหน้าอิสระให้ข้อมูลที่เป็นเท็จในการลงทะเบียน</li>
                                <li>- สมาชิกนายหน้าอิสระกระทำการไม่เหมาะสม ขัดต่อกฎหมายหรือศีลธรรมอันดีของประชาชน หรือมีเหตุอันควรเชื่อได้ว่ามีเจตนาทุจริต</li>
                              </ul>
                            </li>
                            <li>2.4 สมาชิกนายหน้าอิสระ ไม่สามารถรับเงินจอง หรือเงินมัดจำ จากผู้จะซื้อ ในทุกๆ กรณี</li>
                          </ul>
                        </li>
                        <li>3. การจำกัดความรับผิดชอบ
                          <ul>
                            <li>3.1 ถึงแม้ “บริษัท” จะทำการตรวจสอบความถูกต้องของทรัพย์และอัพเดทข้อมูลอยู่เป็นประจำก็ตาม แต่ “บริษัท” ก็ไม่สามารถรับประกันว่าข้อมูลดังกล่าวมีความถูกต้อง สมบูรณ์เพียงพอหรือทันกาลเวลา ดังนั้น “บริษัท” จึงไม่อาจรับผิดชอบไม่ว่าทางตรง หรือทางอ้อม สำหรับความผิดพลาด ความล่าช้าของข้อมูลการขาดความต่อเนื่องของการเชื่อมโยงอุปกรณ์ ไม่ว่าจะมีผู้ใดได้แจ้งหรือแนะนำ “บริษัท” ก่อนหน้าเกี่ยวกับโอกาสที่จะเกิดปัญหาดังกล่าวหรือไม่ก็ตาม</li>
                          </ul>
                        </li>
                        <li>4. สิทธิของ “บริษัท”
                          <ul>
                            <li>4.1 “บริษัท” มีสิทธิในการแก้ไข เปลี่ยนแปลง หรือยกเลิกเงื่อนไขในข้อตกลงฉบับนี้ โดยไม่จำต้องแจ้งให้สมาชิกนายหน้าอิสระทราบล่วงหน้า</li>
                            <li>4. 2 สมาชิกนายหน้าอิสระตกลงและยอมรับว่าระบบการทำงาน เครื่องหมายการค้า แนวคิด โลโก้และรูปแบบในการนำเสนอ รวมถึงทุกส่วนของ software รวมถึง source code และ object code ที่พัฒนาขึ้นหรือกำลังพัฒนาขึ้นเพื่อใช้ในระบบการทำงานนี้ถือเป็นงานที่ได้รับความคุ้มครองโดยกฎหมายทรัพย์สินทางปัญญา ห้ามผู้ใดทำการลอกเลียนแบบ ทำซ้ำ หรือดัดแปลงไม่ว่าจะบางส่วนหรือทั้งหมด</li>
                          </ul>
                        </li>
                        <li>5. ทรัพย์ หมายถึง อสังหาริมทรัพย์ทุกชนิดที่ทาง “บริษัท” ให้สมาชิกนายหน้าอิสระช่วยกันทำงานผ่านระบบ “HOB”</li>
                        <li>6. เจ้าของ Stock หมายถึง เจ้าของกรรมสิทธิ์ในทรัพย์ชิ้นนั้น ๆ หรือ บุคคลอื่นใดที่ได้รับอนุญาตจากเจ้าของกรรมสิทธิ์ให้ขายได้ ไม่ว่าจะทางวาจาหรือลายลักษณ์อักษรก็ดี ซึ่งบุคคลอื่นใดนั้นอาจจะเป็นสมาชิกนายหน้าอิสระหรือไม่เป็นก็ได้ โดยนำทรัพย์มาฝากขายผ่านระบบ “HOB”</li>
                        <li>7. ทรัพย์ทุกรายการที่ “บริษัท” นำมาขายผ่านระบบ “HOB” ไม่ใช่ทรัพย์ของ “บริษัท” และทรัพย์นั้น ๆ ถูกกำหนดราคาโดยเจ้าของStock โดยเจ้าของStock จะต้องเป็นผู้รับผิดชอบในการชำระค่านายหน้าให้กับ “บริษัท”เพื่อที่ “บริษัท” จะนำไปจ่ายค่าตอบแทนให้กับสมาชิกนายหน้าอิสระต่อไป</li>
                        <li>8. ค่าตอบแทน (ค่าคอมมิชชั่น) สมาชิกนายหน้าอิสระที่ทำงานผ่านระบบ “HOB” จะได้รับค่าตอบแทน (ค่าคอมมิชชั่น) ตามสัดส่วนรายได้ที่ “บริษัท” ได้รับ มีรายละเอียดดังนี้
                          <ul>
                            <li>8.1 เจ้าของ Stock จะได้รับค่าตอบแทน จำนวน 25% จากสัดส่วนที่ “บริษัท” ได้รับค่านายหน้าจากทรัพย์ชิ้นนั้น ๆโดยผู้ที่ส่งข้อมูลก่อน จะได้รับสิทธิ์ในการติดต่อก่อนตามลำดับในกรณีที่ข้อมูลซ้ำกัน (ในกรณีที่ข้อมูลไม่ถูกต้อง”บริษัท”ขอสงวนสิทธิ์ในการติดต่อกับเจ้าของ Stock ลำดับถัดไป)</li>
                            <li>8.2 สมาชิกนายหน้าอิสระ หรือ HOBแชร์ข้อมูลของทรัพย์บนเว็บไซต์ www.condo-compare.com เมื่อมีผู้จะซื้อสนใจทรัพย์จากการแชร์ข้อมูลของสมาชิกนายหน้าอิสระ หรือ HOB ติดต่อกลับผ่านเว็บไซต์ www.condo-compare.comและสามารถปิดการขายได้ จะได้รับค่าตอบแทน 5% จากสัดส่วนที่ “บริษัท” ได้รับค่านายหน้าจากทรัพย์ชิ้นนั้น ๆ (หลังจากหักค่าสต็อคและค่าแชร์แล้ว ถ้ามี)</li>
                            <li>8.3 สมาชิกนายหน้าอิสระ หรือ HOB แนะนำให้ “บริษัท” รู้จักกับผู้จะซื้อโดยตรงและร่วมกันทำงานจนกระทั่งสามารถปิดการขายได้ จะได้รับค่าตอบแทนจำนวน 50% จากสัดส่วนที่ “บริษัท” ได้รับค่านายหน้าจากทรัพย์ชิ้นนั้น ๆ(หลังจากหักค่าสต็อคและค่าแชร์แล้ว ถ้ามี)</li>
                          </ul>
                        </li>
                      </ul>
                      <strong><u>หมายเหตุ</u>  สมาชิกนายหน้าอิสระจะได้รับค่าตอบแทน ก็ต่อเมื่อสามารถปิดการขายได้ภายใน 60 วัน นับตั้งแต่วันที่ได้รับอีเมล์ยืนยันการแชร์ข้อมูล และ”บริษัท” ได้รับค่านายหน้าครบถ้วน</strong>
                    </div>
                  </div>

                  <div class="medium-12 columns">
										<div class="input-group">
                      <fieldset>
                        <!-- <legend>ยอมรับเงื่อนไขการสมัครสมาชิก</legend> -->
                        <input id="confirm_hob" name="confirm_hob" type="checkbox" required=""><label for="checkbox1">ยอมรับเงื่อนไขการสมัครสมาชิก</label>
                      </fieldset>
										</div>
									</div>
                </div>

								<div class="row">
									<div class="small-12 medium-4 align-center columns">
										<div class="input-group">
											<button name="action_condo_register" id="" class="button button-red-white expanded">Submit</button>
											<input type="hidden" name="member_type_id" value="<?php echo $member_type_id; ?>">
											<input type="hidden" name="approved" value="<?php echo $approved; ?>">
										</div>
									</div>
								</div>

							</div>
						</form>

					</div>
				</div>
			</div>
		</div>
	</div>

	<?php $this->load->view('partials/footer', array('title'=>isset($title)?$title:'','css'=>isset($css)?$css:array(),'BodyClass'=>''));?>
		<script>





			$("#form_register").submit(function (event) {
				event.preventDefault();
//				var data = $('#form_register').serialize();

				var data = new FormData($(this)[0]);
//				console.log(data);
				var email = $('#member_email').val();
				var fname = $('#member_fname').val();
				var password = $('#member_password').val();
				var cf_password = $('#cf_member_password').val();

				if( email === '' )
				{
					swal({
						title: 	"เกิดข้อผิดพลาด!",
						text: 	"กรุณากรอกข้อมูลให้ถูกต้องและครบถ้วน.....",
						type: 	"warning",
					});
					return false;
				}
				else if( password === '' )
				{
					swal({
						title: 	"เกิดข้อผิดพลาด!",
						text: 	"กรุณากรอกข้อมูลให้ถูกต้องและครบถ้วน.....",
						type: 	"warning",
					});
					return false;
				}
				else if( password === '' )
				{
					swal({
						title: 	"เกิดข้อผิดพลาด!",
						text: 	"กรุณากรอกข้อมูลให้ถูกต้องและครบถ้วน.....",
						type: 	"warning",
					});
					return false;
				}
				else if( cf_password === '' )
				{
					swal({
						title: 	"เกิดข้อผิดพลาด!",
						text: 	"กรุณากรอกข้อมูลให้ถูกต้องและครบถ้วน.....",
						type: 	"warning",
					});
					return false;
				}
				else
				{
					var url = '<?php echo site_url('page/m_register_success'); ?>';
					$.ajax({
						method: 'POST',
						url: url,
						data: data,
						cache: false,
						contentType: false,
						processData: false,
						success: function (data) {
							//console.log(data);
							if( data == 'passwordnotmatch' ) //Password Not Match!
							{
								swal({
									title: 	"เกิดข้อผิดพลาด!",
									text: 	"รหัสผ่านไม่ตรงกัน",
									type: 	"warning",
								});
							}
							else if( data == '2' )//Repeat Email Acc.
							{
								swal({
									title: 	"เกิดข้อผิดพลาด!",
									text: 	"มีชื่ออีเมล์นี้ในระบบแล้ว",
									type: 	"warning",
								});
							}
							else if( data == 'duplicate' )//Repeat Email Acc.
							{
								swal({
									title: 	"เกิดข้อผิดพลาด!",
									text: 	"มีชื่ออีเมล์นี้ในระบบแล้ว",
									type: 	"warning",
								});
							}
							else if ( data == 'success' )
							{
								swal({
									title: 	"สำเร็จ",
									text: 	"ท่านได้สมัครสมาชิกเรียบร้อยแล้ว รอการอนุมัติการผู้ดูแลระบบ..",
									type: 	"info",
								},
									function(isConfirm){
										window.location.replace('home');
								});
							}

						},
						error: function () {
							console.log("failure");
						}

					});
					return false;
				}

			});//END FORM SUBMIT

			$("#provinces_id").change(function () {
				/*dropdown post */ //
				$.ajax({
					url: "<?php echo base_url(); ?>page/provinces_districts_relation",
					data: {
						id: $(this).val()
					},
					type: "POST",
					success: function (data) {
						$("#districts_id").html(data);
					}
				});
			});

			$("#districts_id").change(function () {
				/*dropdown post */ //
				$.ajax({
					url: "<?php echo base_url(); ?>page/districts_subdistricts_relation",
					data: {
						id: $(this).val()
					},
					type: "POST",
					success: function (data) {
						$("#sub_districts_id").html(data);
					}
				});
			});
		</script>


<script type="text/javascript">
$(document).ready(function() {
	$.uploadPreview({
		input_field: "#member_profile",
		preview_box: "#member_preview",
		no_label: true
	});
});
</script>

<script>
		var croppicContaineroutputOptions = {
				uploadUrl: '<?php echo base_url(); ?>main/img_save_file/member/',
				cropUrl: '<?php echo base_url(); ?>main/img_crop_file/member/',
				outputUrlId:'cropOutput',
				modal:false,
				loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
				onBeforeImgUpload: function(){ console.log('onBeforeImgUpload') },
				onAfterImgUpload: function(){ console.log('onAfterImgUpload') },
				onImgDrag: function(){ console.log('onImgDrag') },
				onImgZoom: function(){ console.log('onImgZoom') },
				onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
				onAfterImgCrop:function(){ console.log('onAfterImgCrop') },
				onReset:function(){ console.log('onReset') },
				onError:function(errormessage){ console.log('onError:'+errormessage) }
		}

		var cropContaineroutput = new Croppic('cropContaineroutput', croppicContaineroutputOptions);



	</script>
