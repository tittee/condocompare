<?php
defined('BASEPATH') or exit();
/*HEADER*/
$this->load->view('partials/header', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'BodyClass' => ''));

$sfilepath = base_url().'uploads/condominium';

$transportation_category_id = '';
$mass_transportation_category_id = '1';
$type_offering_id = '0';
$type_zone_id = '0';
?>


	<header id="header" class="header">
		<!-- <h1 class="text-center"><span class="color-white">Thailand’s Smartest</span><span class="color-red">  Property Search</span></h1> -->
		<div class="container show-for-small hide-for-large">
      <div class="row">
        <div class="columns">

					<ul class="accordion" data-accordion data-responsive-accordion-tabs="tabs medium-accordion large-tabs">
					  <li class="accordion-item is-active" data-accordion-item>
					    <a href="#tab-1m" class="accordion-title"><i class="fa fa-search color-red"></i> ค้นหาคอนโด</a>
					    <div id="tab-1m" class="accordion-content" data-tab-content>
								<form class="container" id="form_1" method="get" action="<?php echo site_url('condominium'); ?>">
									<div class="row">
								    <div class="small-3 columns">
								      <label for="search" class="text-right">คำค้นหา</label>
								    </div>
								    <div class="small-9 columns">
								      <input type="text" name="search" value="" class="" placeholder="พิมพ์คำค้นหา">
								    </div>
								  </div>
									<div class="clearfix"></div>
									<!-- END -->

									<div class="row">
								    <div class="small-3 columns">
								      <label for="min_price" class="text-right">ราคาต่ำสุด</label>
								    </div>
								    <div class="small-9 columns">
											<select name="min_price" value="min_price">
												<option value="">----เลือก----</option>
												<option value="1000000">ไม่ต่ำกว่า 1,000,000</option>
												<option value="2000000">ไม่ต่ำกว่า 2,000,000</option>
												<option value="3000000">ไม่ต่ำกว่า 3,000,000</option>
												<option value="4000000">ไม่ต่ำกว่า 4,000,000</option>
												<option value="5000000">ไม่ต่ำกว่า 5,000,000</option>
											</select>
								    </div>
								  </div>
									<div class="clearfix"></div>
									<!-- END -->

									<div class="row">
								    <div class="small-3 columns">
								      <label for="min_price" class="text-right">ราคาสูงสุด</label>
								    </div>
								    <div class="small-9 columns">
											<select name="max_price" value="max_price">
												<option value="">----เลือก----</option>
												<!-- <option value="5000000">ไม่เกิน 5,000,001</option> -->
												<option value="6000000">ไม่เกิน 6,000,000</option>
												<option value="7000000">ไม่เกิน 7,000,000</option>
												<option value="8000000">ไม่เกิน 8,000,000</option>
												<option value="9000000">ไม่เกิน 9,000,000</option>
												<option value="10000000">ไม่เกิน 10,000,000</option>
												<option value="11000000">ไม่เกิน 11,000,000</option>
												<option value="12000000">ไม่เกิน 12,000,000</option>
												<option value="13000000">ไม่เกิน 13,000,000</option>
												<option value="14000000">ไม่เกิน 14,000,000</option>
												<option value="15000000">ไม่เกิน 15,000,000</option>
												<option value="16000000">ไม่เกิน 16,000,000</option>
												<option value="17000000">ไม่เกิน 17,000,000</option>
												<option value="18000000">ไม่เกิน 18,000,000</option>
												<option value="19000000">ไม่เกิน 19,000,000</option>
												<option value="20000000">ไม่เกิน 20,000,000</option>
												<option value="20000001">ตั้งแต่ 20,000,001 ขึ้นไป</option>
											</select>
								    </div>
								  </div>
									<div class="clearfix"></div>
									<!-- END -->

									<div class="row">
								    <div class="small-3 columns">
								      <label for="max_price" class="text-right">ราคาสูงสุด</label>
								    </div>
								    <div class="small-9 columns">
											<select name="max_price" value="max_price">
												<option value="">----เลือก----</option>
												<!-- <option value="5000000">ไม่เกิน 5,000,001</option> -->
												<option value="6000000">ไม่เกิน 6,000,000</option>
												<option value="7000000">ไม่เกิน 7,000,000</option>
												<option value="8000000">ไม่เกิน 8,000,000</option>
												<option value="9000000">ไม่เกิน 9,000,000</option>
												<option value="10000000">ไม่เกิน 10,000,000</option>
												<option value="11000000">ไม่เกิน 11,000,000</option>
												<option value="12000000">ไม่เกิน 12,000,000</option>
												<option value="13000000">ไม่เกิน 13,000,000</option>
												<option value="14000000">ไม่เกิน 14,000,000</option>
												<option value="15000000">ไม่เกิน 15,000,000</option>
												<option value="16000000">ไม่เกิน 16,000,000</option>
												<option value="17000000">ไม่เกิน 17,000,000</option>
												<option value="18000000">ไม่เกิน 18,000,000</option>
												<option value="19000000">ไม่เกิน 19,000,000</option>
												<option value="20000000">ไม่เกิน 20,000,000</option>
												<option value="20000001">ตั้งแต่ 20,000,001 ขึ้นไป</option>
											</select>
								    </div>
								  </div>
									<div class="clearfix"></div>
									<!-- END -->

									<div class="row">
								    <div class="small-3 columns">
								      <label for="type_offering" class="text-right">ประเภท</label>
								    </div>
								    <div class="small-9 columns">
											<select name="type_offering" value="type_offering">
												<option value="">----เลือก----</option>
												<?php
                          foreach ($type_offering as $row) {
                          //START FOREACH
                          $sel = ($row->type_offering_id == $type_offering_id) ? 'selected' : '';
                        ?>
													<option value="<?php echo $row->type_offering_id;?>" <?php echo $sel;?>>
														<?php echo $row->type_offering_title;?>
													</option>
													<?php
												}//END FOREACH
												?>
											</select>
								    </div>
								  </div>
									<div class="clearfix"></div>
									<!-- END -->

									<div class="row">
								    <div class="small-3 columns">
								      <label for="type_suites" class="text-right">ประเภทห้อง</label>
								    </div>
								    <div class="small-9 columns">
											<select name="type_suites" value="type_suites">
												<option value="">----เลือก----</option>
												<?php
                          foreach ($type_suites as $row) {
                              //START FOREACH
                              $sel = ($row->type_suites_id == $type_suites_id) ? 'selected' : '';
                              ?>
													<option value="<?php echo $row->type_suites_id;?>" <?php echo $sel;?>>
														<?php echo $row->type_suites_title;?>
													</option>
													<?php
                              }//END FOREACH
                          ?>
											</select>
								    </div>
								  </div>
									<div class="clearfix"></div>
									<!-- END -->

									<div class="row">
								    <div class="small-3 columns">
								      <label for="condo_room" class="text-right">จำนวนห้อง</label>
								    </div>
								    <div class="small-9 columns">
											<select name="condo_room" value="condo_room">
												<option value="">----เลือก----</option>
												<option value="1">1 ห้องนอน</option>
												<option value="2">2 ห้องนอน</option>
												<option value="3">3 ห้องนอน</option>
											</select>
								    </div>
								  </div>
									<div class="clearfix"></div>
									<!-- END -->

									<div class="row">
								    <div class="small-3 columns">
								      <label for="condo_area_apartment" class="text-right">จำนวนห้อง</label>
								    </div>
								    <div class="small-9 columns">
											<select name="condo_area_apartment" value="condo_area_apartment">
												<option value="">----เลือก----</option>
												<option value="1">ต่ำกว่า 25 ตร.ม.</option>
												<option value="2">26 - 50 ตร.ม.</option>
												<option value="3">51 - 75 ตร.ม.</option>
												<option value="4">76 - 100 ตร.ม.</option>
												<option value="5">101 - 125 คร.ม.</option>
												<option value="6">126 - 150 ตร.ม.</option>
												<option value="7">151 - 175 ตร.ม.</option>
												<option value="8">176 - 200 ตร.ม.</option>
												<option value="9">201 ตร.ม.ขึ้นไป</option>
											</select>
								    </div>
								  </div>
									<div class="clearfix"></div>
									<!-- END -->

									<div class="row">
										<div class="columns small-12">
											<input type="submit" name="submit" class="button expanded button-red-white" value="ค้นหา" >
										</div>
									</div>
									<!-- END -->
								</form>
							<!-- END FORM -->
					    </div>
					  </li>
						<li class="accordion-item" data-accordion-item>
					    <a href="#tab-2m" class="accordion-title"><i class="fa fa-bus color-red"></i> ค้นหาจากระบบขนส่งมวลชน</a>
					    <div id="tab-2m" class="accordion-content" data-tab-content>
								<form class="container" id="form_1" method="get" action="<?php echo site_url('condominium'); ?>">
									<div class="row">
								    <div class="small-3 columns">
								      <label for="mobile_mass_transportation_category_id" class="text-right">ระบบขนส่งมวลชน</label>
								    </div>
								    <div class="small-9 columns">
											<select name="mass_transportation_category_id" class="mobile_mass_transportation_category_id">
												<option value="">----เลือก----</option>
													<?php
														foreach ($mass_transportation_category as $row) {
																//START FOREACH GROUP
																$sel = ($row->mass_transportation_category_id == $mass_transportation_category_id) ? 'selected' : '';
														?>
														<option value="<?php echo $row->mass_transportation_category_id; ?>"><?php echo $row->mass_transportation_category_title; ?></option>
														<?php
														}//END FOREACH GROUP
													?>
											</select>
								    </div>
								  </div>
									<div class="clearfix"></div>
									<!-- END -->

									<div class="row" id="bts_mobile">
								    <div class="small-3 columns">
								      <label for="bts" class="text-right"><strong id="mobile_mass_transportation_category_title">เลือกระบบขนส่งมวลชน</strong></label>
								    </div>
								    <div class="small-9 columns">

											<select name="mass_transportation_id" class="mobile_mass_transportation_id">
												<option value="">----เลือก----</option>
											</select>


								    </div>
								  </div>
									<div class="clearfix"></div>
									<!-- END -->


									<div class="row">
										<div class="columns small-12">
											<input type="submit" name="submit" class="button expanded button-red-white" value="ค้นหา" >
										</div>
									</div>
									<!-- END -->
								</form>
								<!-- END FORM -->
					    </div>
					  </li>
						<li class="accordion-item " data-accordion-item>
					    <a href="#tab-3m" class="accordion-title"><i class="fa fa-map-marker color-red"></i> ค้นหาจากทำเล</a>
					    <div id="tab-3m" class="accordion-content" data-tab-content>
								<form class="container" id="form_1" method="get" action="<?php echo site_url('condominium'); ?>">
									<div class="row">
								    <div class="small-3 columns">
								      <label for="type_zone_id" class="text-right">ค้นหาจากทำเล</label>
								    </div>
								    <div class="small-9 columns">
											<select name="type_zone_id" value="type_zone_id">
												<option value="">----เลือก----</option>
												<?php
                        foreach ($type_zone as $row) {
                            //START FOREACH
                            $sel = ($row->type_zone_id == $type_zone_id) ? 'selected' : '';
                            ?>
													<option value="<?php echo $row->type_zone_id;?>" <?php echo $sel;?>>
														<?php echo $row->type_zone_title;?>
													</option>
													<?php

                              }//END FOREACH
                          ?>

											</select>
								    </div>
								  </div>
									<div class="clearfix"></div>
									<!-- END -->

									<div class="row">
										<div class="columns small-12">
											<input type="submit" name="submit" class="button expanded button-red-white" value="ค้นหา" >
										</div>
									</div>
									<!-- END -->
								</form>
								<!-- END FORM -->
					    </div>
					  </li>
						<li class="accordion-item " data-accordion-item>
					    <a href="#tab-4m" class="accordion-title"><i class="fa fa-map-marker color-red"></i> ค้นหาสถานที่สำคัญ</a>
					    <div id="tab-4m" class="accordion-content" data-tab-content>
								<form class="container" id="form_1" method="get" action="<?php echo site_url('condominium'); ?>">
									<div class="row">
								    <div class="small-3 columns">
								      <label for="transportation_id" class="text-right">หมวดสถานที่</label>
								    </div>
								    <div class="small-9 columns">
											<select id="transportation_id" name="transportation_id" onchange="(this)">
												<option value="0">----เลือก----</option>
												<?php
		                      foreach ($transportation_category as $row) {
		                          //START FOREACH GROUP
		                          $sel = ($row->transportation_category_id == $transportation_category_id) ? 'selected' : '';

		                          if ($transportation_category != $row->transportation_category_id) {
		                              if ($transportation_category != '') {
		                                  echo '</optgroup>';
		                              }
		                              echo '<optgroup label="'.ucfirst($row->transportation_category_title).'">';
		                          }
		                          $transportation = $this->Condominium_model->get_transportation_option($row->transportation_category_id);
		                          foreach ($transportation as $row_in) {
		                              //START FOREACH
		                              echo '<option value="'.$row_in->transportation_id.'">'.$row_in->transportation_title.'</option>';
		                          }//END FOREACH
		                      ?>

													<?php

                          }//END FOREACH GROUP
                          if ($transportation_category != '') {
                              echo '</optgroup>';
                          }
                          ?>
											</select>
								    </div>
								  </div>
									<div class="clearfix"></div>
									<!-- END -->

									<div class="row">
										<div class="columns small-12">
											<input type="submit" name="submit" class="button expanded button-red-white" value="ค้นหา" >
										</div>
									</div>
									<!-- END -->
								</form>
								<!-- END FORM -->
					    </div>
					  </li>
					</ul>

        </div>
      </div>
    </div>
		<!--  END MOBILE -->

		<div class="container show-for-large">
			<div class="row">
				<div class="columns">
					<section class="tabs">
						<input type="radio" name="radio-set" id="tab-1" checked="checked" class="tab-selector-1">
						<label for="tab-1" class="tab-label-1 right"><i class="fa fa-search color-red"></i> ค้นหาคอนโด</label>
						<input type="radio" name="radio-set" id="tab-2" class="tab-selector-2">
						<label for="tab-2" class="tab-label-2 right"><i class="fa fa-bus color-red"></i> ค้นหาจากระบบขนส่งมวลชน</label>
						<input type="radio" name="radio-set" id="tab-3" class="tab-selector-3">
						<label for="tab-3" class="tab-label-3 right"><i class="fa fa-map-marker color-red"></i> ค้นหาจากทำเล</label>
						<input type="radio" name="radio-set" id="tab-4" class="tab-selector-4">
						<label for="tab-4" class="tab-label-4 right"><i class="fa fa-map-marker color-red"></i> ค้นหาสถานที่สำคัญ</label>
						<div class="clear-shadow"></div>
						<div class="content">
							<div class="content-1">
								<!-- <form class="container" id="form_1" method="get" action="<?php echo site_url('page/search'); ?>"> -->
								<form class="container" id="form_1" method="get" action="<?php echo site_url('condominium'); ?>">
									<p class="medium-12 columns">
									<label class="medium-12 columns" style="width: 100%;">
										<!-- <input type="hidden" name="action_search" value="action_search"> -->
										<input type="text" name="search" value="" class="medium-12 columns search-input" placeholder="พิมพ์คำค้นหา">
										<input type="submit" id="btn-search" name="submit" value="" class="sb-search-submit"><span class="sb-icon-search"></span>
									</label>
									</p>
									<p class="medium-4 columns"><strong>ราคาต่ำสุด</strong>
										<select name="min_price" value="min_price">
											<option value="">----เลือก----</option>
											<option value="1000000">ไม่ต่ำกว่า 1,000,000</option>
											<option value="2000000">ไม่ต่ำกว่า 2,000,000</option>
											<option value="3000000">ไม่ต่ำกว่า 3,000,000</option>
											<option value="4000000">ไม่ต่ำกว่า 4,000,000</option>
											<option value="5000000">ไม่ต่ำกว่า 5,000,000</option>
										</select>
									</p>
									<p class="medium-4 columns"><strong>ราคาสูงสุด</strong>
										<select name="max_price" value="max_price">
											<option value="">----เลือก----</option>
											<!-- <option value="5000000">ไม่เกิน 5,000,001</option> -->
											<option value="6000000">ไม่เกิน 6,000,000</option>
											<option value="7000000">ไม่เกิน 7,000,000</option>
											<option value="8000000">ไม่เกิน 8,000,000</option>
											<option value="9000000">ไม่เกิน 9,000,000</option>
											<option value="10000000">ไม่เกิน 10,000,000</option>
											<option value="11000000">ไม่เกิน 11,000,000</option>
											<option value="12000000">ไม่เกิน 12,000,000</option>
											<option value="13000000">ไม่เกิน 13,000,000</option>
											<option value="14000000">ไม่เกิน 14,000,000</option>
											<option value="15000000">ไม่เกิน 15,000,000</option>
											<option value="16000000">ไม่เกิน 16,000,000</option>
											<option value="17000000">ไม่เกิน 17,000,000</option>
											<option value="18000000">ไม่เกิน 18,000,000</option>
											<option value="19000000">ไม่เกิน 19,000,000</option>
											<option value="20000000">ไม่เกิน 20,000,000</option>
											<option value="20000001">ตั้งแต่ 20,000,001 ขึ้นไป</option>
										</select>
									</p>

									<p class="medium-4 columns"><strong>ประเภท</strong>
										<select name="type_offering" value="type_offering">
											<option value="">----เลือก----</option>
											<?php
                        foreach ($type_offering as $row) {
                            //START FOREACH
                            $sel = ($row->type_offering_id == $type_offering_id) ? 'selected' : '';
                            ?>
												<option value="<?php echo $row->type_offering_id;?>" <?php echo $sel;?>>
													<?php echo $row->type_offering_title;?>
												</option>
												<?php

                            }//END FOREACH
                        ?>
										</select>
									</p>
									<p class="medium-4 columns"><strong>ประเภทห้อง</strong>
										<select name="type_suites" value="type_suites">
											<option value="">----เลือก----</option>
											<?php
                        foreach ($type_suites as $row) {
                            //START FOREACH
                            $sel = ($row->type_suites_id == $type_suites_id) ? 'selected' : '';
                            ?>
												<option value="<?php echo $row->type_suites_id;?>" <?php echo $sel;?>>
													<?php echo $row->type_suites_title;?>
												</option>
												<?php
                            }//END FOREACH
                        ?>
										</select>
									</p>

									<p class="medium-4 columns"><strong>จำนวนห้อง</strong>
										<select name="condo_room" value="condo_room">
											<option value="">----เลือก----</option>
											<option value="1">1 ห้องนอน</option>
											<option value="2">2 ห้องนอน</option>
											<option value="3">3 ห้องนอน</option>
										</select>
									</p>

									<p class="medium-4 columns"><strong>ขนาดห้อง</strong>
										<select name="condo_area_apartment" value="condo_area_apartment">
											<option value="">----เลือก----</option>
											<option value="1">ต่ำกว่า 25 ตร.ม.</option>
											<option value="2">26 - 50 ตร.ม.</option>
											<option value="3">51 - 75 ตร.ม.</option>
											<option value="4">76 - 100 ตร.ม.</option>
											<option value="5">101 - 125 คร.ม.</option>
											<option value="6">126 - 150 ตร.ม.</option>
											<option value="7">151 - 175 ตร.ม.</option>
											<option value="8">176 - 200 ตร.ม.</option>
											<option value="9">201 ตร.ม.ขึ้นไป</option>
										</select>
									</p>

								</form>
							</div>

							<!-- TAB 2 -->
							<div class="content-2">
								<!-- <img src="<?php echo base_url(); ?>assets/images/home-filter-3.jpg" alt=""> -->
								<!-- <form class="container" id="form_1" method="get" action="<?php echo site_url('page/search'); ?>"> -->
								<form class="container" id="form_1" method="get" action="<?php echo site_url('condominium'); ?>">

									<p class="medium-4 columns"><strong>ระบบขนส่งมวลชน</strong>
										<select name="mass_transportation_category_id" class="mass_transportation_category_id">
											<option value="">----เลือก----</option>
												<?php
													foreach ($mass_transportation_category as $row) {
															//START FOREACH GROUP
															$sel = ($row->mass_transportation_category_id == $mass_transportation_category_id) ? 'selected' : '';
													?>
													<option value="<?php echo $row->mass_transportation_category_id; ?>"><?php echo $row->mass_transportation_category_title; ?></option>
													<?php
													}//END FOREACH GROUP
												?>
										</select>
									</p>
									<!-- BTS -->
									<p class="medium-6 columns" class="mass_transportation_id"><strong id="mass_transportation_category_title">เลือกระบบขนส่งมวลชน</strong>
										<select name="mass_transportation_id" class="mass_transportation_id">
											<option value="">----เลือก----</option>
										</select>
									</p>
									<p class="medium-2 columns">
										<button class="bg-cs-red btn-search-top button" href="#"><i class="fa fa-search"></i></button>
									</p>
								</form>
							</div>

							<div class="content-3">
								<!-- <img src="<?php echo base_url(); ?>assets/images/home-filter-2.jpg" alt=""> -->
								<form class="container" id="form_1" method="get" action="<?php echo site_url('condominium'); ?>">

									<p class="medium-10 columns"><strong>ค้นหาจากทำเล</strong>
										<select name="type_zone_id" value="type_zone_id">
											<option value="">----เลือก----</option>
											<?php
                                                    foreach ($type_zone as $row) {
                                                        //START FOREACH
                                                        $sel = ($row->type_zone_id == $type_zone_id) ? 'selected' : '';
                                                        ?>
												<option value="<?php echo $row->type_zone_id;
                                                        ?>" <?php echo $sel;
                                                        ?>>
													<?php echo $row->type_zone_title;
                                                        ?>
												</option>
												<?php

                                                    }//END FOREACH
                                                ?>

										</select>
									</p>
									<p class="medium-2 columns">
										<button class="bg-cs-red btn-search-top button" href="#"><i class="fa fa-search"></i></button>
									</p>
								</form>
							</div>

							<div class="content-4">
								<form class="container" method="get" action="<?php echo site_url('condominium'); ?>">
									<p class="medium-10 columns"><strong>หมวดสถานที่</strong>
										<select id="transportation_id" name="transportation_id" onchange="(this)">
											<option value="0">----เลือก----</option>
											<?php
												foreach ($transportation_category as $row) {
												    //START FOREACH GROUP
												    $sel = ($row->transportation_category_id == $transportation_category_id) ? 'selected' : '';

												    if ($transportation_category != $row->transportation_category_id) {
												        if ($transportation_category != '') {
												            echo '</optgroup>';
												        }
												        echo '<optgroup label="'.ucfirst($row->transportation_category_title).'">';
												    }
												    $transportation = $this->Condominium_model->get_transportation_option($row->transportation_category_id);
												    foreach ($transportation as $row_in) {
												        //START FOREACH
												        echo '<option value="'.$row_in->transportation_id.'">'.$row_in->transportation_title.'</option>';
												    }//END FOREACH
												?>

												<?php

												}//END FOREACH GROUP
												if ($transportation_category != '') {
												    echo '</optgroup>';
												}
												?>
										</select>
									</p>
									<p class="medium-2 columns">
										<button class="bg-cs-red btn-search-top button" href="#"><i class="fa fa-search"></i></button>
									</p>

								</form>

							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</header>

	<div class="container border-bottom">
		<div id="news" class="row display">
			<div class="title small-12 large-6 column display-right">
				<h2 class="left">รีวิว<span>ใหม่</span></h2>
				<div class="bg-cs-blue inside">
					<div class="location hover">
						<!-- REVIEWS -->
						<?php
                            $sfilepath_reviews = base_url().'uploads/reviews/';
                            $reviews_pic_large = (!empty($reviews->reviews_pic_large)) ? $sfilepath_reviews.$reviews->reviews_pic_large : 'http://placehold.it/1400x175&amp;text=HOB Share/890';
                            $reviews_id = (!empty($reviews->reviews_id)) ? $reviews->reviews_id : '-';
                            $reviews_title = (!empty($reviews->reviews_title)) ? $reviews->reviews_title : '-';
                            $reviews_caption = (!empty($reviews->reviews_caption)) ? $reviews->reviews_caption : '-';
                        ?>
						<figure><a href="<?php echo site_url('reviews-details/'.$reviews_id); ?>"><img src="<?php echo $reviews_pic_large; ?>" alt="<?php echo $reviews_title; ?>" class="float-center"></a></figure>
						<div class="caption">
							<h3><a href="<?php echo site_url('reviews-details/'.$reviews_id); ?>">รีวิวโครงการ: <?php echo $reviews_title; ?></a></h3>
							<p>
								<a href="<?php echo site_url('reviews-details/'.$reviews_id); ?>"><?php echo $reviews_caption; ?></a>
							</p>
						</div>
						<div><a href="<?php echo site_url('reviews'); ?>" class="button">ดูรีวิวทั้งหมด</a></div>
					</div>
				</div>
			</div>
			<div class="title small-12 large-6 column display-left">
				<h2 class="right">ข่าว<span>อสังหาฯ และบทความ</span></h2>
				<div class="bg-cs-gray inside">
					<article>
						<div class="row">
							<?php
                            foreach ($news as $value_news) {
                                $sfilepath_news = base_url().'uploads/news/';
                                $news_pic_thumb = (!empty($value_news->news_pic_thumb)) ? $sfilepath_news.$value_news->news_pic_thumb : 'http://placehold.it/1400x175&amp;text=HOB Share/120';
                                $news_id = (!empty($value_news->news_id)) ? $value_news->news_id : '0';
                                $news_title = (!empty($value_news->news_title)) ? $value_news->news_title : '-';
                                $news_slug = (!empty($value_news->news_slug)) ? $value_news->news_slug : $value_news->news_id;
                                $news_caption = (!empty($value_news->news_caption)) ? $value_news->news_caption : '-';
                                ?>
							<div class="small-12 columns hover-eff">
								<figure>
									<div class="new-list">
										<div class="small-12 large-4 columns">
											<p><a href="<?php echo site_url('news-details/'.$news_slug);
                                ?>"><img src="<?php echo $news_pic_thumb;
                                ?>" alt="<?php echo $news_title;
                                ?>" class="float-center"></a></p>
										</div>
										<div class="small-12 large-8 columns caption-list">
											<h3><a href="<?php echo site_url('news-details/'.$news_slug);
                                ?>"><?php echo $news_title;
                                ?></a></h3>
											<p><a href="<?php echo site_url('news-details/'.$news_slug);
                                ?>"><?php echo $news_caption;
                                ?></a></p>
										</div>
									</div>
									<div class="clearfix"></div>
								</figure>
							</div>
							<?php

                            }//END FOREACH ?>
							<div class="small-12 columns"><a href="<?php echo base_url('news'); ?>" class="button">ดูข่าวและบทความทั้งหมด</a></div>
						</div>
					</article>
				</div>
			</div>

			<!--  ID : 1 = banner หน้าแรกใต้ ข่าวสารและกิจกรรม -->
			<div class="column banner mar-top-small">
				<div class="">
					<div id="" class="banner-carousel owl-carousel owl-theme">

			<?php
                // $banner_1 = $this->Page_model->get_banner_location(1);
                // $sfilepath_banner = base_url().'uploads/banner/';
                // $banner_count = count($banner_1);
                                //
                // $banner_1_arr = array();
                // for ($i = 0; $i < $banner_count;  ++$i) {
                //     $banner_1_id = (!empty($banner_1[$i]->banner_id)) ? $banner_1[$i]->banner_id : '';
                                //
                //     //UPDATE DISPLAY
                //     $displayed = (!empty($banner_1[$i]->displayed)) ? $banner_1[$i]->displayed : '';
                //     $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_1_id, 'banner');
                                //
                //     $banner_1_title = (!empty($banner_1[$i]->banner_title)) ? $banner_1[$i]->banner_title : '';
                //     $banner_1_url = (!empty($banner_1[$i]->banner_url)) ? $banner_1[$i]->banner_url : '#';
                //     $banner_1_pic_thumb = (!empty($banner_1[$i]->pic_thumb)) ? $sfilepath_banner.$banner_1[$i]->pic_thumb : 'http://placehold.it/1400x175/333?text=1400x175';
                //     $banner_1_arr[] = '<a href="'.$banner_1_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_1_id.'"><img src="'.$banner_1_pic_thumb.'" alt="'.$banner_1_title.'" title=""></a>';
                // }

                $banner_1 = $this->Page_model->get_banner_location(1);
                $sfilepath_banner = base_url().'uploads/banner/';
                $banner_count = count($banner_1);

                // $banner_1_arr = array();
                for ($i = 0; $i < $banner_count;  ++$i) {
                    //UPDATE DISPLAY
                                        $banner_1_id = (!empty($banner_1[$i]->banner_id)) ? $banner_1[$i]->banner_id : '';
                    $displayed = (!empty($banner_1[$i]->displayed)) ? $banner_1[$i]->displayed : '';
                    $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_1_id, 'banner');

                    $banner_1_title = (!empty($banner_1[$i]->banner_title)) ? $banner_1[$i]->banner_title : '';
                    $banner_1_url = (!empty($banner_1[$i]->banner_url)) ? $banner_1[$i]->banner_url : '#';
                    $banner_1_pic_thumb = (!empty($banner_1[$i]->pic_thumb)) ? $sfilepath_banner.$banner_1[$i]->pic_thumb : 'http://placehold.it/1400x175/333?text=1400x175';
                                        // $banner_1_arr[] = '<a href="'.$banner_1_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_1_id.'"><img src="'.$banner_1_pic_thumb.'" alt="'.$banner_1_title.'" title=""></a>';
                    echo $banner_1_arr = '<div class="item"><a href="'.$banner_1_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_1_id.'"><img src="'.$banner_1_pic_thumb.'" alt="'.$banner_1_title.'" title=""></a></div>';
                }
                                // shuffle($banner_1_arr);
                // echo (!empty($banner_1_arr[0])) ? $banner_1_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/1400x175/333?text=1400x175" alt="1400x175" title="1400x175"></a>';
            ?>
					</div>
					<!-- END CAROUSEL -->
				</div>
			</div>

		</div>
	</div>

	<div class="container">
		<div id="highlight" class="row">
			<!-- <div class="sub-container"> -->
			<div class="title small-12 medium-4 large-9 column hover">
				<h1>โครงการใหม่<span>แนะนำ</span></h1>

				<!-- <div class="small-12 large-12 columns"> -->
					<div class="location display ">
						<figure>
							<a href="<?php echo site_url('condominium-details/'.$highlight_big->condo_id); ?>"><img src="<?php echo $pic_large = $sfilepath.'/'.$highlight_big->pic_large; ?>" alt="<?php echo $highlight_big->condo_title; ?>"></a></figure>
						<div class="caption">
							<h2><?php echo $highlight_big->condo_title; ?></h2>
							<p>
								<?php //echo $highlight_big->type_zone_title; ?>
								<?php
                  echo $this->primaryclass->condo_road_soi($highlight_big->condo_road,  $highlight_big->condo_alley);
                ?>
							</p>
						</div>
					</div>
				<!-- </div> -->
			<!-- </div> -->
			</div>
			<?php
            //HIGHLIGHT

            foreach ($highlight as $row => $value) {
                $pic_thumb = $sfilepath.'/'.$value->pic_thumb;

                $type_offering_id = (!empty($value->type_offering_id)) ? $value->type_offering_id : '0';
                if ($type_offering_id !== '' || $type_offering_id !== '0') {
                    $type_offering = $this->Page_model->get_offering($type_offering_id);
                    $type_offering_img = base_url().'uploads/config/'.$type_offering->type_offering_images;
                } else {
                    $type_offering_img = base_url().'assets/images/sell1.png';
                }
                $condo_road_alley = $this->primaryclass->condo_road_soi($value->condo_road, $value->condo_alley);
                ?>
				<div class="title small-12 medium-4 large-3 column hover">
					<div class="row">
						<div class="location display small-12 large-12 column">
							<figure>
								<span style="right: 0;"><img src="<?php echo $type_offering_img;
                ?>" alt="" /></span>
								<a href="<?php echo site_url('condominium-details/'.$value->condo_id);
                ?>"><img src="<?php echo $pic_thumb;
                ?>" alt="<?php echo $value->condo_title;
                ?>"></a></figure>
							<div class="caption">
								<h2><?php echo $value->condo_title;
                ?></h2>
								<p>
									<?php echo $condo_road_alley;
                ?>
								</p>
							</div>
						</div>
					</div>
				</div>

				<?php

            }//END FOREACH
        ?>

		</div>
	</div>
	<div class="container bg-cs-gray">
		<div id="projects" class="row">
			<h2>โปรโมชั่นโครงการพิเศษ สำหรับงาน condo-compare Expo 2017 <img src="<?php echo base_url('assets/images/HOB.gif?v=001'); ?>"></h2>
			<div class="sub-container">
				<?php
                //HIGHLIGHT
                $i = 0;
                foreach ($newsarrival as $rownews => $valuenews) {
                    $pic_thumb = $sfilepath.'/'.$valuenews->pic_thumb;
                    $type_offering_id = (!empty($valuenews->type_offering_id)) ? $valuenews->type_offering_id : '';
                    // echo $valuenews->type_offering_id;
                    if (!empty($type_offering_id)) {
												$icon_hot =  base_url().'assets/images/hot.gif?v=001';
                        $type_offering = $this->Page_model->get_offering($type_offering_id);
                        $type_offering_img = base_url().'uploads/config/'.$type_offering->type_offering_images;
                        // echo $type_offering->type_offering_images;
                    } else {
                        $type_offering_img = base_url().'assets/images/sell1.png';
                    }
                    $newsarrival_road_alley = $this->primaryclass->condo_road_soi($valuenews->condo_road, $valuenews->condo_alley);

                    ?>
					<div class="title small-12 medium-6 large-3 column hover">
						<div class="row">
							<div class="location display small-12 large-12 column">
								<figure>
									<!-- <span><img src="<?php echo $type_offering_img;
                    ?>" alt="" /></span> -->
									<a href="<?php echo site_url('condominium-details/'.$valuenews->condo_id);?>"><img src="<?php echo $pic_thumb;?>" alt="<?php echo $valuenews->condo_title;?>"></a>
								</figure>
								<div class="caption">
									<h2><?php echo $valuenews->condo_title?> </h2>
									<p>
										<?php echo $newsarrival_road_alley; ?> <img src="<?php echo $icon_hot;?>" alt="Hot Property">
									</p>
								</div>
							</div>
						</div>
					</div>

					<?php
                    ++$i;
                    if ($i % 4 == 0) {
                        echo '</div><div class="sub-container">';
                    }
                }
            ?>
			</div>
			<!-- <div class="column"><a href="<?php //echo site_url('condominium'); ?>" class="button">ดูโครงการใหม่ทั้งหมด</a></div> -->
		</div>
	</div>

	<div class="container">
		<div id="videos" >
			<h2 class="left large-offset-4 large-8">วีดีโอโครงการ<span>ใหม่</span></h2>
      <div class="row" data-equalizer="video" data-equalize-on="large">
			<div class="title small-12 large-9 column bg-cs-blue " data-equalizer-watch="video">
				<?php
                $site_id = '1';
                $site_setting = $this->M->get_video_setting($site_id);
                $site_video_youtube = (!empty($site_setting['site_video_youtube'])) ? 'https://www.youtube.com/embed/'.$site_setting['site_video_youtube'].'?v'.VERSION : 'https://www.youtube.com/embed/WwrHDRsrNGM';
                $site_video_text = (!empty($site_setting['site_video_text'])) ? $site_setting['site_video_text'] : '';
                $site_video_url = (!empty($site_setting['site_video_url'])) ? $site_setting['site_video_url'] : '#';
                ?>
				<div class="mar-top-medium">
					<!-- <div class="location"> -->
						<!-- <figure class="video-container">
							<iframe src="<?php echo $site_video_youtube; ?>" frameborder="0" allowfullscreen width="640" height="370"></iframe>

						</figure> -->
						<div class="responsive-embed">
							<iframe width="640" height="370" src="<?php echo $site_video_youtube; ?>" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="caption">
							<p><?php echo $site_video_text; ?></p>
						</div>
						<!-- <div><a href="<?php echo $site_video_url; ?>" class="button">ดูวีดีโออื่นๆ</a></div> -->
					<!-- </div> -->
				</div>

			</div>

			<!-- ------- กิจกรรม --------- -->
			<div class="title small-12 large-3 column" data-equalizer-watch="video">
				<!-- <div class="row"> -->
				<!-- <div class="medium-6 large-12 columns"> -->
					<div id="" class="banner-carousel owl-carousel owl-theme">
					<!--  ID : 2 = banner หน้าแรก ขวามือหน้าเหนือกิจกรรม -->
					<?php
	          $banner_2 = $this->Page_model->get_banner_location(2);
	          $sfilepath_banner = base_url().'uploads/banner/';
	          $banner_count = count($banner_2);

	          // $banner_2_arr = array();
	          for ($i = 0; $i < $banner_count;  ++$i) {
	              $banner_2_id = (!empty($banner_2[$i]->banner_id)) ? $banner_2[$i]->banner_id : '';

	              //UPDATE DISPLAY
	              $displayed = (!empty($banner_2[$i]->displayed)) ? $banner_2[$i]->displayed : '';
	              $banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_2_id, 'banner');

	              $banner_2_title = (!empty($banner_2[$i]->banner_title)) ? $banner_2[$i]->banner_title : '';
	              $banner_2_url = (!empty($banner_2[$i]->banner_url)) ? $banner_2[$i]->banner_url : '#';
	              $banner_2_pic_thumb = (!empty($banner_2[$i]->pic_thumb)) ? $sfilepath_banner.$banner_2[$i]->pic_thumb : 'http://placehold.it/370x540/333?text=370x540';
	              // $banner_2_arr[] = '<a href="'.$banner_2_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_2_id.'"><img src="'.$banner_2_pic_thumb.'" alt="'.$banner_2_title.'" title=""></a>';
	              echo $banner_2_arr = '<div class="item"><a href="'.$banner_2_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_2_id.'"><img src="'.$banner_2_pic_thumb.'" alt="'.$banner_2_title.'" title=""></a></div>';
	          }
	          // shuffle($banner_2_arr);
	          // echo (!empty($banner_2_arr[0])) ? $banner_2_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/370x540/333?text=370x540" alt="370x540" title="370x540"></a>';
	                    ?>
					<!-- </div> -->
				</div>
				<!-- <div class="medium-6 large-12 columns"> -->
				<div class="clearfix"></div>
					<h2 class="right">กิจกรรม<span>ที่น่าสนใจ</span></h2>
					<!-- <div class="inside"> -->
					<article>
						<!-- <div class="row"> -->

					<?php
            //HIGHLIGHT
            $i = 0;
            $sfilepath_activities = base_url().'uploads/activities/';
            foreach ($activities as $rowactivities => $valueactivities) {
                $activities_id = (!empty($valueactivities->activities_id)) ? $valueactivities->activities_id : '-';

                $activities_date = (!empty($valueactivities->activities_date)) ? $valueactivities->activities_date : '';
                $activities_date_t = ($activities_date != '0000-00-00 00:00:00') ? $this->dateclass->DateTimeShortFormat($activities_date, 0, 1, 'Th') : 'ยังไม่มีข้อมูล';

                if (!empty($activities_date)) {
                    $activities_date_arr = explode(' ', $activities_date_t);
                    $activities_date_arr[0]; //วัน
                    $activities_date_arr[1]; //เดือน
                    $activities_date_arr[2]; //เดือน
                }

                $activities_days = (!empty($valueactivities->activities_days)) ? $valueactivities->activities_days : '-';
                $activities_title = (!empty($valueactivities->activities_title)) ? $valueactivities->activities_title : '-';
                $activities_caption = (!empty($valueactivities->activities_caption)) ? $valueactivities->activities_caption : '-';

                ?>

							<div class="activities-list small-12">

									<div class="small-4 large-4 columns bg-cs-blue"><span><?php echo $activities_date_arr[1];
                                ?></span><span><?php echo $activities_date_arr[0];
                                ?></span><span><?php echo $activities_days;
                                ?> Days</span></div>
									<div class="small-8 large-8 columns caption-list bg-cs-gray">
										<h3><?php echo $activities_title;
                                ?></h3>
										<p><?php echo $activities_caption;
                                ?></p>
									</div>

							</div>
							<div class="clearfix"></div>
							<?php

                            }//END FOREACH ?>

								<div class="large-12"><a href="<?php echo base_url('activities')?>" class="small expanded button">ดูกิจกรรมทั้งหมด</a></div>

						<!-- </div> -->
					</article>
				<!-- </div> -->
					<!-- </div> -->
				</div>

			</div>
		</div>


		</div>
	</div>

	<!--  ID : 3 = banner หน้าแรก เหนือ footer -->
	<div class="row clearfix">
		<div class="large-12 columns banner">
			<div id="" class="banner-carousel owl-carousel owl-theme">
			<?php
				$banner_3 = $this->Page_model->get_banner_location(3);
				$sfilepath_banner = base_url().'uploads/banner/';
				$banner_count = count($banner_3);

				// $banner_3_arr = array();
				for ($i = 0; $i < $banner_count;  ++$i) {
						$banner_3_id = (!empty($banner_3[$i]->banner_id)) ? $banner_3[$i]->banner_id : '';

					//UPDATE DISPLAY
					$displayed = (!empty($banner_3[$i]->displayed)) ? $banner_3[$i]->displayed : '';
						$banner_displayed = $this->M->update_displayed('banner_id', $displayed, $banner_3_id, 'banner');

						$banner_3_title = (!empty($banner_3[$i]->banner_title)) ? $banner_3[$i]->banner_title : '';
						$banner_3_url = (!empty($banner_3[$i]->banner_url)) ? $banner_3[$i]->banner_url : '#';
						$banner_3_pic_thumb = (!empty($banner_3[$i]->pic_thumb)) ? $sfilepath_banner.$banner_3[$i]->pic_thumb : 'http://placehold.it/1400x175/333?text=1400x175';
					// $banner_3_arr[] = '<a href="'.$banner_3_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_3_id.'"><img src="'.$banner_3_pic_thumb.'" alt="'.$banner_3_title.'" title=""></a>';
					echo $banner_3_arr = '<div class="item"><a href="'.$banner_3_url.'" class="condo-compare-banner" target="_blank" id="'.$banner_3_id.'"><img src="'.$banner_3_pic_thumb.'" alt="'.$banner_3_title.'" title=""></a></div>';
				}
				// shuffle($banner_3_arr);
				// echo (!empty($banner_3_arr[0])) ? $banner_3_arr[0] : '<a href="#" class="" target="_blank" id=""><img src="http://placehold.it/1400x175/333?text=1400x175" alt="1400x175" title="1400x175"></a>';
				?>
							</div>
		</div>
	</div>
	<?php $this->load->view('partials/footer', array('title' => isset($title) ? $title : '', 'css' => isset($css) ? $css : array(), 'BodyClass' => ''));?>


		<script>
			$(document).ready(function () {

				$('#bts').show();
				$("#transportation_category").change(function() {

				  var $value = $(this).val();
					console.log($value);

					if($value === 'bts') {
	          $('#bts').show();
						$('#mrt').hide();
						$('#airportlink').hide();
	        }

					else if($value === 'mrt') {
	          $('#mrt').show();
						$('#bts').hide();
						$('#airportlink').hide();
	        }

					else if($value === 'airportlink') {
	          $('#airportlink').show();
						$('#bts').hide();
						$('#mrt').hide();
	        }



				});

				$('#bts_mobile').show();
				$("#transportation_category_mobile").change(function() {

				  var $value = $(this).val();
					console.log($value);

					if($value === 'bts') {
	          $('#bts_mobile').show();
						$('#mrt_mobile').hide();
						$('#airportlink_mobile').hide();
	        }

					else if($value === 'mrt') {
	          $('#mrt_mobile').show();
						$('#bts_mobile').hide();
						$('#airportlink_mobile').hide();
	        }

					else if($value === 'airportlink') {
	          $('#airportlink_mobile').show();
						$('#bts_mobile').hide();
						$('#mrt_mobile').hide();
	        }



				});

			});
		</script>
